-- MySQL dump 10.13  Distrib 5.6.23, for Win64 (x86_64)
--
-- Host: vl00044    Database: higo_mb_dev
-- ------------------------------------------------------
-- Server version	5.6.27

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `areas`
--

DROP TABLE IF EXISTS `areas`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `areas` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `name_cn` varchar(50) NOT NULL,
  `create_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `update_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `update_by` varchar(50) DEFAULT NULL,
  `is_delete` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=utf8 AVG_ROW_LENGTH=8192;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `areas`
--

LOCK TABLES `areas` WRITE;
/*!40000 ALTER TABLE `areas` DISABLE KEYS */;
INSERT INTO `areas` VALUES (1,'Central and Western','中西區','2016-09-23 10:56:52','2016-09-23 10:56:52',NULL,0),(2,'Eastern','東區','2016-09-23 10:56:52','2016-09-23 10:56:52',NULL,0),(3,'Southern ','南區','2016-09-23 10:56:52','2016-09-23 10:56:52',NULL,0),(4,'Wan Chai','灣仔區','2016-09-23 10:56:52','2016-09-23 10:56:52',NULL,0),(5,'Sham Shui Po','深水埗區','2016-09-23 10:56:52','2016-09-23 10:56:52',NULL,0),(6,'Kowloon City','九龍城區','2016-09-23 10:56:52','2016-09-23 10:56:52',NULL,0),(7,'Kwun Tong','觀塘區','2016-09-23 10:56:52','2016-09-23 10:56:52',NULL,0),(8,'Wong Tai Sin','黃大仙區','2016-09-23 10:56:52','2016-09-23 10:56:52',NULL,0),(9,'Yau Tsim Mong','油尖旺區','2016-09-23 10:56:52','2016-09-23 10:56:52',NULL,0),(10,'Islands','離島區','2016-09-23 10:56:52','2016-09-23 10:56:52',NULL,0),(11,'Kwai Tsing','葵青區','2016-09-23 10:56:52','2016-09-23 10:56:52',NULL,0),(12,'North','北區','2016-09-23 10:56:52','2016-09-23 10:56:52',NULL,0),(13,'Sai Kung','西貢區','2016-09-23 10:56:52','2016-09-23 10:56:52',NULL,0),(14,'Sha Tin','沙田區','2016-09-23 10:56:52','2016-09-23 10:56:52',NULL,0),(15,'Tai Po','大埔區','2016-09-23 10:56:52','2016-09-23 10:56:52',NULL,0),(16,'Tsuen Wan','荃灣區','2016-09-23 10:56:52','2016-09-23 10:56:52',NULL,0),(17,'Tuen Mun','屯門區','2016-09-23 10:56:52','2016-09-23 10:56:52',NULL,0),(18,'Yuen Long','元朗區','2016-09-23 10:56:52','2016-09-23 10:56:52',NULL,0),(19,'Overseas','海外','2016-09-23 10:56:52','2016-09-23 10:56:52',NULL,0);
/*!40000 ALTER TABLE `areas` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_group`
--

DROP TABLE IF EXISTS `auth_group`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_group` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(80) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_group`
--

LOCK TABLES `auth_group` WRITE;
/*!40000 ALTER TABLE `auth_group` DISABLE KEYS */;
/*!40000 ALTER TABLE `auth_group` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_group_permissions`
--

DROP TABLE IF EXISTS `auth_group_permissions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_group_permissions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `group_id` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `auth_group_permissions_group_id_0cd325b0_uniq` (`group_id`,`permission_id`),
  KEY `auth_group_permissi_permission_id_84c5c92e_fk_auth_permission_id` (`permission_id`),
  CONSTRAINT `auth_group_permissi_permission_id_84c5c92e_fk_auth_permission_id` FOREIGN KEY (`permission_id`) REFERENCES `auth_permission` (`id`),
  CONSTRAINT `auth_group_permissions_group_id_b120cbf9_fk_auth_group_id` FOREIGN KEY (`group_id`) REFERENCES `auth_group` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_group_permissions`
--

LOCK TABLES `auth_group_permissions` WRITE;
/*!40000 ALTER TABLE `auth_group_permissions` DISABLE KEYS */;
/*!40000 ALTER TABLE `auth_group_permissions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_permission`
--

DROP TABLE IF EXISTS `auth_permission`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_permission` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `content_type_id` int(11) NOT NULL,
  `codename` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `auth_permission_content_type_id_01ab375a_uniq` (`content_type_id`,`codename`),
  CONSTRAINT `auth_permissi_content_type_id_2f476e4b_fk_django_content_type_id` FOREIGN KEY (`content_type_id`) REFERENCES `django_content_type` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=133 DEFAULT CHARSET=utf8 AVG_ROW_LENGTH=124;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_permission`
--

LOCK TABLES `auth_permission` WRITE;
/*!40000 ALTER TABLE `auth_permission` DISABLE KEYS */;
INSERT INTO `auth_permission` VALUES (1,'Can add log entry',1,'add_logentry'),(2,'Can change log entry',1,'change_logentry'),(3,'Can delete log entry',1,'delete_logentry'),(4,'Can add user',2,'add_user'),(5,'Can change user',2,'change_user'),(6,'Can delete user',2,'delete_user'),(7,'Can add group',3,'add_group'),(8,'Can change group',3,'change_group'),(9,'Can delete group',3,'delete_group'),(10,'Can add permission',4,'add_permission'),(11,'Can change permission',4,'change_permission'),(12,'Can delete permission',4,'delete_permission'),(13,'Can add content type',5,'add_contenttype'),(14,'Can change content type',5,'change_contenttype'),(15,'Can delete content type',5,'delete_contenttype'),(16,'Can add session',6,'add_session'),(17,'Can change session',6,'change_session'),(18,'Can delete session',6,'delete_session'),(19,'Can add areas',7,'add_areas'),(20,'Can change areas',7,'change_areas'),(21,'Can delete areas',7,'delete_areas'),(22,'Can add contacts',8,'add_contacts'),(23,'Can change contacts',8,'change_contacts'),(24,'Can delete contacts',8,'delete_contacts'),(25,'Can add coupon details',9,'add_coupondetails'),(26,'Can change coupon details',9,'change_coupondetails'),(27,'Can delete coupon details',9,'delete_coupondetails'),(28,'Can add coupons',10,'add_coupons'),(29,'Can change coupons',10,'change_coupons'),(30,'Can delete coupons',10,'delete_coupons'),(31,'Can add customer coupons',11,'add_customercoupons'),(32,'Can change customer coupons',11,'change_customercoupons'),(33,'Can delete customer coupons',11,'delete_customercoupons'),(34,'Can add customer foods',12,'add_customerfoods'),(35,'Can change customer foods',12,'change_customerfoods'),(36,'Can delete customer foods',12,'delete_customerfoods'),(37,'Can add customers',13,'add_customers'),(38,'Can change customers',13,'change_customers'),(39,'Can delete customers',13,'delete_customers'),(40,'Can add customer votes',14,'add_customervotes'),(41,'Can change customer votes',14,'change_customervotes'),(42,'Can delete customer votes',14,'delete_customervotes'),(43,'Can add food categories',15,'add_foodcategories'),(44,'Can change food categories',15,'change_foodcategories'),(45,'Can delete food categories',15,'delete_foodcategories'),(46,'Can add food items',16,'add_fooditems'),(47,'Can change food items',16,'change_fooditems'),(48,'Can delete food items',16,'delete_fooditems'),(49,'Can add food prices',17,'add_foodprices'),(50,'Can change food prices',17,'change_foodprices'),(51,'Can delete food prices',17,'delete_foodprices'),(52,'Can add foods',18,'add_foods'),(53,'Can change foods',18,'change_foods'),(54,'Can delete foods',18,'delete_foods'),(55,'Can add food types',19,'add_foodtypes'),(56,'Can change food types',19,'change_foodtypes'),(57,'Can delete food types',19,'delete_foodtypes'),(58,'Can add items',20,'add_items'),(59,'Can change items',20,'change_items'),(60,'Can delete items',20,'delete_items'),(61,'Can add restaurants',21,'add_restaurants'),(62,'Can change restaurants',21,'change_restaurants'),(63,'Can delete restaurants',21,'delete_restaurants'),(64,'Can add stations',22,'add_stations'),(65,'Can change stations',22,'change_stations'),(66,'Can delete stations',22,'delete_stations'),(67,'Can add supplier items',23,'add_supplieritems'),(68,'Can change supplier items',23,'change_supplieritems'),(69,'Can delete supplier items',23,'delete_supplieritems'),(70,'Can add supplier orders',24,'add_supplierorders'),(71,'Can change supplier orders',24,'change_supplierorders'),(72,'Can delete supplier orders',24,'delete_supplierorders'),(73,'Can add suppliers',25,'add_suppliers'),(74,'Can change suppliers',25,'change_suppliers'),(75,'Can delete suppliers',25,'delete_suppliers'),(76,'Can add topic categories',26,'add_topiccategories'),(77,'Can change topic categories',26,'change_topiccategories'),(78,'Can delete topic categories',26,'delete_topiccategories'),(79,'Can add topics',27,'add_topics'),(80,'Can change topics',27,'change_topics'),(81,'Can delete topics',27,'delete_topics'),(82,'Can add vote items',28,'add_voteitems'),(83,'Can change vote items',28,'change_voteitems'),(84,'Can delete vote items',28,'delete_voteitems'),(85,'Can add vote sessions',29,'add_votesessions'),(86,'Can change vote sessions',29,'change_votesessions'),(87,'Can delete vote sessions',29,'delete_votesessions'),(88,'Can add document',30,'add_document'),(89,'Can change document',30,'change_document'),(90,'Can delete document',30,'delete_document'),(91,'Can add upl coupon',31,'add_uplcoupon'),(92,'Can change upl coupon',31,'change_uplcoupon'),(93,'Can delete upl coupon',31,'delete_uplcoupon'),(94,'Can add upl customer',32,'add_uplcustomer'),(95,'Can change upl customer',32,'change_uplcustomer'),(96,'Can delete upl customer',32,'delete_uplcustomer'),(97,'Can add upl food',33,'add_uplfood'),(98,'Can change upl food',33,'change_uplfood'),(99,'Can delete upl food',33,'delete_uplfood'),(100,'Can add upl item',34,'add_uplitem'),(101,'Can change upl item',34,'change_uplitem'),(102,'Can delete upl item',34,'delete_uplitem'),(103,'Can add upl restaurant',35,'add_uplrestaurant'),(104,'Can change upl restaurant',35,'change_uplrestaurant'),(105,'Can delete upl restaurant',35,'delete_uplrestaurant'),(106,'Can add upl topic',36,'add_upltopic'),(107,'Can change upl topic',36,'change_upltopic'),(108,'Can delete upl topic',36,'delete_upltopic'),(109,'Can add operations',37,'add_operations'),(110,'Can change operations',37,'change_operations'),(111,'Can delete operations',37,'delete_operations'),(112,'Can add operations restaurants',38,'add_operationsrestaurants'),(113,'Can change operations restaurants',38,'change_operationsrestaurants'),(114,'Can delete operations restaurants',38,'delete_operationsrestaurants'),(115,'Can add settings',39,'add_settings'),(116,'Can change settings',39,'change_settings'),(117,'Can delete settings',39,'delete_settings'),(118,'Can add reservations',40,'add_reservations'),(119,'Can change reservations',40,'change_reservations'),(120,'Can delete reservations',40,'delete_reservations'),(121,'Can add customer points',41,'add_customerpoints'),(122,'Can change customer points',41,'change_customerpoints'),(123,'Can delete customer points',41,'delete_customerpoints'),(124,'Can add customer prizes',42,'add_customerprizes'),(125,'Can change customer prizes',42,'change_customerprizes'),(126,'Can delete customer prizes',42,'delete_customerprizes'),(127,'Can add prizes',43,'add_prizes'),(128,'Can change prizes',43,'change_prizes'),(129,'Can delete prizes',43,'delete_prizes'),(130,'Can add restaurant points',44,'add_restaurantpoints'),(131,'Can change restaurant points',44,'change_restaurantpoints'),(132,'Can delete restaurant points',44,'delete_restaurantpoints');
/*!40000 ALTER TABLE `auth_permission` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_user`
--

DROP TABLE IF EXISTS `auth_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `password` varchar(128) NOT NULL,
  `last_login` datetime(6) DEFAULT NULL,
  `is_superuser` tinyint(1) NOT NULL,
  `username` varchar(150) NOT NULL,
  `first_name` varchar(30) NOT NULL,
  `last_name` varchar(30) NOT NULL,
  `email` varchar(254) NOT NULL,
  `is_staff` tinyint(1) NOT NULL,
  `is_active` tinyint(1) NOT NULL,
  `date_joined` datetime(6) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`)
) ENGINE=InnoDB AUTO_INCREMENT=61 DEFAULT CHARSET=utf8 AVG_ROW_LENGTH=606;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_user`
--

LOCK TABLES `auth_user` WRITE;
/*!40000 ALTER TABLE `auth_user` DISABLE KEYS */;
INSERT INTO `auth_user` VALUES (1,'pbkdf2_sha256$30000$b9EGsvOtrRAK$yYUPOO5CHr7inGZvHOjy/SIjMp9Lqb6gVGhynZj1i+4=',NULL,1,'admin','admin','toyosu','admin@toyosu.co.jp',1,1,'2017-01-21 07:57:14.000000'),(2,'pbkdf2_sha256$30000$DC0oKvTfcqRn$COhsVIeunALjqOKrbVzL8/s83zJaGibgKJvP092gqB4=','2017-04-19 08:51:24.732254',1,'adminmente@toyosu.com','Toyosu','Mente','adminmente@toyosu.com',0,1,'2017-01-21 07:57:14.000000'),(3,'pbkdf2_sha256$30000$iTCohYY77GjB$oXJz99DBJmxBaSw/MAxZSZNfjenVCe9uvba8mnD3Xz8=',NULL,1,'TYSSS00001','toyosu','suisan','info@toyosu-suisan.com',0,1,'2017-01-21 08:08:58.000000'),(40,'pbkdf2_sha256$30000$qb8vEmQs7Khc$aRJF3R+1oS6H2nUucITuVU4wnF5a87+hwgO7/TyTQvE=','2017-04-20 06:37:46.426198',0,'linh.truong@inte.co.jp','','','linh.truong@inte.co.jp',0,1,'2017-03-10 08:05:09.133388'),(44,'pbkdf2_sha256$30000$yjZitmgP3UoA$tO9bFLEbC6EElWfAXerwiBB6h7PjE4cp+7mqg/2DCQ8=',NULL,0,'letu@gmail.com','','','letu@gmail.com',0,1,'2017-03-13 10:23:55.648305'),(45,'pbkdf2_sha256$30000$ZEKGtDL6pmQ0$kc/pX+b1jDWwp2UzZFKSneYd7SiaEow8VR6fdfSvW04=','2017-04-20 07:15:27.785316',1,'admin@higo.com','','','admin@higo.com',1,1,'2017-03-13 11:00:28.807098'),(46,'pbkdf2_sha256$30000$VVOsefXyjVg3$HNWci7OdmFtf9gyPfTVfMMaKHAaR6i0OodzsjwlRTuo=','2017-03-20 02:45:10.304284',0,'phu.le@inte.co.jp','','','phu.le@inte.co.jp',0,1,'2017-03-20 02:44:45.096763'),(47,'pbkdf2_sha256$30000$RGlgw3JxKfsc$olv27HH1t6IeT9Hdx6HXDSEwntYcuFmt73jau4kgdw4=',NULL,0,'aaasdasd@gmail.com','','','aaasdasd@gmail.com',0,1,'2017-03-28 08:50:11.499908'),(48,'pbkdf2_sha256$30000$hC8aCUuByjgo$BrFJJUBs+F0ZJH4lWCiLGV1Y//J1EB9mMb1WIBxviRk=',NULL,0,'linh.tgdrasfdsd@inte.com','','','linh.tgdrasfdsd@inte.com',0,1,'2017-03-28 08:59:06.742427'),(49,'pbkdf2_sha256$30000$rUOMwTREZ35N$IMUWJLjAVb9CphA1pcld6REi+wJup0mJkCoAn92hzFw=',NULL,0,'asdadsasd@gmail.com','','','asdadsasd@gmail.com',0,1,'2017-03-28 09:00:31.121864'),(50,'pbkdf2_sha256$30000$T60vISywhrXC$LH4IV5Nm3KJ8uqhIJwYo/x+QuE0PRmJGqc6AmsZ839s=',NULL,0,'asddasaht@gmail.com','','','asddasaht@gmail.com',0,1,'2017-03-30 03:48:56.711542'),(51,'pbkdf2_sha256$30000$p7nCuuM4FzR4$KDRxvSLY93ROD7Cipst+Q+p27DA5QNJDOO7GMU6Wj28=',NULL,0,'adasdahsk@gmail.com','','','adasdahsk@gmail.com',0,1,'2017-03-30 03:51:28.612410'),(52,'pbkdf2_sha256$30000$nmZ0KlkA6VK3$znrHpjCQQa/XLLKDTnMklMWG/bUxL/Izhl2W9+TUpZY=',NULL,0,'asdhgsatetst@gmail.com','','','asdhgsatetst@gmail.com',0,1,'2017-03-30 03:52:12.022751'),(53,'pbkdf2_sha256$30000$Nlsj1fcIxBIj$Pcnr/2ACwuA0sT7pMf/JjfqyHvudDyj0yhXCAwzhoXk=',NULL,0,'letu1@gmail.com','','','letu1@gmail.com',0,1,'2017-03-30 04:00:58.144357'),(54,'pbkdf2_sha256$30000$9CqUEIAtq28x$g/zuiSBRdJ45HYTLJUfKkxp5YZL8B+MVea1OcD1xcHE=',NULL,0,'tesst@magical.com','','','tesst@magical.com',0,1,'2017-03-30 07:22:28.375307'),(55,'pbkdf2_sha256$30000$9NnICCjyIfo9$oycAoNvG5A48SHbDbAHG9cbC6D27qNr/PwSgEI5Y1ZQ=',NULL,0,'babycute@gmail.com','','','babycute@gmail.com',0,1,'2017-03-30 07:23:22.835752'),(56,'pbkdf2_sha256$30000$6r7rRCsJzvDS$exqtROfoqJOhx867INNezAMlY/W7p3uhkTd/54v0czo=','2017-04-20 02:22:16.916561',0,'letu2@gmail.com','','','letu2@gmail.com',0,1,'2017-04-19 09:59:09.124238'),(57,'pbkdf2_sha256$30000$BaROn40NpPuf$a/napY1LF0A6IP10Y3YfZ06+q8yuSaPH0rz5UqYHWVc=',NULL,0,'testbirthday@gmail.com','','','testbirthday@gmail.com',0,1,'2017-04-19 10:09:36.281948'),(58,'pbkdf2_sha256$30000$MC0BXEvDaopB$cFAsByHWaHf54qkW1Yrop2bu7Ezj1cnAOD1xr0Dw/fY=',NULL,0,'birthdayphu@gmail.com','','','birthdayphu@gmail.com',0,1,'2017-04-19 10:10:48.770196'),(59,'pbkdf2_sha256$30000$oTiFFRohurtk$ishaUO1wwk2pQG2ZSnufpTEvtQtnesGVAcbT4P4yX+8=',NULL,0,'phubirthday@gmail.com','','','phubirthday@gmail.com',0,1,'2017-04-19 10:22:58.555167'),(60,'pbkdf2_sha256$30000$kbQoIp2N0t2q$rFzyMkMIfSzp3HYtQywcXWA0/oeZK6CFw1kocH+pf3Q=',NULL,0,'1234123g99@gmail.com','','','1234123g99@gmail.com',0,1,'2017-04-19 10:34:01.223865');
/*!40000 ALTER TABLE `auth_user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_user_groups`
--

DROP TABLE IF EXISTS `auth_user_groups`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_user_groups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `group_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `auth_user_groups_user_id_94350c0c_uniq` (`user_id`,`group_id`),
  KEY `auth_user_groups_group_id_97559544_fk_auth_group_id` (`group_id`),
  CONSTRAINT `auth_user_groups_group_id_97559544_fk_auth_group_id` FOREIGN KEY (`group_id`) REFERENCES `auth_group` (`id`),
  CONSTRAINT `auth_user_groups_user_id_6a12ed8b_fk_auth_user_id` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_user_groups`
--

LOCK TABLES `auth_user_groups` WRITE;
/*!40000 ALTER TABLE `auth_user_groups` DISABLE KEYS */;
/*!40000 ALTER TABLE `auth_user_groups` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_user_user_permissions`
--

DROP TABLE IF EXISTS `auth_user_user_permissions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_user_user_permissions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `auth_user_user_permissions_user_id_14a6b632_uniq` (`user_id`,`permission_id`),
  KEY `auth_user_user_perm_permission_id_1fbb5f2c_fk_auth_permission_id` (`permission_id`),
  CONSTRAINT `auth_user_user_perm_permission_id_1fbb5f2c_fk_auth_permission_id` FOREIGN KEY (`permission_id`) REFERENCES `auth_permission` (`id`),
  CONSTRAINT `auth_user_user_permissions_user_id_a95ead1b_fk_auth_user_id` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_user_user_permissions`
--

LOCK TABLES `auth_user_user_permissions` WRITE;
/*!40000 ALTER TABLE `auth_user_user_permissions` DISABLE KEYS */;
/*!40000 ALTER TABLE `auth_user_user_permissions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `bill_details`
--

DROP TABLE IF EXISTS `bill_details`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `bill_details` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `bill_id` int(11) NOT NULL,
  `product_id` int(11) DEFAULT NULL,
  `quantity` int(11) NOT NULL,
  `sales` decimal(8,2) DEFAULT NULL,
  `combo_code` varchar(50) DEFAULT '',
  `comment` varchar(500) DEFAULT NULL,
  `create_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `update_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `update_by` varchar(50) DEFAULT NULL,
  `is_delete` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `FK_BILLDETAIL_BILLS_idx` (`bill_id`),
  KEY `fk_billdetail_foods_idx` (`product_id`),
  CONSTRAINT `FK_BILLDETAIL_BILLS` FOREIGN KEY (`bill_id`) REFERENCES `bills` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_BILLDETAIL_PRODUCTS` FOREIGN KEY (`product_id`) REFERENCES `pos_products` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=4927 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `bill_details`
--

LOCK TABLES `bill_details` WRITE;
/*!40000 ALTER TABLE `bill_details` DISABLE KEYS */;
INSERT INTO `bill_details` VALUES (4865,428,4662,1,104.00,'2021','[ByBill, Product ID, Combo Code]:[1021-TA , 3002, 2021]','2017-04-19 11:30:33','2017-04-19 11:30:33',NULL,0),(4866,429,4663,1,0.00,'2021','[ByBill, Product ID, Combo Code]:[1021-TB , 2021, 2021]','2017-04-19 11:30:33','2017-04-19 11:30:33',NULL,0),(4867,429,4664,1,14.00,'2021','[ByBill, Product ID, Combo Code]:[1021-TB , 1002, 2021]','2017-04-19 11:30:34','2017-04-19 11:30:34',NULL,0),(4868,429,4665,1,14.00,'2021','[ByBill, Product ID, Combo Code]:[1021-TB , 2001, 2021]','2017-04-19 11:30:34','2017-04-19 11:30:34',NULL,0),(4869,429,4666,4,15.40,'AAA8014','[ByBill, Product ID, Combo Code]:[1021-TB , 11004, AAA8014]','2017-04-19 11:30:34','2017-04-19 11:30:34',NULL,0),(4870,429,4667,5,15.40,'AAA8014','[ByBill, Product ID, Combo Code]:[1021-TB , 11005, AAA8014]','2017-04-19 11:30:35','2017-04-19 11:30:35',NULL,0),(4871,429,4664,5,15.40,'AAA8014','[ByBill, Product ID, Combo Code]:[1021-TB , 1002, AAA8014]','2017-04-19 11:30:35','2017-04-19 11:30:35',NULL,0),(4872,429,4668,5,660.00,'AAA8014','[ByBill, Product ID, Combo Code]:[1021-TB , 1001, AAA8014]','2017-04-19 11:30:35','2017-04-19 11:30:35',NULL,0),(4873,429,4669,5,0.00,'','[ByBill, Product ID, Combo Code]:[1021-TB , AAA8014, ]','2017-04-19 11:30:35','2017-04-19 11:30:35',NULL,0),(4874,429,4670,6,0.00,'','[ByBill, Product ID, Combo Code]:[1021-TB , 98001, ]','2017-04-19 11:30:35','2017-04-19 11:30:35',NULL,0),(4875,429,4671,6,0.00,'','[ByBill, Product ID, Combo Code]:[1021-TB , 98002, ]','2017-04-19 11:30:35','2017-04-19 11:30:35',NULL,0),(4876,429,4672,7,0.00,'','[ByBill, Product ID, Combo Code]:[1021-TB , 15050, ]','2017-04-19 11:30:35','2017-04-19 11:30:35',NULL,0),(4877,429,4673,7,0.00,'','[ByBill, Product ID, Combo Code]:[1021-TB , 15052, ]','2017-04-19 11:30:36','2017-04-19 11:30:36',NULL,0),(4878,429,4674,7,299.00,'AAA8014','[ByBill, Product ID, Combo Code]:[1021-TB , 11001, AAA8014]','2017-04-19 11:30:36','2017-04-19 11:30:36',NULL,0),(4879,429,4675,8,319.00,'','[ByBill, Product ID, Combo Code]:[1021-TB , 9013, ]','2017-04-19 11:30:36','2017-04-19 11:30:36',NULL,0),(4880,429,4676,8,1119.40,'','[ByBill, Product ID, Combo Code]:[1021-TB , 9023, ]','2017-04-19 11:30:36','2017-04-19 11:30:36',NULL,0),(4881,429,4677,8,3469.20,'','[ByBill, Product ID, Combo Code]:[1021-TB , AA8014, ]','2017-04-19 11:30:36','2017-04-19 11:30:36',NULL,0),(4882,429,4678,9,1320.00,'AAA8014','[ByBill, Product ID, Combo Code]:[1021-TB , 1003, AAA8014]','2017-04-19 11:30:37','2017-04-19 11:30:37',NULL,0),(4883,429,4679,9,903.40,'','[ByBill, Product ID, Combo Code]:[1021-TB , 16009, ]','2017-04-19 11:30:37','2017-04-19 11:30:37',NULL,0),(4884,429,4680,9,522.40,'','[ByBill, Product ID, Combo Code]:[1021-TB , 9002, ]','2017-04-19 11:30:37','2017-04-19 11:30:37',NULL,0),(4885,429,4681,10,600060.00,'AAA8014','[ByBill, Product ID, Combo Code]:[1021-TB , 1004, AAA8014]','2017-04-19 11:30:37','2017-04-19 11:30:37',NULL,0),(4886,429,4682,12,555.00,'','[ByBill, Product ID, Combo Code]:[1021-TB , 10009, ]','2017-04-19 11:30:37','2017-04-19 11:30:37',NULL,0),(4887,429,4683,12,628.80,'','[ByBill, Product ID, Combo Code]:[1021-TB , 10001, ]','2017-04-19 11:30:38','2017-04-19 11:30:38',NULL,0),(4888,430,4684,12,333.60,'','[ByBill, Product ID, Combo Code]:[1021-TC , 5009, ]','2017-04-19 11:30:38','2017-04-19 11:30:38',NULL,0),(4889,430,4685,12,422.40,'','[ByBill, Product ID, Combo Code]:[1021-TC , 14024, ]','2017-04-19 11:30:38','2017-04-19 11:30:38',NULL,0),(4890,430,4686,12,1346.40,'','[ByBill, Product ID, Combo Code]:[1021-TC , 10010, ]','2017-04-19 11:30:38','2017-04-19 11:30:38',NULL,0),(4891,430,4687,12,807.40,'','[ByBill, Product ID, Combo Code]:[1021-TC , 16007, ]','2017-04-19 11:30:38','2017-04-19 11:30:38',NULL,0),(4892,430,4688,13,743.60,'','[ByBill, Product ID, Combo Code]:[1021-TC , 9005, ]','2017-04-19 11:30:38','2017-04-19 11:30:38',NULL,0),(4893,430,4689,13,1458.60,'','[ByBill, Product ID, Combo Code]:[1021-TC , 10012, ]','2017-04-19 11:30:39','2017-04-19 11:30:39',NULL,0),(4894,430,4690,13,92.40,'AAA8014','[ByBill, Product ID, Combo Code]:[1021-TC , 11003, AAA8014]','2017-04-19 11:30:39','2017-04-19 11:30:39',NULL,0),(4895,430,4691,13,743.60,'','[ByBill, Product ID, Combo Code]:[1021-TC , 9003, ]','2017-04-19 11:30:39','2017-04-19 11:30:39',NULL,0),(4896,430,4692,14,893.20,'','[ByBill, Product ID, Combo Code]:[1021-TC , 9012, ]','2017-04-19 11:30:39','2017-04-19 11:30:39',NULL,0),(4897,430,4693,14,0.00,'','[ByBill, Product ID, Combo Code]:[1021-TC , 15047, ]','2017-04-19 11:30:39','2017-04-19 11:30:39',NULL,0),(4898,430,4694,14,2125.20,'','[ByBill, Product ID, Combo Code]:[1021-TC , 5019, ]','2017-04-19 11:30:39','2017-04-19 11:30:39',NULL,0),(4899,430,4695,15,2112.00,'','[ByBill, Product ID, Combo Code]:[1021-TC , 9016, ]','2017-04-19 11:30:39','2017-04-19 11:30:39',NULL,0),(4900,430,4696,17,0.00,'','[ByBill, Product ID, Combo Code]:[1021-TC , 15049, ]','2017-04-19 11:30:39','2017-04-19 11:30:39',NULL,0),(4901,430,4697,17,17.80,'','[ByBill, Product ID, Combo Code]:[1021-TC , 15063, ]','2017-04-19 11:30:40','2017-04-19 11:30:40',NULL,0),(4902,430,4698,17,0.00,'','[ByBill, Product ID, Combo Code]:[1021-TC , 15048, ]','2017-04-19 11:30:40','2017-04-19 11:30:40',NULL,0),(4903,430,4699,18,940.80,'','[ByBill, Product ID, Combo Code]:[1021-TC , 9017, ]','2017-04-19 11:30:40','2017-04-19 11:30:40',NULL,0),(4904,430,4700,18,2019.60,'','[ByBill, Product ID, Combo Code]:[1021-TC , 10011, ]','2017-04-19 11:30:40','2017-04-19 11:30:40',NULL,0),(4905,430,4701,19,2667.60,'','[ByBill, Product ID, Combo Code]:[1021-TC , 9022, ]','2017-04-19 11:30:40','2017-04-19 11:30:40',NULL,0),(4906,430,4667,19,668.80,'','[ByBill, Product ID, Combo Code]:[1021-TC , 11005, ]','2017-04-19 11:30:40','2017-04-19 11:30:40',NULL,0),(4907,430,4702,19,418.00,'','[ByBill, Product ID, Combo Code]:[1021-TC , 6022, ]','2017-04-19 11:30:40','2017-04-19 11:30:40',NULL,0),(4908,430,4703,21,966.00,'','[ByBill, Product ID, Combo Code]:[1021-TC , 10002, ]','2017-04-19 11:30:41','2017-04-19 11:30:41',NULL,0),(4909,430,4704,22,1016.40,'','[ByBill, Product ID, Combo Code]:[1021-TC , 15065, ]','2017-04-19 11:30:41','2017-04-19 11:30:41',NULL,0),(4910,430,4705,22,0.00,'','[ByBill, Product ID, Combo Code]:[1021-TC , 15051, ]','2017-04-19 11:30:41','2017-04-19 11:30:41',NULL,0),(4911,430,4706,23,3997.40,'','[ByBill, Product ID, Combo Code]:[1021-TC , 12005, ]','2017-04-19 11:30:41','2017-04-19 11:30:41',NULL,0),(4912,430,4707,23,1315.60,'','[ByBill, Product ID, Combo Code]:[1021-TC , 9015, ]','2017-04-19 11:30:41','2017-04-19 11:30:41',NULL,0),(4913,430,4708,23,30.80,'AAA8014','[ByBill, Product ID, Combo Code]:[1021-TC , 11002, AAA8014]','2017-04-19 11:30:41','2017-04-19 11:30:41',NULL,0),(4914,430,4709,25,571.80,'','[ByBill, Product ID, Combo Code]:[1021-TC , 5006, ]','2017-04-19 11:30:41','2017-04-19 11:30:41',NULL,0),(4915,430,4710,25,990.00,'','[ByBill, Product ID, Combo Code]:[1021-TC , 14011, ]','2017-04-19 11:30:41','2017-04-19 11:30:41',NULL,0),(4916,430,4711,25,1430.00,'','[ByBill, Product ID, Combo Code]:[1021-TC , 9004, ]','2017-04-19 11:30:41','2017-04-19 11:30:41',NULL,0),(4917,430,4712,26,0.00,'','[ByBill, Product ID, Combo Code]:[1021-TC , 15045, ]','2017-04-19 11:30:42','2017-04-19 11:30:42',NULL,0),(4918,430,4713,26,1159.60,'','[ByBill, Product ID, Combo Code]:[1021-TC , 6021, ]','2017-04-19 11:30:42','2017-04-19 11:30:42',NULL,0),(4919,430,4714,27,1247.40,'','[ByBill, Product ID, Combo Code]:[1021-TC , 10004, ]','2017-04-19 11:30:42','2017-04-19 11:30:42',NULL,0),(4920,430,4715,28,644.80,'','[ByBill, Product ID, Combo Code]:[1021-TC , 5007, ]','2017-04-19 11:30:42','2017-04-19 11:30:42',NULL,0),(4921,430,4716,29,1084.60,'','[ByBill, Product ID, Combo Code]:[1021-TC , 14019, ]','2017-04-19 11:30:42','2017-04-19 11:30:42',NULL,0),(4922,430,4717,29,8442.00,'','[ByBill, Product ID, Combo Code]:[1021-TC , 15039, ]','2017-04-19 11:30:42','2017-04-19 11:30:42',NULL,0),(4923,430,4718,30,990.00,'AAA8014','[ByBill, Product ID, Combo Code]:[1021-TC , 1005, AAA8014]','2017-04-19 11:30:43','2017-04-19 11:30:43',NULL,0),(4924,430,4719,30,1.60,'','[ByBill, Product ID, Combo Code]:[1021-TC , 13011, ]','2017-04-19 11:30:43','2017-04-19 11:30:43',NULL,0),(4925,430,4720,32,1474.20,'','[ByBill, Product ID, Combo Code]:[1021-TC , 10003, ]','2017-04-19 11:30:43','2017-04-19 11:30:43',NULL,0),(4926,431,4669,61,200050.00,'AAA8014','[ByBill, Product ID, Combo Code]:[1021-TD , AAA8014, AAA8014]','2017-04-19 11:30:43','2017-04-19 11:30:43',NULL,0);
/*!40000 ALTER TABLE `bill_details` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `bills`
--

DROP TABLE IF EXISTS `bills`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `bills` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `bill_code` varchar(50) NOT NULL,
  `bill_date` datetime DEFAULT NULL,
  `customer_id` int(11) NOT NULL,
  `create_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `update_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `update_by` varchar(50) DEFAULT NULL,
  `is_delete` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `by_bill_UNIQUE` (`bill_code`),
  KEY `FK_bill_customers_idx` (`customer_id`),
  CONSTRAINT `FK_bill_customers` FOREIGN KEY (`customer_id`) REFERENCES `customers` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=432 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `bills`
--

LOCK TABLES `bills` WRITE;
/*!40000 ALTER TABLE `bills` DISABLE KEYS */;
INSERT INTO `bills` VALUES (428,'1021-TA','2017-02-10 16:21:55',20,'2017-04-19 11:30:33','2017-04-19 11:30:33',NULL,0),(429,'1021-TB','2017-02-10 16:21:55',25,'2017-04-19 11:30:33','2017-04-19 11:30:33',NULL,0),(430,'1021-TC','2017-02-10 16:21:55',26,'2017-04-19 11:30:38','2017-04-19 11:30:38',NULL,0),(431,'1021-TD','2018-02-10 16:21:55',27,'2017-04-19 11:30:43','2017-04-19 11:30:43',NULL,0);
/*!40000 ALTER TABLE `bills` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `commonmodels_document`
--

DROP TABLE IF EXISTS `commonmodels_document`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `commonmodels_document` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `file` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `commonmodels_document`
--

LOCK TABLES `commonmodels_document` WRITE;
/*!40000 ALTER TABLE `commonmodels_document` DISABLE KEYS */;
/*!40000 ALTER TABLE `commonmodels_document` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `commonmodels_uplcoupon`
--

DROP TABLE IF EXISTS `commonmodels_uplcoupon`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `commonmodels_uplcoupon` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `file` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `commonmodels_uplcoupon`
--

LOCK TABLES `commonmodels_uplcoupon` WRITE;
/*!40000 ALTER TABLE `commonmodels_uplcoupon` DISABLE KEYS */;
/*!40000 ALTER TABLE `commonmodels_uplcoupon` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `commonmodels_uplcustomer`
--

DROP TABLE IF EXISTS `commonmodels_uplcustomer`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `commonmodels_uplcustomer` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `file` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `commonmodels_uplcustomer`
--

LOCK TABLES `commonmodels_uplcustomer` WRITE;
/*!40000 ALTER TABLE `commonmodels_uplcustomer` DISABLE KEYS */;
/*!40000 ALTER TABLE `commonmodels_uplcustomer` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `commonmodels_uplfood`
--

DROP TABLE IF EXISTS `commonmodels_uplfood`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `commonmodels_uplfood` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `file` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `commonmodels_uplfood`
--

LOCK TABLES `commonmodels_uplfood` WRITE;
/*!40000 ALTER TABLE `commonmodels_uplfood` DISABLE KEYS */;
/*!40000 ALTER TABLE `commonmodels_uplfood` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `commonmodels_uplitem`
--

DROP TABLE IF EXISTS `commonmodels_uplitem`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `commonmodels_uplitem` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `file` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `commonmodels_uplitem`
--

LOCK TABLES `commonmodels_uplitem` WRITE;
/*!40000 ALTER TABLE `commonmodels_uplitem` DISABLE KEYS */;
/*!40000 ALTER TABLE `commonmodels_uplitem` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `commonmodels_uplrestaurant`
--

DROP TABLE IF EXISTS `commonmodels_uplrestaurant`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `commonmodels_uplrestaurant` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `file` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `commonmodels_uplrestaurant`
--

LOCK TABLES `commonmodels_uplrestaurant` WRITE;
/*!40000 ALTER TABLE `commonmodels_uplrestaurant` DISABLE KEYS */;
/*!40000 ALTER TABLE `commonmodels_uplrestaurant` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `commonmodels_upltopic`
--

DROP TABLE IF EXISTS `commonmodels_upltopic`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `commonmodels_upltopic` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `file` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `commonmodels_upltopic`
--

LOCK TABLES `commonmodels_upltopic` WRITE;
/*!40000 ALTER TABLE `commonmodels_upltopic` DISABLE KEYS */;
/*!40000 ALTER TABLE `commonmodels_upltopic` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `contacts`
--

DROP TABLE IF EXISTS `contacts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `contacts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `restaurant_id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `email` varchar(50) NOT NULL,
  `phone` varchar(20) NOT NULL,
  `content` varchar(2000) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1' COMMENT '1: Request; 2: Processing; 3: Finished; 4: Cancel',
  `create_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `update_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `update_by` varchar(50) DEFAULT NULL,
  `is_delete` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `FK_contacts_restaurants_id` (`restaurant_id`),
  CONSTRAINT `FK_contacts_restaurants_id` FOREIGN KEY (`restaurant_id`) REFERENCES `restaurants` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 AVG_ROW_LENGTH=1489;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `contacts`
--

LOCK TABLES `contacts` WRITE;
/*!40000 ALTER TABLE `contacts` DISABLE KEYS */;
INSERT INTO `contacts` VALUES (1,1,'bbbbb','linhtnnfpt@gmail.com','974030987','bbbb',1,'2017-02-03 05:52:31','2017-04-04 01:54:34',NULL,1),(2,1,'Admin Higo','admin@gmail.com','12345678','Aaaa',1,'2017-03-13 04:15:48','2017-03-13 04:15:48',NULL,0);
/*!40000 ALTER TABLE `contacts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `coupon_details`
--

DROP TABLE IF EXISTS `coupon_details`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `coupon_details` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `coupon_id` int(11) NOT NULL,
  `food_id` int(11) NOT NULL,
  `quantity` int(11) NOT NULL DEFAULT '1',
  `create_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `update_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `update_by` varchar(50) DEFAULT NULL,
  `is_delete` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `FK_coupon_details_coupons_id` (`coupon_id`),
  KEY `FK_coupon_details_foods_id` (`food_id`),
  CONSTRAINT `FK_coupon_details_coupons_id` FOREIGN KEY (`coupon_id`) REFERENCES `coupons` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_coupon_details_foods_id` FOREIGN KEY (`food_id`) REFERENCES `foods` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AVG_ROW_LENGTH=16384;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `coupon_details`
--

LOCK TABLES `coupon_details` WRITE;
/*!40000 ALTER TABLE `coupon_details` DISABLE KEYS */;
/*!40000 ALTER TABLE `coupon_details` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `coupons`
--

DROP TABLE IF EXISTS `coupons`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `coupons` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `restaurant_id` int(11) NOT NULL,
  `number` varchar(20) NOT NULL,
  `title` varchar(200) NOT NULL,
  `title_cn` varchar(200) NOT NULL,
  `name` varchar(200) DEFAULT NULL,
  `name_cn` varchar(200) DEFAULT NULL,
  `price_off` float(20,1) DEFAULT NULL,
  `price_minus` float(20,1) DEFAULT NULL,
  `from_date` datetime NOT NULL,
  `to_date` datetime NOT NULL,
  `start_time` time DEFAULT NULL,
  `end_time` time DEFAULT NULL,
  `week_days` varchar(50) DEFAULT NULL,
  `current_register` int(11) NOT NULL DEFAULT '0',
  `quantity` int(11) NOT NULL,
  `image_path` varchar(500) DEFAULT NULL,
  `image_width` int(11) NOT NULL DEFAULT '200',
  `image_height` int(11) NOT NULL DEFAULT '200',
  `coupon_type` tinyint(1) DEFAULT '0' COMMENT '0: Food; 1: Event',
  `comment` varchar(500) DEFAULT NULL,
  `comment_cn` varchar(500) DEFAULT NULL,
  `condition` varchar(2000) DEFAULT NULL,
  `condition_cn` varchar(2000) DEFAULT NULL,
  `is_mail_notify` tinyint(1) NOT NULL DEFAULT '1',
  `is_sms_notify` tinyint(1) NOT NULL DEFAULT '1',
  `is_app_notify` tinyint(1) NOT NULL DEFAULT '1',
  `is_already_send` tinyint(1) NOT NULL DEFAULT '0',
  `hash_tags` varchar(200) DEFAULT NULL,
  `create_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `update_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `update_by` varchar(50) DEFAULT NULL,
  `is_delete` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `FK_coupons_restaurant_id` (`restaurant_id`),
  CONSTRAINT `FK_coupons_restaurant_id` FOREIGN KEY (`restaurant_id`) REFERENCES `restaurants` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 AVG_ROW_LENGTH=5461;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `coupons`
--

LOCK TABLES `coupons` WRITE;
/*!40000 ALTER TABLE `coupons` DISABLE KEYS */;
INSERT INTO `coupons` VALUES (1,1,'CP000001','Please take a screen shot or print the coupon from below.','請存下或打印在下面的優惠券。  我們期望與您再見！','Sale 20$','銷售20$',20.0,NULL,'2016-10-01 00:00:00','2016-12-31 00:00:00','00:00:00','23:00:00','1,2',0,999,'coupons/coupon_201610.png',1010,698,1,'Please take a screen shot or print the coupon from below.','請存下或打印在下面的優惠券。  我們期望與您再見！','Each guest spending over $100 (plus service charge) can use a coupon, one coupon can be used once per person per visit','每個客人花費超過$ 100（加上服務費）可以使用優惠券，每個人每次訪問可以使用一個優惠券一次',1,1,1,0,'#linh','2016-10-25 06:37:33','2017-03-03 04:20:45','',0),(2,1,'CP000002','This is a free online calculator which counts the number of characters or letters in a text, useful for your tweets on Twitter, as well as a multitude of other applications.asdasdasdasdasdasdasddasaa.','請到場於上午11點45分鐘。','Sale 15% hoaSale 15% hoaSale 15% hoaSale 15% hoaSale 15% hoaSale 15% hoaSale 15% hoaSale 15% hoa','銷售15％',NULL,15.0,'2016-10-01 00:00:00','2017-04-04 00:00:00','00:00:00','23:00:00','1,3,5,7',0,999,'coupons/event_n_720.jpg',720,539,0,'','','Customers need to leave the venue before 1pm','客戶需要在下午1點前離開場地',0,0,1,0,'#coupon','2016-10-25 06:37:33','2017-03-27 07:33:10',NULL,0),(7,1,'CP889348','ahihi','ahihi','Sale 25$','ahihi',50.0,NULL,'2017-04-03 00:00:00','2017-05-31 00:00:00','00:00:00','23:30:00','1,2,3,4,5,6,7',0,1000,'coupons/1178718_16031010460040643247.jpg',1024,768,0,'','','asd','ads',0,0,0,0,'#coupon','2017-04-03 06:30:20','2017-04-03 06:31:17',NULL,0);
/*!40000 ALTER TABLE `coupons` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `customer_coupons`
--

DROP TABLE IF EXISTS `customer_coupons`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `customer_coupons` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `customer_id` int(11) NOT NULL,
  `coupon_id` int(11) NOT NULL,
  `register_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `use_date` datetime DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1' COMMENT '1: Register; 2: Used',
  `description` varchar(500) DEFAULT NULL,
  `create_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `update_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `update_by` varchar(50) DEFAULT NULL,
  `is_delete` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `FK_customer_coupons_coupons_id` (`coupon_id`),
  KEY `FK_customer_coupons_customers_id` (`customer_id`),
  CONSTRAINT `FK_customer_coupons_coupons_id` FOREIGN KEY (`coupon_id`) REFERENCES `coupons` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_customer_coupons_customers_id` FOREIGN KEY (`customer_id`) REFERENCES `customers` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `customer_coupons`
--

LOCK TABLES `customer_coupons` WRITE;
/*!40000 ALTER TABLE `customer_coupons` DISABLE KEYS */;
/*!40000 ALTER TABLE `customer_coupons` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `customer_foods`
--

DROP TABLE IF EXISTS `customer_foods`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `customer_foods` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `food_id` int(11) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `use_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `quantity` int(11) NOT NULL,
  `total_price` decimal(19,2) NOT NULL,
  `create_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `update_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `update_by` varchar(50) DEFAULT NULL,
  `is_delete` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `FK_customer_foods_customers_id` (`customer_id`),
  KEY `FK_customer_foods_foods_id` (`food_id`),
  CONSTRAINT `FK_customer_foods_customers_id` FOREIGN KEY (`customer_id`) REFERENCES `customers` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_customer_foods_foods_id` FOREIGN KEY (`food_id`) REFERENCES `foods` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `customer_foods`
--

LOCK TABLES `customer_foods` WRITE;
/*!40000 ALTER TABLE `customer_foods` DISABLE KEYS */;
/*!40000 ALTER TABLE `customer_foods` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `customer_points`
--

DROP TABLE IF EXISTS `customer_points`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `customer_points` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `customer_id` int(11) NOT NULL,
  `res_point_id` int(11) NOT NULL,
  `use_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `create_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `update_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `update_by` varchar(50) DEFAULT NULL,
  `is_delete` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0:Active, 1: Inactive',
  PRIMARY KEY (`id`),
  KEY `FK_customer_points_customers_id` (`customer_id`),
  KEY `FK_customer_points_restaurant_points_id` (`res_point_id`),
  CONSTRAINT `FK_customer_points_customers_id` FOREIGN KEY (`customer_id`) REFERENCES `customers` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_customer_points_restaurant_points_id` FOREIGN KEY (`res_point_id`) REFERENCES `restaurant_points` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8 AVG_ROW_LENGTH=16384;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `customer_points`
--

LOCK TABLES `customer_points` WRITE;
/*!40000 ALTER TABLE `customer_points` DISABLE KEYS */;
INSERT INTO `customer_points` VALUES (8,20,11,'2017-04-11 07:45:15','2017-04-11 07:45:15','2017-04-11 07:45:15',NULL,0);
/*!40000 ALTER TABLE `customer_points` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `customer_prizes`
--

DROP TABLE IF EXISTS `customer_prizes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `customer_prizes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `customer_id` int(11) NOT NULL,
  `prize_id` int(11) NOT NULL,
  `tracking_no` int(11) DEFAULT NULL,
  `delivery_type` tinyint(1) DEFAULT '1' COMMENT '1: Prize to home\n2: Go to restaurant to get prize\n3: Get direction',
  `name` varchar(50) DEFAULT NULL,
  `phone` varchar(20) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `address` varchar(500) DEFAULT NULL,
  `use_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `delivery_date` datetime DEFAULT NULL,
  `receive_date` datetime DEFAULT NULL,
  `status` tinyint(1) DEFAULT '1' COMMENT '1: Not delivery\n2: Sent\n3: Received\n4: Cancel',
  `create_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `update_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `update_by` varchar(50) DEFAULT NULL,
  `is_delete` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0:Active, 1: Delete',
  PRIMARY KEY (`id`),
  KEY `FK_customer_prizes_customers_id` (`customer_id`),
  KEY `FK_customer_prizes_prizes_id` (`prize_id`),
  CONSTRAINT `FK_customer_prizes_customers_id` FOREIGN KEY (`customer_id`) REFERENCES `customers` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_customer_prizes_prizes_id` FOREIGN KEY (`prize_id`) REFERENCES `prizes` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=202 DEFAULT CHARSET=utf8 AVG_ROW_LENGTH=67;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `customer_prizes`
--

LOCK TABLES `customer_prizes` WRITE;
/*!40000 ALTER TABLE `customer_prizes` DISABLE KEYS */;
INSERT INTO `customer_prizes` VALUES (181,17,20,NULL,1,'Linh Test','13254774','linh.truong@inte.co.jp','132456744gcghf','2017-04-18 01:53:01',NULL,NULL,1,'2017-04-18 01:53:01','2017-04-18 01:53:01',NULL,0),(182,17,20,NULL,1,'Linh Test','13254774','linh.truong@inte.co.jp','132456744gcghf','2017-04-18 01:55:45',NULL,NULL,1,'2017-04-18 01:55:45','2017-04-18 01:55:45',NULL,0),(183,17,20,NULL,1,'Linh Test','13254774','linh.truong@inte.co.jp','132456744gcghf','2017-04-18 01:56:04',NULL,NULL,1,'2017-04-18 01:56:04','2017-04-18 01:56:04',NULL,0),(184,17,20,NULL,1,'Linh Test','13254774','linh.truong@inte.co.jp','132456744gcghf','2017-04-18 01:56:36',NULL,NULL,1,'2017-04-18 01:56:36','2017-04-18 01:56:36',NULL,0),(185,17,20,NULL,1,'Linh Test','13254774','linh.truong@inte.co.jp','132456744gcghf','2017-04-18 01:57:43',NULL,NULL,1,'2017-04-18 01:57:43','2017-04-18 01:57:43',NULL,0),(186,17,20,NULL,1,'Linh Test','13254774','linh.truong@inte.co.jp','132456744gcghf','2017-04-18 01:59:08',NULL,NULL,1,'2017-04-18 01:59:08','2017-04-18 01:59:08',NULL,0),(187,17,20,NULL,1,'Linh Test','13254774','linh.truong@inte.co.jp','132456744gcghf','2017-04-18 02:00:49',NULL,NULL,1,'2017-04-18 02:00:49','2017-04-18 02:00:49',NULL,0),(188,17,20,NULL,1,'Linh Test','13254774','linh.truong@inte.co.jp','132456744gcghf','2017-04-18 02:09:28',NULL,NULL,1,'2017-04-18 02:09:28','2017-04-18 02:09:28',NULL,0),(189,17,12,NULL,1,'Linh Test','13254774','linh.truong@inte.co.jp','132456744gcghf','2017-04-18 02:12:15',NULL,NULL,1,'2017-04-18 02:12:15','2017-04-18 02:12:15',NULL,0),(190,17,12,NULL,1,'Linh Test','13254774','linh.truong@inte.co.jp','132456744gcghf','2017-04-18 02:12:46',NULL,NULL,1,'2017-04-18 02:12:46','2017-04-18 02:12:46',NULL,0),(191,17,20,NULL,1,'Linh Test','13254774','linh.truong@inte.co.jp','132456744gcghf','2017-04-18 02:14:32',NULL,'2017-04-18 02:14:32',1,'2017-04-18 02:14:32','2017-04-18 02:14:32',NULL,0),(192,17,20,NULL,1,'Linh Test','13254774','linh.truong@inte.co.jp','132456744gcghf','2017-04-18 02:15:08',NULL,'2017-04-18 02:15:08',1,'2017-04-18 02:15:08','2017-04-18 02:15:08',NULL,0),(193,17,18,NULL,1,'Linh Test','13254774','linh.truong@inte.co.jp','132456744gcghf','2017-04-18 02:20:50',NULL,'2017-04-18 02:20:50',1,'2017-04-18 02:20:50','2017-04-18 02:20:50',NULL,0),(194,20,20,NULL,1,'Roann Admin','1234658778','admin@higo.com','','2017-04-18 02:31:33',NULL,NULL,1,'2017-04-18 02:31:33','2017-04-18 02:31:33',NULL,0),(195,20,20,NULL,1,'Roann Admin','1234658778','admin@higo.com','','2017-04-18 02:32:07',NULL,NULL,1,'2017-04-18 02:32:07','2017-04-18 02:32:07',NULL,0),(196,17,20,NULL,1,'Linh Test','13254774','linh.truong@inte.co.jp','132456744gcghf','2017-04-18 02:35:21',NULL,'2017-04-18 02:35:21',1,'2017-04-18 02:35:21','2017-04-18 02:35:21',NULL,0),(197,17,20,NULL,1,'Linh Test','13254774','linh.truong@inte.co.jp','132456744gcghf','2017-04-18 02:36:01',NULL,'2017-04-18 02:36:01',1,'2017-04-18 02:36:01','2017-04-18 02:36:01',NULL,0),(198,17,20,NULL,1,'Linh Test','13254774','linh.truong@inte.co.jp','132456744gcghf','2017-04-18 02:39:02',NULL,NULL,1,'2017-04-18 02:39:02','2017-04-18 02:39:02',NULL,0),(199,20,20,NULL,1,'Roann Admin','1234658778','admin@higo.com','','2017-04-18 02:42:36',NULL,NULL,1,'2017-04-18 02:42:36','2017-04-18 02:42:36',NULL,0),(200,20,20,NULL,1,'Roann Admin','1234658778','admin@higo.com','','2017-04-18 03:27:09',NULL,NULL,1,'2017-04-18 03:27:09','2017-04-18 03:27:09',NULL,0),(201,20,20,NULL,1,'Roann Admin','1234658778','admin@higo.com','vn','2017-04-18 03:29:48',NULL,NULL,1,'2017-04-18 03:29:48','2017-04-18 03:29:48',NULL,0);
/*!40000 ALTER TABLE `customer_prizes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `customer_votes`
--

DROP TABLE IF EXISTS `customer_votes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `customer_votes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `session_id` int(11) NOT NULL,
  `vote_item_id` int(11) NOT NULL,
  `rate` tinyint(1) NOT NULL COMMENT 'From 1-5',
  `create_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `update_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `update_by` varchar(50) DEFAULT NULL,
  `is_delete` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `FK_customer_votes_vote_items_id` (`vote_item_id`),
  KEY `FK_customer_votes_vote_sessions_id` (`session_id`),
  CONSTRAINT `FK_customer_votes_vote_items_id` FOREIGN KEY (`vote_item_id`) REFERENCES `vote_items` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_customer_votes_vote_sessions_id` FOREIGN KEY (`session_id`) REFERENCES `vote_sessions` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `customer_votes`
--

LOCK TABLES `customer_votes` WRITE;
/*!40000 ALTER TABLE `customer_votes` DISABLE KEYS */;
/*!40000 ALTER TABLE `customer_votes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `customers`
--

DROP TABLE IF EXISTS `customers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `customers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `area_id` int(11) NOT NULL,
  `restaurant_id` int(11) NOT NULL,
  `number` int(11) DEFAULT NULL,
  `name` varchar(50) NOT NULL,
  `image_path` varchar(500) DEFAULT NULL,
  `age` int(11) NOT NULL,
  `birthday` datetime DEFAULT NULL,
  `zip_code` varchar(10) DEFAULT NULL,
  `sex` tinyint(1) NOT NULL DEFAULT '2' COMMENT '0: Male; 1: Female; 2: Unknow',
  `address` varchar(200) DEFAULT NULL,
  `gps_data` varchar(200) DEFAULT NULL,
  `email` varchar(50) NOT NULL,
  `login_password` varchar(50) NOT NULL,
  `phone` varchar(20) DEFAULT NULL,
  `last_transaction_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `current_point` int(11) NOT NULL DEFAULT '0',
  `total_point` int(11) NOT NULL DEFAULT '0',
  `is_mail_notify` tinyint(1) NOT NULL DEFAULT '1' COMMENT '1: Receive; 0: No receive',
  `is_sms_notify` tinyint(1) NOT NULL DEFAULT '1' COMMENT '1: Receive; 0: No receive',
  `access_token` varchar(60) DEFAULT NULL,
  `recover_password` varchar(20) DEFAULT NULL,
  `level_up_date` datetime DEFAULT NULL,
  `num_visit` int(11) NOT NULL DEFAULT '0',
  `create_date` datetime NOT NULL,
  `update_date` datetime NOT NULL,
  `update_by` varchar(50) DEFAULT NULL,
  `is_delete` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `number_UNIQUE` (`number`),
  KEY `FK_customers_restaurants_id` (`restaurant_id`),
  KEY `FK_customers_areas_id` (`area_id`),
  CONSTRAINT `FK_customers_areas_id` FOREIGN KEY (`area_id`) REFERENCES `areas` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=39 DEFAULT CHARSET=utf8 AVG_ROW_LENGTH=1092;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `customers`
--

LOCK TABLES `customers` WRITE;
/*!40000 ALTER TABLE `customers` DISABLE KEYS */;
INSERT INTO `customers` VALUES (17,40,6,1,12345678,'Linh Nhut','customers/Amazon_dev.png',26,'1991-08-24 17:00:00',NULL,0,'132456744gcghf','','linh.truong@inte.co.jp','111qqq','13254774','2017-04-20 14:43:58',13050,7000,1,1,NULL,NULL,'2017-04-20 07:44:01',0,'2017-03-10 08:05:09','2017-04-20 07:44:01',NULL,0),(20,45,2,1,10000001,'Roann Admin','',27,'1990-08-21 17:00:00',NULL,2,'asdadsadsads','','admin@higo.com','111qqq','1234658778','2018-02-20 16:21:55',1,0,1,1,NULL,NULL,'2018-02-20 16:21:55',1,'2017-03-13 11:00:29','2017-04-20 07:55:14',NULL,0),(25,46,1,1,10000002,'Phu Le','',30,NULL,NULL,2,'','','phu.le@inte.co.jp','111qqq','123456789','2018-02-10 16:21:55',6099,6099,1,1,NULL,NULL,NULL,1,'2017-03-20 02:44:45','2017-04-10 06:06:07',NULL,1),(26,47,1,1,10000003,'retreat stat','',30,NULL,NULL,2,'','','aaasdasd@gmail.com','111qqq','12347484','2018-02-10 16:21:55',431,431,1,1,NULL,NULL,'2017-04-19 03:58:39',1,'2017-03-28 08:50:12','2017-03-28 08:50:12',NULL,0),(27,48,1,1,10000004,'Asdasddsa Add Add','',30,NULL,NULL,2,'','','linh.tgdrasfdsd@inte.com','111qqq','12345678911','2018-02-10 16:21:55',2000,2000,1,1,NULL,NULL,'2018-02-10 16:21:55',1,'2017-03-28 08:59:07','2017-03-28 08:59:07',NULL,0),(28,49,1,1,81394726,'Assad As D','',30,NULL,NULL,2,'','','asdadsasd@gmail.com','111qqq','11111111123','2017-03-28 09:00:37',0,0,1,1,NULL,NULL,'2017-04-19 03:58:39',0,'2017-03-28 09:00:31','2017-03-28 09:00:31',NULL,0),(29,51,1,1,65663390,'Adsdasdsa Add','',30,NULL,NULL,2,'','','adasdahsk@gmail.com','111qqq','1123123134','2017-03-30 03:51:27',0,0,1,1,NULL,NULL,'2017-04-19 03:58:39',0,'2017-03-30 03:51:29','2017-03-30 03:51:29',NULL,0),(30,52,5,1,31636038,'Adssdsa Asd','',30,NULL,NULL,0,'','','asdhgsatetst@gmail.com','111qqq','56481233','2017-03-30 03:52:11',0,0,1,1,NULL,NULL,'2017-04-19 03:58:39',0,'2017-03-30 03:52:12','2017-03-30 03:52:12',NULL,0),(31,53,1,1,79458594,'le tu','',30,NULL,NULL,0,'','','letu1@gmail.com','111qqq','156836404541','2017-03-30 11:00:55',0,0,1,1,NULL,NULL,'2017-04-19 03:58:39',0,'2017-03-30 04:01:00','2017-03-30 04:01:00',NULL,0),(32,54,16,1,91399657,'Test Tat','',30,NULL,NULL,2,'','','tesst@magical.com','111qqq','123785131','2017-03-30 07:22:24',0,0,1,1,NULL,NULL,'2017-04-19 03:58:39',0,'2017-03-30 07:22:31','2017-03-30 07:22:31',NULL,0),(33,55,2,1,99287786,'test cms phu','customers/Amazon_dev.png',30,NULL,NULL,2,'vdsfdsfdfsdsffds phu','','babycute@gmail.com','111qqq','1231235556','2017-03-30 07:23:22',10000,10000,1,1,NULL,NULL,'2017-04-19 03:58:39',0,'2017-03-30 07:23:23','2017-04-10 06:20:37',NULL,0),(34,56,1,1,90873129,'Le Tu','',31,'1986-05-21 00:00:00',NULL,0,'','','letu2@gmail.com','111qqq','187568926834','2017-04-19 16:59:08',0,0,1,1,NULL,NULL,NULL,0,'2017-04-19 09:59:09','2017-04-20 02:43:15',NULL,0),(35,57,1,1,28605785,'Test Birthday','',12,'2011-04-19 10:09:16',NULL,2,'','','testbirthday@gmail.com','111qqq','84132564513','2017-04-19 10:09:42',0,0,1,1,NULL,NULL,NULL,0,'2017-04-19 10:09:36','2017-04-19 10:09:36',NULL,0),(36,58,1,1,86523738,'Phu Test Birthday','',12,'1980-05-01 10:10:07',NULL,2,'','','birthdayphu@gmail.com','111qqq','564564351','2017-04-19 10:10:54',0,0,1,1,NULL,NULL,NULL,0,'2017-04-19 10:10:49','2017-04-19 10:10:49',NULL,0),(37,59,1,1,80252656,'Phutest BD','',40,'1977-05-01 10:22:11',NULL,2,'','','phubirthday@gmail.com','111qqq','1112312331','2017-04-19 10:23:04',0,0,1,1,NULL,NULL,NULL,0,'2017-04-19 10:22:59','2017-04-19 10:22:59',NULL,0),(38,60,1,1,65048494,'Asdasd Asdasd','',12,'2004-12-31 17:00:00',NULL,2,'','','1234123g99@gmail.com','111qqq','8974561545','2017-04-19 10:34:07',0,0,1,1,NULL,NULL,NULL,0,'2017-04-19 10:34:01','2017-04-19 10:34:01',NULL,0);
/*!40000 ALTER TABLE `customers` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `device_info`
--

DROP TABLE IF EXISTS `device_info`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `device_info` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `device_token` varchar(500) NOT NULL,
  `type` tinyint(1) NOT NULL COMMENT '0:IOS; 1:Anroind',
  `is_push_notify` tinyint(1) NOT NULL DEFAULT '1',
  `create_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `update_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `update_by` varchar(50) DEFAULT NULL,
  `is_delete` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AVG_ROW_LENGTH=862;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `device_info`
--

LOCK TABLES `device_info` WRITE;
/*!40000 ALTER TABLE `device_info` DISABLE KEYS */;
/*!40000 ALTER TABLE `device_info` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `django_admin_log`
--

DROP TABLE IF EXISTS `django_admin_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `django_admin_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `action_time` datetime(6) NOT NULL,
  `object_id` longtext,
  `object_repr` varchar(200) NOT NULL,
  `action_flag` smallint(5) unsigned NOT NULL,
  `change_message` longtext NOT NULL,
  `content_type_id` int(11) DEFAULT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `django_admin__content_type_id_c4bce8eb_fk_django_content_type_id` (`content_type_id`),
  KEY `django_admin_log_user_id_c564eba6_fk_auth_user_id` (`user_id`),
  CONSTRAINT `django_admin__content_type_id_c4bce8eb_fk_django_content_type_id` FOREIGN KEY (`content_type_id`) REFERENCES `django_content_type` (`id`),
  CONSTRAINT `django_admin_log_user_id_c564eba6_fk_auth_user_id` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AVG_ROW_LENGTH=188;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `django_admin_log`
--

LOCK TABLES `django_admin_log` WRITE;
/*!40000 ALTER TABLE `django_admin_log` DISABLE KEYS */;
/*!40000 ALTER TABLE `django_admin_log` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `django_content_type`
--

DROP TABLE IF EXISTS `django_content_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `django_content_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `app_label` varchar(100) NOT NULL,
  `model` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `django_content_type_app_label_76bd3d3b_uniq` (`app_label`,`model`)
) ENGINE=InnoDB AUTO_INCREMENT=46 DEFAULT CHARSET=utf8 AVG_ROW_LENGTH=364;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `django_content_type`
--

LOCK TABLES `django_content_type` WRITE;
/*!40000 ALTER TABLE `django_content_type` DISABLE KEYS */;
INSERT INTO `django_content_type` VALUES (1,'admin','logentry'),(3,'auth','group'),(4,'auth','permission'),(2,'auth','user'),(7,'commonmodels','areas'),(8,'commonmodels','contacts'),(9,'commonmodels','coupondetails'),(10,'commonmodels','coupons'),(11,'commonmodels','customercoupons'),(12,'commonmodels','customerfoods'),(41,'commonmodels','customerpoints'),(42,'commonmodels','customerprizes'),(13,'commonmodels','customers'),(14,'commonmodels','customervotes'),(45,'commonmodels','deviceinfo'),(30,'commonmodels','document'),(15,'commonmodels','foodcategories'),(16,'commonmodels','fooditems'),(17,'commonmodels','foodprices'),(18,'commonmodels','foods'),(19,'commonmodels','foodtypes'),(20,'commonmodels','items'),(37,'commonmodels','operations'),(38,'commonmodels','operationsrestaurants'),(43,'commonmodels','prizes'),(40,'commonmodels','reservations'),(44,'commonmodels','restaurantpoints'),(21,'commonmodels','restaurants'),(39,'commonmodels','settings'),(22,'commonmodels','stations'),(23,'commonmodels','supplieritems'),(24,'commonmodels','supplierorders'),(25,'commonmodels','suppliers'),(26,'commonmodels','topiccategories'),(27,'commonmodels','topics'),(31,'commonmodels','uplcoupon'),(32,'commonmodels','uplcustomer'),(33,'commonmodels','uplfood'),(34,'commonmodels','uplitem'),(35,'commonmodels','uplrestaurant'),(36,'commonmodels','upltopic'),(28,'commonmodels','voteitems'),(29,'commonmodels','votesessions'),(5,'contenttypes','contenttype'),(6,'sessions','session');
/*!40000 ALTER TABLE `django_content_type` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `django_migrations`
--

DROP TABLE IF EXISTS `django_migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `django_migrations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `app` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `applied` datetime(6) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=utf8 AVG_ROW_LENGTH=780;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `django_migrations`
--

LOCK TABLES `django_migrations` WRITE;
/*!40000 ALTER TABLE `django_migrations` DISABLE KEYS */;
INSERT INTO `django_migrations` VALUES (1,'contenttypes','0001_initial','2016-10-27 07:09:44.331201'),(2,'auth','0001_initial','2016-10-27 07:09:56.656433'),(3,'admin','0001_initial','2016-10-27 07:09:59.270695'),(4,'admin','0002_logentry_remove_auto_add','2016-10-27 07:09:59.393707'),(5,'contenttypes','0002_remove_content_type_name','2016-10-27 07:10:00.984866'),(6,'auth','0002_alter_permission_name_max_length','2016-10-27 07:10:01.946962'),(7,'auth','0003_alter_user_email_max_length','2016-10-27 07:10:02.890057'),(8,'auth','0004_alter_user_username_opts','2016-10-27 07:10:02.950063'),(9,'auth','0005_alter_user_last_login_null','2016-10-27 07:10:03.628131'),(10,'auth','0006_require_contenttypes_0002','2016-10-27 07:10:03.702138'),(11,'auth','0007_alter_validators_add_error_messages','2016-10-27 07:10:03.783146'),(12,'auth','0008_alter_user_username_max_length','2016-10-27 07:10:04.621230'),(13,'commonmodels','0001_initial','2016-10-27 07:10:04.752243'),(14,'sessions','0001_initial','2016-10-27 07:10:05.477315'),(15,'commonmodels','0002_document','2016-10-28 03:23:18.673107'),(16,'commonmodels','0003_uplcoupon_uplcustomer_uplfood_uplitem_uplrestaurant_upltopic','2016-10-28 04:26:28.344355'),(17,'commonmodels','0004_operations_operationsrestaurants_settings','2016-10-31 03:25:54.492953'),(18,'commonmodels','0005_reservations','2016-11-02 07:27:46.924354'),(19,'commonmodels','0002_auto_20161005_1505','2016-11-07 10:43:06.724222'),(20,'commonmodels','0003_auto_20161010_1405','2016-11-07 10:43:06.832222'),(21,'commonmodels','0006_merge_20161107_1743','2016-11-07 10:43:06.902222'),(22,'commonmodels','0006_customerpoints_customerprizes_prizes_restaurantpoints','2016-11-08 10:00:12.083172');
/*!40000 ALTER TABLE `django_migrations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `django_session`
--

DROP TABLE IF EXISTS `django_session`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `django_session` (
  `session_key` varchar(40) NOT NULL,
  `session_data` longtext NOT NULL,
  `expire_date` datetime(6) NOT NULL,
  PRIMARY KEY (`session_key`),
  KEY `django_session_de54fa62` (`expire_date`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AVG_ROW_LENGTH=690;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `django_session`
--

LOCK TABLES `django_session` WRITE;
/*!40000 ALTER TABLE `django_session` DISABLE KEYS */;
INSERT INTO `django_session` VALUES ('0n533m26pen0fziowoyl2di9pdjowtow','NmI3NGM4OGE5MjRlMzcyYjI5ZmYyNmYwOTIwYjYzMDlkYzk3M2QyZTp7InVzZXJfaWQiOjE3LCJfc2Vzc2lvbl9leHBpcnkiOjk5OTk5OTk5LCJhdXRoX3Rva2VuIjoiMG41MzNtMjZwZW4wZnppb3dveWwyZGk5cGRqb3d0b3ciLCJfYXV0aF9zaWduX2luX2F1dGgiOmZhbHNlLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9hdXRoX3NpZ25faW5fbWFwcGluZ19pZCI6NDAsIl9hdXRoX3VzZXJfaWQiOiI0MCIsIl9hdXRoX3NpZ25faW5faWQiOjE3LCJfYXV0aF91c2VyX2hhc2giOiJmM2JhZjA5M2ExZDA4ZGZiZGIxZmUwM2RlNmQ1ODdjY2ZjYjRiNzQxIn0=','2020-05-13 16:17:05.351944'),('0rzsdbdi7l6fa7xzekw4bc7y4q4j22wo','MjM1ZmY4MTUxNmIyN2RlNjRiZDYzMjAxNDkyZGFjNDE1MmZiZjE5OTp7Il9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfc2lnbl9pbl9hdXRoIjpmYWxzZSwiYXV0aF90b2tlbiI6IjByenNkYmRpN2w2ZmE3eHpla3c0YmM3eTRxNGoyMndvIiwiX2F1dGhfc2lnbl9pbl9pZCI6MjAsIl9hdXRoX3VzZXJfaWQiOiI0NSIsIl9hdXRoX3NpZ25faW5fbWFwcGluZ19pZCI6NDUsInVzZXJfaWQiOjIwLCJfc2Vzc2lvbl9leHBpcnkiOjk5OTk5OTk5LCJfYXV0aF91c2VyX2hhc2giOiI4N2VlZTk5ZDM5NDVkYjgxN2E0Y2FiZDM0MWE1NzcyYWJkMTY0MTM2In0=','2020-05-14 12:09:24.517288'),('16c85hr68es2erk2fc0c3rizes4kqwyp','YzgwODVmYTNkMThjZGY4M2RkZmZhZTRiMWVjYTNkYmJmMTlmODdkZTp7ImF1dGhfdG9rZW4iOiIxNmM4NWhyNjhlczJlcmsyZmMwYzNyaXplczRrcXd5cCIsIl9hdXRoX3VzZXJfaWQiOiI1NiIsIl9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfc2lnbl9pbl9tYXBwaW5nX2lkIjo1NiwiX2F1dGhfdXNlcl9oYXNoIjoiZGY2Y2M5OWMwZTRhNWEzZGVjYzNlZmQ2NTk3MDdlNWNlMDFmNDVhZSIsIl9hdXRoX3NpZ25faW5fYXV0aCI6ZmFsc2UsIl9hdXRoX3NpZ25faW5faWQiOjM0LCJ1c2VyX2lkIjozNCwiX3Nlc3Npb25fZXhwaXJ5Ijo5OTk5OTk5OX0=','2020-06-20 12:08:56.059575'),('19elt2mkfm74rsq14kqpb8rn8reczyjv','NjhlYjU1MjZlOWQxZDRjYzM3NTMxZDAzZDJkZjBkN2IxNmM3M2JiNDp7Il9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9oYXNoIjoiNGMzOGY4MzZlOGMxOTMyM2FkNWM5Mzc5ZGM4MTJhOTMzMDExOWRiNyIsIl9zZXNzaW9uX2V4cGlyeSI6OTk5OTk5OTksInVzZXJfaWQiOjE4LCJhdXRoX3Rva2VuIjoiMTllbHQybWtmbTc0cnNxMTRrcXBiOHJuOHJlY3p5anYiLCJfYXV0aF9zaWduX2luX21hcHBpbmdfaWQiOjQxLCJfYXV0aF9zaWduX2luX2F1dGgiOmZhbHNlLCJfYXV0aF91c2VyX2lkIjoiNDEiLCJfYXV0aF9zaWduX2luX2lkIjoxOH0=','2020-05-13 13:38:54.660880'),('1l67phcllk6lt6r938fov5un1uqjswjt','NTkxNDMwNDljMjI4ZTgwOTM1MjgzMTZlYTFiMGJhMmU2ODJkMWZjMzp7InVzZXJfaWQiOjE4LCJfc2Vzc2lvbl9leHBpcnkiOjk5OTk5OTk5LCJhdXRoX3Rva2VuIjoiMWw2N3BoY2xsazZsdDZyOTM4Zm92NXVuMXVxanN3anQiLCJfYXV0aF9zaWduX2luX2F1dGgiOmZhbHNlLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9hdXRoX3NpZ25faW5fbWFwcGluZ19pZCI6NDEsIl9hdXRoX3VzZXJfaWQiOiI0MSIsIl9hdXRoX3NpZ25faW5faWQiOjE4LCJfYXV0aF91c2VyX2hhc2giOiI0YzM4ZjgzNmU4YzE5MzIzYWQ1YzkzNzlkYzgxMmE5MzMwMTE5ZGI3In0=','2020-05-13 15:39:15.407972'),('1lg5h405dj3mlu5kr58dl35nyzv19hqo','OTI2ZDkwN2M2NWJkMmEwMzU1YjUxNzNlODYyY2U1YjVmNTU0ZGI5ZTp7Il9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfc2lnbl9pbl9pZCI6MjAsIl9zZXNzaW9uX2V4cGlyeSI6OTk5OTk5OTksImF1dGhfdG9rZW4iOiIxbGc1aDQwNWRqM21sdTVrcjU4ZGwzNW55enYxOWhxbyIsIl9hdXRoX3VzZXJfaGFzaCI6Ijg3ZWVlOTlkMzk0NWRiODE3YTRjYWJkMzQxYTU3NzJhYmQxNjQxMzYiLCJfYXV0aF9zaWduX2luX21hcHBpbmdfaWQiOjQ1LCJ1c2VyX2lkIjoyMCwiX2F1dGhfdXNlcl9pZCI6IjQ1IiwiX2F1dGhfc2lnbl9pbl9hdXRoIjpmYWxzZX0=','2020-06-10 19:24:51.101080'),('20voh9rx2sykejgo6fevhxpasv33g0m9','OGVmYWEzZDJhM2NiZTIyOTFmMjNjNTkzNmEyZTBhOGI3OWFiYjhjZjp7Il9hdXRoX3VzZXJfaGFzaCI6Ijg3ZWVlOTlkMzk0NWRiODE3YTRjYWJkMzQxYTU3NzJhYmQxNjQxMzYiLCJ1c2VyX2lkIjoyMCwiX2F1dGhfc2lnbl9pbl9tYXBwaW5nX2lkIjo0NSwiX2F1dGhfdXNlcl9pZCI6IjQ1IiwiX3Nlc3Npb25fZXhwaXJ5Ijo5OTk5OTk5OSwiX2F1dGhfc2lnbl9pbl9hdXRoIjpmYWxzZSwiX2F1dGhfc2lnbl9pbl9pZCI6MjAsIl9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiYXV0aF90b2tlbiI6IjIwdm9oOXJ4MnN5a2VqZ282ZmV2aHhwYXN2MzNnMG05In0=','2020-05-17 20:15:37.287857'),('26ir6qq6kk00fcyfcq66t5pb1sf67spi','ZGM2ZjAxMTg4M2MyNDkwZGI5OWViZGFkYzZkMDMwMGJlZjJhMTgwYzp7Il9hdXRoX3NpZ25faW5fYXV0aCI6ZmFsc2UsIl9hdXRoX3VzZXJfaWQiOiI0NiIsIl9hdXRoX3VzZXJfaGFzaCI6Ijk5Njg0ZWZmN2FiNWM4NjM5NzQ1ZTdmNzU2YWQ2MTcyZWNkNzYwYWYiLCJfYXV0aF9zaWduX2luX21hcHBpbmdfaWQiOjQ2LCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9hdXRoX3NpZ25faW5faWQiOjI1LCJhdXRoX3Rva2VuIjoiMjZpcjZxcTZrazAwZmN5ZmNxNjZ0NXBiMXNmNjdzcGkiLCJ1c2VyX2lkIjoyNSwiX3Nlc3Npb25fZXhwaXJ5Ijo5OTk5OTk5OX0=','2020-05-20 12:36:50.198370'),('28w6keo59bvn5wnzryj9mu4q5spx1o4l','ZGQ2NGMxY2NmYjdjYjJkZGEzZThjYjQzZjI4NWQ3OTk3YjEyM2YyYjp7Il9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfc2lnbl9pbl9hdXRoIjpmYWxzZSwidXNlcl9pZCI6MjAsIl9hdXRoX3VzZXJfaGFzaCI6Ijg3ZWVlOTlkMzk0NWRiODE3YTRjYWJkMzQxYTU3NzJhYmQxNjQxMzYiLCJhdXRoX3Rva2VuIjoiMjh3NmtlbzU5YnZuNXduenJ5ajltdTRxNXNweDFvNGwiLCJfYXV0aF9zaWduX2luX2lkIjoyMCwiX2F1dGhfdXNlcl9pZCI6IjQ1IiwiX2F1dGhfc2lnbl9pbl9tYXBwaW5nX2lkIjo0NSwiX3Nlc3Npb25fZXhwaXJ5Ijo5OTk5OTk5OX0=','2020-05-14 13:15:29.666787'),('29qwltiwytpjh7kjbapo3onmzbugmwhk','YzZjNTc3ZDVkZjZhNjJkZTgzM2JjOTA5ZTU1Mzc1ZmE2NjE0NTBmOTp7InVzZXJfaWQiOjE3LCJfc2Vzc2lvbl9leHBpcnkiOjk5OTk5OTk5LCJhdXRoX3Rva2VuIjoiMjlxd2x0aXd5dHBqaDdramJhcG8zb25temJ1Z213aGsiLCJfYXV0aF9zaWduX2luX2F1dGgiOmZhbHNlLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9hdXRoX3NpZ25faW5fbWFwcGluZ19pZCI6NDAsIl9hdXRoX3VzZXJfaWQiOiI0MCIsIl9hdXRoX3NpZ25faW5faWQiOjE3LCJfYXV0aF91c2VyX2hhc2giOiJmM2JhZjA5M2ExZDA4ZGZiZGIxZmUwM2RlNmQ1ODdjY2ZjYjRiNzQxIn0=','2020-05-13 16:24:33.866791'),('2mrnrbw31e1bxcwovz5ntigxnxltp7l3','NWExNTgxNWJmZWYxYTI5OGQ0YjczOTc1NTliMDJiYmI0MzZlNzRjNjp7Il9hdXRoX3VzZXJfaWQiOiI0NSIsIl9hdXRoX3NpZ25faW5fYXV0aCI6ZmFsc2UsIl9zZXNzaW9uX2V4cGlyeSI6OTk5OTk5OTksIl9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiYXV0aF90b2tlbiI6IjJtcm5yYnczMWUxYnhjd292ejVudGlneG54bHRwN2wzIiwidXNlcl9pZCI6MjAsIl9hdXRoX3VzZXJfaGFzaCI6Ijg3ZWVlOTlkMzk0NWRiODE3YTRjYWJkMzQxYTU3NzJhYmQxNjQxMzYiLCJfYXV0aF9zaWduX2luX21hcHBpbmdfaWQiOjQ1LCJfYXV0aF9zaWduX2luX2lkIjoyMH0=','2020-05-22 19:28:21.332803'),('3ewsobokys57ak31w442a36ksmvis9q0','YzYxODE0NDc4MGIwMWNmZTljMzYzMDdhMzY5YmQzYmVlMmUzOWI5Mjp7Il9hdXRoX3VzZXJfaGFzaCI6Ijg3ZWVlOTlkMzk0NWRiODE3YTRjYWJkMzQxYTU3NzJhYmQxNjQxMzYiLCJ1c2VyX2lkIjoyMCwiX2F1dGhfc2lnbl9pbl9tYXBwaW5nX2lkIjo0NSwiX2F1dGhfdXNlcl9pZCI6IjQ1IiwiX3Nlc3Npb25fZXhwaXJ5Ijo5OTk5OTk5OSwiX2F1dGhfc2lnbl9pbl9hdXRoIjpmYWxzZSwiX2F1dGhfc2lnbl9pbl9pZCI6MjAsIl9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiYXV0aF90b2tlbiI6IjNld3NvYm9reXM1N2FrMzF3NDQyYTM2a3NtdmlzOXEwIn0=','2020-05-17 17:50:54.453627'),('47xhetsqsytid2h8s47cltuj1jwmyevk','MGI4OGE4MDk1MGQ3ZDhjYTVjNzk0YzgxZmQ4NzgwNTJiMmU4MzZkYjp7Il9hdXRoX3NpZ25faW5faWQiOjIwLCJfc2Vzc2lvbl9leHBpcnkiOjk5OTk5OTk5LCJfYXV0aF91c2VyX2hhc2giOiI4N2VlZTk5ZDM5NDVkYjgxN2E0Y2FiZDM0MWE1NzcyYWJkMTY0MTM2IiwidXNlcl9pZCI6MjAsIl9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfc2lnbl9pbl9hdXRoIjpmYWxzZSwiX2F1dGhfc2lnbl9pbl9tYXBwaW5nX2lkIjo0NSwiYXV0aF90b2tlbiI6IjQ3eGhldHNxc3l0aWQyaDhzNDdjbHR1ajFqd215ZXZrIiwiX2F1dGhfdXNlcl9pZCI6IjQ1In0=','2020-05-22 19:02:49.956681'),('48bn4nvlpohhmcnjtbb6dhsftbtzln3v','MTRlY2I3MjAwOTFjZGI1YzZiMTVmMTQ5MzA5YmM2MjUwMmY4Yjg3YTp7Il9hdXRoX3VzZXJfaGFzaCI6ImYzYmFmMDkzYTFkMDhkZmJkYjFmZTAzZGU2ZDU4N2NjZmNiNGI3NDEiLCJfYXV0aF91c2VyX2lkIjoiNDAiLCJfYXV0aF9zaWduX2luX2lkIjoxNywiX2F1dGhfc2lnbl9pbl9tYXBwaW5nX2lkIjo0MCwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJfc2Vzc2lvbl9leHBpcnkiOjk5OTk5OTk5LCJ1c2VyX2lkIjoxNywiYXV0aF90b2tlbiI6IjQ4Ym40bnZscG9oaG1jbmp0YmI2ZGhzZnRidHpsbjN2IiwiX2F1dGhfc2lnbl9pbl9hdXRoIjpmYWxzZX0=','2020-05-13 13:49:52.975880'),('4cfz9k6e1m2iqqxa1sc93p1hukufh29a','ZmJiYWYzNmE5ODk5MmE2NWE3MzhmM2Y2NzkxYThiYWNkYzIzMGM5Zjp7Il9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfc2lnbl9pbl9hdXRoIjpmYWxzZSwidXNlcl9pZCI6MjAsIl9hdXRoX3VzZXJfaGFzaCI6Ijg3ZWVlOTlkMzk0NWRiODE3YTRjYWJkMzQxYTU3NzJhYmQxNjQxMzYiLCJhdXRoX3Rva2VuIjoiNGNmejlrNmUxbTJpcXF4YTFzYzkzcDFodWt1ZmgyOWEiLCJfYXV0aF9zaWduX2luX2lkIjoyMCwiX2F1dGhfdXNlcl9pZCI6IjQ1IiwiX2F1dGhfc2lnbl9pbl9tYXBwaW5nX2lkIjo0NSwiX3Nlc3Npb25fZXhwaXJ5Ijo5OTk5OTk5OX0=','2020-05-14 13:51:24.490257'),('4gxzzf0znsei8im619yi51xmrcmzb9jl','OTk5ZTJkZDhkYTQ0ZWYzYzI4MjI4Yzc0MTcwN2JlZDQ0NmUwN2VhYjp7Il9hdXRoX3VzZXJfaGFzaCI6IjRjMzhmODM2ZThjMTkzMjNhZDVjOTM3OWRjODEyYTkzMzAxMTlkYjciLCJfYXV0aF91c2VyX2lkIjoiNDEiLCJfYXV0aF9zaWduX2luX2lkIjoxOCwiX2F1dGhfc2lnbl9pbl9tYXBwaW5nX2lkIjo0MSwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJfc2Vzc2lvbl9leHBpcnkiOjk5OTk5OTk5LCJ1c2VyX2lkIjoxOCwiYXV0aF90b2tlbiI6IjRneHp6ZjB6bnNlaThpbTYxOXlpNTF4bXJjbXpiOWpsIiwiX2F1dGhfc2lnbl9pbl9hdXRoIjpmYWxzZX0=','2020-05-13 13:49:27.598880'),('560xc7327kpjxesmoicsosk7nzd4v8ir','NzllNDRlZmZmNDU5YzQxYjQwZDIwNjc5MWNhMzc5YTY1MjUxNTIwNTp7Il9hdXRoX3VzZXJfaGFzaCI6Ijg3ZWVlOTlkMzk0NWRiODE3YTRjYWJkMzQxYTU3NzJhYmQxNjQxMzYiLCJ1c2VyX2lkIjoyMCwiX2F1dGhfc2lnbl9pbl9tYXBwaW5nX2lkIjo0NSwiX2F1dGhfdXNlcl9pZCI6IjQ1IiwiX3Nlc3Npb25fZXhwaXJ5Ijo5OTk5OTk5OSwiX2F1dGhfc2lnbl9pbl9hdXRoIjpmYWxzZSwiX2F1dGhfc2lnbl9pbl9pZCI6MjAsIl9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiYXV0aF90b2tlbiI6IjU2MHhjNzMyN2twanhlc21vaWNzb3NrN256ZDR2OGlyIn0=','2020-05-17 18:31:46.361826'),('5ijh3k2bdpcxf80dcnwr53k04emaz4gw','YzIwMzU0MTlkNGVjNWMxNmY2ZWFkMDU0ODBkYjZhOWE4YWNkZjIyNzp7Il9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfc2lnbl9pbl9hdXRoIjpmYWxzZSwidXNlcl9pZCI6MjAsIl9hdXRoX3VzZXJfaGFzaCI6Ijg3ZWVlOTlkMzk0NWRiODE3YTRjYWJkMzQxYTU3NzJhYmQxNjQxMzYiLCJhdXRoX3Rva2VuIjoiNWlqaDNrMmJkcGN4ZjgwZGNud3I1M2swNGVtYXo0Z3ciLCJfYXV0aF9zaWduX2luX2lkIjoyMCwiX2F1dGhfdXNlcl9pZCI6IjQ1IiwiX2F1dGhfc2lnbl9pbl9tYXBwaW5nX2lkIjo0NSwiX3Nlc3Npb25fZXhwaXJ5Ijo5OTk5OTk5OX0=','2020-05-14 15:34:36.615450'),('5kb7hcbsu50tle7y2qixndzdngw5kg78','ZmNlYmM3YzA1OWFiYzczMTVkYWE5ZDgyYjEzMDlmMjFmMTcwOGM2Yzp7InVzZXJfaWQiOjIwLCJfYXV0aF91c2VyX2lkIjoiNDUiLCJfc2Vzc2lvbl9leHBpcnkiOjk5OTk5OTk5LCJfYXV0aF9zaWduX2luX21hcHBpbmdfaWQiOjQ1LCJfYXV0aF9zaWduX2luX2lkIjoyMCwiX2F1dGhfc2lnbl9pbl9hdXRoIjpmYWxzZSwiX2F1dGhfdXNlcl9oYXNoIjoiODdlZWU5OWQzOTQ1ZGI4MTdhNGNhYmQzNDFhNTc3MmFiZDE2NDEzNiIsImF1dGhfdG9rZW4iOiI1a2I3aGNic3U1MHRsZTd5MnFpeG5kemRuZ3c1a2c3OCIsIl9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIn0=','2020-05-15 16:55:16.024656'),('5mbmnu36cnc8s8wphh9ucfk42kxbh3xc','M2EzMDY5ZTc0ZjFmYzNmMjcwZDc0N2ExMTMyZjlhN2I2MTkyNzdkZDp7Il9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfc2lnbl9pbl9hdXRoIjpmYWxzZSwidXNlcl9pZCI6MjAsIl9hdXRoX3VzZXJfaGFzaCI6Ijg3ZWVlOTlkMzk0NWRiODE3YTRjYWJkMzQxYTU3NzJhYmQxNjQxMzYiLCJhdXRoX3Rva2VuIjoiNW1ibW51MzZjbmM4czh3cGhoOXVjZms0Mmt4YmgzeGMiLCJfYXV0aF9zaWduX2luX2lkIjoyMCwiX2F1dGhfdXNlcl9pZCI6IjQ1IiwiX2F1dGhfc2lnbl9pbl9tYXBwaW5nX2lkIjo0NSwiX3Nlc3Npb25fZXhwaXJ5Ijo5OTk5OTk5OX0=','2020-05-14 13:12:19.334756'),('5ruitbm5ukic0tx28o8fzlxqbyamrfxd','ZGEyODU1MTNkMzZjNTQ4N2U3NGE2NmIyZmU4MTM0YmQzYjcyY2E2OTp7Il9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfc2lnbl9pbl9hdXRoIjpmYWxzZSwidXNlcl9pZCI6MjAsIl9hdXRoX3VzZXJfaGFzaCI6Ijg3ZWVlOTlkMzk0NWRiODE3YTRjYWJkMzQxYTU3NzJhYmQxNjQxMzYiLCJhdXRoX3Rva2VuIjoiNXJ1aXRibTV1a2ljMHR4MjhvOGZ6bHhxYnlhbXJmeGQiLCJfYXV0aF9zaWduX2luX2lkIjoyMCwiX2F1dGhfdXNlcl9pZCI6IjQ1IiwiX2F1dGhfc2lnbl9pbl9tYXBwaW5nX2lkIjo0NSwiX3Nlc3Npb25fZXhwaXJ5Ijo5OTk5OTk5OX0=','2020-05-14 12:49:38.306666'),('5wm6ycjan7kyw1ci2idm7uqkj00dsji7','NzA5ZTczYzY4N2M3MzdjZjIxNDlmOTA0ZDc5NTA4MTkwNGZiMWUwYzp7Il9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9oYXNoIjoiODdlZWU5OWQzOTQ1ZGI4MTdhNGNhYmQzNDFhNTc3MmFiZDE2NDEzNiIsImF1dGhfdG9rZW4iOiI1d202eWNqYW43a3l3MWNpMmlkbTd1cWtqMDBkc2ppNyIsIl9hdXRoX3VzZXJfaWQiOiI0NSIsIl9zZXNzaW9uX2V4cGlyeSI6OTk5OTk5OTksInVzZXJfaWQiOjIwLCJfYXV0aF9zaWduX2luX2lkIjoyMCwiX2F1dGhfc2lnbl9pbl9hdXRoIjpmYWxzZSwiX2F1dGhfc2lnbl9pbl9tYXBwaW5nX2lkIjo0NX0=','2020-05-29 20:15:19.553537'),('67682md5z2rbbr9cjipu9p9q8uuf4mb4','OTVkZTY4OTY0YmY0ZmQ1ZGYwNDRlNjk0ZGEwMGFhZjFjYWQ3Yzk5Yzp7Il9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9oYXNoIjoiZjNiYWYwOTNhMWQwOGRmYmRiMWZlMDNkZTZkNTg3Y2NmY2I0Yjc0MSIsImF1dGhfdG9rZW4iOiI2NzY4Mm1kNXoycmJicjljamlwdTlwOXE4dXVmNG1iNCIsIl9hdXRoX3VzZXJfaWQiOiI0MCIsIl9zZXNzaW9uX2V4cGlyeSI6OTk5OTk5OTksInVzZXJfaWQiOjE3LCJfYXV0aF9zaWduX2luX2lkIjoxNywiX2F1dGhfc2lnbl9pbl9hdXRoIjpmYWxzZSwiX2F1dGhfc2lnbl9pbl9tYXBwaW5nX2lkIjo0MH0=','2020-05-29 19:03:43.458951'),('68sdxof7xq8fzsemd2p16qf8fps7mgds','OWI4ODZhNDMxZGE3ZTU2YzhlZjUyZTlmYjViOWY0NTdkYjFkN2Y4Mzp7Il9hdXRoX3NpZ25faW5fYXV0aCI6ZmFsc2UsInVzZXJfaWQiOjIwLCJfYXV0aF9zaWduX2luX2lkIjoyMCwiX3Nlc3Npb25fZXhwaXJ5Ijo5OTk5OTk5OSwiX2F1dGhfdXNlcl9pZCI6IjQ1IiwiX2F1dGhfdXNlcl9oYXNoIjoiODdlZWU5OWQzOTQ1ZGI4MTdhNGNhYmQzNDFhNTc3MmFiZDE2NDEzNiIsIl9hdXRoX3NpZ25faW5fbWFwcGluZ19pZCI6NDUsIl9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiYXV0aF90b2tlbiI6IjY4c2R4b2Y3eHE4ZnpzZW1kMnAxNnFmOGZwczdtZ2RzIn0=','2020-06-03 16:07:00.180096'),('6az3l9filg2kx0eil1vqbl2trda4yf49','NGJkNDYwZTE4OGY2YWYwYzM0MmViY2I5ZmFhYzc0MjQyZjJlNzc4MDp7Il9zZXNzaW9uX2V4cGlyeSI6OTk5OTk5OTksIl9hdXRoX3NpZ25faW5fbWFwcGluZ19pZCI6NDAsIl9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfc2lnbl9pbl9hdXRoIjpmYWxzZSwiX2F1dGhfdXNlcl9pZCI6IjQwIiwidXNlcl9pZCI6MTcsImF1dGhfdG9rZW4iOiI2YXozbDlmaWxnMmt4MGVpbDF2cWJsMnRyZGE0eWY0OSIsIl9hdXRoX3NpZ25faW5faWQiOjE3LCJfYXV0aF91c2VyX2hhc2giOiJmM2JhZjA5M2ExZDA4ZGZiZGIxZmUwM2RlNmQ1ODdjY2ZjYjRiNzQxIn0=','2020-05-13 19:58:07.458305'),('6fkfa0wa7x27ah7er8qoqlug56hrkg1k','MzhiMTcxN2FiZjlmNjgyYTUxN2IyNzM2NGVlNDlkOGUwNjY5MTRmYjp7Il9hdXRoX3VzZXJfaGFzaCI6Ijg3ZWVlOTlkMzk0NWRiODE3YTRjYWJkMzQxYTU3NzJhYmQxNjQxMzYiLCJ1c2VyX2lkIjoyMCwiX2F1dGhfc2lnbl9pbl9tYXBwaW5nX2lkIjo0NSwiX2F1dGhfdXNlcl9pZCI6IjQ1IiwiX3Nlc3Npb25fZXhwaXJ5Ijo5OTk5OTk5OSwiX2F1dGhfc2lnbl9pbl9hdXRoIjpmYWxzZSwiX2F1dGhfc2lnbl9pbl9pZCI6MjAsIl9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiYXV0aF90b2tlbiI6IjZma2ZhMHdhN3gyN2FoN2VyOHFvcWx1ZzU2aHJrZzFrIn0=','2020-05-17 17:59:09.358113'),('6prxut1phz8bernanoiozh8egha5pkwe','ZTYzZDhkZDc3YjcxYWE1ZmNjYzBiZDA3YmE3YTY3YWMwMzg0ZmM5ZDp7Il9hdXRoX3VzZXJfaGFzaCI6IjRjMzhmODM2ZThjMTkzMjNhZDVjOTM3OWRjODEyYTkzMzAxMTlkYjciLCJfYXV0aF9zaWduX2luX2lkIjoxOCwidXNlcl9pZCI6MTgsIl9hdXRoX3NpZ25faW5fYXV0aCI6ZmFsc2UsIl9hdXRoX3VzZXJfaWQiOiI0MSIsIl9hdXRoX3NpZ25faW5fbWFwcGluZ19pZCI6NDEsIl9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiYXV0aF90b2tlbiI6IjZwcnh1dDFwaHo4YmVybmFub2lvemg4ZWdoYTVwa3dlIiwiX3Nlc3Npb25fZXhwaXJ5Ijo5OTk5OTk5OX0=','2020-05-13 14:07:33.033752'),('6rfwzblv1kgcwrtfdo9bm9xqv4ej5um2','MmZhOWQ1NjFmYmEzMjZjMjJlZjllNmJjNDFhNWY1NTk3OGU2YzViYjp7Il9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfc2lnbl9pbl9hdXRoIjpmYWxzZSwidXNlcl9pZCI6MjAsIl9hdXRoX3VzZXJfaGFzaCI6Ijg3ZWVlOTlkMzk0NWRiODE3YTRjYWJkMzQxYTU3NzJhYmQxNjQxMzYiLCJhdXRoX3Rva2VuIjoiNnJmd3pibHYxa2djd3J0ZmRvOWJtOXhxdjRlajV1bTIiLCJfYXV0aF9zaWduX2luX2lkIjoyMCwiX2F1dGhfdXNlcl9pZCI6IjQ1IiwiX2F1dGhfc2lnbl9pbl9tYXBwaW5nX2lkIjo0NSwiX3Nlc3Npb25fZXhwaXJ5Ijo5OTk5OTk5OX0=','2020-05-14 12:26:14.065233'),('6y2w7rxzm2soq5p7rznwmjystwzcnbj6','NTQ3ODJlOWUzZjdkNWNjNzFjNTYwMWY2ODc5MGY0OWIyN2VmMDFhMzp7Il9hdXRoX3VzZXJfaGFzaCI6IjY0YjdkOGNjY2YyMmYyOGUxNzM2NDZmZmI1YmI2ZjViNzU5YzgwZDYiLCJfYXV0aF91c2VyX2lkIjoiMzMiLCJfYXV0aF9zaWduX2luX2lkIjoxMywiX2F1dGhfc2lnbl9pbl9tYXBwaW5nX2lkIjozMywiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJfc2Vzc2lvbl9leHBpcnkiOjk5OTk5OTk5LCJ1c2VyX2lkIjoxMywiYXV0aF90b2tlbiI6IjZ5Mnc3cnh6bTJzb3E1cDdyem53bWp5c3R3emNuYmo2IiwiX2F1dGhfc2lnbl9pbl9hdXRoIjpmYWxzZX0=','2020-05-13 13:41:53.673880'),('6zbuyld95l9ra4msbfadr9cn8tix29z7','MDU0NDM5ZWJiMjhlMTM2MTdlZDY2ZGQ0MGUwOTNkOThkZmYzZTVmMzp7Il9hdXRoX3VzZXJfaGFzaCI6Ijg3ZWVlOTlkMzk0NWRiODE3YTRjYWJkMzQxYTU3NzJhYmQxNjQxMzYiLCJ1c2VyX2lkIjoyMCwiX2F1dGhfc2lnbl9pbl9tYXBwaW5nX2lkIjo0NSwiX2F1dGhfdXNlcl9pZCI6IjQ1IiwiX3Nlc3Npb25fZXhwaXJ5Ijo5OTk5OTk5OSwiX2F1dGhfc2lnbl9pbl9hdXRoIjpmYWxzZSwiX2F1dGhfc2lnbl9pbl9pZCI6MjAsIl9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiYXV0aF90b2tlbiI6IjZ6YnV5bGQ5NWw5cmE0bXNiZmFkcjljbjh0aXgyOXo3In0=','2020-05-17 17:55:21.897369'),('79eg2f4cyrv26l2d51owvn7x7g6xhi3a','YjcxM2QyM2ZhMTE5NjA0MDQwMGQ3MDlhZmRhZTVhY2MxNDI2YWIzZDp7Il9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfc2lnbl9pbl9hdXRoIjpmYWxzZSwidXNlcl9pZCI6MjAsIl9hdXRoX3VzZXJfaGFzaCI6Ijg3ZWVlOTlkMzk0NWRiODE3YTRjYWJkMzQxYTU3NzJhYmQxNjQxMzYiLCJhdXRoX3Rva2VuIjoiNzllZzJmNGN5cnYyNmwyZDUxb3d2bjd4N2c2eGhpM2EiLCJfYXV0aF9zaWduX2luX2lkIjoyMCwiX2F1dGhfdXNlcl9pZCI6IjQ1IiwiX2F1dGhfc2lnbl9pbl9tYXBwaW5nX2lkIjo0NSwiX3Nlc3Npb25fZXhwaXJ5Ijo5OTk5OTk5OX0=','2020-05-14 13:54:42.260032'),('7am17snm1b4e5bmhlendvkvc819iho90','ZmY0MGU4OThlMjA1MjU5ZmY5OGU5MjJlNDFiOWRjMmRjNzM2OTYxYjp7Il9hdXRoX3VzZXJfaGFzaCI6Ijg3ZWVlOTlkMzk0NWRiODE3YTRjYWJkMzQxYTU3NzJhYmQxNjQxMzYiLCJ1c2VyX2lkIjoyMCwiX2F1dGhfc2lnbl9pbl9tYXBwaW5nX2lkIjo0NSwiX2F1dGhfdXNlcl9pZCI6IjQ1IiwiX3Nlc3Npb25fZXhwaXJ5Ijo5OTk5OTk5OSwiX2F1dGhfc2lnbl9pbl9hdXRoIjpmYWxzZSwiX2F1dGhfc2lnbl9pbl9pZCI6MjAsIl9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiYXV0aF90b2tlbiI6IjdhbTE3c25tMWI0ZTVibWhsZW5kdmt2YzgxOWlobzkwIn0=','2020-05-17 17:28:30.507246'),('7dbe1s36lr4l4ych71bdclnosenidlir','ZTAzZDRmZTkxZTY1NzhiNGE0NmYzYjIzNzg1ZjlkMWMwNWYzZmM0Mjp7Il9hdXRoX3NpZ25faW5faWQiOjE4LCJfc2Vzc2lvbl9leHBpcnkiOjk5OTk5OTk5LCJfYXV0aF91c2VyX2lkIjoiNDEiLCJfYXV0aF91c2VyX2hhc2giOiI0YzM4ZjgzNmU4YzE5MzIzYWQ1YzkzNzlkYzgxMmE5MzMwMTE5ZGI3IiwidXNlcl9pZCI6MTgsIl9hdXRoX3NpZ25faW5fbWFwcGluZ19pZCI6NDEsIl9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiYXV0aF90b2tlbiI6IjdkYmUxczM2bHI0bDR5Y2g3MWJkY2xub3NlbmlkbGlyIiwiX2F1dGhfc2lnbl9pbl9hdXRoIjpmYWxzZX0=','2020-05-13 15:30:48.684396'),('7jfgsaf8nhyiklda97onj746yqk3u0eb','YjFiMDJlY2Q5NWJjYTQyNWE0ZTY5YjhhYTgzN2MwYjZhYzAzNTJlODp7Il9hdXRoX3VzZXJfaWQiOiI0MCIsIl9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfc2lnbl9pbl9hdXRoIjpmYWxzZSwiX2F1dGhfc2lnbl9pbl9pZCI6MTcsIl9hdXRoX3VzZXJfaGFzaCI6ImYzYmFmMDkzYTFkMDhkZmJkYjFmZTAzZGU2ZDU4N2NjZmNiNGI3NDEiLCJhdXRoX3Rva2VuIjoiN2pmZ3NhZjhuaHlpa2xkYTk3b25qNzQ2eXFrM3UwZWIiLCJfc2Vzc2lvbl9leHBpcnkiOjk5OTk5OTk5LCJfYXV0aF9zaWduX2luX21hcHBpbmdfaWQiOjQwLCJ1c2VyX2lkIjoxN30=','2020-05-24 19:59:41.461864'),('7nl0jq9ha8xshgqb228z84tuo3505egd','OGJmN2Q1NjVlMWYzOWFiMDE1MDZkZWNmNWU5Mjc5ODNkMzMzMWZhZTp7Il9zZXNzaW9uX2V4cGlyeSI6OTk5OTk5OTksIl9hdXRoX3VzZXJfaGFzaCI6Ijg3ZWVlOTlkMzk0NWRiODE3YTRjYWJkMzQxYTU3NzJhYmQxNjQxMzYiLCJfYXV0aF9zaWduX2luX21hcHBpbmdfaWQiOjQ1LCJfYXV0aF91c2VyX2lkIjoiNDUiLCJfYXV0aF9zaWduX2luX2lkIjoyMCwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJfYXV0aF9zaWduX2luX2F1dGgiOmZhbHNlLCJhdXRoX3Rva2VuIjoiN25sMGpxOWhhOHhzaGdxYjIyOHo4NHR1bzM1MDVlZ2QiLCJ1c2VyX2lkIjoyMH0=','2020-05-15 16:14:05.445613'),('7rcatyqv0jw66bj34y9r7zzcmibl0vym','ZjAyYzJlYTE3ODA1MWRmNzM5ZmY4MzI4MjdkNDczZDIzYTdhNDdlZjp7InVzZXJfaWQiOjE3LCJfc2Vzc2lvbl9leHBpcnkiOjk5OTk5OTk5LCJhdXRoX3Rva2VuIjoiN3JjYXR5cXYwanc2NmJqMzR5OXI3enpjbWlibDB2eW0iLCJfYXV0aF9zaWduX2luX2F1dGgiOmZhbHNlLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9hdXRoX3NpZ25faW5fbWFwcGluZ19pZCI6NDAsIl9hdXRoX3VzZXJfaWQiOiI0MCIsIl9hdXRoX3NpZ25faW5faWQiOjE3LCJfYXV0aF91c2VyX2hhc2giOiJmM2JhZjA5M2ExZDA4ZGZiZGIxZmUwM2RlNmQ1ODdjY2ZjYjRiNzQxIn0=','2020-05-13 16:15:02.122622'),('7rj2fcfftdzjlmxhjj7neuw5ihdgjw89','YWNiNjRhYmFmMTY2OTc1Y2M1MDQ0MWI4M2FlZTRhMzA2NWY0OTFmOTp7Il9hdXRoX3NpZ25faW5faWQiOjIwLCJfYXV0aF9zaWduX2luX2F1dGgiOmZhbHNlLCJ1c2VyX2lkIjoyMCwiX2F1dGhfdXNlcl9pZCI6IjQ1IiwiX2F1dGhfdXNlcl9oYXNoIjoiODdlZWU5OWQzOTQ1ZGI4MTdhNGNhYmQzNDFhNTc3MmFiZDE2NDEzNiIsIl9hdXRoX3NpZ25faW5fbWFwcGluZ19pZCI6NDUsIl9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiYXV0aF90b2tlbiI6IjdyajJmY2ZmdGR6amxteGhqajduZXV3NWloZGdqdzg5IiwiX3Nlc3Npb25fZXhwaXJ5Ijo5OTk5OTk5OX0=','2020-05-14 11:33:26.803522'),('7zwg4wk24g0m6e7870szbffyhywo0e0y','YzMwN2IxY2IxNGJlNjA5ZTliODVlYzA1NDJiMGI1MjQ4MjM0MDJiMDp7Il9zZXNzaW9uX2V4cGlyeSI6OTk5OTk5OTksIl9hdXRoX3VzZXJfaGFzaCI6Ijg3ZWVlOTlkMzk0NWRiODE3YTRjYWJkMzQxYTU3NzJhYmQxNjQxMzYiLCJfYXV0aF9zaWduX2luX21hcHBpbmdfaWQiOjQ1LCJfYXV0aF91c2VyX2lkIjoiNDUiLCJfYXV0aF9zaWduX2luX2lkIjoyMCwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJfYXV0aF9zaWduX2luX2F1dGgiOmZhbHNlLCJhdXRoX3Rva2VuIjoiN3p3ZzR3azI0ZzBtNmU3ODcwc3piZmZ5aHl3bzBlMHkiLCJ1c2VyX2lkIjoyMH0=','2020-05-15 13:24:01.251612'),('815zemq0gf6ud3kun06iop6zwcrzelv2','YWYzNzBhNzc0NzFkM2QwNjg5MTc5MDRhYjc5YmQ2YzdhNjUxNzdhODp7Il9hdXRoX3NpZ25faW5fYXV0aCI6ZmFsc2UsIl9zZXNzaW9uX2V4cGlyeSI6OTk5OTk5OTksIl9hdXRoX3VzZXJfaGFzaCI6ImYzYmFmMDkzYTFkMDhkZmJkYjFmZTAzZGU2ZDU4N2NjZmNiNGI3NDEiLCJhdXRoX3Rva2VuIjoiODE1emVtcTBnZjZ1ZDNrdW4wNmlvcDZ6d2NyemVsdjIiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9hdXRoX3NpZ25faW5faWQiOjE3LCJfYXV0aF91c2VyX2lkIjoiNDAiLCJ1c2VyX2lkIjoxNywiX2F1dGhfc2lnbl9pbl9tYXBwaW5nX2lkIjo0MH0=','2020-05-22 13:22:16.566729'),('88vmi4d0vd93mm21xqtjdkop276017kq','ZTA2ZWJjZTU4NDIyZDY3YzllZGEwZWVmYzJhOWNjZjMxYjc4MjhiMDp7Il9zZXNzaW9uX2V4cGlyeSI6OTk5OTk5OTksIl9hdXRoX3NpZ25faW5faWQiOjE3LCJ1c2VyX2lkIjoxNywiX2F1dGhfc2lnbl9pbl9hdXRoIjpmYWxzZSwiX2F1dGhfc2lnbl9pbl9tYXBwaW5nX2lkIjo0MCwiX2F1dGhfdXNlcl9pZCI6IjQwIiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJhdXRoX3Rva2VuIjoiODh2bWk0ZDB2ZDkzbW0yMXhxdGpka29wMjc2MDE3a3EiLCJfYXV0aF91c2VyX2hhc2giOiJmM2JhZjA5M2ExZDA4ZGZiZGIxZmUwM2RlNmQ1ODdjY2ZjYjRiNzQxIn0=','2020-06-11 13:44:12.433056'),('8lqzftrgpsv4yavadv0ii02ky17kt2my','ZWJhZjQ0YzNhNTc2ZWQ0NTg0YTVkMTVjNjI3YTlkMzYzMzk2NzcxNTp7Il9hdXRoX3NpZ25faW5fbWFwcGluZ19pZCI6NDUsImF1dGhfdG9rZW4iOiI4bHF6ZnRyZ3BzdjR5YXZhZHYwaWkwMmt5MTdrdDJteSIsIl9hdXRoX3VzZXJfaGFzaCI6Ijg3ZWVlOTlkMzk0NWRiODE3YTRjYWJkMzQxYTU3NzJhYmQxNjQxMzYiLCJfc2Vzc2lvbl9leHBpcnkiOjk5OTk5OTk5LCJ1c2VyX2lkIjoyMCwiX2F1dGhfc2lnbl9pbl9pZCI6MjAsIl9hdXRoX3NpZ25faW5fYXV0aCI6ZmFsc2UsIl9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9pZCI6IjQ1In0=','2020-05-27 20:00:52.574912'),('8w3lhqklyae4wndf6q403u9wzm3pjz9y','YTFlZjhiYzc2NDhhMTM3ZTY5NzllMWRmM2Q1ODUzZjkzYWI1ZDRjOTp7Il9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9oYXNoIjoiZjNiYWYwOTNhMWQwOGRmYmRiMWZlMDNkZTZkNTg3Y2NmY2I0Yjc0MSIsImF1dGhfdG9rZW4iOiI4dzNsaHFrbHlhZTR3bmRmNnE0MDN1OXd6bTNwano5eSIsIl9hdXRoX3VzZXJfaWQiOiI0MCIsIl9zZXNzaW9uX2V4cGlyeSI6OTk5OTk5OTksInVzZXJfaWQiOjE3LCJfYXV0aF9zaWduX2luX2lkIjoxNywiX2F1dGhfc2lnbl9pbl9hdXRoIjpmYWxzZSwiX2F1dGhfc2lnbl9pbl9tYXBwaW5nX2lkIjo0MH0=','2020-05-29 19:10:59.093511'),('957u7nvkz40j1peylc5s9wud6vqhsyxk','YzUwNzY4MjU5YTQ3ZDM2ZTdhYjEzOWU0OTdlZGYyMjk2OGJkOGI4YTp7ImF1dGhfdG9rZW4iOiI5NTd1N252a3o0MGoxcGV5bGM1czl3dWQ2dnFoc3l4ayIsIl9hdXRoX3NpZ25faW5fYXV0aCI6ZmFsc2UsInVzZXJfaWQiOjIwLCJfYXV0aF91c2VyX2hhc2giOiI4N2VlZTk5ZDM5NDVkYjgxN2E0Y2FiZDM0MWE1NzcyYWJkMTY0MTM2IiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJfYXV0aF9zaWduX2luX2lkIjoyMCwiX2F1dGhfdXNlcl9pZCI6IjQ1IiwiX2F1dGhfc2lnbl9pbl9tYXBwaW5nX2lkIjo0NSwiX3Nlc3Npb25fZXhwaXJ5Ijo5OTk5OTk5OX0=','2020-05-14 12:37:43.090720'),('9b9hmf4lak0auh0xkdfcs2qof650kin8','NjliZmJhYWE5NzJmYzI2MzA2MzUzMTY0OGUyNjYwMjc1YzVkNjIxMTp7Il9hdXRoX3NpZ25faW5fYXV0aCI6ZmFsc2UsInVzZXJfaWQiOjE3LCJhdXRoX3Rva2VuIjoiOWI5aG1mNGxhazBhdWgweGtkZmNzMnFvZjY1MGtpbjgiLCJfYXV0aF9zaWduX2luX21hcHBpbmdfaWQiOjQwLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9hdXRoX3VzZXJfaWQiOiI0MCIsIl9hdXRoX3NpZ25faW5faWQiOjE3LCJfYXV0aF91c2VyX2hhc2giOiJmM2JhZjA5M2ExZDA4ZGZiZGIxZmUwM2RlNmQ1ODdjY2ZjYjRiNzQxIiwiX3Nlc3Npb25fZXhwaXJ5Ijo5OTk5OTk5OX0=','2020-06-13 20:23:49.746434'),('9tjixx5s5kprhkfgvmf5vk5jq4tqp1od','YzhiZTFmOWNhOTAyNDNlYmJhMzg2NzY4MTRmMmJjMGM2YjYzM2ZkYjp7Il9hdXRoX3NpZ25faW5faWQiOjIwLCJfYXV0aF9zaWduX2luX21hcHBpbmdfaWQiOjQ1LCJfYXV0aF91c2VyX2hhc2giOiI4N2VlZTk5ZDM5NDVkYjgxN2E0Y2FiZDM0MWE1NzcyYWJkMTY0MTM2IiwiX2F1dGhfc2lnbl9pbl9hdXRoIjpmYWxzZSwiYXV0aF90b2tlbiI6Ijl0aml4eDVzNWtwcmhrZmd2bWY1dms1anE0dHFwMW9kIiwidXNlcl9pZCI6MjAsIl9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX3Nlc3Npb25fZXhwaXJ5Ijo5OTk5OTk5OSwiX2F1dGhfdXNlcl9pZCI6IjQ1In0=','2020-06-18 19:48:36.041403'),('9vvmc22xaefes82pi8l80k19m2v39xbx','ODllOGQ3NjY3ZDRjY2I5NzU5NWMxM2I5YzM0MGUxY2YxM2Q4ZmEwNjp7Il9hdXRoX3NpZ25faW5fbWFwcGluZ19pZCI6NDUsIl9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwidXNlcl9pZCI6MjAsIl9hdXRoX3VzZXJfaWQiOiI0NSIsIl9hdXRoX3VzZXJfaGFzaCI6Ijg3ZWVlOTlkMzk0NWRiODE3YTRjYWJkMzQxYTU3NzJhYmQxNjQxMzYiLCJfYXV0aF9zaWduX2luX2lkIjoyMCwiX2F1dGhfc2lnbl9pbl9hdXRoIjpmYWxzZSwiYXV0aF90b2tlbiI6Ijl2dm1jMjJ4YWVmZXM4MnBpOGw4MGsxOW0ydjM5eGJ4IiwiX3Nlc3Npb25fZXhwaXJ5Ijo5OTk5OTk5OX0=','2020-05-17 16:51:34.330650'),('9w8dz041u0a0s8garz7nmfjtaf96hob4','ZTk1ODdlMmEyMmI4ZWE1NGM1NjM0YTQ0YzdhMWEyNjBhZTk1Y2MyMjp7Il9hdXRoX3NpZ25faW5faWQiOjIwLCJfYXV0aF9zaWduX2luX2F1dGgiOmZhbHNlLCJ1c2VyX2lkIjoyMCwiX2F1dGhfdXNlcl9pZCI6IjQ1IiwiX2F1dGhfdXNlcl9oYXNoIjoiODdlZWU5OWQzOTQ1ZGI4MTdhNGNhYmQzNDFhNTc3MmFiZDE2NDEzNiIsIl9hdXRoX3NpZ25faW5fbWFwcGluZ19pZCI6NDUsIl9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiYXV0aF90b2tlbiI6Ijl3OGR6MDQxdTBhMHM4Z2FyejdubWZqdGFmOTZob2I0IiwiX3Nlc3Npb25fZXhwaXJ5Ijo5OTk5OTk5OX0=','2020-05-14 11:53:11.133527'),('9zcu80ptshky428imiq4qjgw3rhzvsbt','MDBiM2FhMTBiOWUzZTY2NWE4NWE2ZjYwYzU1YTMwYjhiYTNiMWE5YTp7Il9hdXRoX3VzZXJfaWQiOiI0NSIsIl9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfc2lnbl9pbl9hdXRoIjpmYWxzZSwiX2F1dGhfc2lnbl9pbl9pZCI6MjAsIl9hdXRoX3VzZXJfaGFzaCI6Ijg3ZWVlOTlkMzk0NWRiODE3YTRjYWJkMzQxYTU3NzJhYmQxNjQxMzYiLCJhdXRoX3Rva2VuIjoiOXpjdTgwcHRzaGt5NDI4aW1pcTRxamd3M3JoenZzYnQiLCJfc2Vzc2lvbl9leHBpcnkiOjk5OTk5OTk5LCJfYXV0aF9zaWduX2luX21hcHBpbmdfaWQiOjQ1LCJ1c2VyX2lkIjoyMH0=','2020-05-24 20:10:39.098621'),('9zwe0c0yabdkgr4kqifcik35wj1xr6sz','ODJiNWU4YTRmOTE2NDhkYmQwOGExZjgxNWU1NjdkNmJjZDBmZWMxNDp7Il9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9pZCI6IjQwIiwiX2F1dGhfdXNlcl9oYXNoIjoiZjNiYWYwOTNhMWQwOGRmYmRiMWZlMDNkZTZkNTg3Y2NmY2I0Yjc0MSIsIl9zZXNzaW9uX2V4cGlyeSI6OTk5OTk5OTksIl9hdXRoX3NpZ25faW5faWQiOjE3LCJfYXV0aF9zaWduX2luX21hcHBpbmdfaWQiOjQwLCJ1c2VyX2lkIjoxNywiX2F1dGhfc2lnbl9pbl9hdXRoIjpmYWxzZSwiYXV0aF90b2tlbiI6Ijl6d2UwYzB5YWJka2dyNGtxaWZjaWszNXdqMXhyNnN6In0=','2020-05-30 18:33:20.973630'),('a3a7b4wj6xhtnzzasa3vuqez8p35wzx1','ZTQ5MjUxNGVmNTM0N2QyNjQzNDYyNzQ3ZTY0MTE5YjlhMDc0YTBmYzp7ImF1dGhfdG9rZW4iOiJhM2E3YjR3ajZ4aHRuenphc2EzdnVxZXo4cDM1d3p4MSIsIl9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX3Nlc3Npb25fZXhwaXJ5Ijo5OTk5OTk5OSwiX2F1dGhfdXNlcl9pZCI6IjQ1IiwiX2F1dGhfc2lnbl9pbl9pZCI6MjAsIl9hdXRoX3NpZ25faW5fYXV0aCI6ZmFsc2UsIl9hdXRoX3NpZ25faW5fbWFwcGluZ19pZCI6NDUsInVzZXJfaWQiOjIwLCJfYXV0aF91c2VyX2hhc2giOiI4N2VlZTk5ZDM5NDVkYjgxN2E0Y2FiZDM0MWE1NzcyYWJkMTY0MTM2In0=','2020-06-10 12:57:46.524750'),('aems7b3fwy5iwtmqvihqyg2t8at2oc6f','M2YxNGNhZGMzODAwZTgyOWU0ODRlYjJhODM0MTk1ZWU4ZTRkMjA5Yzp7Il9hdXRoX3VzZXJfaGFzaCI6Ijg3ZWVlOTlkMzk0NWRiODE3YTRjYWJkMzQxYTU3NzJhYmQxNjQxMzYiLCJfc2Vzc2lvbl9leHBpcnkiOjk5OTk5OTk5LCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsInVzZXJfaWQiOjIwLCJhdXRoX3Rva2VuIjoiYWVtczdiM2Z3eTVpd3RtcXZpaHF5ZzJ0OGF0Mm9jNmYiLCJfYXV0aF9zaWduX2luX2F1dGgiOmZhbHNlLCJfYXV0aF91c2VyX2lkIjoiNDUiLCJfYXV0aF9zaWduX2luX2lkIjoyMCwiX2F1dGhfc2lnbl9pbl9tYXBwaW5nX2lkIjo0NX0=','2020-05-31 12:24:38.773273'),('afobml3h1zqvwsai3b5uruxv4t1dwnjj','NzBkZTJkNGFlMDMzN2RhOWM2YzVjZDVlMzRhYzQxMDI1MGEwODJmNzp7ImF1dGhfdG9rZW4iOiJhZm9ibWwzaDF6cXZ3c2FpM2I1dXJ1eHY0dDFkd25qaiIsIl9zZXNzaW9uX2V4cGlyeSI6OTk5OTk5OTksIl9hdXRoX3NpZ25faW5fYXV0aCI6ZmFsc2UsIl9hdXRoX3NpZ25faW5faWQiOjE3LCJfYXV0aF91c2VyX2hhc2giOiJmM2JhZjA5M2ExZDA4ZGZiZGIxZmUwM2RlNmQ1ODdjY2ZjYjRiNzQxIiwiX2F1dGhfdXNlcl9pZCI6IjQwIiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJ1c2VyX2lkIjoxNywiX2F1dGhfc2lnbl9pbl9tYXBwaW5nX2lkIjo0MH0=','2020-06-04 19:34:01.729101'),('aj0wa6o0p4t5qeorj3oxrbvyiijzaq2u','NDllMmJiNTU1MDI2Njk2YjVhOWI3ODhjZTFlYzhhNWRkNDJiZTE2MTp7Il9hdXRoX3VzZXJfaGFzaCI6Ijg3ZWVlOTlkMzk0NWRiODE3YTRjYWJkMzQxYTU3NzJhYmQxNjQxMzYiLCJ1c2VyX2lkIjoyMCwiX2F1dGhfc2lnbl9pbl9tYXBwaW5nX2lkIjo0NSwiX2F1dGhfdXNlcl9pZCI6IjQ1IiwiX3Nlc3Npb25fZXhwaXJ5Ijo5OTk5OTk5OSwiX2F1dGhfc2lnbl9pbl9hdXRoIjpmYWxzZSwiX2F1dGhfc2lnbl9pbl9pZCI6MjAsIl9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiYXV0aF90b2tlbiI6ImFqMHdhNm8wcDR0NXFlb3JqM294cmJ2eWlpanphcTJ1In0=','2020-05-17 17:31:32.167410'),('an3y99n44awoph09alk0n5f3dzdqaylw','NjQ3MGFjMDRiOTdjZDlhNjJmZTA4YTViMzI2ODhhOWEwNWZmNzYyOTp7Il9hdXRoX3VzZXJfaGFzaCI6Ijg3ZWVlOTlkMzk0NWRiODE3YTRjYWJkMzQxYTU3NzJhYmQxNjQxMzYiLCJhdXRoX3Rva2VuIjoiYW4zeTk5bjQ0YXdvcGgwOWFsazBuNWYzZHpkcWF5bHciLCJ1c2VyX2lkIjoyMCwiX2F1dGhfc2lnbl9pbl9hdXRoIjpmYWxzZSwiX2F1dGhfdXNlcl9pZCI6IjQ1IiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJfYXV0aF9zaWduX2luX2lkIjoyMCwiX2F1dGhfc2lnbl9pbl9tYXBwaW5nX2lkIjo0NSwiX3Nlc3Npb25fZXhwaXJ5Ijo5OTk5OTk5OX0=','2020-05-31 15:50:50.829156'),('atnp66mtkbgk6h3jva3smxr5uqf339qe','NzY2MjAzODdhZmY2MWY4NjhlNjVkMDIxMzI5NjRkNjc5ZDNmMjgwODp7Il9hdXRoX3NpZ25faW5faWQiOjIwLCJfYXV0aF91c2VyX2lkIjoiNDUiLCJfYXV0aF9zaWduX2luX2F1dGgiOmZhbHNlLCJfc2Vzc2lvbl9leHBpcnkiOjk5OTk5OTk5LCJhdXRoX3Rva2VuIjoiYXRucDY2bXRrYmdrNmgzanZhM3NteHI1dXFmMzM5cWUiLCJfYXV0aF9zaWduX2luX21hcHBpbmdfaWQiOjQ1LCJfYXV0aF91c2VyX2hhc2giOiI4N2VlZTk5ZDM5NDVkYjgxN2E0Y2FiZDM0MWE1NzcyYWJkMTY0MTM2IiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJ1c2VyX2lkIjoyMH0=','2020-05-31 13:51:47.562619'),('axi8kmy21dakoq555ak8j540cfimynzj','ZDc1MzYxYmIxNGY5OGQ0M2FhMDc1N2JjZmZhNDkzZGEwNzdhZWUyMjp7Il9hdXRoX3NpZ25faW5faWQiOjIwLCJfYXV0aF9zaWduX2luX21hcHBpbmdfaWQiOjQ1LCJfYXV0aF91c2VyX2hhc2giOiI4N2VlZTk5ZDM5NDVkYjgxN2E0Y2FiZDM0MWE1NzcyYWJkMTY0MTM2IiwiX2F1dGhfdXNlcl9pZCI6IjQ1IiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJhdXRoX3Rva2VuIjoiYXhpOGtteTIxZGFrb3E1NTVhazhqNTQwY2ZpbXluemoiLCJ1c2VyX2lkIjoyMCwiX2F1dGhfc2lnbl9pbl9hdXRoIjpmYWxzZSwiX3Nlc3Npb25fZXhwaXJ5Ijo5OTk5OTk5OX0=','2020-05-16 13:32:09.163411'),('b6ar2gu9pnwdgd0zl12omiyiz12f6ogo','YjA1ZWEyYmJhNmNmNTkzNTA1OGQ0YzY2NWZhY2RkZTYxNTk2NDQ0NDp7InVzZXJfaWQiOjIwLCJfYXV0aF91c2VyX2lkIjoiNDUiLCJfc2Vzc2lvbl9leHBpcnkiOjk5OTk5OTk5LCJfYXV0aF9zaWduX2luX21hcHBpbmdfaWQiOjQ1LCJfYXV0aF9zaWduX2luX2lkIjoyMCwiX2F1dGhfc2lnbl9pbl9hdXRoIjpmYWxzZSwiX2F1dGhfdXNlcl9oYXNoIjoiODdlZWU5OWQzOTQ1ZGI4MTdhNGNhYmQzNDFhNTc3MmFiZDE2NDEzNiIsImF1dGhfdG9rZW4iOiJiNmFyMmd1OXBud2RnZDB6bDEyb21peWl6MTJmNm9nbyIsIl9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIn0=','2020-05-15 16:55:20.087062'),('b93o7v1tskxqlp38xthis5yiqfcr9dty','MzM1NTA3NTk4YzJiYzk0Y2JlNWM1NWExNTY1M2MxMjg0Y2NjMzcxYzp7ImF1dGhfdG9rZW4iOiJiOTNvN3YxdHNreHFscDM4eHRoaXM1eWlxZmNyOWR0eSIsIl9hdXRoX3NpZ25faW5fYXV0aCI6ZmFsc2UsInVzZXJfaWQiOjIwLCJfYXV0aF91c2VyX2hhc2giOiI4N2VlZTk5ZDM5NDVkYjgxN2E0Y2FiZDM0MWE1NzcyYWJkMTY0MTM2IiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJfYXV0aF9zaWduX2luX2lkIjoyMCwiX2F1dGhfdXNlcl9pZCI6IjQ1IiwiX2F1dGhfc2lnbl9pbl9tYXBwaW5nX2lkIjo0NSwiX3Nlc3Npb25fZXhwaXJ5Ijo5OTk5OTk5OX0=','2020-05-14 12:49:21.561992'),('bmuef9v8vph27vrj3b5khrnm3fuily2b','ZDY3ZTIxNmJlMTAwZTE2MDRkNDE2ZTY4Y2ExOTJkMzc5ZDc4YzU2Mjp7ImF1dGhfdG9rZW4iOiJibXVlZjl2OHZwaDI3dnJqM2I1a2hybm0zZnVpbHkyYiIsIl9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX3Nlc3Npb25fZXhwaXJ5Ijo5OTk5OTk5OSwiX2F1dGhfc2lnbl9pbl9tYXBwaW5nX2lkIjo0NSwiX2F1dGhfdXNlcl9oYXNoIjoiODdlZWU5OWQzOTQ1ZGI4MTdhNGNhYmQzNDFhNTc3MmFiZDE2NDEzNiIsIl9hdXRoX3NpZ25faW5faWQiOjIwLCJfYXV0aF91c2VyX2lkIjoiNDUiLCJ1c2VyX2lkIjoyMCwiX2F1dGhfc2lnbl9pbl9hdXRoIjpmYWxzZX0=','2020-05-22 11:41:57.451726'),('bwj2brmoposuoypzqawvlqfxnvs01a48','ZDExN2NhNzMxZGU0YzFmZjIwZjVlZDk1YTZjMGEyMTM4OGU5MGJiMDp7Il9hdXRoX3NpZ25faW5faWQiOjIwLCJfYXV0aF9zaWduX2luX2F1dGgiOmZhbHNlLCJ1c2VyX2lkIjoyMCwiX2F1dGhfdXNlcl9pZCI6IjQ1IiwiX2F1dGhfdXNlcl9oYXNoIjoiODdlZWU5OWQzOTQ1ZGI4MTdhNGNhYmQzNDFhNTc3MmFiZDE2NDEzNiIsIl9hdXRoX3NpZ25faW5fbWFwcGluZ19pZCI6NDUsIl9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiYXV0aF90b2tlbiI6ImJ3ajJicm1vcG9zdW95cHpxYXd2bHFmeG52czAxYTQ4IiwiX3Nlc3Npb25fZXhwaXJ5Ijo5OTk5OTk5OX0=','2020-05-14 11:50:04.464527'),('bx63wywwwbj0fi4lg6v7wnwld3c39ywe','MWU1NWZhOTU4MDVjNmM4ZjAzMTFjZjMxZWI1YWNmY2IyMWY2NDlmNjp7ImF1dGhfdG9rZW4iOiJieDYzd3l3d3diajBmaTRsZzZ2N3dud2xkM2MzOXl3ZSIsIl9hdXRoX3NpZ25faW5fYXV0aCI6ZmFsc2UsIl9hdXRoX3NpZ25faW5faWQiOjE4LCJfYXV0aF91c2VyX2hhc2giOiI0YzM4ZjgzNmU4YzE5MzIzYWQ1YzkzNzlkYzgxMmE5MzMwMTE5ZGI3IiwiX2F1dGhfc2lnbl9pbl9tYXBwaW5nX2lkIjo0MSwidXNlcl9pZCI6MTgsIl9zZXNzaW9uX2V4cGlyeSI6OTk5OTk5OTksIl9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9pZCI6IjQxIn0=','2020-05-13 15:36:01.578591'),('c406sea6y65n28s80ahso6k7hq7s5bn2','MTIzMDdlNDgzNGEzMGY2OWVmYjE1MDcyZWIyNjExOGQ3MmM2OWRiNzp7Il9hdXRoX3VzZXJfaWQiOiI0MCIsIl9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfc2lnbl9pbl9hdXRoIjpmYWxzZSwiX2F1dGhfc2lnbl9pbl9pZCI6MTcsIl9hdXRoX3VzZXJfaGFzaCI6ImYzYmFmMDkzYTFkMDhkZmJkYjFmZTAzZGU2ZDU4N2NjZmNiNGI3NDEiLCJhdXRoX3Rva2VuIjoiYzQwNnNlYTZ5NjVuMjhzODBhaHNvNms3aHE3czVibjIiLCJfc2Vzc2lvbl9leHBpcnkiOjk5OTk5OTk5LCJfYXV0aF9zaWduX2luX21hcHBpbmdfaWQiOjQwLCJ1c2VyX2lkIjoxN30=','2020-05-24 11:40:05.404782'),('c7an9f9odotfr756ovsatwob12nxzx0w','ZDUzZjlmMzIwYWQ0OWVkMDQzOWY1NWQ5NDY0ZWEwZDZmNGI3Y2UyNjp7Il9hdXRoX3VzZXJfaGFzaCI6IjY0YjdkOGNjY2YyMmYyOGUxNzM2NDZmZmI1YmI2ZjViNzU5YzgwZDYiLCJfYXV0aF9zaWduX2luX2lkIjoxMywidXNlcl9pZCI6MTMsIl9hdXRoX3NpZ25faW5fYXV0aCI6ZmFsc2UsIl9hdXRoX3VzZXJfaWQiOiIzMyIsIl9hdXRoX3NpZ25faW5fbWFwcGluZ19pZCI6MzMsIl9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiYXV0aF90b2tlbiI6ImM3YW45ZjlvZG90ZnI3NTZvdnNhdHdvYjEybnh6eDB3IiwiX3Nlc3Npb25fZXhwaXJ5Ijo5OTk5OTk5OX0=','2020-05-13 13:58:59.703973'),('cg7olem2m813nyphdcosvqe6n3kdtg2z','MzAzOGRmZjY5NjU4NGM0Y2MyMjU0YTVkOGZkNzQxZjA4YWNiZjhhYjp7ImF1dGhfdG9rZW4iOiJjZzdvbGVtMm04MTNueXBoZGNvc3ZxZTZuM2tkdGcyeiIsIl9zZXNzaW9uX2V4cGlyeSI6OTk5OTk5OTksIl9hdXRoX3VzZXJfaWQiOiI0NSIsIl9hdXRoX3NpZ25faW5faWQiOjIwLCJfYXV0aF91c2VyX2hhc2giOiI4N2VlZTk5ZDM5NDVkYjgxN2E0Y2FiZDM0MWE1NzcyYWJkMTY0MTM2IiwidXNlcl9pZCI6MjAsIl9hdXRoX3NpZ25faW5fYXV0aCI6ZmFsc2UsIl9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfc2lnbl9pbl9tYXBwaW5nX2lkIjo0NX0=','2020-05-22 19:04:16.565341'),('cm5kk1jbds2yzjvdlp7u46jh8zgvbh8r','NjdiNWY0OTAwYjA2MzhkZmE3M2MzOWE1Njc3ZTc1MDBmYTQ0ZmE3NDp7InVzZXJfaWQiOjE3LCJfc2Vzc2lvbl9leHBpcnkiOjk5OTk5OTk5LCJhdXRoX3Rva2VuIjoiY201a2sxamJkczJ5emp2ZGxwN3U0NmpoOHpndmJoOHIiLCJfYXV0aF9zaWduX2luX2F1dGgiOmZhbHNlLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9hdXRoX3NpZ25faW5fbWFwcGluZ19pZCI6NDAsIl9hdXRoX3VzZXJfaWQiOiI0MCIsIl9hdXRoX3NpZ25faW5faWQiOjE3LCJfYXV0aF91c2VyX2hhc2giOiJmM2JhZjA5M2ExZDA4ZGZiZGIxZmUwM2RlNmQ1ODdjY2ZjYjRiNzQxIn0=','2020-05-13 15:45:57.740201'),('cr4d43kop2cybu9nexnh2i2c0diqda2y','NTczZmIxOTQ2MGYzNTBiMmEzNDc3NDljZGMyZTJiNDczNzE4OGZiNzp7Il9hdXRoX3VzZXJfaGFzaCI6IjRjMzhmODM2ZThjMTkzMjNhZDVjOTM3OWRjODEyYTkzMzAxMTlkYjciLCJfYXV0aF91c2VyX2lkIjoiNDEiLCJfYXV0aF9zaWduX2luX2lkIjoxOCwiX2F1dGhfc2lnbl9pbl9tYXBwaW5nX2lkIjo0MSwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJfc2Vzc2lvbl9leHBpcnkiOjk5OTk5OTk5LCJ1c2VyX2lkIjoxOCwiYXV0aF90b2tlbiI6ImNyNGQ0M2tvcDJjeWJ1OW5leG5oMmkyYzBkaXFkYTJ5IiwiX2F1dGhfc2lnbl9pbl9hdXRoIjpmYWxzZX0=','2020-05-13 13:46:55.612880'),('crfjyydf0otshmokm33tbni91qzuyp1x','YjNkZjYwMzc0NTljMzRhODQwZGEyNzg2ODE2NWY0MGQ2NjkwMTc3NTp7Il9hdXRoX3NpZ25faW5faWQiOjE3LCJfc2Vzc2lvbl9leHBpcnkiOjk5OTk5OTk5LCJfYXV0aF91c2VyX2lkIjoiNDAiLCJfYXV0aF9zaWduX2luX21hcHBpbmdfaWQiOjQwLCJfYXV0aF9zaWduX2luX2F1dGgiOmZhbHNlLCJ1c2VyX2lkIjoxNywiYXV0aF90b2tlbiI6ImNyZmp5eWRmMG90c2htb2ttMzN0Ym5pOTFxenV5cDF4IiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJfYXV0aF91c2VyX2hhc2giOiJmM2JhZjA5M2ExZDA4ZGZiZGIxZmUwM2RlNmQ1ODdjY2ZjYjRiNzQxIn0=','2020-05-29 14:04:49.477667'),('d03fjudubq8k5ojsmfgl40d8m6o6gp3a','YjRkNjY5ZDk4MjVmOTI1NjBhNDliMzUzZjk0ZDhlYTk4ZDRkYTczMjp7Il9hdXRoX3NpZ25faW5fYXV0aCI6ZmFsc2UsIl9hdXRoX3VzZXJfaWQiOiI0NSIsIl9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfc2lnbl9pbl9pZCI6MjAsIl9hdXRoX3VzZXJfaGFzaCI6Ijg3ZWVlOTlkMzk0NWRiODE3YTRjYWJkMzQxYTU3NzJhYmQxNjQxMzYiLCJhdXRoX3Rva2VuIjoiZDAzZmp1ZHVicThrNW9qc21mZ2w0MGQ4bTZvNmdwM2EiLCJfc2Vzc2lvbl9leHBpcnkiOjk5OTk5OTk5LCJ1c2VyX2lkIjoyMCwiX2F1dGhfc2lnbl9pbl9tYXBwaW5nX2lkIjo0NX0=','2020-05-28 18:03:02.738152'),('d41mko5zumwey9tdkky35ty5c6dxvugd','NWIzMmRmZWU0ZTljMTJiZDM3MWJjMWFjYjlkMDUwMTcyNmEwOGIxNTp7Il9hdXRoX3NpZ25faW5faWQiOjIwLCJfYXV0aF91c2VyX2hhc2giOiI4N2VlZTk5ZDM5NDVkYjgxN2E0Y2FiZDM0MWE1NzcyYWJkMTY0MTM2IiwiX2F1dGhfc2lnbl9pbl9tYXBwaW5nX2lkIjo0NSwiX2F1dGhfc2lnbl9pbl9hdXRoIjpmYWxzZSwiX3Nlc3Npb25fZXhwaXJ5Ijo5OTk5OTk5OSwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJ1c2VyX2lkIjoyMCwiYXV0aF90b2tlbiI6ImQ0MW1rbzV6dW13ZXk5dGRra3kzNXR5NWM2ZHh2dWdkIiwiX2F1dGhfdXNlcl9pZCI6IjQ1In0=','2020-05-27 13:13:48.533019'),('dvffdnkbfi661nliohtvutx3x3lavv3s','ODZkN2Y5Yzk3ZjM5MWQzNGUwOGUwNDhkNzQzM2I3ODAxMzYwNzYzOTp7Il9zZXNzaW9uX2V4cGlyeSI6OTk5OTk5OTksIl9hdXRoX3NpZ25faW5fbWFwcGluZ19pZCI6NDAsInVzZXJfaWQiOjE3LCJfYXV0aF91c2VyX2hhc2giOiJmM2JhZjA5M2ExZDA4ZGZiZGIxZmUwM2RlNmQ1ODdjY2ZjYjRiNzQxIiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJfYXV0aF91c2VyX2lkIjoiNDAiLCJfYXV0aF9zaWduX2luX2F1dGgiOmZhbHNlLCJhdXRoX3Rva2VuIjoiZHZmZmRua2JmaTY2MW5saW9odHZ1dHgzeDNsYXZ2M3MiLCJfYXV0aF9zaWduX2luX2lkIjoxN30=','2020-06-12 18:52:21.158361'),('e93b6de33xggejx4hw2bsp54bqtnygbj','NDExNThlYWUyNmNiZmZlMTU0OGE5Mzk3ZjU3MjY4YjIxODIxNGM0Mjp7Il9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfc2lnbl9pbl9hdXRoIjpmYWxzZSwidXNlcl9pZCI6MjAsIl9hdXRoX3VzZXJfaGFzaCI6Ijg3ZWVlOTlkMzk0NWRiODE3YTRjYWJkMzQxYTU3NzJhYmQxNjQxMzYiLCJhdXRoX3Rva2VuIjoiZTkzYjZkZTMzeGdnZWp4NGh3MmJzcDU0YnF0bnlnYmoiLCJfYXV0aF9zaWduX2luX2lkIjoyMCwiX2F1dGhfdXNlcl9pZCI6IjQ1IiwiX2F1dGhfc2lnbl9pbl9tYXBwaW5nX2lkIjo0NSwiX3Nlc3Npb25fZXhwaXJ5Ijo5OTk5OTk5OX0=','2020-05-14 13:27:25.425500'),('enzz48e46skulybm20htlyay1b3cqlfa','YTJkMjBmZGNlNjVjNWVlMWE5ZWQxMzAxM2Q4Y2ZiNDc3MGYxNWYwNTp7Il9zZXNzaW9uX2V4cGlyeSI6OTk5OTk5OTksIl9hdXRoX3VzZXJfaGFzaCI6Ijg3ZWVlOTlkMzk0NWRiODE3YTRjYWJkMzQxYTU3NzJhYmQxNjQxMzYiLCJfYXV0aF9zaWduX2luX21hcHBpbmdfaWQiOjQ1LCJfYXV0aF91c2VyX2lkIjoiNDUiLCJfYXV0aF9zaWduX2luX2lkIjoyMCwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJfYXV0aF9zaWduX2luX2F1dGgiOmZhbHNlLCJhdXRoX3Rva2VuIjoiZW56ejQ4ZTQ2c2t1bHlibTIwaHRseWF5MWIzY3FsZmEiLCJ1c2VyX2lkIjoyMH0=','2020-05-15 13:27:54.045612'),('erdrd389lunp7g9h3oje3qkmq0fsterb','NzliYjI5NGVhNjcxYzcyNWQ3NzE4ZDY1YmZhZmY5Yzk3ZGQzM2FjZTp7Il9hdXRoX3VzZXJfaWQiOiIyIiwicmVzX2xpc3QiOlt7Im5hbWUiOiJLYXBwbyBybyBhbm4iLCJpZCI6MX0seyJuYW1lIjoiUm9hbm4gMiBUZXN0IiwiaWQiOjJ9XSwiY21zX2F1dGhfc2lnbl9pbl9tYXBwaW5nX3JvbGVfbmFtZSI6Ik1FTlRFIiwiY21zX2F1dGhfc2lnbl9pbl9jdXJyX3Jlc19pZCI6MSwiY21zX2F1dGhfc2lnbl9pbl9hdXRoIjpmYWxzZSwiY21zX2F1dGhfc2lnbl9pbl9pZCI6MSwiY21zX2F1dGhfc2lnbl9pbl9tYXBwaW5nX3JvbGUiOjEsImNtc19hdXRoX3NpZ25faW5fbWFwcGluZ19mbmFtZSI6IkhpZ28iLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9hdXRoX3VzZXJfaGFzaCI6IjQzZDI1ZWVmNDAzMzA1YzNhZGZjNGY2YzhmNmVmMjhhZjkwYTRmNmMiLCJjbXNfYXV0aF9zaWduX2luX21hcHBpbmdfaWQiOiJhZG1pbm1lbnRlQHRveW9zdS5jb20ifQ==','2017-04-25 07:47:42.990921'),('eup2fn8pn5kek9j580gmnxky25njekx3','YjZjMjQ0ZDhmMTU3MDI4NDQxYzNhMzFlNjI1Mjg4ZjdiOTc4NjllZjp7Il9hdXRoX3NpZ25faW5fbWFwcGluZ19pZCI6NDAsIl9hdXRoX3VzZXJfaGFzaCI6ImYzYmFmMDkzYTFkMDhkZmJkYjFmZTAzZGU2ZDU4N2NjZmNiNGI3NDEiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9zZXNzaW9uX2V4cGlyeSI6OTk5OTk5OTksInVzZXJfaWQiOjE3LCJfYXV0aF9zaWduX2luX2lkIjoxNywiX2F1dGhfc2lnbl9pbl9hdXRoIjpmYWxzZSwiYXV0aF90b2tlbiI6ImV1cDJmbjhwbjVrZWs5ajU4MGdtbnhreTI1bmpla3gzIiwiX2F1dGhfdXNlcl9pZCI6IjQwIn0=','2020-05-27 20:12:47.684912'),('ewl5fpp78wegiyikf5e4cy46ihqtobfb','NWRmYzc1MWExMGZmODI1NWY0NzhjNjM3ZDI1YTRiYzgyODZhMWQ0ZTp7Il9hdXRoX3NpZ25faW5faWQiOjIwLCJfYXV0aF9zaWduX2luX2F1dGgiOmZhbHNlLCJ1c2VyX2lkIjoyMCwiX2F1dGhfdXNlcl9pZCI6IjQ1IiwiX2F1dGhfdXNlcl9oYXNoIjoiODdlZWU5OWQzOTQ1ZGI4MTdhNGNhYmQzNDFhNTc3MmFiZDE2NDEzNiIsIl9hdXRoX3NpZ25faW5fbWFwcGluZ19pZCI6NDUsIl9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiYXV0aF90b2tlbiI6ImV3bDVmcHA3OHdlZ2l5aWtmNWU0Y3k0NmlocXRvYmZiIiwiX3Nlc3Npb25fZXhwaXJ5Ijo5OTk5OTk5OX0=','2020-05-14 12:04:05.582398'),('f7jij6sqxlsmmajppr5j598bdcb3ntey','MDM0NjA1NDk3MDA5NmY0OTYwNjY1NTljNDA4YTc5MmVjNzY3YTNjNzp7Il9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfc2lnbl9pbl9hdXRoIjpmYWxzZSwidXNlcl9pZCI6MjAsIl9hdXRoX3VzZXJfaGFzaCI6Ijg3ZWVlOTlkMzk0NWRiODE3YTRjYWJkMzQxYTU3NzJhYmQxNjQxMzYiLCJhdXRoX3Rva2VuIjoiZjdqaWo2c3F4bHNtbWFqcHByNWo1OThiZGNiM250ZXkiLCJfYXV0aF9zaWduX2luX2lkIjoyMCwiX2F1dGhfdXNlcl9pZCI6IjQ1IiwiX2F1dGhfc2lnbl9pbl9tYXBwaW5nX2lkIjo0NSwiX3Nlc3Npb25fZXhwaXJ5Ijo5OTk5OTk5OX0=','2020-05-14 12:24:11.491977'),('fnif3km56ngu7tc2zznvzfmf5w7miifj','M2E2Mzk5MjA3OTA3M2Q1MWNhYzk0NTEyMmY1MTVjNjczYThhOGE5Yjp7Il9hdXRoX3VzZXJfaGFzaCI6ImYzYmFmMDkzYTFkMDhkZmJkYjFmZTAzZGU2ZDU4N2NjZmNiNGI3NDEiLCJ1c2VyX2lkIjoxNywiX2F1dGhfc2lnbl9pbl9tYXBwaW5nX2lkIjo0MCwiX2F1dGhfdXNlcl9pZCI6IjQwIiwiX3Nlc3Npb25fZXhwaXJ5Ijo5OTk5OTk5OSwiX2F1dGhfc2lnbl9pbl9hdXRoIjpmYWxzZSwiX2F1dGhfc2lnbl9pbl9pZCI6MTcsIl9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiYXV0aF90b2tlbiI6ImZuaWYza201Nm5ndTd0YzJ6em52emZtZjV3N21paWZqIn0=','2020-05-17 18:43:11.492333'),('fsym122uankbzzn9kl62oiuq563y3hkw','ZWI3NWNlZGI1N2MwY2FlNWRhZmI2MzI2MTNmMTVlODYyODNhNmQxNTp7Il9hdXRoX3VzZXJfaGFzaCI6Ijg3ZWVlOTlkMzk0NWRiODE3YTRjYWJkMzQxYTU3NzJhYmQxNjQxMzYiLCJfYXV0aF91c2VyX2lkIjoiNDUiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9hdXRoX3NpZ25faW5fYXV0aCI6ZmFsc2UsIl9zZXNzaW9uX2V4cGlyeSI6OTk5OTk5OTksIl9hdXRoX3NpZ25faW5faWQiOjIwLCJ1c2VyX2lkIjoyMCwiX2F1dGhfc2lnbl9pbl9tYXBwaW5nX2lkIjo0NSwiYXV0aF90b2tlbiI6ImZzeW0xMjJ1YW5rYnp6bjlrbDYyb2l1cTU2M3kzaGt3In0=','2020-06-10 17:25:54.496745'),('fxeze72vw2xpaxuzgloa9t3w8mqo4ss0','NDViZjRmYjZhNDg2NzhjZTU0OTVlNzg4YzgwOWViOGI1MjM2M2ZmNTp7Il9hdXRoX3NpZ25faW5fbWFwcGluZ19pZCI6NDUsIl9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwidXNlcl9pZCI6MjAsIl9hdXRoX3VzZXJfaWQiOiI0NSIsIl9hdXRoX3VzZXJfaGFzaCI6Ijg3ZWVlOTlkMzk0NWRiODE3YTRjYWJkMzQxYTU3NzJhYmQxNjQxMzYiLCJfYXV0aF9zaWduX2luX2lkIjoyMCwiX2F1dGhfc2lnbl9pbl9hdXRoIjpmYWxzZSwiYXV0aF90b2tlbiI6ImZ4ZXplNzJ2dzJ4cGF4dXpnbG9hOXQzdzhtcW80c3MwIiwiX3Nlc3Npb25fZXhwaXJ5Ijo5OTk5OTk5OX0=','2020-05-17 16:50:13.367555'),('fxz4xi69msv6dtf6c3c531z56xgo1a4m','YzJlY2M0MmQ1ZGM3YmU0MWUwNDE1YTc1MGZiMjViYmNjOTEzNDI4Zjp7Il9hdXRoX3VzZXJfaGFzaCI6Ijg3ZWVlOTlkMzk0NWRiODE3YTRjYWJkMzQxYTU3NzJhYmQxNjQxMzYiLCJ1c2VyX2lkIjoyMCwiX2F1dGhfc2lnbl9pbl9tYXBwaW5nX2lkIjo0NSwiX2F1dGhfdXNlcl9pZCI6IjQ1IiwiX3Nlc3Npb25fZXhwaXJ5Ijo5OTk5OTk5OSwiX2F1dGhfc2lnbl9pbl9hdXRoIjpmYWxzZSwiX2F1dGhfc2lnbl9pbl9pZCI6MjAsIl9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiYXV0aF90b2tlbiI6ImZ4ejR4aTY5bXN2NmR0ZjZjM2M1MzF6NTZ4Z28xYTRtIn0=','2020-05-17 17:55:06.583838'),('gbq92eu07feusgwk3exoxzd8ebl1wrta','ZDZhOTY2MjBlN2Y0M2RiZjAyN2NlZGUyZDU2MDMzODVjZmUyMzljOTp7Il9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfc2lnbl9pbl9hdXRoIjpmYWxzZSwidXNlcl9pZCI6MjAsIl9hdXRoX3VzZXJfaGFzaCI6Ijg3ZWVlOTlkMzk0NWRiODE3YTRjYWJkMzQxYTU3NzJhYmQxNjQxMzYiLCJhdXRoX3Rva2VuIjoiZ2JxOTJldTA3ZmV1c2d3azNleG94emQ4ZWJsMXdydGEiLCJfYXV0aF9zaWduX2luX2lkIjoyMCwiX2F1dGhfdXNlcl9pZCI6IjQ1IiwiX2F1dGhfc2lnbl9pbl9tYXBwaW5nX2lkIjo0NSwiX3Nlc3Npb25fZXhwaXJ5Ijo5OTk5OTk5OX0=','2020-05-14 12:27:26.470472'),('gudpijyyhx4s5u379fp3wg1of3nuejzt','YTM4MzgyYjQxODg0M2NiZTM3MTY5ZTE2Yzg0YWU5ZTNjYjQ1NDY3Mjp7Il9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9oYXNoIjoiNGMzOGY4MzZlOGMxOTMyM2FkNWM5Mzc5ZGM4MTJhOTMzMDExOWRiNyIsIl9zZXNzaW9uX2V4cGlyeSI6OTk5OTk5OTksInVzZXJfaWQiOjE4LCJhdXRoX3Rva2VuIjoiZ3VkcGlqeXloeDRzNXUzNzlmcDN3ZzFvZjNudWVqenQiLCJfYXV0aF9zaWduX2luX21hcHBpbmdfaWQiOjQxLCJfYXV0aF9zaWduX2luX2F1dGgiOmZhbHNlLCJfYXV0aF91c2VyX2lkIjoiNDEiLCJfYXV0aF9zaWduX2luX2lkIjoxOH0=','2020-05-13 13:33:31.320476'),('gykuggiuu8e478ccdb0f9hpo6su6w8vj','YWYyNDRjOWZiYWFlZDdiMDRkMGQyN2IyYmI5M2ExZjQ1Y2UwYzJjNzp7Il9hdXRoX3VzZXJfaGFzaCI6Ijg3ZWVlOTlkMzk0NWRiODE3YTRjYWJkMzQxYTU3NzJhYmQxNjQxMzYiLCJ1c2VyX2lkIjoyMCwiX2F1dGhfc2lnbl9pbl9tYXBwaW5nX2lkIjo0NSwiX2F1dGhfdXNlcl9pZCI6IjQ1IiwiX3Nlc3Npb25fZXhwaXJ5Ijo5OTk5OTk5OSwiX2F1dGhfc2lnbl9pbl9hdXRoIjpmYWxzZSwiX2F1dGhfc2lnbl9pbl9pZCI6MjAsIl9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiYXV0aF90b2tlbiI6Imd5a3VnZ2l1dThlNDc4Y2NkYjBmOWhwbzZzdTZ3OHZqIn0=','2020-05-17 17:34:24.914683'),('h476xog0nfek6m92ajrclzuadbu6yfvv','Nzk2Y2QwZGRmOTAxYTMxNjlhMjc4ZTliZmJkNjliMTk5ZDIxZTAzYjp7ImF1dGhfdG9rZW4iOiJoNDc2eG9nMG5mZWs2bTkyYWpyY2x6dWFkYnU2eWZ2diIsIl9hdXRoX3VzZXJfaWQiOiI0NSIsIl9zZXNzaW9uX2V4cGlyeSI6OTk5OTk5OTksIl9hdXRoX3NpZ25faW5fbWFwcGluZ19pZCI6NDUsIl9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwidXNlcl9pZCI6MjAsIl9hdXRoX3VzZXJfaGFzaCI6Ijg3ZWVlOTlkMzk0NWRiODE3YTRjYWJkMzQxYTU3NzJhYmQxNjQxMzYiLCJfYXV0aF9zaWduX2luX2F1dGgiOmZhbHNlLCJfYXV0aF9zaWduX2luX2lkIjoyMH0=','2020-05-20 16:56:58.799264'),('h4xi3ot0vsvke2w8sewo6h9s38o4t9co','ZmQyMzRkMGJmYmQ3NjkwYWRjOGNmODA3YmJlYzEwZWMxOTgzNmZmNzp7Il9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9pZCI6IjQ1IiwidXNlcl9pZCI6MjAsIl9hdXRoX3VzZXJfaGFzaCI6Ijg3ZWVlOTlkMzk0NWRiODE3YTRjYWJkMzQxYTU3NzJhYmQxNjQxMzYiLCJfYXV0aF9zaWduX2luX2lkIjoyMCwiYXV0aF90b2tlbiI6Img0eGkzb3QwdnN2a2UydzhzZXdvNmg5czM4bzR0OWNvIiwiX2F1dGhfc2lnbl9pbl9hdXRoIjpmYWxzZSwiX3Nlc3Npb25fZXhwaXJ5Ijo5OTk5OTk5OSwiX2F1dGhfc2lnbl9pbl9tYXBwaW5nX2lkIjo0NX0=','2020-05-23 19:29:26.377679'),('h6ny79zyws0ut2702xrugl6gonqk2iud','OWExYjNkZDQwZThiYjdiODhhNWM0Y2I2MTUxYjQwZDk0MTIwZjZhYjp7InVzZXJfaWQiOjE3LCJfc2Vzc2lvbl9leHBpcnkiOjk5OTk5OTk5LCJhdXRoX3Rva2VuIjoiaDZueTc5enl3czB1dDI3MDJ4cnVnbDZnb25xazJpdWQiLCJfYXV0aF9zaWduX2luX2F1dGgiOmZhbHNlLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9hdXRoX3NpZ25faW5fbWFwcGluZ19pZCI6NDAsIl9hdXRoX3VzZXJfaWQiOiI0MCIsIl9hdXRoX3NpZ25faW5faWQiOjE3LCJfYXV0aF91c2VyX2hhc2giOiJmM2JhZjA5M2ExZDA4ZGZiZGIxZmUwM2RlNmQ1ODdjY2ZjYjRiNzQxIn0=','2020-05-13 16:16:08.485258'),('hfnjxigxxdv579smd4q0lkt12pgdmofz','ODBlZjk3Yjg1ZDE2ZWZlOGFiNjlhMzc4NDJiYWZiYTMzMjM1YTc0NDp7Il9hdXRoX3NpZ25faW5fYXV0aCI6ZmFsc2UsIl9hdXRoX3VzZXJfaWQiOiI0NSIsIl9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfc2lnbl9pbl9pZCI6MjAsIl9hdXRoX3VzZXJfaGFzaCI6Ijg3ZWVlOTlkMzk0NWRiODE3YTRjYWJkMzQxYTU3NzJhYmQxNjQxMzYiLCJhdXRoX3Rva2VuIjoiaGZuanhpZ3h4ZHY1NzlzbWQ0cTBsa3QxMnBnZG1vZnoiLCJfc2Vzc2lvbl9leHBpcnkiOjk5OTk5OTk5LCJ1c2VyX2lkIjoyMCwiX2F1dGhfc2lnbl9pbl9tYXBwaW5nX2lkIjo0NX0=','2020-05-28 19:12:48.132668'),('hp746d3ujxc1a6nfn6an07glxrzdwpfn','YjA4ZGIyZDVmMzk3MzgyNDI1NGU1Njc0MjkyNThmYTc4MzAxOTY4Zjp7Il9hdXRoX3VzZXJfaGFzaCI6Ijg3ZWVlOTlkMzk0NWRiODE3YTRjYWJkMzQxYTU3NzJhYmQxNjQxMzYiLCJ1c2VyX2lkIjoyMCwiX2F1dGhfc2lnbl9pbl9tYXBwaW5nX2lkIjo0NSwiX2F1dGhfdXNlcl9pZCI6IjQ1IiwiX3Nlc3Npb25fZXhwaXJ5Ijo5OTk5OTk5OSwiX2F1dGhfc2lnbl9pbl9hdXRoIjpmYWxzZSwiX2F1dGhfc2lnbl9pbl9pZCI6MjAsIl9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiYXV0aF90b2tlbiI6ImhwNzQ2ZDN1anhjMWE2bmZuNmFuMDdnbHhyemR3cGZuIn0=','2020-05-17 19:02:49.543126'),('hrsqv49gzv8r6sod7cfa4cweskdsohvd','ZTA5NzA2MzBmYWU1NWE0NGY4MmNhODUwMjI0MjU3MDExYWU5ZDMwYzp7Il9hdXRoX3NpZ25faW5fbWFwcGluZ19pZCI6NDUsIl9hdXRoX3NpZ25faW5fYXV0aCI6ZmFsc2UsIl9hdXRoX3VzZXJfaGFzaCI6Ijg3ZWVlOTlkMzk0NWRiODE3YTRjYWJkMzQxYTU3NzJhYmQxNjQxMzYiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsInVzZXJfaWQiOjIwLCJfc2Vzc2lvbl9leHBpcnkiOjk5OTk5OTk5LCJfYXV0aF9zaWduX2luX2lkIjoyMCwiYXV0aF90b2tlbiI6Imhyc3F2NDlnenY4cjZzb2Q3Y2ZhNGN3ZXNrZHNvaHZkIiwiX2F1dGhfdXNlcl9pZCI6IjQ1In0=','2020-05-29 18:31:01.896815'),('i4c6gg7g0923ye27ywmq5tlwngpasth3','MDJhN2EyY2RlOWVlNTQwMTNjZjIyNTZkMDFiYWZjZDA0ZGFhODBiODp7Il9hdXRoX3NpZ25faW5fYXV0aCI6ZmFsc2UsIl9hdXRoX3VzZXJfaWQiOiI0NSIsIl9hdXRoX3NpZ25faW5fbWFwcGluZ19pZCI6NDUsIl9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9oYXNoIjoiODdlZWU5OWQzOTQ1ZGI4MTdhNGNhYmQzNDFhNTc3MmFiZDE2NDEzNiIsImF1dGhfdG9rZW4iOiJpNGM2Z2c3ZzA5MjN5ZTI3eXdtcTV0bHduZ3Bhc3RoMyIsIl9hdXRoX3NpZ25faW5faWQiOjIwLCJ1c2VyX2lkIjoyMCwiX3Nlc3Npb25fZXhwaXJ5Ijo5OTk5OTk5OX0=','2020-06-14 12:08:21.552959'),('idnktpsyru2vpim0q4ibq3trkctlihpa','NzgzNDg4NGM2MmJkMjdiZTA2NjAwOTNmN2EzOTU5Njk0ODk5MzQwYjp7Il9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfc2lnbl9pbl9hdXRoIjpmYWxzZSwidXNlcl9pZCI6MjAsIl9hdXRoX3VzZXJfaGFzaCI6Ijg3ZWVlOTlkMzk0NWRiODE3YTRjYWJkMzQxYTU3NzJhYmQxNjQxMzYiLCJhdXRoX3Rva2VuIjoiaWRua3Rwc3lydTJ2cGltMHE0aWJxM3Rya2N0bGlocGEiLCJfYXV0aF9zaWduX2luX2lkIjoyMCwiX2F1dGhfdXNlcl9pZCI6IjQ1IiwiX2F1dGhfc2lnbl9pbl9tYXBwaW5nX2lkIjo0NSwiX3Nlc3Npb25fZXhwaXJ5Ijo5OTk5OTk5OX0=','2020-05-14 13:20:03.267144'),('ii3ge7bsooun0yyken5mt1kp707tz0gm','NTgxY2RkNjQxNjk5NWQ0NjU0ODk4YTE3ZDliNDUxMTRiNjQ5NDRiYTp7Il9hdXRoX3VzZXJfaGFzaCI6ImYzYmFmMDkzYTFkMDhkZmJkYjFmZTAzZGU2ZDU4N2NjZmNiNGI3NDEiLCJfYXV0aF9zaWduX2luX21hcHBpbmdfaWQiOjQwLCJfc2Vzc2lvbl9leHBpcnkiOjk5OTk5OTk5LCJfYXV0aF91c2VyX2lkIjoiNDAiLCJ1c2VyX2lkIjoxNywiX2F1dGhfc2lnbl9pbl9pZCI6MTcsIl9hdXRoX3NpZ25faW5fYXV0aCI6ZmFsc2UsImF1dGhfdG9rZW4iOiJpaTNnZTdic29vdW4weXlrZW41bXQxa3A3MDd0ejBnbSIsIl9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIn0=','2020-06-07 16:04:26.435143'),('isy397rqrbidy672tmrhz9dok1rvxhoc','ZGM4ZWM4N2JjZWY0MjQzMzg5NjkwOGEwZDRjYWZhNGI0MzFjOTUzNjp7Il9hdXRoX3VzZXJfaGFzaCI6Ijg3ZWVlOTlkMzk0NWRiODE3YTRjYWJkMzQxYTU3NzJhYmQxNjQxMzYiLCJ1c2VyX2lkIjoyMCwiX2F1dGhfc2lnbl9pbl9tYXBwaW5nX2lkIjo0NSwiX2F1dGhfdXNlcl9pZCI6IjQ1IiwiX3Nlc3Npb25fZXhwaXJ5Ijo5OTk5OTk5OSwiX2F1dGhfc2lnbl9pbl9hdXRoIjpmYWxzZSwiX2F1dGhfc2lnbl9pbl9pZCI6MjAsIl9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiYXV0aF90b2tlbiI6ImlzeTM5N3JxcmJpZHk2NzJ0bXJoejlkb2sxcnZ4aG9jIn0=','2020-05-17 18:02:21.769352'),('it9zbtpu2mwia6z667pf1k59dupvh4ch','YzM2YTcxM2U2YmRjMjhmNzhhMjhhMDViNjQ3YWM5ZDljNDFkOTQwOTp7Il9hdXRoX3VzZXJfaGFzaCI6Ijg3ZWVlOTlkMzk0NWRiODE3YTRjYWJkMzQxYTU3NzJhYmQxNjQxMzYiLCJ1c2VyX2lkIjoyMCwiX2F1dGhfc2lnbl9pbl9tYXBwaW5nX2lkIjo0NSwiX2F1dGhfdXNlcl9pZCI6IjQ1IiwiX3Nlc3Npb25fZXhwaXJ5Ijo5OTk5OTk5OSwiX2F1dGhfc2lnbl9pbl9hdXRoIjpmYWxzZSwiX2F1dGhfc2lnbl9pbl9pZCI6MjAsIl9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiYXV0aF90b2tlbiI6Iml0OXpidHB1Mm13aWE2ejY2N3BmMWs1OWR1cHZoNGNoIn0=','2020-05-17 18:30:27.846976'),('j17a772mnc97zbx19r5yt2hbuznezjbq','NDMwYmMyNjEyMDAxMzM1NGEwY2MyMDE4N2I0OTRhNGM4ODNiNjkzZjp7Il9hdXRoX3VzZXJfaWQiOiIyIiwiY21zX2F1dGhfc2lnbl9pbl9tYXBwaW5nX2lkIjoiYWRtaW5tZW50ZUB0b3lvc3UuY29tIiwiY21zX2F1dGhfc2lnbl9pbl9jdXJyX3Jlc19pZCI6MSwiY21zX2F1dGhfc2lnbl9pbl9hdXRoIjpmYWxzZSwiY21zX2F1dGhfc2lnbl9pbl9tYXBwaW5nX3JvbGUiOjEsIl9hdXRoX3VzZXJfaGFzaCI6IjQzZDI1ZWVmNDAzMzA1YzNhZGZjNGY2YzhmNmVmMjhhZjkwYTRmNmMiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsImNtc19hdXRoX3NpZ25faW5fbWFwcGluZ19yb2xlX25hbWUiOiJNRU5URSIsInJlc19saXN0IjpbeyJuYW1lIjoiS2FwcG8gcm8gYW5uIiwiaWQiOjF9LHsibmFtZSI6IlJvYW5uIDIgVGVzdCIsImlkIjoyfV0sImNtc19hdXRoX3NpZ25faW5fbWFwcGluZ19mbmFtZSI6IkhpZ28iLCJjbXNfYXV0aF9zaWduX2luX2lkIjoxfQ==','2017-05-03 02:29:32.741542'),('jkxmt2nq7yf09z10jipbcosc280ufsf1','ZDQ3MDNhNTEwMzQ4ZjY2ZjZkZTVhOTE1NzdiODI2YjdiY2FmY2E1NDp7InVzZXJfaWQiOjE3LCJfc2Vzc2lvbl9leHBpcnkiOjk5OTk5OTk5LCJhdXRoX3Rva2VuIjoiamt4bXQybnE3eWYwOXoxMGppcGJjb3NjMjgwdWZzZjEiLCJfYXV0aF9zaWduX2luX2F1dGgiOmZhbHNlLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9hdXRoX3NpZ25faW5fbWFwcGluZ19pZCI6NDAsIl9hdXRoX3VzZXJfaWQiOiI0MCIsIl9hdXRoX3NpZ25faW5faWQiOjE3LCJfYXV0aF91c2VyX2hhc2giOiJmM2JhZjA5M2ExZDA4ZGZiZGIxZmUwM2RlNmQ1ODdjY2ZjYjRiNzQxIn0=','2020-05-13 15:48:41.479574'),('jog5p833a1b8akvgxhlfheuaiflr3urx','NDliMTBlODM3OTUwNmQxMzU5ZGI1YzQyMzM5OTEzNmI3M2Y5ZGQ4YTp7Il9zZXNzaW9uX2V4cGlyeSI6OTk5OTk5OTksIl9hdXRoX3NpZ25faW5fbWFwcGluZ19pZCI6NDAsIl9hdXRoX3NpZ25faW5fYXV0aCI6ZmFsc2UsIl9hdXRoX3VzZXJfaWQiOiI0MCIsIl9hdXRoX3VzZXJfaGFzaCI6ImYzYmFmMDkzYTFkMDhkZmJkYjFmZTAzZGU2ZDU4N2NjZmNiNGI3NDEiLCJ1c2VyX2lkIjoxNywiYXV0aF90b2tlbiI6ImpvZzVwODMzYTFiOGFrdmd4aGxmaGV1YWlmbHIzdXJ4IiwiX2F1dGhfc2lnbl9pbl9pZCI6MTcsIl9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIn0=','2020-05-13 19:34:30.869420'),('jrxtio9nmdbe41k32oakibipersq79wn','ODE0MDQyNTk5ZTUyNjE5ZTk0ZjIzMzRiNTBhMmUwYTc3OGViODAxYTp7Il9hdXRoX3NpZ25faW5faWQiOjIwLCJfc2Vzc2lvbl9leHBpcnkiOjk5OTk5OTk5LCJfYXV0aF91c2VyX2hhc2giOiI4N2VlZTk5ZDM5NDVkYjgxN2E0Y2FiZDM0MWE1NzcyYWJkMTY0MTM2IiwidXNlcl9pZCI6MjAsIl9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfc2lnbl9pbl9hdXRoIjpmYWxzZSwiX2F1dGhfc2lnbl9pbl9tYXBwaW5nX2lkIjo0NSwiYXV0aF90b2tlbiI6ImpyeHRpbzlubWRiZTQxazMyb2FraWJpcGVyc3E3OXduIiwiX2F1dGhfdXNlcl9pZCI6IjQ1In0=','2020-05-22 19:00:43.943117'),('jukyhzugi44qqlrhipk5nltjflppae9p','ZmUwMmE5YmRkZWE1YmYyYTQ3NmRkYmExYjU1MjhlYzY2MmFmZTlmZjp7Il9hdXRoX3NpZ25faW5fbWFwcGluZ19pZCI6NDAsIl9hdXRoX3VzZXJfaGFzaCI6ImYzYmFmMDkzYTFkMDhkZmJkYjFmZTAzZGU2ZDU4N2NjZmNiNGI3NDEiLCJ1c2VyX2lkIjoxNywiX2F1dGhfc2lnbl9pbl9hdXRoIjpmYWxzZSwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJhdXRoX3Rva2VuIjoianVreWh6dWdpNDRxcWxyaGlwazVubHRqZmxwcGFlOXAiLCJfYXV0aF91c2VyX2lkIjoiNDAiLCJfYXV0aF9zaWduX2luX2lkIjoxNywiX3Nlc3Npb25fZXhwaXJ5Ijo5OTk5OTk5OX0=','2020-06-18 15:54:01.004070'),('k1ya3k74i93ml2r7ynwidrpn19tme458','ZDgwNGZmYTMxZDlkNzk2Y2I5OTg5NTE4M2E4YjAzNWIxMDkwODhiMDp7Il9hdXRoX3NpZ25faW5faWQiOjIwLCJfc2Vzc2lvbl9leHBpcnkiOjk5OTk5OTk5LCJfYXV0aF91c2VyX2hhc2giOiI4N2VlZTk5ZDM5NDVkYjgxN2E0Y2FiZDM0MWE1NzcyYWJkMTY0MTM2IiwidXNlcl9pZCI6MjAsIl9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfc2lnbl9pbl9hdXRoIjpmYWxzZSwiX2F1dGhfc2lnbl9pbl9tYXBwaW5nX2lkIjo0NSwiYXV0aF90b2tlbiI6ImsxeWEzazc0aTkzbWwycjd5bndpZHJwbjE5dG1lNDU4IiwiX2F1dGhfdXNlcl9pZCI6IjQ1In0=','2020-05-22 18:59:39.409117'),('k3ihkwen1mn22od0imuhk6lcioajoa5q','YWFiZDBiMGY4MWM2MzFiMjZlZmM4OTkzNGExMjYxMzlhZTIzNmNmYjp7Il9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfc2lnbl9pbl9tYXBwaW5nX2lkIjo0MCwiX2F1dGhfdXNlcl9pZCI6IjQwIiwiX3Nlc3Npb25fZXhwaXJ5Ijo5OTk5OTk5OSwiX2F1dGhfdXNlcl9oYXNoIjoiZjNiYWYwOTNhMWQwOGRmYmRiMWZlMDNkZTZkNTg3Y2NmY2I0Yjc0MSIsIl9hdXRoX3NpZ25faW5fYXV0aCI6ZmFsc2UsIl9hdXRoX3NpZ25faW5faWQiOjE3LCJhdXRoX3Rva2VuIjoiazNpaGt3ZW4xbW4yMm9kMGltdWhrNmxjaW9ham9hNXEiLCJ1c2VyX2lkIjoxN30=','2020-06-14 17:31:41.079829'),('k8rrhamkfcdlcwoflznrqynm82w6rezh','ZDg4MjYxMTk4MDBjN2IzMTFiN2E2YmJiYjMyZjFlYWZlMWRkOWY5MTp7Il9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9oYXNoIjoiODdlZWU5OWQzOTQ1ZGI4MTdhNGNhYmQzNDFhNTc3MmFiZDE2NDEzNiIsImF1dGhfdG9rZW4iOiJrOHJyaGFta2ZjZGxjd29mbHpucnF5bm04Mnc2cmV6aCIsIl9hdXRoX3VzZXJfaWQiOiI0NSIsIl9zZXNzaW9uX2V4cGlyeSI6OTk5OTk5OTksInVzZXJfaWQiOjIwLCJfYXV0aF9zaWduX2luX2lkIjoyMCwiX2F1dGhfc2lnbl9pbl9hdXRoIjpmYWxzZSwiX2F1dGhfc2lnbl9pbl9tYXBwaW5nX2lkIjo0NX0=','2020-05-29 19:10:17.234325'),('kbdlaz0yz8ip3wlpgep0nrnecp29humr','ZDVmMTgxNDhhNGZhZTUwODRhYzYyMjhmMGU1NjJmZmFlOTg4NmQ1ZTp7Il9hdXRoX3VzZXJfaGFzaCI6Ijg3ZWVlOTlkMzk0NWRiODE3YTRjYWJkMzQxYTU3NzJhYmQxNjQxMzYiLCJ1c2VyX2lkIjoyMCwiX2F1dGhfc2lnbl9pbl9tYXBwaW5nX2lkIjo0NSwiX2F1dGhfdXNlcl9pZCI6IjQ1IiwiX3Nlc3Npb25fZXhwaXJ5Ijo5OTk5OTk5OSwiX2F1dGhfc2lnbl9pbl9hdXRoIjpmYWxzZSwiX2F1dGhfc2lnbl9pbl9pZCI6MjAsIl9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiYXV0aF90b2tlbiI6ImtiZGxhejB5ejhpcDN3bHBnZXAwbnJuZWNwMjlodW1yIn0=','2020-05-17 18:29:29.899964'),('kf5z9e3rt3un6geidptojh1kajnnlspx','NWJkZDY5ZWJiZTUwMDc5ZjBiYzg5M2Y1ZDRjODg3NWZhNzQwNGE5ODp7InVzZXJfaWQiOjE4LCJfc2Vzc2lvbl9leHBpcnkiOjk5OTk5OTk5LCJhdXRoX3Rva2VuIjoia2Y1ejllM3J0M3VuNmdlaWRwdG9qaDFrYWpubmxzcHgiLCJfYXV0aF9zaWduX2luX2F1dGgiOmZhbHNlLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9hdXRoX3NpZ25faW5fbWFwcGluZ19pZCI6NDEsIl9hdXRoX3VzZXJfaWQiOiI0MSIsIl9hdXRoX3NpZ25faW5faWQiOjE4LCJfYXV0aF91c2VyX2hhc2giOiI0YzM4ZjgzNmU4YzE5MzIzYWQ1YzkzNzlkYzgxMmE5MzMwMTE5ZGI3In0=','2020-05-13 15:37:59.045337'),('kh27a1gna22bq9ilq9r16g9e6vkyfips','MWY3MjFkZmM3NWNlNDM5OTRjMmI0Mjc4MTVlOTNmYTA2ZTY5NTVhYjp7Il9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9pZCI6IjMzIiwidXNlcl9pZCI6MTMsIl9zZXNzaW9uX2V4cGlyeSI6OTk5OTk5OTksIl9hdXRoX3VzZXJfaGFzaCI6IjY0YjdkOGNjY2YyMmYyOGUxNzM2NDZmZmI1YmI2ZjViNzU5YzgwZDYiLCJfYXV0aF9zaWduX2luX2lkIjoxMywiX2F1dGhfc2lnbl9pbl9tYXBwaW5nX2lkIjozMywiX2F1dGhfc2lnbl9pbl9hdXRoIjpmYWxzZX0=','2020-05-13 13:31:41.103456'),('kjzawls1yqk27sd79y6fqm0rq8gzviq4','ZjBiYzVmY2I4OWUxM2M1MjI1MDQxNWYyYjRjOGIzM2ZjN2YzYjUzOTp7Il9zZXNzaW9uX2V4cGlyeSI6OTk5OTk5OTksIl9hdXRoX3VzZXJfaGFzaCI6ImYzYmFmMDkzYTFkMDhkZmJkYjFmZTAzZGU2ZDU4N2NjZmNiNGI3NDEiLCJfYXV0aF9zaWduX2luX21hcHBpbmdfaWQiOjQwLCJfYXV0aF91c2VyX2lkIjoiNDAiLCJfYXV0aF9zaWduX2luX2lkIjoxNywiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJfYXV0aF9zaWduX2luX2F1dGgiOmZhbHNlLCJhdXRoX3Rva2VuIjoia2p6YXdsczF5cWsyN3NkNzl5NmZxbTBycThnenZpcTQiLCJ1c2VyX2lkIjoxN30=','2020-05-15 16:28:02.245285'),('kpbar1m38xw9y6emunvvjjxlfdv1p3f3','MTQ3MGZmZDQ2ZDkxNTc5YjUwNmFjNjExODRjOTkxZmQ2Mjc5NTEwYzp7Il9hdXRoX3NpZ25faW5fbWFwcGluZ19pZCI6NDUsIl9zZXNzaW9uX2V4cGlyeSI6OTk5OTk5OTksIl9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9oYXNoIjoiODdlZWU5OWQzOTQ1ZGI4MTdhNGNhYmQzNDFhNTc3MmFiZDE2NDEzNiIsImF1dGhfdG9rZW4iOiJrcGJhcjFtMzh4dzl5NmVtdW52dmpqeGxmZHYxcDNmMyIsIl9hdXRoX3VzZXJfaWQiOiI0NSIsIl9hdXRoX3NpZ25faW5fYXV0aCI6ZmFsc2UsIl9hdXRoX3NpZ25faW5faWQiOjIwLCJ1c2VyX2lkIjoyMH0=','2020-05-21 16:12:52.221060'),('kslhpyprh2vum4eg5bmhkvhr16ej33io','MTMzYzhhNGNkODgwY2FjMzE1NDk2NzgyZTQ0MDg2N2JiNDU0N2EzZDp7ImF1dGhfdG9rZW4iOiJrc2xocHlwcmgydnVtNGVnNWJtaGt2aHIxNmVqMzNpbyIsIl9zZXNzaW9uX2V4cGlyeSI6OTk5OTk5OTksInVzZXJfaWQiOjIwLCJfYXV0aF9zaWduX2luX2F1dGgiOmZhbHNlLCJfYXV0aF9zaWduX2luX21hcHBpbmdfaWQiOjQ1LCJfYXV0aF9zaWduX2luX2lkIjoyMCwiX2F1dGhfdXNlcl9pZCI6IjQ1IiwiX2F1dGhfdXNlcl9oYXNoIjoiODdlZWU5OWQzOTQ1ZGI4MTdhNGNhYmQzNDFhNTc3MmFiZDE2NDEzNiIsIl9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIn0=','2020-05-27 17:25:52.866998'),('kx989yu1287xn64gkm7dbfnjvbyme445','ZDJhMGY4OWJkZGYyYzVhYmZkMDA5ZGQ1YjVhN2YxMzRlMDI3NGQ1NTp7Il9hdXRoX3NpZ25faW5fbWFwcGluZ19pZCI6NDUsIl9zZXNzaW9uX2V4cGlyeSI6OTk5OTk5OTksIl9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9oYXNoIjoiODdlZWU5OWQzOTQ1ZGI4MTdhNGNhYmQzNDFhNTc3MmFiZDE2NDEzNiIsImF1dGhfdG9rZW4iOiJreDk4OXl1MTI4N3huNjRna203ZGJmbmp2YnltZTQ0NSIsIl9hdXRoX3VzZXJfaWQiOiI0NSIsIl9hdXRoX3NpZ25faW5fYXV0aCI6ZmFsc2UsIl9hdXRoX3NpZ25faW5faWQiOjIwLCJ1c2VyX2lkIjoyMH0=','2020-05-21 16:26:18.501680'),('l2dtbcnnyxgl13buzbok7bg7ymr22u2b','MjJiODFkYTMzZTVhMWUyYjI2MzkyM2I3ODVmODBhZjZiMGViMTUxMDp7InVzZXJfaWQiOjE3LCJfc2Vzc2lvbl9leHBpcnkiOjk5OTk5OTk5LCJhdXRoX3Rva2VuIjoibDJkdGJjbm55eGdsMTNidXpib2s3Ymc3eW1yMjJ1MmIiLCJfYXV0aF9zaWduX2luX2F1dGgiOmZhbHNlLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9hdXRoX3NpZ25faW5fbWFwcGluZ19pZCI6NDAsIl9hdXRoX3VzZXJfaWQiOiI0MCIsIl9hdXRoX3NpZ25faW5faWQiOjE3LCJfYXV0aF91c2VyX2hhc2giOiJmM2JhZjA5M2ExZDA4ZGZiZGIxZmUwM2RlNmQ1ODdjY2ZjYjRiNzQxIn0=','2020-05-13 15:42:44.496879'),('l8zdvm7xvdty4z2ssuey0aouuk3bzp1s','NWI2YmQ3MjJlMzBhMWNhNjQ4Mzk1NGI1ODc3MzFlNDRiYzQxZDg5YTp7Il9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9oYXNoIjoiODdlZWU5OWQzOTQ1ZGI4MTdhNGNhYmQzNDFhNTc3MmFiZDE2NDEzNiIsImF1dGhfdG9rZW4iOiJsOHpkdm03eHZkdHk0ejJzc3VleTBhb3V1azNienAxcyIsIl9hdXRoX3VzZXJfaWQiOiI0NSIsIl9zZXNzaW9uX2V4cGlyeSI6OTk5OTk5OTksInVzZXJfaWQiOjIwLCJfYXV0aF9zaWduX2luX2lkIjoyMCwiX2F1dGhfc2lnbl9pbl9hdXRoIjpmYWxzZSwiX2F1dGhfc2lnbl9pbl9tYXBwaW5nX2lkIjo0NX0=','2020-05-29 18:54:33.247936'),('lckitx9y9x1g0jxt18exjtey37rd55zq','YTlkNTFkMzdlNzViZmQwZjUyY2EyNTgyODBjNDczZTI5MjQ3NjViZDp7Il9hdXRoX3VzZXJfaGFzaCI6Ijg3ZWVlOTlkMzk0NWRiODE3YTRjYWJkMzQxYTU3NzJhYmQxNjQxMzYiLCJ1c2VyX2lkIjoyMCwiX2F1dGhfc2lnbl9pbl9tYXBwaW5nX2lkIjo0NSwiX2F1dGhfdXNlcl9pZCI6IjQ1IiwiX3Nlc3Npb25fZXhwaXJ5Ijo5OTk5OTk5OSwiX2F1dGhfc2lnbl9pbl9hdXRoIjpmYWxzZSwiX2F1dGhfc2lnbl9pbl9pZCI6MjAsIl9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiYXV0aF90b2tlbiI6Imxja2l0eDl5OXgxZzBqeHQxOGV4anRleTM3cmQ1NXpxIn0=','2020-05-17 17:26:28.043001'),('lmnivjhtdcp87pcks2fg6qcvrkv22n9o','OTAyODc3ZTVlYTU4MWEzNjk5ZTM5Yzk0ZDVkMDFhMWI3MGE1NmRiMjp7Il9hdXRoX3VzZXJfaGFzaCI6IjRjMzhmODM2ZThjMTkzMjNhZDVjOTM3OWRjODEyYTkzMzAxMTlkYjciLCJfYXV0aF91c2VyX2lkIjoiNDEiLCJfYXV0aF9zaWduX2luX2lkIjoxOCwiX2F1dGhfc2lnbl9pbl9tYXBwaW5nX2lkIjo0MSwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJfc2Vzc2lvbl9leHBpcnkiOjk5OTk5OTk5LCJ1c2VyX2lkIjoxOCwiYXV0aF90b2tlbiI6Imxtbml2amh0ZGNwODdwY2tzMmZnNnFjdnJrdjIybjlvIiwiX2F1dGhfc2lnbl9pbl9hdXRoIjpmYWxzZX0=','2020-05-13 13:41:57.114880'),('lvw9xd2pk6f401ozdxkbgcnfa0ouwgdm','YTZkZjQzMzMyN2JkOWNkODY2N2IyNTM5MzJiNjIxZDAxY2VkM2YzMTp7Il9hdXRoX3NpZ25faW5faWQiOjE3LCJfYXV0aF9zaWduX2luX2F1dGgiOmZhbHNlLCJ1c2VyX2lkIjoxNywiX2F1dGhfdXNlcl9pZCI6IjQwIiwiX2F1dGhfdXNlcl9oYXNoIjoiZjNiYWYwOTNhMWQwOGRmYmRiMWZlMDNkZTZkNTg3Y2NmY2I0Yjc0MSIsIl9hdXRoX3NpZ25faW5fbWFwcGluZ19pZCI6NDAsIl9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiYXV0aF90b2tlbiI6Imx2dzl4ZDJwazZmNDAxb3pkeGtiZ2NuZmEwb3V3Z2RtIiwiX3Nlc3Npb25fZXhwaXJ5Ijo5OTk5OTk5OX0=','2020-05-14 11:57:29.223766'),('mgeq54061imlp4e5x2233jvgtgb4e7go','ZTA3YWM5ZDFhNDhkNjUxMjAwM2VkZGNkMGJmZDRjMDhiODgwMzZlODp7Il9zZXNzaW9uX2V4cGlyeSI6OTk5OTk5OTksIl9hdXRoX3VzZXJfaGFzaCI6Ijg3ZWVlOTlkMzk0NWRiODE3YTRjYWJkMzQxYTU3NzJhYmQxNjQxMzYiLCJfYXV0aF9zaWduX2luX21hcHBpbmdfaWQiOjQ1LCJfYXV0aF91c2VyX2lkIjoiNDUiLCJfYXV0aF9zaWduX2luX2lkIjoyMCwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJfYXV0aF9zaWduX2luX2F1dGgiOmZhbHNlLCJhdXRoX3Rva2VuIjoibWdlcTU0MDYxaW1scDRlNXgyMjMzanZndGdiNGU3Z28iLCJ1c2VyX2lkIjoyMH0=','2020-05-15 13:26:19.812612'),('mqadru23dg96fv0k1ta5bofbvcso3w74','YTllODQ2YWMwODk3MzY3Y2RkNDEzYWVhZmI1NjI5ODUzMzJjOGRhNTp7Il9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwidXNlcl9pZCI6MjAsIl9hdXRoX3NpZ25faW5fbWFwcGluZ19pZCI6NDUsIl9hdXRoX3NpZ25faW5fYXV0aCI6ZmFsc2UsIl9hdXRoX3VzZXJfaGFzaCI6Ijg3ZWVlOTlkMzk0NWRiODE3YTRjYWJkMzQxYTU3NzJhYmQxNjQxMzYiLCJfYXV0aF9zaWduX2luX2lkIjoyMCwiX2F1dGhfdXNlcl9pZCI6IjQ1IiwiYXV0aF90b2tlbiI6Im1xYWRydTIzZGc5NmZ2MGsxdGE1Ym9mYnZjc28zdzc0IiwiX3Nlc3Npb25fZXhwaXJ5Ijo5OTk5OTk5OX0=','2020-06-14 17:44:29.001356'),('mx2a9d8mq2o9iojqjhcx3wupvae7jhb4','NGQzZjMzNWM0M2E1MmFhYjQzYzUzOTBkYjU3YWQ3NzVhZjg3ZTllZTp7InVzZXJfaWQiOjE3LCJfc2Vzc2lvbl9leHBpcnkiOjk5OTk5OTk5LCJhdXRoX3Rva2VuIjoibXgyYTlkOG1xMm85aW9qcWpoY3gzd3VwdmFlN2poYjQiLCJfYXV0aF9zaWduX2luX2F1dGgiOmZhbHNlLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9hdXRoX3NpZ25faW5fbWFwcGluZ19pZCI6NDAsIl9hdXRoX3VzZXJfaWQiOiI0MCIsIl9hdXRoX3NpZ25faW5faWQiOjE3LCJfYXV0aF91c2VyX2hhc2giOiJmM2JhZjA5M2ExZDA4ZGZiZGIxZmUwM2RlNmQ1ODdjY2ZjYjRiNzQxIn0=','2020-05-13 17:51:57.048545'),('mz2r7syg40u0st6qo6pwx99u0qh0s5tw','OWI3NjFiODc4MDcxZmNiNGEzZjk1YzRjNDAwYmRjNzdkMjI2NDM4OTp7Il9zZXNzaW9uX2V4cGlyeSI6OTk5OTk5OTksIl9hdXRoX3NpZ25faW5fbWFwcGluZ19pZCI6NDAsIl9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfc2lnbl9pbl9hdXRoIjpmYWxzZSwiX2F1dGhfdXNlcl9pZCI6IjQwIiwidXNlcl9pZCI6MTcsImF1dGhfdG9rZW4iOiJtejJyN3N5ZzQwdTBzdDZxbzZwd3g5OXUwcWgwczV0dyIsIl9hdXRoX3NpZ25faW5faWQiOjE3LCJfYXV0aF91c2VyX2hhc2giOiJmM2JhZjA5M2ExZDA4ZGZiZGIxZmUwM2RlNmQ1ODdjY2ZjYjRiNzQxIn0=','2020-05-13 19:50:40.339357'),('n1e4q92m6bbmmxbi24q37z9gxjr5sibt','ODc2ZTA0YTRkOTMxMTY0Mjk3ZDAxMmZkY2ZhNTYyZDRmNzQwNGViYTp7Il9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfc2lnbl9pbl9hdXRoIjpmYWxzZSwidXNlcl9pZCI6MjAsIl9hdXRoX3VzZXJfaGFzaCI6Ijg3ZWVlOTlkMzk0NWRiODE3YTRjYWJkMzQxYTU3NzJhYmQxNjQxMzYiLCJhdXRoX3Rva2VuIjoibjFlNHE5Mm02YmJtbXhiaTI0cTM3ejlneGpyNXNpYnQiLCJfYXV0aF9zaWduX2luX2lkIjoyMCwiX2F1dGhfdXNlcl9pZCI6IjQ1IiwiX2F1dGhfc2lnbl9pbl9tYXBwaW5nX2lkIjo0NSwiX3Nlc3Npb25fZXhwaXJ5Ijo5OTk5OTk5OX0=','2020-05-14 13:17:31.911010'),('ne6j2b18bqophvm0h842xi6pd3v41hj8','ZTkwNWFiNTUzODFiMTllMTA0M2I1NTg0YzgyN2UzYjk4MjdkYWJkZDp7Il9hdXRoX3VzZXJfaGFzaCI6ImYzYmFmMDkzYTFkMDhkZmJkYjFmZTAzZGU2ZDU4N2NjZmNiNGI3NDEiLCJ1c2VyX2lkIjoxNywiX2F1dGhfc2lnbl9pbl9tYXBwaW5nX2lkIjo0MCwiX2F1dGhfdXNlcl9pZCI6IjQwIiwiX3Nlc3Npb25fZXhwaXJ5Ijo5OTk5OTk5OSwiX2F1dGhfc2lnbl9pbl9hdXRoIjpmYWxzZSwiX2F1dGhfc2lnbl9pbl9pZCI6MTcsIl9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiYXV0aF90b2tlbiI6Im5lNmoyYjE4YnFvcGh2bTBoODQyeGk2cGQzdjQxaGo4In0=','2020-05-17 18:00:21.607337'),('nhmrkl7amfe0alj21t165mm0nnhwffb9','ZjcxZWQ0MThhMjdjYWFkNjA5MjA2ZDljMTEzZTMyMmFkZDJjOTdlYTp7Il9hdXRoX3NpZ25faW5fYXV0aCI6ZmFsc2UsIl9hdXRoX3VzZXJfaWQiOiI0NSIsIl9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfc2lnbl9pbl9pZCI6MjAsIl9hdXRoX3VzZXJfaGFzaCI6Ijg3ZWVlOTlkMzk0NWRiODE3YTRjYWJkMzQxYTU3NzJhYmQxNjQxMzYiLCJhdXRoX3Rva2VuIjoibmhtcmtsN2FtZmUwYWxqMjF0MTY1bW0wbm5od2ZmYjkiLCJfc2Vzc2lvbl9leHBpcnkiOjk5OTk5OTk5LCJ1c2VyX2lkIjoyMCwiX2F1dGhfc2lnbl9pbl9tYXBwaW5nX2lkIjo0NX0=','2020-05-28 19:33:06.031446'),('nkrdh199shs9284wya4ehq5jnftd3e9s','NzVmYzg2YWU4M2U4Nzc0MTRkYmNiZTAzZjQ5ZjRjZGU0NGVkYmQ5Nzp7ImNtc19hdXRoX3NpZ25faW5faWQiOjEsImNtc19hdXRoX3NpZ25faW5fbWFwcGluZ19yb2xlIjoxLCJjbXNfYXV0aF9zaWduX2luX2F1dGgiOmZhbHNlLCJjbXNfYXV0aF9zaWduX2luX2N1cnJfcmVzX2lkIjoxLCJfYXV0aF91c2VyX2hhc2giOiI0M2QyNWVlZjQwMzMwNWMzYWRmYzRmNmM4ZjZlZjI4YWY5MGE0ZjZjIiwiY21zX2F1dGhfc2lnbl9pbl9tYXBwaW5nX2lkIjoiYWRtaW5tZW50ZUB0b3lvc3UuY29tIiwiY21zX2F1dGhfc2lnbl9pbl9tYXBwaW5nX3JvbGVfbmFtZSI6Ik1FTlRFIiwicmVzX2xpc3QiOlt7ImlkIjoxLCJuYW1lIjoiS2FwcG8gcm8gYW5uIn1dLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9hdXRoX3VzZXJfaWQiOiIyIiwiY21zX2F1dGhfc2lnbl9pbl9tYXBwaW5nX2ZuYW1lIjoiVG95b3N1In0=','2017-03-27 06:56:02.242527'),('nmri9bi4zwhxx3uij32vv9584gnx6aal','MDljZDA1MTgzNGFiMGFmYmM2YWU2ZGIwNGU3ZjU1YzU4OGVmNWZlMjp7Il9hdXRoX3VzZXJfaWQiOiI0MCIsImF1dGhfdG9rZW4iOiJubXJpOWJpNHp3aHh4M3VpajMydnY5NTg0Z254NmFhbCIsIl9hdXRoX3NpZ25faW5fbWFwcGluZ19pZCI6NDAsInVzZXJfaWQiOjE3LCJfYXV0aF9zaWduX2luX2F1dGgiOmZhbHNlLCJfYXV0aF9zaWduX2luX2lkIjoxNywiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJfc2Vzc2lvbl9leHBpcnkiOjk5OTk5OTk5LCJfYXV0aF91c2VyX2hhc2giOiJmM2JhZjA5M2ExZDA4ZGZiZGIxZmUwM2RlNmQ1ODdjY2ZjYjRiNzQxIn0=','2020-06-14 20:08:46.736532'),('nyheqeova8jtizorvcklfde1jtjckk2w','YjlhZWYyNjFhZjU0MmYwMzBhM2YwMTkxZWZmZGI4MDM5NzViNmQ5Mzp7InVzZXJfaWQiOjE4LCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsImF1dGhfdG9rZW4iOiJueWhlcWVvdmE4anRpem9ydmNrbGZkZTFqdGpja2sydyIsIl9hdXRoX3NpZ25faW5faWQiOjE4LCJfYXV0aF9zaWduX2luX2F1dGgiOmZhbHNlLCJfYXV0aF91c2VyX2hhc2giOiI0YzM4ZjgzNmU4YzE5MzIzYWQ1YzkzNzlkYzgxMmE5MzMwMTE5ZGI3IiwiX2F1dGhfc2lnbl9pbl9tYXBwaW5nX2lkIjo0MSwiX3Nlc3Npb25fZXhwaXJ5Ijo5OTk5OTk5OSwiX2F1dGhfdXNlcl9pZCI6IjQxIn0=','2020-05-13 15:25:48.464396'),('o5on4auest6eyna5jl451uffq6xvtr0b','ZTNjY2RkYjg0ZGJlZWRjZGUyZjI3MjM2NGM3ZWMxOTMyNjhhMTZlMjp7ImF1dGhfdG9rZW4iOiJvNW9uNGF1ZXN0NmV5bmE1amw0NTF1ZmZxNnh2dHIwYiIsIl9hdXRoX3VzZXJfaWQiOiI0NSIsIl9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfc2lnbl9pbl9pZCI6MjAsIl9hdXRoX3NpZ25faW5fbWFwcGluZ19pZCI6NDUsIl9zZXNzaW9uX2V4cGlyeSI6OTk5OTk5OTksInVzZXJfaWQiOjIwLCJfYXV0aF9zaWduX2luX2F1dGgiOmZhbHNlLCJfYXV0aF91c2VyX2hhc2giOiI4N2VlZTk5ZDM5NDVkYjgxN2E0Y2FiZDM0MWE1NzcyYWJkMTY0MTM2In0=','2020-05-28 19:19:49.474798'),('o8gngcekz8ijra5a9ahlhwvmkvaj0im3','N2JlZjc5OGY1NTVmNGRiOTcxMWFlNGIyOTVlMDRhM2ZmN2M2NmEzMTp7Il9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX3Nlc3Npb25fZXhwaXJ5Ijo5OTk5OTk5OSwiX2F1dGhfdXNlcl9oYXNoIjoiODdlZWU5OWQzOTQ1ZGI4MTdhNGNhYmQzNDFhNTc3MmFiZDE2NDEzNiIsIl9hdXRoX3NpZ25faW5faWQiOjIwLCJfYXV0aF9zaWduX2luX2F1dGgiOmZhbHNlLCJhdXRoX3Rva2VuIjoibzhnbmdjZWt6OGlqcmE1YTlhaGxod3Zta3ZhajBpbTMiLCJfYXV0aF91c2VyX2lkIjoiNDUiLCJ1c2VyX2lkIjoyMCwiX2F1dGhfc2lnbl9pbl9tYXBwaW5nX2lkIjo0NX0=','2020-05-22 13:44:52.549313'),('oagtmk8w0hhgqnebti54qmpobbx84pt2','M2M5MzFkZGMzN2I2YzFjNWQ5NmUyNDI5ZjA1NjcxZTIwOGIyOGY0Njp7Il9zZXNzaW9uX2V4cGlyeSI6OTk5OTk5OTksIl9hdXRoX3VzZXJfaGFzaCI6ImYzYmFmMDkzYTFkMDhkZmJkYjFmZTAzZGU2ZDU4N2NjZmNiNGI3NDEiLCJhdXRoX3Rva2VuIjoib2FndG1rOHcwaGhncW5lYnRpNTRxbXBvYmJ4ODRwdDIiLCJfYXV0aF9zaWduX2luX2F1dGgiOmZhbHNlLCJfYXV0aF91c2VyX2lkIjoiNDAiLCJfYXV0aF9zaWduX2luX2lkIjoxNywidXNlcl9pZCI6MTcsIl9hdXRoX3NpZ25faW5fbWFwcGluZ19pZCI6NDAsIl9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIn0=','2020-05-13 20:02:20.681305'),('oecbkvtl9wlgcnr915trmcy2ke2ivmo6','ZTUyNGI3ODg1NzkzOGI3MTRkZjVkYzgzMDQ5YTgxMDk5ZTVlYzQxOTp7Il9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfc2lnbl9pbl9pZCI6MjAsIl9hdXRoX3NpZ25faW5fbWFwcGluZ19pZCI6NDUsIl9hdXRoX3VzZXJfaGFzaCI6Ijg3ZWVlOTlkMzk0NWRiODE3YTRjYWJkMzQxYTU3NzJhYmQxNjQxMzYiLCJfYXV0aF9zaWduX2luX2F1dGgiOmZhbHNlLCJfYXV0aF91c2VyX2lkIjoiNDUiLCJhdXRoX3Rva2VuIjoib2VjYmt2dGw5d2xnY25yOTE1dHJtY3kya2UyaXZtbzYiLCJ1c2VyX2lkIjoyMCwiX3Nlc3Npb25fZXhwaXJ5Ijo5OTk5OTk5OX0=','2020-05-23 16:19:19.709701'),('ogjzclvyilij2gc1asrfkr263j8ulfsa','M2Y0NjA2MTZjYzQxNDVmNmM4ZTEzN2U4M2I0MDQ3OTc4MjA4NDM4ZDp7Il9zZXNzaW9uX2V4cGlyeSI6OTk5OTk5OTksIl9hdXRoX3VzZXJfaGFzaCI6Ijg3ZWVlOTlkMzk0NWRiODE3YTRjYWJkMzQxYTU3NzJhYmQxNjQxMzYiLCJfYXV0aF9zaWduX2luX21hcHBpbmdfaWQiOjQ1LCJfYXV0aF91c2VyX2lkIjoiNDUiLCJfYXV0aF9zaWduX2luX2lkIjoyMCwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJfYXV0aF9zaWduX2luX2F1dGgiOmZhbHNlLCJhdXRoX3Rva2VuIjoib2dqemNsdnlpbGlqMmdjMWFzcmZrcjI2M2o4dWxmc2EiLCJ1c2VyX2lkIjoyMH0=','2020-05-15 13:28:24.061612'),('p2zado68iyltyl9rzv0gs4xm8wclh0h3','NGY2NDRlODQ4OGY4ZTI4MDgwNGIzZjU0ZmU3YWZkZDU4NzEzYWZjNzp7Il9hdXRoX3VzZXJfaGFzaCI6IjY0YjdkOGNjY2YyMmYyOGUxNzM2NDZmZmI1YmI2ZjViNzU5YzgwZDYiLCJfYXV0aF9zaWduX2luX2lkIjoxMywidXNlcl9pZCI6MTMsIl9hdXRoX3NpZ25faW5fYXV0aCI6ZmFsc2UsIl9hdXRoX3VzZXJfaWQiOiIzMyIsIl9hdXRoX3NpZ25faW5fbWFwcGluZ19pZCI6MzMsIl9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiYXV0aF90b2tlbiI6InAyemFkbzY4aXlsdHlsOXJ6djBnczR4bTh3Y2xoMGgzIiwiX3Nlc3Npb25fZXhwaXJ5Ijo5OTk5OTk5OX0=','2020-05-13 13:58:05.645164'),('p3mqa3g1wrrrry07ejci4i2nelsi78r9','ZjYwNzZmZDg5MzAyOWE5ODM1Y2E1MmExZGVlMDI0NjRiZWY0ZWYzODp7Il9hdXRoX3VzZXJfaGFzaCI6IjY0YjdkOGNjY2YyMmYyOGUxNzM2NDZmZmI1YmI2ZjViNzU5YzgwZDYiLCJfYXV0aF9zaWduX2luX2lkIjoxMywidXNlcl9pZCI6MTMsIl9hdXRoX3NpZ25faW5fYXV0aCI6ZmFsc2UsIl9hdXRoX3VzZXJfaWQiOiIzMyIsIl9hdXRoX3NpZ25faW5fbWFwcGluZ19pZCI6MzMsIl9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiYXV0aF90b2tlbiI6InAzbXFhM2cxd3JycnJ5MDdlamNpNGkybmVsc2k3OHI5IiwiX3Nlc3Npb25fZXhwaXJ5Ijo5OTk5OTk5OX0=','2020-05-13 14:02:29.162368'),('pd0zu105elqo8qs7gkr9w1ixa8v7i17m','NWI5ZjcwOGIxZWQzZDc1OGQ2ZTJiYmEyNmViMjU1YjYzMTA1ZWJkMzp7Il9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9pZCI6IjIiLCJyZXNfbGlzdCI6W3sibmFtZSI6IkthcHBvIHJvIGFubiAtIFRPWU9TVSIsImlkIjoxfSx7Im5hbWUiOiJLYXBwbyBybyBhbm4gLSBQUk9EIiwiaWQiOjJ9XSwiY21zX2F1dGhfc2lnbl9pbl9tYXBwaW5nX2ZuYW1lIjoiSGlnbyIsImNtc19hdXRoX3NpZ25faW5fbWFwcGluZ19yb2xlIjoxLCJjbXNfYXV0aF9zaWduX2luX21hcHBpbmdfcm9sZV9uYW1lIjoiTUVOVEUiLCJjbXNfYXV0aF9zaWduX2luX21hcHBpbmdfaWQiOiJhZG1pbm1lbnRlQHRveW9zdS5jb20iLCJjbXNfYXV0aF9zaWduX2luX2lkIjoxLCJjbXNfYXV0aF9zaWduX2luX2F1dGgiOmZhbHNlLCJfYXV0aF91c2VyX2hhc2giOiI0M2QyNWVlZjQwMzMwNWMzYWRmYzRmNmM4ZjZlZjI4YWY5MGE0ZjZjIiwiY21zX2F1dGhfc2lnbl9pbl9jdXJyX3Jlc19pZCI6MX0=','2017-05-03 08:51:48.072588'),('piaegkow7jy724vawh8cmxfy2ynqq5i2','NDY5NGQ2ZmU1NDcwZDkyNWFmZmIxNzBmNjM2NWJlMWVlNGZhMWYwYjp7Il9hdXRoX3NpZ25faW5fbWFwcGluZ19pZCI6NDAsInVzZXJfaWQiOjE3LCJfYXV0aF9zaWduX2luX2F1dGgiOmZhbHNlLCJhdXRoX3Rva2VuIjoicGlhZWdrb3c3ank3MjR2YXdoOGNteGZ5MnlucXE1aTIiLCJfYXV0aF9zaWduX2luX2lkIjoxNywiX2F1dGhfdXNlcl9oYXNoIjoiZjNiYWYwOTNhMWQwOGRmYmRiMWZlMDNkZTZkNTg3Y2NmY2I0Yjc0MSIsIl9zZXNzaW9uX2V4cGlyeSI6OTk5OTk5OTksIl9hdXRoX3VzZXJfaWQiOiI0MCIsIl9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIn0=','2020-06-07 20:24:38.437253'),('pim7doa33apnt6emjlvmtmg8zdsghd9j','OTlkYTU2YTljNTU4ZjFlMGIxOWEwZGNiZGVkODZhZDcwNzY4YzM4Yzp7Il9zZXNzaW9uX2V4cGlyeSI6OTk5OTk5OTksIl9hdXRoX3VzZXJfaGFzaCI6Ijg3ZWVlOTlkMzk0NWRiODE3YTRjYWJkMzQxYTU3NzJhYmQxNjQxMzYiLCJfYXV0aF91c2VyX2lkIjoiNDUiLCJ1c2VyX2lkIjoyMCwiYXV0aF90b2tlbiI6InBpbTdkb2EzM2FwbnQ2ZW1qbHZtdG1nOHpkc2doZDlqIiwiX2F1dGhfc2lnbl9pbl9pZCI6MjAsIl9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfc2lnbl9pbl9hdXRoIjpmYWxzZSwiX2F1dGhfc2lnbl9pbl9tYXBwaW5nX2lkIjo0NX0=','2020-05-31 20:05:55.047159'),('pqqvy1jfjq2hnbcu368jsrrnnwa9i68q','ZmY4MDI5MDJkYmQ4MzUzYjM1OTg2OWQ3OGFiOTU4MjZkMDhlNTYyYjp7fQ==','2017-04-07 09:17:28.370483'),('q4vs9pto8zrv7ajm4otr4lij99q14kqk','ZTMwYTRhMTVkMGZjNGIzZDZkMzMwOWMzZjk0ZjU5OGIwYzA0ZDU5Njp7ImF1dGhfdG9rZW4iOiJxNHZzOXB0bzh6cnY3YWptNG90cjRsaWo5OXExNGtxayIsIl9hdXRoX3VzZXJfaWQiOiI0MCIsIl9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfc2lnbl9pbl9tYXBwaW5nX2lkIjo0MCwiX2F1dGhfdXNlcl9oYXNoIjoiZjNiYWYwOTNhMWQwOGRmYmRiMWZlMDNkZTZkNTg3Y2NmY2I0Yjc0MSIsIl9hdXRoX3NpZ25faW5fYXV0aCI6ZmFsc2UsIl9hdXRoX3NpZ25faW5faWQiOjE3LCJ1c2VyX2lkIjoxNywiX3Nlc3Npb25fZXhwaXJ5Ijo5OTk5OTk5OX0=','2020-06-20 15:42:26.857357'),('qczr22wsyovy42xouq4yg8l8p349c17f','NmMxYzA3ZjNlOTI4ZGI0NzBhMjQ2ZmY5YWE5MTk2NmZmZDkzMTNiYzp7ImF1dGhfdG9rZW4iOiJxY3pyMjJ3c3lvdnk0MnhvdXE0eWc4bDhwMzQ5YzE3ZiIsIl9hdXRoX3VzZXJfaWQiOiI1NiIsIl9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfc2lnbl9pbl9tYXBwaW5nX2lkIjo1NiwiX2F1dGhfdXNlcl9oYXNoIjoiZGY2Y2M5OWMwZTRhNWEzZGVjYzNlZmQ2NTk3MDdlNWNlMDFmNDVhZSIsIl9hdXRoX3NpZ25faW5fYXV0aCI6ZmFsc2UsIl9hdXRoX3NpZ25faW5faWQiOjM0LCJ1c2VyX2lkIjozNCwiX3Nlc3Npb25fZXhwaXJ5Ijo5OTk5OTk5OX0=','2020-06-20 12:06:18.099781'),('qf7u4mrohli24kat2uf61q6eme10ga2m','MWJhMmEwMTY2NzVlM2Q1MWJhYzQ1NmIwZTkwODM0ZGVjMjIxMjJlMzp7Il9hdXRoX3VzZXJfaGFzaCI6Ijg3ZWVlOTlkMzk0NWRiODE3YTRjYWJkMzQxYTU3NzJhYmQxNjQxMzYiLCJ1c2VyX2lkIjoyMCwiX2F1dGhfc2lnbl9pbl9tYXBwaW5nX2lkIjo0NSwiX2F1dGhfdXNlcl9pZCI6IjQ1IiwiX3Nlc3Npb25fZXhwaXJ5Ijo5OTk5OTk5OSwiX2F1dGhfc2lnbl9pbl9hdXRoIjpmYWxzZSwiX2F1dGhfc2lnbl9pbl9pZCI6MjAsIl9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiYXV0aF90b2tlbiI6InFmN3U0bXJvaGxpMjRrYXQydWY2MXE2ZW1lMTBnYTJtIn0=','2020-05-17 17:56:46.656844'),('qfnn8wpmh1dglawa9k3rtna37wvycdfh','N2E5MjEyZjE4MjA1NDBkNjhjYmJiZjZhZGM4ZjQyNzhmOTRjNWQwNjp7Il9zZXNzaW9uX2V4cGlyeSI6OTk5OTk5OTksIl9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfc2lnbl9pbl9tYXBwaW5nX2lkIjo0NSwiX2F1dGhfdXNlcl9pZCI6IjQ1IiwiX2F1dGhfc2lnbl9pbl9pZCI6MjAsIl9hdXRoX3VzZXJfaGFzaCI6Ijg3ZWVlOTlkMzk0NWRiODE3YTRjYWJkMzQxYTU3NzJhYmQxNjQxMzYiLCJfYXV0aF9zaWduX2luX2F1dGgiOmZhbHNlLCJhdXRoX3Rva2VuIjoicWZubjh3cG1oMWRnbGF3YTlrM3J0bmEzN3d2eWNkZmgiLCJ1c2VyX2lkIjoyMH0=','2020-05-15 16:39:06.260689'),('qk2fmkwde6bwwkh6zt4if4molmb4ypr9','MjgzY2VjZjhjNjZjMWYzYmYyNTQzMjhiODk0NDQ0ZjdiYzIxNGVmODp7Il9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfc2lnbl9pbl9pZCI6MjAsIl9zZXNzaW9uX2V4cGlyeSI6OTk5OTk5OTksImF1dGhfdG9rZW4iOiJxazJmbWt3ZGU2Ynd3a2g2enQ0aWY0bW9sbWI0eXByOSIsIl9hdXRoX3VzZXJfaGFzaCI6Ijg3ZWVlOTlkMzk0NWRiODE3YTRjYWJkMzQxYTU3NzJhYmQxNjQxMzYiLCJfYXV0aF9zaWduX2luX21hcHBpbmdfaWQiOjQ1LCJ1c2VyX2lkIjoyMCwiX2F1dGhfdXNlcl9pZCI6IjQ1IiwiX2F1dGhfc2lnbl9pbl9hdXRoIjpmYWxzZX0=','2020-06-10 19:11:12.890267'),('qllhut1q363d5uwvzdphcg6ovsjje5ss','MTMzY2QyZTZhNDFlMzkzOTQwMDkwM2M3Nzc5Mzc5YzNmNmMxYzIwYTp7Il9hdXRoX3NpZ25faW5fbWFwcGluZ19pZCI6NDUsIl9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfc2lnbl9pbl9hdXRoIjpmYWxzZSwiX2F1dGhfdXNlcl9pZCI6IjQ1IiwiX2F1dGhfdXNlcl9oYXNoIjoiODdlZWU5OWQzOTQ1ZGI4MTdhNGNhYmQzNDFhNTc3MmFiZDE2NDEzNiIsImF1dGhfdG9rZW4iOiJxbGxodXQxcTM2M2Q1dXd2emRwaGNnNm92c2pqZTVzcyIsInVzZXJfaWQiOjIwLCJfYXV0aF9zaWduX2luX2lkIjoyMCwiX3Nlc3Npb25fZXhwaXJ5Ijo5OTk5OTk5OX0=','2020-05-22 18:51:21.360117'),('qxbezls8qkinpty9yutpwnull3x250sm','ZmZhN2Q4NTEzNzgxNjliMTdiMGZiMjQ3MTcyNTA1M2I1NGI0Y2QyZjp7Il9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfc2lnbl9pbl9pZCI6MjAsIl9hdXRoX3NpZ25faW5fbWFwcGluZ19pZCI6NDUsIl9hdXRoX3VzZXJfaGFzaCI6Ijg3ZWVlOTlkMzk0NWRiODE3YTRjYWJkMzQxYTU3NzJhYmQxNjQxMzYiLCJfYXV0aF9zaWduX2luX2F1dGgiOmZhbHNlLCJfYXV0aF91c2VyX2lkIjoiNDUiLCJhdXRoX3Rva2VuIjoicXhiZXpsczhxa2lucHR5OXl1dHB3bnVsbDN4MjUwc20iLCJ1c2VyX2lkIjoyMCwiX3Nlc3Npb25fZXhwaXJ5Ijo5OTk5OTk5OX0=','2020-05-23 15:57:45.118446'),('qzjkda4o1k1mciw5dkessp2a3guv42yd','N2U5ZDgzZWI5MzA4Yjg0M2ZkYmU1YTZkOTNlOGYyMTlhZWY2NjgzZDp7Il9hdXRoX3NpZ25faW5fYXV0aCI6ZmFsc2UsInVzZXJfaWQiOjIwLCJfYXV0aF91c2VyX2lkIjoiNDUiLCJfYXV0aF91c2VyX2hhc2giOiI4N2VlZTk5ZDM5NDVkYjgxN2E0Y2FiZDM0MWE1NzcyYWJkMTY0MTM2IiwiX3Nlc3Npb25fZXhwaXJ5Ijo5OTk5OTk5OSwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJfYXV0aF9zaWduX2luX2lkIjoyMCwiX2F1dGhfc2lnbl9pbl9tYXBwaW5nX2lkIjo0NSwiYXV0aF90b2tlbiI6InF6amtkYTRvMWsxbWNpdzVka2Vzc3AyYTNndXY0MnlkIn0=','2020-06-18 12:17:28.900618'),('r4yqwbebrgdvq84eera3hxpi89spf7v9','YTc2YzVlYzVjOGI1OTRkNmI0NDZjMDljZWIxMDAwZTAzZDk1MzY1Mjp7Il9hdXRoX3VzZXJfaGFzaCI6Ijg3ZWVlOTlkMzk0NWRiODE3YTRjYWJkMzQxYTU3NzJhYmQxNjQxMzYiLCJ1c2VyX2lkIjoyMCwiX2F1dGhfc2lnbl9pbl9tYXBwaW5nX2lkIjo0NSwiX2F1dGhfdXNlcl9pZCI6IjQ1IiwiX3Nlc3Npb25fZXhwaXJ5Ijo5OTk5OTk5OSwiX2F1dGhfc2lnbl9pbl9hdXRoIjpmYWxzZSwiX2F1dGhfc2lnbl9pbl9pZCI6MjAsIl9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiYXV0aF90b2tlbiI6InI0eXF3YmVicmdkdnE4NGVlcmEzaHhwaTg5c3BmN3Y5In0=','2020-05-17 17:30:04.311625'),('rg31wa54drq76f4sdhikam6iws9t7wfa','YWUyNDY4ODMzMmY3MTNkNTQyM2ZjMDJmNTI1N2MyY2Y1ZGU0NzIxYTp7Il9hdXRoX3VzZXJfaGFzaCI6IjY0YjdkOGNjY2YyMmYyOGUxNzM2NDZmZmI1YmI2ZjViNzU5YzgwZDYiLCJfYXV0aF9zaWduX2luX2lkIjoxMywidXNlcl9pZCI6MTMsIl9hdXRoX3NpZ25faW5fYXV0aCI6ZmFsc2UsIl9hdXRoX3VzZXJfaWQiOiIzMyIsIl9hdXRoX3NpZ25faW5fbWFwcGluZ19pZCI6MzMsIl9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiYXV0aF90b2tlbiI6InJnMzF3YTU0ZHJxNzZmNHNkaGlrYW02aXdzOXQ3d2ZhIiwiX3Nlc3Npb25fZXhwaXJ5Ijo5OTk5OTk5OX0=','2020-05-13 14:13:01.950641'),('rim05jexzkh7uhgx3sg4ibvmkkbd779x','MGNkYTk4NmMzZjhjMTkwYjI0ZTBjMmMwNDA0OWYzNjYzZmViOTQ0Mzp7Il9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfc2lnbl9pbl9hdXRoIjpmYWxzZSwidXNlcl9pZCI6MjAsIl9hdXRoX3VzZXJfaGFzaCI6Ijg3ZWVlOTlkMzk0NWRiODE3YTRjYWJkMzQxYTU3NzJhYmQxNjQxMzYiLCJhdXRoX3Rva2VuIjoicmltMDVqZXh6a2g3dWhneDNzZzRpYnZta2tiZDc3OXgiLCJfYXV0aF9zaWduX2luX2lkIjoyMCwiX2F1dGhfdXNlcl9pZCI6IjQ1IiwiX2F1dGhfc2lnbl9pbl9tYXBwaW5nX2lkIjo0NSwiX3Nlc3Npb25fZXhwaXJ5Ijo5OTk5OTk5OX0=','2020-05-14 13:50:39.739782'),('rjghfs4b0wkcfp8vv55blrptc8i1wo7u','MGE2MmQyZDczNGU3OWYxOTY4OTkzMTA5ZTY3ZWFlZDRiNjMwNWUzYjp7Il9zZXNzaW9uX2V4cGlyeSI6OTk5OTk5OTksIl9hdXRoX3NpZ25faW5fbWFwcGluZ19pZCI6NDUsInVzZXJfaWQiOjIwLCJfYXV0aF91c2VyX2hhc2giOiI4N2VlZTk5ZDM5NDVkYjgxN2E0Y2FiZDM0MWE1NzcyYWJkMTY0MTM2IiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJfYXV0aF91c2VyX2lkIjoiNDUiLCJfYXV0aF9zaWduX2luX2F1dGgiOmZhbHNlLCJhdXRoX3Rva2VuIjoicmpnaGZzNGIwd2tjZnA4dnY1NWJscnB0YzhpMXdvN3UiLCJfYXV0aF9zaWduX2luX2lkIjoyMH0=','2020-06-12 17:39:22.416488'),('rjx7t7xesxf1fqv0f9uunlr2myvns6sp','OTM5MmY1ZjgzMDE1MTQ5ZGFkZGRhOGRlNDA4YjMyMDA1ODY0MjNmODp7Il9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9pZCI6IjMzIiwiYXV0aF90b2tlbiI6InJqeDd0N3hlc3hmMWZxdjBmOXV1bmxyMm15dm5zNnNwIiwidXNlcl9pZCI6MTMsIl9zZXNzaW9uX2V4cGlyeSI6OTk5OTk5OTksIl9hdXRoX3VzZXJfaGFzaCI6IjY0YjdkOGNjY2YyMmYyOGUxNzM2NDZmZmI1YmI2ZjViNzU5YzgwZDYiLCJfYXV0aF9zaWduX2luX2lkIjoxMywiX2F1dGhfc2lnbl9pbl9tYXBwaW5nX2lkIjozMywiX2F1dGhfc2lnbl9pbl9hdXRoIjpmYWxzZX0=','2020-05-13 13:26:42.275576'),('ry5ok821ozla1pm664uw3bdgnntmltru','ZWU4Mzc2OGIxMTZjODQzNjgzMDgxZjhlNmNkMzViNzdkYWY0NTE5Yzp7Il9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfc2lnbl9pbl9hdXRoIjpmYWxzZSwidXNlcl9pZCI6MTcsIl9hdXRoX3VzZXJfaGFzaCI6ImYzYmFmMDkzYTFkMDhkZmJkYjFmZTAzZGU2ZDU4N2NjZmNiNGI3NDEiLCJhdXRoX3Rva2VuIjoicnk1b2s4MjFvemxhMXBtNjY0dXczYmRnbm50bWx0cnUiLCJfYXV0aF9zaWduX2luX2lkIjoxNywiX2F1dGhfdXNlcl9pZCI6IjQwIiwiX2F1dGhfc2lnbl9pbl9tYXBwaW5nX2lkIjo0MCwiX3Nlc3Npb25fZXhwaXJ5Ijo5OTk5OTk5OX0=','2020-05-14 16:28:52.098970'),('s0xaec1nyl5pd9iits44kk9lghzo0p8f','MmU4YmZkMjIxNDg3ZjBhODI0M2JmMzUyYTA4YWUwZjBlZjRkZTQ2Zjp7Il9hdXRoX3VzZXJfaGFzaCI6Ijg3ZWVlOTlkMzk0NWRiODE3YTRjYWJkMzQxYTU3NzJhYmQxNjQxMzYiLCJ1c2VyX2lkIjoyMCwiX2F1dGhfc2lnbl9pbl9tYXBwaW5nX2lkIjo0NSwiX2F1dGhfdXNlcl9pZCI6IjQ1IiwiX3Nlc3Npb25fZXhwaXJ5Ijo5OTk5OTk5OSwiX2F1dGhfc2lnbl9pbl9hdXRoIjpmYWxzZSwiX2F1dGhfc2lnbl9pbl9pZCI6MjAsIl9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiYXV0aF90b2tlbiI6InMweGFlYzFueWw1cGQ5aWl0czQ0a2s5bGdoem8wcDhmIn0=','2020-05-17 18:33:00.837273'),('s7d76bsyenus4j3ke9pfhcsnivus7gdb','YzJlNjYxOTlmMWFmY2JlNGFkYTE1ZTRiY2Q3NGFkNzA5NjVjNWMyZDp7Il9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9oYXNoIjoiZjNiYWYwOTNhMWQwOGRmYmRiMWZlMDNkZTZkNTg3Y2NmY2I0Yjc0MSIsIl9hdXRoX3NpZ25faW5fbWFwcGluZ19pZCI6NDAsIl9hdXRoX3NpZ25faW5faWQiOjE3LCJ1c2VyX2lkIjoxNywiX3Nlc3Npb25fZXhwaXJ5Ijo5OTk5OTk5OSwiX2F1dGhfdXNlcl9pZCI6IjQwIiwiX2F1dGhfc2lnbl9pbl9hdXRoIjpmYWxzZSwiYXV0aF90b2tlbiI6InM3ZDc2YnN5ZW51czRqM2tlOXBmaGNzbml2dXM3Z2RiIn0=','2020-05-30 17:27:13.789937'),('sbj9urnt1bmo35ufja7bdq27xzzol7gp','YTJlZGM4ZGQyMDAwMjU0YjJlNTU5NDEzOThlYTc2NGQ0NzYyZTczZjp7ImF1dGhfdG9rZW4iOiJzYmo5dXJudDFibW8zNXVmamE3YmRxMjd4enpvbDdncCIsIl9hdXRoX3NpZ25faW5fYXV0aCI6ZmFsc2UsInVzZXJfaWQiOjIwLCJfYXV0aF91c2VyX2hhc2giOiI4N2VlZTk5ZDM5NDVkYjgxN2E0Y2FiZDM0MWE1NzcyYWJkMTY0MTM2IiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJfYXV0aF9zaWduX2luX2lkIjoyMCwiX2F1dGhfdXNlcl9pZCI6IjQ1IiwiX2F1dGhfc2lnbl9pbl9tYXBwaW5nX2lkIjo0NSwiX3Nlc3Npb25fZXhwaXJ5Ijo5OTk5OTk5OX0=','2020-05-14 12:33:30.230720'),('sifboy2p4wni8njlgyg3xm4oqq0t9ucu','ZmUxYTY5YjdlZDhmMWE2N2QxZGMzYTgxZWUzZjdiZTJhYzRiMjExNDp7InVzZXJfaWQiOjE3LCJfc2Vzc2lvbl9leHBpcnkiOjk5OTk5OTk5LCJhdXRoX3Rva2VuIjoic2lmYm95MnA0d25pOG5qbGd5ZzN4bTRvcXEwdDl1Y3UiLCJfYXV0aF9zaWduX2luX2F1dGgiOmZhbHNlLCJfYXV0aF91c2VyX2hhc2giOiJmM2JhZjA5M2ExZDA4ZGZiZGIxZmUwM2RlNmQ1ODdjY2ZjYjRiNzQxIiwiX2F1dGhfc2lnbl9pbl9tYXBwaW5nX2lkIjo0MCwiX2F1dGhfc2lnbl9pbl9pZCI6MTcsIl9hdXRoX3VzZXJfaWQiOiI0MCIsIl9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIn0=','2020-05-13 17:39:18.458545'),('sk7fxi2sfpz8adxcctqc0pnf4z3q017j','OTY5NDU4OGZhNzMzZTg1MTExNTBjYmY2MWY1YjIyNGFlNmFlOGUxNTp7Il9hdXRoX3VzZXJfaGFzaCI6ImYzYmFmMDkzYTFkMDhkZmJkYjFmZTAzZGU2ZDU4N2NjZmNiNGI3NDEiLCJ1c2VyX2lkIjoxNywiX2F1dGhfc2lnbl9pbl9tYXBwaW5nX2lkIjo0MCwiX2F1dGhfdXNlcl9pZCI6IjQwIiwiX3Nlc3Npb25fZXhwaXJ5Ijo5OTk5OTk5OSwiX2F1dGhfc2lnbl9pbl9hdXRoIjpmYWxzZSwiX2F1dGhfc2lnbl9pbl9pZCI6MTcsIl9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiYXV0aF90b2tlbiI6InNrN2Z4aTJzZnB6OGFkeGNjdHFjMHBuZjR6M3EwMTdqIn0=','2020-05-17 17:56:49.876166'),('ssjarennme32d9q611i5sohvz1zhp2id','ZjU5NmJmYTAwNTdlYjAwNDQxZDFiNGE0NGZmZWI0OGZkN2RkMDE3MTp7Il9zZXNzaW9uX2V4cGlyeSI6OTk5OTk5OTksIl9hdXRoX3NpZ25faW5fbWFwcGluZ19pZCI6NDAsIl9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfc2lnbl9pbl9hdXRoIjpmYWxzZSwiX2F1dGhfdXNlcl9pZCI6IjQwIiwidXNlcl9pZCI6MTcsImF1dGhfdG9rZW4iOiJzc2phcmVubm1lMzJkOXE2MTFpNXNvaHZ6MXpocDJpZCIsIl9hdXRoX3NpZ25faW5faWQiOjE3LCJfYXV0aF91c2VyX2hhc2giOiJmM2JhZjA5M2ExZDA4ZGZiZGIxZmUwM2RlNmQ1ODdjY2ZjYjRiNzQxIn0=','2020-05-13 19:40:26.897019'),('swask6ohljb74mwdz4smd8h0njopxtf5','YTZiYjczMjIwNDNlNmJlMzljNjE5Y2I5OWM5Y2RmODZjZDVjZmJlZjp7Il9zZXNzaW9uX2V4cGlyeSI6OTk5OTk5OTksIl9hdXRoX3VzZXJfaGFzaCI6ImYzYmFmMDkzYTFkMDhkZmJkYjFmZTAzZGU2ZDU4N2NjZmNiNGI3NDEiLCJfYXV0aF9zaWduX2luX21hcHBpbmdfaWQiOjQwLCJfYXV0aF91c2VyX2lkIjoiNDAiLCJfYXV0aF9zaWduX2luX2lkIjoxNywiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJfYXV0aF9zaWduX2luX2F1dGgiOmZhbHNlLCJhdXRoX3Rva2VuIjoic3dhc2s2b2hsamI3NG13ZHo0c21kOGgwbmpvcHh0ZjUiLCJ1c2VyX2lkIjoxN30=','2020-05-15 11:47:00.459407'),('sympf117026l9ff9oosuignbpvh34j5h','OTk1ZTViODIzZmM5YTg5YTBlODVkYzAzMTJhMjViMzY1Y2QwODk1MDp7ImF1dGhfdG9rZW4iOiJzeW1wZjExNzAyNmw5ZmY5b29zdWlnbmJwdmgzNGo1aCIsIl9hdXRoX3NpZ25faW5fYXV0aCI6ZmFsc2UsInVzZXJfaWQiOjIwLCJfYXV0aF91c2VyX2hhc2giOiI4N2VlZTk5ZDM5NDVkYjgxN2E0Y2FiZDM0MWE1NzcyYWJkMTY0MTM2IiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJfYXV0aF9zaWduX2luX2lkIjoyMCwiX2F1dGhfdXNlcl9pZCI6IjQ1IiwiX2F1dGhfc2lnbl9pbl9tYXBwaW5nX2lkIjo0NSwiX3Nlc3Npb25fZXhwaXJ5Ijo5OTk5OTk5OX0=','2020-05-14 12:21:59.542783'),('t3ulpxg5ti2zbj0i97c3kxcmsq7nsjl0','ZmY4MDI5MDJkYmQ4MzUzYjM1OTg2OWQ3OGFiOTU4MjZkMDhlNTYyYjp7fQ==','2017-04-28 03:00:14.628158'),('t4m9fxakmrnmdcraq09fuqk46sx7vlus','NjAxOTFlZDcxYmY5ZDM3M2NmODE4ZGI1M2YxMDA4NDNlYTMwNDljYzp7ImF1dGhfdG9rZW4iOiJ0NG05Znhha21ybm1kY3JhcTA5ZnVxazQ2c3g3dmx1cyIsIl9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX3Nlc3Npb25fZXhwaXJ5Ijo5OTk5OTk5OSwiX2F1dGhfc2lnbl9pbl9tYXBwaW5nX2lkIjo0NSwiX2F1dGhfdXNlcl9oYXNoIjoiODdlZWU5OWQzOTQ1ZGI4MTdhNGNhYmQzNDFhNTc3MmFiZDE2NDEzNiIsIl9hdXRoX3NpZ25faW5faWQiOjIwLCJfYXV0aF91c2VyX2lkIjoiNDUiLCJ1c2VyX2lkIjoyMCwiX2F1dGhfc2lnbl9pbl9hdXRoIjpmYWxzZX0=','2020-05-22 12:07:11.707146'),('tn2m4n7yf5dsdvyx2grf63b2c9bcxtnd','NzczYTJjZGFhYTE4NDVjYzhhMWYxMTExNzA2YThlMTYzODI1ZjBhZDp7Il9hdXRoX3NpZ25faW5fYXV0aCI6ZmFsc2UsIl9hdXRoX3VzZXJfaWQiOiI0NSIsImF1dGhfdG9rZW4iOiJ0bjJtNG43eWY1ZHNkdnl4MmdyZjYzYjJjOWJjeHRuZCIsInVzZXJfaWQiOjIwLCJfc2Vzc2lvbl9leHBpcnkiOjk5OTk5OTk5LCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9hdXRoX3VzZXJfaGFzaCI6Ijg3ZWVlOTlkMzk0NWRiODE3YTRjYWJkMzQxYTU3NzJhYmQxNjQxMzYiLCJfYXV0aF9zaWduX2luX21hcHBpbmdfaWQiOjQ1LCJfYXV0aF9zaWduX2luX2lkIjoyMH0=','2020-05-22 19:19:21.679843'),('trs050lbudooax443xgwncu2n78owhsp','YjRhZWE4OWQ1MTBmM2YzYTExNWVjMjBiMTRmMDdhZDUzYmM1OTg1MDp7Il9hdXRoX3NpZ25faW5faWQiOjIwLCJfYXV0aF9zaWduX2luX21hcHBpbmdfaWQiOjQ1LCJfYXV0aF91c2VyX2hhc2giOiI4N2VlZTk5ZDM5NDVkYjgxN2E0Y2FiZDM0MWE1NzcyYWJkMTY0MTM2IiwiX2F1dGhfdXNlcl9pZCI6IjQ1IiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJhdXRoX3Rva2VuIjoidHJzMDUwbGJ1ZG9vYXg0NDN4Z3duY3Uybjc4b3doc3AiLCJ1c2VyX2lkIjoyMCwiX2F1dGhfc2lnbl9pbl9hdXRoIjpmYWxzZSwiX3Nlc3Npb25fZXhwaXJ5Ijo5OTk5OTk5OX0=','2020-05-16 13:47:26.868181'),('txn2l4w40buf34fmqdtf4krgkljm8w66','NTUzZmYwYjMyZDlkYjYwMTg1NTJkMWMxMjM5OWE5ZjY2Mjg2YWJkYjp7Il9hdXRoX3NpZ25faW5fbWFwcGluZ19pZCI6NDUsIl9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfc2lnbl9pbl9hdXRoIjpmYWxzZSwiX2F1dGhfdXNlcl9oYXNoIjoiODdlZWU5OWQzOTQ1ZGI4MTdhNGNhYmQzNDFhNTc3MmFiZDE2NDEzNiIsInVzZXJfaWQiOjIwLCJfYXV0aF9zaWduX2luX2lkIjoyMCwiX2F1dGhfdXNlcl9pZCI6IjQ1IiwiYXV0aF90b2tlbiI6InR4bjJsNHc0MGJ1ZjM0Zm1xZHRmNGtyZ2tsam04dzY2IiwiX3Nlc3Npb25fZXhwaXJ5Ijo5OTk5OTk5OX0=','2020-06-20 17:45:58.502198'),('u4lfc5x7dk80jlxo7ka2fjjj7t6kz2j5','YjVhOWM0NzM3MmFiNGZiNTI2NWM0OGExMzMyNjEwZWE2NTIwOTM0ZTp7Il9hdXRoX3VzZXJfaGFzaCI6Ijg3ZWVlOTlkMzk0NWRiODE3YTRjYWJkMzQxYTU3NzJhYmQxNjQxMzYiLCJfYXV0aF91c2VyX2lkIjoiNDUiLCJhdXRoX3Rva2VuIjoidTRsZmM1eDdkazgwamx4bzdrYTJmampqN3Q2a3oyajUiLCJfc2Vzc2lvbl9leHBpcnkiOjk5OTk5OTk5LCJfYXV0aF9zaWduX2luX2lkIjoyMCwiX2F1dGhfc2lnbl9pbl9tYXBwaW5nX2lkIjo0NSwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJ1c2VyX2lkIjoyMCwiX2F1dGhfc2lnbl9pbl9hdXRoIjpmYWxzZX0=','2020-05-29 12:38:30.740826'),('uaag4kehw6re5tlivgs5m6aw7kacgme5','M2U0ZGQ3ZDBhOTg0ZTU2MjFkYzdhOTZkZTliMTRjYjAxMjQ3MjViNTp7Il9hdXRoX3VzZXJfaGFzaCI6Ijg3ZWVlOTlkMzk0NWRiODE3YTRjYWJkMzQxYTU3NzJhYmQxNjQxMzYiLCJ1c2VyX2lkIjoyMCwiX2F1dGhfc2lnbl9pbl9tYXBwaW5nX2lkIjo0NSwiX2F1dGhfdXNlcl9pZCI6IjQ1IiwiX3Nlc3Npb25fZXhwaXJ5Ijo5OTk5OTk5OSwiX2F1dGhfc2lnbl9pbl9hdXRoIjpmYWxzZSwiX2F1dGhfc2lnbl9pbl9pZCI6MjAsIl9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiYXV0aF90b2tlbiI6InVhYWc0a2VodzZyZTV0bGl2Z3M1bTZhdzdrYWNnbWU1In0=','2020-05-17 17:57:40.061184'),('ui2pwnjmbyszov19a359or84znctn21x','ZDAwMWM3M2RhNzc4NjEyODkxMDkwYzZlYWQ5ZTJjYjdmYmViYTQ5NTp7InVzZXJfaWQiOjE3LCJfc2Vzc2lvbl9leHBpcnkiOjk5OTk5OTk5LCJhdXRoX3Rva2VuIjoidWkycHduam1ieXN6b3YxOWEzNTlvcjg0em5jdG4yMXgiLCJfYXV0aF9zaWduX2luX2F1dGgiOmZhbHNlLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9hdXRoX3NpZ25faW5fbWFwcGluZ19pZCI6NDAsIl9hdXRoX3VzZXJfaWQiOiI0MCIsIl9hdXRoX3NpZ25faW5faWQiOjE3LCJfYXV0aF91c2VyX2hhc2giOiJmM2JhZjA5M2ExZDA4ZGZiZGIxZmUwM2RlNmQ1ODdjY2ZjYjRiNzQxIn0=','2020-05-13 18:07:47.949165'),('uw1nrxydrrzinhxe5wiqqm0rq65pjrb0','YWNkZTQ3MWI4YjcxMjhmNzY5ZjRmNmYzMmVkNDA3MDU5MmU4YjlmZjp7Il9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX3Nlc3Npb25fZXhwaXJ5Ijo5OTk5OTk5OSwiX2F1dGhfc2lnbl9pbl9tYXBwaW5nX2lkIjo0NSwiX2F1dGhfdXNlcl9oYXNoIjoiODdlZWU5OWQzOTQ1ZGI4MTdhNGNhYmQzNDFhNTc3MmFiZDE2NDEzNiIsIl9hdXRoX3NpZ25faW5faWQiOjIwLCJfYXV0aF91c2VyX2lkIjoiNDUiLCJ1c2VyX2lkIjoyMCwiX2F1dGhfc2lnbl9pbl9hdXRoIjpmYWxzZSwiYXV0aF90b2tlbiI6InV3MW5yeHlkcnJ6aW5oeGU1d2lxcW0wcnE2NXBqcmIwIn0=','2020-05-21 20:22:12.348924'),('uykx1a54x88i1i2giuxd2flcsq0876os','NTgwMDlhZDMxMmEwZDdlMjUyNTkxZjI1ZWU0YzM3MzQwNjk3NDg5Njp7Il9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9oYXNoIjoiZjNiYWYwOTNhMWQwOGRmYmRiMWZlMDNkZTZkNTg3Y2NmY2I0Yjc0MSIsImF1dGhfdG9rZW4iOiJ1eWt4MWE1NHg4OGkxaTJnaXV4ZDJmbGNzcTA4NzZvcyIsIl9hdXRoX3VzZXJfaWQiOiI0MCIsIl9zZXNzaW9uX2V4cGlyeSI6OTk5OTk5OTksInVzZXJfaWQiOjE3LCJfYXV0aF9zaWduX2luX2lkIjoxNywiX2F1dGhfc2lnbl9pbl9hdXRoIjpmYWxzZSwiX2F1dGhfc2lnbl9pbl9tYXBwaW5nX2lkIjo0MH0=','2020-05-29 18:55:53.160926'),('uzukjdwdx6ybcj9ms7scwoorhzagi6rk','NjIwNTkxYWVhZjFlMzRmNWVjOGI3MmNmOWRhMWY0OGY5OWYxNWJkZjp7ImF1dGhfdG9rZW4iOiJ1enVramR3ZHg2eWJjajltczdzY3dvb3JoemFnaTZyayIsIl9zZXNzaW9uX2V4cGlyeSI6OTk5OTk5OTksIl9hdXRoX3VzZXJfaWQiOiI0NSIsIl9hdXRoX3NpZ25faW5faWQiOjIwLCJfYXV0aF91c2VyX2hhc2giOiI4N2VlZTk5ZDM5NDVkYjgxN2E0Y2FiZDM0MWE1NzcyYWJkMTY0MTM2IiwidXNlcl9pZCI6MjAsIl9hdXRoX3NpZ25faW5fYXV0aCI6ZmFsc2UsIl9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfc2lnbl9pbl9tYXBwaW5nX2lkIjo0NX0=','2020-05-22 19:04:37.357420'),('vihgoboc03okyg8l48ktl9o8k3eo91vu','YmI4YTg5MThiYjE2NGE3ZTNmNWE2MWY2NzVhMWMwYjhhODcwYmFkZTp7Il9hdXRoX3VzZXJfaGFzaCI6Ijg3ZWVlOTlkMzk0NWRiODE3YTRjYWJkMzQxYTU3NzJhYmQxNjQxMzYiLCJ1c2VyX2lkIjoyMCwiX2F1dGhfc2lnbl9pbl9tYXBwaW5nX2lkIjo0NSwiX2F1dGhfdXNlcl9pZCI6IjQ1IiwiX3Nlc3Npb25fZXhwaXJ5Ijo5OTk5OTk5OSwiX2F1dGhfc2lnbl9pbl9hdXRoIjpmYWxzZSwiX2F1dGhfc2lnbl9pbl9pZCI6MjAsIl9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiYXV0aF90b2tlbiI6InZpaGdvYm9jMDNva3lnOGw0OGt0bDlvOGszZW85MXZ1In0=','2020-05-17 17:32:35.880781'),('vuric43o34p3f6n5pssvof8j56lq9e2d','MTkzODliYTYyY2VmMjQxNmI4OGJhNzNkYzYyZTJmNzQ4OTg5NDVjZTp7Il9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwidXNlcl9pZCI6MjAsIl9hdXRoX3VzZXJfaWQiOiI0NSIsIl9hdXRoX3VzZXJfaGFzaCI6Ijg3ZWVlOTlkMzk0NWRiODE3YTRjYWJkMzQxYTU3NzJhYmQxNjQxMzYiLCJfYXV0aF9zaWduX2luX21hcHBpbmdfaWQiOjQ1LCJhdXRoX3Rva2VuIjoidnVyaWM0M28zNHAzZjZuNXBzc3ZvZjhqNTZscTllMmQiLCJfc2Vzc2lvbl9leHBpcnkiOjk5OTk5OTk5LCJfYXV0aF9zaWduX2luX2F1dGgiOmZhbHNlLCJfYXV0aF9zaWduX2luX2lkIjoyMH0=','2020-05-15 17:28:24.396473'),('w181pykx6692eumofylh5tv1tkst7z3c','MTRhNDE5MGU5MmUwOGEzOTc4ODFlMjQwMWVlMGE0ZjZlZWJmZjBhZDp7InVzZXJfaWQiOjIwLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9hdXRoX3NpZ25faW5fbWFwcGluZ19pZCI6NDUsIl9hdXRoX3VzZXJfaWQiOiI0NSIsIl9hdXRoX3NpZ25faW5faWQiOjIwLCJfYXV0aF9zaWduX2luX2F1dGgiOmZhbHNlLCJhdXRoX3Rva2VuIjoidzE4MXB5a3g2NjkyZXVtb2Z5bGg1dHYxdGtzdDd6M2MiLCJfYXV0aF91c2VyX2hhc2giOiI4N2VlZTk5ZDM5NDVkYjgxN2E0Y2FiZDM0MWE1NzcyYWJkMTY0MTM2IiwiX3Nlc3Npb25fZXhwaXJ5Ijo5OTk5OTk5OX0=','2020-05-22 18:56:02.411117'),('w5svjol61kmo8j1vl3fevlaninne93xn','NTdlMmYyYWQ0NjhkNTkxYzRkYmUyODFhZmE3NTFlOTVmYWJmNmFkZTp7Il9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9oYXNoIjoiODdlZWU5OWQzOTQ1ZGI4MTdhNGNhYmQzNDFhNTc3MmFiZDE2NDEzNiIsImF1dGhfdG9rZW4iOiJ3NXN2am9sNjFrbW84ajF2bDNmZXZsYW5pbm5lOTN4biIsIl9hdXRoX3VzZXJfaWQiOiI0NSIsIl9zZXNzaW9uX2V4cGlyeSI6OTk5OTk5OTksInVzZXJfaWQiOjIwLCJfYXV0aF9zaWduX2luX2lkIjoyMCwiX2F1dGhfc2lnbl9pbl9hdXRoIjpmYWxzZSwiX2F1dGhfc2lnbl9pbl9tYXBwaW5nX2lkIjo0NX0=','2020-05-29 19:11:47.531354'),('w7nqdm95k9l45m2yehmg25pnwqc1u89x','ZThhYzc5MWI3ODk4NTk3NzU0ZTBmYWU0NmRiNGM3NTc5NGIxMzdkYjp7Il9hdXRoX3VzZXJfaGFzaCI6Ijg3ZWVlOTlkMzk0NWRiODE3YTRjYWJkMzQxYTU3NzJhYmQxNjQxMzYiLCJ1c2VyX2lkIjoyMCwiX2F1dGhfc2lnbl9pbl9tYXBwaW5nX2lkIjo0NSwiX2F1dGhfdXNlcl9pZCI6IjQ1IiwiX3Nlc3Npb25fZXhwaXJ5Ijo5OTk5OTk5OSwiX2F1dGhfc2lnbl9pbl9hdXRoIjpmYWxzZSwiX2F1dGhfc2lnbl9pbl9pZCI6MjAsIl9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiYXV0aF90b2tlbiI6Inc3bnFkbTk1azlsNDVtMnllaG1nMjVwbndxYzF1ODl4In0=','2020-05-17 18:26:09.990964'),('w9o8asof1iq14n91dprm3hqygndnd4ls','OGI4NWVkOTEzNjI2NmZlYjEyYzBlNTA3YzFlZjFmNDY5MTFmMjY4Njp7Il9hdXRoX3NpZ25faW5faWQiOjIwLCJfYXV0aF9zaWduX2luX21hcHBpbmdfaWQiOjQ1LCJfc2Vzc2lvbl9leHBpcnkiOjk5OTk5OTk5LCJ1c2VyX2lkIjoyMCwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJfYXV0aF91c2VyX2hhc2giOiI4N2VlZTk5ZDM5NDVkYjgxN2E0Y2FiZDM0MWE1NzcyYWJkMTY0MTM2IiwiX2F1dGhfc2lnbl9pbl9hdXRoIjpmYWxzZSwiX2F1dGhfdXNlcl9pZCI6IjQ1IiwiYXV0aF90b2tlbiI6Inc5bzhhc29mMWlxMTRuOTFkcHJtM2hxeWduZG5kNGxzIn0=','2020-05-30 13:58:11.514788'),('wfsdwo1xkc6fe1d67alrwxoen34h4h5i','MGZlMTFmMDZjMDFjNjE0NWU4MDA5ZTMxY2ZiNDRjNzhhYjBlZWFlZTp7InJlc19saXN0IjpbeyJpZCI6MSwibmFtZSI6IkthcHBvIHJvIGFubiJ9XSwiY21zX2F1dGhfc2lnbl9pbl9jdXJyX3Jlc19pZCI6MSwiY21zX2F1dGhfc2lnbl9pbl9hdXRoIjpmYWxzZSwiX2F1dGhfdXNlcl9pZCI6IjIiLCJjbXNfYXV0aF9zaWduX2luX21hcHBpbmdfcm9sZSI6MSwiY21zX2F1dGhfc2lnbl9pbl9tYXBwaW5nX3JvbGVfbmFtZSI6Ik1FTlRFIiwiY21zX2F1dGhfc2lnbl9pbl9pZCI6MSwiX2F1dGhfdXNlcl9oYXNoIjoiNDNkMjVlZWY0MDMzMDVjM2FkZmM0ZjZjOGY2ZWYyOGFmOTBhNGY2YyIsImNtc19hdXRoX3NpZ25faW5fbWFwcGluZ19pZCI6ImFkbWlubWVudGVAdG95b3N1LmNvbSIsImNtc19hdXRoX3NpZ25faW5fbWFwcGluZ19mbmFtZSI6IkhpZ28iLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCJ9','2017-04-10 07:33:12.355159'),('wjmgiqsi6c2ztviogkbdqgo7zcrswby9','OTA5Yjg5YWNiMWM2NzNhMTEwMTZiNjAxMmM0ZjIwMDgzYWUyY2U3Yjp7Il9zZXNzaW9uX2V4cGlyeSI6OTk5OTk5OTksIl9hdXRoX3VzZXJfaGFzaCI6Ijg3ZWVlOTlkMzk0NWRiODE3YTRjYWJkMzQxYTU3NzJhYmQxNjQxMzYiLCJfYXV0aF9zaWduX2luX21hcHBpbmdfaWQiOjQ1LCJfYXV0aF91c2VyX2lkIjoiNDUiLCJfYXV0aF9zaWduX2luX2lkIjoyMCwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJfYXV0aF9zaWduX2luX2F1dGgiOmZhbHNlLCJhdXRoX3Rva2VuIjoid2ptZ2lxc2k2YzJ6dHZpb2drYmRxZ283emNyc3dieTkiLCJ1c2VyX2lkIjoyMH0=','2020-05-15 16:13:43.396408'),('wndy8qzs6j7tp4hppe99l2uo7hkt8q9z','NDI4YWE2OGM2OWUyY2Q1MjA4NjQxNjk3MWI2YTMxNDA4Mzk2ZDU1ZDp7Il9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfc2lnbl9pbl9pZCI6MjAsIl9zZXNzaW9uX2V4cGlyeSI6OTk5OTk5OTksImF1dGhfdG9rZW4iOiJ3bmR5OHF6czZqN3RwNGhwcGU5OWwydW83aGt0OHE5eiIsIl9hdXRoX3VzZXJfaGFzaCI6Ijg3ZWVlOTlkMzk0NWRiODE3YTRjYWJkMzQxYTU3NzJhYmQxNjQxMzYiLCJfYXV0aF9zaWduX2luX21hcHBpbmdfaWQiOjQ1LCJ1c2VyX2lkIjoyMCwiX2F1dGhfdXNlcl9pZCI6IjQ1IiwiX2F1dGhfc2lnbl9pbl9hdXRoIjpmYWxzZX0=','2020-06-10 19:35:47.725736'),('x38co7dhlnm56m1x7li8vpmpwd7nnun4','ODg4MmY1Mjg5ZDc1YjEyODE4MGM1ZjVhMTllMTRlZjZkNjlhNTE0Zjp7Il9hdXRoX3VzZXJfaGFzaCI6Ijg3ZWVlOTlkMzk0NWRiODE3YTRjYWJkMzQxYTU3NzJhYmQxNjQxMzYiLCJ1c2VyX2lkIjoyMCwiX2F1dGhfc2lnbl9pbl9tYXBwaW5nX2lkIjo0NSwiX2F1dGhfdXNlcl9pZCI6IjQ1IiwiX3Nlc3Npb25fZXhwaXJ5Ijo5OTk5OTk5OSwiX2F1dGhfc2lnbl9pbl9hdXRoIjpmYWxzZSwiX2F1dGhfc2lnbl9pbl9pZCI6MjAsIl9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiYXV0aF90b2tlbiI6IngzOGNvN2RobG5tNTZtMXg3bGk4dnBtcHdkN25udW40In0=','2020-05-17 20:01:41.229259'),('x8532xoezq0i67q7h7shv194qubtome5','ZjNmZjEzNTI2ZWZmZDI5NmMzYTNlNzI0YTNhZTkzNGQ0ZDE0NzgyODp7ImF1dGhfdG9rZW4iOiJ4ODUzMnhvZXpxMGk2N3E3aDdzaHYxOTRxdWJ0b21lNSIsInVzZXJfaWQiOjE3LCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9hdXRoX3NpZ25faW5fYXV0aCI6ZmFsc2UsIl9hdXRoX3NpZ25faW5fbWFwcGluZ19pZCI6NDAsIl9hdXRoX3VzZXJfaGFzaCI6ImYzYmFmMDkzYTFkMDhkZmJkYjFmZTAzZGU2ZDU4N2NjZmNiNGI3NDEiLCJfYXV0aF91c2VyX2lkIjoiNDAiLCJfYXV0aF9zaWduX2luX2lkIjoxNywiX3Nlc3Npb25fZXhwaXJ5Ijo5OTk5OTk5OX0=','2020-06-14 13:24:41.730955'),('xdlnkwv4xf8sruu2b9d9eavz9cjbggvl','NzQ1ZTc5ZWUxMDczYzZmMTEzNzJlZjM1YjAyMWRmM2Y3MzE3Y2E5Mjp7Il9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfc2lnbl9pbl9hdXRoIjpmYWxzZSwidXNlcl9pZCI6MjAsIl9hdXRoX3VzZXJfaGFzaCI6Ijg3ZWVlOTlkMzk0NWRiODE3YTRjYWJkMzQxYTU3NzJhYmQxNjQxMzYiLCJhdXRoX3Rva2VuIjoieGRsbmt3djR4ZjhzcnV1MmI5ZDllYXZ6OWNqYmdndmwiLCJfYXV0aF9zaWduX2luX2lkIjoyMCwiX2F1dGhfdXNlcl9pZCI6IjQ1IiwiX2F1dGhfc2lnbl9pbl9tYXBwaW5nX2lkIjo0NSwiX3Nlc3Npb25fZXhwaXJ5Ijo5OTk5OTk5OX0=','2020-05-14 13:02:27.513579'),('xh8pbr1hwgqqcgqiau7gjpiwm0f6vwfm','ZjY5OGU5MmYyM2VmOGM4N2UxNDIzODQ4YzJjODE3NmYxOTJjZjllZjp7Il9hdXRoX3VzZXJfaGFzaCI6IjY0YjdkOGNjY2YyMmYyOGUxNzM2NDZmZmI1YmI2ZjViNzU5YzgwZDYiLCJfYXV0aF9zaWduX2luX2lkIjoxMywidXNlcl9pZCI6MTMsIl9hdXRoX3NpZ25faW5fYXV0aCI6ZmFsc2UsIl9hdXRoX3VzZXJfaWQiOiIzMyIsIl9hdXRoX3NpZ25faW5fbWFwcGluZ19pZCI6MzMsIl9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiYXV0aF90b2tlbiI6InhoOHBicjFod2dxcWNncWlhdTdnanBpd20wZjZ2d2ZtIiwiX3Nlc3Npb25fZXhwaXJ5Ijo5OTk5OTk5OX0=','2020-05-13 14:19:47.361178'),('xt3d5zoizj920hcowmcjjvy4fnrpur3t','Y2MyYjAwN2EzMzBhOWZiNDdjNzM5ZDU2MGY0YzM2ZWM1OTliYjljZTp7Il9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfc2lnbl9pbl9hdXRoIjpmYWxzZSwidXNlcl9pZCI6MjAsIl9hdXRoX3VzZXJfaGFzaCI6Ijg3ZWVlOTlkMzk0NWRiODE3YTRjYWJkMzQxYTU3NzJhYmQxNjQxMzYiLCJhdXRoX3Rva2VuIjoieHQzZDV6b2l6ajkyMGhjb3dtY2pqdnk0Zm5ycHVyM3QiLCJfYXV0aF9zaWduX2luX2lkIjoyMCwiX2F1dGhfdXNlcl9pZCI6IjQ1IiwiX2F1dGhfc2lnbl9pbl9tYXBwaW5nX2lkIjo0NSwiX3Nlc3Npb25fZXhwaXJ5Ijo5OTk5OTk5OX0=','2020-05-14 12:27:28.390664'),('xz0v6njqyyc38prd22elrooajou4lw1s','NmJmNDdkOWQwMzU0OTFjZDkxN2U1ZTliNzI5ZTM1ZmU5YTY1YmVjOTp7Il9hdXRoX3VzZXJfaGFzaCI6Ijg3ZWVlOTlkMzk0NWRiODE3YTRjYWJkMzQxYTU3NzJhYmQxNjQxMzYiLCJ1c2VyX2lkIjoyMCwiX2F1dGhfc2lnbl9pbl9tYXBwaW5nX2lkIjo0NSwiX2F1dGhfdXNlcl9pZCI6IjQ1IiwiX3Nlc3Npb25fZXhwaXJ5Ijo5OTk5OTk5OSwiX2F1dGhfc2lnbl9pbl9hdXRoIjpmYWxzZSwiX2F1dGhfc2lnbl9pbl9pZCI6MjAsIl9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiYXV0aF90b2tlbiI6Inh6MHY2bmpxeXljMzhwcmQyMmVscm9vYWpvdTRsdzFzIn0=','2020-05-17 19:58:17.819920'),('y8em9dyxk5b6qoicggyh87r8ur77x8bp','MmQyZGNjMDdiMGNhZjg4OTZiNmM4MjYxYTM0YmVkMzBjOTczN2Y5Mjp7Il9hdXRoX3NpZ25faW5faWQiOjIwLCJfc2Vzc2lvbl9leHBpcnkiOjk5OTk5OTk5LCJfYXV0aF91c2VyX2hhc2giOiI4N2VlZTk5ZDM5NDVkYjgxN2E0Y2FiZDM0MWE1NzcyYWJkMTY0MTM2IiwidXNlcl9pZCI6MjAsIl9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfc2lnbl9pbl9hdXRoIjpmYWxzZSwiX2F1dGhfc2lnbl9pbl9tYXBwaW5nX2lkIjo0NSwiYXV0aF90b2tlbiI6Ink4ZW05ZHl4azViNnFvaWNnZ3loODdyOHVyNzd4OGJwIiwiX2F1dGhfdXNlcl9pZCI6IjQ1In0=','2020-05-22 19:00:09.614117'),('ykwn4yyl0m1si24htcte5qc3ape5p4hw','ZTYwYjY5ZDRjNWY4NDE3MTc3MTllMGI5NzQ5MjhiMGYzMmQwYjA4OTp7Il9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfc2lnbl9pbl9hdXRoIjpmYWxzZSwidXNlcl9pZCI6MjAsIl9hdXRoX3VzZXJfaGFzaCI6Ijg3ZWVlOTlkMzk0NWRiODE3YTRjYWJkMzQxYTU3NzJhYmQxNjQxMzYiLCJhdXRoX3Rva2VuIjoieWt3bjR5eWwwbTFzaTI0aHRjdGU1cWMzYXBlNXA0aHciLCJfYXV0aF9zaWduX2luX2lkIjoyMCwiX2F1dGhfdXNlcl9pZCI6IjQ1IiwiX2F1dGhfc2lnbl9pbl9tYXBwaW5nX2lkIjo0NSwiX3Nlc3Npb25fZXhwaXJ5Ijo5OTk5OTk5OX0=','2020-05-14 13:22:35.957500'),('yrrxfgm2k241d7zi1h6fcuknh6n5hppt','Yjg3OWUyMjRiMTEwMWVjMWRhODljMDQ2ZmMyMjZkMzE4ZWQ0YThhMTp7InVzZXJfaWQiOjE3LCJfc2Vzc2lvbl9leHBpcnkiOjk5OTk5OTk5LCJhdXRoX3Rva2VuIjoieXJyeGZnbTJrMjQxZDd6aTFoNmZjdWtuaDZuNWhwcHQiLCJfYXV0aF9zaWduX2luX2F1dGgiOmZhbHNlLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9hdXRoX3NpZ25faW5fbWFwcGluZ19pZCI6NDAsIl9hdXRoX3VzZXJfaWQiOiI0MCIsIl9hdXRoX3NpZ25faW5faWQiOjE3LCJfYXV0aF91c2VyX2hhc2giOiJmM2JhZjA5M2ExZDA4ZGZiZGIxZmUwM2RlNmQ1ODdjY2ZjYjRiNzQxIn0=','2020-05-13 15:47:27.959222'),('ywccjbclrs33mrvlkp89q9cd8t4v6cin','YTYxMWJjMjIyZTkyYzM2ODkxMmIzNTM0Y2RmNDQwZjhiNTk1NjNjYzp7ImNtc19hdXRoX3NpZ25faW5fbWFwcGluZ19yb2xlX25hbWUiOiJNRU5URSIsImNtc19hdXRoX3NpZ25faW5faWQiOjEsImNtc19hdXRoX3NpZ25faW5fbWFwcGluZ19mbmFtZSI6IkhpZ28iLCJfYXV0aF91c2VyX2hhc2giOiI0M2QyNWVlZjQwMzMwNWMzYWRmYzRmNmM4ZjZlZjI4YWY5MGE0ZjZjIiwiY21zX2F1dGhfc2lnbl9pbl9tYXBwaW5nX2lkIjoiYWRtaW5tZW50ZUB0b3lvc3UuY29tIiwiX2F1dGhfdXNlcl9pZCI6IjIiLCJjbXNfYXV0aF9zaWduX2luX21hcHBpbmdfcm9sZSI6MSwicmVzX2xpc3QiOlt7ImlkIjoxLCJuYW1lIjoiS2FwcG8gcm8gYW5uIn0seyJpZCI6MiwibmFtZSI6IlJvYW5uIDIgVGVzdCJ9XSwiY21zX2F1dGhfc2lnbl9pbl9jdXJyX3Jlc19pZCI6MSwiY21zX2F1dGhfc2lnbl9pbl9hdXRoIjpmYWxzZSwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQifQ==','2017-05-03 08:50:51.313475'),('zaa4vhgkxfykxwdwn5wcy9kit935pb79','ODJhNGZhMjc5ZWQwZjk0MWM0YzNiZmE4OTk1NWE4ODhjMTQzNThhZDp7Il9hdXRoX3VzZXJfaWQiOiI0NSIsIl9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfc2lnbl9pbl9hdXRoIjpmYWxzZSwiX2F1dGhfc2lnbl9pbl9pZCI6MjAsIl9hdXRoX3VzZXJfaGFzaCI6Ijg3ZWVlOTlkMzk0NWRiODE3YTRjYWJkMzQxYTU3NzJhYmQxNjQxMzYiLCJhdXRoX3Rva2VuIjoiemFhNHZoZ2t4ZnlreHdkd241d2N5OWtpdDkzNXBiNzkiLCJfc2Vzc2lvbl9leHBpcnkiOjk5OTk5OTk5LCJfYXV0aF9zaWduX2luX21hcHBpbmdfaWQiOjQ1LCJ1c2VyX2lkIjoyMH0=','2020-05-24 14:22:22.617092'),('zby9kvenip6my3g5mtcg2o8uboh1vj02','MGViZTEzMDg4Nzk3NjQxMTUwMDMyZjFlNGIyZGNmM2ZkYzllMTM1Zjp7Il9hdXRoX3NpZ25faW5faWQiOjIwLCJfYXV0aF9zaWduX2luX2F1dGgiOmZhbHNlLCJ1c2VyX2lkIjoyMCwiX2F1dGhfdXNlcl9pZCI6IjQ1IiwiX2F1dGhfdXNlcl9oYXNoIjoiODdlZWU5OWQzOTQ1ZGI4MTdhNGNhYmQzNDFhNTc3MmFiZDE2NDEzNiIsIl9hdXRoX3NpZ25faW5fbWFwcGluZ19pZCI6NDUsIl9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiYXV0aF90b2tlbiI6InpieTlrdmVuaXA2bXkzZzVtdGNnMm84dWJvaDF2ajAyIiwiX3Nlc3Npb25fZXhwaXJ5Ijo5OTk5OTk5OX0=','2020-05-14 11:27:50.597905'),('zkaqfiwnk43vkdv9jbexk8uy7peh81v9','ZWYyMjhhY2YwZTE4ZDJmNTE1YmEzM2MwOTYzMTI3MWYzYmE0YWUxNTp7Il9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfc2lnbl9pbl9pZCI6MTcsIl9zZXNzaW9uX2V4cGlyeSI6OTk5OTk5OTksImF1dGhfdG9rZW4iOiJ6a2FxZml3bms0M3ZrZHY5amJleGs4dXk3cGVoODF2OSIsIl9hdXRoX3VzZXJfaGFzaCI6ImYzYmFmMDkzYTFkMDhkZmJkYjFmZTAzZGU2ZDU4N2NjZmNiNGI3NDEiLCJfYXV0aF9zaWduX2luX21hcHBpbmdfaWQiOjQwLCJ1c2VyX2lkIjoxNywiX2F1dGhfdXNlcl9pZCI6IjQwIiwiX2F1dGhfc2lnbl9pbl9hdXRoIjpmYWxzZX0=','2020-06-10 19:25:08.313801'),('zxqrvzrmdpnp0jzj127q3v91kz7mfjh1','M2VmODZkOGFlZWQyMmRlMWQ2MzQyMTk5ZjliZGE5MDMzMzEyNTg5NDp7Il9hdXRoX3NpZ25faW5faWQiOjIwLCJfYXV0aF91c2VyX2hhc2giOiI4N2VlZTk5ZDM5NDVkYjgxN2E0Y2FiZDM0MWE1NzcyYWJkMTY0MTM2IiwidXNlcl9pZCI6MjAsIl9hdXRoX3NpZ25faW5fbWFwcGluZ19pZCI6NDUsIl9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9pZCI6IjQ1IiwiX2F1dGhfc2lnbl9pbl9hdXRoIjpmYWxzZSwiX3Nlc3Npb25fZXhwaXJ5Ijo5OTk5OTk5OSwiYXV0aF90b2tlbiI6Inp4cXJ2enJtZHBucDBqemoxMjdxM3Y5MWt6N21mamgxIn0=','2020-05-28 13:07:26.473007');
/*!40000 ALTER TABLE `django_session` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `food_categories`
--

DROP TABLE IF EXISTS `food_categories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `food_categories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `name_cn` varchar(50) NOT NULL,
  `image_path` varchar(500) DEFAULT NULL,
  `create_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `update_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `update_by` varchar(50) DEFAULT NULL,
  `is_delete` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8 AVG_ROW_LENGTH=8192;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `food_categories`
--

LOCK TABLES `food_categories` WRITE;
/*!40000 ALTER TABLE `food_categories` DISABLE KEYS */;
INSERT INTO `food_categories` VALUES (1,'Recommend','特别','','2016-10-25 06:37:34','2017-02-03 08:52:45','',0),(2,'Maguro','金枪鱼','','2016-10-25 06:37:34','2017-02-13 03:23:18','',0),(3,'Seafood','海鲜','','2016-10-25 06:37:34','2017-02-13 06:03:03','',0),(4,'Japan Bowls','日本碗','','2016-10-25 06:37:34','2017-02-13 06:05:23','',0),(10,'Test','Test','','2017-02-13 09:49:55','2017-02-23 03:25:48',NULL,0);
/*!40000 ALTER TABLE `food_categories` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `food_items`
--

DROP TABLE IF EXISTS `food_items`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `food_items` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `food_id` int(11) NOT NULL,
  `item_id` int(11) NOT NULL,
  `comment` varchar(200) DEFAULT NULL,
  `comment_cn` varchar(200) DEFAULT NULL,
  `create_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `update_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `update_by` varchar(50) DEFAULT NULL,
  `is_delete` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `FK_food_items_foods_id` (`food_id`),
  KEY `FK_food_items_items_id` (`item_id`),
  CONSTRAINT `FK_food_items_foods_id` FOREIGN KEY (`food_id`) REFERENCES `foods` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_food_items_items_id` FOREIGN KEY (`item_id`) REFERENCES `items` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `food_items`
--

LOCK TABLES `food_items` WRITE;
/*!40000 ALTER TABLE `food_items` DISABLE KEYS */;
/*!40000 ALTER TABLE `food_items` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `food_prices`
--

DROP TABLE IF EXISTS `food_prices`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `food_prices` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `food_id` int(11) NOT NULL,
  `qr_code` varchar(20) DEFAULT NULL,
  `price_date` datetime NOT NULL,
  `price` decimal(19,2) NOT NULL,
  `comment` varchar(200) DEFAULT NULL,
  `comment_cn` varchar(200) DEFAULT NULL,
  `create_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `update_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `update_by` varchar(50) DEFAULT NULL,
  `is_delete` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `FK_food_prices_foods_id` (`food_id`),
  CONSTRAINT `FK_food_prices_foods_id` FOREIGN KEY (`food_id`) REFERENCES `foods` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `food_prices`
--

LOCK TABLES `food_prices` WRITE;
/*!40000 ALTER TABLE `food_prices` DISABLE KEYS */;
/*!40000 ALTER TABLE `food_prices` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `food_types`
--

DROP TABLE IF EXISTS `food_types`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `food_types` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `name_cn` varchar(50) DEFAULT NULL,
  `create_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `update_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `update_by` varchar(50) DEFAULT NULL,
  `is_delete` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 AVG_ROW_LENGTH=16384;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `food_types`
--

LOCK TABLES `food_types` WRITE;
/*!40000 ALTER TABLE `food_types` DISABLE KEYS */;
INSERT INTO `food_types` VALUES (1,'Food','食品','2016-10-25 06:37:34','2016-10-25 06:37:34','',0),(2,'Drink','喝酒','2016-10-25 06:37:34','2016-10-25 06:37:34','',0);
/*!40000 ALTER TABLE `food_types` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `foods`
--

DROP TABLE IF EXISTS `foods`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `foods` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `restaurant_id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  `type_id` int(11) NOT NULL DEFAULT '1',
  `name` varchar(50) NOT NULL,
  `name_cn` varchar(50) NOT NULL,
  `price` decimal(19,2) NOT NULL,
  `description` varchar(2000) DEFAULT NULL,
  `description_cn` varchar(2000) DEFAULT NULL,
  `start_time` time DEFAULT NULL,
  `end_time` time DEFAULT NULL,
  `week_days` varchar(50) DEFAULT NULL,
  `current_order` int(11) DEFAULT '0',
  `quantity_per_day` int(11) DEFAULT NULL,
  `image_path` varchar(500) NOT NULL,
  `image_width` int(11) NOT NULL DEFAULT '200',
  `image_height` int(11) NOT NULL DEFAULT '200',
  `hash_tags` varchar(200) DEFAULT NULL,
  `create_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `update_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `update_by` varchar(50) DEFAULT NULL,
  `is_delete` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `FK_foods_food_categories_id` (`category_id`),
  KEY `FK_foods_food_types_id` (`type_id`),
  KEY `FK_foods_restaurants_id` (`restaurant_id`),
  CONSTRAINT `FK_foods_food_categories_id` FOREIGN KEY (`category_id`) REFERENCES `food_categories` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_foods_food_types_id` FOREIGN KEY (`type_id`) REFERENCES `food_types` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_foods_restaurants_id` FOREIGN KEY (`restaurant_id`) REFERENCES `restaurants` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=47 DEFAULT CHARSET=utf8 AVG_ROW_LENGTH=4096;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `foods`
--

LOCK TABLES `foods` WRITE;
/*!40000 ALTER TABLE `foods` DISABLE KEYS */;
INSERT INTO `foods` VALUES (1,1,1,1,'Finely Chopped Flying Maguro Bowl','飛天黒鮪三昧丼',248.00,'Fatty Tuna, Lean Tuna, Chopped Tuna','黒鮪中拖羅、黑鮪赤身、黑鮪魚腩蓉','00:00:00','23:00:00','1,2,3,4,5,6,7',1,NULL,'foods/Item_11.jpg',1080,719,'#toyosu','2016-10-24 08:13:29','2016-10-24 08:13:29','',0),(2,1,1,1,'Special Uni, Scallop & Salmon Roe Bowl','海膽帆立貝三文魚籽丼',348.00,'Uni, Salmon Roe, Scallop','海膽、三文魚籽、帆立貝','00:00:00','23:00:00','1,2,3,4,5,6,7',2,NULL,'foods/Item_12.jpg',1080,719,'#toyosu','2016-10-24 08:16:07','2016-10-24 08:16:07','',0),(3,1,1,1,'Lightly Broiled Sashimi Bowl','火炙海鮮丼',248.00,'Lean Tuna, Flounder Edge, Salmon Roe, Scallop, Salmon','黑鮪赤身、鰈魚裙邊、三文魚籽、帆立貝、三文魚','00:00:00','23:00:00','1,2,3,4,5,6,7',3,NULL,'foods/Item_13.jpg',1080,719,'#toyosu','2016-10-24 08:16:51','2016-10-24 08:16:51','',0),(4,1,1,1,'Hokkaido Whelk, Snow Crab & Sweet Shrimp Bowl','北海道響螺、松葉蟹、甜蝦丼',208.00,'Hokkaido Whelk、Sweet Shrimp, Snow Crab, Squid, Flying Fish Roe','北海道響螺、甜蝦、松葉蟹、魷魚、飛魚籽','00:00:00','23:00:00','1,2,3,4,5,6,7',4,NULL,'foods/Item_14.jpg',1080,719,'#toyosu','2016-10-25 06:37:35','2016-10-25 06:37:35','',0),(5,1,1,1,'Premium Uni Bowl  with Salmon Roe Onsen Egg (limit','謹製雲丹丼伴三文魚籽溫泉蛋（每天限定20客）',398.00,'Uni, Salmon Roe, Chopped Tuna','海膽、黑鮪魚腩蓉、三文魚籽','00:00:00','23:00:00','1,2,3,4,5,6,7',5,NULL,'foods/Item_15.jpg',1080,719,'#toyosu','2016-10-25 06:37:35','2016-10-25 06:37:35','',0),(6,1,2,1,'Finely Chopped Flying Maguro Bowl ','招牌刀琢飛天黑鮪魚腩蓉丼',198.00,'Chopped Tuna, Salmon Roe, Sweet Shrimp, Abalone, Soft-boiled Egg with Soy Sauce','\"黑鮪魚腩蓉、三文魚籽、甜蝦、鮑魚、\n特製醬油煮蛋\"','00:00:00','23:00:00','1,2,3,4,5,6,7',6,NULL,'foods/Item_01.jpg',1080,719,'#toyosu','2016-10-26 06:37:35','2016-10-25 06:37:35','',0),(7,1,2,1,'Flying Maguro Bowl','飛天黑鮪丼',248.00,'Lean Tuna, Chopped Tuna, Salmon Roe, Sweet Shrimp, Abalone, Soft-boiled Egg with Soy Sauce','黑鮪赤身、黑鮪魚腩蓉、三文魚籽、甜蝦、鮑魚、特製醬油煮蛋','00:00:00','23:00:00','1,2,3,4,5,6,7',7,NULL,'foods/Item_02.jpg',1080,719,'#toyosu','2016-10-25 06:37:35','2016-10-25 06:37:35','',0),(8,1,2,1,'Uni & Finely Chopped Flying Maguro Bowl','海膽招牌刀琢飛天黑鮪魚腩蓉丼',358.00,'Chopped Tuna, Uni, Salmon Roe, Sweet Shrimp, Abalone, Soft-boiled Egg with Soy Sauce','黑鮪魚腩蓉、海膽、三文魚籽、甜蝦、鮑魚、特製醬油煮蛋','00:00:00','23:00:00','1,2,3,4,5,6,7',8,NULL,'foods/Item_03.jpg',1080,719,'#toyosu','2016-10-25 06:37:35','2016-10-25 06:37:35','',0),(9,1,2,1,'Snow Crab & Finely Chopped Flying Maguro Bowl','松葉蟹招牌刀琢飛天黑鮪魚腩蓉丼',228.00,'Chopped Tuna,  Salmon Roe, Snow Crab, Sweet Shrimp, Abalone, Flying Fish Roe, Soft-boiled Egg with Soy Sauce','黑鮪魚腩蓉、三文魚籽、松葉蟹肉、甜蝦、鮑魚、特製醬油煮蛋','00:00:00','23:00:00','1,2,3,4,5,6,7',9,NULL,'foods/Item_05.jpg',1080,719,'#toyosu','2016-10-25 06:37:35','2016-10-25 06:37:35','',0),(10,1,2,1,'Scallop & Finely Chopped Flying Maguro Bowl','帆立貝招牌刀琢飛天黑鮪魚腩蓉丼',228.00,'Chopped Tuna, Scallop, Salmon Roe, Sweet Shrimp, Abalone, Soft-boiled Egg with Soy Sauce','黑鮪魚腩蓉、帆立貝、三文魚籽、甜蝦、鮑魚、特製醬油煮蛋','00:00:00','23:00:00','1,2,3,4,5,6,7',10,NULL,'foods/Item_06.jpg',1080,719,'#toyosu','2016-10-25 06:37:35','2016-10-25 06:37:35','',0),(11,1,2,1,'Salmon & Finely Chopped Flying Maguro Bowl','三文魚招牌刀琢飛天黑鮪魚腩蓉丼',188.00,'Chopped Tuna, Salmon, Salmon Roe, Sweet Shrimp, Soft-boiled Egg with Soy Sauce','黑鮪魚腩蓉、三文魚、三文魚籽、甜蝦、特製醬油煮蛋','00:00:00','23:00:00','1,2,3,4,5,6,7',11,NULL,'foods/Item_07.jpg',1080,719,'#toyosu','2016-10-25 06:37:35','2016-10-25 06:37:35','',0),(12,1,2,1,'Yellow Tail & Finely Chopped Flying Maguro Bowl','油甘魚招牌刀琢飛天黑鮪魚腩蓉丼',188.00,'Chopped Tuna, Yellow Tail, Salmon Roe, Sweet Shrimp, Soft-boiled Egg with Soy Sauce','黑鮪魚腩蓉、油甘魚、三文魚籽、甜蝦、特製醬油煮蛋','00:00:00','23:00:00','1,2,3,4,5,6,7',12,NULL,'foods/Item_08.jpg',1080,719,'#toyosu','2016-10-25 06:37:35','2016-10-25 06:37:35','',0),(13,1,3,1,'Toyosu Suisan Sashimi Bowl Mega Size','大漁豊洲水産海鮮丼 特盛',388.00,'Chopped Tuna, Lean Tuna,  Salmon Roe, Scallop, Botan Shrimp, Hokkaido Whelk, Yellow Tail, Snow Crab, Abalone, Vinegared Mackerel from Kumamoto, Flying Fish Roe, Soft-boiled Egg with Soy Sauce','黑鮪魚腩蓉、黑鮪赤身、三文魚籽、帆立貝、牡丹蝦、北海道響螺、油甘魚、松葉蟹、鮑魚、熊本縣產醋鯖魚、飛魚籽、特製醬油煮蛋','00:00:00','23:00:00','1,2,3,4,5,6,7',13,NULL,'foods/Item_09.jpg',1080,719,'#toyosu','2016-10-25 06:37:35','2016-10-25 06:37:35','',0),(14,1,3,1,'Toyosu Suisan Sashimi Bowl','豊洲水産海鮮丼',238.00,'Minced Flying Maguro,  Flying Maugro,  Salmon Roe, Scallop, Botan Shrimp, Hokkaido Whelk, Yellow Tail,  Snow Crab, Abalone,　Vinegared Mackerel from Kumamoto,  Salmon, Flying Fish Roe, Soft-boiled Egg with Soy Sauce','黑鮪魚腩蓉、黑鮪赤身、三文魚籽、帆立貝、牡丹蝦、北海道響螺、油甘魚，松葉蟹、鮑魚、三文魚、飛魚籽、熊本縣產醋鯖魚、特製醬油煮蛋','00:00:00','23:00:00','1,2,3,4,5,6,7',14,NULL,'foods/Item_10.jpg',1080,719,'#toyosu','2016-10-25 06:37:35','2016-10-25 06:37:35','',0),(15,1,4,1,'Prawn Tempura served with Salmon Roe Onsen Egg Bow','虎蝦天婦羅丼伴三文魚籽溫泉蛋',188.00,'Tiger Prawn, Perilla Leaf','虎蝦、大葉','00:00:00','23:00:00','1,2,3,4,5,6,7',15,NULL,'foods/Item_16.jpg',1080,719,'#toyosu','2016-10-25 06:37:35','2016-10-25 06:37:35','',0),(22,1,4,1,'Honey Plum Salty Flavor Ox Tongue Bowl','蜜餞梅干鹽味牛脷丼',118.00,'No information available.','无资料。','00:00:00','23:00:00','1,2,3,4,5,6,7',22,NULL,'foods/Item_17.jpg',1080,719,'#toyosu','2016-10-25 06:37:35','2016-10-25 06:37:35','',0),(23,1,4,1,'Grilled Eel Bowl & Japanese Style Cod Roe Omelet B','浦焼鰻魚、自家製明太子蛋卷丼',138.00,'No information available.','无资料。','00:00:00','23:00:00','1,2,3,4,5,6,7',23,NULL,'foods/Item_18.jpg',1080,719,'#toyosu','2016-10-25 06:37:35','2016-10-25 06:37:35','',0),(24,1,4,1,'Special Wagyu Bowl','薄燒日本和牛丼',178.00,'No information available.','无资料。','00:00:00','23:00:00','1,2,3,4,5,6,7',24,NULL,'foods/Item_19.jpg',1080,719,'#toyosu','2016-10-25 06:37:35','2017-01-16 04:07:58','',0),(34,1,4,1,'phu food','phu food cn',128.00,'description','desctiprion cn',NULL,NULL,'2,5,6',0,1000,'foods/member_ships.png',678,456,'#food #phu','2017-02-06 02:36:47','2017-02-06 06:27:26',NULL,0),(35,1,1,1,'sa','asdasd',12.00,'','','00:00:00','23:00:00','1,2,3,4,5,6,7',0,1000,'foods/MINE_Types.png',1001,491,'#food','2017-02-06 02:48:30','2017-02-06 02:53:31',NULL,0),(36,1,1,1,'asdf','特别',111.00,'','','02:00:00','00:00:00','1,2,3,4,5,6,7',0,1000,'foods/booking_3hb3MCz.png',638,306,'#food','2017-02-06 03:08:12','2017-02-06 04:23:15',NULL,0),(37,1,1,1,'zzz','zzz',123.00,'','','01:00:00','02:00:00','1,2,3,4,5,6,7',0,1000,'foods/Capture.PNG',820,553,'#food','2017-02-06 04:28:32','2017-02-06 05:45:20',NULL,0),(38,1,2,1,'bbb','bbb',444.00,'','','00:00:00','23:00:00','1,2,3,4,5,6,7',0,1000,'foods/14680734_548722488664844_8581964015279421895_n_xFPFlBr.jpg',720,960,'#food','2017-02-06 05:48:43','2017-02-06 09:30:05',NULL,0),(39,1,1,1,'vvv','123',123.00,'','','00:00:00','23:00:00','1,2,5,6,7',0,1000,'foods/booking_ks8vHOZ.png',638,306,'#food','2017-02-06 07:41:43','2017-02-13 02:59:43',NULL,0),(40,1,1,1,'Phu','234',234.00,'','','00:00:00','02:00:00','1,2,3,4,5,6,7',0,1000,'foods/14680734_548722488664844_8581964015279421895_n_OwcTANt.jpg',720,960,'#food','2017-02-06 08:02:34','2017-02-06 09:07:06',NULL,0),(41,1,1,1,'Linh','JHGF',44.00,'zxczxc','','01:00:00','15:00:00','1,2,3,4,5,6,7',0,1000,'foods/booking_cwM789i.png',638,306,'#food','2017-02-06 08:05:37','2017-02-06 08:46:04',NULL,0),(42,1,1,1,'Hihi','ahihih',234.00,'sdffs','','00:00:00','23:00:00','1,2,3,5',0,1000,'foods/14680734_548722488664844_8581964015279421895_n_sLwdvPr.jpg',720,960,'#food','2017-02-13 03:38:09','2017-02-13 06:02:10',NULL,0),(43,1,1,1,'zzzz','活动新闻',111.00,'','','00:00:00','23:30:00','1,2,3,4,5,6,7',0,1000,'foods/14680734_548722488664844_8581964015279421895_n_yRNf1B9.jpg',720,960,'#food','2017-02-13 06:16:59','2017-02-13 06:16:59',NULL,0),(44,1,1,1,'sdkhsdfjkh','海鲜',44.00,'123123','123123123123','00:00:00','23:00:00','1,2,3,4,5,6,7',0,1000,'foods/images.png',638,306,'#food','2017-02-13 06:21:34','2017-02-13 07:06:53',NULL,0),(45,1,1,1,'Testttt','Testttt',12.00,'des','des cn','00:00:00','23:00:00','1,2,3,4,5,6,7',0,1000,'foods/Capture_MEbGfl5.PNG',1026,884,'#food','2017-02-13 09:50:43','2017-02-15 07:55:38',NULL,0),(46,1,10,1,'Testttt2222','Testttt2222',6969.00,'adsdsssss','asdasd','00:00:00','23:30:00','1,2,3,4,5,6,7',0,1000,'foods/Capture_9gOW2gF.PNG',1026,884,'#food','2017-02-15 07:56:23','2017-02-15 07:56:23',NULL,0);
/*!40000 ALTER TABLE `foods` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `gift_seasons`
--

DROP TABLE IF EXISTS `gift_seasons`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gift_seasons` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `member_status_id` int(11) NOT NULL,
  `season_id` int(11) NOT NULL,
  `create_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `update_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `update_by` varchar(50) DEFAULT NULL,
  `is_delete` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `gift_seasons`
--

LOCK TABLES `gift_seasons` WRITE;
/*!40000 ALTER TABLE `gift_seasons` DISABLE KEYS */;
INSERT INTO `gift_seasons` VALUES (8,4,1,'2017-04-19 12:55:47','2017-04-19 12:55:47',NULL,0),(13,4,2,'2017-04-19 13:21:43','2017-04-19 13:21:43',NULL,0),(14,4,3,'2017-04-19 13:21:43','2017-04-19 13:21:43',NULL,0),(15,4,4,'2017-04-19 13:21:43','2017-04-19 13:21:43',NULL,0),(16,3,2,'2017-04-19 13:22:52','2017-04-19 13:22:52',NULL,0),(17,3,4,'2017-04-19 13:22:52','2017-04-19 13:22:52',NULL,0),(18,2,2,'2017-04-19 13:22:52','2017-04-19 13:22:52',NULL,0);
/*!40000 ALTER TABLE `gift_seasons` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `items`
--

DROP TABLE IF EXISTS `items`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `items` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `category_id` int(11) NOT NULL,
  `restaurant_id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `name_cn` varchar(50) DEFAULT NULL,
  `image_path` varchar(500) DEFAULT NULL,
  `create_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `update_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `update_by` varchar(50) DEFAULT NULL,
  `is_delete` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `FK_items_restaurants_id` (`restaurant_id`),
  KEY `FK_items_food_categories_id` (`category_id`),
  CONSTRAINT `FK_items_food_categories_id` FOREIGN KEY (`category_id`) REFERENCES `food_categories` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `items`
--

LOCK TABLES `items` WRITE;
/*!40000 ALTER TABLE `items` DISABLE KEYS */;
/*!40000 ALTER TABLE `items` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `membership_statuses`
--

DROP TABLE IF EXISTS `membership_statuses`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `membership_statuses` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `name_cn` varchar(50) NOT NULL,
  `content` varchar(2000) DEFAULT NULL,
  `content_cn` varchar(2000) DEFAULT NULL,
  `content_special` varchar(2000) DEFAULT NULL,
  `content_special_cn` varchar(2000) DEFAULT NULL,
  `goal_point` int(11) NOT NULL,
  `image_path` varchar(500) NOT NULL,
  `hex_color` varchar(50) DEFAULT NULL,
  `create_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `update_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `update_by` varchar(50) DEFAULT NULL,
  `is_delete` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `membership_statuses`
--

LOCK TABLES `membership_statuses` WRITE;
/*!40000 ALTER TABLE `membership_statuses` DISABLE KEYS */;
INSERT INTO `membership_statuses` VALUES (1,'Bronze','青銅','•    Receive our updated news and special coupons','ahihi',NULL,NULL,0,'memberships/bronze.png','#e69d5e','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,0),(2,'Silver','白銀','•    Always enjoy 5%OFF with “roan style” services\n•    Always enjoy Free Shipping on roan style selections online store\n•    Always enjoy free first drink at restaurant “割烹 櫓杏 Kappo ro ann”\n•    Receive our updated news and special coupons\n•    Seasonal Special Gift Box form Japan (worth HK$1,000) for 1 seasons.',NULL,NULL,NULL,1000,'memberships/silver.png','#b3b3b3','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,0),(3,'Gold','黃金','•    Always enjoy 10%OFF with “roan style” services\n•    Always enjoy Free Shipping on roan style selections online store\n•    Always enjoy free first drink at restaurant “割烹 櫓杏 Kappo ro ann”\n•    Receive our updated news and special coupons\n•    Invited our special parties and events such as new menu tasting event\n•    Seasonal Special Gift Box form Japan (worth HK$1,000) for 2 seasons.',NULL,NULL,NULL,3000,'memberships/gold.png','#f0c400','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,0),(4,'Diamond','鑽石','•    Always enjoy 10%OFF with “roan style” services\n•    Always enjoy Free Shipping on roan style selections online store\n•    Always enjoy free first drink at restaurant “割烹 櫓杏 Kappo ro ann”\n•    Receive our updated news and special coupons\n•    Invited our special parties and events such as new menu tasting event\n•    Seasonal Special Gift Box form Japan (worth HK$1,000) for 4 seasons.\n•    Around trip ticket to Japan\nSpecial condition only for Diamond level\n•    Only diamond members can enjoy special benefit of trip to Japan!\n•    Condition of the ticket is below and detail will be in T&C*3.\n•    (Terms and Conditions subject to change depend on the seasons)\n•    Round trip ticket following inclusions:\n•    Return economy airfare by direct air flight* (Airline may change depend on season)\n•    Return Flight Hong Kong to Fukuoka\n•    Return Express tickets Fukuoka to Kumamoto\n•    Tax, Service change and surcharge in not included on this ticket.\n•    Flight Schedule follows in the official web site of the airline.\n•    The Conditional and T&C with more detail will be updated once the condition will be fixed.','•    Always enjoy 10%OFF with “roan style” services\n•    Always enjoy Free Shipping on roan style selections online store\n•    Always enjoy free first drink at restaurant “割烹 櫓杏 Kappo ro ann”\n•    Receive our updated news and special coupons\n•    Invited our special parties and events such as new menu tasting event\n•    Seasonal Special Gift Box form Japan (worth HK$1,000) for 4 seasons.\n•    Around trip ticket to Japan\nSpecial condition only for Diamond level\n•    Only diamond members can enjoy special benefit of trip to Japan!\n•    Condition of the ticket is below and detail will be in T&C*3.\n•    (Terms and Conditions subject to change depend on the seasons)\n•    Round trip ticket following inclusions:\n•    Return economy airfare by direct air flight* (Airline may change depend on season)\n•    Return Flight Hong Kong to Fukuoka\n•    Return Express tickets Fukuoka to Kumamoto\n•    Tax, Service change and surcharge in not included on this ticket.\n•    Flight Schedule follows in the official web site of the airline.\n•    The Conditional and T&C with more detail will be updated once the condition will be fixed.','Only diamond members can enjoy special benefit of trip to Japan!\nCondition of the ticket is below and detail will be in T&C*3.\n(Terms and Conditions subject to change depend on the seasons)\nRound trip ticket following inclusions:\nReturn economy airfare by direct air flight* (Airline may change depend on season)\nReturn Flight Hong Kong to Fukuoka\nReturn Express tickets Fukuoka to Kumamoto\nTax, Service change and surcharge in not included on this ticket.\nFlight Schedule follows in the official web site of the airline.\nThe Conditional and T&C with more detail will be updated once the condition will be fixed.',NULL,7000,'memberships/diamond.png','#add8e5','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,0);
/*!40000 ALTER TABLE `membership_statuses` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `memberships`
--

DROP TABLE IF EXISTS `memberships`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `memberships` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `device_id` varchar(500) DEFAULT NULL,
  `number` int(11) DEFAULT NULL,
  `barcode` varchar(50) DEFAULT NULL,
  `qr_code` varchar(50) DEFAULT NULL,
  `type` tinyint(1) DEFAULT NULL,
  `create_date` datetime DEFAULT CURRENT_TIMESTAMP,
  `update_date` datetime DEFAULT NULL,
  `is_delete` varchar(45) DEFAULT NULL,
  `update_by` smallint(1) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=46 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `memberships`
--

LOCK TABLES `memberships` WRITE;
/*!40000 ALTER TABLE `memberships` DISABLE KEYS */;
INSERT INTO `memberships` VALUES (39,'4CA8E307-E075-4AB9-811D-710559FF1984',88276493,NULL,NULL,0,'2017-01-23 09:55:10','2017-01-23 09:55:10','0',NULL),(40,'C7E7DC2A-1BD1-48A9-B5A2-F21E9B051657',35494115,NULL,NULL,0,'2017-01-24 08:53:38','2017-01-24 08:53:38','0',NULL),(41,'DB59DA75-6A9F-42A9-8686-8C775A441B42',55440047,NULL,NULL,0,'2017-01-25 02:59:31','2017-01-25 02:59:31','0',NULL),(42,'com.ibs.toyosusuisan.357737926047990',14135906,NULL,NULL,1,'2017-02-03 07:23:39','2017-02-03 07:23:39','0',NULL),(43,'1231231221',22897247,NULL,NULL,0,'2017-03-02 02:56:14','2017-03-02 02:56:14','0',NULL),(44,'com.ibs.roann.358714986786243',19435077,NULL,NULL,1,'2017-03-07 03:30:10','2017-03-07 03:30:10','0',NULL),(45,'018E5132-1D49-44EE-A2F5-951FFEB16F7B',81719079,NULL,NULL,0,'2017-03-09 07:04:49','2017-03-09 07:04:49','0',NULL);
/*!40000 ALTER TABLE `memberships` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `operations`
--

DROP TABLE IF EXISTS `operations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `operations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `area_id` int(11) NOT NULL,
  `type_id` int(11) NOT NULL DEFAULT '3' COMMENT '1: Mente; 2: Group; 3: Restaurant',
  `last_name` varchar(30) NOT NULL,
  `first_name` varchar(30) NOT NULL,
  `image_path` varchar(500) DEFAULT NULL,
  `birthday` datetime DEFAULT NULL,
  `sex` tinyint(1) NOT NULL DEFAULT '2' COMMENT '0: Male; 1:Female; 2:Unknow',
  `zip_code` varchar(10) DEFAULT NULL,
  `address` varchar(500) DEFAULT NULL,
  `email` varchar(50) NOT NULL,
  `login_password` varchar(50) NOT NULL,
  `phone` varchar(20) NOT NULL,
  `last_res_id` int(11) DEFAULT NULL,
  `create_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `update_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `update_by` varchar(50) DEFAULT NULL,
  `is_delete` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `FK_operations_areas_id` (`area_id`),
  CONSTRAINT `FK_operations_areas_id` FOREIGN KEY (`area_id`) REFERENCES `areas` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 AVG_ROW_LENGTH=8192;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `operations`
--

LOCK TABLES `operations` WRITE;
/*!40000 ALTER TABLE `operations` DISABLE KEYS */;
INSERT INTO `operations` VALUES (1,40,2,1,'Mente','Higo','','2016-11-16 08:40:09',0,'','','adminmente@toyosu.com','toyosuMente@1234','0987921104',1,'2016-11-02 08:40:43','2017-04-10 06:16:33','',0),(2,45,2,1,'Admin','Higo','','2016-11-16 08:40:09',0,'','','admin@higo.com','111qqq','12345678',1,'2016-11-02 08:40:43','2017-02-13 01:39:33','',0);
/*!40000 ALTER TABLE `operations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `operations_restaurants`
--

DROP TABLE IF EXISTS `operations_restaurants`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `operations_restaurants` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `operation_id` int(11) NOT NULL,
  `restaurant_id` int(11) NOT NULL,
  `description` varchar(500) DEFAULT NULL,
  `description_cn` varchar(500) DEFAULT NULL,
  `create_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `update_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `update_by` varchar(50) DEFAULT NULL,
  `is_delete` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `FK_operations_restaurants_operations_id` (`operation_id`),
  KEY `FK_operations_restaurants_restaurants_id` (`restaurant_id`),
  CONSTRAINT `FK_operations_restaurants_operations_id` FOREIGN KEY (`operation_id`) REFERENCES `operations` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_operations_restaurants_restaurants_id` FOREIGN KEY (`restaurant_id`) REFERENCES `restaurants` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `operations_restaurants`
--

LOCK TABLES `operations_restaurants` WRITE;
/*!40000 ALTER TABLE `operations_restaurants` DISABLE KEYS */;
/*!40000 ALTER TABLE `operations_restaurants` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pos_products`
--

DROP TABLE IF EXISTS `pos_products`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pos_products` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `product_code` varchar(50) NOT NULL,
  `name` varchar(100) NOT NULL,
  `create_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `update_date` datetime DEFAULT NULL,
  `update_by` varchar(50) DEFAULT NULL,
  `is_delete` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `product_code_UNIQUE` (`product_code`)
) ENGINE=InnoDB AUTO_INCREMENT=4721 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pos_products`
--

LOCK TABLES `pos_products` WRITE;
/*!40000 ALTER TABLE `pos_products` DISABLE KEYS */;
INSERT INTO `pos_products` VALUES (4662,'3002','Risotto','2017-04-19 11:30:33',NULL,NULL,0),(4663,'2021','Lunch Set A','2017-04-19 11:30:33',NULL,NULL,0),(4664,'1002','Chix Nuggets','2017-04-19 11:30:34',NULL,NULL,0),(4665,'2001','5Greens Salad','2017-04-19 11:30:34',NULL,NULL,0),(4666,'11004','S.pellegrino','2017-04-19 11:30:34',NULL,NULL,0),(4667,'11005','Aqua Panna','2017-04-19 11:30:35',NULL,NULL,0),(4668,'1001','Biscuits','2017-04-19 11:30:35',NULL,NULL,0),(4669,'AAA8014','Book A','2017-04-19 11:30:35',NULL,NULL,0),(4670,'98001','eRunConnect','2017-04-19 11:30:35',NULL,NULL,0),(4671,'98002','eRunConnect','2017-04-19 11:30:35',NULL,NULL,0),(4672,'15050','Tea/Apple Rooibos(S)','2017-04-19 11:30:35',NULL,NULL,0),(4673,'15052','Coke Light(S)','2017-04-19 11:30:35',NULL,NULL,0),(4674,'11001','Coke','2017-04-19 11:30:36',NULL,NULL,0),(4675,'9013','Spritzer','2017-04-19 11:30:36',NULL,NULL,0),(4676,'9023','Maragarita Cranberry /750ML','2017-04-19 11:30:36',NULL,NULL,0),(4677,'AA8014','Book B','2017-04-19 11:30:36',NULL,NULL,0),(4678,'1003','Truffle Fries','2017-04-19 11:30:36',NULL,NULL,0),(4679,'16009','Cockage','2017-04-19 11:30:37',NULL,NULL,0),(4680,'9002','Maragarita / Cranberry','2017-04-19 11:30:37',NULL,NULL,0),(4681,'1004','Arancini','2017-04-19 11:30:37',NULL,NULL,0),(4682,'10009','Virgin Mule','2017-04-19 11:30:37',NULL,NULL,0),(4683,'10001','Bloody Babe','2017-04-19 11:30:38',NULL,NULL,0),(4684,'5009','Chimichurri','2017-04-19 11:30:38',NULL,NULL,0),(4685,'14024','Hot Lemon Water','2017-04-19 11:30:38',NULL,NULL,0),(4686,'10010','Virgin Mule / 750ml','2017-04-19 11:30:38',NULL,NULL,0),(4687,'16007','Open drink','2017-04-19 11:30:38',NULL,NULL,0),(4688,'9005','Bloody Mary / Mexican','2017-04-19 11:30:38',NULL,NULL,0),(4689,'10012','Virginrita Lime / 750ML','2017-04-19 11:30:39',NULL,NULL,0),(4690,'11003','Sprite','2017-04-19 11:30:39',NULL,NULL,0),(4691,'9003','Maragarita / Yuzu','2017-04-19 11:30:39',NULL,NULL,0),(4692,'9012','Pimm\'s Cup','2017-04-19 11:30:39',NULL,NULL,0),(4693,'15047','Tea/Peppermint(S)','2017-04-19 11:30:39',NULL,NULL,0),(4694,'5019','party steak','2017-04-19 11:30:39',NULL,NULL,0),(4695,'9016','Moscow Mule / 750ml','2017-04-19 11:30:39',NULL,NULL,0),(4696,'15049','Tea/Peach Green(S)','2017-04-19 11:30:39',NULL,NULL,0),(4697,'15063','Ice Milk Tea(S)','2017-04-19 11:30:40',NULL,NULL,0),(4698,'15048','Ice L Water (S)','2017-04-19 11:30:40',NULL,NULL,0),(4699,'9017','Whisky Ale','2017-04-19 11:30:40',NULL,NULL,0),(4700,'10011','Virginrita Cranberry /750ML','2017-04-19 11:30:40',NULL,NULL,0),(4701,'9022','Maragarita Lime /750ML','2017-04-19 11:30:40',NULL,NULL,0),(4702,'6022','toast','2017-04-19 11:30:40',NULL,NULL,0),(4703,'10002','Virginrita / Lime','2017-04-19 11:30:40',NULL,NULL,0),(4704,'15065','Ice coffee','2017-04-19 11:30:41',NULL,NULL,0),(4705,'15051','Coke(S)','2017-04-19 11:30:41',NULL,NULL,0),(4706,'12005','Cht. Petit Boyer 2013','2017-04-19 11:30:41',NULL,NULL,0),(4707,'9015','Moscow Mule','2017-04-19 11:30:41',NULL,NULL,0),(4708,'11002','Coke Light','2017-04-19 11:30:41',NULL,NULL,0),(4709,'5006','Mustard Cream','2017-04-19 11:30:41',NULL,NULL,0),(4710,'14011','Espresso','2017-04-19 11:30:41',NULL,NULL,0),(4711,'9004','Bloody Mary / Classic','2017-04-19 11:30:41',NULL,NULL,0),(4712,'15045','Milk Tea (S)','2017-04-19 11:30:41',NULL,NULL,0),(4713,'6021','F BREAD','2017-04-19 11:30:42',NULL,NULL,0),(4714,'10004','Virginrita / Yuzu','2017-04-19 11:30:42',NULL,NULL,0),(4715,'5007','Hot Pepper','2017-04-19 11:30:42',NULL,NULL,0),(4716,'14019','Iced Choco','2017-04-19 11:30:42',NULL,NULL,0),(4717,'15039','Prime Rib  8','2017-04-19 11:30:42',NULL,NULL,0),(4718,'1005','Fish && Chip','2017-04-19 11:30:42',NULL,NULL,0),(4719,'13011','(SHK) Arnold Palmer','2017-04-19 11:30:43',NULL,NULL,0),(4720,'10003','Virginrita / Cranberry','2017-04-19 11:30:43',NULL,NULL,0);
/*!40000 ALTER TABLE `pos_products` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `prizes`
--

DROP TABLE IF EXISTS `prizes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `prizes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `restaurant_id` int(11) NOT NULL,
  `request_point` int(11) NOT NULL,
  `title` varchar(200) NOT NULL,
  `title_cn` varchar(200) NOT NULL,
  `content` varchar(500) NOT NULL,
  `content_cn` varchar(500) DEFAULT NULL,
  `condition` varchar(2000) NOT NULL,
  `condition_cn` varchar(2000) NOT NULL,
  `from_date` datetime NOT NULL,
  `to_date` datetime NOT NULL,
  `start_time` time DEFAULT NULL,
  `end_time` time DEFAULT NULL,
  `week_days` varchar(50) DEFAULT NULL COMMENT 'Example week day index: 1,2,4,5',
  `current_register` int(11) NOT NULL DEFAULT '0',
  `quantity` int(11) NOT NULL COMMENT 'Maximium',
  `image_path` varchar(500) DEFAULT NULL,
  `image_width` int(11) NOT NULL DEFAULT '200',
  `image_height` int(11) NOT NULL DEFAULT '200',
  `link_address` varchar(500) DEFAULT NULL,
  `is_mail_notify` tinyint(1) NOT NULL DEFAULT '1' COMMENT '1:Recieve,0: No Recieve',
  `is_sms_notify` tinyint(1) NOT NULL DEFAULT '1' COMMENT '1:Recieve,0: No Recieve',
  `is_app_notify` tinyint(1) NOT NULL DEFAULT '1' COMMENT '1:Recieve,0: No Recieve',
  `is_already_send` tinyint(1) NOT NULL DEFAULT '0' COMMENT '1:Recieve,0: No Recieve',
  `delivery_type` tinyint(1) DEFAULT '1',
  `delivery_duration_days` int(11) DEFAULT '3',
  `hash_tags` varchar(200) DEFAULT NULL,
  `create_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `update_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `update_by` varchar(50) DEFAULT NULL,
  `is_delete` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0:Active, 1: Delete',
  PRIMARY KEY (`id`),
  KEY `FK_prizes_restaurants_id` (`restaurant_id`),
  CONSTRAINT `FK_prizes_restaurants_id` FOREIGN KEY (`restaurant_id`) REFERENCES `restaurants` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=utf8 AVG_ROW_LENGTH=8192;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `prizes`
--

LOCK TABLES `prizes` WRITE;
/*!40000 ALTER TABLE `prizes` DISABLE KEYS */;
INSERT INTO `prizes` VALUES (12,1,1000,'1000pt - Test','Buy 1 get 1','asdoaskldnsal','asdas','T&C\n','T&C\n','2016-12-28 00:00:00','2018-12-07 00:00:00','00:00:00','23:00:00','1,2,3,4,5,6,7',57,100,'prizes/1_04.jpg',750,651,NULL,1,1,1,0,1,3,NULL,'2016-12-28 07:07:14','2017-04-18 02:12:46',NULL,0),(14,1,900,'900pt - Test','Buy 1 get 1','kldnlaksd','sdnklfsdklfnlk','sffsdf','sdfsd','2016-12-28 00:00:00','2018-12-07 00:00:00','00:00:00','23:00:00','1,2,3,4,5,6,7',18,100,'prizes/1_04.jpg',750,651,NULL,1,1,1,0,1,3,NULL,'2016-12-28 07:07:14','2017-03-14 07:11:48',NULL,0),(15,1,800,'800pt - Test','Buy 1 get 1','kldnlaksd','sdnklfsdklfnlk','sffsdf','sdfsd','2016-12-28 00:00:00','2018-12-07 00:00:00','00:00:00','23:00:00','1,2,3,4,5,6,7',16,100,'prizes/1_04.jpg',750,651,NULL,1,1,1,0,1,3,NULL,'2016-12-28 07:07:14','2017-03-14 07:20:50',NULL,0),(16,1,700,'700pt - Test','Buy 1 get 1','kldnlaksd','sdnklfsdklfnlk','sffsdf','sdfsd','2016-12-28 00:00:00','2018-12-07 00:00:00','00:00:00','23:00:00','1,2,3,4,5,6,7',17,100,'prizes/1_04.jpg',750,651,NULL,1,1,1,0,1,3,NULL,'2016-12-28 07:07:14','2017-04-11 08:19:30',NULL,0),(17,1,600,'600pt - Test','Buy 1 get 1','kldnlaksd','sdnklfsdklfnlk','sffsdf','sdfsd','2016-12-28 00:00:00','2018-12-07 00:00:00','00:00:00','23:00:00','1,2,3,4,5,6,7',16,100,'prizes/1_04.jpg',750,651,NULL,1,1,1,0,1,3,NULL,'2016-12-28 07:07:14','2017-04-07 10:16:37',NULL,0),(18,1,400,'400pt - Test','Buy 1 get 1','kldnlaksd','sdnklfsdklfnlk','sffsdf','sdfsd','2016-12-28 00:00:00','2018-12-07 00:00:00','00:00:00','23:00:00','1,2,3,4,5,6,7',18,100,'prizes/1_04.jpg',750,651,NULL,1,1,1,0,1,3,NULL,'2016-12-28 07:07:14','2017-04-18 02:20:50',NULL,0),(19,1,300,'300pt - Test','Buy 1 get 1','kldnlaksd','sdnklfsdklfnlk','sffsdf','sdfsd','2016-12-28 00:00:00','2018-12-07 00:00:00','00:00:00','23:00:00','1,2,3,4,5,6,7',34,100,'prizes/prize_o4M2ylG.jpg',799,1200,'asdkadkjshkdas',1,1,1,0,2,3,NULL,'2016-12-28 07:07:14','2017-04-13 05:55:23',NULL,0),(20,1,200,'200pt - Test','ahaihiahiahiahi','kldnlaksd','sdnklfsdklfnlk','sffsdf','sdfsd','2016-12-28 00:00:00','2018-12-07 00:00:00','00:00:00','23:00:00','1,2,3,4,5,6,7',82,100,'prizes/prize.jpg',799,1200,'http://higo.ibsv.vn/product/turtle-pattern-food-container-1-set-for-2',1,1,1,0,1,3,NULL,'2016-12-28 07:07:14','2017-04-18 03:29:48',NULL,0),(21,1,500,'500pt - Test','500 pts - asdadjakdsbk cn','View product detail','View product detail','ads','ads','2017-06-01 00:00:00','2025-01-01 00:00:00','00:00:00','23:30:00','1,2,3,4,5,6,7',1,1000,'prizes/Simulator_Screen_Shot_Mar_27_2017_2.54.36_PM.png',750,1334,'http://www.google.com',0,0,0,0,1,3,NULL,'2017-03-29 04:09:50','2017-03-31 03:34:26',NULL,0);
/*!40000 ALTER TABLE `prizes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `reservations`
--

DROP TABLE IF EXISTS `reservations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `reservations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `booking_id` int(11) NOT NULL,
  `customer_id` int(11) DEFAULT NULL,
  `client_id` int(11) NOT NULL,
  `event_id` int(11) DEFAULT NULL,
  `unit_id` int(11) NOT NULL DEFAULT '0',
  `status_id` tinyint(1) DEFAULT NULL,
  `start_date_time` datetime NOT NULL,
  `end_date_time` datetime NOT NULL,
  `number_of_seat` int(11) NOT NULL,
  `hash` varchar(200) NOT NULL DEFAULT '0',
  `code` varchar(50) NOT NULL,
  `is_confirmed` tinyint(1) NOT NULL,
  `create_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `update_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `update_by` varchar(50) DEFAULT NULL,
  `is_delete` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `FK_reservations_restaurants_id` (`booking_id`)
) ENGINE=InnoDB AUTO_INCREMENT=402 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `reservations`
--

LOCK TABLES `reservations` WRITE;
/*!40000 ALTER TABLE `reservations` DISABLE KEYS */;
INSERT INTO `reservations` VALUES (399,604,17,68,1,5,1,'2017-04-15 09:00:00','2017-04-15 10:00:00',5,'2d3ea1e227d91a66350a1df61c2d87b6','blggr4x2',1,'2017-04-14 03:12:40','2017-04-14 03:12:40',NULL,0),(400,607,17,68,1,2,1,'2017-05-01 11:00:00','2017-05-01 12:00:00',5,'ae999b60ee0383871a8514d8ee9c635d','blgguri0',1,'2017-04-14 07:44:29','2017-04-14 07:44:29',NULL,0),(401,49,17,21,5,21,1,'2017-04-19 11:00:00','2017-04-19 11:05:00',6,'dcc075625885d21d7509e356cfe29187','cqd1c19k',1,'2017-04-14 07:45:02','2017-04-14 07:45:02',NULL,0);
/*!40000 ALTER TABLE `reservations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `restaurant_points`
--

DROP TABLE IF EXISTS `restaurant_points`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `restaurant_points` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `restaurant_id` int(11) NOT NULL,
  `code` varchar(30) NOT NULL COMMENT 'Unique',
  `issue_date` datetime NOT NULL,
  `point` int(11) NOT NULL,
  `qr_code` varchar(50) NOT NULL,
  `comment` varchar(500) DEFAULT NULL,
  `comment_cn` varchar(500) DEFAULT NULL,
  `create_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `update_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `update_by` varchar(50) DEFAULT NULL,
  `is_delete` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0:Active, 1:Delete',
  PRIMARY KEY (`id`),
  UNIQUE KEY `Code_Unique` (`code`),
  KEY `FK_restaurant_points_restaurants_id` (`restaurant_id`),
  CONSTRAINT `FK_restaurant_points_restaurants_id` FOREIGN KEY (`restaurant_id`) REFERENCES `restaurants` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8 AVG_ROW_LENGTH=4096;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `restaurant_points`
--

LOCK TABLES `restaurant_points` WRITE;
/*!40000 ALTER TABLE `restaurant_points` DISABLE KEYS */;
INSERT INTO `restaurant_points` VALUES (1,1,'uzeD6SFabYNjmJ6cW6jFXEHa6t6UZe','2017-02-20 00:00:00',100,'d8b8b563af16bdfd88f77fdcc06c6167','Scan - Add 100 points','Scan - Add 100 points','2017-02-21 01:37:29','2017-02-21 01:37:29',NULL,0),(2,1,'m47Xy344zPPZZj9UdmeJmBZsZuDYfM','2017-03-11 00:00:00',100,'2ed9a9e835e1fc22940137d8aeada389','ád','ád','2017-02-21 01:39:30','2017-02-21 01:39:30',NULL,0),(3,1,'uArCn4fHGRYXNEA4ztcejB8vPm8JJ4','2017-01-31 00:00:00',20,'47499c80e8e379b03d4654a764048d81','dfgdghfghfgh','ufufufug','2017-02-21 03:32:14','2017-02-22 06:31:51',NULL,0),(4,1,'f2LGapsVKZQcAHLBddp2SPM8Ccmp5k','2017-03-13 00:00:00',200,'3ba8d6c8e66eaca3e7a7ffbf8b38c670','asdsad','asdasd','2017-03-13 06:05:37','2017-03-13 06:05:37',NULL,0),(5,1,'eGVxqwShdcfAvUZzwggrn5ZHShataw','2017-03-13 00:00:00',300,'93fb2c5b0cca961a6b1f85a6cf8a8226','asdasd','adad','2017-03-13 06:12:08','2017-03-13 06:12:08',NULL,0),(6,1,'RDjVq3fKuLTBVBvr8ewH5xX9QURrEF','2017-03-13 00:00:00',400,'a0fe5d5d0bba98fc8a639d8095141f86','wweqw','qweqwe','2017-03-13 06:25:17','2017-03-13 06:25:17',NULL,0),(7,1,'zz3yzjr94HqtqRdKMy8rZZNteZ9vEf','2017-03-14 00:00:00',333,'68f4964324d7cd5d22496828f7a83463','dfsdf','s dfsdf','2017-03-13 06:31:35','2017-03-13 06:31:35',NULL,0),(8,1,'VvMUsy8uwNTUuCmpGBBkkfezXDghUz','2017-03-13 00:00:00',20,'cf9e56ee2371aba71ba1b63c138bc1d0','sdf','sfs','2017-03-13 06:41:46','2017-03-13 06:41:46',NULL,0),(9,1,'Egn9Cf8c4pgAF469pUeESjweqguxVn','2017-03-13 00:00:00',111,'c30952032ab90a1ffbcbceae2c5b1f93','asdasd','adasd','2017-03-13 06:53:06','2017-03-13 06:53:06',NULL,0),(10,1,'jZBf6QXKfRu93nfCJWx5wV7Adfv5Q8','2017-03-13 00:00:00',121,'6296d5ff75e46022b8b00a96681195cf','asdasda a','sdadasd','2017-03-13 06:56:01','2017-03-13 06:56:01',NULL,0),(11,1,'NKeAM4ujTbDcj3wBxFeXzbpwmNDs2G','2017-03-14 00:00:00',100,'b703c4bfe469bcfe0a5e8c4ee462e739','asd','ads','2017-03-14 05:39:49','2017-03-14 05:39:49',NULL,0);
/*!40000 ALTER TABLE `restaurant_points` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `restaurant_schedules`
--

DROP TABLE IF EXISTS `restaurant_schedules`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `restaurant_schedules` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `restaurant_id` int(11) NOT NULL,
  `start_week_day` int(11) NOT NULL,
  `end_week_day` int(11) NOT NULL,
  `start_time` time NOT NULL,
  `end_time` time NOT NULL,
  `latest_order_time` time NOT NULL,
  `create_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `update_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `update_by` varchar(50) DEFAULT NULL,
  `is_delete` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `FK_restaurant_schedules_restaurants_id` (`restaurant_id`),
  CONSTRAINT `FK_restaurant_schedules_restaurants_id` FOREIGN KEY (`restaurant_id`) REFERENCES `restaurants` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `restaurant_schedules`
--

LOCK TABLES `restaurant_schedules` WRITE;
/*!40000 ALTER TABLE `restaurant_schedules` DISABLE KEYS */;
INSERT INTO `restaurant_schedules` VALUES (1,1,0,0,'18:00:00','23:30:00','00:00:00','2016-12-20 09:31:07','2016-12-20 09:31:07',NULL,0);
/*!40000 ALTER TABLE `restaurant_schedules` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `restaurants`
--

DROP TABLE IF EXISTS `restaurants`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `restaurants` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `area_id` int(11) NOT NULL,
  `station_id` int(11) NOT NULL,
  `sb_company_login` varchar(100) NOT NULL,
  `sb_username` varchar(50) NOT NULL,
  `sb_user_password` varchar(50) NOT NULL,
  `sb_api_key` varchar(500) NOT NULL,
  `code` varchar(10) NOT NULL,
  `name` varchar(100) NOT NULL,
  `name_cn` varchar(100) DEFAULT NULL,
  `image_path` varchar(500) NOT NULL,
  `contact_person` varchar(100) DEFAULT NULL,
  `email` varchar(50) NOT NULL,
  `phone` varchar(20) NOT NULL,
  `address` varchar(500) NOT NULL,
  `address_cn` varchar(500) DEFAULT NULL,
  `description` varchar(2000) DEFAULT NULL,
  `description_cn` varchar(2000) DEFAULT NULL,
  `google_map_url` varchar(500) DEFAULT NULL,
  `longitude` decimal(11,8) DEFAULT NULL,
  `latitude` decimal(11,8) DEFAULT NULL,
  `week_days` varchar(50) DEFAULT NULL,
  `start_time` time DEFAULT NULL,
  `end_time` time DEFAULT NULL,
  `limit_days_book` int(11) DEFAULT NULL,
  `confirm_reserve` varchar(500) DEFAULT NULL,
  `confirm_reserve_cn` varchar(2000) DEFAULT NULL,
  `create_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `update_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `update_by` varchar(50) DEFAULT NULL,
  `is_delete` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `FK_restaurants_areas_id` (`area_id`),
  KEY `FK_restaurants_stations_id` (`station_id`),
  CONSTRAINT `FK_restaurants_areas_id` FOREIGN KEY (`area_id`) REFERENCES `areas` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_restaurants_stations_id` FOREIGN KEY (`station_id`) REFERENCES `stations` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 AVG_ROW_LENGTH=16384;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `restaurants`
--

LOCK TABLES `restaurants` WRITE;
/*!40000 ALTER TABLE `restaurants` DISABLE KEYS */;
INSERT INTO `restaurants` VALUES (1,4,1,'higodemo','admin','123456','f4676f2e972b72ffe5f3f88ccbdae32a0b03d6b36fc493aabe7590c94e046ce3','ROANN0001','Kappo ro ann - TOYOSU','割烹 櫓杏','restaurants/res.jpg','https://www.facebook.com/toyosusuisanhk/','info@higo.hk.com','+852 2818 0031','Shop 2801, 28/F, iSquare, 63 Nathan Road, Tsim Sha Tsui, Kowloon, Hong Kong','香港九龍尖沙咀彌敦道63號iSQUARE 國際廣場28樓2801號舖','Toyosu opened very first shop in Hong Kong operated by Professional Maguro importer and wholesaler in Japan managed by Mr. Ito Hiro. Using high-quality Maguro and the other ingredients directly brought by Air flight to Hong Kong.\n\nToyosu uses ingredients from direct delivered by daily air flight from the Fish market in Japan to Hong Kong by ensuring freshness and quality standards. Toyosu provides more than 30 special Maguro and other fresh fish and other bowls(Don) and dishes. Seafood is carefully selected by the professional in Japan. Also using high quality of rice grains from Yamagata Prefecture in Japan which best match to the Maguro Bowls.\nEvery bowl is accompanied with the homemade original miso soup made by fresh fish bone, a full of fish flavor is definitely the best match!\nAlso providing the original unique a la carte such as Maguro fish eyes, Maguro heads, and Maguro tails which directly imported from Japan too.\n\n“You can get all time discount for memberships on Apps, seasonal limited coupons. latest news, updated menus and more.\"','日本海鲜餐厅香港特色：供应午餐，晚餐和饮料服务：餐饮，外送，团体用餐，适合儿童，外带，服务员，欢迎光临','',114.16978120,22.29687750,'1,2,3,4,5,6,7','06:00:00','23:00:00',30,'Thank you for your reservation!\nWe will make a phone call once the booking will be confirmed.','感謝您的預約！ \n我們將給您打電話一旦預訂確認。','2016-10-27 14:35:29','2016-11-16 01:52:18','',0),(2,4,1,'higodemo','admin','123456','f4676f2e972b72ffe5f3f88ccbdae32a0b03d6b36fc493aabe7590c94e046ce3','ROANN0002','Kappo ro ann - PROD','割烹 櫓杏','restaurants/res.jpg','https://www.facebook.com/toyosusuisanhk/','info@higo.hk.com','+852 2818 0031','Shop 2801, 28/F, iSquare, 63 Nathan Road, Tsim Sha Tsui, Kowloon, Hong Kong','香港九龍尖沙咀彌敦道63號iSQUARE 國際廣場28樓2801號舖','Toyosu opened very first shop in Hong Kong operated by Professional Maguro importer and wholesaler in Japan managed by Mr. Ito Hiro. Using high-quality Maguro and the other ingredients directly brought by Air flight to Hong Kong.\r\n\r\nToyosu uses ingredients from direct delivered by daily air flight from the Fish market in Japan to Hong Kong by ensuring freshness and quality standards. Toyosu provides more than 30 special Maguro and other fresh fish and other bowls(Don) and dishes. Seafood is carefully selected by the professional in Japan. Also using high quality of rice grains from Yamagata Prefecture in Japan which best match to the Maguro Bowls.\r\nEvery bowl is accompanied with the homemade original miso soup made by fresh fish bone, a full of fish flavor is definitely the best match!\r\nAlso providing the original unique a la carte such as Maguro fish eyes, Maguro heads, and Maguro tails which directly imported from Japan too.\r\n\r\n“You can get all time discount for memberships on Apps, seasonal limited coupons. latest news, updated menus and more.\"','日本海鲜餐厅香港特色：供应午餐，晚餐和饮料服务：餐饮，外送，团体用餐，适合儿童，外带，服务员，欢迎光临','',114.16978120,22.29687750,'','06:00:00','23:00:00',5,'Thank you for your reservation!\nWe will make a phone call once the booking will be confirmed.','感謝您的預約！ \n我們將給您打電話一旦預訂確認。','2016-10-27 14:35:29','2017-04-03 06:40:59','',0);
/*!40000 ALTER TABLE `restaurants` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `seasons`
--

DROP TABLE IF EXISTS `seasons`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `seasons` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `name_cn` varchar(100) DEFAULT NULL,
  `from_date` datetime NOT NULL,
  `to_date` datetime NOT NULL,
  `check_date` datetime DEFAULT NULL,
  `create_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `update_date` datetime DEFAULT CURRENT_TIMESTAMP,
  `update_by` varchar(50) DEFAULT NULL,
  `is_delete` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `seasons`
--

LOCK TABLES `seasons` WRITE;
/*!40000 ALTER TABLE `seasons` DISABLE KEYS */;
INSERT INTO `seasons` VALUES (1,'Spring','Spring','2017-01-01 00:00:00','2017-03-30 00:00:00','2017-01-15 00:00:00','2017-04-19 12:49:47','2017-04-19 12:49:47',NULL,0),(2,'Summer','Summer','2017-04-01 00:00:00','2017-06-30 00:00:00','2017-04-19 00:00:00','2017-04-19 12:49:48','2017-04-19 12:49:48',NULL,0),(3,'Autumn','Autumn','2017-07-01 00:00:00','2017-10-30 00:00:00','2017-08-17 00:00:00','2017-04-19 12:49:48','2017-04-19 12:49:48',NULL,0),(4,'Winter','Winter','2017-11-01 00:00:00','2017-12-30 00:00:00','2017-11-15 00:00:00','2017-04-19 12:49:48','2017-04-19 12:49:48',NULL,0);
/*!40000 ALTER TABLE `seasons` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `settings`
--

DROP TABLE IF EXISTS `settings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `settings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(20) NOT NULL,
  `value` text NOT NULL,
  `value_cn` text,
  `description` varchar(2000) DEFAULT NULL,
  `description_cn` varchar(2000) DEFAULT NULL,
  `create_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `update_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `update_by` varchar(50) DEFAULT NULL,
  `is_delete` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=utf8 AVG_ROW_LENGTH=1820;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `settings`
--

LOCK TABLES `settings` WRITE;
/*!40000 ALTER TABLE `settings` DISABLE KEYS */;
INSERT INTO `settings` VALUES (1,'privacy_policy','1. Acquisition, use and provision of personal information\nWhen colleting personal information, TOYOSU SUISAN shall identify and clearly specify the scope necessary for personal information to be used to achieve business objectives and, shall only acquire personal information through legal and fair means. Personal information acquired by TOYOSU SUISAN shall only be used, provided within the scope necessary to achieve previously specified business objectives and, shall not be used for purposes outside the scope required to achieve said aims of use. In addition, TOYOSU SUISAN shall implement periodic checks to ensure that personal information is not being used for purposes other than the said objectives.\n\n2. Compliance with the law and other regulations in relation to personal information:\nTOYOSU SUISAN shall comply with laws and other regulations relating to the personal information protection.\n\n3. Prevention of damages and personal information corrective measures:\nTOYOSU SUISAN shall implement appropriate security measures to prevent unauthorized access, loss, destruction, misuse and leaks of personal information. In addition, these security measures shall be periodically and promptly reviewed and revised.\n\n4. Complaints and advice\nTOYOSU SUISAN shall establish a consulting division to address complaints, questions and inquiries regarding the use of personal information and to support fast settlement services for clients’ needs.','1.  接受、使用及提供個人信息:\n在收集個人信息時，\"ro ann style club\"負責確定及為被使用個人信息明確確定必須範圍以達到經營目的並只能通過公平及合法措施來收集個人信息。\"ro ann style club\"所獲得的個人信息只在必須範圍中使用及提供以達到規劃的經營目標，同時不能在該範圍目的外以達到所述的使用目的。另外，\"ro ann style club\"將進行定期檢查以保證個人信息不被使用於其他目的.\n2.  涉及個人信息的遵守法律及其他規定：\n\"ro ann style club\"對於個人信息保密事宜必須遵守法律及其他規定.\n3.  為了保密個人信息防止損害及客服措施：\n\"ro ann style club\"執行合理安寧措施以防止個人信息的非法登入、使用、防止損失、破壞、錯誤使用及洩露等。另外，確保安寧措施必須檢查、及時和定期更新.\n4.  投訴及咨詢:\n\"ro ann style club\"將成立咨詢部門以解決關於個人信息及支持客戶快速付款服務等的投訴、疑問及要求.','','','2016-10-31 09:52:22','2016-11-16 02:14:14','',0),(2,'terms_and_condition','1. Acquisition, use and provision of personal information\nWhen colleting personal information, TOYOSU SUISAN shall identify and clearly specify the scope necessary for personal information to be used to achieve business objectives and, shall only acquire personal information through legal and fair means. Personal information acquired by TOYOSU SUISAN shall only be used, provided within the scope necessary to achieve previously specified business objectives and, shall not be used for purposes outside the scope required to achieve said aims of use. In addition, TOYOSU SUISAN shall implement periodic checks to ensure that personal information is not being used for purposes other than the said objectives.\n\n2. Compliance with the law and other regulations in relation to personal information:\nTOYOSU SUISAN shall comply with laws and other regulations relating to the personal information protection.\n\n3. Prevention of damages and personal information corrective measures:\nTOYOSU SUISAN shall implement appropriate security measures to prevent unauthorized access, loss, destruction, misuse and leaks of personal information. In addition, these security measures shall be periodically and promptly reviewed and revised.\n\n4. Complaints and advice\nTOYOSU SUISAN shall establish a consulting division to address complaints, questions and inquiries regarding the use of personal information and to support fast settlement services for clients’ needs.','1.  接受、使用及提供個人信息:\n在收集個人信息時，\"ro ann style club\"負責確定及為被使用個人信息明確確定必須範圍以達到經營目的並只能通過公平及合法措施來收集個人信息。\"ro ann style club\"所獲得的個人信息只在必須範圍中使用及提供以達到規劃的經營目標，同時不能在該範圍目的外以達到所述的使用目的。另外，\"ro ann style club\"將進行定期檢查以保證個人信息不被使用於其他目的.\n\n2.  涉及個人信息的遵守法律及其他規定：\n\"ro ann style club\"對於個人信息保密事宜必須遵守法律及其他規定.\n\n3.  為了保密個人信息防止損害及客服措施：\n\"ro ann style club\"執行合理安寧措施以防止個人信息的非法登入、使用、防止損失、破壞、錯誤使用及洩露等。另外，確保安寧措施必須檢查、及時和定期更新.\n\n4.  投訴及咨詢:\n\"ro ann style club\"將成立咨詢部門以解決關於個人信息及支持客戶快速付款服務等的投訴、疑問及要求.','','','2016-11-02 10:45:06','2016-11-16 02:12:05','',0),(14,'website_url','http://discoverjp.info/shop','http://discoverjp.info/shop','URL of EC site','URL of EC site','2017-03-03 10:30:42','2017-03-03 10:30:42',NULL,0),(16,'about_prize','At Member can redeem the prize shown on the application upon the points owned.\r\nThe point can be exchange to the attractive prizes at “Prize” on this application.','At Member can redeem the prize shown on the application upon the points owned.\r\nThe point can be exchange to the attractive prizes at “Prize” on this application.',NULL,NULL,'2017-03-28 17:05:36','2017-03-28 17:05:36',NULL,0),(17,'about_point','Whenever you shop in \"ro ann” groups you will get 1% back in ro ann points. \r\nTo earn 1 point from every HK$100 spending at “ro ann” group services. \r\n(managed by C& Marketing Company Limited)\r\nYou can be accumulated point at our “ro ann style” services and groups.\r\nEvery time paying at ro ann group services, \r\nPlease inform or input your \"Membership No.\" for earning points.','Whenever you shop in \"ro ann” groups you will get 1% back in ro ann points. \r\nTo earn 1 point from every HK$100 spending at “ro ann” group services. \r\n(managed by C& Marketing Company Limited)\r\nYou can be accumulated point at our “ro ann style” services and groups.\r\nEvery time paying at ro ann group services, \r\nPlease inform or input your \"Membership No.\" for earning points.',NULL,NULL,'2017-03-28 17:05:45','2017-03-28 17:05:45',NULL,0),(18,'booking_tandc','Article 1 Compliance with basic items\r\nIn connection with the use of this system, it is required that customers comply with technical regulations and general cultures related to the use of the internet.\r\nIt is strictly forbidden to use the system for personal gain for any reason.\r\nArticle 2 Dealing in case of violation of basic regulations\r\nFor customers who perform behaviors detrimental to a third party or a company\'s services, they will be prohibited from using the system or store of the company.\r\nArticle 3 Effects from customer\'s usage environment\r\nThis system is intended for people who have appropriately made various settings such as network and e-mail. This restaurant is not responsible for the operation result of those who does not fall under this condition and the various influences that it brings. Even if the above conditions are fulfilled, even if the system does not operate properly due to reasons not controlled by the restaurant, including all the circumstances concerning the environment setting on the customer side, regarding this impact, our restaurant is not responsible.\r\nArticle 4 Conditions for system users\r\nThis system can be used only by customers who have agreed to these regulations.\r\nAlso, by using this system, we will assume that the customers have agreed to this Regulation and comply with the provisions of the regulations.\r\nArticle 5 Services provided by the system\r\nYou can make a reservation for the seat provided by this restaurant.\r\nArticle 6 Observance of regulations for using reservation service\r\nTo use the services, the customers are required to comply with the usage rules which our restaurant separately prescribes.\r\nArticle 7 Notes on using this system\r\nServices provided by the system do not come with other utilities such as telephone reservation, direct reservation at stores, etc.. Therefore, it may not be possible to reserve due to some reasons such as full occupancy.\r\nArticle 8 User\'s responsibilities\r\nCustomers using the system bear all responsibility for their own behavior and the consequences of such behavior such as the use of personal email and other behaviors. In addition, when using the system, except in the case of out-of-store events, the customers are liable entirely for damages to a third party or for settling disputes with a third party. \r\nIn the event that a user damages a system or restaurant due to acts corresponding to the items below, the store may require that user to compensate for the loss incurred.\r\n(1) In the event of breaching these Terms of Use, various Conditions and Rules of Use separately defined by the Company.\r\n(2) In case of transmitting or writing a harmful computer program.\r\n(3) When third-party information is transmitted or written.\r\n(4) Use of store information in a private way\r\n(5) Violations of the law\r\nArticle 9 Amount of compensation for breach\r\nIf the reservation is canceled due to reasons attributable to the user, we will charge a penalty as stipulated under our restaurant rules.\r\nArticle 10 Cancellation\r\nIf the customer wants to cancel the booking, he/she can via our website.\r\nCancellations can be conducted within 2 days before the reservation time. If it passes, please cancel by telephone directly to our restaurant.\r\nArticle 11 Items to be prepared for use\r\nBefore using this system, it is absolutely necessary to have the following items.\r\nE-mail account that can send and receive e-mails\r\nA smartphone that can connect to the Internet\r\nPhones that can receive phone calls in Hong Kong\r\nArticle 12 Reservation of the table\r\nMembership registration is required before making reservations with this system. Please enter all necessary personal data exactly into the membership registration form.\r\nPlease input all necessary data accurately to reservation form about your request.\r\nIf there is incompleteness in inputting personal data to the membership registration form and inputting data to the reservation form, the reservation may be invalid.\r\nArticle 13 Change of the content of the system\r\nWe may change the operation or contents of this system without notifying the customer in advance if we decide that this restaurant needs it. Please be sure to confirm this agreement every time. After changing the contents of this agreement, only the changed contents will be valid, and the contents before the change will be invalid.\r\nArticle 14 Temporary discontinuance of use\r\nThe restaurant may suspend the use of the system without prior notice to the customer in the circumstances below.\r\n(1) System maintenance.\r\n(2) Natural disasters or other unusual circumstances that may interfere with the operation of the system.\r\n(3) Where the shop finds it necessary to stop using the system for reasons other than operating the system.\r\nArticle 15 Law as the basis of the system\r\nThe handling of this system shall be governed and constructed by the law of Hong Kong.\r\nArticle 16 Effectiveness of the Regulation\r\nThis Regulation shall enter into force from April 1, 2017.','第一條：執行基本項目\r\n有關使用本系統，要求用戶遵守各技術性規定及與網絡使用一般相關文化文件。\r\n禁止為任何理由私利目的使用本系統。\r\n\r\n第二條：處理違反基本規定\r\n對於這些執行如損害或不便第三方行為的客戶，該行為可能造成公司服務損害，被認為不恰當的行為將處理禁止使用系統或公司的商店。\r\n\r\n第三條：自用戶環境的影響\r\n對象為各方做出了相應的設置如網絡、電子郵件等等。對於不符合本條件的活動結果，或者從該活動帶來的影響，本店絕對不承擔任何責任。此外，雖然滿足上述條件的情況下，但系統仍無法正常運行因本店管理錯誤，比如涉及用戶方環境設置問題就因此造成的影響，本店也不會承擔任何責任。\r\n\r\n第四條：系統用戶的條件\r\n只有對於本規定已同意的客戶方可使用本系統。\r\n在使用時點，在本系統訂貨就等於客戶已同意本規定並遵守規定中的條款。\r\n\r\n第五條：系統提供的服務\r\n可以在本店訂餐。\r\n\r\n第六條：對於訂餐服務使用的遵守規定\r\n客戶使用服務時必須遵守本店另行規定的使用原則。\r\n\r\n第七條：使用系統時的注意事項\r\n本系統所提供的服務不包括服務外的便宜比如通過電話訂餐，在餐廳直接訂餐等等所以有一些情況因沒位置客戶無法訂餐的。\r\n\r\n第八條：用戶責任\r\n使用該系統客戶必須承擔自己的行為以及因該行為引起的其他後果，如使用個人電子郵箱的，和其他行為而本身客戶自行的。此外，使用該系統時，因本店原因除外，客戶對第三方造成損害或解決與第三方發生糾紛的必須承擔全部責任。\r\n在用戶造成系統和本店損失的情況下，並因下列款項相當行為，本店可以要求該使用者賠償全部損失。\r\n (１) 在違反本規則，公司另行規定的承諾條款和使用規則的情況。\r\n(２) 在發送或輸入信息至危險計算機程序的情況。\r\n(３) 在發送或輸入第三方信息的情況。\r\n(４) 在隨意為私人目的使用本店信息的情況。\r\n(５) 具有違反法律行為的情況。\r\n\r\n第九條：違反賠償款項\r\n在因用戶原因引起客戶取消訂餐，就本店可以按照本店規定要求客戶定金。\r\n\r\n第十條：取消\r\n客戶具有取消訂餐要求，可以在網站上取消，其取消後該系統將發送確認短信。\r\n在網站取消可以在訂餐前兩天執行。若超過該時間就必須直接打電話到餐廳以取消的。\r\n\r\n第十一條：使用前準備事項\r\n系統使用時必須準備以下項目：\r\n可以發送和接受資料的電子郵箱地址。\r\n具有連接網絡應用軟件的智慧手機。\r\n可以在香港打電話的手機。\r\n\r\n第十二條：訂餐登記 \r\n在系統訂餐時必須先登記成員。使用者必須在成員登記表單中正確地輸入必要的個人信息。\r\n然後，在訂餐表格填寫信息若在餐廳須有訂餐的。\r\n在成員登記表格或訂貨單未填寫完整信息就客戶訂餐內容無效。\r\n\r\n第十三條：系統內容更改\r\n在本店認為必須更改關於系統的內容或運行機制的情況下，可以進行更改而不需要向客戶另行通知。因此，在每一次使用服務中，請客戶檢查確認本規制。規制內容更新後，就更新後內容生效並其之前內容無效。\r\n\r\n第十四條：暫停使用 \r\n本店可以暫停使用系統而不用先通知客戶，規定在如下情況：\r\n(１) 在系統維護執行的情況。\r\n(２) 在發生天災或其他不可抗力事件的情況，其可能造成系統正常運行的障礙。\r\n(３) 在有系統運行的其他原因，而本店認為必須停止使用系統。\r\n\r\n第十五條：系統的基礎法律\r\n本系統適用香港現行效力價值的法律基礎。\r\n\r\n第十六條：本規定有效期間\r\n本規定自2017年4月1日起生效。',NULL,NULL,'2017-03-30 14:25:03','2017-03-30 14:25:03',NULL,0),(19,'prize_start_date','3','3',NULL,NULL,'2017-04-05 17:05:46','2017-04-05 17:05:46',NULL,0),(20,'prize_end_date','30','30',NULL,NULL,'2017-04-10 10:19:45','2017-04-10 10:19:45',NULL,0),(22,'insta_access_token','213777430.ea576c6.c2e868fc5e4643d1af8f96b2e6da5358',NULL,NULL,NULL,'2017-04-18 10:55:34','2017-04-18 10:55:34',NULL,0);
/*!40000 ALTER TABLE `settings` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `stations`
--

DROP TABLE IF EXISTS `stations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `stations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `station_cd` int(11) NOT NULL,
  `name` varchar(200) NOT NULL,
  `name_cn` varchar(200) DEFAULT NULL,
  `line_cd` int(11) DEFAULT NULL,
  `post` varchar(20) DEFAULT NULL,
  `address` varchar(500) DEFAULT NULL,
  `address_cn` varchar(500) DEFAULT NULL,
  `lon` float DEFAULT NULL,
  `lat` float DEFAULT NULL,
  `create_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `update_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `update_by` varchar(50) DEFAULT NULL,
  `is_delete` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 AVG_ROW_LENGTH=16384;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `stations`
--

LOCK TABLES `stations` WRITE;
/*!40000 ALTER TABLE `stations` DISABLE KEYS */;
INSERT INTO `stations` VALUES (1,1,'Exit C','Exit C',415,'70000','Causeway Bay MTR','Causeway Bay MTR',NULL,NULL,'2016-10-25 06:37:32','2016-10-25 06:37:32','',0);
/*!40000 ALTER TABLE `stations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `supplier_items`
--

DROP TABLE IF EXISTS `supplier_items`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `supplier_items` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `supplier_id` int(11) NOT NULL,
  `item_id` int(11) NOT NULL,
  `from_date` datetime DEFAULT NULL,
  `to_date` datetime DEFAULT NULL,
  `unit_price` decimal(19,2) NOT NULL,
  `quantity_per_day` int(11) DEFAULT NULL,
  `create_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `update_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `update_by` varchar(50) DEFAULT NULL,
  `is_delete` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `FK_supplier_items_items_id` (`item_id`),
  KEY `FK_supplier_items_suppliers_id` (`supplier_id`),
  CONSTRAINT `FK_supplier_items_items_id` FOREIGN KEY (`item_id`) REFERENCES `items` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_supplier_items_suppliers_id` FOREIGN KEY (`supplier_id`) REFERENCES `suppliers` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `supplier_items`
--

LOCK TABLES `supplier_items` WRITE;
/*!40000 ALTER TABLE `supplier_items` DISABLE KEYS */;
/*!40000 ALTER TABLE `supplier_items` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `supplier_orders`
--

DROP TABLE IF EXISTS `supplier_orders`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `supplier_orders` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `supplier_id` int(11) NOT NULL,
  `item_id` int(11) NOT NULL,
  `order_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `receive_date` datetime DEFAULT NULL,
  `quantity` int(11) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1' COMMENT '1: Request; 2: Delivery: 3: Done',
  `comment` varchar(500) DEFAULT NULL,
  `comment_cn` varchar(500) DEFAULT NULL,
  `create_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `update_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `update_by` varchar(50) DEFAULT NULL,
  `is_delete` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `FK_supplier_orders_items_id` (`item_id`),
  KEY `FK_supplier_orders_suppliers_id` (`supplier_id`),
  CONSTRAINT `FK_supplier_orders_items_id` FOREIGN KEY (`item_id`) REFERENCES `items` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_supplier_orders_suppliers_id` FOREIGN KEY (`supplier_id`) REFERENCES `suppliers` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `supplier_orders`
--

LOCK TABLES `supplier_orders` WRITE;
/*!40000 ALTER TABLE `supplier_orders` DISABLE KEYS */;
/*!40000 ALTER TABLE `supplier_orders` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `suppliers`
--

DROP TABLE IF EXISTS `suppliers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `suppliers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `restaurant_id` int(11) NOT NULL,
  `area_id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `name_cn` varchar(100) DEFAULT NULL,
  `address` varchar(500) NOT NULL,
  `address_cn` varchar(500) DEFAULT NULL,
  `postal_code` varchar(20) DEFAULT NULL,
  `email` varchar(50) NOT NULL,
  `phone` varchar(20) NOT NULL,
  `fax` varchar(20) DEFAULT NULL,
  `create_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `update_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `update_by` varchar(50) DEFAULT NULL,
  `is_delete` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `FK_suppliers_areas_id` (`area_id`),
  KEY `FK_suppliers_restaurant_id` (`restaurant_id`),
  CONSTRAINT `FK_suppliers_areas_id` FOREIGN KEY (`area_id`) REFERENCES `areas` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_suppliers_restaurant_id` FOREIGN KEY (`restaurant_id`) REFERENCES `restaurants` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `suppliers`
--

LOCK TABLES `suppliers` WRITE;
/*!40000 ALTER TABLE `suppliers` DISABLE KEYS */;
/*!40000 ALTER TABLE `suppliers` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `topic_categories`
--

DROP TABLE IF EXISTS `topic_categories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `topic_categories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `name_cn` varchar(50) DEFAULT NULL,
  `create_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `update_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `update_by` varchar(50) DEFAULT NULL,
  `is_delete` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 AVG_ROW_LENGTH=16384;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `topic_categories`
--

LOCK TABLES `topic_categories` WRITE;
/*!40000 ALTER TABLE `topic_categories` DISABLE KEYS */;
INSERT INTO `topic_categories` VALUES (1,'News','食品新闻','2016-10-25 16:51:34','2017-02-13 02:32:47',NULL,0),(2,'Event','活动新闻','2016-10-25 16:51:34','2017-02-13 07:20:02',NULL,0),(3,'Seafood','1323活动新闻','2017-02-13 07:31:59','2017-02-13 12:02:51',NULL,0),(4,'Event11','1323活动新闻1','2017-02-13 09:42:20','2017-02-14 02:48:14',NULL,1);
/*!40000 ALTER TABLE `topic_categories` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `topics`
--

DROP TABLE IF EXISTS `topics`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `topics` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `restaurant_id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  `subject` varchar(500) NOT NULL,
  `subject_cn` varchar(500) NOT NULL,
  `body` varchar(4000) NOT NULL,
  `body_cn` varchar(4000) NOT NULL,
  `image_path` varchar(500) DEFAULT NULL,
  `image_width` int(11) NOT NULL DEFAULT '200',
  `image_height` int(11) NOT NULL DEFAULT '200',
  `link_address` varchar(500) DEFAULT NULL,
  `open_date` datetime NOT NULL,
  `close_date` datetime NOT NULL,
  `num_like` int(11) NOT NULL DEFAULT '0',
  `num_share` int(11) NOT NULL DEFAULT '0',
  `is_mail_notify` tinyint(1) NOT NULL DEFAULT '1',
  `is_sms_notify` tinyint(1) NOT NULL DEFAULT '1',
  `is_app_notify` tinyint(1) NOT NULL DEFAULT '1',
  `is_already_send` tinyint(1) NOT NULL DEFAULT '0',
  `hash_tags` varchar(200) DEFAULT NULL,
  `create_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `update_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `update_by` varchar(50) DEFAULT NULL,
  `is_delete` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `FK_topics_restaurants_id` (`restaurant_id`),
  KEY `FK_topics_topic_categories_id` (`category_id`),
  CONSTRAINT `FK_topics_restaurants_id` FOREIGN KEY (`restaurant_id`) REFERENCES `restaurants` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_topics_topic_categories_id` FOREIGN KEY (`category_id`) REFERENCES `topic_categories` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8 AVG_ROW_LENGTH=2048;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `topics`
--

LOCK TABLES `topics` WRITE;
/*!40000 ALTER TABLE `topics` DISABLE KEYS */;
INSERT INTO `topics` VALUES (1,1,1,'New delicios food','新產品－極上鮮魚味噌湯','And then to Monday, then fast on the 3rd link with the holidays to put the end of the matter, and today tied into a new start week! Once the morning do not want to see the Air Link wild to wild!? Link as Dixie it very fish soup miso soup first friends ~! Miso soup with sea bream, fish, and other fish Gan fish, add onions, kelp, mixed Miso boiled. More special use of high-level generous Hokkaido Shiretoko Peninsula Rokuku cloth boiled soup, the taste of sweet, fragrant fish flavor, really loses no way Department!','然後到星期一，然後快速在第三個鏈接上用節假日結束事情，今天綁到一個新的開始週！ 一旦上午不想看到空中鏈接野生野生！ 鏈接作為Dixie它非常魚湯味噌湯第一個朋友〜！ 味噌湯配海鯛，魚和其他魚贛魚，加洋蔥，海帶，混合味噌煮。 更特別的使用高級慷慨北海道知床半島Rokuku布煮湯，甜美的味道，芬芳的魚風味，真的沒有方法部門！','topics/15936766_683914621780995_1322401284982529790_o_wEBzKoX.jpg',1080,1364,'https://www.facebook.com/toyosusuisanhk/photos/a.1599276130372401.1073741828.1590864181213596/1623713284595352/?type=3&theater','2016-10-25 00:00:00','2017-11-01 00:00:00',500,30,1,1,1,0,'#toyosu #kaisedon #special','2016-10-25 16:53:24','2017-03-02 06:37:12',NULL,0),(2,1,2,'Temporary close because of typhoon','8號風球暫停營業','【Special Announcement】 As the No. 8 typhoon has been hoisted, it will be closed for business today until further notice. Thank you!','【特別通告】由於八號颱風已經懸掛，今日將會封閉，直至另行通知為止。 謝謝！','topics/new2.jpg',800,800,'https://www.facebook.com/toyosusuisanhk/photos/a.1599276130372401.1073741828.1590864181213596/1622421021391245/?type=3&theater','2016-10-26 00:00:00','2017-11-01 00:00:00',200,15,1,1,1,0,'#toyosu #temporary #typhoon','2016-10-25 16:54:28','2016-10-25 16:54:28',NULL,0),(3,1,1,'Food for weather today','提提你','Today is a good rain! Head first out of the street with the socks are buried wet ~ But I know that even hit the winds and heavy rain have hit the block Do not you want to pursue food! Remember to take umbrella Yeah ~ ~ After the night rain, Fan, when the action! Source: Network','今天是一場好雨！ 頭首先在街上用襪子埋了濕〜但我知道，甚至打風和大雨都打了塊不要你想追求食物！ 記得帶傘呀〜〜夜雨後，風扇，當行動！ 資料來源：網絡','topics/new3.jpg',480,300,'https://www.facebook.com/toyosusuisanhk/photos/a.1599276130372401.1073741828.1590864181213596/1621827198117294/?type=3&theater','2016-10-27 00:00:00','2017-06-02 00:00:00',100,10,0,0,1,0,'#news #food #weather #today','2016-10-25 16:56:00','2017-02-13 08:29:40',NULL,0),(4,1,1,'Have food together','熱話推介','Good luck to the city of leisure 嚟 left the rich continent Aquatic seafood shop shoot it! Daughter good girl beautiful Zhu Qianxue Tracy buried with Zhang Zhiwen Albert Chung has Zhu Minhan Brian eat a few happy ~ want to know the drainage to eat left-mate with dumping left-baa topic? Remember to pay attention to November 2 generous urban leisure friends ~!','祝運氣到城市的休閒嚟離開豐富的大陸水產海鮮店拍！ 女兒好女孩美麗朱茜茜Tracy與張志文埋葬阿爾伯特鍾有朱敏漢布萊恩吃了幾個快樂〜想知道排水，左撇子與左傾a主題嗎？ 記得要注意11月2日慷慨的都市休閒朋友〜！','topics/new4.jpg',960,720,'https://www.facebook.com/toyosusuisanhk/photos/a.1599276130372401.1073741828.1590864181213596/1621002851533062/?type=3&theater','2016-10-28 00:00:00','2017-01-01 00:00:00',120,9,1,1,1,0,'#toyosu #food #together','2016-10-25 16:56:43','2016-10-25 16:56:43',NULL,0),(5,1,1,'New seafood food','深宵肥美推介','[Why is the night is always hungry]\nThe next time hungry hungry want to eat Jagged taste wild field, from the rich continent aquatic seafood shop specializing in salt-burning tuna gel to swallow left dainty saliva! Tuna fish bone less tender and fleshy fat, after burning more aroma overflowing, Department of Seafood generous than recommended generous snacks! You have tried it ~ ','[為什麼夜晚總是飢餓]下一次飢餓的飢餓想要吃Jagged味道野生田，從豐富的大陸水產海鮮店專門從事鹽燒金槍魚凝膠吞下左精液唾液！ 金槍魚骨少嫩肉多脂，後燃燒更多的香氣溢出，系海鮮大方比推薦大方小吃！ 你試過了〜','topics/new5.jpg',960,678,'https://www.facebook.com/toyosusuisanhk/photos/a.1599276130372401.1073741828.1590864181213596/1620267168273297/?type=3&theater','2016-10-29 00:00:00','2017-11-01 00:00:00',130,10,1,1,1,0,'#toyosu #seafood #HK','2016-10-25 16:57:51','2016-10-25 16:57:51',NULL,0),(6,1,1,'New menu list','食個飯叉叉電','[Friend] is done and leave, then quickly went to South Third issue, Qin days to reset the day leave you Department of Mi good fun too! Happy affiliate Sorry Biqu stop it, go out and walk Yutaka Island Seafood aquatic food stores Fan Jing Jing to get you into a bowl of seafood a week electric fork a fork, powerful microphone to listen listen to day work continues fans Hello!','[朋友]做完了就離開了，然後很快就去了南三三問題，秦天重置了一天離開你部門的米好好玩！ 快樂的聯盟夥伴對不起Biqu停了它，出去和走Yutaka島海鮮水產食品店Fan Jing Jing讓你進一碗海鮮一周電叉叉，強大的麥克風聽聽日工作繼續球迷你好！','topics/new6.jpg',960,774,'https://www.facebook.com/toyosusuisanhk/photos/a.1599276130372401.1073741828.1590864181213596/1618576481775699/?type=3&theater','2016-10-30 00:00:00','2017-11-01 00:00:00',150,12,1,1,1,0,'#toyosu #newmenu','2016-10-25 16:58:53','2016-10-25 16:58:53',NULL,0),(7,1,2,'Food on island Feng Jing','多謝新假期報導','Thank you, new stories generous vacation!\r\nDepartment of friends today and again on Friday night friends gathered at the meal, right? Come and walk seafood specialty food stores large bowl generous Feng Jing Island Seafood Island Yutaka Dan Ngam sun on this matter, because the food is good wild fatigue can wash generous wedding week ~~ GOD we quickly go out and walk it!','謝謝你，新故事慷慨度假！ 朋友們今天和周五在星期五晚上的朋友聚在一起吃飯，對吧？ 來海鮮專賣食品店大碗慷慨馮景島海鮮島嶼Yutaka Dan Ngam太陽就這件事，因為食物是好的野生疲勞可以洗慷慨的婚禮週~~上帝我們很快出去走走吧！','topics/new7.jpg',200,200,'https://www.facebook.com/toyosusuisanhk/photos/a.1599276130372401.1073741828.1590864181213596/1617021168597897/?type=3&theater','2016-10-31 00:00:00','2017-11-01 00:00:00',140,30,1,1,1,0,'#toyosu #island #food #FengJing','2016-10-25 17:00:35','2016-12-09 10:28:36',NULL,0),(8,1,2,'Tsukiji Wholesaler Shins Into Hong Kong Island','引入Tsukiji供應商','Tsukiji Wholesaler Shins Into Hong Kong Island! \"Yu Chau Fisheries\" is the first to try out seafood!','Tsukiji批發商入香港島！ “Yu洲漁業”是第一個嘗試海鮮！','topics/new8.jpg',1000,600,'http://www.weekendhk.com/dining/%E8%B1%8A%E6%B4%B2%E6%B0%B4%E7%94%A2-%E6%97%A5%E6%9C%AC-%E7%AF%89%E5%9C%B0-%E9%8A%85%E9%91%BC%E7%81%A3/?utm_campaign=Sharing&utm_medium=Facebook_Post&utm_source=Facebook','2016-10-25 00:00:00','2017-11-01 00:00:00',220,20,1,0,1,0,'#toyosu #Tsukiji #Shin','2016-10-25 17:01:25','2016-11-10 06:53:17',NULL,0),(9,1,2,'Waiting Us','座位安排','【Seating arrangement】Today there will be no pay arrangements, we just wait in line outside the shop, Qi people can be seated.','【座位安排】今天會有沒有工資安排，我們只是在店外排隊等候，齊人可以坐。','topics/new9.jpg',960,720,'https://www.facebook.com/toyosusuisanhk/photos/a.1599276130372401.1073741828.1590864181213596/1614925988807415/?type=3&theater','2016-10-26 00:00:00','2017-11-01 00:00:00',200,25,1,1,1,0,'#toyosu #waiting #us','2016-10-25 17:02:08','2016-11-15 09:41:41',NULL,0),(10,1,1,'New delicious food','1212323','sdfsdsdf','sdfvsdfsdfsdfsdfsdf','topics/15936766_683914621780995_1322401284982529790_o.jpg',1080,1364,'https://www.facebook.com/toyosusuisanhk','2017-02-13 00:00:00','2017-03-02 00:00:00',0,0,0,0,0,0,'#news','2017-02-13 08:41:23','2017-03-02 06:36:27',NULL,0),(11,1,1,'23423242323','zxczxczxc','1e','e1231232312323','topics/14680734_548722488664844_8581964015279421895_n_H2Rrokm.jpg',720,960,'','2017-02-13 00:00:00','2017-02-26 00:00:00',0,0,0,0,0,0,'#news','2017-02-13 08:49:03','2017-02-13 08:50:10',NULL,1),(12,1,1,'zxcxc','test','content','content_jp','topics/MINE_Types.png',1001,491,'','2017-02-13 00:00:00','2017-02-14 00:00:00',0,0,0,0,1,0,'#news','2017-02-13 09:45:02','2017-02-14 09:19:19',NULL,0),(13,1,1,'Test2','Test2','123','123','topics/tumblr_nfyie3BnHh1r92k5yo1_1280.jpg',1080,720,'','2017-02-14 00:00:00','2017-05-31 00:00:00',0,0,0,0,0,0,'#news','2017-02-14 10:14:37','2017-02-14 10:14:37',NULL,0),(14,1,1,'Test aa','Test2aa','aa','aâ','topics/Toyosu_Shop_info.png',750,1334,'','2017-02-14 00:00:00','2018-02-14 00:00:00',0,0,0,0,0,0,'#news','2017-02-14 10:18:54','2017-02-15 07:54:32',NULL,0),(15,1,1,'VVV','VVV','Testttt','Testttt','topics/Capture.PNG',750,1334,'','2017-02-15 00:00:00','2017-03-31 00:00:00',2457,876,0,0,0,0,'#news','2017-02-15 07:55:15','2017-02-22 02:38:21',NULL,0),(16,1,1,'subject','subject','asdasd','cmta','topics/Capture2.PNG',200,200,'','2017-02-22 00:00:00','2017-03-07 00:00:00',0,0,0,0,0,0,'#news','2017-02-22 02:39:12','2017-02-22 11:19:42',NULL,0),(17,1,1,'new date','new date','new date','new date','topics/15936766_683914621780995_1322401284982529790_o_wEBzKoX.jpg',1080,1364,'https://www.facebook.com/toyosusuisanhk/photos/a.1599276130372401.1073741828.1590864181213596/1623713284595352/?type=3&theater','2016-10-25 00:00:00','2017-11-01 00:00:00',500,30,1,1,1,0,'#toyosu #kaisedon #special','2017-03-15 16:53:24','2017-03-02 06:37:12',NULL,0);
/*!40000 ALTER TABLE `topics` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `vote_items`
--

DROP TABLE IF EXISTS `vote_items`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `vote_items` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `restaurant_id` int(11) NOT NULL,
  `name` varchar(200) NOT NULL,
  `name_cn` varchar(200) DEFAULT NULL,
  `comment` varchar(2000) DEFAULT NULL,
  `comment_cn` varchar(2000) DEFAULT NULL,
  `create_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `update_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `update_by` varchar(50) DEFAULT NULL,
  `is_delete` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `FK_vote_items_restaurants_id` (`restaurant_id`),
  CONSTRAINT `FK_vote_items_restaurants_id` FOREIGN KEY (`restaurant_id`) REFERENCES `restaurants` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `vote_items`
--

LOCK TABLES `vote_items` WRITE;
/*!40000 ALTER TABLE `vote_items` DISABLE KEYS */;
/*!40000 ALTER TABLE `vote_items` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `vote_sessions`
--

DROP TABLE IF EXISTS `vote_sessions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `vote_sessions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `customer_id` int(11) NOT NULL,
  `vote_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `comment` varchar(2000) DEFAULT NULL,
  `create_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `update_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `update_by` varchar(50) DEFAULT NULL,
  `is_delete` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `FK_vote_sessions_customers_id` (`customer_id`),
  CONSTRAINT `FK_vote_sessions_customers_id` FOREIGN KEY (`customer_id`) REFERENCES `customers` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `vote_sessions`
--

LOCK TABLES `vote_sessions` WRITE;
/*!40000 ALTER TABLE `vote_sessions` DISABLE KEYS */;
/*!40000 ALTER TABLE `vote_sessions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping events for database 'higo_mb_dev'
--

--
-- Dumping routines for database 'higo_mb_dev'
--
/*!50003 DROP FUNCTION IF EXISTS `get_area_name` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`ibsv_admin`@`%` FUNCTION `get_area_name`(area_id int(11)) RETURNS varchar(100) CHARSET utf8
BEGIN
	select `name` INTO @area_name from areas where id = area_id;
    
    RETURN @area_name;
    
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP FUNCTION IF EXISTS `get_membership_name` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`ibsv_admin`@`%` FUNCTION `get_membership_name`(total_point int(11)) RETURNS varchar(100) CHARSET utf8
BEGIN
	select `name` INTO @status_name from membership_statuses where goal_point <= total_point order by goal_point desc limit 1;
    
    RETURN @status_name;
    
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP FUNCTION IF EXISTS `get_membership_status` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`ibsv_admin`@`%` FUNCTION `get_membership_status`(total_point int(11)) RETURNS int(11)
BEGIN
	select id INTO @current_status_id from membership_statuses where goal_point <= total_point order by goal_point desc limit 1;
    
    RETURN @current_status_id;
    
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `customer_update_point` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`ibsv_admin`@`%` PROCEDURE `customer_update_point`(
																	IN bill_code VARCHAR(50), 
																	IN total_sales decimal(8,2),
																	IN last_trans_date varchar(50))
BEGIN
	
    declare save_point int(11) default 0;
    
	if EXISTS(select id FROM bills b where b.bill_code = bill_code)
    then
		-- Get product_id
		-- select @customer_id:= customer_id FROM bills b where b.bill_code = bill_code;
        select customer_id INTO @customer_id FROM bills b where b.bill_code = bill_code;

		set save_point = FLOOR( ((total_sales/100)*1));
		-- select save_point;
        
		if (save_point > 0) then
			update customers c set c.num_visit = c.num_visit + 1 where id = @customer_id;
            
            -- Update last transaction
			update customers c set c.last_transaction_date = last_trans_date where id = @customer_id and last_transaction_date < last_trans_date;
        
			-- Get current status
			select total_point into @current_total_point from customers where id= @customer_id;
            select id INTO @current_status_id from membership_statuses where goal_point <= @current_total_point order by goal_point desc limit 1;
            
			update customers c set c.current_point = c.current_point + save_point, c.total_point = c.total_point + save_point where id= @customer_id;
        
			-- Get new status
            select total_point into @new_total_point from customers where id= @customer_id;
            select id INTO @new_status_id from membership_statuses where goal_point <= @new_total_point order by goal_point desc limit 1;
            
            -- Is go to next level
            if @new_status_id > @current_status_id then
				update customers c set c.level_up_date = last_trans_date where id= @customer_id and level_up_date < last_trans_date;
            end if;
            
        end if;
        
    end if;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `get_gifts` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`ibsv_admin`@`%` PROCEDURE `get_gifts`(IN ss_id int(11))
BEGIN

    declare sql_select text default '';	-- Final search
    
    -- Update last year config to this year
    update seasons s set s.from_date = DATE_ADD(s.from_date, INTERVAL 1 YEAR) where YEAR(s.from_date) < YEAR(NOW()) and id >=1;
	update seasons s set s.to_date = DATE_ADD(s.to_date, INTERVAL 1 YEAR) where YEAR(s.to_date) < YEAR(NOW()) and id >=1;
	update seasons s set s.check_date = DATE_ADD(s.check_date, INTERVAL 1 YEAR) where YEAR(s.check_date) < YEAR(NOW()) and id >=1;
    
    -- Get session checking date
    select check_date into @check_date from seasons where id = ss_id;
    -- select to_date into @to_date from seasons where id = ss_id;
    
    set sql_select = 'select c.id as id';
    set sql_select = CONCAT(sql_select, ', c.number as membership_no');
    set sql_select = CONCAT(sql_select, ', get_membership_name(c.total_point) as customer_type');
    set sql_select = CONCAT(sql_select, ', DATE(c.level_up_date) as level_up_date');
    set sql_select = CONCAT(sql_select, ', c.name as customer_name');
    set sql_select = CONCAT(sql_select, ', c.email as email');
    set sql_select = CONCAT(sql_select, ', c.phone as phone');
    set sql_select = CONCAT(sql_select, ', c.address as address');
    set sql_select = CONCAT(sql_select, ', get_area_name(c.area_id) as area_name');
    set sql_select = CONCAT(sql_select, ', c.total_point as life_point');
    set sql_select = CONCAT(sql_select, ', c.current_point');
    set sql_select = CONCAT(sql_select,' from customers c where get_membership_status(c.total_point) in ');

	set sql_select = CONCAT(sql_select, '(');
	set sql_select = CONCAT(sql_select, 'select id from membership_statuses where id in (');
	set sql_select = CONCAT(sql_select, 'select member_status_id from gift_seasons where season_id = ', ss_id);
	set sql_select = CONCAT(sql_select, ')');
    set sql_select = CONCAT(sql_select, ')');
	set sql_select = CONCAT(sql_select, ' and (');
    -- set sql_select = CONCAT(sql_select,' c.level_up_date >= \'', @check_date, '\' and c.level_up_date <= \'', @to_date, '\')');
    set sql_select = CONCAT(sql_select,' DATE(c.level_up_date) <= \'', DATE(@check_date) , '\' or c.level_up_date is NULL)');
    
    set sql_select = CONCAT('select * from (', sql_select, ') as order_select ', ' order by customer_type');
    
    SET @query = sql_select;
      
    PREPARE stmt FROM @query;
	EXECUTE stmt;
	DEALLOCATE Prepare stmt;
    
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `get_list_ope_res` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`inteasia`@`%` PROCEDURE `get_list_ope_res`(IN ope_id integer(11), IN type_id tinyint)
BEGIN
  # Define variables
  DECLARE searchAllSql varchar(500) default '';
  DECLARE searchCondSql varchar(500) default '';
  DECLARE stringSql varchar(500) default '';
  
  # Build query string
	SET searchAllSql = 'SELECT * FROM restaurants';
  SET searchCondSql = '
    SELECT
      res.*
    FROM restaurants res
      INNER JOIN operations_restaurants opr ON res.id = opr.restaurant_id
      INNER JOIN operations ope ON opr.operation_id = ope.id
    WHERE
      ope.is_delete = 0
  ';
	SET searchCondSql = CONCAT(searchCondSql, ' AND ope.id = ', ope_id);

  # Check role for bind condition
  IF type_id IS NULL OR type_id = 1 THEN  #Mente operator
		SET stringSql = searchAllSql;
  ELSE
    SET stringSql = searchCondSql;
  END IF;
  
  # Asign to query
  SET @query = stringSql;

  # Exec query
  PREPARE stmt FROM @query;
	EXECUTE stmt;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `import_bill` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`ibsv_admin`@`%` PROCEDURE `import_bill`(
														IN customer_id int(11),
														IN bill_date VARCHAR(50),
														IN bill_code VARCHAR(50),
														IN product_code VARCHAR(50),
														IN product_name VARCHAR(100),
														IN quantity int(11),
                                                        IN combo_code VARCHAR(100),
														IN sales decimal(8,2),
                                                        OUT out_sales decimal(8,2)
											)
BEGIN    
    declare bill_comment varchar(500) default '';
    
    -- Check current bill is exist OR not
    if EXISTS(select id FROM bills b where b.bill_code = bill_code)
    then
		-- Get bill_id
		-- select @bill_id:= id FROM bills b where b.bill_code = bill_code;
        select id INTO @bill_id FROM bills b where b.bill_code = bill_code;
        
	else
		-- Insert bill
        INSERT INTO bills(bill_code, bill_date, customer_id) VALUES ( bill_code,bill_date, customer_id);
        -- CALL insert_bill(bill_code,bill_date, customer_id);
        
        -- Get bill_id
        -- select @bill_id:= id FROM bills b where b.bill_code = bill_code;
        select id INTO @bill_id FROM bills b where b.bill_code = bill_code;
        
    end if;
    
    -- Check product is exist OR not
    if EXISTS(select id FROM pos_products b where b.product_code = product_code)
    then
		-- Get product_id
		-- select @product_id:= id FROM pos_products b where b.product_code = product_code;
        select id into @product_id FROM pos_products b where b.product_code = product_code;
        
	else
		-- Insert product
        INSERT INTO pos_products(product_code, name) VALUES ( product_code, product_name);
        -- CALL insert_post_product(product_code, product_name);
        
        -- Get product id
        -- select @product_id:= id FROM pos_products b where b.product_code = product_code;
        select id INTO @product_id FROM pos_products b where b.product_code = product_code;
    end if;
    
    -- Insert bill detail
    if EXISTS(select id FROM bill_details b where b.bill_id = @bill_id and b.product_id = @product_id and b.combo_code=combo_code)
    THEN
		set out_sales = 0;
	ELSE
		
		-- set bill_comment = '[ByBill, Product ID, Product Name, Combo Code]:';
        -- set bill_comment = CONCAT(bill_comment, '[', bill_code, ' , ', product_code, ',\' ', product_name, '\', ', combo_code,']');
        
        set bill_comment = '[ByBill, Product ID, Combo Code]:';
        set bill_comment = CONCAT(bill_comment, '[', bill_code, ' , ', product_code, ', ', combo_code,']');
    
		INSERT INTO bill_details(bill_id, product_id, quantity, sales, combo_code, comment) VALUES ( @bill_id, @product_id, quantity, sales, combo_code, bill_comment);
		-- Call insert_bill_detail(@bill_id, @product_id, quantity, sales, combo_code, bill_comment);
        set out_sales = sales;
    END IF;
    
    COMMIT;
	
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `import_pos` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`ibsv_admin`@`%` PROCEDURE `import_pos`(IN pos_data text)
BEGIN
	declare pos_customer_id VARCHAR(50) default '';	-- Pos customer id 
    declare pos_customer_name VARCHAR(50) default '';	-- Pos customer name	
    declare membership_no INT(11);	-- Pos membership no
    declare gender VARCHAR(50) default '';	-- Pos sex
    declare age VARCHAR(50) default '';	-- Pos age
    declare email VARCHAR(50) default '';	-- Pos email
    declare tel VARCHAR(50) default '';	-- Pos phone
	declare bill_date VARCHAR(50) default '';	-- Pos bill date
	declare bill_code VARCHAR(50) default '';	-- Pos bill code
	declare product_code VARCHAR(50) default '';	-- Pos product code
	declare product_name VARCHAR(100) default '';	-- Pos product name
    declare combo_code VARCHAR(100) default '';	-- Bill combo code
	declare quantity int(11) default '';	-- Pos product quantity
	declare sales decimal(8,2) default 0;	-- Pos bill sate
    
    -- declare customer_id INT(11);
    declare before_bill_code VARCHAR(50) default '';	-- Before line bill code
    declare bill_total_sales decimal(8,2) default 0;	-- Current bill total sale
    declare current_line varchar(4000) default '';	-- Current report line
    declare line_index int(11) default 0;	-- Current line index
    declare delim_index int default 0;	-- Index of split character
    declare delim_line_index int default 0;	-- Index of split character
    declare loop_index int default 0;	-- Index of loop
    
    set pos_data = trim(pos_data);	-- remove first and latest space in hash_tags first
    
    while length(pos_data) > 1 do	-- Exist hash tag in list hash tags
    
		set loop_index = loop_index + 1;	-- Increase loop index
        
        set delim_index =  position(';' in pos_data);	-- Find end position of first hash tag
        
        if delim_index > 1 then		-- Find hash tag
			set current_line = substring(pos_data, 1, delim_index -1 );
        else
			set current_line = substring(pos_data, 1);
        end if;
        -- select current_line;
        
        -- Processing line
        BEGIN
			
            set current_line = replace(current_line,'***','\'');
            set current_line = replace(current_line,',','');
			
            -- pos customer id
			set delim_line_index =  position('|' in current_line);
			set pos_customer_id = TRIM(substring(current_line, 1, delim_line_index -1 ));
			set current_line = substring(current_line, delim_line_index + 1);	
			-- select pos_customer_id;
			
			-- customer name
			set delim_line_index =  position('|' in current_line);
			set pos_customer_name = TRIM(substring(current_line, 1, delim_line_index -1 ));
			set current_line = substring(current_line, delim_line_index + 1);	
			-- select pos_customer_name;
			
			-- membership no
			set delim_line_index =  position('|' in current_line);
			set membership_no = CAST(TRIM(substring(current_line, 1, delim_line_index -1 )) AS UNSIGNED);
			set current_line = substring(current_line, delim_line_index + 1);	
			-- select membership_no;
            
            if EXISTS(select id FROM customers c where c.`number` = membership_no)
			THEN
				-- select @customer_id:= id FROM customers c where c.`number` = membership_no;
                select id INTO @customer_id FROM customers c where c.`number` = membership_no;
				-- select @customer_id;

				-- Gender
				set delim_line_index =  position('|' in current_line);
				set gender = TRIM(substring(current_line, 1, delim_line_index -1 ));
				set current_line = substring(current_line, delim_line_index + 1);	
				-- select gender;
				
				-- Age
				set delim_line_index =  position('|' in current_line);
				set age = TRIM(substring(current_line, 1, delim_line_index -1 ));
				set current_line = substring(current_line, delim_line_index + 1);	
				-- select age;
				
                -- Email
				set delim_line_index =  position('|' in current_line);
				set email = TRIM(substring(current_line, 1, delim_line_index -1 ));
				set current_line = substring(current_line, delim_line_index + 1);	
				-- select email;
                
                -- Tel
				set delim_line_index =  position('|' in current_line);
				set tel = TRIM(substring(current_line, 1, delim_line_index -1 ));
				set current_line = substring(current_line, delim_line_index + 1);	
				-- select tel;
                
				-- Bill Date
				set delim_line_index =  position('|' in current_line);
				set bill_date = TRIM(substring(current_line, 1, delim_line_index -1 ));
				set current_line = substring(current_line, delim_line_index + 1);	
				-- select bill_date;
				
				-- Bill code
				set delim_line_index =  position('|' in current_line);
				set bill_code = TRIM(substring(current_line, 1, delim_line_index -1 ));
				set current_line = substring(current_line, delim_line_index + 1);	
				-- select bill_code;
				
				-- Product code
				set delim_line_index =  position('|' in current_line);
				set product_code = TRIM(substring(current_line, 1, delim_line_index -1 ));
				set current_line = substring(current_line, delim_line_index + 1);	
				-- select product_code;
				
				-- Product name
				set delim_line_index =  position('|' in current_line);
				set product_name = TRIM(substring(current_line, 1, delim_line_index -1 ));
				set current_line = substring(current_line, delim_line_index + 1);	
				-- select product_name;
				
                -- Tel
				set delim_line_index =  position('|' in current_line);
				set combo_code = TRIM(substring(current_line, 1, delim_line_index -1 ));
				set current_line = substring(current_line, delim_line_index + 1);	
				-- select combo_code;
                
				-- Quantity
				set delim_line_index =  position('|' in current_line);
				set quantity = TRIM(substring(current_line, 1, delim_line_index -1 ));
				-- select quantity;
				
				set sales = CAST(TRIM(REPLACE(substring(current_line, delim_line_index + 1),',','')) as decimal(8, 2));	
				-- select sales;
                
                call import_bill(@customer_id, bill_date, bill_code, product_code, product_name, quantity, combo_code, sales, @our_sales);
				-- select @our_sales;
				
                if (delim_index <= 1) then	-- Current row is lalest row
                
					if (loop_index = 1) then	-- Data have only 1 row
						call customer_update_point(bill_code, @our_sales, bill_date);
                        
                    else	-- Data have many rows
						
                        if (before_bill_code != bill_code) then	-- Before row is another bill_code
                        
							-- Update point for before bill code
                            call customer_update_point(before_bill_code, bill_total_sales, bill_date);
                            
                            -- Update point for current bill code
                            call customer_update_point(bill_code, @our_sales, bill_date);
                        else	-- Same bill_code
							
                            set bill_total_sales = bill_total_sales + @our_sales;
							call customer_update_point(bill_code, bill_total_sales, bill_date);
                        end if;
                    end if;
				else
                    if (before_bill_code != bill_code) then	-- Before line is another bill code
                    
						call customer_update_point(before_bill_code, bill_total_sales, bill_date);
						set before_bill_code = bill_code;	-- Update current bill code
						set bill_total_sales = @our_sales;
                        
					else	-- Before line is same bill code
                        set bill_total_sales = bill_total_sales + @our_sales;
                    end if;
				end if;
			ELSE
				select 'Membership is not found';
			END IF;
		END;
        
        if delim_index <= 1 then	-- Current loop can not find has tag
			set pos_data = '';
        end if;
            
		-- Remove list hash tags by remove first hash tag  which is processed above
        set pos_data = substring(pos_data, delim_index + 1);		
        -- select pos_data;
        
    END WHILE;
    
    COMMIT;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `search_comp_info` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`inteasia`@`%` PROCEDURE `search_comp_info`()
BEGIN
  SELECT
  MAX(CASE WHEN s.code = 'privacy_policy' THEN value END) privacy_policy,
  MAX(CASE WHEN s.code = 'privacy_policy' THEN value_cn END) privacy_policy_cn,
  MAX(CASE WHEN s.code = 'company_map_url' THEN value END) company_map_url,
  MAX(CASE WHEN s.code = 'company_description ' THEN value END) company_description,
  MAX(CASE WHEN s.code = 'company_description ' THEN value_cn END) company_description_cn,
  MAX(CASE WHEN s.code = 'company_email' THEN value END) company_email,
  MAX(CASE WHEN s.code = 'company_image_path' THEN value END) company_image_path,
  MAX(CASE WHEN s.code = 'company_phone' THEN value END) company_phone,
  MAX(CASE WHEN s.code = 'company_address' THEN value END) company_address,
  MAX(CASE WHEN s.code = 'company_address' THEN value_cn END) company_address_cn,
  MAX(CASE WHEN s.code = 'company_name' THEN value END) company_name,
  MAX(CASE WHEN s.code = 'company_name' THEN value_cn END) company_name_cn
 FROM  settings s;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `search_foods_cat` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`inteasia`@`%` PROCEDURE `search_foods_cat`(IN cat_ids varchar(50),
IN curr_date datetime, IN _limit integer, IN _offset integer)
BEGIN
  SELECT
    fc.id AS cat_id,
    fc.name AS cat_name,
    fc.name_cn AS cat_name_cn,
    fc.image_path AS cat_image_path,
    f.*,
    (SELECT COUNT(*) FROM foods f
    INNER JOIN food_categories fc
      ON f.category_id = fc.id
  WHERE fc.is_delete = 0
  AND f.is_delete = 0
  AND COALESCE(f.start_time, TIME(curr_date)) <= TIME(curr_date)
  AND COALESCE(f.end_time, TIME(curr_date)) >= TIME(curr_date)
  AND COALESCE(f.current_order, 1) < COALESCE(f.quantity_per_day, 1000000)
  AND FIND_IN_SET(DAYOFWEEK(curr_date), COALESCE(f.week_days, '1,2,3,4,5,6,7'))
  AND FIND_IN_SET(fc.id, (cat_ids))) as total
  FROM foods f
    INNER JOIN food_categories fc
      ON f.category_id = fc.id
  WHERE fc.is_delete = 0
  AND f.is_delete = 0
  AND COALESCE(f.start_time, TIME(curr_date)) <= TIME(curr_date)
  AND COALESCE(f.end_time, TIME(curr_date)) >= TIME(curr_date)
  AND COALESCE(f.current_order, 1) < COALESCE(f.quantity_per_day, 1000000)
  AND FIND_IN_SET(DAYOFWEEK(curr_date), COALESCE(f.week_days, '1,2,3,4,5,6,7'))
  AND FIND_IN_SET(fc.id, (cat_ids))
  LIMIT _offset, _limit
  ;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `search_keyword` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`inteasia`@`%` PROCEDURE `search_keyword`(IN `restaurant_id` INT(11), IN `keyword` varchar(100), IN `num_row` INT(11))
BEGIN
	declare search_topics text default '';	-- Search on topic list
    declare search_coupons text default '';	-- Search on coupon list
    declare search_foods text default '';	-- Search on food list
    declare search text default '';	-- Final search
    declare now_week_day_index integer default 0; 	-- current week day index of now
    
    set now_week_day_index = weekday(NOW());	-- get current week day index to find food display
    
    -- Mapping mysql week day index to CMS week day index
    if(now_week_day_index = 6) then
		set now_week_day_index = 1;
	else
		set now_week_day_index = now_week_day_index + 2;
    end if;
    
    -- Make query for search on coupons
	set search_coupons = '';
	set search_coupons = CONCAT(search_coupons, 'select ');
	
	set search_coupons = CONCAT(search_coupons, 'c_r.', 'image_path as res_image_path');
	set search_coupons = CONCAT(search_coupons, ', c_r.', 'name as res_name');
	set search_coupons = CONCAT(search_coupons, ', c_r.', 'name_cn as res_name_cn');
	set search_coupons = CONCAT(search_coupons, ', c.', 'image_path');
	set search_coupons = CONCAT(search_coupons, ', \'\' as subject');
	set search_coupons = CONCAT(search_coupons, ', \'\' as subject_cn');
	set search_coupons = CONCAT(search_coupons, ', c.','name');
	set search_coupons = CONCAT(search_coupons, ', c.','name_cn');
	set search_coupons = CONCAT(search_coupons, ', \'\' as body');
	set search_coupons = CONCAT(search_coupons, ', \'\' as body_cn');
	set search_coupons = CONCAT(search_coupons, ', c.','title');
	set search_coupons = CONCAT(search_coupons, ', c.','title_cn');
	set search_coupons = CONCAT(search_coupons, ', 0 as price');
	set search_coupons = CONCAT(search_coupons, ', \'\' as description');
	set search_coupons = CONCAT(search_coupons, ', \'\' as description_cn');
	set search_coupons = CONCAT(search_coupons, ', \'\' as open_date');
	set search_coupons = CONCAT(search_coupons, ', c.','to_date');
	set search_coupons = CONCAT(search_coupons, ', c.','end_time');
	set search_coupons = CONCAT(search_coupons, ', c.','price_off');
	set search_coupons = CONCAT(search_coupons, ', c.','price_minus');
	set search_coupons = CONCAT(search_coupons, ', c.','condition');
	set search_coupons = CONCAT(search_coupons, ', c.','condition_cn');
	set search_coupons = CONCAT(search_coupons, ', c.', 'hash_tags');
	set search_coupons = CONCAT(search_coupons, ', 1 as object_type ');

	set search_coupons = CONCAT(search_coupons, ' from coupons c left join restaurants c_r on c.restaurant_id = c_r.id');
	
	set search_coupons = CONCAT(search_coupons,' where c.is_delete = 0 ');
	set search_coupons = CONCAT(search_coupons, ' and DATE(CURDATE()) >= DATE( c.from_date) and DATE(CURDATE()) <= DATE( c.to_date) ');
	set search_coupons = CONCAT(search_coupons, ' and TIME(NOW()) >= TIME(c.start_time) and TIME(NOW()) <= TIME(c.end_time) ');
	set search_coupons = CONCAT(search_coupons, ' and (');
    set search_coupons = CONCAT(search_coupons, ' c.name like \'%', keyword, '%\' ');
    set search_coupons = CONCAT(search_coupons, ' or c.name_cn like \'%', keyword, '%\' ');
    set search_coupons = CONCAT(search_coupons, ' or c.title like \'%', keyword, '%\' ');
    set search_coupons = CONCAT(search_coupons, ' or c.title_cn like \'%', keyword, '%\' ');
    set search_coupons = CONCAT(search_coupons, ' or c.hash_tags like \'%', keyword, '%\' ');
    set search_coupons = CONCAT(search_coupons, ' )');
	set search_coupons = CONCAT(search_coupons, ' limit 0, ', (num_row div 3));
	
	-- Make query for search on foods
	set search_foods = '';
	set search_foods = CONCAT(search_foods, 'select ');
	
	set search_foods = CONCAT(search_foods, 'f_r.', 'image_path as res_image_path');
	set search_foods = CONCAT(search_foods, ', f_r.', 'name as res_name');
	set search_foods = CONCAT(search_foods, ', f_r.', 'name_cn as res_name_cn');
	set search_foods = CONCAT(search_foods, ', f.', 'image_path');
	set search_foods = CONCAT(search_foods, ', \'\' as subject');
	set search_foods = CONCAT(search_foods, ', \'\' as subject_cn');
	set search_foods = CONCAT(search_foods, ', f.','name');
	set search_foods = CONCAT(search_foods, ', f.','name_cn');
	set search_foods = CONCAT(search_foods, ', \'\' as body');
	set search_foods = CONCAT(search_foods, ', \'\' as body_cn');
	set search_foods = CONCAT(search_foods, ', \'\' as title');
	set search_foods = CONCAT(search_foods, ', \'\' as title_cn');
	set search_foods = CONCAT(search_foods, ', f.','price');
	set search_foods = CONCAT(search_foods, ', f.', 'description');
	set search_foods = CONCAT(search_foods, ', f.', 'description_cn');
	set search_foods = CONCAT(search_foods, ', \'\' as open_date');
	set search_foods = CONCAT(search_foods, ', \'\' as to_date');
	set search_foods = CONCAT(search_foods, ', \'\' as end_time');
	set search_foods = CONCAT(search_foods, ', 0 as price_off');
	set search_foods = CONCAT(search_foods, ', 0 as price_minus');
	set search_foods = CONCAT(search_foods, ', \'\' as \'condition\'');
	set search_foods = CONCAT(search_foods, ', \'\' as condition_cn');
	set search_foods = CONCAT(search_foods, ', f.', 'hash_tags');
	set search_foods = CONCAT(search_foods, ', 2 as object_type ');

	set search_foods = CONCAT(search_foods, ' from foods f left join restaurants f_r on f.restaurant_id = f_r.id');
	
	set search_foods = CONCAT(search_foods,' where f.is_delete = 0 ');
	set search_foods = CONCAT(search_foods, ' and TIME(NOW()) >= TIME(f.start_time) and TIME(NOW()) <= TIME(f.end_time) ');
	set search_foods = CONCAT(search_foods, ' and position(\'', now_week_day_index, '\' in f.week_days) >=1 ');
	set search_foods = CONCAT(search_foods, ' and (');
    set search_foods = CONCAT(search_foods, ' f.name like \'%', keyword, '%\' ');
    set search_foods = CONCAT(search_foods, ' or f.name_cn like \'%', keyword, '%\' ');
    set search_foods = CONCAT(search_foods, ' or f.hash_tags like \'%', keyword, '%\' ');
    set search_foods = CONCAT(search_foods, ') ');
	set search_foods = CONCAT(search_foods, ' limit 0, ', (num_row div 3));

	
	-- Make query for search on topic (news)
	set search_topics = '';
	set search_topics = CONCAT(search_topics, 'select ');
	
	set search_topics = CONCAT(search_topics, 't_r.', 'image_path as res_image_path');
	set search_topics = CONCAT(search_topics, ', t_r.', 'name as res_name');
	set search_topics = CONCAT(search_topics, ', t_r.', 'name_cn as res_name_cn');
	set search_topics = CONCAT(search_topics, ', t.', 'image_path');
	set search_topics = CONCAT(search_topics, ', t.', 'subject');
	set search_topics = CONCAT(search_topics, ', t.', 'subject_cn');
	set search_topics = CONCAT(search_topics, ', \'\' as name');
	set search_topics = CONCAT(search_topics, ', \'\' as name_cn');
	set search_topics = CONCAT(search_topics, ', t.','body');
	set search_topics = CONCAT(search_topics, ', t.','body_cn');
	set search_topics = CONCAT(search_topics, ', \'\' as title');
	set search_topics = CONCAT(search_topics, ', \'\' as title_cn');
	set search_topics = CONCAT(search_topics, ', 0 as price');
	set search_topics = CONCAT(search_topics, ', \'\' as \'description\'');	
	set search_topics = CONCAT(search_topics, ', \'\' as description_cn');	
	set search_topics = CONCAT(search_topics, ', t.', 'open_date');
	set search_topics = CONCAT(search_topics, ', \'\' as to_date');
	set search_topics = CONCAT(search_topics, ', \'\' as end_time');
	set search_topics = CONCAT(search_topics, ', 0 as price_off');
	set search_topics = CONCAT(search_topics, ', 0 as price_minus');
	set search_topics = CONCAT(search_topics, ', \'\' as \'condition\'');
	set search_topics = CONCAT(search_topics, ', \'\' as condition_cn');
	set search_topics = CONCAT(search_topics, ', t.', 'hash_tags');
	set search_topics = CONCAT(search_topics, ', 3 as object_type ');

	set search_topics = CONCAT(search_topics, ' from topics t left join restaurants t_r on t.restaurant_id = t_r.id');
	
	set search_topics = CONCAT(search_topics,' where t.is_delete = 0 ');
	set search_topics = CONCAT(search_topics, ' and DATE(CURDATE()) >= DATE(', 't.open_date) and DATE(CURDATE()) <= DATE(', 't.close_date) ');
	set search_topics = CONCAT(search_topics, ' and (');
    set search_topics = CONCAT(search_topics, ' t.subject like \'%', keyword, '%\' ');
    set search_topics = CONCAT(search_topics, ' or t.subject_cn like \'%', keyword, '%\' ');
    set search_topics = CONCAT(search_topics, ' or t.hash_tags like \'%', keyword, '%\' ');
    set search_topics = CONCAT(search_topics, ') ');
	set search_topics = CONCAT(search_topics, ' limit 0, ', (num_row - ((num_row div 3)*2)));

	set search = CONCAT(search, '(', search_coupons, ') union (',search_foods , ' )',' union (',search_topics , ' )');
	
	set search = CONCAT('select * from (', search, ') s order by object_type');
	
   -- select searchSql;
    SET @query = search;
      
    PREPARE stmt FROM @query;
	EXECUTE stmt;
	DEALLOCATE Prepare stmt;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `search_ope_res` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`inteasia`@`%` PROCEDURE `search_ope_res`(IN ope_id integer(11))
BEGIN
  SELECT 
    res.id,
    res.name
  FROM operations_restaurants opr
    INNER JOIN restaurants res ON res.id = opr.restaurant_id
    AND res.is_delete=0
  WHERE opr.operation_id=ope_id
    AND opr.is_delete=0;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `search_related_items` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
DELIMITER ;;
CREATE DEFINER=`inteasia`@`%` PROCEDURE `search_related_items`(
																IN `restaurant_id` INT(11), 
                                                                IN `obj_id` INT(11), 
                                                                IN `obj_type` INT(11), 
																IN `hash_tags` varchar(500), 
																IN `num_row` INT(11)
                                                                )
BEGIN

	declare search_topics text default '';	-- Search on topic list
    declare search_coupons text default '';	-- Search on coupon list
    declare search_foods text default '';	-- Search on food list
    declare search text default '';	-- Final search
	declare now_week_day_index integer default 0; 	-- current week day index of now
    
    declare search_limit text default '';	-- Final search
	declare current_hash_tag varchar(100) default '';    -- Get current hash tag from list has_tags
    declare delim_index int default 0;	-- Index of split character
    declare loop_index int default 0;	-- Index of loop
    set hash_tags = trim(hash_tags);	-- remove first and latest space in hash_tags first
    
    set now_week_day_index = weekday(NOW());	-- get current week day index to find food display
    
    -- Mapping mysql week day index to CMS week day index
    if(now_week_day_index = 6) then
		set now_week_day_index = 1;
	else
		set now_week_day_index = now_week_day_index + 2;
    end if;
    
	while length(hash_tags) > 1 do	-- Exist hash tag in list hash tags
    
		set loop_index = loop_index + 1;	-- Increase loop index
    
		set delim_index =  position(' ' in hash_tags);	-- Find end position of first hash tag
		
        if delim_index > 1 then		-- Find hash tag
			set current_hash_tag = substring(hash_tags, 2, delim_index -1 );
            
        else
			set current_hash_tag = substring(hash_tags, 2);
        end if;
        
        set current_hash_tag = trim(current_hash_tag);	-- Remove first and last space in current hash tag
        
        
		-- Make query for search on coupons
        set search_coupons = '';
		set search_coupons = CONCAT(search_coupons, 'select ');
		
		set search_coupons = CONCAT(search_coupons, 'c', loop_index, '.', 'id as id');
        set search_coupons = CONCAT(search_coupons, ', c', loop_index, '.', 'restaurant_id');
        set search_coupons = CONCAT(search_coupons, ', c_r', loop_index, '.', 'image_path as res_image_path');
		set search_coupons = CONCAT(search_coupons, ', c_r', loop_index, '.', 'name as res_name');
		set search_coupons = CONCAT(search_coupons, ', c_r', loop_index, '.', 'name_cn as res_name_cn');
		
        set search_coupons = CONCAT(search_coupons, ', c', loop_index, '.', 'image_path');
        set search_coupons = CONCAT(search_coupons, ', c', loop_index, '.', 'image_width');
        set search_coupons = CONCAT(search_coupons, ', c', loop_index, '.', 'image_height');
		set search_coupons = CONCAT(search_coupons, ', \'\' as subject');
        set search_coupons = CONCAT(search_coupons, ', \'\' as subject_cn');
		set search_coupons = CONCAT(search_coupons, ', c', loop_index, '.','name');
		set search_coupons = CONCAT(search_coupons, ', c', loop_index, '.','name_cn');
		set search_coupons = CONCAT(search_coupons, ', \'\' as body');
		set search_coupons = CONCAT(search_coupons, ', \'\' as body_cn');
		set search_coupons = CONCAT(search_coupons, ', c', loop_index, '.','title');
		set search_coupons = CONCAT(search_coupons, ', c', loop_index, '.','title_cn');
		set search_coupons = CONCAT(search_coupons, ', 0 as price');
		set search_coupons = CONCAT(search_coupons, ', \'\' as description');
		set search_coupons = CONCAT(search_coupons, ', \'\' as description_cn');
		set search_coupons = CONCAT(search_coupons, ', \'\' as open_date');
		set search_coupons = CONCAT(search_coupons, ', DATE_FORMAT(c', loop_index, '.', 'to_date, \'%Y-%m-%dT%TZ\') as to_date');
		set search_coupons = CONCAT(search_coupons, ', c', loop_index, '.','end_time');
		set search_coupons = CONCAT(search_coupons, ', c', loop_index, '.','price_off');
		set search_coupons = CONCAT(search_coupons, ', c', loop_index, '.','price_minus');
        set search_coupons = CONCAT(search_coupons, ', c', loop_index, '.','condition');
        set search_coupons = CONCAT(search_coupons, ', c', loop_index, '.','condition_cn');
        set search_coupons = CONCAT(search_coupons, ', c', loop_index, '.', 'hash_tags');
		set search_coupons = CONCAT(search_coupons, ', 1 as object_type ');

		set search_coupons = CONCAT(search_coupons, ' from coupons c', loop_index,' left join ',' restaurants c_r', loop_index);
		set search_coupons = CONCAT(search_coupons, ' on c', loop_index,'.restaurant_id = c_r', loop_index,'.id ');

		set search_coupons = CONCAT(search_coupons,' where c', loop_index, '.is_delete = 0 ');
        set search_coupons = CONCAT(search_coupons, ' and DATE(CURDATE()) <= DATE( c', loop_index, '.to_date) ');
		-- set search_coupons = CONCAT(search_coupons, ' and DATE(CURDATE()) >= DATE( c', loop_index, '.from_date) and DATE(CURDATE()) <= DATE( c', loop_index, '.to_date) ');
        set search_coupons = CONCAT(search_coupons, ' and TIME(NOW()) >= TIME( c', loop_index, '.start_time) and TIME(NOW()) <= TIME( c', loop_index, '.end_time) ');
		if obj_type = 1 then
		 	set search_coupons = CONCAT(search_coupons,' and c', loop_index, '.id <> ', obj_id,' ');
        end if;
        set search_coupons = CONCAT(search_coupons, ' and hash_tags like \'%', current_hash_tag, '%\' ');
		set search_coupons = CONCAT(search_coupons, ' limit 0, ', (num_row div 3));
        
        
        -- Make query for search on foods
        set search_foods = '';
		set search_foods = CONCAT(search_foods, 'select ');
		
        set search_foods = CONCAT(search_foods, 'f', loop_index, '.', 'id as id');
        set search_foods = CONCAT(search_foods, ', f', loop_index, '.', 'restaurant_id');
		set search_foods = CONCAT(search_foods, ', f_r', loop_index, '.', 'image_path as res_image_path');
		set search_foods = CONCAT(search_foods, ', f_r', loop_index, '.', 'name as res_name');
		set search_foods = CONCAT(search_foods, ', f_r', loop_index, '.', 'name_cn as res_name_cn');
		set search_foods = CONCAT(search_foods, ', f', loop_index, '.', 'image_path');
        set search_foods = CONCAT(search_foods, ', f', loop_index, '.', 'image_width');
        set search_foods = CONCAT(search_foods, ', f', loop_index, '.', 'image_height');
		set search_foods = CONCAT(search_foods, ', \'\' as subject');
        set search_foods = CONCAT(search_foods, ', \'\' as subject_cn');
		set search_foods = CONCAT(search_foods, ', f', loop_index, '.','name');
		set search_foods = CONCAT(search_foods, ', f', loop_index, '.','name_cn');
		set search_foods = CONCAT(search_foods, ', \'\' as body');
		set search_foods = CONCAT(search_foods, ', \'\' as body_cn');
		set search_foods = CONCAT(search_foods, ', \'\' as title');
		set search_foods = CONCAT(search_foods, ', \'\' as title_cn');
		set search_foods = CONCAT(search_foods, ', f', loop_index, '.','price');
		set search_foods = CONCAT(search_foods, ', f', loop_index, '.', 'description');
		set search_foods = CONCAT(search_foods, ', f', loop_index, '.', 'description_cn');
		set search_foods = CONCAT(search_foods, ', \'\' as open_date');
		set search_foods = CONCAT(search_foods, ', \'\' as to_date');
		set search_foods = CONCAT(search_foods, ', \'\' as end_time');
		set search_foods = CONCAT(search_foods, ', 0 as price_off');
		set search_foods = CONCAT(search_foods, ', 0 as price_minus');
        set search_foods = CONCAT(search_foods, ', \'\' as \'condition\'');
        set search_foods = CONCAT(search_foods, ', \'\' as condition_cn');
        set search_foods = CONCAT(search_foods, ', f', loop_index, '.', 'hash_tags');
		set search_foods = CONCAT(search_foods, ', 2 as object_type ');

		set search_foods = CONCAT(search_foods, ' from foods f', loop_index,' left join ',' restaurants f_r', loop_index);
		set search_foods = CONCAT(search_foods, ' on f', loop_index,'.restaurant_id = f_r', loop_index,'.id ');
        
        set search_foods = CONCAT(search_foods,' where f', loop_index, '.is_delete = 0 ');
        set search_foods = CONCAT(search_foods, ' and TIME(NOW()) >= TIME(f', loop_index,'.start_time) and TIME(NOW()) <= TIME(f', loop_index,'.end_time) ');
        set search_foods = CONCAT(search_foods, ' and position(\'', now_week_day_index, '\' in f', loop_index,'.week_days) >=1 ');
		if obj_type = 2 then
			set search_foods = CONCAT(search_foods,' and f', loop_index, '.id <> ', obj_id,' ');
        end if;
        set search_foods = CONCAT(search_foods, ' and f', loop_index,'.hash_tags like \'%', current_hash_tag, '%\' ');
		set search_foods = CONCAT(search_foods, ' limit 0, ', (num_row div 3));

        
        -- Make query for search on topic (news)
        set search_topics = '';
		set search_topics = CONCAT(search_topics, 'select ');
		
        set search_topics = CONCAT(search_topics, 't', loop_index, '.', 'id as id');
        set search_topics = CONCAT(search_topics, ', t', loop_index, '.', 'restaurant_id');
		set search_topics = CONCAT(search_topics, ', t_r', loop_index, '.', 'image_path as res_image_path');
		set search_topics = CONCAT(search_topics, ', t_r', loop_index, '.', 'name as res_name');
		set search_topics = CONCAT(search_topics, ', t_r', loop_index, '.', 'name_cn as res_name_cn');
        set search_topics = CONCAT(search_topics, ', t', loop_index, '.', 'image_path');
        set search_topics = CONCAT(search_topics, ', t', loop_index, '.', 'image_width');
        set search_topics = CONCAT(search_topics, ', t', loop_index, '.', 'image_height');
		set search_topics = CONCAT(search_topics, ', t', loop_index, '.', 'subject');
        set search_topics = CONCAT(search_topics, ', t', loop_index, '.', 'subject_cn');
		set search_topics = CONCAT(search_topics, ', \'\' as name');
		set search_topics = CONCAT(search_topics, ', \'\' as name_cn');
		set search_topics = CONCAT(search_topics, ', t', loop_index, '.','body');
		set search_topics = CONCAT(search_topics, ', t', loop_index, '.','body_cn');
		set search_topics = CONCAT(search_topics, ', \'\' as title');
		set search_topics = CONCAT(search_topics, ', \'\' as title_cn');
		set search_topics = CONCAT(search_topics, ', 0 as price');
		set search_topics = CONCAT(search_topics, ', \'\' as \'description\'');	
		set search_topics = CONCAT(search_topics, ', \'\' as description_cn');	
		set search_topics = CONCAT(search_topics, ', DATE_FORMAT(t', loop_index, '.', 'open_date, \'%Y-%m-%dT%TZ\') as open_date');
		set search_topics = CONCAT(search_topics, ', \'\' as to_date');
		set search_topics = CONCAT(search_topics, ', \'\' as end_time');
		set search_topics = CONCAT(search_topics, ', 0 as price_off');
		set search_topics = CONCAT(search_topics, ', 0 as price_minus');
        set search_topics = CONCAT(search_topics, ', \'\' as \'condition\'');
        set search_topics = CONCAT(search_topics, ', \'\' as condition_cn');
        set search_topics = CONCAT(search_topics, ', t', loop_index, '.', 'hash_tags');
		set search_topics = CONCAT(search_topics, ', 3 as object_type ');

		set search_topics = CONCAT(search_topics, ' from topics t', loop_index,' left join ',' restaurants t_r', loop_index);
		set search_topics = CONCAT(search_topics, ' on t', loop_index,'.restaurant_id = t_r', loop_index,'.id ');
		
		set search_topics = CONCAT(search_topics,' where t', loop_index, '.is_delete = 0 ');
		set search_topics = CONCAT(search_topics, ' and DATE(CURDATE()) >= DATE(', 't', loop_index, '.open_date) and DATE(CURDATE()) <= DATE(', 't', loop_index, '.close_date) ');
		if obj_type = 3 then
			set search_topics = CONCAT(search_topics,' and t', loop_index, '.id <> ', obj_id,' ');
        end if;

        set search_topics = CONCAT(search_topics, ' and t', loop_index, '.hash_tags like \'%', current_hash_tag, '%\' ');
        set search_topics = CONCAT(search_topics, ' limit 0, ', (num_row - ((num_row div 3)*2)));

		-- Final search
        if length(search) > 0 then	-- First hash tag
			set search = CONCAT(search, 'union (', search_topics, ') union (', search_coupons, ' )',' union (', search_foods, ' )');
        
        else	-- Combine with from sencond hash tag
			set search = CONCAT(search, '(', search_topics, ') union (', search_coupons, ' )',' union (', search_foods, ' )');
        end if;
		

		if delim_index <= 1 then	-- Current loop can not find has tag
			set hash_tags = '';
        end if;
            
		-- Remove list hash tags by remove first hash tag  which is processed above
        set hash_tags = substring(hash_tags, delim_index + 1);		
        
    END WHILE;

	-- Set priority display
	set search = CONCAT('select * from (', search, ') s order by object_type');
	
   -- select searchSql;
    SET @query = search;
      
    PREPARE stmt FROM @query;
	EXECUTE stmt;
	DEALLOCATE Prepare stmt;
    
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-04-20 15:02:34
