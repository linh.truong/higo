<?php
/* Template name: infohome */

get_header( 'infohome' ); ?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">
			<div class="infoHomeTop">
				<?php
				if ( get_query_var( 'paged' ) ) {
					$paged = get_query_var( 'paged' );
				} elseif ( get_query_var( 'page' ) ) { // 'page' is used instead of 'paged' on Static Front Page
					$paged = get_query_var( 'page' );
				} else {
					$paged = 1;
				}

				$custom_query_args = array(
					'post_type'           => 'post',
					'posts_per_page'      => 5,
					'paged'               => $paged,
					'post_status'         => 'publish',
					'ignore_sticky_posts' => true,
					//'category_name' => 'custom-cat',
					'order'               => 'DESC', // 'ASC'
					'orderby'             => 'date' // modified | title | name | ID | rand
				);
				$custom_query      = new WP_Query( $custom_query_args );
				$custom_query      = new WP_Query( $custom_query_args );

				if ( $custom_query->have_posts() ) :
					while ( $custom_query->have_posts() ) : $custom_query->the_post(); ?>

						<div class="infoHomeClearfix">
							<div class="infoHomeColumn infoHomeMenu">
								<a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>"><?php the_post_thumbnail( 'thumbnail' ); ?></a>
							</div>
							<div class="infoHomeColumn infoHomeContent">
								<h4 class="title-content">
									<a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>">
										<?php truncate_text( get_the_title(), 100, 10 ); ?>
									</a>
								</h4>
								<h4 class="title-content mobile-title" style="display: none">
									<a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>">
										<?php truncate_text( get_the_title(), 40, 7 ); ?>
									</a>
								</h4>
								<p style="margin: 0px;" class="main-content">
									<?php
									truncate_text(get_the_content(), 100, 20)
									?>
								</p>
								<p style="margin: 0px;" class="author-content"><?php the_time( 'Y-m-d' ); ?> by <?php the_author_posts_link() ?> --- <?php if ( function_exists( 'the_views' ) ) {
										the_views();
									} ?></p>
							</div>
						</div>

						<?php
					endwhile;
					?>

					<?php
					$wp_query = $custom_query;
					the_posts_pagination( array(
						'end_size'  => 1,
						'mid_size'  => 2,
						'prev_next' => false
					) );
					?>

					<?php
					wp_reset_postdata(); // reset the query
				else:
					echo '<p>' . __( 'Sorry, no posts matched your criteria.', 'woocommerce' ) . '</p>';
				endif;
				?>
			</div>
		</main><!-- #main -->
	</div><!-- #primary -->

<?php
do_action( 'storefront_sidebar' );
get_footer();
