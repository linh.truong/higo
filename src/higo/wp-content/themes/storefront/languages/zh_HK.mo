��    7      �  I   �      �  
   �     �     �     �     �  	   �               '     0     E     Y     ^     l  W   ~     �     �  
   �     �     �     
           /  P   =     �     �  %   �     �     �     �  	   �     �          #  F   6     }     �     �     �     �     �     �     �     �  \     
   i     t     {     �  
   �  $   �  T   �     %	     <	  �  T	     �
     �
  	                   $  	   -     7     >     E     X     h     o     |  :   �     �     �     �     �     �     �  	     	     I   !     k     r          �  	   �     �  	   �  	   �  	   �     �  D   �     3     @     G     Z     g     n     u     �     �  X   �     �     �               #  !   *     L  	   l     v     2   	                                        0           6   -       /         &                 +          #         $                 '          (               1          "      7   3          5   ,           
   *      4      %                 )   .   !    % Comments %1$s designed by %2$s. %d item %d items &larr; Older Comments ,  1 Comment Best Sellers Comment navigation Comments Comments are closed. Continue reading %s Edit Fan Favorites Featured Products It seems we can&rsquo;t find what you&rsquo;re looking for. Perhaps searching can help. Leave a comment Menu My Account New In New In Store Newer Comments &rarr; Next postNext Nothing Found Nothing was found at this location. Try searching, or check out the links below. On Sale On Sale Now Oops! That page can&rsquo;t be found. Order Received Pages: Popular Products Posted in Previous postPrevious Primary Navigation Product Categories Ready to publish your first post? <a href="%1$s">Get started here</a>. Recommendation for you Search Search Results for: %s Secondary Navigation See All See More Shop by Category Skip to content Skip to navigation Sorry, but nothing matched your search terms. Please try again with some different keywords. Storefront Tagged View your shopping cart We Recommend Written by Your comment is awaiting moderation. comments titleOne thought on &ldquo;%2$s&rdquo; %1$s thoughts on &ldquo;%2$s&rdquo; post datePosted on %s ro ann style selections Project-Id-Version: Storefront 2.1.8
Report-Msgid-Bugs-To: https://github.com/woothemes/storefront/issues
POT-Creation-Date: 2017-03-08 11:05+0700
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
PO-Revision-Date: 2017-04-03 13:13+0700
Language-Team: EMAIL@ADDRESS
X-Generator: Poedit 1.8.12
Last-Translator: 
Plural-Forms: nplurals=1; plural=0;
Language: zh_HK
 % 評論 %1$s 設計 %2$s. %d 項目 ＆larr; 舊評論 ， 1 評論 最畅销 評論 評論 評論被關閉。 繼續閱讀 %s 修改 粉絲收藏 特色產品 我們找不到你要找的。 也許搜索可以幫助。 離開評論 Menu 我的帳戶 最新 最新商店 更新評論 ＆rarr; 下一篇 沒找到 此位置找不到任何內容。 嘗試搜索，或查看以下鏈接。 出售 現在出售 對不起！ 找不到頁面。 收到訂單 頁數： 流行產品 發佈在 上一篇 主導航 產品分類 準備發布你的第一篇文章了嗎？<a href="%1$s">開始</a>. 為您推薦 搜索 尋找結果： %s 輔助導航 全部 更多 類別購物 跳到內容 跳到導航 很抱歉，沒有任何符合你的搜尋。 請使用一些不同的字再試一次。 商店 標籤 查看你的購物 我們推薦 寫作 您的評論正在等待審核。 一個想法 &ldquo;%2$s&rdquo; 發布 %s ro ann style selections 