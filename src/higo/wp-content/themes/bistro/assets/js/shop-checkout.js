jQuery(document).ready(function ($) {
    /* Check out page */
    $('.comfirm-checkout').on('click', function (event) {
        // Billing table
        var billing_name = $('#billing_first_name').val() + " " + $('#billing_last_name').val();
        var billing_phone = $('#billing_phone').val();
        var billing_email = $('#billing_email').val();
        var billing_address = $('#billing_address_2').val();
        if (billing_address) {
            billing_address += ", " + $('#billing_address_1').val();
        }
        else {
            billing_address = $('#billing_address_1').val();
        }
        var billing_city = $('#billing_city').val();
        var billing_state = $('#billing_state').val();
        $("#table_billing_name").text(billing_name);
        $("#table_billing_phone").text(billing_phone);
        $("#table_billing_email").text(billing_email);
        $("#table_billing_address").text(billing_address);
        $("#table_billing_city").text(billing_city);
        $("#table_billing_state").text(billing_state);

        // Shipping table
        var ship_to_different_address = $('#ship-to-different-address-checkbox').is(":checked");
        if (ship_to_different_address) {
            var shipping_name = $('#shipping_first_name').val() + " " + $('#shipping_last_name').val();
            var shipping_address = $('#shipping_address_2').val();
            if (shipping_address) {
                shipping_address += ", " + $('#shipping_address_1').val();
            }
            else {
                shipping_address = $('#shipping_address_1').val();
            }
            var shipping_city = $('#shipping_city').val();
            var shipping_state = $('#shipping_state').val();
            $("#table_shipping_name").text(shipping_name);
            $("#table_shipping_address").text(shipping_address);
            $("#table_shipping_city").text(shipping_city);
            $("#table_shipping_state").text(shipping_state);
        }
        else {
            $("#table_shipping_name").text(billing_name);
            $("#table_shipping_address").text(billing_address);
            $("#table_shipping_city").text(billing_city);
            $("#table_shipping_state").text(billing_state);
        }

        var order_comments = $('#order_comments').val();
        $("#table_shipping_notes").text(order_comments);

        // Hidden and show element
        $(".form-address").hide();
        $(".comfirm-checkout").hide();
        $(".place-order").show();
        $(".back-comfirm-checkout").show();
        $(".table-address").show();
        $('html, body').animate({
            scrollTop: $("body").offset().top
        }, 500);

        //Change title
        if ($(".entry-title").text() == "Order Confirmation") {
            $(".entry-title").text("Place Order");
            $('.woocommerce-breadcrumb').html($('.woocommerce-breadcrumb').html().replace('Order Confirmation', 'Place Order'));
            $("title").text("Place Order");
        }
        else if ($(".entry-title").text() == "订单确认") {
            $(".entry-title").text("下訂單");
            $('.woocommerce-breadcrumb').html($('.woocommerce-breadcrumb').html().replace('订单确认', '下訂單'));
            $("title").text("下訂單");
        }
    });

    $(".back-comfirm-checkout").on("click", function () {
        // hide and show element
        $(".form-address").show();
        $(".comfirm-checkout").show();
        $(".place-order").hide();
        $(".back-comfirm-checkout").hide();
        $(".table-address").hide();
        $('html, body').animate({
            scrollTop: $("body").offset().top
        }, 500);

        //Change title
        if ($(".entry-title").text() == "Place Order") {
            $(".entry-title").text("Order Confirmation");
            $('.woocommerce-breadcrumb').html($('.woocommerce-breadcrumb').html().replace('Place Order', 'Order Confirmation'));
            $("title").text("Order Confirmation");
        }
        else if ($(".entry-title").text() == "下訂單") {
            $(".entry-title").text("订单确认");
            $('.woocommerce-breadcrumb').html($('.woocommerce-breadcrumb').html().replace('下訂單', '订单确认'));
            $("title").text("订单确认");
        }
    });
    // disable submit by enter event
    $('.checkout').on('keyup keypress', function (e) {
        var keyCode = e.keyCode || e.which;
        if (keyCode === 13) {
            e.preventDefault();
            return false;
        }
    });

    // input only number for point
    $(document.body).on('keydown', '#point_number', function (e) {
        // Allow: backspace, delete, tab, escape, enter and .
        if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
            // Allow: Ctrl/cmd+A
            (e.keyCode == 65 && (e.ctrlKey === true || e.metaKey === true)) ||
            // Allow: Ctrl/cmd+C
            (e.keyCode == 67 && (e.ctrlKey === true || e.metaKey === true)) ||
            // Allow: Ctrl/cmd+X
            (e.keyCode == 88 && (e.ctrlKey === true || e.metaKey === true)) ||
            // Allow: home, end, left, right
            (e.keyCode >= 35 && e.keyCode <= 39)) {
            // let it happen, don't do anything
            return;
        }
        // Ensure that it is a number and stop the keypress
        if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
            e.preventDefault();
        }
        var max_point = $(this).data('maxpoint');
        var this_var = $(this).val().concat(e.key)
        if(parseInt(this_var) > parseInt(max_point)){
            e.preventDefault();
        }
    });
});