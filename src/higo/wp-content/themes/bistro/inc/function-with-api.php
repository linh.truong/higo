<?php
/*
 * Update point to app
 * Return current point of customer on EC ( not on app, because API maybe failed )
 * @customer_id : customer id | if = null : get current user
 * @point : point number to update
 * @type_update: "add" - add point | "sub" - subtract point
*/
if ( ! function_exists( 'update_customer_point_to_app' ) ) {
	function update_customer_point_to_app( $customer_id=null, $point = null, $type_update = "add" ) {
		if(null == $customer_id){
			// Get current user
			$customer_id = wp_get_current_user()->id;
		}
		if ( null == $point ) {
			// just update point is not yet add into app
			$add_point = get_user_meta( $customer_id, "update_point", 1 );
		} else {
			// update point is not yet add into app + new point
			if($type_update == "add"){
				// add point
				$add_point = intval( get_user_meta( $customer_id, "update_point", 1 ) ) + intval( $point );
			}
			else {
				// subtract point
				$add_point =  intval( $point );
			}
		}

		// Call API
		$result      = api_add_customer_point_to_app( $customer_id, $add_point, $type_update );

		$data_return = array();
		if ( ( gettype( $result ) == "object" ) || $result['response']['message'] != "OK" ) {
			$data_return['message'] = __( 'Can not connect to Roann App.', 'woocommerce' );
			$data_return['data']    = get_user_meta( $customer_id, "_ywpar_user_total_points", 1 );
			$data_return['status']  = 0;
		} else {
			$body_result = json_decode( $result['body'] );
			if ( $body_result->error != 0 ) {
				if ($body_result->error == 69 and $type_update == "sub" ){
					$data_return['message'] = __( 'Your point not enough to use.', 'woocommerce' );
					$data_return['data']    = $body_result->data->current_point;
				}
				else{
					$data_return['message'] = __( 'Can not connect to Roann App.', 'woocommerce' );
					$data_return['data']    = get_user_meta( $customer_id, "_ywpar_user_total_points", 1 );
				}
				$data_return['status']  = 0;
			} else {
				$data_return['data']   = $body_result->data->current_point;
				$data_return['status'] = 1;
				update_user_meta( $customer_id, "_ywpar_user_total_points", $body_result->data->current_point ); // Update total_points
				update_user_meta( $customer_id, "update_point", 0 ); // Update udpate_point is 0
			}
		}
		// Update "update_point" field if api is failed
		if ( $data_return['status'] == 0 and $point != null and $type_update == "add") {
			$update_point = intval(get_user_meta( $customer_id, "update_point", 1 )) + intval( $point );
			update_user_meta( $customer_id, "update_point", $update_point );
			update_user_meta( $customer_id, "lastTransactionDate", date("Y-m-d H:i:s") );
		}

		return $data_return;

	}
}
/*
 * Get curent customer point from app
*/
if ( ! function_exists( 'get_customer_point_from_app' ) ) {
	function get_customer_point_from_app() {
		$result      = api_get_customer_info_from_app();
		$body_result = json_decode( $result );
		$data_return = array();
		if ( ( gettype( $result ) == "object" ) || $result['response']['message'] != "OK" ) {
			$data_return['message'] = __( 'Can not connect to Roann App.', 'woocommerce' );
			$data_return['data']    = - 1;
		} else {
			if ( $body_result->error != 0 ) {
				$data_return['message'] = __( 'Can not get your current point.', 'woocommerce' );
				$data_return['data']    = - 1;
			} else {
				$data_return['message'] = "0";
				$data_return['data']    = $body_result->current_point;
			}
		}

		return $data_return;

	}
}