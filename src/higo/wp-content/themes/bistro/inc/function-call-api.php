<?php
$api_url_roann = 'http://ope.higo.mb.discoverjp.info/api/';
//$api_url_roann = 'http://stg.higo.mb.discoverjp.info/api/';

/*
 * Check login
 * @username : String
 * @password : String
*/
$api_url_roann_login = $api_url_roann . 'login/sign_in';

/*
 * Add point
 * @cusNumber : Integer
 * @ecPoint : Interger
*/
$api_url_roann_add_point    = $api_url_roann . 'cus_point/ec_site_point';

/*
 * Subtract point
 * @cusNumber : Integer
 * @ecPoint : Interger
*/
$api_url_roann_sub_point   = $api_url_roann . 'cus_point/ec_site_minus_point';

/*
 * Get customer infomation
 * @cusNumber : Integer
 * @ecPoint : Interger
*/
$api_url_roann_get_customer_info = $api_url_roann . 'cus/getinfo';

/*
 * Call api with url and data
*/
if ( ! function_exists( 'call_api_roann_app' ) ) {
	function call_api_roann_app( $body, $url ) {
		$args = array(
			'body'        => $body,
			'timeout'     => '10',
			'redirection' => '5',
			'httpversion' => '1.0',
			'blocking'    => true,
			'headers'     => array(),
			'cookies'     => array()
		);

		// call API and return
		return wp_remote_post( $url, $args );

	}
}

/*
 * Get curent customer infomation from app
*/
if ( ! function_exists( 'api_get_customer_info_from_app' ) ) {
	function api_get_customer_info_from_app() {
		global $api_url_roann_get_customer_info;
		$current_user = wp_get_current_user();
		$user_app_id  = get_user_meta( $current_user->id, 'user_app_id', true );
		$auth_token   = get_user_meta( $current_user->id, 'auth_token', true );
		/* Call API from roann app to get current point*/
		$body = array(
			'id'         => $user_app_id,
			'auth_token' => $auth_token,
		);

		return call_api_roann_app( $api_url_roann_get_customer_info, $body );

	}
}

/*
 * Update customer point to app
 * @customer_id : customer id
 * @point : point number to update
 * @type_update: "add" - add point | "sub" - subtract point
*/
if ( ! function_exists( 'api_add_customer_point_to_app' ) ) {
	function api_add_customer_point_to_app( $customer_id, $point, $type_update = "add" ) {
		global $api_url_roann_add_point;
		global $api_url_roann_sub_point;
		$roann_code = get_user_meta( $customer_id, 'roann_code', true );
		/* Call API from roann app to get current point*/
		if($point != 0) {
			$body = array(
				'cusNumber' => $roann_code,
				'ecPoint'   => $point,
				'lastTransactionDate'   => date("Y-m-d H:i:s"),
			);
		}
		else {
			$body = array(
				'cusNumber' => $roann_code,
				'ecPoint'   => $point
			);
		}
		if ( $type_update == "add" ) {
			return call_api_roann_app( $body, $api_url_roann_add_point );
		} else {
			return call_api_roann_app( $body, $api_url_roann_sub_point );
		}
	}
}

