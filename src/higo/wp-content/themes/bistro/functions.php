<?php
/**
 * Bistro engine room
 *
 * @package bistro
 */

/**
 * Set the theme version number as a global variable
 */
$theme          = wp_get_theme( 'bistro' );
$bistro_version = $theme['Version'];

$theme              = wp_get_theme( 'storefront' );
$storefront_version = $theme['Version'];

/**
 * Load the individual classes required by this theme
 */
require_once( 'inc/class-bistro.php' );
require_once( 'inc/class-bistro-customizer.php' );
require_once( 'inc/class-bistro-integrations.php' );
require_once( 'inc/bistro-template-hooks.php' );
require_once( 'inc/bistro-template-functions.php' );
require_once( 'inc/plugged.php' );
require_once( 'inc/constant.php' );
require_once( 'inc/function-call-api.php' );
require_once( 'inc/function-with-api.php' );

/**
 * Do not add custom code / snippets here.
 * While Child Themes are generally recommended for customisations, in this case it is not
 * wise. Modifying this file means that your changes will be lost when an automatic update
 * of this theme is performed. Instead, add your customisations to a plugin such as
 * https://github.com/woothemes/theme-customisations
 */

/* Remove strength password when register */
function wc_ninja_remove_password_strength() {
	if ( wp_script_is( 'wc-password-strength-meter', 'enqueued' ) ) {
		wp_dequeue_script( 'wc-password-strength-meter' );
	}
}

add_action( 'wp_print_scripts', 'wc_ninja_remove_password_strength', 100 );

/**
 * Display the footer banner for shop page
 *
 * @since  1.0.0
 * @return  void
 */
if ( ! function_exists( 'customization_add_banner_footer_ec' ) ) {
	function customization_add_banner_footer_ec() {
		$args         = array(
			'taxonomy' => 'product_cat',
			'slug'     => 'roann',
		);
		$product_cat  = get_terms( 'product_cat', $args )[0];
		$thumbnail_id = get_woocommerce_term_meta( $product_cat->term_id, 'thumbnail_id', true );
		$roann_url    = get_term_link( $product_cat->term_id, 'product_cat' );
		?>

		<div class="banner_footer_ec">
			<a href="http://www.gogofoods.com.hk/en/">
				<img src="<?php echo wp_upload_dir()['baseurl'] . '/common/banner_gogofoods.png'; ?>" alt="">
			</a>
			<a href="http://primemarket.com.hk/en/">
				<img src="<?php echo wp_upload_dir()['baseurl'] . '/common/banner_primemarket.png'; ?>" alt="">
			</a>
			<a href="<?php echo $roann_url ?>">
				<img src="<?php echo wp_upload_dir()['baseurl'] . '/common/banner_roannstyle.png'; ?>" alt="">
			</a>
		</div>

		<?php

	}
}
add_action( 'customization_banner_footer_ec', 'customization_add_banner_footer_ec' );


/**
 * Display the footer banner for news page
 *
 * @since  1.0.0
 * @return  void
 */
if ( ! function_exists( 'customization_add_banner_footer_news' ) ) {
	function customization_add_banner_footer_news() {
		?>


		<div class="banner_footer_news">
			<a href="http://primemarket.com.hk/en/" style="float: left">
				<img src="<?php echo wp_upload_dir()['baseurl'] . '/common/banner_primemarket.png'; ?>" alt="">
			</a>
			<a href="<?php echo get_site_url() . '/shop' ?>">
				<img src="<?php echo wp_upload_dir()['baseurl'] . '/common/banner_roannstyle.png'; ?>" alt="">
			</a>
			<a href="<?php echo get_site_url() . '/roann' ?>" style="float: left">
				<img src="<?php echo wp_upload_dir()['baseurl'] . '/common/banner_roann.png'; ?>" alt="">
			</a>
			<a href="http://www.gogofoods.com.hk/en/">
				<img src="<?php echo wp_upload_dir()['baseurl'] . '/common/banner_gogofoods.png'; ?>" alt="">
			</a>
		</div>

		<?php

	}
}
add_action( 'customization_banner_footer_news', 'customization_add_banner_footer_news' );


/**
 * truncate_text
 *
 * @since  1.0.0
 * @return  void
 */
if ( ! function_exists( 'truncate_text' ) ) {
	function truncate_text( $text, $num_zh, $num_en, $type_return = true ) {
		if ( get_bloginfo( 'language' ) == 'zh-HK' ) {
			if ( strlen( $text ) > $num_zh ) {
				$text = preg_replace( '(<[^>]+>)', '', mb_strimwidth( $text, 0, $num_zh, '...' ) );
			} else {
				$text = preg_replace( '(<[^>]+>)', '', mb_strimwidth( $text, 0, $num_zh ) );
			}
		} else {
			$text = wp_trim_words( $text, $num_en, '...' );
		}
		if ( $type_return == false ) {
			return $text;
		}
		echo $text;
	}
}

/**
 * Register new status
 * Tutorial: http://www.sellwithwp.com/woocommerce-custom-order-status-2/
 **/
function register_awaiting_shipment_order_status() {
	register_post_status( 'wc-awaiting-shipment', array(
		'label'                     => 'Awaiting shipment',
		'public'                    => true,
		'exclude_from_search'       => false,
		'show_in_admin_all_list'    => true,
		'show_in_admin_status_list' => true,
		'label_count'               => _n_noop( 'Awaiting shipment <span class="count">(%s)</span>', 'Awaiting shipment <span class="count">(%s)</span>' )
	) );
}

add_action( 'init', 'register_awaiting_shipment_order_status' );

// Add to list of WC Order statuses
function add_awaiting_shipment_to_order_statuses( $order_statuses ) {

	$new_order_statuses = array();

	// add new order status after processing
	foreach ( $order_statuses as $key => $status ) {

		$new_order_statuses[ $key ] = $status;

		if ( 'wc-processing' === $key ) {
			$new_order_statuses['wc-awaiting-shipment'] = 'Awaiting shipment';
		}
	}

	return $new_order_statuses;
}

add_filter( 'wc_order_statuses', 'add_awaiting_shipment_to_order_statuses' );

if ( ! function_exists( 'save_tracking_number' ) ) {
	function save_tracking_number( $post_id ) {
		if ( $_POST['tracking_number'] != '' ) {
			update_post_meta( $post_id, 'tracking_number', sanitize_text_field( $_POST['tracking_number'] ) );
		}
	}
}
add_action( 'save_post', 'save_tracking_number' );