<?php
/*
Plugin Name: Woo Related Products on Posts
Plugin URI: https://said.solutions/
Description: Allows Display of Related WooCommerce Products on Posts.
Author: Matt Shirk
Version: 2.5
Author URI: https://said.solutions/
*/


// Remove bad stuff from any string entered as a color

function wrpp_xss_strip( $wrpp_input ) {
	$wrpp_input = strip_tags( $wrpp_input );
	$wrpp_input = htmlspecialchars( $wrpp_input );
	$wrpp_input = preg_replace( "/['\"&()]<>/", "", $wrpp_input );

	return $wrpp_input;
}

// Get all products from WooCommerce DB

global $wpdb;
$wrpp_tnme          = $wpdb->prefix . 'posts';
$wrpp_relateddProds = $wpdb->get_results( "SELECT post_title, ID 
	FROM $wrpp_tnme 
	WHERE post_type = 'product' 
	AND post_title != 'Auto Draft' 
        AND post_status = 'publish'
	GROUP BY ID ASC", OBJECT );


// Adds the Related Product Selection meta box in the Blog Post Editor page

function wrpp_custom_metaaa() {
	add_meta_box( 'wrpp_metaaa', __( 'Add WooCommerce Related Products to Bottom of This Post', 'prfx-textdomain' ), 'wrpp_metaaa_callback', 'post' );
}

add_action( 'add_meta_boxes', 'wrpp_custom_metaaa', 9999 );


// Outputs the Related Product Selection meta box in the Blog Post Editor page

function wrpp_metaaa_callback( $post ) {

	wp_nonce_field( basename( __FILE__ ), 'prfx_nonce' );
	$wrpp_stored_meta = get_post_meta( $post->ID );

	// Output HTML for selecting Related Products on the Blog Post Editor page
	?>

	<p>
		<span class="prfx-row-title"><?php _e( 'Select Related Products that will appear at the bottom of this post', 'prfx-textdomain' ) ?></span>
		<div class="prfx-row-content">
	<p class="form-field">
		<select multiple="multiple" name="taskOption[]">
			<?php
			$wrpp_productCount = 0;
			global $wrpp_relateddProds;
			foreach ( $wrpp_relateddProds as $wrpp_relateddProd ) {
				$wrpp_prodpostid    = $wrpp_relateddProd->ID;
				$wrpp_prodposttitle = $wrpp_relateddProd->post_title;
				$wrpp_prodposttitle = wrpp_xss_strip( $wrpp_prodposttitle );
				$wrpp_productCount ++;
//$wrpp_itemnm = 'wrpp-meta-checkbox' . $wrpp_productCount;
				$wrpp_itemnm = 'wrpp-meta-selected' . $wrpp_productCount;
//$isSelected = "";
//if ( isset ( $wrpp_stored_meta[$wrpp_itemnm] ) ) {
//    if ( (string) $wrpp_stored_meta[$wrpp_itemnm][0] === (string) $wrpp_prodpostid )
//        $isSelected = "selected='selected'";
//    else
//        $isSelected = "";
//}
//if($isSelected != ""){
//    echo "<option {$isSelected} value='{$wrpp_itemnm}'>{$wrpp_prodposttitle} (SELECTED)</option>\n";
//    } else {
//        echo "<option value='{$wrpp_itemnm}'>{$wrpp_prodposttitle}</option>\n";
//    }
				?>
				<option value="<?php echo $wrpp_prodpostid; ?>" id="<?php echo $wrpp_itemnm; ?>" name="<?php echo $wrpp_itemnm; ?>" <?php if ( isset ( $wrpp_stored_meta[ $wrpp_itemnm ] ) ) {
					selected( $wrpp_stored_meta[ $wrpp_itemnm ][0], $wrpp_prodpostid );
				} ?>><?php _e( $wrpp_prodposttitle, 'prfx-textdomain' ) ?></option>
			<?php } ?>
		</select>
	</p>

	</div>
	</p>

	<?php
}

// This Function saves the custom meta input on blog post editor page

function wrpp_metadata_save( $post_id ) {

	// Checks save status
	$wrpp_is_autosave    = wp_is_post_autosave( $post_id );
	$wrpp_is_revision    = wp_is_post_revision( $post_id );
	$wrpp_is_valid_nonce = ( isset( $_POST['prfx_nonce'] ) && wp_verify_nonce( $_POST['prfx_nonce'], basename( __FILE__ ) ) ) ? 'true' : 'false';

	// Exits script depending on save status
	if ( $wrpp_is_autosave || $wrpp_is_revision || ! $wrpp_is_valid_nonce ) {
		return;
	}

	// Checks for input and sanitizes/saves if needed
	if ( isset( $_POST['meta-text'] ) ) {
		update_post_meta( $post_id, 'meta-text', sanitize_text_field( $_POST['meta-text'] ) );
	}

	// gets selected products
	$wrpp_productCountz = 0;
	global $wrpp_relateddProds;
	$selected_val = $_POST['taskOption'];
	foreach ( $wrpp_relateddProds as $wrpp_relateddProd ) {
		$wrpp_rr = $wrpp_relateddProd->ID;
		$wrpp_productCountz ++;
//				$wrpp_itemnmz = 'wrpp-meta-checkbox' . $wrpp_productCountz;
		$wrpp_itemnmz = 'wrpp-meta-selected' . $wrpp_productCountz;
		// Checks for input and saves

//                                if( isset( $_POST[ $wrpp_itemnmz ] ) ) {
//					update_post_meta( $post_id, $wrpp_itemnmz, $wrpp_rr );
//				} else {
//					update_post_meta( $post_id, $wrpp_itemnmz, '' );
//				}
		if ( in_array( $wrpp_rr, $selected_val ) ) {
			update_post_meta( $post_id, $wrpp_itemnmz, $wrpp_rr );
		} else {
			update_post_meta( $post_id, $wrpp_itemnmz, '' );
		}

	}

}

add_action( 'save_post', 'wrpp_metadata_save' );


// Define the woocommerce_after_main_content callback
function wrpp_displayProducts( $wrpp_content ) {

	// Checks to make sure this is a post and nothing else
	if ( is_singular() && ! is_archive() && ! is_cart() && ! is_checkout() && ! is_account_page() ) {
		$wrpp_productCountzz = 0;
		global $wrpp_relateddProds;
		$wrpp_countter = 0;
		$wrpp_contenta = '';
		foreach ( $wrpp_relateddProds as $wrpp_relateddProd ) {
			$wrpp_productCountzz ++;
			$wrpp_id = $wrpp_relateddProd->ID;
//					$wrpp_itemnmzz = 'wrpp-meta-checkbox' . $wrpp_productCountzz;
			$wrpp_itemnmzz    = 'wrpp-meta-selected' . $wrpp_productCountzz;
			$wrpp_key_value   = get_post_meta( get_the_ID(), $wrpp_itemnmzz, true );
			$wrpp_add_to_cart = do_shortcode( '[add_to_cart id="' . $wrpp_id . '"]' );

			// Check if the custom field has a value.
			if ( ! empty( $wrpp_key_value ) ) {
				$wrpp_tit        = get_the_title( $wrpp_id );
				$wrpp_tit        = truncate_text( $wrpp_tit, 8, 5, false );
				$product         = new WC_Product( $wrpp_id );
				$product_price   = '<div class="product-price">$' . number_format(floatval($product->get_price()), 2) . '</div>';
				$wrpp_tit        = wrpp_xss_strip( $wrpp_tit );
				$wrpp_img        = get_the_post_thumbnail( $wrpp_id, 'medium', array( 'title' => '' . $wrpp_tit . '', 'alt' => '' . $wrpp_tit . '' ) );
				$get_image_data  = wp_get_attachment_image_src( get_post_thumbnail_id( $wrpp_id ), "medium" );
				$get_image_width = $get_image_data[1];
				$wrpp_link       = get_permalink( $wrpp_id );
				$wrpp_countter ++;
				$wrpp_contenta = $wrpp_contenta . '<li><div class="relprods" style="display:inline-block;">
						<div class="prodImg"><a href="' . $wrpp_link . '">' . $wrpp_img . '</a></div><div class="product-title" style="word-wrap: break-word; width: ' . $get_image_width . 'px;">' . $wrpp_tit . '</div>' . $product_price . $wrpp_add_to_cart . '</div></li>';
			}
		}

		// First check if there are Products and then check if there are Related Products for this blog post
		if ( $wrpp_productCountzz > 0 ) {
			if ( $wrpp_countter > 0 ) {
				/*$wrpp_csswidth = 100/$wrpp_countter;
				$wrpp_csswidthb = $wrpp_csswidth * 0.25;
				$wrpp_csswidth = $wrpp_csswidth - $wrpp_csswidthb;
				$wrpp_csswidthb = $wrpp_csswidthb/2;*/
				$wrpp_styles = '<style>
						div.prodImg{
							min-height:100px;
                                                        margin-left: 5px;
						}
						div.relprods span{
							display:none !important;
						}
						div.relprods p{
							border:0px !important;
						}
						div.relprods {
							width: auto;
							margin: 0px 2.5%;
						}
						div.relprods a {
							text-align:center;
							display:block !important;
						}
						h2.relprodtit{
							text-align: left;
							border-bottom: 1px solid;
							margin-bottom: 20px;
							padding-bottom: 10px;
							border-color: rgba(0, 0, 0, 0.1);
						}
						div.relprodscontainer {
							text-align: center;
							border-bottom: 1px solid;
							padding-bottom: 20px;
						}
					</style>';
			} else {
				$wrpp_styles = '<style></style>';
			}

			if ( $wrpp_countter > 0 ) {
				$wrpp_content = $wrpp_content . '<div class="relprodscontainer"><h2 class="relprodtit">' . __( 'Related Products:', 'woocommerce' ) . '</h2><div class="item"><ul id="content-slider" class="content-slider">' . $wrpp_contenta . '</ul></div></div>' . $wrpp_styles;
			}

			return $wrpp_content;
		}

	} elseif ( is_archive() ) {

		echo strip_tags( substr( $wrpp_content, 0, 500 ) ) . '.....<br>';
	} else {
		return $wrpp_content;

	}

}

// add the action that displays the related products after the content
//		add_filter( 'the_content', 'wrpp_displayProducts', 999999, 2 );


//Adds the meta box stylesheet when appropriate
function wrpp_admin_stylessss() {
	global $wrpp_typenow;
	if ( $wrpp_typenow == 'post' ) {
		wp_enqueue_style( 'wrpp_metaaa_box_styles', plugin_dir_url( __FILE__ ) . 'meta-box-styles.css' );
	}
}

add_action( 'admin_print_styles', 'wrpp_admin_stylessss' ); ?>