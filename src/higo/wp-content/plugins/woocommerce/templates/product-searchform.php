<?php
/**
 * The template for displaying product search form
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/product-searchform.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @author  WooThemes
 * @package WooCommerce/Templates
 * @version 2.5.0
 */

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

?>

<form role="search" method="get" class="woocommerce-product-search">
	<label class="screen-reader-text" for="woocommerce-product-search-field"><?php _e( 'Search for:', 'woocommerce' ); ?></label>
<!--		<input type="search" id="woocommerce-product-search-field" class="search-field" placeholder="--><?php //echo esc_attr_x( 'Search Products&hellip;', 'placeholder', 'woocommerce' ); ?><!--" value="--><?php //echo get_search_query(); ?><!--" name="s" title="--><?php //echo esc_attr_x( 'Search for:', 'label', 'woocommerce' ); ?><!--" />-->
<!--		<input type="submit" value="--><?php //echo esc_attr_x( 'Search', 'submit button', 'woocommerce' ); ?><!--" />-->
<!--		<input type="hidden" name="post_type" value="product" />-->
	<div class="woof_show_text_search_container">
		<img width="36" class="woof_show_text_search_loader" style="display: none;" src="<?php echo $loader_img ?>" alt="loader"/>
		<a href="javascript:void(0);" data-uid="<?php echo $unique_id ?>" class="woof_text_search_go <?php echo $unique_id ?>"></a>
		<input type="search" id="woocommerce-product-search-field" class="search-field woof_show_text_search" data-auto_res_count="<?php echo( isset( $auto_res_count ) ? $auto_res_count : 0 ) ?>" placeholder="<?php echo "Search Products..." ?>" name="woof_text"/>
	</div>
</form>
