<?php
/**
 * Shop breadcrumb
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/global/breadcrumb.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see        https://docs.woocommerce.com/document/template-structure/
 * @author        WooThemes
 * @package    WooCommerce/Templates
 * @version     2.3.0
 * @see         woocommerce_breadcrumb()
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

if ( ! empty( $breadcrumb ) ) {

	echo $wrap_before;
	global $post;
	$parent_post = get_post( wp_get_post_parent_id( $post->ID ) );

	foreach ( $breadcrumb as $key => $crumb ) {

		echo $before;

		if ( ! empty( $crumb[1] ) && sizeof( $breadcrumb ) !== $key + 1 ) {
			// Set link home page for shop site and news site
			if ( $key == 0 && ( is_woocommerce() || is_page_template( 'template-homepage.php' ) || is_cart() || is_account_page() || is_checkout() || $parent_post->post_name == "shop" ) ) {
				echo '<a href="' . get_permalink( get_page_by_path( 'shop' ) ) . '">' . esc_html( $crumb[0] ) . '</a>';
			} else {
				echo '<a href="' . esc_url( $crumb[1] ) . '">' . esc_html( $crumb[0] ) . '</a>';
			}
		} else {
			echo truncate_text(esc_html( $crumb[0] ), 60, 12);
		}

		echo $after;

		if ( sizeof( $breadcrumb ) !== $key + 1 ) {
			echo $delimiter;
		}

	}

	echo $wrap_after;

}
