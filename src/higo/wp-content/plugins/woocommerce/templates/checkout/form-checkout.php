<?php
/**
 * Checkout Form
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/checkout/form-checkout.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see        https://docs.woocommerce.com/document/template-structure/
 * @author        WooThemes
 * @package    WooCommerce/Templates
 * @version     2.3.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

wc_print_notices();

do_action( 'woocommerce_before_checkout_form', $checkout );

// If checkout registration is disabled and not logged in, the user cannot checkout
if ( ! $checkout->enable_signup && ! $checkout->enable_guest_checkout && ! is_user_logged_in() ) {
	echo apply_filters( 'woocommerce_checkout_must_be_logged_in_message', __( 'You must be logged in to checkout.', 'woocommerce' ) );

	return;
}

?>

<form name="checkout" method="post" class="checkout woocommerce-checkout" action="<?php echo esc_url( wc_get_checkout_url() ); ?>" enctype="multipart/form-data">

	<?php if ( sizeof( $checkout->checkout_fields ) > 0 ) : ?>

		<?php do_action( 'woocommerce_checkout_before_customer_details' ); ?>

		<div class="col2-set" id="customer_details">
			<div class="col-1 form-address">
				<?php
				if ( is_user_logged_in() ) {
					do_action( 'woocommerce_checkout_billing' );
				}
				?>
			</div>

			<div class="col-1 form-address">
				<h3>Coupons</h3>

				<p class="form-row form-row-first" style="width: 55%">
					<input type="text" name="coupon_code" class="input-text" placeholder="<?php esc_attr_e( 'Coupon code', 'woocommerce' ); ?>" id="coupon_code" value=""/>
				</p>

				<p class="form-row form-row-last" style="width: 33%">
					<input type="button" class="button btn-apply-coupon" name="apply_coupon" id="btn-apply-coupon" value="<?php esc_attr_e( 'Apply Coupon', 'woocommerce' ); ?>"/>
				</p>

				<div class="clear"></div>
			</div>

			<div class="col-2 form-address">
				<h3>Points</h3>
				<?php
				// Check curent point on app - call API
				$result = update_customer_point_to_app();
				if ( $result['status'] == 0 ) {
					// Call API is Failed
					?>
					<p class="form-row" style="font-weight: bold; font-size: 16px; color: #a63932">
						<?php echo $result['message']; ?>
					</p>
					<?php
				} else {
					?>
					<p class="form-row" style="font-weight: bold; font-size: 16px;">
						<?php echo esc_attr_e( 'Your current points on app: ', 'woocommerce' ) . $result['data']; ?>
					</p>
					<p class="form-row form-row-first" style="width: 55%">
						<input type="text" name="point_number" class="input-text" placeholder="<?php esc_attr_e( 'Point number', 'woocommerce' ); ?>" id="point_number" value="" data-maxpoint="<?php echo $result['data'] ?>"/>
					</p>

					<p class="form-row form-row-last" style="width: 33%">
						<input type="button" class="button btn-apply-point" name="apply_point" id="btn-apply-point" value="<?php esc_attr_e( 'Apply Point', 'woocommerce' ); ?>" style="min-width: 139.42px;"/>
					</p>
					<?php
				}
				?>

				<div class="clear"></div>
			</div>

			<div class="col-2 form-address">
				<?php
				if ( is_user_logged_in() ) {
					do_action( 'woocommerce_checkout_shipping' );
				}
				?>
			</div>


			<div class="col-1 table-address" style="display: none">
				<h3><?php _e( 'Billing Address', 'woocommerce' ); ?></h3>
				<table>
					<tr>
						<th><?php _e( 'Name', 'woocommerce' ); ?></th>
						<td id="table_billing_name"></td>
					</tr>
					<tr>
						<th><?php _e( 'Phone', 'woocommerce' ); ?></th>
						<td id="table_billing_phone"></td>
					</tr>
					<tr>
						<th><?php _e( 'Email Address', 'woocommerce' ); ?></th>
						<td id="table_billing_email"></td>
					</tr>
					<tr>
						<th><?php _e( 'Address', 'woocommerce' ); ?></th>
						<td id="table_billing_address"></td>
					</tr>
					<tr>
						<th><?php _e( 'Town / District', 'woocommerce' ); ?></th>
						<td id="table_billing_city"></td>
					</tr>
					<tr>
						<th><?php _e( 'Region', 'woocommerce' ); ?></th>
						<td id="table_billing_state"></td>
					</tr>
				</table>
			</div>

			<div class="col-2 table-address" style="display: none">
				<h3><?php _e( 'Shipping Address', 'woocommerce' ); ?></h3>
				<table>
					<tr>
						<th><?php _e( 'Name', 'woocommerce' ); ?></th>
						<td id="table_shipping_name"></td>
					</tr>
					<tr>
						<th><?php _e( 'Address', 'woocommerce' ); ?></th>
						<td id="table_shipping_address"></td>
					</tr>
					<tr>
						<th><?php _e( 'Town / District', 'woocommerce' ); ?></th>
						<td id="table_shipping_city"></td>
					</tr>
					<tr>
						<th><?php _e( 'Region', 'woocommerce' ); ?></th>
						<td id="table_shipping_state"></td>
					</tr>
					<tr>
						<th><?php _e( 'Order Notes', 'woocommerce' ); ?></th>
						<td id="table_shipping_notes"></td>
					</tr>
				</table>
			</div>
		</div>

		<?php do_action( 'woocommerce_checkout_after_customer_details' ); ?>

	<?php endif; ?>

	<h3 id="order_review_heading"><?php _e( 'Your order', 'woocommerce' ); ?></h3>

	<?php do_action( 'woocommerce_checkout_before_order_review' ); ?>

	<div id="order_review" class="woocommerce-checkout-review-order">
		<?php do_action( 'woocommerce_checkout_order_review' ); ?>
	</div>

	<div class="comfirm-checkout">
		<div class="button alt"><?php _e( 'Comfirm Order', 'woocommerce' ); ?></div>
	</div>

	<div class="back-comfirm-checkout" style="display: none; padding-top: 0">
		<div class="button alt"><i class="fa fa-arrow-left" aria-hidden="true"></i> <?php _e( 'Back', 'woocommerce' ); ?></div>
	</div>

	<?php do_action( 'woocommerce_checkout_after_order_review' ); ?>

</form>

<?php do_action( 'woocommerce_after_checkout_form', $checkout ); ?>
