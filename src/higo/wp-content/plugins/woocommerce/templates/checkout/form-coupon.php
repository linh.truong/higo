<?php
/**
 * Checkout coupon form
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/checkout/form-coupon.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @author  WooThemes
 * @package WooCommerce/Templates
 * @version 2.2
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

if ( ! wc_coupons_enabled() || ! is_user_logged_in() ) {
	return;
}

if ( empty( WC()->cart->applied_coupons ) ) {
	$info_message = apply_filters( 'woocommerce_checkout_coupon_message', __( 'Have a coupon?', 'woocommerce' ) . ' <a href="#" class="showcoupon">' . __( 'Click here to enter your code', 'woocommerce' ) . '</a>' );
	wc_print_notice( $info_message, 'notice' );
}
$info_message_point = apply_filters( 'woocommerce_checkout_coupon_message', __( 'Do you want use your points for payment?', 'woocommerce' ) . ' <a href="#" class="showpoint">' . __( 'Click here to enter your points', 'woocommerce' ) . '</a>' );
wc_print_notice( $info_message_point, 'notice' );
?>
<style>
	.woocommerce .woocommerce-info,
	.woocommerce .woocommerce-error {
		margin-bottom: 1em;
	}
</style>

<form class="checkout_coupon" method="post" style="margin-bottom: 0em">
	<h3>Coupons</h3>

	<p class="form-row form-row-first">
		<input type="text" name="coupon_code" class="input-text" placeholder="<?php esc_attr_e( 'Coupon code', 'woocommerce' ); ?>" id="coupon_code" value=""/>
	</p>

	<p class="form-row form-row-last">
		<input type="submit" class="button" name="apply_coupon" value="<?php esc_attr_e( 'Apply Coupon', 'woocommerce' ); ?>"/>
	</p>

	<div class="clear"></div>
</form>

<form class="checkout_point" method="post">
	<h3>Points</h3>
	<?php
	// Check curent point on app - call API
	$result = update_customer_point_to_app();
	if ( $result['status'] == 0 ) {
		// Call API is Failed
		?>
		<p class="form-row" style="font-weight: bold; font-size: 16px; color: #a63932">
			<?php echo $result['message']; ?>
		</p>
		<?php
	} else {
		?>
		<p class="form-row" style="font-weight: bold; font-size: 16px;">
			<?php echo esc_attr_e( 'Your current points on app: ', 'woocommerce' ) . $result['data']; ?>
		</p>
		<p class="form-row form-row-first">
			<input type="text" name="point_number" class="input-text" placeholder="<?php esc_attr_e( 'Please type a point smaller than your current point', 'woocommerce' ); ?>" id="" value="" data-maxpoint="<?php echo $result['data'] ?>"/>
		</p>

		<p class="form-row form-row-last">
			<input type="submit" class="button" name="apply_point" value="<?php esc_attr_e( 'Apply Point', 'woocommerce' ); ?>" style="min-width: 139.42px;"/>
		</p>
		<?php
	}
	?>

	<div class="clear"></div>
</form>
