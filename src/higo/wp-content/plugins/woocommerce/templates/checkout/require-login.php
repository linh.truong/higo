<?php
/**
 * Require login message when check out
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

if ( is_user_logged_in() == false ) {
	$text = 'Sorry, you need login to check out. <a href="' . esc_url( wc_get_page_permalink( 'myaccount' )) . '" class="wc-backward">Login</a>';
	$info_message = __( $text, 'woocommerce' ) ;
	wc_print_notice( $info_message, 'error' );
}