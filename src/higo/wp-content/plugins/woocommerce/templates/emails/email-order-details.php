<?php
/**
 * Order details table shown in emails.
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/emails/email-order-details.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see 	    https://docs.woocommerce.com/document/template-structure/
 * @author 		WooThemes
 * @package 	WooCommerce/Templates/Emails
 * @version     2.5.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

do_action( 'woocommerce_email_before_order_table', $order, $sent_to_admin, $plain_text, $email ); ?>

<?php if ( $_POST['tracking_number'] and $_POST['order_status'] == "wc-awaiting-shipment" ) : ?>
	<h2><?php printf( __( 'Tracking #%s', 'woocommerce' ), $_POST['tracking_number'] ); ?></h2>
<?php endif; ?>
<?php if ( ! $sent_to_admin ) : ?>
	<h2><?php printf( __( 'Order #%s', 'woocommerce' ), $order->get_order_number() ); ?></h2>
<?php else : ?>
	<h2><a class="link" href="<?php echo esc_url( admin_url( 'post.php?post=' . $order->id . '&action=edit' ) ); ?>"><?php printf( __( 'Order #%s', 'woocommerce'), $order->get_order_number() ); ?></a> (<?php printf( '<time datetime="%s">%s</time>', date_i18n( 'c', strtotime( $order->order_date ) ), date_i18n( wc_date_format(), strtotime( $order->order_date ) ) ); ?>)</h2>
<?php endif; ?>

<table class="td" cellspacing="0" cellpadding="6" style="width: 100%; font-family: 'Helvetica Neue', Helvetica, Roboto, Arial, sans-serif;" border="1">
	<thead>
		<tr>
			<th class="td" scope="col" style="text-align:left;"><?php _e( 'Product', 'woocommerce' ); ?></th>
			<th class="td" scope="col" style="text-align:left;"><?php _e( 'Quantity', 'woocommerce' ); ?></th>
			<th class="td" scope="col" style="text-align:left;"><?php _e( 'Price', 'woocommerce' ); ?></th>
		</tr>
	</thead>
	<tbody>
		<?php echo $order->email_order_items_table( array(
			'show_sku'      => $sent_to_admin,
			'show_image'    => false,
			'image_size'    => array( 32, 32 ),
			'plain_text'    => $plain_text,
			'sent_to_admin' => $sent_to_admin
		) ); ?>
	</tbody>
	<tfoot>
		<?php
			if ( $totals = $order->get_order_item_totals() ) {
				$i = 0;
				foreach ( $totals as $total ) {
					$i++;
					?>
					<tr>
						<th class="td" scope="row" colspan="2" style="text-align:left; <?php if ( $i === 1 ) echo 'border-top-width: 4px;'; ?>"><?php echo $total['label']; ?></th>
						<td class="td" style="text-align:left; <?php if ( $i === 1 ) echo 'border-top-width: 4px;'; ?>"><?php echo $total['value']; ?></td>
					</tr>
					<?php
					if( $i == 1) {
						$point_number = get_post_meta($order->id, '_order_point_number', true);
						$point_discount = get_post_meta($order->id, '_order_point_discount', true);
						if(!empty($point_number) and !empty($point_discount)) {
							?>
							<tr>
								<th class="td" scope="row" colspan="2" style="text-align:left; <?php if ( $i === 1 ) {
									echo 'border-top-width: 4px;';
								} ?>"><?php echo __( 'Pay with earn points: ', 'woocommerce' ) . $point_number. " points"; ?></th>
								<td class="td" style="text-align:left; <?php if ( $i === 1 ) {
									echo 'border-top-width: 4px;';
								} ?>">
									<?php
									echo "$".number_format(floatval($point_discount), 2);
									?>
								</td>
							</tr>
							<?php
						}
					}
					if( $i == 2) {
						$point_subtotal = YITH_WC_Points_Rewards_Earning()->get_point_by_subtotal(WC()->cart->subtotal);
						?>
						<tr>
							<th class="td" scope="row" colspan="2" style="text-align:left; <?php if ( $i === 1 ) echo 'border-top-width: 4px;'; ?>"><?php echo __( 'Point value', 'woocommerce' ) . " : "; ?></th>
							<td class="td" style="text-align:left; <?php if ( $i === 1 ) echo 'border-top-width: 4px;'; ?>">
								<?php
								if($point_subtotal == 1){
									echo $point_subtotal. __(" point", 'woocommerce');
								}
								else {
									echo $point_subtotal. __(" points", 'woocommerce');
								}; ?></td>
						</tr>
						<?php
					}
				}
				?>
		<?php
			}
		?>
	</tfoot>
</table>

<?php do_action( 'woocommerce_email_after_order_table', $order, $sent_to_admin, $plain_text, $email ); ?>
