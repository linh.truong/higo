=== WooCommerce Instagram Product Photos ===
Plugin Name: WooCommerce Instagram Product Photos
Plugin URI: http://multidots.com/
Author: Multidots
Author URI: http://multidots.com/
Contributors: dots, mp518
Stable tag: 2.0
Tags: instagram, WooCommerce instagram, instagram images, instagram plugin, WooCommerce
Requires at least: 3.8
Tested up to: 4.7.1
Donate link: 
Copyright: (c) 2015-2016 Multidots Solutions PVT LTD (info@multidots.com) 
License: GPLv3 or later
License URI: http://www.gnu.org/licenses/gpl-3.0.html

WooCommerce Instagram Product Photos displays Instagram photographs of your products, based on a hashtag.


== Description ==
Bring these images on your website, by integrating Instagram photographs, tagged with a specific hashtag, directly on to product details page.

Plugin Demo : <a href ="http://wooinstagramproductphotos.demo.store.multidots.com/">View Demo</a>

= How to setup: =

1. Enter Client ID and Redirect URI in their respective text boxes Woo Instagram General Settings Page and click on Generate Access Token button to generate token.
2. You will be asked to login to your Instagram account. Login with the same Instagram account which you used to generate Client ID and proceed.
3. Token will be automatically filled in the access token text box. Enter limit of number of photos to be displayed on product page and click Save Settings at the end to save all information.
4. Add a hashtag to each product you’d like to display Instagram images on.
5. You’re done!

= How to setup plugin : =

[youtube https://www.youtube.com/watch?v=C1PgTE8hPAI]

We always welcomes user suggestions. Let us know what you think about this plugin you liked or may have disliked. Users feedback is important for us to improve more our plugin.

= You can check our other plugins: =

1. <a href ="https://store.multidots.com/go/flat-rate">Advance Flat Rate Shipping Method For WooCommerce</a>
2. <a href ="https://store.multidots.com/go/dotstore-woocommerce-blocker">WooCommerce Blocker – Prevent Fake Orders And Blacklist Fraud Customers</a>
3. <a href ="https://store.multidots.com/go/dotstore-enhanced-ecommerce-tracking">WooCommerce Enhanced Ecommerce Analytics Integration With Conversion Tracking</a>
4. <a href ="https://store.multidots.com/go/dotstore-woo-category-banner">Woocommerce Category Banner Management</a>
5. <a href ="https://store.multidots.com/go/dotstore-woo-extra-fees">Woocommerce Conditional Extra Fees</a>
6. <a href ="https://store.multidots.com/go/dotstore-woo-product-sizechart">Woocommerce Advanced Product Size Charts</a>
7. <a href ="https://store.multidots.com/go/dotstore-admenumanager-wp">Advance Menu Manager for WordPress</a>
8. <a href ="https://store.multidots.com/go/dotstore-woo-savefor-later">Woocommerce Save For Later Cart Enhancement</a>
9. <a href ="https://store.multidots.com/go/brandagency">Brand Agency- One Page HTML Template For Agency,Startup And Business</a>
10. <a href ="https://store.multidots.com/go/Meraki">Meraki One Page HTML Resume Template</a>
11. <a href ="https://store.multidots.com/go/dotstore-aapify-theme">Appify - Multipurpose One Page Mobile App landing page HTML</a>

== Installation ==

= Minimum Requirements =

* WooCommerce 2.1 or higher

= Automatic installation =

Automatic installation is the easiest option as WordPress handles the file transfers itself and you don't need to leave your web browser. To do an automatic install of WooCommerce Instagram, log in to your WordPress dashboard, navigate to the Plugins menu and click Add New.

In the search field type WooCommerce Instagram and click Search Plugins. Once you've found our plugin you can view details about it such as the the rating and description. Most importantly, of course, you can install it by simply clicking Install Now�?.

= Manual Installation =

1. Unzip the files and upload the folder into your plugins folder (/wp-content/plugins/) overwriting older versions if they exist
2. Activate the plugin in your WordPress admin area.

== Frequently Asked Questions ==

== In which WordPress version this Plugin is compatible?

It is compatible from 3.8 to 4.7 WordPress version.

== In which WooCommerce version this Plugin is compatible? ==

It is compatible for 2.1 and greater than WooCommerce Plugin


== Screenshots ==
1. WooCommerce Instagram Screenshot 1.
2. WooCommerce Product Page Instagram Hashtag inputbox.
3. List Images on Product Page.

== Upgrade Notice ==

Automatic updates should work great for you.  As always, though, we recommend backing up your site prior to making any updates just to be sure nothing goes wrong.

== Changelog ==

= 2.0 - 20.01.2017 =
* New instagram tag API used.
* Introduce New Features:Access Token Generate Form.
* Introduce New Features:UI design Updated Woo Instagram General Settings. 

= 1.0.1 - 30.11.2015 =
* Tweak - Remote request handles on activate.

= 1.0.2 - 24.02.2016 =
* Fixes - Removed php error notice.

= 1.0.3 - 15.07.2016 =
* New - WooCommerce 2.6 compatibility tested
* New - Subscription form added
* Fixes - Minor bug solved

= 1.1  - 29-08-2016 =
* Check WordPress and WooCommerce compatibility