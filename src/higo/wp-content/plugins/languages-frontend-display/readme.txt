=== Languages Frontend Display ===
Tags: qTranslate,qTranslate-x,language,hide,disable,frontend
Donate link: 
Contributors: 
Tested up to: 4.7.2
Requires at least: 3.9
Stable tag: trunk
License: GNU General Public License v2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

qTranslate-X extension. Enable/disable languages on frontend


== Description ==
		
This Plugin is an extension for qTranslate-X (and will not work alone).

You can exclude some languages on frontend. Check the plugins settings page.

The excluded languages will not appear in language chooser.
In case someone wants to access an excluded language, he/she/it/they will be redirected to the default language. 


qTranslate-X option "Detect Browser Language" and  "URL Modification Mode -> Hide URL language information for default language" has to be false.

tested with qTranslate-X version 3.4.6.8.

Thanks to CMB2 for their easy metaboxes and optionpages!

== Installation ==
Requirements:
* qTranslate-X

Upload and install this Plugin in the same way you\'d install any other plugin.

== Screenshots ==
1. http://waterproof-webdesign.info/wp-content/uploads/2017/02/LanguagesFrontendDisplay_settings.png

== Changelog ==

= 0.0.3 =
Bug fix: some lost whitespace characters found their way between the closing and opening php tags. Resulted in a 'headers already sent' error on some servers.

= 0.0.2 =
added '(default language)' to settings multicheck

= 0.0.1 =
hurray, first stable version!

