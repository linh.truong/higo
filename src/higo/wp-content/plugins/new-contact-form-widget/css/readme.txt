=== Contact Form ===
Contributors: awordpresslife
Tags: captcha, contact, contact form, contact forms, custom form, email, feedback, form, form builder, form manager, contact widget, contact form widget, contact form shortcode 
Donate link: http://awplife.com/
Requires at least: 3.8
Tested up to: 4.7.1
Stable tag: 0.2.3
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

Contact Form Widget Shortcode Plugin For WordPress

== Description ==

https://www.youtube.com/watch?v=hWjQ3NOmfTs

Don't loose your potential client's queries. Each and every query is important for you. So, we made a very easy and simple plugin to add the contact form on your WordPress website. 

There is two method into the plugin to add the contact form on WordPress blog site.

1. First is using Contact Form Widget
2. Second is Contact Form Shortcode: **[CFW]**

= Contact Form PRO FEATURES AND DEMO =

**Features**

* Show Contact Form Using Widget
* Show Contact Form Using Shortcode [CFW]
* Manage All Queries
* Download All Queries
* Contact Form Setting
* Form Design Setting
* Form Error Message Setting
* Pagination Setting
* Dashboard to Manage All Contact Queries

**Upgrade To Premium Plugin - <a href="http://awplife.com/product/contact-form-premium/">Click Here</a>**

**Check Premium Plugin Demo - <a href="http://demo.awplife.com/contact-form-premium-admin-demo/">Click Here</a>**

== Installation ==

Install Contact Form Widget either via the WordPress.org plugin directory or by uploading the files to your server.

After activating Contact Form Widget plugin, go to plugin menu.

Login into admin dashboard. Go to Appearance --> Widgets : Contact Form Widget

Activate the Contact Form Widget into widget area fo theme.

Configure settings and save.

That's it. You're ready to go!

== Frequently Asked Questions ==

Have any queries?

Please post your question on plugin support forum

https://wordpress.org/support/plugin/new-contact-form-widget/

== Screenshots ==

1. Contact Form Shortcode Example 1
2. Contact Form Shortcode Example 2
3. Contact Form Shortcode Example 3
4. Contact Form Shortcode Example 4
5. Contact Form Shortcode Example 5
6. Contact Form Shortcode Example 6
7. Contact Form Shortcode Example 7
8. Contact Form Shortcode Example 8
9. Contact Form Widget Example 1
10. Contact Form Widget Example 2
11. Contact Form Widget Example 3
12. Contact Form Widget Example 4
13. Contact Form Widget Example 5
14. Contact Form Widget Example 6
15. Contact Form Widget Example 7
16. Contact Form Widget Example 8
17. Admin Dashboard For Query Management
18. Contact Form Settings


== Changelog ==
Feature Enhancements: Version 0.2.3
* Enhancements: Sanitize All Text Fileds
* Bug Fix: None
* Additional changes: None

Feature Enhancements: Version 0.2.2
* Enhancements: None
* Bug Fix: None
* Additional changes: None

Feature Enhancements: Version 0.2.1
* Enhancements: None
* Bug Fix: None
* Additional changes: None

Feature Enhancements: Version 0.2.0
* Enhancements: None
* Bug Fix: None
* Additional changes: None

Feature Enhancements: Version 0.1.9
* Enhancements: None
* Bug Fix: None
* Additional changes: None

Feature Enhancements: Version 0.1.8
* Enhancements: new updates
* Bug Fix: None
* Additional changes: None

Feature Enhancements: Version 0.1.7
* Enhancements: new updates
* Bug Fix: None
* Additional changes: None

Feature Enhancements: Version 0.1.6
* Enhancements: compatible for new wordpress version 4.7
* Bug Fix: None
* Additional changes: None

Feature Enhancements: Version 0.1.5
* Enhancements: None
* Bug Fix: None
* Additional changes: None

Feature Enhancements: Version 0.1.4
* Enhancements: None
* Bug Fix: None
* Additional changes: None

Feature Enhancements: Version 0.1.3
* Enhancements: None
* Bug Fix: None
* Additional changes: None

Feature Enhancements: Version 0.1.2
* Enhancements: None
* Bug Fix: None
* Additional changes: None

Feature Enhancements: Version 0.1.1
* Enhancements: None
* Bug Fix: None
* Additional changes: None

Feature Enhancements: Version 0.1.0
* Enhancements: None
* Bug Fix: None
* Additional changes: None

Feature Enhancements: Version 0.0.10
* Enhancements: None
* Bug Fix: None
* Additional changes: None

Feature Enhancements: Version 0.0.9
* Enhancements:  fix pagination error, Email setting  
* Bug Fix: None
* Additional changes: None

Feature Enhancements: Version 0.0.8
* Enhancements: None
* Bug Fix: None
* Additional changes: None

Feature Enhancements: Version 0.0.7
* Enhancements: None
* Bug Fix: None
* Additional changes: None

Feature Enhancements: Version 0.0.6
* Enhancements: None
* Bug Fix: None
* Additional changes: Working On New Wordpress Version 4.6.1

Feature Enhancements: Version 0.0.5
* Enhancements: None
* Bug Fix: Menu Position Change
* Additional changes: Working On New Wordpress Version

Feature Enhancements: Version 0.0.4
* Enhancements: None
* Bug Fix: Menu Position Change
* Additional changes: Working On New Wordpress Version

Feature Enhancements: Version 0.0.3
* Enhancements: None
* Bug Fix: None
* Additional changes: Working On New Wordpress Version

Feature Enhancements: Version 0.0.2
* Enhancements: None
* Bug Fix: None
* Additional changes: Working On New Wordpress Version

Feature Enhancements: Version 0.0.1
* Enhancements: None
* Bug Fix: None
* Additional changes: None

== Upgrade Notice ==
This is an initial release. Start with version 0.0.1 and share your feedback <a href="https://wordpress.org/support/view/plugin-reviews/new-contact-form-widget//">here</a>.