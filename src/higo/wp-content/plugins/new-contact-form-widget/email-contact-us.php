<?php
/**
 * Send mail when customer create contact us
 *
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

?>
	<style>
		.email-contact-us {
			color: #737373;
			border-collapse: collapse;
		}
		.email-contact-us th {
			width: 120px;
			text-align: left;
			vertical-align: top;
			padding: 8px 20px;
			border: 1px solid #e4e4e4;
		}
		.email-contact-us td {
			max-width: 650px;
			text-align: left;
			vertical-align: top;
			padding: 8px 20px;
			border: 1px solid #e4e4e4;
		}

	</style>
	<h2><?php echo "Inquiry #".$id; ?></h2>
	<table class="email-contact-us">
		<caption>
			<h2><?php echo $subject; ?></h2>
		</caption>
		<tr>
			<th>Name:</th>
			<td><?php echo $name; ?></td>
		</tr>
		<tr>
			<th>Email:</th>
			<td><?php echo $email; ?></td>
		</tr>
		<tr>
			<th>Order Number:</th>
			<td><?php echo $order_number; ?></td>
		</tr>
		<tr>
			<th>Message:</th>
			<td><?php echo $message; ?></td>
		</tr>
	</table>

<?php
