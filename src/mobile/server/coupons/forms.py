import datetime
import random

from django import forms
from django.forms import ModelChoiceField
from commonmodels.models import Coupons, Restaurants
from utils import constants as const


# Make Res list
class ResModelChoiceField(ModelChoiceField):
    def label_from_instance(self, obj):
        try:
            return obj.name
        except AttributeError:
            return obj.name


# Topic form
class CouponInfoForm(forms.ModelForm):
    number = forms.CharField(
        widget=forms.TextInput(
            attrs={'class': 'form-control', 'required': 'required', 'readonly': 'readonly'}),
        max_length=20, error_messages={'required': 'This field is required.'})

    title = forms.CharField(
        widget=forms.TextInput(attrs={'class': 'form-control', 'required': 'required'}), max_length=200, )

    title_cn = forms.CharField(
        widget=forms.TextInput(attrs={'class': 'form-control', 'required': 'required'}), max_length=200, )

    name = forms.CharField(
        widget=forms.TextInput(attrs={'class': 'form-control', 'required': 'required'}), max_length=200, )

    name_cn = forms.CharField(
        widget=forms.TextInput(attrs={'class': 'form-control', 'required': 'required'}), max_length=200, )

    coupon_type = forms.ChoiceField(
        choices=const.CMS_COUPONS_TYPE, required=True, widget=forms.Select(attrs={'class': 'form-control'}))

    price_off = forms.DecimalField(
        required=False,
        widget=forms.TextInput(attrs={'class': 'form-control cms_form_decimal'}))

    price_minus = forms.DecimalField(
        required=False,
        widget=forms.TextInput(attrs={'class': 'form-control cms_form_decimal'}))

    from_date = forms.DateTimeField(
        required=True,
        widget=forms.TextInput(
            attrs={'value': datetime.datetime.now().strftime('%Y-%m-%d'),
                   'class': 'form-control form-control-inline input-medium default-date-picker'}))

    to_date = forms.DateTimeField(
        required=True,
        widget=forms.TextInput(
            attrs={'value': datetime.datetime.now().strftime('%Y-%m-%d'),
                   'class': 'form-control form-control-inline input-medium default-date-picker'}))

    start_time = forms.TimeField(
        required=False,
        widget=forms.TextInput(
            attrs={'pattern': '([01]?[0-9]|2[0-3])(:[0-5][0-9])',
                   'class': 'form-control form-control-inline input-medium cms-time-picker'}))

    end_time = forms.TimeField(
        required=False,
        widget=forms.TextInput(
            attrs={'pattern': '([01]?[0-9]|2[0-3])(:[0-5][0-9])',
                   'class': 'form-control form-control-inline input-medium cms-time-picker'}))

    week_days = forms.MultipleChoiceField(
        choices=const.CMS_WEEK_DAYS, required=False,
        widget=forms.CheckboxSelectMultiple(attrs={'class': 'cms_week_days'}))

    quantity = forms.CharField(
        widget=forms.TextInput(attrs={'class': 'form-control cms_form_number', 'required': 'required'}),
        max_length=11,
        error_messages={'required': 'This field is required.'})

    comment = forms.CharField(
        widget=forms.Textarea(attrs={'class': 'form-control', 'rows': 3}), max_length=500, required=False)

    comment_cn = forms.CharField(
        widget=forms.Textarea(attrs={'class': 'form-control', 'rows': 3}), max_length=500, required=False)

    condition = forms.CharField(
        widget=forms.Textarea(attrs={'class': 'form-control', 'rows': 3}), max_length=2000, required=False)

    condition_cn = forms.CharField(
        widget=forms.Textarea(attrs={'class': 'form-control', 'rows': 3}), max_length=2000, required=False)

    is_mail_notify = forms.ChoiceField(
        choices=const.CMS_NOTIFY_STATUS, required=True, widget=forms.Select(attrs={'class': 'form-control'}))

    is_sms_notify = forms.ChoiceField(
        choices=const.CMS_NOTIFY_STATUS, required=True, widget=forms.Select(attrs={'class': 'form-control'}))

    is_app_notify = forms.ChoiceField(
        choices=const.CMS_NOTIFY_STATUS, required=True, widget=forms.Select(attrs={'class': 'form-control'}))

    current_register = forms.CharField(
        widget=forms.TextInput(attrs={'class': 'form-control cms_form_number', 'readonly': 'readonly'}),
        max_length=11, required=False, )

    hash_tags = forms.CharField(
        widget=forms.TextInput(attrs={'class': 'form-control'}), max_length=200, required=False)

    class Meta:
        model = Coupons
        fields = (
            'number', 'title', 'title_cn', 'name', 'name_cn', 'from_date', 'to_date', 'start_time', 'end_time',
            'week_days', 'quantity', 'coupon_type', 'comment', 'comment_cn', 'current_register', 'hash_tags',
            'condition', 'condition_cn', 'is_mail_notify', 'is_sms_notify', 'is_app_notify', 'price_off', 'price_minus')

    def __init__(self, *args, **kwargs):
        super(CouponInfoForm, self).__init__(*args, **kwargs)
        self.fields['current_register'].initial = 0
        self.fields['is_mail_notify'].initial = 0
        self.fields['is_sms_notify'].initial = 0
        self.fields['is_app_notify'].initial = 0
        self.fields['week_days'].initial = [1, 2, 3, 4, 5, 6, 7]
        self.fields['hash_tags'].initial = '#coupon '
        self.fields['start_time'].initial = '00:00'
        self.fields['end_time'].initial = '23:30'
        num_length = 6
        number = random.randint(10 ** (num_length - 1), (10 ** (num_length) - 1))
        number_str = str(number)
        self.fields['number'].initial = 'CP' + number_str


