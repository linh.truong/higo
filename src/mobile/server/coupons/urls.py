"""toyosu URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.10/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url

import coupons.views as services

urlpatterns = [
    # ---------------------------------------------------------
    # ======================= COUPONS =========================
    # ---------------------------------------------------------

    # List
    url(r'^cms/coupons/list/$', services.load_list_coupons_page, name='coupons_list'),
    url(r'^coupons_list_json/$', services.get_list_coupons_as_json, name='get_coupons_list_as_json'),
    # Insert
    url(r'^cms/coupons/add/$', services.add_coupon, name='add_coupon'),
    # Edit
    url(r'^cms/coupons/edit/([\d\D]+)/$', services.edit_coupon, name='edit_coupon'),
    # Delete
    url(r'^cms/coupons/delete/([\d\D]+)/$', services.delete_coupons, name='delete_coupons'),
]
