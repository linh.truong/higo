from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.http import HttpResponseRedirect
from django.shortcuts import render

import commonmodels.views_cms as cms
from commonmodels.models import Coupons
from coupons.forms import CouponInfoForm
from utils import constants as const
from utils import utility as util

# ---------------------------------------------------------
# ====================== COUPONS ==========================
# ---------------------------------------------------------

# URL define
list_url = '/cms/coupons/list'
add_url = '/cms/coupons/add'
edit_url = '/cms/coupons/edit/'


# Load list coupons page
@login_required(login_url=const.CMS_LOGIN_URL)
def load_list_coupons_page(request):
    return render(request, 'coupons/list.html')


# get coupons list by json
@login_required(login_url=const.CMS_LOGIN_URL)
def get_list_coupons_as_json(request):
    # add curr_res_id params if you need
    _res_id = request.session[const.SESSION_CMS_CURR_RES_ID]
    mapping = {
        'curr_res_id': _res_id,
        'search': ['name', 'name_cn', 'title', 'title_cn', 'from_date', 'to_date', 'start_time', 'end_time'],
        'sort': {
            0: 'id',
            1: 'name',
            2: 'name_cn',
            3: 'coupon_type',
            4: 'title',
            5: 'title_cn',
            6: 'from_date',
            7: 'to_date',
            8: 'start_time',
            9: 'end_time'
        },
    }
    return cms.get_list_display(request, Coupons, mapping)


# add coupons
@login_required(login_url=const.CMS_LOGIN_URL)
def add_coupon(request):
    context = {}
    model = Coupons
    if request.method == 'POST':
        try:
            form_info = CouponInfoForm(request.POST)

            if form_info.is_valid():

                # check duplicate
                check_cp = cms.chk_dup_ins(model, form_info, ['name', 'name_cn'])
                if check_cp:
                    messages.error(request, check_cp['errors'])
                    context['form_info'] = form_info
                    context['request_method'] = request.method
                    return render(request, 'coupons/add.html', context)

                cp = form_info.save(commit=False)

                cp.restaurant_id = request.session[const.SESSION_CMS_CURR_RES_ID]
                _week_days = ','.join(request.POST.getlist('week_days'))

                cp.week_days = _week_days

                # Get & Set image_path
                image_file = request.FILES.get('image_path', False)
                if image_file:
                    cp.image_path.delete(False)
                    cp.image_path.save(image_file.name, image_file, False)

                cp.save()

                return HttpResponseRedirect(list_url)
            else:
                print("Err" + str(form_info.errors))
                messages.add_message(request, messages.ERROR, form_info.errors, extra_tags='add_foods')
        except Exception as e:
            print("error" + str(e))
            messages.add_message(request, messages.ERROR, e, extra_tags='add_foods')
        return render(request, 'error.html')
    else:

        form_info = CouponInfoForm()
        # Set return context
        context['form_info'] = form_info
        context['request_method'] = request.method

        return render(request, 'coupons/add.html', context)


# Edit coupon
@login_required(login_url=const.CMS_LOGIN_URL)
def edit_coupon(request, coupon_id):
    return cms.action_insert_or_update(request, Coupons, CouponInfoForm, 'coupons', coupon_id)


# Delete coupons
@login_required(login_url=const.CMS_LOGIN_URL)
def delete_coupons(request, coupon_id):
    return cms.action_insert_or_update(request, Coupons, CouponInfoForm, 'coupons', coupon_id)


# check duplicate code
def check_dub_code(_number, _old_number=None):
    try:
        result = None
        _coupon = Coupons.objects.get(number=_number)
        if _old_number is not None:
            if str(_number) != str(_old_number):
                if _coupon:
                    result = _coupon.number
        else:
            if _coupon:
                result = _coupon.number
    except Exception as e:
        print("Check dup err : " + str(e))
        pass
    return result
