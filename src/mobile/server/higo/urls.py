"""higo URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.10/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url, include
from django.contrib import admin
import commonmodels.views

# Overrides the default 400 handler django.views.defaults.bad_request
from utils import constants

handler400 = 'commonmodels.views.bad_request'

# Overrides the default 403 handler django.views.defaults.permission_denied
handler403 = 'commonmodels.views.permission_denied'

# Overrides the default 404 handler django.views.defaults.page_not_found
handler404 = 'commonmodels.views.page_not_found'

# Overrides the default 500 handler django.views.defaults.server_error
handler500 = 'commonmodels.views.server_error'

urlpatterns = [
    url(r'^$', commonmodels.views.api_home),
    url(r'^admin/', admin.site.urls),

    url(r'^', include('api.urls')),
    url(r'^', include('commonmodels.urls')),
    url(r'^', include('coupons.urls')),
    url(r'^', include('customers.urls')),
    url(r'^', include('foods.urls')),
    url(r'^', include('items.urls')),
    url(r'^', include('restaurants.urls')),
    url(r'^', include('suppliers.urls')),
    url(r'^', include('topics.urls')),
    url(r'^', include('locations.urls')),
    url(r'^', include('login.urls')),

    # Internationalization
    url(r'^i18n/', include('django.conf.urls.i18n')),
]

# if constants.DEBUG is False:
#     from django.contrib.staticfiles.urls import staticfiles_urlpatterns
#     urlpatterns += staticfiles_urlpatterns()
