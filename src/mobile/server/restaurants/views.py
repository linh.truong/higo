from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.http import HttpResponseRedirect
from django.shortcuts import render
from django.contrib.auth.models import User
from django.db import transaction
from django.templatetags.tz import register

import commonmodels.views as cmn
import commonmodels.views_cms as cms
from restaurants.forms import ResPointInfoForm, PrizeInfoForm, ContactInfoForm, RestaurantInfoForm, ReservationInfoForm
from utils import constants as const
from utils import utility as util
from commonmodels.models import Restaurants, RestaurantPoints, Prizes, Reservations, Contacts


# ---------------------------------------------------------
# =======================Restaurants Points================
# ---------------------------------------------------------
# Generate point code
def generate_point_code():
    _point_code = User.objects.make_random_password(30)
    _res_point = Restaurants.objects.filter(code=str(_point_code))
    if _res_point:
        return generate_point_code()

    return _point_code


# Load point list page
@login_required(login_url=const.CMS_LOGIN_URL)
def load_list_point_page(request):
    return render(request, 'res_points/list.html')


# Get point list as json
@login_required(login_url=const.CMS_LOGIN_URL)
def get_list_point_as_json(request):
    # add curr_res_id params if you need
    _res_id = request.session[const.SESSION_CMS_CURR_RES_ID]
    mapping = {
        'curr_res_id': _res_id,
        'search': ['code', 'point', 'comment', 'restaurant__name'],
        'sort': {
            0: 'id',
            1: 'code',
            2: 'restaurant_name',
            3: 'issue_date',
            4: 'point',
            5: 'comment',
        },
        # Map foreign key
        'map': ['restaurant_id', 'restaurant_name']
    }
    return cms.get_list_display(request, RestaurantPoints, mapping)


# Add new point with QR Code
@login_required(login_url=const.CMS_LOGIN_URL)
def add_point(request):
    if request.method == 'POST':
        try:
            form_info = ResPointInfoForm(request.session[const.SESSION_CMS_CURR_RES_ID], request.POST)
            if form_info.is_valid():

                # Get cur info
                _curr_res = Restaurants.objects.get(pk=request.session[const.SESSION_CMS_CURR_RES_ID])

                # Transfer data from Form to Model
                res_point = form_info.save(commit=False)

                # Generate point code
                _point_code = generate_point_code()

                # Generate qr code string
                _data_encode = \
                    _curr_res.code + _point_code + request.POST.get('issue_date') + request.POST.get('point')

                _qr_code = util.res_info_md5_hash(str(_data_encode))

                # Manual field
                res_point.code = _point_code
                res_point.qr_code = _qr_code
                res_point.restaurant_id = request.session['cms_auth_sign_in_curr_res_id']
                res_point.save()

                return HttpResponseRedirect('/cms/res_points/list')
            else:
                print("error" + str(form_info.errors))
                messages.add_message(request, messages.ERROR, form_info.errors, extra_tags='add_res_points')
        except Exception as e:
            print("error" + str(e))
            messages.add_message(request, messages.ERROR, e, extra_tags='add_res_points')
        return render(request, 'error.html')

    else:
        context = {}
        form_info = ResPointInfoForm(res_id=request.session[const.SESSION_CMS_CURR_RES_ID])
        # Set return context
        context['form_info'] = form_info
        context['request_method'] = request.method

        return render(request, 'res_points/add.html', context)


# Delete prize
@login_required(login_url=const.CMS_LOGIN_URL)
def delete_res_point(request, res_point_id):
    return cms.action_insert_or_update(request, RestaurantPoints, ResPointInfoForm, 'res_points', res_point_id)


# ---------------------------------------------------------
# ======================= PRIZES ==========================
# ---------------------------------------------------------
# Load prize list page
@login_required(login_url=const.CMS_LOGIN_URL)
def load_list_prize_page(request):
    return render(request, 'prizes/list.html')


# Get prize list as json
@login_required(login_url=const.CMS_LOGIN_URL)
def get_list_prize_as_json(request):
    # add curr_res_id params if you need
    _res_id = request.session[const.SESSION_CMS_CURR_RES_ID]
    mapping = {
        'curr_res_id': _res_id,
        'search': ['title', 'title_cn', 'request_point', 'from_date'],
        'sort': {
            0: 'id',
            1: 'title',
            2: 'title_cn',
            3: 'request_point',
            4: 'from_date',
        },
        # Map foreign key
        'map': ['restaurant_id', 'restaurant_name']
    }
    return cms.get_list_display(request, Prizes, mapping)


# Load prize add page
@login_required(login_url=const.CMS_LOGIN_URL)
def add_prize(request):
    return cms.action_insert_or_update(request, Prizes, PrizeInfoForm, 'prizes')


# Load prize edit page
@login_required(login_url=const.CMS_LOGIN_URL)
def edit_prize(request, prize_id):
    return cms.action_insert_or_update(request, Prizes, PrizeInfoForm, 'prizes', prize_id)


# Delete prize
@login_required(login_url=const.CMS_LOGIN_URL)
def delete_prize(request, prize_id):
    return cms.action_insert_or_update(request, Prizes, PrizeInfoForm, 'prizes', prize_id)


# ---------------------------------------------------------------
# ======================= RESERVATIONS ==========================
# ---------------------------------------------------------------
# Load reservation list page
@login_required(login_url=const.CMS_LOGIN_URL)
def load_list_reservation_page(request):
    return render(request, 'reservations/list.html')


# Get reservation list as json
@login_required(login_url=const.CMS_LOGIN_URL)
def get_list_reservation_as_json(request):
    # add curr_res_id params if you need
    _res_id = request.session[const.SESSION_CMS_CURR_RES_ID]
    mapping = {
        'curr_res_id': _res_id,
        'search': ['name', 'email', 'phone', 'reserve_type', 'reserve_date', 'reserve_time', 'status'],
        'sort': {
            0: 'id',
            1: 'name',
            2: 'email',
            3: 'phone',
            4: 'reserve_type',
            5: 'reserve_date',
            6: 'reserve_time',
            7: 'num_adult',
            8: 'num_child',
            9: 'status',
        },
        # Map foreign key
        'map': ['restaurant_id', 'restaurant_name']
    }
    return cms.get_list_display(request, Reservations, mapping)


# Load reserve add page
@login_required(login_url=const.CMS_LOGIN_URL)
def add_reservation(request):
    return cms.action_insert_or_update(request, Reservations, ReservationInfoForm, 'reservations')


# Load reservation edit page
@login_required(login_url=const.CMS_LOGIN_URL)
def edit_reservation(request, reserve_id):
    return cms.action_insert_or_update(request, Reservations, ReservationInfoForm, 'reservations', reserve_id)


# Delete reservation
@login_required(login_url=const.CMS_LOGIN_URL)
def delete_reservation(request, reserve_id):
    return cms.action_insert_or_update(request, Reservations, ReservationInfoForm, 'reservations', reserve_id)


# -----------------------------------------------------------
# ======================= CONTACTS ==========================
# -----------------------------------------------------------
# Load contact list page
@login_required(login_url=const.CMS_LOGIN_URL)
def load_list_contact_page(request):
    return render(request, 'contacts/list.html')


# Get contact list as json
@login_required(login_url=const.CMS_LOGIN_URL)
def get_list_contact_as_json(request):
    # add curr_res_id params if you need
    _res_id = request.session[const.SESSION_CMS_CURR_RES_ID]
    mapping = {
        'curr_res_id': _res_id,
        'search': ['name', 'email', 'phone', 'status', 'content'],
        'sort': {
            0: 'id',
            1: 'name',
            2: 'email',
            3: 'phone',
            4: 'content',
            5: 'status',
            6: 'update_date',
        },
        # Map foreign key
        'map': ['restaurant_id', 'restaurant_name']
    }
    return cms.get_list_display(request, Contacts, mapping)


# Load contact add page
@login_required(login_url=const.CMS_LOGIN_URL)
def add_contact(request):
    return cms.action_insert_or_update(request, Contacts, ContactInfoForm, 'contacts')


# Load contact edit page
@login_required(login_url=const.CMS_LOGIN_URL)
def edit_contact(request, contact_id):
    return cms.action_insert_or_update(request, Contacts, ContactInfoForm, 'contacts', contact_id)


# Delete contact
@login_required(login_url=const.CMS_LOGIN_URL)
def delete_contact(request, contact_id):
    return cms.action_insert_or_update(request, Contacts, ContactInfoForm, 'contacts', contact_id)


# --------------------------------------------------------------
# ======================= RESTAURANTS ==========================
# --------------------------------------------------------------
# Load restaurant list page
@login_required(login_url=const.CMS_LOGIN_URL)
def load_list_restaurant_page(request):
    return render(request, 'restaurants/list.html')


# Get restaurant list as json
@login_required(login_url=const.CMS_LOGIN_URL)
def get_list_restaurant_as_json(request):
    mapping = {
        'search': ['code', 'name', 'address', 'email', 'phone'],
        'sort': {
            0: 'id',
            1: 'code',
            2: 'name',
            3: 'address',
            4: 'email',
            5: 'phone',
            6: 'update_date',
        },
        # Map foreign key
        'map': ['restaurant_id', 'restaurant_name']
    }
    ope_id = request.session[const.SESSION_CMS_U_ID]
    type_id = request.session[const.SESSION_CMS_R_MAP]
    return cms.prc_get_list_display(request, Restaurants, mapping, 'get_list_ope_res', ope_id, type_id)


# Load restaurant add page
@login_required(login_url=const.CMS_LOGIN_URL)
def add_restaurant(request):
    return cms.action_insert_or_update(request, Restaurants, RestaurantInfoForm, 'restaurants')


# Load restaurant edit page
@login_required(login_url=const.CMS_LOGIN_URL)
def edit_restaurant(request, restaurant_id):
    return cms.action_insert_or_update(request, Restaurants, RestaurantInfoForm, 'restaurants', restaurant_id)
