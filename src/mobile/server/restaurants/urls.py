"""toyosu URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.10/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url,include
import restaurants.views as services

urlpatterns = [
    # ---------------------------------------------------------
    # ======================= QR CODE =========================
    # ---------------------------------------------------------
    url(r'^cms/res_points/list/$', services.load_list_point_page, name='res_points_list'),
    url(r'^res_points_list_json/$', services.get_list_point_as_json, name='get_res_points_list_as_json'),
    url(r'^cms/res_points/add/$', services.add_point, name='add_res_points'),
    url(r'^cms/res_points/delete/([\d\D]+)/$', services.delete_res_point, name='delete_res_points'),

    # ---------------------------------------------------------
    # ======================= PRIZES ==========================
    # ---------------------------------------------------------
    url(r'^cms/prizes/list/$', services.load_list_prize_page, name='prizes_list'),
    url(r'^prizes_list_json/$', services.get_list_prize_as_json, name='get_prizes_list_as_json'),
    url(r'^cms/prizes/add/$', services.add_prize, name='add_prizes'),
    url(r'^cms/prizes/edit/([\d\D]+)/$', services.edit_prize, name='edit_prizes'),
    url(r'^cms/prizes/delete/([\d\D]+)/$', services.delete_prize, name='delete_prizes'),

    # ---------------------------------------------------------------
    # ======================= RESERVATIONS ==========================
    # ---------------------------------------------------------------
    url(r'^cms/reservations/list/$', services.load_list_reservation_page, name='reservations_list'),
    url(r'^reservations_list_json/$', services.get_list_reservation_as_json, name='get_reservations_list_as_json'),
    url(r'^cms/reservations/add/$', services.add_reservation, name='add_reservation'),
    url(r'^cms/reservations/edit/([\d\D]+)/$', services.edit_reservation, name='edit_reservation'),
    url(r'^cms/reservations/delete/([\d\D]+)/$', services.delete_reservation, name='delete_reservation'),

    # -----------------------------------------------------------
    # ======================= CONTACTS ==========================
    # -----------------------------------------------------------
    url(r'^cms/contacts/list/$', services.load_list_contact_page, name='contacts_list'),
    url(r'^contacts_list_json/$', services.get_list_contact_as_json, name='get_contacts_list_as_json'),
    url(r'^cms/contacts/add/$', services.add_contact, name='add_contacts'),
    url(r'^cms/contacts/edit/([\d\D]+)/$', services.edit_contact, name='edit_contacts'),
    url(r'^cms/contacts/delete/([\d\D]+)/$', services.delete_contact, name='delete_contact'),

    # --------------------------------------------------------------
    # ======================= RESTAURANTS ==========================
    # --------------------------------------------------------------
    url(r'^cms/restaurants/list/$', services.load_list_restaurant_page, name='restaurants_list'),
    url(r'^restaurants_list_json/$', services.get_list_restaurant_as_json, name='get_restaurants_list_as_json'),
    url(r'^cms/restaurants/add/$', services.add_restaurant, name='add_restaurants'),
    url(r'^cms/restaurants/edit/([\d\D]+)/$', services.edit_restaurant, name='edit_restaurants'),
]
