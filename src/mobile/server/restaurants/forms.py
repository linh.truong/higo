import datetime

from django import forms
from django.forms import ModelChoiceField
from commonmodels.models import Restaurants, RestaurantPoints, Prizes, Areas, Stations, Reservations
from utils import constants as const


# Make choice list binding
class ResModelChoiceField(ModelChoiceField):
    def label_from_instance(self, obj):
        try:
            return obj.name
        except AttributeError:
            return obj.name


class AreaModelChoiceField(ModelChoiceField):
    def label_from_instance(self, obj):
        try:
            return obj.name
        except AttributeError:
            return obj.name


class StationModelChoiceField(ModelChoiceField):
    def label_from_instance(self, obj):
        try:
            return obj.name
        except AttributeError:
            return obj.name


# RestaurantPoints form
class ResPointInfoForm(forms.ModelForm):
    restaurant = ResModelChoiceField(
        queryset=None, empty_label=None, required=True,
        widget=forms.Select(attrs={'class': 'form-control', 'disabled': False}))
    issue_date = forms.DateTimeField(
        required=True,
        widget=forms.TextInput(
            attrs={'value': datetime.datetime.now().strftime('%Y-%m-%d'),
                   'class': 'form-control form-control-inline input-medium default-date-picker'}))

    point = forms.IntegerField(
        widget=forms.TextInput(attrs={'class': 'form-control cms_form_number', 'required': 'required'}))

    comment = forms.CharField(
        widget=forms.Textarea(attrs={'class': 'form-control', 'rows': 3}), max_length=500, required=False)

    comment_cn = forms.CharField(
        widget=forms.Textarea(attrs={'class': 'form-control', 'rows': 3}), max_length=500, required=False)

    class Meta:
        model = RestaurantPoints
        fields = (
            'issue_date', 'point', 'comment', 'comment_cn')

    def __init__(self, res_id, *args, **kwargs):
        super(ResPointInfoForm, self).__init__(*args, **kwargs)
        self.fields['restaurant'].queryset = Restaurants.objects.filter(pk=res_id, is_delete=const.ACTIVE_FLAG)
        self.fields['restaurant'].initial = res_id


# Restaurant Contact form
class ContactInfoForm(forms.ModelForm):
    # restaurant = ResModelChoiceField(
    #     queryset=None, empty_label=None, required=True,
    #     widget=forms.Select(attrs={'class': 'form-control', 'disabled': False}))

    name = forms.CharField(
        widget=forms.TextInput(attrs={'class': 'form-control'}), max_length=100, required=True)

    email = forms.CharField(
        widget=forms.EmailInput(attrs={'class': 'form-control'}), max_length=50, required=True)

    phone = forms.CharField(
        widget=forms.TextInput(attrs={'class': 'form-control'}), max_length=20, required=True)

    content = forms.CharField(
        widget=forms.Textarea(attrs={'class': 'form-control', 'rows': 3}), max_length=2000, required=True)

    status = forms.ChoiceField(
        choices=const.CMS_STATE, required=True, widget=forms.Select(attrs={'class': 'form-control'}))

    class Meta:
        model = Prizes
        fields = (
            'name', 'email', 'phone', 'content', 'status',)

    def __init__(self, *args, **kwargs):
        super(ContactInfoForm, self).__init__(*args, **kwargs)
        # self.fields['restaurant'].queryset = Restaurants.objects.filter(pk=res_id, is_delete=const.ACTIVE_FLAG)
        # self.fields['restaurant'].initial = res_id


# RestaurantPrizes form
class PrizeInfoForm(forms.ModelForm):
    # restaurant = ResModelChoiceField(
    #     queryset=None, empty_label=None, required=True,
    #     widget=forms.Select(attrs={'class': 'form-control', 'disabled': False}))

    request_point = forms.IntegerField(
        widget=forms.TextInput(attrs={'class': 'form-control cms_form_number', 'maxlength': 11}), required=True)

    title = forms.CharField(
        widget=forms.TextInput(attrs={'class': 'form-control'}), max_length=200, required=True)

    title_cn = forms.CharField(
        widget=forms.TextInput(attrs={'class': 'form-control'}), max_length=200, required=True)

    link_address = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control'}), max_length=500,
                                   required=True)

    content = forms.CharField(
        widget=forms.Textarea(attrs={'class': 'form-control', 'rows': 3}), max_length=500, required=True)

    content_cn = forms.CharField(
        widget=forms.Textarea(attrs={'class': 'form-control', 'rows': 3}), max_length=500, required=True)

    condition = forms.CharField(
        widget=forms.Textarea(attrs={'class': 'form-control', 'rows': 3}), max_length=2000, required=True)

    condition_cn = forms.CharField(
        widget=forms.Textarea(attrs={'class': 'form-control', 'rows': 3}), max_length=2000, required=True)

    from_date = forms.DateTimeField(
        required=True,
        widget=forms.TextInput(
            attrs={'value': datetime.datetime.now().strftime('%Y-%m-%d'),
                   'class': 'form-control form-control-inline input-medium default-date-picker'}))

    to_date = forms.DateTimeField(
        required=True,
        widget=forms.TextInput(
            attrs={'value': datetime.datetime.now().strftime('%Y-%m-%d'),
                   'class': 'form-control form-control-inline input-medium default-date-picker'}))

    start_time = forms.TimeField(
        required=False,
        widget=forms.TextInput(
            attrs={'pattern': '([01]?[0-9]|2[0-3])(:[0-5][0-9])',
                   'class': 'form-control form-control-inline input-medium cms-time-picker'}))

    end_time = forms.TimeField(
        required=False,
        widget=forms.TextInput(
            attrs={'pattern': '([01]?[0-9]|2[0-3])(:[0-5][0-9])',
                   'class': 'form-control form-control-inline input-medium cms-time-picker'}))

    week_days = forms.MultipleChoiceField(
        choices=const.CMS_WEEK_DAYS, required=False,
        widget=forms.CheckboxSelectMultiple(attrs={'class': 'cms_week_days'}))

    current_register = forms.IntegerField(
        widget=forms.TextInput(attrs={'class': 'form-control cms_form_number', 'maxlength': 11}), required=True)

    quantity = forms.IntegerField(
        widget=forms.TextInput(attrs={'class': 'form-control cms_form_number', 'maxlength': 11}), required=True)

    is_mail_notify = forms.ChoiceField(
        choices=const.CMS_NOTIFY_STATUS, required=True, widget=forms.Select(attrs={'class': 'form-control'}))

    is_sms_notify = forms.ChoiceField(
        choices=const.CMS_NOTIFY_STATUS, required=True, widget=forms.Select(attrs={'class': 'form-control'}))

    is_app_notify = forms.ChoiceField(
        choices=const.CMS_NOTIFY_STATUS, required=True, widget=forms.Select(attrs={'class': 'form-control'}))

    is_already_send = forms.ChoiceField(
        choices=const.CMS_SEND_STATUS, required=True, widget=forms.Select(attrs={'class': 'form-control'}))

    class Meta:
        model = Prizes
        fields = (
            'title', 'title_cn', 'content', 'content_cn', 'condition', 'condition_cn', 'request_point',
            'from_date', 'to_date', 'start_time', 'end_time', 'week_days', 'current_register', 'quantity',
            'is_mail_notify', 'is_sms_notify', 'is_app_notify', 'is_already_send', 'link_address'
        )

    def __init__(self, *args, **kwargs):
        super(PrizeInfoForm, self).__init__(*args, **kwargs)
        # self.fields['restaurant'].queryset = Restaurants.objects.filter(pk=res_id, is_delete=const.ACTIVE_FLAG)
        # self.fields['restaurant'].initial = res_id
        self.fields['current_register'].initial = 0
        self.fields['quantity'].initial = 1000
        self.fields['is_mail_notify'].initial = 0
        self.fields['is_sms_notify'].initial = 0
        self.fields['is_app_notify'].initial = 0
        self.fields['week_days'].initial = [1, 2, 3, 4, 5, 6, 7]
        self.fields['start_time'].initial = '00:00'
        self.fields['end_time'].initial = '23:30'
        self.fields['from_date'].initial = '2017-06-01'
        self.fields['to_date'].initial = '2025-01-01'
        self.fields['content'].initial = 'View product detail'
        self.fields['content_cn'].initial = '產品詳情'
        self.fields['link_address'].initial = 'http://'


# Reservation form
class ReservationInfoForm(forms.ModelForm):
    # restaurant = ResModelChoiceField(
    #     queryset=None, empty_label=None, required=True,
    #     widget=forms.Select(attrs={'class': 'form-control', 'disabled': False}))

    name = forms.CharField(
        widget=forms.TextInput(attrs={'class': 'form-control'}), max_length=100, required=True)

    email = forms.EmailField(
        widget=forms.EmailInput(attrs={'class': 'form-control'}), max_length=50, required=True)

    phone = forms.CharField(
        widget=forms.TextInput(attrs={'class': 'form-control cms_form_number'}), max_length=20, required=True)

    reserve_type = forms.ChoiceField(
        choices=const.CMS_RESERVE_TYPE, required=True, widget=forms.Select(attrs={'class': 'form-control'}))

    reserve_date = forms.DateTimeField(
        required=True,
        widget=forms.TextInput(
            attrs={'value': datetime.datetime.now().strftime('%Y-%m-%d'),
                   'class': 'form-control form-control-inline input-medium default-date-picker'}))

    reserve_time = forms.TimeField(
        required=False,
        widget=forms.TextInput(
            attrs={'pattern': '([01]?[0-9]|2[0-3])(:[0-5][0-9])',
                   'class': 'form-control form-control-inline input-medium cms-time-picker'}))

    num_adult = forms.IntegerField(
        widget=forms.TextInput(attrs={'class': 'form-control cms_form_number', 'maxlength': 11}), required=True)

    num_child = forms.IntegerField(
        widget=forms.TextInput(attrs={'class': 'form-control cms_form_number', 'maxlength': 11}), required=True)

    comment = forms.CharField(
        widget=forms.Textarea(attrs={'class': 'form-control', 'rows': 3}), max_length=500, required=False)

    status = forms.ChoiceField(
        choices=const.CMS_STATE, required=True, widget=forms.Select(attrs={'class': 'form-control'}))

    class Meta:
        model = Reservations
        fields = (
            'name', 'email', 'phone', 'reserve_type', 'reserve_date', 'reserve_time', 'num_adult', 'num_child',
            'comment', 'status',
        )

    def __init__(self, *args, **kwargs):
        super(ReservationInfoForm, self).__init__(*args, **kwargs)
        # self.fields['restaurant'].queryset = Restaurants.objects.filter(pk=res_id, is_delete=const.ACTIVE_FLAG)
        # self.fields['restaurant'].initial = res_id


# Restaurant form
class RestaurantInfoForm(forms.ModelForm):
    area = AreaModelChoiceField(
        queryset=None, empty_label=None, required=True, widget=forms.Select(attrs={'class': 'form-control'}))

    station = StationModelChoiceField(
        queryset=None, empty_label=None, required=True, widget=forms.Select(attrs={'class': 'form-control'}))

    code = forms.CharField(
        widget=forms.TextInput(attrs={'class': 'form-control'}), max_length=10, required=True)

    name = forms.CharField(
        widget=forms.TextInput(attrs={'class': 'form-control'}), max_length=100, required=True)

    name_cn = forms.CharField(
        widget=forms.TextInput(attrs={'class': 'form-control'}), max_length=100, required=False)

    contact_person = forms.CharField(
        widget=forms.TextInput(attrs={'class': 'form-control'}), max_length=100, required=False)

    email = forms.EmailField(
        widget=forms.EmailInput(attrs={'class': 'form-control'}), max_length=50, required=True)

    phone = forms.CharField(
        widget=forms.TextInput(attrs={'class': 'form-control'}), max_length=20, required=True)

    address = forms.CharField(
        widget=forms.Textarea(attrs={'class': 'form-control', 'rows': 3}), max_length=500, required=True)

    address_cn = forms.CharField(
        widget=forms.Textarea(attrs={'class': 'form-control', 'rows': 3}), max_length=500, required=False)

    description = forms.CharField(
        widget=forms.Textarea(attrs={'class': 'form-control', 'rows': 3}), max_length=2000, required=False)

    description_cn = forms.CharField(
        widget=forms.Textarea(attrs={'class': 'form-control', 'rows': 3}), max_length=2000, required=False)

    google_map_url = forms.CharField(
        widget=forms.TextInput(attrs={'class': 'form-control'}), max_length=500, required=False)

    class Meta:
        model = Restaurants
        fields = (
            'area', 'station', 'code', 'name', 'name_cn', 'contact_person', 'email', 'phone',
            'address', 'address_cn', 'description', 'description_cn', 'google_map_url',
        )

    def __init__(self, *args, **kwargs):
        super(RestaurantInfoForm, self).__init__(*args, **kwargs)
        self.fields['area'].queryset = Areas.objects.filter(is_delete=const.ACTIVE_FLAG)
        self.fields['station'].queryset = Stations.objects.filter(is_delete=const.ACTIVE_FLAG)
