/**
 * Created by tho.pham on 8/16/2016.
 */
$(document).ready(function () {
    //Search
    // $('#search').on('keyup', function (e) {
    //     $('#tblData').dataTable().fnFilter($(this).val());
    // });
    // //pagination
    // $('#tblData').dataTable({
    //     "aaSorting": [[4, "desc"]],
    //     "bFilter": true,
    //     "bLengthChange": false,
    //     "iDisplayLength": 5,
    // });
    //Filter item by supplier
    // var supplier_name = $('#id_supplier option:selected').text();
    // $('#tblData').dataTable().fnFilter(supplier_name);
    $('#btnSave').on('click', function () {
        var countRowVisible = $('#dynamic-table tr.gradeX:visible').length;
        if (countRowVisible == 0) {
            $('#items_error').removeAttr('style');
            $('#btnPrint').attr('disabled', true);
            $('#btnSave').attr('disabled', true);
            $('#btnSend').attr('disabled', true);
            $('#btnSendForEdit').attr('disabled', true);
        }
    });
});

$(document).keypress(function (e) {
    if (e.which == 13 && !$(event.target).is("textarea")) {
        e.preventDefault();
    }
});

//Add Extra Label Value formset
$(document).ready(function () {
    checkDisplay();
    var supplier = $('#hdSupplierId').val();
    if (supplier == null) {
        $('#btnSave').attr('disabled', true);
        $('#btnSend').attr('disabled', true);
        $('#btnPrint').attr('disabled', true);
        $('#btnSendForEdit').attr('disabled', true);
    } else {
        $('#btnSave').removeAttr('disabled');
        $('#btnSend').removeAttr('disabled');
        $('#btnPrint').removeAttr('disabled');
        $('#btnSendForEdit').removeAttr('disabled');
    }
    $('#add_more_right').click(function () {
        cloneMore('div.table-right:last', 'formset_right');
    });
    $('#add_more_left').click(function () {
        cloneMore('div.table-left:last', 'formset_left');
    });
    $('#add_more_code').click(function () {
        cloneMore('div.table-code:last', 'formset_code');
    });
    function cloneMore(selector, type) {
        var display = $(selector).css("display")
        if (display == 'none') {
            $(selector).removeAttr("style")
        }
        else {
            var total = $('#id_' + type + '-TOTAL_FORMS').val();
            var newElement = $(selector).clone(true);
            newElement.removeAttr("style")
            newElement.find(':input').each(function () {
                var name = $(this).attr('name').replace('-' + (total - 1) + '-', '-' + total + '-');
                var id = 'id_' + name;
                $(this).attr({'name': name, 'id': id}).val('').removeAttr('checked');
            });
            newElement.find('label').each(function () {
                var newFor = $(this).attr('for').replace('-' + (total - 1) + '-', '-' + total + '-');
                $(this).attr('for', newFor);
            });
            total++;
            $('#id_' + type + '-TOTAL_FORMS').val(total);
            $(selector).after(newElement);
        }
    }

    function checkDisplay() {
        var request_method = $('#request_method').val();
        if (request_method == 'GET') {
            if ($('#id_formset_right-TOTAL_FORMS').val() > 1) {
                $('div.table-right:last').remove();
                $('#id_formset_right-TOTAL_FORMS').val($('#id_formset_right-TOTAL_FORMS').val() - 1);
            } else {
                $('div.table-right:last').css("display", "none");
            }
            if ($('#id_formset_left-TOTAL_FORMS').val() > 1) {
                $('div.table-left:last').remove();
                $('#id_formset_left-TOTAL_FORMS').val($('#id_formset_left-TOTAL_FORMS').val() - 1);
            } else {
                $('div.table-left:last').css("display", "none");
            }
            if ($('#id_formset_code-TOTAL_FORMS').val() > 1) {
                $('div.table-code:last').remove();
                $('#id_formset_code-TOTAL_FORMS').val($('#id_formset_code-TOTAL_FORMS').val() - 1);
            } else {
                $('div.table-code:last').css("display", "none");
            }
            $('#dynamic-table tr.gradeX:last').css("display", "none");
            $('#items_error').css("display", "none");
        } else if (request_method == 'POST') {
            var right_label = $('#id_formset_right-0-label').val();
            var right_somevalue = $('#id_formset_right-0-value').val();
            if (right_label == "" && right_somevalue == "") {
                $('div.table-right:last').css("display", "none");
            }
            var left_label = $('#id_formset_left-0-label').val();
            var left_somevalue = $('#id_formset_left-0-value').val();
            if (left_label == "" && left_somevalue == "") {
                $('div.table-left:last').css("display", "none");
            }
            var code_label = $('#id_formset_code-0-label').val();
            var code_somevalue = $('#id_formset_code-0-value').val();
            if (code_label == "" && code_somevalue == "") {
                $('div.table-code:last').css("display", "none");
            }
            var item = $('#id_formset_item-0-item').val();
            var quantity = $('#id_formset_item-0-quantity').val();
            var price = $('#id_formset_item-0-price').val();
            var amount = $('#id_formset_item-0-amount').val();
            if (quantity == "" && price == "" && amount == "") {
                $('#dynamic-table tr.gradeX:last').css("display", "none");
                $('#items_error').removeAttr('style');
            } else $('#items_error').css("display", "none");
        }
    }

    $(document).on('click', "[class^=removerow-left]", function () {
        if ($('#id_formset_left-TOTAL_FORMS').val() == 1) {
            var total = $('#id_formset_left-TOTAL_FORMS').val();
            $('div.table-left:last').find(':input').each(function () {
                var name = $(this).attr('name').replace('-' + total + '-', '-' + (total - 1) + '-');
                var id = 'id_' + name;
                $(this).attr({'name': name, 'id': id}).val('').removeAttr('value');
            });
            $(this).parents("div.table-left:last").css("display", "none")
        } else {
            var minus = $('input[name=formset_left-TOTAL_FORMS]').val() - 1;
            $('#id_formset_left-TOTAL_FORMS').val(minus);
            $(this).parents("div.table-left").remove();
            var i = 0;
            $('div.table-left').each(function () {
                var $tds = $(this).find('input');
                var label = $tds[0].name;
                var value = $tds[1].name;
                if (label.replace(/[^\d.]/g, '') == 0) {
                    i++;
                } else {
                    for (i; i < minus; i++) {
                        $tds[0].name = label.replace(/\d+/g, i);
                        $tds[0].id = 'id_' + $tds[0].name;
                        $tds[1].name = value.replace(/\d+/g, i);
                        $tds[1].id = 'id_' + $tds[1].name;
                        i++;
                        break;
                    }
                }
            });
        }
    });

    $(document).on('click', "[class^=removerow-right]", function () {
        if ($('#id_formset_right-TOTAL_FORMS').val() == 1) {
            var total = $('#id_formset_right-TOTAL_FORMS').val();
            $('div.table-right:last').find(':input').each(function () {
                var name = $(this).attr('name').replace('-' + total + '-', '-' + (total - 1) + '-');
                var id = 'id_' + name;
                $(this).attr({'name': name, 'id': id}).val('').removeAttr('value');
            });
            $(this).parents("div.table-right").css("display", "none");
        } else {
            var minus = $('input[name=formset_right-TOTAL_FORMS]').val() - 1;
            $('#id_formset_right-TOTAL_FORMS').val(minus);
            $(this).parents("div.table-right").remove();
            var i = 0;
            $('div.table-right').each(function () {
                var $tds = $(this).find('input');
                var label = $tds[0].name;
                var value = $tds[1].name;
                if (label.replace(/[^\d.]/g, '') == 0) {
                    i++;
                } else {
                    for (i; i < minus; i++) {
                        $tds[0].name = label.replace(/\d+/g, i);
                        $tds[0].id = 'id_' + $tds[0].name;
                        $tds[1].name = value.replace(/\d+/g, i);
                        $tds[1].id = 'id_' + $tds[1].name;
                        i++;
                        break;
                    }
                }
            });
        }
    });

    $(document).on('click', "[class^=removerow-code]", function () {
        if ($('#id_formset_code-TOTAL_FORMS').val() == 1) {
            var total = $('#id_formset_code-TOTAL_FORMS').val();
            $('div.table-code:last').find(':input').each(function () {
                var name = $(this).attr('name').replace('-' + total + '-', '-' + (total - 1) + '-');
                var id = 'id_' + name;
                $(this).attr({'name': name, 'id': id}).val('').removeAttr('value');
            });
            $(this).parents("div.table-code").css("display", "none")
        } else {
            var minus = $('input[name=formset_code-TOTAL_FORMS]').val() - 1;
            $('#id_formset_code-TOTAL_FORMS').val(minus);
            $(this).parents("div.table-code").remove();
            var i = 0;
            $('div.table-code').each(function () {
                var $tds = $(this).find('input');
                var label = $tds[0].name;
                var value = $tds[1].name;
                if (label.replace(/[^\d.]/g, '') == 0) {
                    i++;
                } else {
                    for (i; i < minus; i++) {
                        $tds[0].name = label.replace(/\d+/g, i);
                        $tds[0].id = 'id_' + $tds[0].name;
                        $tds[1].name = value.replace(/\d+/g, i);
                        $tds[1].id = 'id_' + $tds[1].name;
                        i++;
                        break;
                    }
                }
            });
        }
    });
});

//Company Information
$(document).ready(function () {

    //toggle `popup` / `inline` mode
    $.fn.editable.defaults.mode = 'inline';
    //Get company id
    var company_id = $('#company_id').val();
    //make status editable
    $('#companyname').editable({
        type: 'text',
        pk: company_id,
        url: '/orders/change_company/' + company_id + '/',
        title: 'Enter company name',
        success: function (response, newValue) {

            if (!response) {
                return "Unknown error!"
            }
            if (response.success === false) {
                return respond.msg;
            }
        }
    });

    $('#address').editable({
        type: 'text',
        pk: company_id,
        url: '/orders/change_company/' + company_id + '/',
        title: 'Enter company address',
        success: function (response, newValue) {

            if (!response) {
                return "Unknown error!"
            }
            if (response.success === false) {
                return respond.msg;
            }
        }
    });

    $('#email').editable({
        type: 'text',
        pk: company_id,
        url: '/orders/change_company/' + company_id + '/',
        title: 'Enter company email',
        validate: function (value) {
            var valid = valib.String.isEmailLike(value)
            if (valid == false) return 'Please insert valid email'
        },
        success: function (response, newValue) {

            if (!response) {
                return "Unknown error!"
            }
            if (response.success === false) {
                return respond.msg;
            }
        }
    });

});
//Supplier Information
$(document).ready(function (hdSupplierId) {
    var hdSupplierId = $('#hdSupplierId').val();
    loadSupplierInfo(hdSupplierId);

    var callback = function () {
        $.ajax({
            method: "POST",
            url: '/orders/supplier_search_by_code/',
            dataType: 'JSON',
            data: {
                'csrfmiddlewaretoken': $('input[name="csrfmiddlewaretoken"]').val(),
                'supplier_code': $("#form_supplier_code").val()
            },
            responseTime: 200,
            response: function (settings) {
                if (settings.data.value) {
                    this.responseText = '{"success": true}';
                }
                else {
                    this.responseTime = '{"success: false, "msg": "required"}';
                }
            },
            success: function (json) {
                console.log(json);
                $('#hdSupplierId').val(json['id']);
                $("#form_supplier_code").val(json['code']);
                $('#supplier_name').editable('destroy');
                $('#supplier_address').editable('destroy');
                $('#supplier_email').editable('destroy');

                $('#supplier_name').attr('data-pk', json['id']);
                $('#supplier_name').text(json['name']);
                $('#supplier_address').text(json['address']);

                $('#supplier_email').attr('data-pk', json['id']);
                $('#supplier_email').text(json['email']);
                $('#customer_payment_term').text('Payment Term: ' + json['term'] + ' days');
                $('#customer_payment_mode').text('Payment Mode: ' + json['payment_mode']);
                $('#customer_credit_limit').text('Credit Limit: ' + json['credit_limit']);
                loadSupplierInfo(json['id']);
                $('#id_tax').find('option').removeAttr("selected");
                $('#id_tax').find('option').removeAttr("disabled");
                $('#id_tax').find('option[value="' + json['tax_id'] + '"]').attr("selected", "selected");
                $('#id_tax').val(json['tax_id']);
                $('#id_tax option:not(:selected)').attr('disabled', true);
                // load tax again
                var taxid = parseInt($('#id_tax').val());
                if (isNaN(taxid)) {
                    $('#id_tax_amount').val(0);
                    $('#id_total').val(parseFloat($('#id_tax_amount').val()) - parseFloat($('#id_discount').val()) + parseFloat($('#id_subtotal').val()));
                } else {
                    $.ajax({
                        method: "POST",
                        url: '/orders/load_tax/',
                        dataType: 'JSON',
                        data: {
                            'csrfmiddlewaretoken': $('input[name="csrfmiddlewaretoken"]').val(),
                            'tax_id': taxid,
                        },
                        success: function (json) {
                            var tax_amount = (parseFloat(json) * parseFloat($('#id_subtotal').val())) / 100;
                            $('#id_tax_amount').val(tax_amount.toFixed(2));
                            var total = parseFloat($('#id_subtotal').val()) + tax_amount;
                            $('#id_total').val(total.toFixed(2));
                        }
                    });
                }
                $('#id_currency').find('option').removeAttr('selected');
                $('#id_currency').find('option').removeAttr('disabled');
                $('#id_currency option[value=' + json['currency_id'] + ']').attr('selected', 'selected');
                $('#id_currency').val(json['currency_id']);
                $('#id_currency option:not(:selected)').attr('disabled', true);
            }
        })
    };
    $("#form_supplier_code").keypress(function (e) {
        if (e.which == 13) {
            e.preventDefault();
            callback();
        }
    });
    $('#btnSearchSupplier').on('click', function () {
        var datatbl = $('#supplier-table').DataTable();
        datatbl.destroy();
        $('#supplier-table').dataTable({
            "iDisplayLength": 5,
            "bLengthChange": false,
            "order": [[0, "desc"]],
            "serverSide": true,
            "ajax": {
                "url": "/orders/suppliers_list_as_json/"
            },
            "columns": [
                {"data": "code", "sClass": "text-left"},
                {"data": "name", "sClass": "text-left"},
                {"data": "term_days", "sClass": "text-left"},
                {"data": "payment_mode", "sClass": "text-left"},
                {"data": "credit_limit", "sClass": "text-left"},
                {
                    "orderable": false,
                    "data": null,
                    "render": function (data, type, full, meta) {
                        return '<input type="radio" name="choices" id="' +
                            full.id + '" class="call-checkbox" value="' + full.id + '">';
                    }
                }
            ]
        });
    });

    $('#btnSupplierSelect').on('click', function () {
        var supplier_select_id = $("input[name='choices']:checked").attr('id');

        $('#hdSupplierId').val(supplier_select_id);

        $('#id_currency option[value=' + supplier_select_id + ']').attr('selected', 'selected');

        var nRow = $("input[name='choices']:checked").parents('tr')[0];
        var jqInputs = $('td', nRow);
        $("#form_supplier_code").val(jqInputs[0].innerText);

        $(this).attr('data-dismiss', 'modal');
        callback();
    });

    var hdSupplierId = $('#hdSupplierId').val();
    loadSupplierInfo(hdSupplierId);

    function loadSupplierInfo(hdSupplierId) {
        $.fn.editable.defaults.mode = 'inline';

        //make status editable
        $('#supplier_name').editable({
            type: 'text',
            pk: hdSupplierId,
            url: '/orders/change_supplier/' + hdSupplierId + '/',
            title: 'Enter supplier name',
            success: function (response, newValue) {
                if (!response) {
                    return "Unknown error!"
                }
                if (response.success === false) {
                    return respond.msg;
                }
            }
        });

        $('#supplier_address').editable({
            type: 'text',
            pk: hdSupplierId,
            url: '/orders/change_supplier/' + hdSupplierId + '/',
            title: 'Enter supplier address',
            success: function (response, newValue) {
                if (!response) {
                    return "Unknown error!"
                }
                if (response.success === false) {
                    return respond.msg;
                }
            }
        });

        $('#supplier_email').editable({
            type: 'text',
            pk: hdSupplierId,
            url: '/orders/change_supplier/' + hdSupplierId + '/',
            title: 'Enter supplier email',
            validate: function (value) {
                var valid = valib.String.isEmailLike(value)
                if (valid == false) return 'Please insert valid email'
            },
            success: function (response, newValue) {
                if (!response) {
                    return "Unknown error!"
                }
                if (response.success === false) {
                    return respond.msg;
                }
            }
        });
    };

    // $('#id_supplier').change(function () {
    //     var supplier_id = parseInt($(this).val());
    //     $.ajax({
    //         method: "POST",
    //         url: '/orders/supplier/',
    //         dataType: 'JSON',
    //         data: {
    //             'csrfmiddlewaretoken': $('input[name="csrfmiddlewaretoken"]').val(),
    //             'supplier_id': supplier_id,
    //         },
    //         responseTime: 200,
    //         response: function (settings) {
    //             if (settings.data.value) {
    //                 this.responseText = '{"success": true}';
    //             } else {
    //                 this.responseText = '{"success": false, "msg": "required"}';
    //             }
    //         },
    //         success: function (json) {
    //             console.log(json);
    //             $('#hdSupplierId').val(json['id']);
    //
    //             $('#supplier_name').editable('destroy');
    //             $('#supplier_address').editable('destroy');
    //             $('#supplier_email').editable('destroy');
    //
    //             $('#supplier_name').attr('data-pk', json['id']);
    //             $('#supplier_name').text(json['name']);
    //             $('#supplier_address').attr('data-pk', json['id']);
    //             $('#supplier_address').text(json['address']);
    //             $('#supplier_email').attr('data-pk', json['id']);
    //             $('#supplier_email').text(json['email']);
    //
    //             $('#customer_payment_term').text('Payment Term: ' + json['term'] + ' days');
    //             $('#customer_payment_mode').text('Payment Mode: ' + json['payment_mode']);
    //             $('#customer_credit_limit').text('Credit Limit: ' + json['credit_limit']);
    //             loadSupplierInfo(json['id']);
    //         }
    //     });
    // });
});

//Event check checkbox
$('input[type=checkbox]').click(function () {
    if ($(this).is(':checked'))
        $(this).attr('checked', 'checked');
    else
        $(this).removeAttr('checked');
});

//Add order item
$(document).ready(function () {
    if ($('#id_formset_item-TOTAL_FORMS').val() > 1) {
        var display = $('#dynamic-table tr.gradeX:last').css("display");
        if (display == 'none') {
            $('#dynamic-table tr.gradeX:last').removeAttr("style");
            $('#dynamic-table tr.gradeX:last').remove();
            $('#id_formset_item-TOTAL_FORMS').val($('#id_formset_item-TOTAL_FORMS').val() - 1);
        }
    }

    // Search Item by Part number
    $('#txtPartNo').on('keypress', function (e) {
        if (e.which == 13) {
            addOrderItemByPartNo($('#txtPartNo').val());
            $('#items_error').css('display', 'none');
        }
    });
    function addOrderItemByPartNo(part_number) {
        var exclude_item_array = [];
        var exclude_item_list = {};
        var hdCustomerId = $('#hdCustomerId').val();
        $('#dynamic-table tr.gradeX').each(function () {
            var display = $(this).css("display");
            currentRow = $(this).closest('tr').find('input');
            if (display != 'none') {
                exclude_item_array.push(currentRow[3].value);
            }
        });
        if (exclude_item_array.length > 0) {
            exclude_item_list = JSON.stringify(exclude_item_array);
        }
        $.ajax({
            method: "POST",
            url: '/orders/get_orderitems_by_part_no/',
            dataType: 'JSON',
            data: {
                'csrfmiddlewaretoken': $('input[name="csrfmiddlewaretoken"]').val(),
                'part_number': part_number,
                'customer_id': hdCustomerId,
                'exclude_item_list': exclude_item_list
            },
            success: function (json) {
                console.log(json);
                if (json.length > 0){
                    var allVals = [];
                    $.each(json, function (i, item) {
                        allVals.push({
                            id: item.item_id,
                            part_no: item.part_no,
                            name: item.item_name,
                            supplier: item.supplier_code,
                            location_code: item.location_code,
                            part_gp: item.part_gp,
                            unit_price: item.sales_price,
                            currency: item.currency_code,
                            location_id: item.location_id,
                            currency_id: item.currency_id,
                            uom: item.uom,
                            supplier_id: item.supplier_id,
                        });
                    });
                    if (allVals.length > 0) {
                        cloneMore('#dynamic-table tr.gradeX:last', 'formset_item', allVals);
                        $('input[checked=checked]').each(function () {
                            $(this).removeAttr('checked');
                        });
                        $('#items_error').css('display', 'none');
                    }
                    //Change currency
                    var currency_id = parseInt($('#id_currency option:selected').val());
                    var currency_name = $('#id_currency option:selected').text();
                    var arrItems = [];
                    $('#dynamic-table tr.gradeX').each(function () {
                        currentRow = $(this).closest('tr').find('input');
                        arrItems.push({
                            item_id: currentRow[3].value,
                            currency_id: currentRow[13].value
                        });
                    });
                    changeCurrency(arrItems, currency_id, currency_name);

                    $('#dynamic-table tr.gradeX:last').each(function () {
                        $("input[name*='wanted_date']").datepicker({
                            format: 'yyyy-mm-dd',
                            todayHighlight: true,
                            autoclose: true,
                        });
                        $("input[name*='schedule_date']").datepicker({
                            format: 'yyyy-mm-dd',
                            todayHighlight: true,
                            autoclose: true
                        });
                    });
                } else {
                    sweetAlert("Oops...", "No item matches!", "info");
                }
            }
        });
    }
    //return false;
    $('#btnAddItems').on('click', function () {
        var allVals = [];
        var table = $('#tblData').DataTable();
        var rowcollection = table.$(".call-checkbox:checked", {"page": "all"});
        rowcollection.each(function (index, elem) {
            var row = table.row('#' + elem.id).data();
            allVals.push({
                id: row.item_id,
                price: $(elem).val(),
                name: row.item_name,
                supplier: row.supplier_code,
                ref_no: row.refer_number,
                ref_line: row.refer_line,
                location_code: row.location_code,
                purchase_price: row.purchase_price,
                part_no: row.part_no,
                part_gp: row.part_gp,
                currency: row.currency_code,
                location_id: row.location_id,
                currency_id: row.currency_id,
                uom: row.unit,
                supplier_id: row.supplier_id,
                minimun_order: row.minimun_order,
                ref_id: row.refer_id,
                customer_po_no: row.customer_po_no
            });

            table.row('#' + elem.id).node().setAttribute("style", "display: none");
        });
        if (allVals.length > 0) {
            cloneMore('#dynamic-table tr.gradeX:last', 'formset_item', allVals);
            $('input[checked=checked]').each(function () {
                $(this).removeAttr('checked');
            });
            $('#items_error').css('display', 'none');
        }
        $(this).attr('data-dismiss', 'modal');
        //Change currency
        var currency_id = parseInt($('#id_currency option:selected').val());
        var currency_name = $('#id_currency option:selected').text();
        var arrItems = [];
        $('#dynamic-table tr.gradeX').each(function () {
            currentRow = $(this).closest('tr').find('input');
            arrItems.push({
                item_id: currentRow[3].value,
                currency_id: currentRow[12].value
            });
        });
        changeCurrency(arrItems, currency_id, currency_name);
        // validationItemsFormset();

        $('#dynamic-table tr.gradeX:last').each(function () {
            $("input[name*='wanted_date']").datepicker({
                format: 'yyyy-mm-dd',
                todayHighlight: true,
                autoclose: true
            });
            $("input[name*='schedule_date']").datepicker({
                format: 'yyyy-mm-dd',
                todayHighlight: true,
                autoclose: true
            });
            // $("input[name*='wanted_date']").datepicker("update", new Date());
            // $("input[name*='schedule_date']").datepicker("update", new Date());
        });
    });


    function cloneMore(selector, type, allVals) {
        var display = $(selector).css("display")
        var order_type = $('#order_type').text();
        var sum = 0;
        var i = 0;
        var item_id = 0;
        if (display == 'none') {
            //show first row of table and set Item, Price of dialog
            $(selector).removeAttr("style")
            // $(selector).find("option").each(function () {
            //     $(this).removeAttr('selected');
            //     if ($(this).val() == allVals[i].id) {
            //         $(this).prop("selected", true);
            //         // $(this).attr("selected", true);
            //     }
            // });
            $(selector).find('label').each(function () {
                var name = $(this).attr('name').replace('-' + (total - 1) + '-', '-' + total + '-');
                var id = 'id_' + name;
                $(this).attr({'name': name, 'id': id}).val('').removeAttr('checked');
            });
            findInput = $(selector).find('input');
            // currentItem = $(selector).closest('tr').find('option:selected');
            currentLabel = $(selector).closest('tr').find('label');
            //add value to Input
            findInput[0].value = 1;
            findInput[1].value = allVals[i].part_no;
            findInput[2].value = allVals[i].name;
            findInput[3].value = allVals[i].id;
            findInput[4].value = allVals[i].customer_po_no;
            if (allVals[i].minimun_order == '' || allVals[i].minimun_order == 'None') {
                findInput[5].value = 1;
            } else findInput[5].value = allVals[i].minimun_order;
            findInput[6].value = parseFloat(allVals[i].purchase_price).toFixed(2);
            findInput[11].value = allVals[i].currency;
            findInput[12].value = allVals[i].currency_id;
            findInput[13].value = allVals[i].ref_line;
            findInput[14].value = allVals[i].ref_no;
            if (allVals[i].location_code == '' || allVals[i].location_id == 'None') {
                findInput[15].value = '';
                findInput[16].value = '';
            } else {
                findInput[15].value = allVals[i].location_code;
                findInput[16].value = allVals[i].location_id;
            }
            findInput[17].value = allVals[i].uom;
            findInput[18].value = allVals[i].supplier;
            findInput[19].value = allVals[i].supplier_id;
            findInput[20].value = allVals[i].part_gp;
            findInput[21].value = allVals[i].minimun_order;
            findInput[22].value = allVals[i].ref_id;
            // if (findInput[9].value < 1 && order_type == 1) {
            //     $(selector).attr('style', 'background-color: pink !important');
            //     $('#items_error').removeAttr('style');
            //     $('#items_error').attr('style', 'color: pink !important');
            //     $('#items_error').text('The product don’t have enough stock quantity!');
            // }
            // if (allVals[i].original_price == "" || allVals[i].original_price == "None") {
            //     findInput[7].value = "";
            // } else findInput[7].value = allVals[i].original_price;
            // if (findInput[7].value == "" || findInput[7].value == "None") {
            //     findInput[10].value = 0;
            //     $('#validate_error').text('Price of product must greater than 0 and not none');
            //     $('#validate_error').removeAttr('style');
            //     $('#btnSave').attr('disabled', true);
            // }
            // else findInput[10].value = findInput[5].value * findInput[7].value;
            // findInput[11].value = 0;
            // findInput[12].value = 0;
            //add value to Label
            currentLabel[0].textContent = findInput[0].value;
            currentLabel[1].textContent = findInput[1].value;
            currentLabel[2].textContent = findInput[2].value;
            currentLabel[3].textContent = findInput[11].value;
            currentLabel[4].textContent = findInput[13].value;
            currentLabel[5].textContent = findInput[14].value;
            currentLabel[6].textContent = findInput[15].value;
            currentLabel[7].textContent = findInput[17].value;
            currentLabel[8].textContent = findInput[18].value;
            currentLabel[9].textContent = findInput[20].value;
            sum += parseInt(findInput[8].value);
            $('#id_subtotal').val(sum);
            if (isNaN(sum)) {
                sum = 0;
                $('#id_subtotal').val(0);
                $('#id_total').val(0);
            }
            if ($('#id_tax_amount').val()) {
                sum += parseFloat($('#id_tax_amount').val());
            }
            if ($('#id_discount').val()) {
                sum -= parseFloat($('#id_discount').val());
            }
            $('#id_total').val(sum);
            //if selected items > 1
            i = 1;
        }
        ;
        $('#btnSave').removeAttr('disabled');
        for (i; i < allVals.length; i++) {
            $(selector).each(function () {
                $("input[name*='wanted_date']").datepicker('remove');
                $("input[name*='schedule_date']").datepicker('remove');
            });
            if (allVals[i].id != 0) {
                var newElement = $(selector).clone(true);
                var total = $('#id_' + type + '-TOTAL_FORMS').val();
                newElement.removeAttr("style")
                newElement.find(':input').each(function () {
                    var name = $(this).attr('name').replace('-' + (total - 1) + '-', '-' + total + '-');
                    var id = 'id_' + name;
                    $(this).attr({'name': name, 'id': id}).val('').removeAttr('checked');
                });
                newElement.find('label').each(function () {
                    var name = $(this).attr('name').replace('-' + (total - 1) + '-', '-' + total + '-');
                    var id = 'id_' + name;
                    $(this).attr({'name': name, 'id': id}).val('').removeAttr('checked');
                });
                //Set selected items of dialog to Item Column
                // var value = allVals[i].id;
                // newElement.find("option").each(function () {
                //     $(this).removeAttr('selected');
                //     if ($(this).val() == value) {
                //         $(this).attr("selected", true);
                //     }
                // });
                //Set selected price of dialog to Price Column
                var a = newElement.find('input');
                currentRow = newElement.closest('tr').find('label');
                if (a.length > 1) {
                    a[0].value = parseInt($(selector).find('input')[0].value) + 1;
                    a[1].value = allVals[i].part_no;
                    a[2].value = allVals[i].name;
                    a[3].value = allVals[i].id;
                    a[4].value = allVals[i].customer_po_no;
                    if (allVals[i].minimun_order == '' || allVals[i].minimun_order == 'None') {
                        a[5].value = 1;
                    } else a[5].value = allVals[i].minimun_order;
                    a[6].value = parseFloat(allVals[i].purchase_price).toFixed(2);
                    a[11].value = allVals[i].currency;
                    a[12].value = allVals[i].currency_id;
                    a[13].value = allVals[i].ref_line;
                    a[14].value = allVals[i].ref_no;
                    if (allVals[i].location_code == '' || allVals[i].location_id == 'None') {
                        a[15].value = '';
                        a[16].value = '';
                    } else {
                        a[15].value = allVals[i].location_code;
                        a[16].value = allVals[i].location_id;
                    }
                    a[17].value = allVals[i].uom;
                    a[18].value = allVals[i].supplier;
                    a[19].value = allVals[i].supplier_id;
                    a[20].value = allVals[i].part_gp;
                    a[21].value = allVals[i].minimun_order;
                    a[22].value = allVals[i].ref_id;
                    // if (a[9].value < 1 && order_type == 1) {
                    //     newElement.attr('style', 'background-color: pink !important');
                    //     $('#items_error').removeAttr('style');
                    //     $('#items_error').text('The product don’t have enough stock quantity!');
                    // }
                    // if (allVals[i].original_price == "" || allVals[i].original_price == "None") {
                    //     a[7].value = "";
                    // } else a[7].value = allVals[i].original_price;
                    // if (a[7].value == "" || a[7].value == "None") {
                    //     a[10].value = 0;
                    //     $('#validate_error').text('Price of product must greater than 0 and not none');
                    //     $('#validate_error').removeAttr('style');
                    //     $('#btnSave').attr('disabled', true);
                    // } else a[10].value = a[5].value * a[7].value;
                    // a[11].value = 0;
                    // a[12].value = 0;
                    currentRow[0].textContent = a[0].value;
                    currentRow[1].textContent = a[1].value;
                    currentRow[2].textContent = a[2].value;
                    currentRow[3].textContent = a[11].value;
                    currentRow[4].textContent = a[13].value;
                    currentRow[5].textContent = a[14].value;
                    currentRow[6].textContent = a[15].value;
                    currentRow[7].textContent = a[17].value;
                    currentRow[8].textContent = a[18].value;
                    currentRow[9].textContent = a[20].value;
                    sum = parseInt($('#id_subtotal').val());
                    sum += parseInt(a[8].value);
                    $('#id_subtotal').val(sum);
                    if (isNaN(sum)) {
                        sum = 0;
                        $('#id_subtotal').val(0);
                        $('#id_total').val(0);
                    }
                    if ($('#id_tax_amount').val()) {
                        sum += parseFloat($('#id_tax_amount').val());
                    }
                    if ($('#id_discount').val()) {
                        sum -= parseFloat($('#id_discount').val());
                    }
                    $('#id_total').val(sum);
                }
                // newElement.find('label').each(function () {
                //     var newFor = $(this).attr('for').replace('-' + (total - 1) + '-', '-' + total + '-');
                //     $(this).attr('for', newFor);
                // });
                total++;
                $('#id_' + type + '-TOTAL_FORMS').val(total);
                $(selector).after(newElement);

            }
        }
    }


    $(document).on('click', "[class^=removerow]", function (event) {
        currentRow = $(this).closest('tr').find('input');
        item_id = currentRow[3].value;
        var order_type = $('#order_type').text();
        if ($('#id_formset_item-TOTAL_FORMS').val() == 1) {
            $(this).closest('tr').css('display', 'none');
            $(this).closest('tr').find('input').each(function () {
                $(this)[0].value = '';
            });
            $('#items_error').removeAttr('style');
            $('#id_subtotal').val(0);
            $('#id_total').val(0);
            $('#btnSave').attr('disabled', true);
            $('#btnPrint').attr('disabled', true);
            $('#btnSendForEdit').attr('disabled', true);
            $('#btnSend').attr('disabled', true);
        } else {
            $('#btnSave').removeAttr('disabled');
            $('#btnPrint').removeAttr('disabled');
            $('#btnSend').removeAttr('disabled');
            $('#btnSendForEdit').removeAttr('disabled');
            var sum = parseFloat($('#id_subtotal').val());
            sum -= parseFloat(currentRow[8].value);
            $('#id_subtotal').val(sum.toFixed(2));
            total = sum.toFixed(2) + parseFloat($('#id_tax_amount').val());
            $('#id_total').val(total);
            var minus = $('input[name=formset_item-TOTAL_FORMS]').val() - 1;
            $('#id_formset_item-TOTAL_FORMS').val(minus);
            $(this).parents("tr").remove();
        }
        var i = 0;
        $('#dynamic-table tr.gradeX').each(function () {
            currentLabel = $(this).closest('tr').find('label');
            var $tds = $(this).find(':input');
            var line_number = $tds[0].name;
            var part_no = $tds[1].name;
            var item = $tds[2].name;
            var item_id = $tds[3].name;
            var customer_po_no = $tds[4].name;
            var quantity = $tds[5].name;
            var price = $tds[6].name;
            var exchange_rate = $tds[7].name;
            var amount = $tds[8].name;
            var wanted_date = $tds[9].name;
            var schedule_date = $tds[10].name;
            var currency = $tds[11].name;
            var currency_id = $tds[12].name;
            var refer_line = $tds[13].name;
            var ref_num = $tds[14].name;
            var location_code = $tds[15].name;
            var location_id = $tds[16].name;
            var uom = $tds[17].name;
            var supplier_code = $tds[18].name;
            var supplier_code_id = $tds[19].name;
            var part_gp = $tds[20].name;
            var minimum = $tds[21].name;
            var ref_id = $tds[22].name;
            if (item.replace(/[^\d.]/g, '') == 0) {
                i++;
            } else {
                for (i; i < minus; i++) {
                    $tds[0].name = line_number.replace(/\d+/g, i);
                    $tds[0].id = 'id_' + $tds[0].name;
                    $tds[0].value = i + 1;
                    currentLabel[0].textContent = $tds[0].value;
                    $tds[1].name = part_no.replace(/\d+/g, i);
                    $tds[1].id = 'id_' + $tds[1].name;
                    $tds[2].name = item.replace(/\d+/g, i);
                    $tds[2].id = 'id_' + $tds[2].name;
                    $tds[3].name = item_id.replace(/\d+/g, i);
                    $tds[3].id = 'id_' + $tds[3].name;
                    $tds[4].name = customer_po_no.replace(/\d+/g, i);
                    $tds[4].id = 'id_' + $tds[4].name;
                    $tds[5].name = quantity.replace(/\d+/g, i);
                    $tds[5].id = 'id_' + $tds[5].name;
                    $tds[6].name = price.replace(/\d+/g, i);
                    $tds[6].id = 'id_' + $tds[6].name;
                    $tds[7].name = exchange_rate.replace(/\d+/g, i);
                    $tds[7].id = 'id_' + $tds[7].name;
                    $tds[8].name = amount.replace(/\d+/g, i);
                    $tds[8].id = 'id_' + $tds[8].name;
                    $tds[9].name = wanted_date.replace(/\d+/g, i);
                    $tds[9].id = 'id_' + $tds[9].name;
                    $tds[10].name = schedule_date.replace(/\d+/g, i);
                    $tds[10].id = 'id_' + $tds[10].name;
                    $tds[11].name = currency.replace(/\d+/g, i);
                    $tds[11].id = 'id_' + $tds[11].name;
                    $tds[12].name = currency_id.replace(/\d+/g, i);
                    $tds[12].id = 'id_' + $tds[12].name;
                    $tds[13].name = refer_line.replace(/\d+/g, i);
                    $tds[13].id = 'id_' + $tds[13].name;
                    $tds[14].name = ref_num.replace(/\d+/g, i);
                    $tds[14].id = 'id_' + $tds[14].name;
                    $tds[15].name = location_code.replace(/\d+/g, i);
                    $tds[15].id = 'id_' + $tds[15].name;
                    $tds[16].name = location_id.replace(/\d+/g, i);
                    $tds[16].id = 'id_' + $tds[16].name;
                    $tds[17].name = uom.replace(/\d+/g, i);
                    $tds[17].id = 'id_' + $tds[17].name;
                    $tds[18].name = supplier_code.replace(/\d+/g, i);
                    $tds[18].id = 'id_' + $tds[18].name;
                    $tds[19].name = supplier_code_id.replace(/\d+/g, i);
                    $tds[19].id = 'id_' + $tds[19].name;
                    $tds[20].name = part_gp.replace(/\d+/g, i);
                    $tds[20].id = 'id_' + $tds[20].name;
                    $tds[21].name = minimum.replace(/\d+/g, i);
                    $tds[21].id = 'id_' + $tds[21].name;
                    $tds[22].name = ref_id.replace(/\d+/g, i);
                    $tds[22].id = 'id_' + $tds[22].name;
                    i++;
                    break;
                }
            }
        });
    });
});

function changeCurrency(arrItems, currency_id, currency_name) {
    
    $.ajax({
        method: "POST",
        url: '/orders/load_currency/',
        dataType: 'JSON',
        data: {
            'csrfmiddlewaretoken': $('input[name="csrfmiddlewaretoken"]').val(),
            'arrItems': JSON.stringify(arrItems),
            'currency_id': currency_id,
        },
        success: function (json) {
            // console.log(json);
            var item_currency_not_match = [];
            var sale_price = 0;
            var amount = 0;
            var purchase_price = 0;
            for (var i in json) {
                if (json[i].constructor === Object) {
                    $('#dynamic-table tr.gradeX').each(function () {
                        currentRow = $(this).closest('tr').find('input');
                        currentLabel = $(this).closest('tr').find('label');
                        currentItem = currentRow[2].value;
                        if (currentRow[3].value == json[i].id) {

                            if (json[i].rate == 0 || json[i].sale_price == "") {
                                // item_currency_not_match.push({item: currentItem, currency: json[i].currency});
                                item_currency_not_match.push('Can not get Exchange Rate from ' + json[i].currency + ' to ' + currency_name);
                                currentRow[7].value = 0;
                                currentRow[8].value = 0;
                                $('.lblCurrency').text(json[i].currency);
                            } else {
                                sale_price = currentRow[5].value * currentRow[6].value;
                                currentRow[7].value = parseFloat(json[i].rate).toFixed(4);
                                amount = json[i].rate * sale_price;
                                currentRow[8].value = amount.toFixed(2);
                                $('.lblCurrency').text(json['symbol']);
                            }
                        }
                    });
                }
            }
            ;
            // console.log(item_currency_not_match);
            if (item_currency_not_match.length > 0) {
                $("#currency_error").text("");
                // for (i = 0; i < item_currency_not_match.length; i++) {
                //     document.getElementById('currency_error').innerHTML += 'Can not get Exchange Rate from ' +
                //         item_currency_not_match[i].currency + ' to ' + currency_name + '<br>';
                // }
                var uniqueNames = [];
                $.each(item_currency_not_match, function (i, el) {
                    if ($.inArray(el, uniqueNames) === -1) uniqueNames.push(el);
                });
                for (i = 0; i < uniqueNames.length; i++) {
                    document.getElementById('currency_error').innerHTML += uniqueNames[i] + '<br>';
                }
                $('#currency_error').removeAttr('style');
                $('#btnPrint').attr('disabled', true);
                $('#btnSave').attr('disabled', true);
                $('#btnSend').attr('disabled', true);
                $('#btnSendForEdit').attr('disabled', true);
                $('#dynamic-table tr.gradeX').each(function () {
                    $(this).closest('tr').find('input').attr('disabled', true);
                });
            } else {
                $('#currency_error').css('display', 'none');
                $('#btnPrint').removeAttr('disabled');
                $('#btnSave').removeAttr('disabled');
                $('#btnSend').removeAttr('disabled');
                $('#btnSendForEdit').removeAttr('disabled');
                $('#dynamic-table tr.gradeX').each(function () {
                    $(this).closest('tr').find('input').removeAttr('disabled');
                });
            }
            calculateTotal('#dynamic-table tr.gradeX');
        }
    });
};

// calculate subtotal and total
function calculateTotal(selector) {
    // currentRow = $(selector).closest('tr').find('input');
    // amount = currentRow[6].value * currentRow[10].value;
    // currentRow[11].value = amount.toFixed(4);
    var subtotal = 0;
    var total = 0;
    $('#dynamic-table tr.gradeX').each(function () {
        var $tds = $(this).find('input');
        purchase_price = $tds[5].value * $tds[6].value;
        amount = $tds[7].value * purchase_price;
        $tds[8].value = parseFloat(amount.toFixed(2));
        subtotal += parseFloat(amount);
    });
    $('#id_subtotal').val(subtotal.toFixed(2));
    if ($('#id_discount').val() == '' || $('#id_discount').val() == null) {
        total = subtotal + parseFloat($('#id_tax_amount').val());
    } else {
        total = subtotal + parseFloat($('#id_tax_amount').val()) - parseFloat($('#id_discount').val());
    }
    $('#id_total').val(total.toFixed(2));
}

function supplier_items() {
    // var data = $('#search_input').val();
    var supplier_id = $('#hdSupplierId').val();
    var exclude_item_array = [];
    var exclude_item_list = {};
    $('#dynamic-table tr.gradeX').each(function () {
        var display = $(this).css("display");
        currentRow = $(this).closest('tr').find('input');
        if (display != 'none') {
            exclude_item_array.push(currentRow[3].value);
        }
    });
    if (exclude_item_array.length > 0) {
        exclude_item_list = JSON.stringify(exclude_item_array);
    }

    // if (data.length == 0)
    //     data = '0';
    // $.ajax({
    //     method: "POST",
    //     url: '/orders/change_supplier_items/',
    //     data: {
    //         'csrfmiddlewaretoken': $('input[name="csrfmiddlewaretoken"]').val(),
    //         'supplier_id': supplier_id,
    //         // 'search_condition': data,
    //         'exclude_item_list': exclude_item_list
    //     },
    //     responseTime: 200,
    //     response: function (settings) {
    //         if (settings.data.value) {
    //             this.responseText = '{"success": true}';
    //         } else {
    //             this.responseText = '{"success": false, "msg": "required"}';
    //         }
    //     },
    //     success: function (data) {
    //         $("#myDialog").html('');
    //         $("#myDialog").html(data);
    //         $('#tblData').dataTable({
    //             "aaSorting": [[4, "desc"]],
    //             "bFilter": true,
    //             "bLengthChange": false,
    //             "iDisplayLength": 5,
    //         });
    //     }
    // });

    var datatbl = $('#tblData').DataTable();
    datatbl.destroy();
    var list_url = $('#list_url').text();
    $('#tblData').DataTable({
        "iDisplayLength": 5,
        "bLengthChange": false,
        "order": [[1, "desc"], [0, "desc"]],
        "serverSide": true,
        "ajax": {
            "url": list_url,
            "data": {
                "csrfmiddlewaretoken": $('input[name="csrfmiddlewaretoken"]').val(),
                "supplier_id": supplier_id,
                "exclude_item_list": exclude_item_list
            }
        },
        "rowId": "line_id",
        "columns": [
            {
                "data": "item_id",
                "className": "hide_column"
            },
            {"data": "item_name", "sClass": "text-left"},
            {"data": "supplier_code", "sClass": "text-left"},
            {"data": "refer_number", "sClass": "text-left"},
            {"data": "refer_line", "sClass": "text-left"},
            {"data": "location_code", "sClass": "text-left"},
            {"data": "part_no", "sClass": "text-left"},
            {"data": "part_gp", "sClass": "text-left"},
            {"data": "purchase_price", "sClass": "text-right"},
            {"data": "currency_code", "sClass": "text-left"},
            {
                "data": "location_id",
                "className": "hide_column"
            },
            {
                "data": "currency_id",
                "className": "hide_column"
            },
            {
                "data": "line_id",
                "className": "hide_column"
            },
            {
                "data": "unit",
                "className": "hide_column"
            },
            {
                "data": "supplier_id",
                "className": "hide_column"
            },
            {
                "data": "minimun_order",
                "className": "hide_column"
            },
            {
                "data": "refer_id",
                "className": "hide_column"
            },
            {
                "data": "customer_po_no",
                "className": "hide_column"
            },
            {
                "orderable": false,
                "data": null,
                "render": function (data, type, row, meta) {
                    return '<input type="checkbox" name="choices" id="' + row.line_id + '"'
                        + 'class="call-checkbox" value="' + row.purchase_price + '"></td>';
                }
            }
        ]
    });
}

// event change currency
$('#id_currency').change(function () {
    var currency_id = parseInt($(this).val());
    var currency_name = $('#id_currency option:selected').text();
    var arrItems = [];
    $('#dynamic-table tr.gradeX').each(function () {
        currentRow = $(this).closest('tr').find('input');
        arrItems.push({
            item_id: currentRow[2].value,
            currency_id: currentRow[13].value
        });
    });
    changeCurrency(arrItems, currency_id, currency_name);
});

//event handle calculate subtotal and total base on quantity
$('#dynamic-table tr.gradeX').each(function () {
    currentRow = $(this).closest('tr').find('input');
    currentColumn = $(this).closest('tr').find('td');
    $mainElement = '#' + currentRow[5].id;
    $($mainElement).change(function () {
        currentRow = $(this).closest('tr').find('input');
        var quantity = currentRow[5].value;
        var price = currentRow[6].value;
        if (quantity < parseInt(currentRow[21].value, 10)) {
            $('#minimum_order_error').removeAttr('style');
            $('#minimum_order_error').text('The quantity of product ' + currentRow[2].value + ' must greater than minimun order. The minimun order is: ' + currentRow[21].value);
            $(this).closest('tr').attr('style', 'background-color: yellow !important');
            currentRow[8].value = 0;
            $('#btnPrint').attr('disabled', true);
            $('#btnSave').attr('disabled', true);
            $('#btnSend').attr('disabled', true);
            $('#btnSendForEdit').attr('disabled', true);
            $('#dynamic-table tr.gradeX').each(function () {
                $(this).closest('tr').find('input').not(currentRow[5]).attr('disabled', true);
            });
        } else if (quantity < 1) {
            $('#minimum_order_error').removeAttr('style');
            $('#minimum_order_error').text('The quantity of product ' + currentRow[2].value + ' must greater than 0');
            $(this).closest('tr').attr('style', 'background-color: yellow !important');
            currentRow[8].value = 0;
            $('#btnPrint').attr('disabled', true);
            $('#btnSave').attr('disabled', true);
            $('#btnSend').attr('disabled', true);
            $('#btnSendForEdit').attr('disabled', true);
            $('#dynamic-table tr.gradeX').each(function () {
                $(this).closest('tr').find('input').not(currentRow[5]).attr('disabled', true);
            });
        } else {
            calculateTotal(this);
            $('#dynamic-table tr.gradeX').each(function () {
                $(this).closest('tr').find('input').removeAttr('disabled');
            });
            $('#minimum_order_error').css('display', 'none');
            $(this).closest('tr').removeAttr('style');
            $('#btnPrint').removeAttr('disabled');
            $('#btnSave').removeAttr('disabled');
            $('#btnSend').removeAttr('disabled');
            $('#btnSendForEdit').removeAttr('disabled');
        }
        ;
    });
});

// event change exchange rate
$('#dynamic-table tr.gradeX').find('input').each(function () {
    currentRow = $(this).closest('tr').find('input');
    $exchangeRate = '#' + currentRow[8].id;
    $($exchangeRate).change(function () {
        currentRow = $(this).closest('tr').find('input');
        var exchange_rate = currentRow[8].value;
        if (exchange_rate <= 0) {
            $('#minimum_order_error').removeAttr('style');
            $('#minimum_order_error').text('The exchange rate of product ' + currentRow[1].value + ' must greater than 0');
            $(this).closest('tr').attr('style', 'background-color: red !important');
            currentRow[9].value = 0;
            $('#btnPrint').attr('disabled', true);
            $('#btnSave').attr('disabled', true);
            $('#btnSend').attr('disabled', true);
            $('#btnSendForEdit').attr('disabled', true);
            $('#dynamic-table tr.gradeX').each(function () {
                $(this).closest('tr').find('input').not(currentRow[8]).attr('disabled', true);
            });
        } else {
            $('#dynamic-table tr.gradeX').each(function () {
                $(this).closest('tr').find('input').removeAttr('disabled');
            });
            $('#minimum_order_error').css('display', 'none');
            $(this).closest('tr').removeAttr('style');
            $('#btnPrint').removeAttr('disabled');
            $('#btnSave').removeAttr('disabled');
            $('#btnSend').removeAttr('disabled');
            $('#btnSendForEdit').removeAttr('disabled');
        }
        calculateTotal(this);
    });
});


$(document).ready(function () {
    var order_id = $('#order_id').text();
    if (order_id != "") {
        var currency_id = $('#id_currency').val();
        var currency_name = $('#id_currency option:selected').text();
        var arrItems = [];
        $('#dynamic-table tr.gradeX').each(function () {
            currentRow = $(this).closest('tr').find('input');
            arrItems.push({
                item_id: currentRow[3].value,
                currency_id: currentRow[12].value
            });
            $("input[name*='wanted_date']").datepicker({
                format: 'yyyy-mm-dd',
                autoclose: true
            });
            $("input[name*='schedule_date']").datepicker({
                format: 'yyyy-mm-dd',
                autoclose: true
            });
        });
        // changeCurrency(arrItems, currency_id, currency_name);
    }
    // $("#search_input").keypress(function (e) {
    //     var key = e.which;
    //     if (key == 13) {
    //         supplier_items();
    //     }
    // });
    $('#btnSearchItem').on('click', function () {
        supplier_items();
    });
    $('#btnOpenItemDialog').on('click', function () {
        var dataTable = $('#tblData').dataTable();
        dataTable.fnClearTable(this);
        supplier_items();
    });
});

//Load tax rate
$(document).ready(function () {
    $('#id_tax').change(function () {
        var taxid = parseInt($(this).val());
        if (isNaN(taxid)) {
            $('#id_tax_amount').val(0);
            $('#id_total').val(parseFloat($('#id_tax_amount').val()) + parseFloat($('#id_subtotal').val()));
        } else {
            $.ajax({
                method: "POST",
                url: '/orders/load_tax/',
                dataType: 'JSON',
                data: {
                    'csrfmiddlewaretoken': $('input[name="csrfmiddlewaretoken"]').val(),
                    'tax_id': taxid,
                },
                success: function (json) {
                    var tax_amount = (parseFloat(json) * parseFloat($('#id_subtotal').val())) / 100;
                    $('#id_tax_amount').val(tax_amount.toFixed(2));
                    var total = parseFloat($('#id_subtotal').val()) + tax_amount;
                    $('#id_total').val(total.toFixed(2));
                }
            });
        }
    });
});

//event handle for input discount
$('#id_discount').change(function () {
    if ($(this).val() != '') {
        var sum = parseFloat($('#id_subtotal').val()) + parseFloat($('#id_tax_amount').val());
        sum -= parseInt(this.value);
        $('#id_total').val(sum);
    } else {
        var sum = 0;
        $('#dynamic-table tr.gradeX').each(function () {
            var $tds = $(this).find('input');
            amount = $tds[9].value;
            sum += parseInt(amount).toFixed(2);
            $('#id_subtotal').val(sum);
            var total = sum + parseFloat($('#id_tax_amount').val())
            $('#id_total').val(total);
        })
    }
});

// event change price
$('#dynamic-table tr.gradeX').find('input').each(function () {
    currentRow = $(this).closest('tr').find('input');
    // currentItem = $(this).closest('tr').find('option:selected');
    $priceElement = '#' + currentRow[6].id;
    $($priceElement).change(function () {
        currentRow = $(this).closest('tr').find('input');
        currentRow[8].value = Number(currentRow[5].value * currentRow[6].value * currentRow[7].value).toFixed(2);
        if (currentRow[6].value > 0) {
            $('#validate_error').css('display', 'none');
            // var subtotal = 0;
            // $('#dynamic-table tr.gradeX').each(function () {
            //     var $tds = $(this).find('input');
            //     amount = $tds[9].value;
            //     subtotal += parseFloat(amount);
            //     $('#id_subtotal').val(subtotal.toFixed(2));
            //     if (parseFloat($('#id_tax_amount').val()) != 0) {
            //         var total = (subtotal * parseFloat($('#id_tax_amount').val())) / 100 - parseFloat($('#id_discount').val())
            //         $('#id_total').val(total.toFixed(2));
            //     } else {
            //         var total = subtotal - parseFloat($('#id_discount').val())
            //         $('#id_total').val(total.toFixed(2));
            //     }
            // });
            currentRow.parents('tr').removeAttr('style');
            $('#dynamic-table tr.gradeX').each(function () {
                $(this).closest('tr').find('input').removeAttr('disabled');
            });
            $('#validate_error').css('display', 'none');
            $('#btnPrint').removeAttr('disabled');
            $('#btnSave').removeAttr('disabled');
            $('#btnSend').removeAttr('disabled');
            $('#btnSendForEdit').removeAttr('disabled');
            // validationItemsFormset();
        } else {
            $('#validate_error').text('Price of product must greater than 0 and not none');
            $('#validate_error').removeAttr('style');
            currentRow.parents('tr').attr('style', 'background-color: red !important');
            $('#id_subtotal').val(0);
            $('#id_total').val(0);
            $('#btnPrint').attr('disabled', true);
            $('#btnSave').attr('disabled', true);
            $('#btnSend').attr('disabled', true);
            $('#btnSendForEdit').attr('disabled', true);
            $('#dynamic-table tr.gradeX').each(function () {
                $(this).closest('tr').find('input').not(currentRow[6]).attr('disabled', true);
            });
        }
        calculateTotal(this);
    });
});

//change amount event
$('#dynamic-table tr.gradeX').each(function () {
    currentRow = $(this).closest('tr').find('input');
    $amountElement = '#' + currentRow[8].id;
    $($amountElement).change(function () {
        $tds = $(this).closest('tr').find('input');
        var subtotal = 0;
        var total = 0;
        if ($(this)[0].value < 0) {
            $(this).closest('tr').attr('style', 'background-color: red !important');
            $('#items_error').text('The product ' + $tds[2].value + ' must have amount greater than 0');
            $('#items_error').removeAttr('style');
            $('#btnPrint').attr('disabled', true);
            $('#btnSave').attr('disabled', true);
            $('#btnSend').attr('disabled', true);
            $('#btnSendForEdit').attr('disabled', true);
            $('#dynamic-table tr.gradeX').each(function () {
                $(this).closest('tr').find('input').not($tds[8]).attr('disabled', true);
            });
            $('#id_subtotal').val(0);
            $('#id_total').val(0);
        } else {
            $('#items_error').css('display', 'none');
            $(this).closest('tr').removeAttr('style');
            $('#dynamic-table tr.gradeX').each(function () {
                var $tds = $(this).find('input');
                amount = $tds[8].value;
                subtotal += parseFloat(amount);
                $('#id_subtotal').val(subtotal.toFixed(2));
                discount_val = $('#id_discount').val()
                if (discount_val == '' || discount_val == null) {
                    total = subtotal + parseFloat($('#id_tax_amount').val());
                } else {
                    total = subtotal + parseFloat($('#id_tax_amount').val()) - parseFloat(discount_val);
                }
                $('#id_total').val(total.toFixed(2));
                $(this).closest('tr').find('input').removeAttr('disabled');
            });
            $('#btnPrint').removeAttr('disabled');
            $('#btnSave').removeAttr('disabled');
            $('#btnSend').removeAttr('disabled');
            $('#btnSendForEdit').removeAttr('disabled');
        }
    });
});
