/* Search Supplier button*/
$('#btnSearchSupplier').on('click', function () {
    $("#supplier_error").text(""); // Delete error message
    var datatbl = $('#supplier-table').DataTable();
    datatbl.destroy();
    datatbl.clear().draw();
    $('#supplier-table').dataTable({
        "iDisplayLength": 5,
        "bLengthChange": false,
        "order": [[0, "desc"]],
        "serverSide": true,
        "ajax": {
            "url": "/accounting/supplier_list/"
        },
        "columns": [
            {"data": "code", "sClass": "text-left"},
            {"data": "name", "sClass": "text-left"},
            {"data": "term_days", "sClass": "text-left"},
            {"data": "payment_mode", "sClass": "text-left"},
            {"data": "credit_limit", "sClass": "text-left hide_column"},
            {"data": "id", "sClass": "text-left hide_column",},
            {"data": "tax_id", "sClass": "text-left hide_column",},
            {"data": "currency_id", "sClass": "text-left hide_column",},
            {"data": "currency_code", "sClass": "text-left hide_column"},
            {
                "orderable": false,
                "data": null,
                "render": function (data, type, full, meta) {
                    return '<input type="radio" name="supplier-choices" id="' +
                        full.id + '" class="call-checkbox" value="' + meta.row + '">';
                }
            }
        ]
    });
});

function changeSupllier() {
    var row = $("input[name='supplier-choices']:checked").val();
    if (row) {
        var table = $('#supplier-table').DataTable();

        // Supllier
        var supplier_id = table.cell(row, $("#sup-id").index()).data();
        var id_supplier_code = table.cell(row, $("#sup-code").index()).data();
        var id_supplier_name = table.cell(row, $("#sup-name").index()).data();
        $("#id_supplier").val(supplier_id);
        $("#id_supplier_code").val(id_supplier_code);
        $("#id_supplier_name").val(id_supplier_name);

        // Currency
        var id_currency = table.cell(row, $("#sup-currency").index()).data();
        var id_currency_code = table.cell(row, $("#sup-currency_code").index()).data();
        $("#id_currency").val(id_currency);
        $("#id_currency_code").val(id_currency_code);

        // Document Type
        $("#id_document_number").val("");
        $("#id_document_amount").val("");

        // Tax
        var new_tax = table.cell(row, $("#sup-tax").index()).data();
        var old_tax = $("#id_tax").val();
        if (new_tax != old_tax) {
            $.ajax({
                method: "POST",
                url: '/orders/load_tax/',
                dataType: 'JSON',
                data: {
                    'csrfmiddlewaretoken': $('input[name="csrfmiddlewaretoken"]').val(),
                    'tax_id': new_tax,
                },
                success: function (json) {
                    var tax_amount = (parseFloat(json) * parseFloat($('#id_amount').val())) / 100;
                    $('#id_tax_amount').val(tax_amount.toFixed(2));
                    var total = parseFloat($('#id_amount').val()) + tax_amount;
                    $('#id_total_amount').val(total.toFixed(2));
                    // recalculate tax amount in Transaction Table
                    var rate = parseFloat(json) / 100
                    recalculateTaxAmount(rate)
                    $("#id_tax").val(new_tax);
                }
            });
        }

        $("#SupplierListModal").modal("hide");
    }
    else {
        $("#supplier_error").text("Please choose 1 supplier");
    }
}
/* End Search Supplier button*/


/* Search Account button*/
$('#btnSearchAccount').on('click', function () {
    var datatbl = $('#account-table').DataTable();
    datatbl.destroy();
    datatbl.clear().draw();
    $('#account-table').dataTable({
        "iDisplayLength": 5,
        "bLengthChange": false,
        "order": [[0, "desc"]],
        "serverSide": true,
        "ajax": {
            "url": "/accounting/account_list/"
        },
        "columns": [
            {"data": "code", "sClass": "text-left"},
            {"data": "name", "sClass": "text-left"},
            {"data": "account_type", "sClass": "text-left"},
            {"data": "balance_type", "sClass": "text-left"},
            {"data": "account_group", "sClass": "text-left"},
            {"data": "id", "sClass": "text-left hide_column"},
            {
                "orderable": false,
                "data": null,
                "render": function (data, type, full, meta) {
                    return '<input type="radio" name="account-choices" id="' +
                        full.id + '" class="call-checkbox" value="' + meta.row + '">';
                }
            }
        ]
    });
});

function changeAccount() {
    var row = $("input[name='account-choices']:checked").val();
    if (row) {
        var table = $('#account-table').DataTable();
        var id_account = table.cell(row, $("#acc-id").index()).data();
        var id_account_code = table.cell(row, $("#acc-code").index()).data();
        $("#id_account_set").val(id_account);
        $("#id_account_code").val(id_account_code);

        $("#AccountListModal").modal("hide");
    }
    else {
        $("#account_error").text("Please choose 1 account");
    }
}
/* End Search Account button*/

/* Search Document button*/
$('#btnSearchDocument').on('click', function () {
    var datatbl = $('#document-table').DataTable();
    datatbl.destroy();
    datatbl.clear().draw();
    $('#document-table').dataTable({
        "iDisplayLength": 5,
        "iDisplayStart": 0,
        "bLengthChange": false,
        "order": [[0, "desc"]],
        "serverSide": true,
        "stateSave": false,
        "ajax": {
            "url": "/accounting/supplier_document_list/",
            "data": {
                "supplier_id": $("#id_supplier").val()
            }
        },
        "columns": [
            {"data": "code", "sClass": "text-left"},
            {"data": "date", "sClass": "text-left"},
            {"data": "reference", "sClass": "text-left"},
            {"data": "amount", "sClass": "text-left"},
            {"data": "supplier_name", "sClass": "text-left"},
            {
                "orderable": false,
                "data": null,
                "render": function (data, type, full, meta) {
                    return '<input type="radio" name="document-choices" id="' +
                        full.id + '" class="call-checkbox" value="' + meta.row + '">';
                }
            },
            {"data": "id", "sClass": "text-left hidden"}
        ]
    });
});

function changeDocument() {
    $("#document_error").text(""); // Delete error message
    var row = $("input[name='document-choices']:checked").val();
    if (row) {
        var table = $('#document-table').DataTable();
        var id_document_number = table.cell(row, $("#doc-number").index()).data();
        var id_document_amount = table.cell(row, $("#doc-total_amount").index()).data();
        $("#id_document_number").val(id_document_number);
        $("#id_document_amount").val(id_document_amount);

        $("#modalSearchDocument").modal("hide");
    }
    else {
        $("#account_error").text("Please choose 1 document");
    }
}
/* End Search Document button*/

// Seach on focus out
$('#id_supplier_code').on('blur', function () {
    var supplier_code = $('#id_supplier_code').val()
    if (supplier_code == '') {
        alert("Supplier Code can not be blank !!!");
        $('#id_supplier').val('');
        $('#id_supplier_code').val('');
        $('#id_supplier_name').val('');
        $('#id_currency').val('');
        $('#id_currency_code').val('');
    }
    else {
        $.ajax({
            method: "POST",
            url: '/accounting/search_supplier/',
            dataType: 'JSON',
            data: {
                'csrfmiddlewaretoken': $('input[name="csrfmiddlewaretoken"]').val(),
                'supplier_code': supplier_code,
            },
            success: function (json) {
                $('#id_supplier').val(json.supplier_id);
                $('#id_supplier_code').val(json.supplier_code);
                $('#id_supplier_name').val(json.supplier_name);
                $('#id_tax').find('option').removeAttr("selected");
                $('#id_tax').find('option').removeAttr("disabled");
                $('#id_tax').find('option[value="' + json.tax_id + '"]').attr("selected", "selected");
                $('#id_tax').val(json.tax_id);
                // $('#id_tax option:not(:selected)').attr('disabled', true);
                $("#id_document_number").val('');
                $("#id_document_amount").val('0.00');
                $("#id_document_amount").trigger('change');
                // load tax again
                var taxid = parseInt($('#id_tax').val());
                if (isNaN(taxid)) {
                    $('#id_tax_amount').val(0);
                    $('#id_total_amount').val(0);
                    $('#id_total_amount').trigger('change');
                } else {
                    $.ajax({
                        method: "POST",
                        url: '/orders/load_tax/',
                        dataType: 'JSON',
                        data: {
                            'csrfmiddlewaretoken': $('input[name="csrfmiddlewaretoken"]').val(),
                            'tax_id': taxid,
                        },
                        success: function (json) {
                            var rate = parseFloat(json) / 100
                            var tax_amount = (parseFloat(json) * parseFloat($('#id_amount').val())) / 100;
                            $('#id_tax_amount').val(tax_amount.toFixed(2));

                            var total = parseFloat($('#id_amount').val()) + tax_amount;
                            $('#id_total_amount').val(total.toFixed(2));
                            $('#id_total_amount').trigger('change');

                            // recalculate tax amount in Transaction Table
                            recalculateTaxAmount(rate);
                        }
                    });
                }
                $('#id_currency').val(json.currency_id);
                $('#id_currency_code').val(json.currency_code)
            },
            error: function () {
                alert(supplier_code + " is not a valid Supplier Code. Please search again !!!");
                $('#id_supplier').val('');
                $('#id_supplier_code').val('');
                $('#id_supplier_name').val('');
                $('#id_currency').val('');
                $('#id_currency_code').val('');
            }
        });
    }
});

var test = [
    {
        value: "duong minh tam",
        id: "hoang"
    },
    {
        value: "duong minh tien",
        id:"hoang2"
    }
    ];

$('#id_account_code').autocomplete({
    source: test,
    response: function(event, ui){
        if(ui.content.length === 0){
            max_length = $(this).val().length - 1;
            $(this).attr("maxlength", 1);
        }
        else{
            $(this).attr("maxlength", '');
        }
    }
});

$('#id_account_code123').on('blur', function () {
    var account_code = $('#account_set').val();
    if (account_code == '') {
        alert("Account Set can not be blank !!!");
    }
    else {
        $.ajax({
            method: "POST",
            url: '/accounting/search_accountset/',
            dataType: 'JSON',
            data: {
                'csrfmiddlewaretoken': $('input[name="csrfmiddlewaretoken"]').val(),
                'account_code': account_code,
            },
            success: function (json) {
                $('#id_account_code').val(json.account_code);
                $('#id_account_set').val(json.account_set_id);
            },
            error: function () {
                alert(account_code + " is not a valid Account Set. Please search again !!!");
                $('#id_account_code').val('');
                $('#id_account_set').val("");
            }
        });
    }
});

$('#id_document_number').on('blur', function () {
    var document_number = $('#id_document_number').val();
    var customer_id = $('#id_customer').val();
    if (document_number == '' || customer_id == '') {

    } else {
        $.ajax({
            method: "POST",
            url: '/accounting/search_document/',
            dataType: 'JSON',
            data: {
                'csrfmiddlewaretoken': $('input[name="csrfmiddlewaretoken"]').val(),
                'document_number': document_number,
                'customer_id': customer_id,
            },
            success: function (json) {
                $('#id_document_number').val(json.document_number);
                $('#id_document_amount').val(json.document_amount);
            },
            error: function () {
                alert(document_number + " is not a valid DOcu. Please search again !!!");
                $('#id_document_number').val('');
                $('#id_document_amount').val('');
            }
        });
    }
});

$('#id_tax').on('change', function () {
    var taxid = parseInt($('#id_tax').val());
    if (isNaN(taxid)) {
        $('#id_tax_amount').val(0);
        $('#id_total_amount').val(0);
        $('#id_total_amount').trigger('change');
    } else {
        $.ajax({
            method: "POST",
            url: '/orders/load_tax/',
            dataType: 'JSON',
            data: {
                'csrfmiddlewaretoken': $('input[name="csrfmiddlewaretoken"]').val(),
                'tax_id': taxid,
            },
            success: function (json) {
                var rate = parseFloat(json) / 100
                var tax_amount = (parseFloat(json) * parseFloat($('#id_amount').val())) / 100;
                $('#id_tax_amount').val(tax_amount.toFixed(2));

                var total = parseFloat($('#id_amount').val()) + tax_amount;
                $('#id_total_amount').val(total.toFixed(2));
                $('#id_total_amount').trigger('change');

                // recalculate tax amount in Transaction Table
                recalculateTaxAmount(rate)
            }
        });
    }
});


$(document).ready(function () {
    var document_amount = $('#id_document_amount').val();
    var total_dmount = $('#id_total_amount').val();
    var undistributed_amount = parseFloat(document_amount) - parseFloat(total_dmount);
    $('#undistributed_amount').val(undistributed_amount.toFixed(2));

});

$('#id_document_amount').on("change", function () {
    var document_amount = $('#id_document_amount').val();
    var total_dmount = $('#id_total_amount').val();
    var undistributed_amount = parseFloat(document_amount) - parseFloat(total_dmount);
    $('#undistributed_amount').val(undistributed_amount.toFixed(2));
});

$('#id_total_amount').on("change", function () {
    var document_amount = $('#id_document_amount').val();
    var total_dmount = $('#id_total_amount').val();
    var undistributed_amount = parseFloat(document_amount) - parseFloat(total_dmount);
    $('#undistributed_amount').val(undistributed_amount.toFixed(2));
});

// Add transaction list data when submit form
function getTransactionTableData() {
    array = [];
    var table = $('#transaction-table').DataTable();
    table.rows().every(function (rowIdx, tableLoop, rowLoop) {
        rowData = this.data();
        transaction_list = {};
        transaction_list.id = rowData[$("#trs-id").index()];
        transaction_list.distribution_id = rowData[$("#trs-distribution_id").index()];
        transaction_list.distribution_code = rowData[$("#trs-distribution_code").index()];
        transaction_list.distribution_name = rowData[$("#trs-distribution_name").index()];
        transaction_list.account_id = rowData[$("#trs-account_id").index()];
        transaction_list.account_code = rowData[$("#trs-account_code").index()];
        transaction_list.account_name = rowData[$("#trs-account_name").index()];
        transaction_list.description = rowData[$("#trs-description").index()];
        transaction_list.amount = rowData[$("#trs-amount").index()];
        transaction_list.tax_amount = rowData[$("#trs-tax_amount").index()];
        transaction_list.total_amount = rowData[$("#trs-total_amount").index()];
        transaction_list.delete = rowData[$("#trs-delete").index()];
        array.push(transaction_list);
    });

    $('#transaction_list_data').val(JSON.stringify(array));
}

// function to re-calculate tax amoount and total amount for transaction table
function recalculateTaxAmount(rate) {
    var table = $('#transaction-table').DataTable();
    table.rows().every(function (rowIdx, tableLoop, rowLoop) {
        rowData = this.data();
        amount = parseFloat(rowData[$("#trs-amount").index()]);
        tax_mount = parseFloat(amount * rate);
        total_amount = amount + tax_mount;
        table.cell(rowIdx, $("#trs-tax_amount").index()).data(tax_mount.toFixed(2));
        table.cell(rowIdx, $("#trs-total_amount").index()).data(total_amount.toFixed(2));
    });
    table.draw();
}
