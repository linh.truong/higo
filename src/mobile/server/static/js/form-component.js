var Script = function () {


    //checkbox and radio btn

    var d = document;
    var safari = (navigator.userAgent.toLowerCase().indexOf('safari') != -1) ? true : false;
    var gebtn = function(parEl,child) { return parEl.getElementsByTagName(child); };
    onload = function() {

        var body = gebtn(d,'body')[0];
        body.className = body.className && body.className != '' ? body.className + ' has-js' : 'has-js';

        if (!d.getElementById || !d.createTextNode) return;
        var ls = gebtn(d,'label');
        for (var i = 0; i < ls.length; i++) {
            var l = ls[i];
            if (l.className.indexOf('label_') == -1) continue;
            var inp = gebtn(l,'input')[0];
            if (l.className == 'label_check') {
                l.className = (safari && inp.checked == true || inp.checked) ? 'label_check c_on' : 'label_check c_off';
                l.onclick = check_it;
            };
            if (l.className == 'label_radio') {
                l.className = (safari && inp.checked == true || inp.checked) ? 'label_radio r_on' : 'label_radio r_off';
                l.onclick = turn_radio;
            };
        };
    };
    var check_it = function() {
        var inp = gebtn(this,'input')[0];
        if (this.className == 'label_check c_off' || (!safari && inp.checked)) {
            this.className = 'label_check c_on';
            if (safari) inp.click();
        } else {
            this.className = 'label_check c_off';
            if (safari) inp.click();
        };
    };
    var turn_radio = function() {
        var inp = gebtn(this,'input')[0];
        if (this.className == 'label_radio r_off' || inp.checked) {
            var ls = gebtn(this.parentNode,'label');
            for (var i = 0; i < ls.length; i++) {
                var l = ls[i];
                if (l.className.indexOf('label_radio') == -1)  continue;
                l.className = 'label_radio r_off';
            };
            this.className = 'label_radio r_on';
            if (safari) inp.click();
        } else {
            this.className = 'label_radio r_off';
            if (safari) inp.click();
        };
    };

}();

// common setting to uppercase every input type text
var lenOldVal = '';
$('input[type=text]').on('change paste keyup', function() {
    //var i = $(this);
   // if(lenOldVal != i.val().length) $(this).val(function() { return this.value.toUpperCase()})
   // lenOldVal = i.val().length;
});

// document.forms.obrazac.onkeypress = function(key) {
//     alert("key pressed");
//     if(key.keyCode == 13) {
//         var evnt = key || window.event, 
//         target = evnt.target || evnt.srcElement,
//         nextElm = target.nextSibling;
        
//         while(nextElm.tagName != 'input' && nextElm.nextSibling) {
//             nextElm = nextElm.nextSibling;
//         }
//         nextElm.focus();

//         if(nextELm != this.elements[this.elements.length] - 1) {
//             return false;
//         }
//     }
// }

// move focus to the input when the enter key is pressed
$('body').on('keydown', 'input:not([type=submit], .item_code_search, #form_customer_code, #form_supplier_code, #txtFilter, #id_code, #txtPartNo, #txtPONo, #txtDONo, #txtSONo, #txtGRNo), select, textarea', function(e) {
    var elm = $(this), form = elm.parents('form:eq(0)'), targetFocusElm, nextELm;
    if(e.keyCode == 13) {
        console.log($(this)[0].nodeName);
        var v = this.value;
        var curPos = getCaret(this);
        if(e.shiftKey && $(this)[0].nodeName == 'TEXTAREA') {    
            var newStr = v.substring(0, curPos) + "\n" + v.substring(curPos, v.length);
            this.value = newStr;
            e.preventDefault();

            return false;
        }

        targetFocusElm = form.find('input, select, textarea').filter(':visible');
        nextElm = targetFocusElm.eq(targetFocusElm.index(this)+1);
        nextElm.length ? nextElm.focus() : null;
        return false;
    }

});


function getCaret(el) { 
    if (el.selectionStart) { 
        return el.selectionStart; 
    } else if (document.selection) { 
        el.focus();
        var r = document.selection.createRange(); 
        if (r == null) { 
            return 0;
        }
        var re = el.createTextRange(), rc = re.duplicate();
        re.moveToBookmark(r.getBookmark());
        rc.setEndPoint('EndToStart', re);
        return rc.text.length;
    }  
    return 0; 
}
// $("input:not([type=submit]), select").keypress(function(e) {
//     if(e.keyCode ==  13) {
//         console.log($(this));
//     }
// })