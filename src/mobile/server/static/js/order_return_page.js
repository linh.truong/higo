// var oTable = $('#items-table').dataTable({
//     "aLengthMenu": [
//         [5, 15, 20, -1],
//         [5, 15, 20, "All"] // change per page values here
//     ],
//     // set the initial value
//     "bFilter": false,
//     "bLengthChange": false,
//     "iDisplayLength": 5,
//     "bPaginate": false,
//     "bInfo": false
// });
$('#items-table tr.gradeX').each(function () {
    currentRow = $(this).closest('tr').find('input');
    currentLabel = $(this).closest('tr').find('label');
    $mainElement = '#' + currentRow[0].id;
    $($mainElement).change(function () {
        var quantity = currentRow[0].value;
        var old_quantity = currentRow[1].value;
        var amount = 0;
        var subtotal = 0;
        if (parseInt(quantity) <= parseInt(old_quantity)) {
            calculateTotal($(this));
            $('#items_error').css("display", "none");
            $('#btnSave').removeAttr('disabled');
        } else {
            $('#items_error')[0].innerText = "Input Quantity is greater than in order. Please input smaller number!"
            $('#items_error').css("display", "block");
            $('#btnSave').attr('disabled', 'disabled');
        }

    });
    // $('#items-table tr.gradeX').closest('tr').find('input').keypress(function () {
    //
    // });
});

// calculate subtotal and total
function calculateTotal(selector) {
    currentRow = $(selector).closest('tr').find('input');
    var amount = 0;
    amount = currentRow[0].value * currentRow[2].value;
    currentRow[3].value = amount.toFixed(2);
    currentLabel[2].textContent = amount.toFixed(2);
    var subtotal = 0;
    $('#items-table tr.gradeX').each(function () {
        var $tds = $(this).find('input');
        amount = $tds[3].value;
        subtotal += parseFloat(amount);
        $('#id_subtotal').val(Number(subtotal).toFixed(2));
        var total = subtotal + parseFloat($('#id_tax_amount').val()) - parseFloat($('#id_discount').val())
        $('#id_total').val(total.toFixed(2));
    });
}

$(".item-quantity").change(function () {
    var nRow = $(this).parents('tr')[0];
    var arrInput = $('input', nRow);

    var item_quantity = this.value;
    var old_quantity = arrInput[2].value;
    var item_price = arrInput[3].value;
    var display = $(this).parents('tr').css("display");
    //compare with old quantity
    if (parseFloat(item_quantity) > parseFloat(old_quantity)) {
        $('#items_error')[0].innerText = "Input Quantity is greater than in order. Please input smaller number!"
        $('#items_error').css("display", "block");
        $('#btnSave').attr('disabled', 'disabled');
        $('#btnGenerate').attr('disabled', 'disabled');
        if (display == 'none') {
            $(this).parents('tr').attr("style", "background-color: pink!important; display: none;");
        }
        else {
            $(this).parents('tr').attr("style", "background-color: pink!important; ");
        }
    }
    else {
        $('#items_error')[0].innerText = ""
        $('#items_error').css("display", "none");
        $('#btnSave').removeAttr('disabled');
        $('#btnGenerate').removeAttr('disabled');
        if (display == 'none') {
            $(this).parents('tr').attr("style", "background-color: white!important; display: none;");
        }
        else {
            $(this).parents('tr').attr("style", "background-color: white!important;");
        }
    }
    arrInput[4].value = (parseFloat(item_quantity) * parseFloat(item_price)).toFixed(2);
    //set value of id_subtotal
    calculateTotalAmount();

});
$(document).on('click', "[name^=remove_item]", function (event) {
    $(this).parents('tr').attr("style", "background-color: white!important; display: none;");
    currentRow = $(this).closest('tr').find('input');
    currentRow[4].value = 'False';
    calculateTotalAmount();
});

// $('#remove_item').click(function () {
//     $(this).parents('tr').attr("style", "background-color: white!important; display: none;");
//     currentRow = $(this).closest('tr').find('input');
//     // var nRow = $(this).parents('tr')[0];
//     // var arrInput = $('input', nRow);
//     // arrInput[1].value = arrInput[2].value;
//     // arrInput[4].value = (parseFloat(arrInput[1].value) * parseFloat(arrInput[3].value)).toFixed(2);
//     // $('#' + arrInput[5].id).attr("value", "False");
//     // arrInput[6].value = 'False';
//     calculateTotalAmount();
// });
$('#add_item').click(function () {
    //$('#items-table tr.gradeX').removeAttr("style");
    $('#items-table tr.gradeX').css("display", "");
    arrRow = $('#items-table tr.gradeX');
    for (x = 0; x < arrRow.length; x++) {
        var arrInput = $('input', arrRow[x]);
        arrInput[4].value = 'True';
    }
    ;
    calculateTotalAmount();
});
$('#btnCopySupplier').click(function () {
    $('#id_name')[0].value = $('#supplier_name')[0].innerText;
    $('#id_address')[0].value = $('#supplier_address')[0].innerText;
    $('#id_email')[0].value = $('#supplier_email')[0].innerText;
    $('#id_phone')[0].value = $('#hdSupplierPhone')[0].value;
    $('#id_fax')[0].value = $('#hdSupplierFax')[0].value;
});
$('#btnCleanSupplier').click(function () {
    $('#id_name')[0].value = "";
    $('#id_address')[0].value = "";
    $('#id_email')[0].value = "";
    $('#id_phone')[0].value = "";
    $('#id_fax')[0].value = "";
});
$('#btnGenerate').click(function () {
    $('#hd_is_generate')[0].value = "1";
});
function calculateTotalAmount() {
    // var arrAmount = $(".item-amount:visible");
    var total_amount = 0;
    $('#items-table tr.gradeX').each(function () {
        var display = $(this).css("display");
        currentRow = $(this).closest('tr').find('input');
        if (display != 'none') {
            total_amount += parseFloat(currentRow[3].value);
            $('#id_subtotal')[0].value = total_amount.toFixed(2);
            $('#id_total')[0].value = total_amount.toFixed(2);
        }
    });
}
$(document).ready(function () {
    $('.item-select option:not(:selected)').prop('disabled', true);
    $('.item-quantity').trigger('change');
    calculateTotalAmount();
});

$(document).ready(function () {
    checkDisplay();
    $('#add_more_right').click(function () {
        cloneMore('div.table-right:last', 'formset_right');
    });
    $('#add_more_left').click(function () {
        cloneMore('div.table-left:last', 'formset_left');
    });
    $('#add_more_code').click(function () {
        cloneMore('div.table-code:last', 'formset_code');
    });
    function cloneMore(selector, type) {
        var display = $(selector).css("display")
        if (display == 'none') {
            $(selector).removeAttr("style")
        }
        else {
            var total = $('#id_' + type + '-TOTAL_FORMS').val();
            var newElement = $(selector).clone(true);
            newElement.removeAttr("style")
            newElement.find(':input').each(function () {
                var name = $(this).attr('name').replace('-' + (total - 1) + '-', '-' + total + '-');
                var id = 'id_' + name;
                $(this).attr({'name': name, 'id': id}).val('').removeAttr('checked');
            });
            newElement.find('label').each(function () {
                var newFor = $(this).attr('for').replace('-' + (total - 1) + '-', '-' + total + '-');
                $(this).attr('for', newFor);
            });
            total++;
            $('#id_' + type + '-TOTAL_FORMS').val(total);
            $(selector).after(newElement);
        }
    }

    function checkDisplay() {
        var request_method = $('#request_method').val();
        if (request_method == 'GET') {
            if ($('#id_formset_right-TOTAL_FORMS').val() > 1) {
                $('div.table-right:last').remove();
                $('#id_formset_right-TOTAL_FORMS').val($('#id_formset_right-TOTAL_FORMS').val() - 1);
            } else {
                $('div.table-right:last').css("display", "none");
            }
            if ($('#id_formset_left-TOTAL_FORMS').val() > 1) {
                $('div.table-left:last').remove();
                $('#id_formset_left-TOTAL_FORMS').val($('#id_formset_left-TOTAL_FORMS').val() - 1);
            } else {
                $('div.table-left:last').css("display", "none");
            }
            if ($('#id_formset_code-TOTAL_FORMS').val() > 1) {
                $('div.table-code:last').remove();
                $('#id_formset_code-TOTAL_FORMS').val($('#id_formset_code-TOTAL_FORMS').val() - 1);
            } else {
                $('div.table-code:last').css("display", "none");
            }
            $('#dynamic-table tr.gradeX:last').css("display", "none");
            $('#items_error').css("display", "none");
        } else if (request_method == 'POST') {
            var right_label = $('#id_formset_right-0-label').val();
            var right_somevalue = $('#id_formset_right-0-value').val();
            if (right_label == "" && right_somevalue == "") {
                $('div.table-right:last').css("display", "none");
            }
            var left_label = $('#id_formset_left-0-label').val();
            var left_somevalue = $('#id_formset_left-0-value').val();
            if (left_label == "" && left_somevalue == "") {
                $('div.table-left:last').css("display", "none");
            }
            var code_label = $('#id_formset_code-0-label').val();
            var code_somevalue = $('#id_formset_code-0-value').val();
            if (code_label == "" && code_somevalue == "") {
                $('div.table-code:last').css("display", "none");
            }
            var item = $('#id_formset_item-0-item').val();
            var quantity = $('#id_formset_item-0-quantity').val();
            var price = $('#id_formset_item-0-price').val();
            var amount = $('#id_formset_item-0-amount').val();
            if (quantity == "" && price == "" && amount == "") {
                $('#dynamic-table tr.gradeX:last').css("display", "none");
                $('#items_error').removeAttr('style');
            } else $('#items_error').css("display", "none");
        }
    }

    $(document).on('click', "[class^=removerow-left]", function () {
        if ($('#id_formset_left-TOTAL_FORMS').val() == 1) {
            var total = $('#id_formset_left-TOTAL_FORMS').val();
            $('div.table-left:last').find(':input').each(function () {
                var name = $(this).attr('name').replace('-' + total + '-', '-' + (total - 1) + '-');
                var id = 'id_' + name;
                $(this).attr({'name': name, 'id': id}).val('').removeAttr('value');
            });
            $(this).parents("div.table-left:last").css("display", "none")
        } else {
            var minus = $('input[name=formset_left-TOTAL_FORMS]').val() - 1;
            $('#id_formset_left-TOTAL_FORMS').val(minus);
            $(this).parents("div.table-left").remove();
            var i = 0;
            $('div.table-left').each(function () {
                var $tds = $(this).find('input');
                var label = $tds[0].name;
                var value = $tds[1].name;
                if (label.replace(/[^\d.]/g, '') == 0) {
                    i++;
                } else {
                    for (i; i < minus; i++) {
                        $tds[0].name = label.replace(/\d+/g, i);
                        $tds[0].id = 'id_' + $tds[0].name;
                        $tds[1].name = value.replace(/\d+/g, i);
                        $tds[1].id = 'id_' + $tds[1].name;
                        i++;
                        break;
                    }
                }
            });
        }
    });

    $(document).on('click', "[class^=removerow-right]", function () {
        if ($('#id_formset_right-TOTAL_FORMS').val() == 1) {
            var total = $('#id_formset_right-TOTAL_FORMS').val();
            $('div.table-right:last').find(':input').each(function () {
                var name = $(this).attr('name').replace('-' + total + '-', '-' + (total - 1) + '-');
                var id = 'id_' + name;
                $(this).attr({'name': name, 'id': id}).val('').removeAttr('value');
            });
            $(this).parents("div.table-right").css("display", "none");
        } else {
            var minus = $('input[name=formset_right-TOTAL_FORMS]').val() - 1;
            $('#id_formset_right-TOTAL_FORMS').val(minus);
            $(this).parents("div.table-right").remove();
            var i = 0;
            $('div.table-right').each(function () {
                var $tds = $(this).find('input');
                var label = $tds[0].name;
                var value = $tds[1].name;
                if (label.replace(/[^\d.]/g, '') == 0) {
                    i++;
                } else {
                    for (i; i < minus; i++) {
                        $tds[0].name = label.replace(/\d+/g, i);
                        $tds[0].id = 'id_' + $tds[0].name;
                        $tds[1].name = value.replace(/\d+/g, i);
                        $tds[1].id = 'id_' + $tds[1].name;
                        i++;
                        break;
                    }
                }
            });
        }
    });

    $(document).on('click', "[class^=removerow-code]", function () {
        if ($('#id_formset_code-TOTAL_FORMS').val() == 1) {
            var total = $('#id_formset_code-TOTAL_FORMS').val();
            $('div.table-code:last').find(':input').each(function () {
                var name = $(this).attr('name').replace('-' + total + '-', '-' + (total - 1) + '-');
                var id = 'id_' + name;
                $(this).attr({'name': name, 'id': id}).val('').removeAttr('value');
            });
            $(this).parents("div.table-code").css("display", "none")
        } else {
            var minus = $('input[name=formset_code-TOTAL_FORMS]').val() - 1;
            $('#id_formset_code-TOTAL_FORMS').val(minus);
            $(this).parents("div.table-code").remove();
            var i = 0;
            $('div.table-code').each(function () {
                var $tds = $(this).find('input');
                var label = $tds[0].name;
                var value = $tds[1].name;
                if (label.replace(/[^\d.]/g, '') == 0) {
                    i++;
                } else {
                    for (i; i < minus; i++) {
                        $tds[0].name = label.replace(/\d+/g, i);
                        $tds[0].id = 'id_' + $tds[0].name;
                        $tds[1].name = value.replace(/\d+/g, i);
                        $tds[1].id = 'id_' + $tds[1].name;
                        i++;
                        break;
                    }
                }
            });
        }
    });
});

//Load and edit inline Customer information
$(document).ready(function () {
    var hdCustomerId = $('#hdCustomerId').val();

    loadCustomerInfo(hdCustomerId);

    function loadCustomerInfo(hdCustomerId) {
        $.fn.editable.defaults.mode = 'inline';

        //make status editable
        $('#customer_name').editable({
            type: 'text',
            pk: hdCustomerId,
            url: '/orders/change_customer/' + hdCustomerId + '/',
            title: 'Enter customer name',
            success: function (response, newValue) {
                if (!response) {
                    return "Unknown error!"
                }
                if (response.success === false) {
                    return respond.msg;
                }
            }
        });

        $('#customer_address').editable({
            type: 'text',
            pk: hdCustomerId,
            url: '/orders/change_customer/' + hdCustomerId + '/',
            title: 'Enter customer address',
            success: function (response, newValue) {
                if (!response) {
                    return "Unknown error!"
                }
                if (response.success === false) {
                    return respond.msg;
                }
            }
        });

        $('#customer_email').editable({
            type: 'text',
            pk: hdCustomerId,
            url: '/orders/change_customer/' + hdCustomerId + '/',
            title: 'Enter customer email',
            validate: function (value) {
                var valid = valib.String.isEmailLike(value)
                if (valid == false) return 'Please insert valid email'
            },
            success: function (response, newValue) {
                if (!response) {
                    return "Unknown error!"
                }
                if (response.success === false) {
                    return respond.msg;
                }
            }
        });
    };

    $('#id_customer').change(function () {
        var customerid = parseInt($(this).val());

        $.ajax({
            method: "POST",
            url: '/orders/customer/',
            dataType: 'JSON',
            data: {
                'csrfmiddlewaretoken': $('input[name="csrfmiddlewaretoken"]').val(),
                'customer_id': customerid,
            },
            responseTime: 200,
            response: function (settings) {
                if (settings.data.value) {
                    this.responseText = '{"success": true}';
                } else {
                    this.responseText = '{"success": false, "msg": "required"}';
                }
            },
            success: function (json) {
                console.log(json);
                var list_address = [];
                for (var i in json) {
                    if (json[i].constructor === Array) {
                        list_address.push({
                            id: json[i][0],
                            name: json[i][1],
                            code: json[i][2],
                            phone: json[i][3],
                            fax: json[i][4],
                            email: json[i][5],
                            attention: json[i][6],
                            address: json[i][7],
                            note_1: json[i][8],
                            note_2: json[i][9]
                        });
                    }
                }
                ;
                var list_customer_address = document.getElementById("id_customer_address");
                $('#id_customer_address').find('option').remove();
                for (var i in list_address) {
                    list_customer_address.add(new Option(list_address[i].name, list_address[i].id));
                }
                if (list_address == "") {
                    $('#id_customer_address').text("");
                    $('#id_name').val("");
                    $('#id_code').val("");
                    $('#id_phone').val("");
                    $('#id_fax').val("");
                    $('#id_email').val("");
                    $('#id_attention').val("");
                    $('#id_address').val("");
                    $('#id_note_1').val("");
                    $('#id_note_2').val("");
                }
                else {
                    $('#id_name').val(list_address[0].name);
                    $('#id_code').val(list_address[0].code);
                    $('#id_phone').val(list_address[0].phone);
                    $('#id_fax').val(list_address[0].fax);
                    $('#id_email').val(list_address[0].email);
                    $('#id_attention').val(list_address[0].attention);
                    $('#id_address').val(list_address[0].address);
                    $('#id_note_1').val(list_address[0].note_1);
                    $('#id_note_2').val(list_address[0].note_2);
                }
                $('#hdCustomerId').val(json['id']);

                $('#customer_name').editable('destroy');
                $('#customer_address').editable('destroy');
                $('#customer_email').editable('destroy');

                $('#customer_name').attr('data-pk', json['id']);
                $('#customer_name').text(json['name']);
                $('#customer_address').text(json['address']);
                // $('#customer_address').attr('data-pk', list_address[0].id);
                // $('#customer_address').text(list_address[0].address);
                // $('#customer_address').text(list_address[0].address);

                $('#customer_email').attr('data-pk', json['id']);
                $('#customer_email').text(json['email']);
                // var address_id = $('#customerAddrId').val(list_address[0].id);
                loadCustomerInfo(json['id']);
            }
        });
    });
});

//Company Information
$(document).ready(function () {

    //toggle `popup` / `inline` mode
    $.fn.editable.defaults.mode = 'inline';
    //Get company id
    var company_id = $('#company_id').val();
    //make status editable
    $('#companyname').editable({
        type: 'text',
        pk: company_id,
        url: '/orders/change_company/' + company_id + '/',
        title: 'Enter company name',
        success: function (response, newValue) {

            if (!response) {
                return "Unknown error!"
            }
            if (response.success === false) {
                return respond.msg;
            }
        }
    });

    $('#address').editable({
        type: 'text',
        pk: company_id,
        url: '/orders/change_company/' + company_id + '/',
        title: 'Enter company address',
        success: function (response, newValue) {

            if (!response) {
                return "Unknown error!"
            }
            if (response.success === false) {
                return respond.msg;
            }
        }
    });

    $('#email').editable({
        type: 'text',
        pk: company_id,
        url: '/orders/change_company/' + company_id + '/',
        title: 'Enter company email',
        validate: function (value) {
            var valid = valib.String.isEmailLike(value)
            if (valid == false) return 'Please insert valid email'
        },
        success: function (response, newValue) {

            if (!response) {
                return "Unknown error!"
            }
            if (response.success === false) {
                return respond.msg;
            }
        }
    });

});

//Supplier Information
$(document).ready(function (hdSupplierId) {
    var hdSupplierId = $('#hdSupplierId').val();
    loadSupplierInfo(hdSupplierId);

    function loadSupplierInfo(hdSupplierId) {
        $.fn.editable.defaults.mode = 'inline';

        //make status editable
        $('#supplier_name').editable({
            type: 'text',
            pk: hdSupplierId,
            url: '/orders/change_supplier/' + hdSupplierId + '/',
            title: 'Enter supplier name',
            success: function (response, newValue) {
                if (!response) {
                    return "Unknown error!"
                }
                if (response.success === false) {
                    return respond.msg;
                }
            }
        });

        $('#supplier_address').editable({
            type: 'text',
            pk: hdSupplierId,
            url: '/orders/change_supplier/' + hdSupplierId + '/',
            title: 'Enter supplier address',
            success: function (response, newValue) {
                if (!response) {
                    return "Unknown error!"
                }
                if (response.success === false) {
                    return respond.msg;
                }
            }
        });

        $('#supplier_email').editable({
            type: 'text',
            pk: hdSupplierId,
            url: '/orders/change_supplier/' + hdSupplierId + '/',
            title: 'Enter supplier email',
            validate: function (value) {
                var valid = valib.String.isEmailLike(value)
                if (valid == false) return 'Please insert valid email'
            },
            success: function (response, newValue) {
                if (!response) {
                    return "Unknown error!"
                }
                if (response.success === false) {
                    return respond.msg;
                }
            }
        });
    };

});