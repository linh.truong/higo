//Pagination and search item
$(document).ready(function () {
    // $('#supplier-table').dataTable({
    //     "iDisplayLength": 5,
    //     "bLengthChange": false,
    //     "order": [[ 0, "desc" ]],
    //     "serverSide": true,
    //     "ajax": {
    //         "url": "{% url 'suppliers_list_as_json' %}"
    //     },
    //     "columns": [
    //         {"data": "code"},
    //         {"data": "name"},
    //         {"data": "term_days"},
    //         {"data": "peyment_mode"},
    //         {"data": "credit_limit"},
    //         {
    //             "orderable": false,
    //             "data": null,
    //             "render": function (data, type, full, meta) {
    //                 return '<input type="radio" name="choices" id="' +
    //                     full.id +'" class="call-checkbox" value="' + full.id + '">';
    //             }
    //         }
    //     ]
    // });
    debugger;
    var count_row = 0;
    $('#dynamic-table tr.gradeX').each(function () {
        currentRow = $(this).closest('tr').find('input');
        currentLabel = $(this).closest('tr').find('label');
        count_row += 1;
        currentRow[0].value = count_row;
        currentLabel[0].textContent = currentRow[0].value;
        currentLabel[0].textContent = currentRow[0].value;
    });
});

$(document).keypress(function (e) {
    if (e.which == 13 && !$(event.target).is("textarea")) {
        e.preventDefault();
    }
});


//Add Extra Label Value formset
$(document).ready(function () {
    checkDisplay();
    calculateTotal();
    var status_id = $('#status_id').text();
    var selector = $('#dynamic-table tr.gradeX');
    var supplier = $('#hdSupplierId').val();
    if (supplier != null) {
        $('#btnSave').removeAttr('disabled');
        $('#btnSend').removeAttr('disabled');
        $('#btnPrint').removeAttr('disabled');
        $('#btnSendForEdit').removeAttr('disabled');
        if (status_id == '0') {
            $('#btnOpenItemDialog').css('display', 'none');
            var items_name_list = [];
            selector.each(function () {
                currentRow = $(this).closest('tr').find('input');
                currentLabel = $(this).closest('tr').find('label');
                var quantity = currentRow[5].value;
                var quantity_receive = parseInt(currentRow[5].value) + parseInt(currentLabel[10].textContent);
                if (parseInt(quantity) < 0) {
                    $('#minimum_order_error').removeAttr('style');
                    $('#minimum_order_error').text('The quantity of product must greater than 0');
                    $(this).closest('tr').attr('style', 'background-color: red !important');
                    currentRow[8].value = 0;
                    $('#btnSave').attr('disabled', true);
                    $('#btnSend').attr('disabled', true);
                    $('#btnPrint').attr('disabled', true);
                    $('#btnSendForEdit').attr('disabled', true);
                }
                else if (parseInt(quantity) == 0) {
                    items_name_list.push(currentRow[2].value);
                    $(this).closest('tr').attr('style', 'background-color: aqua !important');
                    $('#' + currentRow[5].id).attr('disabled', true);
                }
            });
            var uniqueNames = [];
            $.each(items_name_list, function (i, el) {
                if ($.inArray(el, uniqueNames) === -1) uniqueNames.push(el);
            });
            if (uniqueNames.length > 0) {
                $('#minimum_order_error').removeAttr('style');
                $('#minimum_order_error').text('The products ' + uniqueNames + ' were received.');
                currentRow[8].value = 0;
                $('#btnSave').attr('disabled', true);
                $('#btnSend').attr('disabled', true);
                $('#btnPrint').attr('disabled', true);
                $('#btnSendForEdit').attr('disabled', true);
            }
        }
    } else {
        $('#btnSave').attr('disabled', true);
        $('#btnSend').attr('disabled', true);
        $('#btnPrint').attr('disabled', true);
        $('#btnSendForEdit').attr('disabled', true);
    }

    $('#add_more_right').click(function () {
        cloneMore('div.table-right:last', 'formset_right');
    });
    $('#add_more_left').click(function () {
        cloneMore('div.table-left:last', 'formset_left');
    });
    $('#add_more_code').click(function () {
        cloneMore('div.table-code:last', 'formset_code');
    });
    function cloneMore(selector, type) {
        var display = $(selector).css("display")
        if (display == 'none') {
            $(selector).removeAttr("style")
        }
        else {
            var total = $('#id_' + type + '-TOTAL_FORMS').val();
            var newElement = $(selector).clone(true);
            newElement.removeAttr("style")
            newElement.find(':input').each(function () {
                var name = $(this).attr('name').replace('-' + (total - 1) + '-', '-' + total + '-');
                var id = 'id_' + name;
                $(this).attr({'name': name, 'id': id}).val('').removeAttr('checked');
            });
            newElement.find('label').each(function () {
                var newFor = $(this).attr('for').replace('-' + (total - 1) + '-', '-' + total + '-');
                $(this).attr('for', newFor);
            });
            total++;
            $('#id_' + type + '-TOTAL_FORMS').val(total);
            $(selector).after(newElement);
        }
    }

    function checkDisplay() {
        var request_method = $('#request_method').val();
        if (request_method == 'GET') {
            if ($('#id_formset_right-TOTAL_FORMS').val() > 1) {
                $('div.table-right:last').remove();
                $('#id_formset_right-TOTAL_FORMS').val($('#id_formset_right-TOTAL_FORMS').val() - 1);
            } else {
                $('div.table-right:last').css("display", "none");
            }
            if ($('#id_formset_left-TOTAL_FORMS').val() > 1) {
                $('div.table-left:last').remove();
                $('#id_formset_left-TOTAL_FORMS').val($('#id_formset_left-TOTAL_FORMS').val() - 1);
            } else {
                $('div.table-left:last').css("display", "none");
            }
            if ($('#id_formset_code-TOTAL_FORMS').val() > 1) {
                $('div.table-code:last').remove();
                $('#id_formset_code-TOTAL_FORMS').val($('#id_formset_code-TOTAL_FORMS').val() - 1);
            } else {
                $('div.table-code:last').css("display", "none");
            }
            $('#dynamic-table tr.gradeX:last').css("display", "none");
            $('#items_error').css("display", "none");
        } else if (request_method == 'POST') {
            var right_label = $('#id_formset_right-0-label').val();
            var right_somevalue = $('#id_formset_right-0-value').val();
            if (right_label == "" && right_somevalue == "") {
                $('div.table-right:last').css("display", "none");
            }
            var left_label = $('#id_formset_left-0-label').val();
            var left_somevalue = $('#id_formset_left-0-value').val();
            if (left_label == "" && left_somevalue == "") {
                $('div.table-left:last').css("display", "none");
            }
            var code_label = $('#id_formset_code-0-label').val();
            var code_somevalue = $('#id_formset_code-0-value').val();
            if (code_label == "" && code_somevalue == "") {
                $('div.table-code:last').css("display", "none");
            }
            var item = $('#id_formset_item-0-item').val();
            var quantity = $('#id_formset_item-0-quantity').val();
            var price = $('#id_formset_item-0-price').val();
            var amount = $('#id_formset_item-0-amount').val();
            if (quantity == "" && price == "" && amount == "") {
                $('#dynamic-table tr.gradeX:last').css("display", "none");
                $('#items_error').removeAttr('style');
            } else $('#items_error').css("display", "none");
        }
    }

    $(document).on('click', "[class^=removerow-left]", function () {
        if ($('#id_formset_left-TOTAL_FORMS').val() == 1) {
            var total = $('#id_formset_left-TOTAL_FORMS').val();
            $('div.table-left:last').find(':input').each(function () {
                var name = $(this).attr('name').replace('-' + total + '-', '-' + (total - 1) + '-');
                var id = 'id_' + name;
                $(this).attr({'name': name, 'id': id}).val('').removeAttr('value');
            });
            $(this).parents("div.table-left:last").css("display", "none")
        } else {
            var minus = $('input[name=formset_left-TOTAL_FORMS]').val() - 1;
            $('#id_formset_left-TOTAL_FORMS').val(minus);
            $(this).parents("div.table-left").remove();
            var i = 0;
            $('div.table-left').each(function () {
                var $tds = $(this).find('input');
                var label = $tds[0].name;
                var value = $tds[1].name;
                if (label.replace(/[^\d.]/g, '') == 0) {
                    i++;
                } else {
                    for (i; i < minus; i++) {
                        $tds[0].name = label.replace(/\d+/g, i);
                        $tds[0].id = 'id_' + $tds[0].name;
                        $tds[1].name = value.replace(/\d+/g, i);
                        $tds[1].id = 'id_' + $tds[1].name;
                        i++;
                        break;
                    }
                }
            });
        }
    });

    $(document).on('click', "[class^=removerow-right]", function () {
        if ($('#id_formset_right-TOTAL_FORMS').val() == 1) {
            var total = $('#id_formset_right-TOTAL_FORMS').val();
            $('div.table-right:last').find(':input').each(function () {
                var name = $(this).attr('name').replace('-' + total + '-', '-' + (total - 1) + '-');
                var id = 'id_' + name;
                $(this).attr({'name': name, 'id': id}).val('').removeAttr('value');
            });
            $(this).parents("div.table-right").css("display", "none");
        } else {
            var minus = $('input[name=formset_right-TOTAL_FORMS]').val() - 1;
            $('#id_formset_right-TOTAL_FORMS').val(minus);
            $(this).parents("div.table-right").remove();
            var i = 0;
            $('div.table-right').each(function () {
                var $tds = $(this).find('input');
                var label = $tds[0].name;
                var value = $tds[1].name;
                if (label.replace(/[^\d.]/g, '') == 0) {
                    i++;
                } else {
                    for (i; i < minus; i++) {
                        $tds[0].name = label.replace(/\d+/g, i);
                        $tds[0].id = 'id_' + $tds[0].name;
                        $tds[1].name = value.replace(/\d+/g, i);
                        $tds[1].id = 'id_' + $tds[1].name;
                        i++;
                        break;
                    }
                }
            });
        }
    });

    $(document).on('click', "[class^=removerow-code]", function () {
        if ($('#id_formset_code-TOTAL_FORMS').val() == 1) {
            var total = $('#id_formset_code-TOTAL_FORMS').val();
            $('div.table-code:last').find(':input').each(function () {
                var name = $(this).attr('name').replace('-' + total + '-', '-' + (total - 1) + '-');
                var id = 'id_' + name;
                $(this).attr({'name': name, 'id': id}).val('').removeAttr('value');
            });
            $(this).parents("div.table-code").css("display", "none")
        } else {
            var minus = $('input[name=formset_code-TOTAL_FORMS]').val() - 1;
            $('#id_formset_code-TOTAL_FORMS').val(minus);
            $(this).parents("div.table-code").remove();
            var i = 0;
            $('div.table-code').each(function () {
                var $tds = $(this).find('input');
                var label = $tds[0].name;
                var value = $tds[1].name;
                if (label.replace(/[^\d.]/g, '') == 0) {
                    i++;
                } else {
                    for (i; i < minus; i++) {
                        $tds[0].name = label.replace(/\d+/g, i);
                        $tds[0].id = 'id_' + $tds[0].name;
                        $tds[1].name = value.replace(/\d+/g, i);
                        $tds[1].id = 'id_' + $tds[1].name;
                        i++;
                        break;
                    }
                }
            });
        }
    });
});


//Load tax rate
$(document).ready(function () {
    $('#id_tax').change(function () {
        var taxid = parseInt($(this).val());
        if (isNaN(taxid)) {
            $('#id_tax_amount').val(0);
            $('#id_total').val(parseFloat($('#id_tax_amount').val()) - parseFloat($('#id_discount').val()) + parseFloat($('#id_subtotal').val()));
        } else {
            $.ajax({
                method: "POST",
                url: '/orders/load_tax/',
                dataType: 'JSON',
                data: {
                    'csrfmiddlewaretoken': $('input[name="csrfmiddlewaretoken"]').val(),
                    'tax_id': taxid,
                },
                success: function (json) {
                    var tax_amount = (parseFloat(json) * parseFloat($('#id_subtotal').val())) / 100;
                    $('#id_tax_amount').val(tax_amount.toFixed(2));
                    var total = parseFloat($('#id_subtotal').val()) + tax_amount - parseFloat($('#id_discount').val());
                    $('#id_total').val(total.toFixed(2));
                }
            });
        }
    });
});

//Event check checkbox
$('input[type=checkbox]').click(function () {
    if ($(this).is(':checked'))
        $(this).attr('checked', 'checked');
    else
        $(this).removeAttr('checked');
});
//Add order item
$(document).ready(function () {
    if ($('#id_formset_item-TOTAL_FORMS').val() > 1) {
        var display = $('#dynamic-table tr.gradeX:last').css("display");
        if (display == 'none') {
            $('#dynamic-table tr.gradeX:last').removeAttr("style");
            $('#dynamic-table tr.gradeX:last').remove();
            $('#id_formset_item-TOTAL_FORMS').val($('#id_formset_item-TOTAL_FORMS').val() - 1);
        }
    }

    //return false;
    $('#btnAddItems').on('click', function () {
        debugger;
        var allVals = [];
        var table = $('#tblData').DataTable();
        var rowcollection = table.$(".call-checkbox:checked", {"page": "all"});
        rowcollection.each(function (index, elem) {
            var row = table.row('#' + elem.id).data();
            allVals.push({
                id: row.item_id,
                price: $(elem).val(),
                item_code: row.part_no,
                name: row.item_name,
                refer_number: row.refer_number,
                refer_line: row.refer_line,
                supplier_code: row.supplier_code,
                location_code: row.location_code,
                part_gp: row.part_gp,
                unit_price: row.purchase_price,
                currency: row.currency_code,
                location_id: row.location_id,
                currency_id: row.currency_id,
                uom: row.unit,
                supplier_id: row.supplier_id,
                minimun_order: row.minimun_order,
                ref_id: row.refer_id,
                customer_po_no: row.customer_po_no,
                quantity: row.order_qty,
                receive_quantity: row.receive_qty
            });
            table.row('#' + elem.id).node().setAttribute("style", "display: none");
        });
        if (allVals.length > 0) {
            cloneMore('#dynamic-table tr.gradeX:last', 'formset_item', allVals);
            $('input[checked=checked]').each(function () {
                $(this).removeAttr('checked');
            });
        }
        $(this).attr('data-dismiss', 'modal');
        $('#items_error').css('display', 'none');
        //Change currency
        var currency_id = parseInt($('#id_currency option:selected').val());
        var currency_name = $('#id_currency option:selected').text();
        var arrItems = [];
        $('#dynamic-table tr.gradeX').each(function () {
            currentRow = $(this).closest('tr').find('input');
            arrItems.push({
                item_id: currentRow[3].value,
                currency_id: currentRow[10].value
            });
        });
        changeCurrency(arrItems, currency_id, currency_name);
        // validationItemsFormset();
        rowcollection.each(function (index, elem) {
            table.row('#' + elem.id).remove()
        });
    });

    // insert orderitem by PO num
    function addOrderItemByPO(po_number) {
        var exclude_item_array = [];
        var exclude_item_list = {};
        var hdSupplierId = $('#hdSupplierId').val();
        $('#dynamic-table tr.gradeX').each(function () {
            var display = $(this).css("display");
            currentRow = $(this).closest('tr').find('input');
            if (display != 'none') {
                exclude_item_array.push(currentRow[3].value);
            }
        });
        if (exclude_item_array.length > 0) {
            exclude_item_list = JSON.stringify(exclude_item_array);
        }
        $.ajax({
            method: "POST",
            url: '/orders/get_orderitems_by_po_no/',
            dataType: 'JSON',
            data: {
                'csrfmiddlewaretoken': $('input[name="csrfmiddlewaretoken"]').val(),
                'po_number': po_number,
                'supplier_id': hdSupplierId,
                'exclude_item_list': exclude_item_list
            },
            success: function (json) {
                console.log(json);
                var allVals = [];
                $.each(json, function (i, item) {
                    allVals.push({
                        id: item.item_id,
                        price: item.purchase_price,
                        item_code: item.part_no,
                        name: item.item_name,
                        refer_number: item.refer_number,
                        refer_line: item.refer_line,
                        supplier_code: item.supplier_code,
                        location_code: item.location_code,
                        part_gp: item.part_gp,
                        unit_price: item.purchase_price,
                        currency: item.currency_code,
                        location_id: item.location_id,
                        currency_id: item.currency_id,
                        uom: item.uom,
                        supplier_id: item.supplier_id,
                        minimun_order: item.minimun_order,
                        ref_id: item.refer_id,
                        customer_po_no: item.customer_po_no,
                        quantity: item.quantity,
                        receive_quantity: item.receive_quantity
                    });
                });
                cloneMore('#dynamic-table tr.gradeX:last', 'formset_item', allVals);
                //Change currency
                var currency_id = parseInt($('#id_currency option:selected').val());
                var currency_name = $('#id_currency option:selected').text();
                var arrItems = [];
                $('#dynamic-table tr.gradeX').each(function () {
                    currentRow = $(this).closest('tr').find('input');
                    arrItems.push({
                        item_id: currentRow[3].value,
                        currency_id: currentRow[10].value
                    });
                });
                changeCurrency(arrItems, currency_id, currency_name);
                $('#btnSave').removeAttr('disabled');
                $('#btnPrint').removeAttr('disabled');
                $('#btnSend').removeAttr('disabled');
                $('#btnSendForEdit').removeAttr('disabled');
                // set customer code
                $('#form_supplier_code').val(json[0]['supplier_code']);
                console.log(json[0]['supplier_code']);
                // change customer according to the sales order
                var e = jQuery.Event("keypress");
                e.which = 13;
                $("#form_supplier_code").trigger(e);
            }
        });
    }

    $('#txtPONo').on('keypress', function (e) {
        if (e.which == 13) {
            addOrderItemByPO($('#txtPONo').val());
            $('#items_error').css('display', 'none');
        }
    });

    function cloneMore(selector, type, allVals) {
        debugger;
        var display = $(selector).css("display");
        var order_type = $('#order_type').text();
        var sum = 0;
        var i = 0;
        var item_id = 0;
        if (display == 'none') {
            //show first row of table and set Item, Price of dialog
            $(selector).removeAttr("style")
            // $(selector).find("option").each(function () {
            //     $(this).removeAttr('selected');
            //     if ($(this).val() == allVals[i].id) {
            //         $(this).prop("selected", true);
            //         // $(this).attr("selected", true);
            //     }
            // });
            $(selector).find('label').each(function () {
                var name = $(this).attr('name').replace('-' + (total - 1) + '-', '-' + total + '-');
                var id = 'id_' + name;
                $(this).attr({'name': name, 'id': id}).val('').removeAttr('checked');
            });
            findInput = $(selector).find('input');
            // currentItem = $(selector).closest('tr').find('option:selected');
            currentLabel = $(selector).closest('tr').find('label');
            //add value to Input
            findInput[0].value = 1;
            findInput[1].value = allVals[i].item_code;
            findInput[2].value = allVals[i].name;
            findInput[3].value = allVals[i].id;
            findInput[4].value = allVals[i].customer_po_no;
            // if (allVals[i].minimun_order == '' || allVals[i].minimun_order == 'None') {
            //     findInput[5].value = 1;
            // } else findInput[5].value = allVals[i].minimun_order;
            findInput[5].value = allVals[i].quantity - allVals[i].receive_quantity;
            findInput[6].value = parseFloat(allVals[i].unit_price).toFixed(2);
            findInput[9].value = allVals[i].currency;
            findInput[10].value = allVals[i].currency_id;
            findInput[11].value = allVals[i].part_gp;
            findInput[12].value = allVals[i].supplier_code;
            findInput[13].value = allVals[i].supplier_id;
            findInput[14].value = allVals[i].location_code;
            findInput[15].value = allVals[i].location_id;
            findInput[16].value = allVals[i].refer_number;
            findInput[17].value = allVals[i].refer_line;
            findInput[18].value = allVals[i].uom;
            findInput[19].value = allVals[i].ref_id;
            findInput[20].value = allVals[i].minimun_order;

            currentLabel[0].textContent = findInput[0].value; // Line Number
            currentLabel[1].textContent = findInput[1].value; // Item Code
            currentLabel[2].textContent = findInput[2].value; // Item Name
            currentLabel[3].textContent = findInput[6].value; // Price
            currentLabel[5].textContent = findInput[9].value; // Currency Code
            currentLabel[6].textContent = findInput[11].value; // Part Group
            currentLabel[7].textContent = findInput[12].value; // Supplier Code
            currentLabel[8].textContent = findInput[14].value; // Location Code
            currentLabel[9].textContent = findInput[16].value; // Refer Number
            currentLabel[10].textContent = findInput[17].value; // Refer Line
            currentLabel[11].textContent = allVals[i].quantity; // Order Quantity
            currentLabel[12].textContent = allVals[i].receive_quantity; // Receive Quantity
            currentLabel[13].textContent = findInput[18].value; // UOM

            sum += parseInt(findInput[8].value);
            $('#id_subtotal').val(sum);
            if (isNaN(sum)) {
                sum = 0;
                $('#id_subtotal').val(0);
                $('#id_total').val(0);
            }
            if ($('#id_tax_amount').val()) {
                sum += parseFloat($('#id_tax_amount').val());
            }
            if ($('#id_discount').val()) {
                sum -= parseFloat($('#id_discount').val());
            }
            $('#id_total').val(sum);
            //if selected items > 1
            i = 1;
        }
        $('#btnSave').removeAttr('disabled');
        for (i; i < allVals.length; i++) {

            if (allVals[i].id != 0) {
                var newElement = $(selector).clone(true);
                var total = $('#id_' + type + '-TOTAL_FORMS').val();
                newElement.removeAttr("style")
                newElement.find(':input').each(function () {
                    var name = $(this).attr('name').replace('-' + (total - 1) + '-', '-' + total + '-');
                    var id = 'id_' + name;
                    $(this).attr({'name': name, 'id': id}).val('').removeAttr('checked');
                });
                newElement.find('label').each(function () {
                    var name = $(this).attr('name').replace('-' + (total - 1) + '-', '-' + total + '-');
                    var id = 'id_' + name;
                    $(this).attr({'name': name, 'id': id}).val('').removeAttr('checked');
                });
                //Set selected items of dialog to Item Column
                // var value = allVals[i].id;
                // newElement.find("option").each(function () {
                //     $(this).removeAttr('selected');
                //     if ($(this).val() == value) {
                //         $(this).attr("selected", true);
                //     }
                // });
                //Set selected price of dialog to Price Column
                var a = newElement.find('input');
                currentRow = newElement.closest('tr').find('label');
                if (a.length > 1) {
                    a[0].value = parseInt($(selector).find('input')[0].value) + 1;
                    a[1].value = allVals[i].item_code;
                    a[2].value = allVals[i].name;
                    a[3].value = allVals[i].id;
                    a[4].value = allVals[i].customer_po_no;
                    // if (allVals[i].minimun_order == '' || allVals[i].minimun_order == 'None') {
                    //     a[5].value = 1;
                    // } else a[5].value = allVals[i].minimun_order;
                    a[5].value = allVals[i].quantity - allVals[i].receive_quantity;
                    a[6].value = parseFloat(allVals[i].unit_price).toFixed(2);
                    a[9].value = allVals[i].currency;
                    a[10].value = allVals[i].currency_id;
                    a[11].value = allVals[i].part_gp;
                    a[12].value = allVals[i].supplier_code;
                    a[13].value = allVals[i].supplier_id;
                    a[14].value = allVals[i].location_code;
                    a[15].value = allVals[i].location_id;
                    a[16].value = allVals[i].refer_number;
                    a[17].value = allVals[i].refer_line;
                    a[18].value = allVals[i].uom;
                    a[19].value = allVals[i].ref_id;
                    a[20].value = allVals[i].minimun_order;

                    currentRow[0].textContent = a[0].value; // Line Number
                    currentRow[1].textContent = a[1].value; // Item Code
                    currentRow[2].textContent = a[2].value; // Item Name
                    currentRow[3].textContent = a[6].value; // Price
                    currentRow[5].textContent = a[9].value; // Currency Code
                    currentRow[6].textContent = a[11].value; // Part Group
                    currentRow[7].textContent = a[12].value; // Supplier Code
                    currentRow[8].textContent = a[14].value; // Location Code
                    currentRow[9].textContent = a[16].value; // Refer Number
                    currentRow[10].textContent = a[17].value; // Refer Line
                    currentRow[11].textContent = allVals[i].quantity; // Order Quantity
                    currentRow[12].textContent = allVals[i].receive_quantity; // Receive Quantity
                    currentRow[13].textContent = a[18].value; // UOM

                    sum = parseInt($('#id_subtotal').val());
                    sum += parseInt(a[8].value);
                    $('#id_subtotal').val(sum);
                    if (isNaN(sum)) {
                        sum = 0;
                        $('#id_subtotal').val(0);
                        $('#id_total').val(0);
                    }
                    if ($('#id_tax_amount').val()) {
                        sum += parseFloat($('#id_tax_amount').val());
                    }
                    if ($('#id_discount').val()) {
                        sum -= parseFloat($('#id_discount').val());
                    }
                    $('#id_total').val(sum);
                }
                // newElement.find('label').each(function () {
                //     var newFor = $(this).attr('for').replace('-' + (total - 1) + '-', '-' + total + '-');
                //     $(this).attr('for', newFor);
                // });
                total++;
                $('#id_' + type + '-TOTAL_FORMS').val(total);
                $(selector).after(newElement);

            }
        }
    }


    $(document).on('click', "[class^=removerow]", function (event) {
        currentRow = $(this).closest('tr').find('input');
        item_id = currentRow[3].value;
        var order_type = $('#order_type').text();
        if ($('#id_formset_item-TOTAL_FORMS').val() == 1) {
            $(this).closest('tr').css('display', 'none');
            $(this).closest('tr').find('input').each(function () {
                $(this)[0].value = '';
            });
            $('#items_error').removeAttr('style');
            $('#id_subtotal').val(0);
            $('#id_total').val(0);
            $('#btnSave').attr('disabled', true);
            $('#btnPrint').attr('disabled', true);
            $('#btnSendForEdit').attr('disabled', true);
            $('#btnSend').attr('disabled', true);
        } else {
            $('#btnSave').removeAttr('disabled');
            $('#btnPrint').removeAttr('disabled');
            $('#btnSend').removeAttr('disabled');
            $('#btnSendForEdit').removeAttr('disabled');
            var sum = parseFloat($('#id_subtotal').val());
            sum -= parseFloat(currentRow[8].value);
            $('#id_subtotal').val(sum.toFixed(2));
            $('#id_total').val((sum.toFixed(2) + parseFloat($('#id_tax_amount').val()) - parseFloat($('#id_discount').val())).toFixed(2));
            var minus = $('input[name=formset_item-TOTAL_FORMS]').val() - 1;
            $('#id_formset_item-TOTAL_FORMS').val(minus);
            $(this).parents("tr").remove();
        }
        $('#dynamic-table tr.gradeX').each(function (rowIndex, r) {
            $(this).find('td').each(function (colIndex, c) {
                $.each(this.childNodes, function (i, elem) {
                    if (elem.nodeName == 'INPUT' || elem.nodeName == 'LABEL') {
                        if (colIndex == 0) {
                            elem.innerHTML = rowIndex + 1;
                            elem.value = rowIndex + 1;
                        }
                        elem.attributes.name.nodeValue = elem.attributes.name.nodeValue.replace(/\d+/g, rowIndex);
                        elem.id = elem.id.replace(/\d+/g, rowIndex);
                    }
                });
            });
        });
        calculateTotal('#dynamic-table tr.gradeX');
    });
});

//Company Information
$(document).ready(function () {

    //toggle `popup` / `inline` mode
    $.fn.editable.defaults.mode = 'inline';
    //Get company id
    var company_id = $('#company_id').val();
    //make status editable
    $('#companyname').editable({
        type: 'text',
        pk: company_id,
        url: '/orders/change_company/' + company_id + '/',
        title: 'Enter company name',
        success: function (response, newValue) {

            if (!response) {
                return "Unknown error!"
            }
            if (response.success === false) {
                return respond.msg;
            }
        }
    });

    $('#address').editable({
        type: 'text',
        pk: company_id,
        url: '/orders/change_company/' + company_id + '/',
        title: 'Enter company address',
        success: function (response, newValue) {

            if (!response) {
                return "Unknown error!"
            }
            if (response.success === false) {
                return respond.msg;
            }
        }
    });

    $('#email').editable({
        type: 'text',
        pk: company_id,
        url: '/orders/change_company/' + company_id + '/',
        title: 'Enter company email',
        validate: function (value) {
            var valid = valib.String.isEmailLike(value)
            if (valid == false) return 'Please insert valid email'
        },
        success: function (response, newValue) {

            if (!response) {
                return "Unknown error!"
            }
            if (response.success === false) {
                return respond.msg;
            }
        }
    });

});
//Supplier Information
$(document).ready(function (hdSupplierId) {
    var hdSupplierId = $('#hdSupplierId').val();
    loadSupplierInfo(hdSupplierId);

    var callback = function () {
        $.ajax({
            method: "POST",
            url: '/orders/supplier_search_by_code/',
            dataType: 'JSON',
            data: {
                'csrfmiddlewaretoken': $('input[name="csrfmiddlewaretoken"]').val(),
                'supplier_code': $("#form_supplier_code").val()
            },
            responseTime: 200,
            response: function (settings) {
                if (settings.data.value) {
                    this.responseText = '{"success": true}';
                }
                else {
                    this.responseTime = '{"success: false, "msg": "required"}';
                }
            },
            success: function (json) {
                console.log(json);
                $('#hdSupplierId').val(json['id']);
                $("#form_supplier_code").val(json['code']);

                $('#supplier_name').editable('destroy');
                $('#supplier_address').editable('destroy');
                $('#supplier_email').editable('destroy');

                $('#supplier_name').attr('data-pk', json['id']);
                $('#supplier_name').text(json['name']);
                $('#supplier_address').attr('data-pk', json['id']);
                $('#supplier_address').text(json['address']);
                $('#supplier_email').attr('data-pk', json['id']);
                $('#supplier_email').text(json['email']);

                $('#supplier_payment_term').text('Payment Term: ' + json['term'] + ' days');
                $('#supplier_payment_mode').text('Payment Mode: ' + json['payment_mode']);
                $('#supplier_credit_limit').text('Credit Limit: ' + json['credit_limit']);
                loadSupplierInfo(json['id']);
                $('#id_tax').find('option').removeAttr("selected");
                $('#id_tax').find('option').removeAttr("disabled");
                $('#id_tax').find('option[value="' + json['tax_id'] + '"]').attr("selected", "selected");
                $('#id_tax').val(json['tax_id']);
                $('#id_tax option:not(:selected)').attr('disabled', true);
                // load tax again
                var taxid = parseInt($('#id_tax').val());
                if (isNaN(taxid)) {
                    $('#id_tax_amount').val(0);
                    $('#id_total').val(parseFloat($('#id_tax_amount').val()) - parseFloat($('#id_discount').val()) + parseFloat($('#id_subtotal').val()));
                } else {
                    $.ajax({
                        method: "POST",
                        url: '/orders/load_tax/',
                        dataType: 'JSON',
                        data: {
                            'csrfmiddlewaretoken': $('input[name="csrfmiddlewaretoken"]').val(),
                            'tax_id': taxid,
                        },
                        success: function (json) {
                            debugger;
                            var tax_amount = (parseFloat(json) * parseFloat($('#id_subtotal').val())) / 100;
                            $('#id_tax_amount').val(tax_amount.toFixed(2));
                            var total = parseFloat($('#id_subtotal').val()) + tax_amount - parseFloat($('#id_discount').val());
                            $('#id_total').val(total.toFixed(2));
                        }
                    });
                }
                $('#id_currency').find('option').removeAttr('selected');
                $('#id_currency').find('option').removeAttr('disabled');
                $('#id_currency option[value=' + json['currency_id'] + ']').attr('selected', 'selected');
                $('#id_currency').val(json['currency_id']);
                $('#id_currency option:not(:selected)').attr('disabled', true);
            }
        })
    };
    $("#form_supplier_code").keypress(function (e) {
        if (e.which == 13) {
            e.preventDefault();
            callback();
        }
    });
    $('#btnSearchSupplier').on('click', function () {
        var datatbl = $('#supplier-table').DataTable();
        datatbl.destroy();
        $('#supplier-table').dataTable({
            "iDisplayLength": 5,
            "bLengthChange": false,
            "order": [[0, "desc"]],
            "serverSide": true,
            "ajax": {
                "url": "/orders/suppliers_list_as_json/"
            },
            "columns": [
                {"data": "code", "sClass": "text-left"},
                {"data": "name", "sClass": "text-left"},
                {"data": "term_days", "sClass": "text-left"},
                {"data": "payment_mode", "sClass": "text-left"},
                {"data": "credit_limit", "sClass": "text-left"},
                {
                    "orderable": false,
                    "data": null,
                    "render": function (data, type, full, meta) {
                        return '<input type="radio" name="choices" id="' +
                            full.id + '" class="call-checkbox" value="' + full.id + '">';
                    }
                }
            ]
        });
    });

    $('#btnSupplierSelect').on('click', function () {
        var supplier_select_id = $("input[name='choices']:checked").attr('id');

        $('#hdSupplierId').val(supplier_select_id);

        $('#id_currency option[value=' + supplier_select_id + ']').attr('selected', 'selected');

        var nRow = $("input[name='choices']:checked").parents('tr')[0];
        var jqInputs = $('td', nRow);
        $("#form_supplier_code").val(jqInputs[0].innerText);

        $(this).attr('data-dismiss', 'modal');
        callback();
    });

    function loadSupplierInfo(hdSupplierId) {
        $.fn.editable.defaults.mode = 'inline';

        //make status editable
        $('#supplier_name').editable({
            type: 'text',
            pk: hdSupplierId,
            url: '/orders/change_supplier/' + hdSupplierId + '/',
            title: 'Enter supplier name',
            success: function (response, newValue) {
                if (!response) {
                    return "Unknown error!"
                }
                if (response.success === false) {
                    return respond.msg;
                }
            }
        });

        $('#supplier_address').editable({
            type: 'text',
            pk: hdSupplierId,
            url: '/orders/change_supplier/' + hdSupplierId + '/',
            title: 'Enter supplier address',
            success: function (response, newValue) {
                if (!response) {
                    return "Unknown error!"
                }
                if (response.success === false) {
                    return respond.msg;
                }
            }
        });

        $('#supplier_email').editable({
            type: 'text',
            pk: hdSupplierId,
            url: '/orders/change_supplier/' + hdSupplierId + '/',
            title: 'Enter supplier email',
            validate: function (value) {
                var valid = valib.String.isEmailLike(value)
                if (valid == false) return 'Please insert valid email'
            },
            success: function (response, newValue) {
                if (!response) {
                    return "Unknown error!"
                }
                if (response.success === false) {
                    return respond.msg;
                }
            }
        });
    };


    // $('#id_supplier').change(function () {
    //     var supplier_id = parseInt($(this).val());
    //     $.ajax({
    //         method: "POST",
    //         url: '/orders/supplier/',
    //         dataType: 'JSON',
    //         data: {
    //             'csrfmiddlewaretoken': $('input[name="csrfmiddlewaretoken"]').val(),
    //             'supplier_id': supplier_id,
    //         },
    //         responseTime: 200,
    //         response: function (settings) {
    //             if (settings.data.value) {
    //                 this.responseText = '{"success": true}';
    //             } else {
    //                 this.responseText = '{"success": false, "msg": "required"}';
    //             }
    //         },
    //         success: function (json) {
    //             console.log(json);
    //             $('#hdSupplierId').val(json['id']);
    //
    //             $('#supplier_name').editable('destroy');
    //             $('#supplier_address').editable('destroy');
    //             $('#supplier_email').editable('destroy');
    //
    //             $('#supplier_name').attr('data-pk', json['id']);
    //             $('#supplier_name').text(json['name']);
    //             $('#supplier_address').attr('data-pk', json['id']);
    //             $('#supplier_address').text(json['address']);
    //             $('#supplier_email').attr('data-pk', json['id']);
    //             $('#supplier_email').text(json['email']);
    //
    //             $('#supplier_payment_term').text('Payment Term: ' + json['term'] + ' days');
    //             $('#supplier_payment_mode').text('Payment Mode: ' + json['payment_mode']);
    //             $('#supplier_credit_limit').text('Credit Limit: ' + json['credit_limit']);
    //             loadSupplierInfo(json['id']);
    //         }
    //     });
    // });
});
//event handle for input discount
$('#id_discount').change(function () {
    if ($(this).val() != '') {
        var sum = parseFloat($('#id_subtotal').val()) + parseFloat($('#id_tax_amount').val());
        sum -= parseInt(this.value);
        $('#id_total').val(sum);
    } else {
        var sum = 0;
        $('#dynamic-table tr.gradeX').each(function () {
            var $tds = $(this).find('input');
            amount = $tds[8].value;
            sum += parseInt(amount);
            $('#id_subtotal').val(sum);
            var total = sum + parseFloat($('#id_tax_amount').val())
            $('#id_total').val(total);
        })
    }
});

// calculate subtotal and total
function calculateTotal(selector) {
    // currentRow = $(selector).closest('tr').find('input');
    // amount = currentRow[7].value * currentRow[11].value;
    // currentRow[13].value = amount.toFixed(4);
    var subtotal = 0;
    var total = 0;
    $('#dynamic-table tr.gradeX').each(function () {
        var $tds = $(this).find('input');
        var label = $(this).find('label');
        price = $tds[6].value * $tds[7].value
        amount = $tds[5].value * price;
        $tds[8].value = parseFloat(amount.toFixed(2));
        label[4].textContent = $tds[8].value;
        subtotal += parseFloat(amount);
    });
    $('#id_subtotal').val(subtotal.toFixed(2));
    // var total = subtotal + parseFloat($('#id_tax_amount').val()) - parseFloat($('#id_discount').val())
    if ($('#id_discount').val() == '' || $('#id_discount').val() == null) {
        total = subtotal + parseFloat($('#id_tax_amount').val());
    } else {
        total = subtotal + parseFloat($('#id_tax_amount').val()) - parseFloat($('#id_discount').val());
    }
    $('#id_total').val(total.toFixed(2));
}

//event handle calculate subtotal and total base on quantity
$('#dynamic-table tr.gradeX').each(function () {
    currentRow = $(this).closest('tr').find('input');
    currentColumn = $(this).closest('tr').find('td');
    $mainElement = '#' + currentRow[5].id; // ID of Quantity Column
    var order_type = $('#order_type').text();
    var order_id = $('#order_id').text();
    $($mainElement).change(function () {
        debugger;
        currentRow = $(this).closest('tr').find('input');
        currentLabel = $(this).closest('tr').find('label');
        var quantity = currentRow[5].value; // Quantity Value
        var quantity_receive = parseInt(currentRow[5].value, 10) + parseInt(currentLabel[10].textContent, 10);
        var price = currentRow[6].value; // Price Value
        // if (quantity < parseInt(currentRow[20].value, 10)) {
        // $('#minimum_order_error').removeAttr('style');
        // $('#minimum_order_error').text('The quantity of product ' + currentRow[1].value + ' must greater than minimun order. The minimun order is: ' + currentRow[20].value);
        // $(this).closest('tr').attr('style', 'background-color: yellow !important');
        // currentRow[9].value = 0; // Amount Value
        // $('#btnPrint').attr('disabled', true);
        // $('#btnSave').attr('disabled', true);
        // $('#btnSend').attr('disabled', true);
        // $('#btnSendForEdit').attr('disabled', true);
        // $('#dynamic-table tr.gradeX').each(function () {
        //     $(this).closest('tr').find('input').not(currentRow[5]).attr('disabled', true);
        // });
        // }
        if (quantity < 1) {
            $('#minimum_order_error').removeAttr('style');
            $('#minimum_order_error').text('The quantity of product ' + currentRow[1].value + ' must greater than 0');
            $(this).closest('tr').attr('style', 'background-color: yellow !important');
            currentRow[8].value = 0; // Amount Value
            $('#btnPrint').attr('disabled', true);
            $('#btnSave').attr('disabled', true);
            $('#btnSend').attr('disabled', true);
            $('#btnSendForEdit').attr('disabled', true);
            $('#dynamic-table tr.gradeX').each(function () {
                $(this).closest('tr').find('input').not(currentRow[5]).attr('disabled', true);
            });
        }
        else if (quantity_receive > parseInt(currentLabel[9].textContent, 10)) {
            $(this).closest('tr').attr('style', 'background-color: pink !important');
            // $('#items_error').text('The product have been removed from stock!');
            $('#items_error').text('The quantity exceeds receivable amount!');
            $('#items_error').removeAttr('style');
            calculateTotal(this);
        }
        else {
            calculateTotal(this);
            $('#dynamic-table tr.gradeX').each(function () {
                $(this).closest('tr').find('input').removeAttr('disabled');
            });
            $(this).closest('tr').removeAttr('style');
            $('#items_error').css('display', 'none');
            $('#minimum_order_error').css('display', 'none');
            $('#btnPrint').removeAttr('disabled');
            $('#btnSave').removeAttr('disabled');
            $('#btnSend').removeAttr('disabled');
            $('#btnSendForEdit').removeAttr('disabled');
        }
        // validationItemsFormset();
    });
});

// event change price
$('#dynamic-table tr.gradeX').find('input').each(function () {
    currentRow = $(this).closest('tr').find('input');
    // currentItem = $(this).closest('tr').find('option:selected');
    $priceElement = '#' + currentRow[6].id; // ID of Price Column
    $($priceElement).change(function () {
        currentRow = $(this).closest('tr').find('input');
        currentRow[8].value = Number(currentRow[5].value * currentRow[6].value * currentRow[7].value).toFixed(2);
        if (currentRow[6].value > 0) { // Check Price Value
            $('#validate_error').css('display', 'none');
            // var subtotal = 0;
            // $('#dynamic-table tr.gradeX').each(function () {
            //     var $tds = $(this).find('input');
            //     amount = $tds[8].value;
            //     subtotal += parseFloat(amount);
            //     $('#id_subtotal').val(subtotal.toFixed(2));
            //     if (parseFloat($('#id_tax_amount').val()) != 0) {
            //         var total = (subtotal * parseFloat($('#id_tax_amount').val())) / 100 - parseFloat($('#id_discount').val())
            //         $('#id_total').val(total.toFixed(2));
            //     } else {
            //         var total = subtotal - parseFloat($('#id_discount').val())
            //         $('#id_total').val(total.toFixed(2));
            //     }
            // });
            currentRow.parents('tr').removeAttr('style');
            $('#dynamic-table tr.gradeX').each(function () {
                $(this).closest('tr').find('input').removeAttr('disabled');
            });
            $('#validate_error').css('display', 'none');
            $('#btnPrint').removeAttr('disabled');
            $('#btnSave').removeAttr('disabled');
            $('#btnSend').removeAttr('disabled');
            $('#btnSendForEdit').removeAttr('disabled');
            // validationItemsFormset();
        } else {
            $('#validate_error').text('Price of product must greater than 0 and not none');
            $('#validate_error').removeAttr('style');
            currentRow.parents('tr').attr('style', 'background-color: red !important');
            $('#id_subtotal').val(0);
            $('#id_total').val(0);
            $('#btnPrint').attr('disabled', true);
            $('#btnSave').attr('disabled', true);
            $('#btnSend').attr('disabled', true);
            $('#btnSendForEdit').attr('disabled', true);
            $('#dynamic-table tr.gradeX').each(function () {
                $(this).closest('tr').find('input').not(currentRow[6]).attr('disabled', true);
            });
        }
        calculateTotal(this);
    });
});

//change amount event
$('#dynamic-table tr.gradeX').each(function () {
    currentRow = $(this).closest('tr').find('input');
    $amountElement = '#' + currentRow[8].id; // Get ID of Amount Column
    $($amountElement).change(function () {
        $tds = $(this).closest('tr').find('input');
        var subtotal = 0;
        if ($(this)[0].value < 0) {
            $(this).closest('tr').attr('style', 'background-color: red !important');
            $('#items_error').text('The product ' + $tds[1].value + ' must have amount greater than 0');
            $('#items_error').removeAttr('style');
            $('#btnPrint').attr('disabled', true);
            $('#btnSave').attr('disabled', true);
            $('#btnSend').attr('disabled', true);
            $('#btnSendForEdit').attr('disabled', true);
            $('#dynamic-table tr.gradeX').each(function () {
                $(this).closest('tr').find('input').not($tds[8]).attr('disabled', true);
            });
            $('#id_subtotal').val(0);
            $('#id_total').val(0);
        } else {
            $('#items_error').css('display', 'none');
            $(this).closest('tr').removeAttr('style');
            $('#dynamic-table tr.gradeX').each(function () {
                var $tds = $(this).find('input');
                amount = $tds[8].value;
                subtotal += parseFloat(amount);
                $('#id_subtotal').val(subtotal.toFixed(2));
                discount_val = $('#id_discount').val()
                if (discount_val == '' || discount_val == null) {
                    var total = subtotal + parseFloat($('#id_tax_amount').val());
                } else {
                    var total = subtotal + parseFloat($('#id_tax_amount').val()) - parseFloat(discount_val);
                }
                $('#id_total').val(total.toFixed(2));
                $(this).closest('tr').find('input').removeAttr('disabled');
            });
            $('#btnPrint').removeAttr('disabled');
            $('#btnSave').removeAttr('disabled');
            $('#btnSend').removeAttr('disabled');
            $('#btnSendForEdit').removeAttr('disabled');
        }
    });
});

// event copy supplier address
$('#btnCopySupplier').click(function () {
    var hdSupplierId = $('#hdSupplierId').val();
    $.ajax({
        method: "POST",
        url: '/orders/supplier/',
        dataType: 'JSON',
        data: {
            'csrfmiddlewaretoken': $('input[name="csrfmiddlewaretoken"]').val(),
            'supplier_id': hdSupplierId,
        },
        success: function (json) {
            console.log(json);
            $('#id_name').val($('#supplier_name').text());
            $('#id_address').val($('#supplier_address').text());
            $('#id_email').val($('#supplier_email').text());
            $('#id_code').val(json['code']);
            $('#id_phone').val(json['phone']);
            $('#id_fax').val(json['fax']);
        }
    });
});

// event change address
$('#id_supplier_address').change(function () {
    var address_id = $(this).val();
    $.ajax({
        method: "POST",
        url: '/orders/change_address/',
        dataType: 'JSON',
        data: {
            'csrfmiddlewaretoken': $('input[name="csrfmiddlewaretoken"]').val(),
            'address_id': address_id,
        },
        success: function (json) {
            console.log(json);
            if ($('#id_supplier_address option:selected').text() == "") {
                $('#id_name').val("");
                $('#id_address').val("");
                $('#id_email').val("");
                $('#id_code').val("");
                $('#id_phone').val("");
                $('#id_fax').val("");
                $('#id_attention').val("");
                $('#id_note_1').val("");
                $('#id_note_2').val("");
            } else {
                $('#id_name').val($('#id_supplier_address option:selected').text());
                $('#id_address').val(json['address']);
                $('#id_email').val(json['email']);
                $('#id_code').val(json['code']);
                $('#id_phone').val(json['phone']);
                $('#id_fax').val(json['fax']);
                $('#id_attention').val(json['attention']);
                $('#id_note_1').val(json['note_1']);
                $('#id_note_2').val(json['note_2']);
            }
        }
    });
});

function validationItemsFormset() {
    $('#dynamic-table tr.gradeX').find('input').each(function () {
        var order_type = $('#order_type').text();
        var order_id = $('#order_id').text();
        var display = $('#dynamic-table tr.gradeX:last').css("display");
        currentItem = $(this).closest('tr').find('option:selected');
        currentRow = $(this).closest('tr').find('input');
        currentLabel = $(this).closest('tr').find('label');
        inputQuantity = '#' + currentRow[5].id;
        var count = $('#dynamic-table').find('tr').filter(function () {
            var colors = ["#ffc0cb", "rgb(255, 192, 203)"];
            return $.inArray($(this).css('background-color'), colors) !== -1;
        }).length;
        if (display == 'none') {
            $('#items_error').css('display', 'none');
        } else {
            if (order_id == "" && order_type == 1) {
                if (parseInt(currentRow[5].value, 10) > (parseInt(currentLabel[9].textContent, 10) -
                    parseInt(currentLabel[10].textContent, 10))) {
                    $(this).closest('tr').attr('style', 'background-color: pink !important');
                    $('#items_error').text('The quantity exceeds receivable amount!');
                    $('#items_error').removeAttr('style');
                    calculateTotal(this);
                }
                if (currentRow[6].value == "" || currentRow[6].value == "None") {
                    $(this).closest('tr').attr('style', 'background-color: red !important');
                    $('#validate_error').text('Price of product must greater than 0 and not none');
                    $('#validate_error').removeAttr('style');
                    $('#btnSave').attr('disabled', true);
                }
                if (count > 0) {
                    $('#items_error').removeAttr('style');
                    $('#items_error').text('Error Adding Product! Please check!');
                } else {
                    $('#items_error').css('display', 'none');
                }
            } else {
                if (parseInt(currentRow[5].value, 10) > (parseInt(currentLabel[9].value, 10) -
                    parseInt(currentLabel[10].value, 10))) {
                    $(this).closest('tr').attr('style', 'background-color: pink !important');
                    // $('#items_error').text('The product have been removed from stock!');
                    $('#items_error').text('The quantity exceeds receivable amount!');
                    $('#items_error').removeAttr('style');
                    calculateTotal(this);
                }
                if (count > 0) {
                    $('#items_error').removeAttr('style');
                    // $('#items_error').text('The product have been removed from stock');
                    $('#items_error').text('Error Adding Product! Please check!');
                } else {
                    $('#items_error').css('display', 'none');
                }
            }
            // Remove/Hide item selected in the modal popup
            // $('#dynamic-table tr.gradeX').each(function () {
            //     var table = $('#tblData').DataTable();
            //     var data = table.rows().data();
            //     currentRow = $(this).closest('tr').find('input');
            //     item_id = currentRow[3].value;
            //     data.each(function (value, index) {
            //         if (value[9] == item_id) {
            //             var row = table.row('#' + item_id).node();
            //             row.setAttribute("style", "display: none");
            //         }
            //     });
            // });
        }
    });
    $('#id_supplier_address').find('option').each(function () {
        if ($('#id_name').val() == $(this).text()) {
            $(this).attr('selected', true);
        }
    });
}

// event change exchange rate
$('#dynamic-table tr.gradeX').find('input').each(function () {
    currentRow = $(this).closest('tr').find('input');
    $exchangeRate = '#' + currentRow[7].id;
    $($exchangeRate).change(function () {
        currentRow = $(this).closest('tr').find('input');
        var exchange_rate = currentRow[7].value;
        if (exchange_rate <= 0) {
            $('#minimum_order_error').removeAttr('style');
            $('#minimum_order_error').text('The exchange rate of product ' + currentRow[1].value + ' must greater than 0');
            $(this).closest('tr').attr('style', 'background-color: red !important');
            currentRow[8].value = 0;
            $('#btnPrint').attr('disabled', true);
            $('#btnSave').attr('disabled', true);
            $('#btnSend').attr('disabled', true);
            $('#btnSendForEdit').attr('disabled', true);
            $('#dynamic-table tr.gradeX').each(function () {
                $(this).closest('tr').find('input').not(currentRow[7]).attr('disabled', true);
            });
        } else {
            $('#dynamic-table tr.gradeX').each(function () {
                $(this).closest('tr').find('input').removeAttr('disabled');
            });
            $('#minimum_order_error').css('display', 'none');
            $(this).closest('tr').removeAttr('style');
            $('#btnPrint').removeAttr('disabled');
            $('#btnSave').removeAttr('disabled');
            $('#btnSend').removeAttr('disabled');
            $('#btnSendForEdit').removeAttr('disabled');
        }
        calculateTotal(this);
    });
});


function changeCurrency(arrItems, currency_id, currency_name) {
    $.ajax({
        method: "POST",
        url: '/orders/load_currency/',
        dataType: 'JSON',
        data: {
            'csrfmiddlewaretoken': $('input[name="csrfmiddlewaretoken"]').val(),
            // 'arrItems[]': JSON.stringify(arrItems),
            'arrItems': JSON.stringify(arrItems),
            'currency_id': currency_id,
        },
        success: function (json) {
            console.log(json);
            var item_currency_not_match = [];
            var amount = 0;
            var purchase_price = 0;
            for (var i in json) {
                if (json[i].constructor === Object) {
                    $('#dynamic-table tr.gradeX').each(function () {
                        currentRow = $(this).closest('tr').find('input');
                        currentLabel = $(this).closest('tr').find('label');
                        currentItem = currentRow[1].value;
                        if (currentRow[3].value == json[i].id) {
                            if (json[i].rate == 0 || json[i].sale_price == "") {
                                // item_currency_not_match.push({item: currentItem, currency: json[i].currency});
                                item_currency_not_match.push('Can not get Exchange Rate from ' + json[i].currency + ' to ' + currency_name);
                                currentRow[7].value = 0;
                                currentRow[8].value = 0;
                                currentLabel[4].textContent = currentRow[8].value;
                                $('.lblCurrency').text(json[i].currency);
                            }
                            else {
                                currentRow[7].value = parseFloat(json[i].rate).toFixed(4);
                                amount = currentRow[7].value * currentRow[5].value * currentRow[6].value;
                                currentRow[8].value = amount.toFixed(2);
                                currentLabel[4].textContent = currentRow[8].value;
                                $('.lblCurrency').text(json['symbol']);
                            }
                        }
                    });
                }
            }
            // console.log(item_currency_not_match);
            if (item_currency_not_match.length > 0) {
                $("#currency_error").text("");
                var uniqueNames = [];
                $.each(item_currency_not_match, function (i, el) {
                    if ($.inArray(el, uniqueNames) === -1) uniqueNames.push(el);
                });
                for (i = 0; i < uniqueNames.length; i++) {
                    document.getElementById('currency_error').innerHTML += uniqueNames[i] + '<br>';
                }
                $('#currency_error').removeAttr('style');
                $('#btnPrint').attr('disabled', true);
                $('#btnSave').attr('disabled', true);
                $('#btnSend').attr('disabled', true);
                $('#btnSendForEdit').attr('disabled', true);
                $('#dynamic-table tr.gradeX').each(function () {
                    $(this).closest('tr').find('input').attr('disabled', true);
                });
            } else {
                $('#currency_error').css('display', 'none');
                $('#btnPrint').removeAttr('disabled');
                $('#btnSave').removeAttr('disabled');
                $('#btnSend').removeAttr('disabled');
                $('#btnSendForEdit').removeAttr('disabled');
                $('#dynamic-table tr.gradeX').each(function () {
                    $(this).closest('tr').find('input').removeAttr('disabled');
                });
            }
            calculateTotal('#dynamic-table tr.gradeX');
        }
    });
}

// event change currency
$('#id_currency').change(function () {
    debugger;
    var currency_id = parseInt($(this).val());
    var currency_name = $('#id_currency option:selected').text();
    var arrItems = [];
    $('#dynamic-table tr.gradeX').each(function () {
        currentRow = $(this).closest('tr').find('input');
        arrItems.push({
            item_id: currentRow[3].value,
            currency_id: currentRow[10].value
        });
    });
    changeCurrency(arrItems, currency_id, currency_name);
});

//event generate document number by order date
$('#id_order_date').change(function () {
    var order_date = $('#id_order_date').val();
    var order_type = $('#order_type').text();
    var document_number = $('#id_document_number').val()
    $.ajax({
        method: "POST",
        url: '/orders/generate_document_number/',
        dataType: 'text',
        data: {
            'csrfmiddlewaretoken': $('input[name="csrfmiddlewaretoken"]').val(),
            'order_date': order_date,
            'order_type': order_type,
            'document_number': document_number
        },
        success: function (data) {
            $('#id_document_number').val(data);
        }
    });
});


$(document).ready(function () {
    // var order_id = $('#order_id').text();
    // if (order_id != "") {
    //     $('#dynamic-table tr.gradeX').each(function () {
    //         currentLabel = $(this).closest('tr').find('label');
    //         currentRow = $(this).closest('tr').find('input');
    //         $mainElement = '#' + currentRow[5].id;
    //         if (parseInt(currentLabel[9].textContent, 10) == parseInt(currentLabel[10].textContent, 10))) {
    //             $(this).closest('tr').find($mainElement).attr('disabled', true);
    //         }
    //     });
    // }

    $("#search_input").keypress(function (e) {
        var key = e.which;
        if (key == 13) {
            supplier_items();
        }
    });
    // $('#btnSearchItem').on('click', function () {
    //     supplier_items();
    // });
    $('#btnOpenItemDialog').on('click', function () {
        var dataTable = $('#tblData').DataTable();
        dataTable.clear();
        $('#tblData_filter > label > input').val(' ');
        supplier_items();
        // $('#tblData_filter > label > input').val('');
    });
    $('#btnSave').on('click', function () {
        var countRowVisible = $('#dynamic-table tr.gradeX:visible').length;
        if (countRowVisible == 0) {
            $('#items_error').text('Please select products before save!');
            $('#items_error').removeAttr('style');
            $('#btnPrint').attr('disabled', true);
            $('#btnSave').attr('disabled', true);
            $('#btnSend').attr('disabled', true);
            $('#btnSendForEdit').attr('disabled', true);
        }
    });
    $('#btnSend').on('click', function () {
        var countRowVisible = $('#dynamic-table tr.gradeX:visible').length;
        if (countRowVisible == 0) {
            $('#items_error').text('Please select products before save!');
            $('#items_error').removeAttr('style');
            $('#btnPrint').attr('disabled', true);
            $('#btnSave').attr('disabled', true);
            $('#btnSend').attr('disabled', true);
            $('#btnSendForEdit').attr('disabled', true);
        }
    });
    $('#btnSendForEdit').on('click', function () {
        var countRowVisible = $('#dynamic-table tr.gradeX:visible').length;
        if (countRowVisible == 0) {
            $('#items_error').text('Please select products before save!');
            $('#items_error').removeAttr('style');
            $('#btnPrint').attr('disabled', true);
            $('#btnSave').attr('disabled', true);
            $('#btnSend').attr('disabled', true);
            $('#btnSendForEdit').attr('disabled', true);
        }
    });
});

function supplier_items() {
    debugger;
    // var data = $('#search_input').val();
    var supplier_id = $('#hdSupplierId').val();
    var exclude_item_array = [];
    var exclude_item_list = {};
    $('#dynamic-table tr.gradeX').each(function () {
        var display = $(this).css("display");
        currentRow = $(this).closest('tr').find('input');
        if (display != 'none') {
            exclude_item_array.push(currentRow[3].value);
        }
    });
    if (exclude_item_array.length > 0) {
        exclude_item_list = JSON.stringify(exclude_item_array);
    }

    // if (data.length == 0)
    //     data = '0';
    // $.ajax({
    //     method: "POST",
    //     // url: '/orders/good_receive_search_item/',
    //     url: '/orders/orderitem_list_json/',
    //     data: {
    //         'csrfmiddlewaretoken': $('input[name="csrfmiddlewaretoken"]').val(),
    //         'supplier_id': supplier_id,
    //         // 'search_condition': data,
    //         'exclude_item_list': exclude_item_list
    //     },
    //     responseTime: 200,
    //     response: function (settings) {
    //         if (settings.data.value) {
    //             this.responseText = '{"success": true}';
    //         } else {
    //             this.responseText = '{"success": false, "msg": "required"}';
    //         }
    //     },
    //     success: function (data) {
    //         $("#myDialog").html('');
    //         $("#myDialog").html(data);
    //         // $('#tblData').dataTable({
    //         //     "aaSorting": [[4, "desc"]],
    //         //     "bFilter": true,
    //         //     "bLengthChange": false,
    //         //     "iDisplayLength": 5,
    //         // });
    //     }
    // });
    var datatbl = $('#tblData').DataTable();
    datatbl.destroy();
    var list_url = $('#list_url').text();
    $('#tblData').DataTable({
        "iDisplayLength": 5,
        "bLengthChange": false,
        "order": [[1, "desc"], [0, "desc"]],
        "serverSide": true,
        "ajax": {
            "url": list_url,
            "data": {
                "csrfmiddlewaretoken": $('input[name="csrfmiddlewaretoken"]').val(),
                "supplier_id": supplier_id,
                "exclude_item_list": exclude_item_list
            }
        },
        "rowId": "line_id",
        "columns": [
            {
                "data": "item_id",
                // "visible": false,
                "className": "hide_column"
            },
            {"data": "item_name", "sClass": "text-left"},
            {"data": "supplier_code", "sClass": "text-left"},
            {"data": "refer_number", "sClass": "text-left"},
            {"data": "refer_line", "sClass": "text-left"},
            {"data": "location_code", "sClass": "text-left"},
            {"data": "part_no", "sClass": "text-left"},
            {"data": "part_gp", "sClass": "text-left"},
            {"data": "purchase_price", "sClass": "text-right"},
            {"data": "currency_code", "sClass": "text-left"},
            {
                "data": "location_id",
                // "visible": false,
                "className": "hide_column"
            },
            {
                "data": "currency_id",
                // "visible": false,
                "className": "hide_column"
            },
            {
                "data": "line_id",
                // "visible": false,
                "className": "hide_column"
            },
            {
                "data": "unit",
                // "visible": false,
                "className": "hide_column"
            },
            {
                "data": "supplier_id",
                // "visible": false,
                "className": "hide_column"
            },
            {
                "data": "minimun_order",
                // "visible": false,
                "className": "hide_column"
            },
            {
                "data": "refer_id",
                // "visible": false,
                "className": "hide_column"
            },
            {
                "data": "customer_po_no",
                // "visible": false,
                "className": "hide_column"
            },
            {"data": "order_qty"},
            {"data": "receive_qty"},
            {
                "orderable": false,
                "data": null,
                "render": function (data, type, row, meta) {
                    return '<input type="checkbox" name="choices" id="' + row.line_id + '"'
                        + 'class="call-checkbox" value="' + row.purchase_price + '"></td>';
                }
            }
        ]
    });
}