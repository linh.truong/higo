/**
 * Created by tho.pham on 10/21/2016.
 */

function cloneMore(selector, type, allVals) {
    var display = $(selector).css("display")
    var order_type = $('#order_type').text();
    var sum = 0;
    var i = 0;
    var item_id = 0;
    var today = $.datepicker.formatDate('yy-mm-dd', new Date());
    if (display == 'none') {
        //show first row of table and set Item, Price of dialog
        $(selector).removeAttr("style")

        $(selector).find('label').each(function () {
            var name = $(this).attr('name').replace('-' + (total - 1) + '-', '-' + total + '-');
            var id = 'id_' + name;
            $(this).attr({'name': name, 'id': id}).val('').removeAttr('checked');
        });
        findInput = $(selector).find('input');
        // currentItem = $(selector).closest('tr').find('option:selected');
        currentLabel = $(selector).closest('tr').find('label');
        //add value to Input
        findInput[0].value = 1;
        findInput[1].value = allVals[i].customer_code;
        findInput[2].value = allVals[i].customer_id;
        findInput[3].value = allVals[i].customer_name;
        findInput[4].value = allVals[i].currency;
        findInput[5].value = allVals[i].currency_id;
        findInput[10].value = today;

        //add value to Label
        currentLabel[0].textContent = findInput[0].value;
        currentLabel[1].textContent = findInput[1].value;
        currentLabel[2].textContent = findInput[3].value;
        currentLabel[3].textContent = findInput[4].value;

        //if selected items > 1
        i = 1;
    }
    ;
    $('#btnSave').removeAttr('disabled');
    for (i; i < allVals.length; i++) {
        $(selector).each(function () {
            $("input[name*='effective_date']").datepicker('remove');
        });
        if (allVals[i].id != 0) {
            var newElement = $(selector).clone(true);
            var total = $('#id_' + type + '-TOTAL_FORMS').val();
            newElement.removeAttr("style")
            newElement.find(':input').each(function () {
                var name = $(this).attr('name').replace('-' + (total - 1) + '-', '-' + total + '-');
                var id = 'id_' + name;
                $(this).attr({'name': name, 'id': id}).val('').removeAttr('checked');
            });
            newElement.find('label').each(function () {
                var name = $(this).attr('name').replace('-' + (total - 1) + '-', '-' + total + '-');
                var id = 'id_' + name;
                $(this).attr({'name': name, 'id': id}).val('').removeAttr('checked');
            });

            var a = newElement.find('input');
            currentRow = newElement.closest('tr').find('label');

            if (a.length > 1) {
                a[0].value = parseInt($(selector).find('input')[0].value) + 1;
                a[1].value = allVals[i].customer_code;
                a[2].value = allVals[i].customer_id;
                a[3].value = allVals[i].customer_name;
                a[4].value = allVals[i].currency;
                a[5].value = allVals[i].currency_id;
                a[10].value = today;

                //add value to Label
                currentRow[0].textContent = a[0].value;
                currentRow[1].textContent = a[1].value;
                currentRow[2].textContent = a[3].value;
                currentRow[3].textContent = a[4].value;

            }
            // newElement.find('label').each(function () {
            //     var newFor = $(this).attr('for').replace('-' + (total - 1) + '-', '-' + total + '-');
            //     $(this).attr('for', newFor);
            // });
            total++;
            $('#id_' + type + '-TOTAL_FORMS').val(total);
            $(selector).after(newElement);
        }
    }
}

$('#btnCustomerSelect').on('click', function () {
    var allVals = [];
    var table = $('#customer-table').DataTable();
    var rowcollection = table.$(".call-checkbox:checked", {"page": "all"});
    rowcollection.each(function (index, elem) {
        var jqInputs = elem.closest('tr').cells;
        allVals.push({
            customer_id: elem.id,
            customer_name: jqInputs[1].innerText,
            customer_code: jqInputs[0].innerText,
            currency_id: elem.value,
            currency: jqInputs[4].innerText
        });
    });
    $('input[type=checkbox]').click(function () {
        if ($(this).is(':checked'))
            $(this).attr('checked', 'checked');
        else
            $(this).removeAttr('checked');
    });
    if (allVals.length > 0) {
        cloneMore('#dynamic-table tr.gradeX:last', 'formset_customer_item', allVals);
        $('input[checked=checked]').each(function () {
            $(this).removeAttr('checked');
        });
        $('#items_error').css('display', 'none');
    }
    ;
    $('#dynamic-table tr.gradeX:last').each(function () {
        $("input[name*='effective_date']").datepicker({
            format: 'yyyy-mm-dd',
            todayHighlight: true,
            autoclose: true
        });
    });

    $(this).attr('data-dismiss', 'modal');
});

$('#txtFilter').on('keypress', function (e) {
    if (e.which === 13) {
        var txtFilter = $('#txtFilter').val();
        $.ajax({
            method: "POST",
            url: '/items/get_customer_info1/',
            dataType: 'JSON',
            data: {
                'csrfmiddlewaretoken': $('input[name="csrfmiddlewaretoken"]').val(),
                'customer_code': txtFilter,
            },
            responseTime: 200,
            success: function (json) {
                console.log(json);
                if (json['Fail']) {
                    alert("Can not find Customer. Please type correct Customer Code!")
                }
                if (json['id']) {
                    var new_data = [json['code'], json['name'], json['currency_code'], '', '', '', '', '', json['id']];
                    var allVals = [];
                    allVals.push({
                        customer_id: json['id'],
                        customer_name: json['name'],
                        customer_code: json['code'],
                        currency_id: json['currency_id'],
                        currency: json['currency_code']
                    });
                    if (allVals.length > 0) {
                        cloneMore('#dynamic-table tr.gradeX:last', 'formset_customer_item', allVals);
                        $('input[checked=checked]').each(function () {
                            $(this).removeAttr('checked');
                        });
                        $('#items_error').css('display', 'none');
                    }
                    $('#dynamic-table tr.gradeX:last').each(function () {
                        $("input[name*='effective_date']").datepicker({
                            format: 'yyyy-mm-dd',
                            todayHighlight: true,
                            autoclose: true
                        });
                    });
                }
                $('#txtFilter').val('');
            }
        });
    }
});

$('#id_code').on('keypress', function (e) {
    if (e.which === 13) {
        var item_code = $('#id_code').val();
        $.ajax({
            method: "POST",
            url: '/items/get_item_info/',
            dataType: 'JSON',
            data: {
                'csrfmiddlewaretoken': $('input[name="csrfmiddlewaretoken"]').val(),
                'item_code': item_code,
                'item_type': 1,
            },
            responseTime: 200,
            complete: function (xmlHttp) {
                if (xmlHttp.status == 278) {
                    window.location.href = xmlHttp.getResponseHeader("Location").replace(/\?.*$/, "?next=" + window.location.pathname);
                }
            },
            success: function (json) {
                console.log(json);
                $('#id_code').next('.inputs').focus();
            }
        });
    }
});


$(document).on('click', "[class^=removerow]", function (event) {
    if ($('#id_formset_customer_item-TOTAL_FORMS').val() == 1) {
        $(this).closest('tr').css("background-color", "");
        $(this).closest('tr').css('display', 'none');
        $('#btnSave').attr('disabled', true);
    } else {
        $('#btnSave').removeAttr('disabled');
        var minus = $('input[name=formset_customer_item-TOTAL_FORMS]').val() - 1;
        $('#id_formset_customer_item-TOTAL_FORMS').val(minus);
        $(this).parents("tr").remove();
    }

    $('#dynamic-table tr.gradeX').each(function (rowIndex, r) {
        $(this).find('td').each(function (colIndex, c) {
            $.each(this.childNodes, function (i, elem) {
                if (elem.nodeName == 'INPUT' || elem.nodeName == 'LABEL') {
                    if (colIndex == 0) {
                        elem.innerHTML = rowIndex + 1;
                        elem.value = rowIndex + 1;
                    }
                    elem.attributes.name.nodeValue = elem.attributes.name.nodeValue.replace(/\d+/g, rowIndex);
                    elem.id = elem.id.replace(/\d+/g, rowIndex);
                }
            });
        });
    });
});

$('#dynamic-table tr.gradeX').find('input').each(function () {
    currentRow = $(this).closest('tr').find('input');
    $priceElement = '#' + currentRow[6].id;
    $($priceElement).change(function () {
        currentRow = $(this).closest('tr').find('input');
        if (currentRow[6].value > 0) {
            $('#validate_error').css('display', 'none');
            currentRow.parents('tr').removeAttr('style');
            $('#dynamic-table tr.gradeX').each(function () {
                $(this).closest('tr').find('input').removeAttr('disabled');
            });
            $('#validate_error').css('display', 'none');
            $('#btnSave').removeAttr('disabled');
            $('#btnOpenItemDialog').removeAttr('disabled');
            $('#btnDelete').removeAttr('disabled');
        } else {
            $('#validate_error').text('Sale Price must greater than 0 and not none');
            $('#validate_error').removeAttr('style');
            currentRow.parents('tr').attr('style', 'background-color: red !important');
            $('#btnSave').attr('disabled', true);
            $('#btnOpenItemDialog').attr('disabled', true);
            $('#btnDelete').attr('disabled', true);
            $('#dynamic-table tr.gradeX').each(function () {
                $(this).closest('tr').find('input').not(currentRow[6]).attr('disabled', true);
            });
        }
    });
});

$('#dynamic-table tr.gradeX').find('input').each(function () {
    currentRow = $(this).closest('tr').find('input');
    $priceElement = '#' + currentRow[9].id;
    $($priceElement).change(function () {
        currentRow = $(this).closest('tr').find('input');
        if (currentRow[9].value > 0) {
            $('#validate_error').css('display', 'none');
            currentRow.parents('tr').removeAttr('style');
            $('#dynamic-table tr.gradeX').each(function () {
                $(this).closest('tr').find('input').removeAttr('disabled');
            });
            $('#validate_error').css('display', 'none');
            $('#btnSave').removeAttr('disabled');
            $('#btnOpenItemDialog').removeAttr('disabled');
            $('#btnDelete').removeAttr('disabled');
        } else {
            $('#validate_error').text('New Price must greater than 0 and not none');
            $('#validate_error').removeAttr('style');
            currentRow.parents('tr').attr('style', 'background-color: red !important');
            $('#btnSave').attr('disabled', true);
            $('#btnOpenItemDialog').attr('disabled', true);
            $('#btnDelete').attr('disabled', true);
            $('#dynamic-table tr.gradeX').each(function () {
                $(this).closest('tr').find('input').not(currentRow[9]).attr('disabled', true);
            });
        }
    });
});
