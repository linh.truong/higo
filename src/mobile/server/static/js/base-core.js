/**
 * Created by trinh.dinh.tinh on 11/9/2016.
 */


/* Base core function ---*/
$(".cms_form_number").keydown(function (event) {
    // Allow: backspace, delete, tab, escape, and enter
    if (event.keyCode == 46 || event.keyCode == 8 || event.keyCode == 9 || event.keyCode == 27 || event.keyCode == 13 ||
        // Allow: Ctrl+A
        (event.keyCode == 65 && event.ctrlKey === true) ||
        // Allow: home, end, left, right
        (event.keyCode >= 35 && event.keyCode <= 39)) {
        // let it happen, don't do anything
        return;
    }
    else {
        // Ensure that it is a number and stop the keypress
        if (event.shiftKey || (event.keyCode < 48 || event.keyCode > 57) && (event.keyCode < 96 || event.keyCode > 105 )) {
            event.preventDefault();
        }
    }
});

$(".cms_form_decimal").keydown(function (event) {
    // Allow: backspace, delete, tab, escape, and enter
    if (event.keyCode == 46 || event.keyCode == 8 || event.keyCode == 9 || event.keyCode == 27 || event.keyCode == 13 ||
        // Allow: Ctrl+A
        (event.keyCode == 65 && event.ctrlKey === true) ||
        // Allow: .
        (event.keyCode == 190) ||
        // Allow: home, end, left, right
        (event.keyCode >= 35 && event.keyCode <= 39)) {
        // let it happen, don't do anything
    }
    else {
        // Ensure that it is a number and stop the keypress
        if (event.shiftKey || (event.keyCode < 48 || event.keyCode > 57) && (event.keyCode < 96 || event.keyCode > 105 )) {
            event.preventDefault();
        }
    }

    // If a decimal has been added, disable the "."-button
    if ($(this).val().indexOf('.') !== -1 && event.keyCode == 190) {
        event.preventDefault();
    } else if ($(this).val().length === 0 && event.keyCode == 190) {
        event.preventDefault();
    }
});


/*------------------------------------------ SELECT 2 COMMON------------------------*/
function updateSelectNoDefault(data, name) {
    // The response comes back as a bunch-o-JSON
    var objs = eval(JSON.stringify(data)); // evaluate JSON
    var rselect = document.getElementById(name);
    // Clear all previous options
    var l = rselect.length;
    while (l > 0) {
        l--;
        rselect.remove(l);
    }
    if (objs) {
        // Rebuild the select
        for (var i = 0; i < objs.length; i++) {
            var obj = objs[i];
            var opt = document.createElement('option');
            opt.text = obj.name;
            opt.value = obj.id;
            try {
                rselect.add(opt, null); // standards compliant; doesn't work in
                // IE
            } catch (ex) {
                rselect.add(opt); // IE only
            }
        }
    }
}


// Update select2 default value
function updateSelectNoDefault2(data, name, value) {
    // The response comes back as a bunch-o-JSON
    var objs = eval(JSON.stringify(data)); // evaluate JSON
    var rselect = document.getElementById(name);
    // Clear all previous options
    var l = rselect.length;
    while (l > 0) {
        l--;
        rselect.remove(l);
    }
    if (!objs) {
    } else {
        // Rebuild the select
        for (var i = 0; i < objs.length; i++) {
            var obj = objs[i];
            var opt = document.createElement('option');
            opt.text = obj.label;
            opt.value = obj.value;
            if (opt.value == value) {
                opt.selected = true;
            }
            try {
                rselect.add(opt, null); // standards compliant; doesn't work in
                // IE
            } catch (ex) {
                rselect.add(opt); // IE only
            }
        }
    }
}


/* update select 2 */
function updateSelect2Default(data, name, value) {
    // The response comes back as a bunch-o-JSON
    var objs = eval(JSON.stringify(data)); // evaluate JSON
    var rselect = document.getElementById(name);
    // Clear all previous options
    var l = rselect.length;
    while (l > 0) {
        l--;
        rselect.remove(l);
    }
    var firstOpt = document.createElement('option');
    firstOpt.text = '';
    firstOpt.value = null;
    try {
        rselect.add(firstOpt, null) // standards compliant; doesn't work in
        // IE
    } catch (ex) {
        rselect.add(firstOpt) // IE only
    }
    if (objs) {
        // Rebuild the select
        for (var i = 0; i < objs.length; i++) {
            var obj = objs[i];
            var opt = document.createElement('option');
            opt.text = obj.name;
            opt.value = obj.id;
            if (opt.value == value) {
                opt.selected = true;
            }
            try {
                rselect.add(opt, null) // standards compliant; doesn't work in
                // IE
            } catch (ex) {
                rselect.add(opt) // IE only
            }
        }
    }
}


function updateSelectDefault(data, name, value) {
    // The response comes back as a bunch-o-JSON
    var objs = eval(JSON.stringify(data)); // evaluate JSON
    var rselect = document.getElementById(name);
    // Clear all previous options
    var l = rselect.length;
    while (l > 0) {
        l--;
        rselect.remove(l);
    }
    var firstOpt = document.createElement('option');
    firstOpt.text = '';
    firstOpt.value = '-1';
    try {
        rselect.add(firstOpt, null) // standards compliant; doesn't work in
        // IE
    } catch (ex) {
        rselect.add(firstOpt) // IE only
    }
    if (objs) {
        // Rebuild the select
        for (var i = 0; i < objs.length; i++) {
            var obj = objs[i];
            var opt = document.createElement('option');
            opt.text = obj.name;
            opt.value = obj.id;
            if (opt.value == value) {
                opt.selected = true
            }
            try {
                rselect.add(opt, null) // standards compliant; doesn't work in
                // IE
            } catch (ex) {
                rselect.add(opt) // IE only
            }
        }
    }
}


function updateSelect2Noblank(data, name, value) {
    // The response comes back as a bunch-o-JSON
    var objs = eval(JSON.stringify(data)); // evaluate JSON
    var rselect = document.getElementById(name);
    // Clear all previous options
    var l = rselect.length;
    while (l > 0) {
        l--;
        rselect.remove(l);
    }
    if (objs) {
        // Rebuild the select
        for (var i = 0; i < objs.length; i++) {
            var obj = objs[i];
            var opt = document.createElement('option');
            opt.text = obj.name;
            opt.value = obj.id;
            if (opt.value == value) {
                opt.selected = true;
            }
            try {
                rselect.add(opt, null) // standards compliant; doesn't work in
                // IE
            } catch (ex) {
                rselect.add(opt) // IE only
            }
        }
    }
}


function switchEditMode(idButton, idObj) {

    var _button = $('#' + idButton);
    var _object = $('#' + idObj);

    if (_button.text().trim() == 'Enable') {
        _object.prop('readonly', false);
        _button.text('Disable');

    } else {
        _object.prop('readonly', true);
        _button.text('Enable');
    }
}


/* Disabled object by id

 params: id  or array id
 Example:
 isDisabled('id_email');
 or
 _disable_lst = ["id_email", "id_current_point"];
 isDisabled(_disable_lst);
 */
function isDisabled(list_id) {

    if (list_id instanceof Array) {
        list_id.forEach(function (element) {
            var _object = $('#' + element);
            _object.prop('readonly', true);
        });
    } else {
        var _object = $('#' + list_id);
        _object.prop('readonly', true);
    }

}
