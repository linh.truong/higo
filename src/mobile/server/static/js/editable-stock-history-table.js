var EditableTable = function () {

    return {
        //main function to initiate the module
        init: function () {
            function restoreRow(oTable, nRow) {
                var aData = oTable.fnGetData(nRow);
                var jqTds = $('>td', nRow);

                for (var i = 0, iLen = jqTds.length; i < iLen; i++) {
                    oTable.fnUpdate(aData[i], nRow, i, false);
                }

                oTable.fnDraw();
            }

            function editRow(oTable, nRow) {                
                var aData = oTable.fnGetData(nRow);
                var jqTds = $('>td', nRow);

                for (i = 0; i < jqTds.length; i++) {
                    var spanData = jqTds[i].getElementsByTagName("span").item(0);
                    if (spanData != null) {
                        var inputData = spanData.getElementsByTagName("input").item(0);
                        var transData = $.trim(aData[i].split('<span')[0])
                        if (inputData != null) {
                            inputData.setAttribute("value", transData);
                        }
                        else {
                            selectData = spanData.getElementsByTagName("option");
                            for (x = 0; x < selectData.length; x++) {
                                var optionData = selectData.item(x);
                                if ($.trim(optionData.innerText) == transData) {
                                    optionData.setAttribute("selected", "selected");
                                    selectData.item(0).setAttribute("selected", "");
                                }
                            }
                        }
                        jqTds[i].innerHTML = spanData.innerHTML;
                    }
                }

                if (jqTds[3].getElementsByTagName("input").item(0) != null) {
                    jqTds[3].innerHTML = jqTds[3].getElementsByTagName("input").item(0).outerHTML;
                }
                jqTds[3].innerHTML = jqTds[3].innerHTML + '<a class="edit fa fa-save btn btn-success btn-xs" href="">Save</a>' +
                    '<a class="cancel fa fa-times btn btn-default btn-xs" href="">Cancel</a>';
                // $('#btnEditSave').removeAttr("style");
                // $('#btnEditCancel').removeAttr("style");
                // $('#btnEditRow').css("display", "none");
                // $('#btnDeleteRow').css("display", "none");
            }

            function saveRow(oTable, nRow) {

                var jqInputs = $('input', nRow);
                var jqSelects = $('select', nRow);
                var jqTds = $('>td', nRow);

                $.ajax({
                    method: "POST",
                    url: '/items/stock_history/edit/' + jqInputs[2].value + '/',
                    dataType: 'JSON',
                    data: {
                        'csrfmiddlewaretoken': $('input[name="csrfmiddlewaretoken"]').val(),
                        'quantity': jqInputs[0].value,
                        'stock_date': jqInputs[1].value,
                    },
                    responseTime: 200,
                    success: function (json) {
                        // if (json == 'Success') {
                        //     $('#lblMessage')[0].innerText = '';
                        //     window.location.reload(true);
                        // }
                        // else {
                        //     window.location.reload(true);
                        //     $('#lblMessage')[0].innerText = json;
                        //     //cancelEditRow(oTable, nRow);
                        // }
                        window.location.reload(true);
                    },
                });

                var jqTds_innerHTML = '';
                 if (jqTds[3].getElementsByTagName("input").item(0) != null) {
                    jqTds_innerHTML = jqTds[3].getElementsByTagName("input").item(0).outerHTML;
                }
                jqTds_innerHTML += '<a class="edit fa fa-edit btn btn-primary btn-xs" href="javascript:;"></a>'
                jqTds_innerHTML += '<a style="min-width: 20px!important;" class="delete fa fa-bitbucket btn btn-danger btn-xs" href="javascript:;"></a>'

                oTable.fnUpdate(jqInputs[0].value, nRow, 1, false);//transaction_date
                oTable.fnUpdate(jqInputs[1].value, nRow, 2, false);//number
                oTable.fnUpdate(jqTds_innerHTML, nRow, 3, false);
                oTable.fnDraw();
            }

            function cancelEditRow(oTable, nRow) {
                var jqInputs = $('input', nRow);
                oTable.fnUpdate(jqInputs[0].value, nRow, 1, false);
                oTable.fnUpdate(jqInputs[1].value, nRow, 2, false);
                oTable.fnUpdate(jqInputs[2].value, nRow, 3, false);
                oTable.fnUpdate('<a class="edit" href="">Edit</a>', nRow, 3, false);
                oTable.fnDraw();
            }

            var oTable = $('#editable-sample').dataTable({
                "aLengthMenu": [
                    [5, 15, 20, -1],
                    [5, 15, 20, "All"] // change per page values here
                ],
                // set the initial value
                "iDisplayLength": 5,
                "sDom": "<'row'<'col-lg-6'l><'col-lg-6'f>r>t<'row'<'col-lg-6'i><'col-lg-6'p>>",
                "sPaginationType": "bootstrap",
                "oLanguage": {
                    "sLengthMenu": "_MENU_ records per page",
                    "oPaginate": {
                        "sPrevious": "Prev",
                        "sNext": "Next"
                    }
                },
                "aoColumnDefs": [{
                    'bSortable': false,
                    'aTargets': [0]
                }
                ]
            });

            jQuery('#editable-sample_wrapper .dataTables_filter input').addClass("form-control medium"); // modify table search input
            jQuery('#editable-sample_wrapper .dataTables_length select').addClass("form-control xsmall"); // modify table per page dropdown

            var nEditing = null;

            $('#editable-sample_new').click(function (e) {
                e.preventDefault();
                var aiNew = oTable.fnAddData(['', '', '', '',
                    '<a class="edit" href="">Edit</a>', '<a class="cancel" data-mode="new" href="">Cancel</a>'
                ]);
                var nRow = oTable.fnGetNodes(aiNew[0]);
                editRow(oTable, nRow);
                nEditing = nRow;
            });

            $('#editable-sample a.delete').live('click', function (e) {
                e.preventDefault();

                $('#delete-transaction-dialog').modal('show');
                var frDelete = $('#delete-transaction-form')[0];
                var nRow = $(this).parents('tr')[0];
                var jqInputs = $('input', nRow);
                var hd_order_type = $('#hd_order_type')[0].value;
                frDelete.setAttribute('action', '/items/stock_history/delete/' + jqInputs[2].value + '/' + hd_order_type + '/');

            });

            $('#editable-sample a.cancel').live('click', function (e) {
                e.preventDefault();
                if ($(this).attr("data-mode") == "new") {
                    var nRow = $(this).parents('tr')[0];
                    oTable.fnDeleteRow(nRow);
                } else {
                    restoreRow(oTable, nEditing);
                    nEditing = null;
                }
            });

            $('#editable-sample a.edit').live('click', function (e) {
                e.preventDefault();
                $('#btnRemoveHistory').trigger('click');
                /* Get the row as a parent of the link that was clicked on */
                var nRow = $(this).parents('tr')[0];

                if (nEditing !== null && nEditing != nRow) {
                    /* Currently editing - but not this row - restore the old before continuing to edit mode */
                    restoreRow(oTable, nEditing);
                    editRow(oTable, nRow);
                    nEditing = nRow;
                } else if (nEditing == nRow && this.innerHTML == "Save") {
                    /* Editing this row and want to save it */
                    saveRow(oTable, nEditing);
                    nEditing = null;
                } else {
                    /* No edit in progress - let's start one */
                    editRow(oTable, nRow);
                    nEditing = nRow;
                }
            });
        }

    };

}();