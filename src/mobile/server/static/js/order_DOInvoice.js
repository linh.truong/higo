/**
 * Created by tho.pham on 5/26/2016.
 */
//Pagination and search item
$(document).ready(function () {
    //Search
    // $('#search').on('keyup', function (e) {
    //     $('#tblData').dataTable().fnFilter($(this).val());
    // });
    //pagination
    // $('#tblData').dataTable({
    //     "aaSorting": [[0, "desc"]],
    //     "bFilter": false,
    //     "bLengthChange": false,
    //     "iDisplayLength": 5,
    // });
    debugger;
    calculateTotal();
    var copy_id = $('#copy_id').text();
    var selector = $('#dynamic-table tr.gradeX');
    var customer = $('#hdCustomerId').val();
    if ((customer != null && copy_id == '') || (customer != null && copy_id == '0')) {
        var count_row = 0;
        selector.each(function () {
            currentRow = $(this).closest('tr').find('input');
            currentLabel = $(this).closest('tr').find('label');
            count_row += 1;
            currentRow[0].value = count_row;
            currentLabel[0].textContent = currentRow[0].value;
            currentLabel[0].textContent = currentRow[0].value;
        });
        fnEnableButton();
    } else {
        // validation quantity of items
        $('#btnOpenItemDialog').css('display', 'none');
        var items_name_list = [];
        selector.each(function () {
            currentRow = $(this).closest('tr').find('input');
            var quantity_do = currentRow[5].value;
            var quantity_delivery = parseInt(quantity_do) + parseInt(currentRow[19].value);
            if (parseInt(quantity_do) < 0) {
                $('#minimum_order_error').removeAttr('style');
                $('#minimum_order_error').text('The quantity of product must greater than 0');
                $(this).closest('tr').attr('style', 'background-color: red !important');
                currentRow[8].value = 0;
                fnDisableButton();
            } else if (parseInt(quantity_do) == 0) {
                items_name_list.push(currentRow[2].value);
                $(this).closest('tr').attr('style', 'background-color: aqua !important');
                $('#' + currentRow[5].id).attr('disabled', true);
                currentRow[8].value = 0;
                fnDisableButton();
            }
        });
        var uniqueNames = [];
        $.each(items_name_list, function (i, el) {
            if ($.inArray(el, uniqueNames) === -1) uniqueNames.push(el);
        });
        if (uniqueNames.length > 0) {
            $('#minimum_order_error').removeAttr('style');
            $('#minimum_order_error').text('The products ' + uniqueNames + ' were delivered.');
        }
    }
});

$(document).keypress(function (e) {
    if (e.which == 13 && !$(event.target).is("textarea")) {
        e.preventDefault();
    }
});


//Add Extra Label Value formset
$(document).ready(function () {
    checkDisplay();
    // var customer = $('#hdCustomerId').val();
    // if (customer == null) {
    //     fnDisableButton();
    // } else {
    //     fnEnableButton();
    // }

    $('#add_more_right').click(function () {
        cloneMore('div.table-right:last', 'formset_right');
    });
    $('#add_more_left').click(function () {
        cloneMore('div.table-left:last', 'formset_left');
    });
    $('#add_more_code').click(function () {
        cloneMore('div.table-code:last', 'formset_code');
    });
    function cloneMore(selector, type) {
        var display = $(selector).css("display")
        if (display == 'none') {
            $(selector).removeAttr("style")
        }
        else {
            var total = $('#id_' + type + '-TOTAL_FORMS').val();
            var newElement = $(selector).clone(true);
            newElement.removeAttr("style")
            newElement.find(':input').each(function () {
                var name = $(this).attr('name').replace('-' + (total - 1) + '-', '-' + total + '-');
                var id = 'id_' + name;
                $(this).attr({'name': name, 'id': id}).val('').removeAttr('checked');
            });
            newElement.find('label').each(function () {
                var newFor = $(this).attr('for').replace('-' + (total - 1) + '-', '-' + total + '-');
                $(this).attr('for', newFor);
            });
            total++;
            $('#id_' + type + '-TOTAL_FORMS').val(total);
            $(selector).after(newElement);
        }
    }

    function checkDisplay() {
        var request_method = $('#request_method').val();
        if (request_method == 'GET') {
            if ($('#id_formset_right-TOTAL_FORMS').val() > 1) {
                $('div.table-right:last').remove();
                $('#id_formset_right-TOTAL_FORMS').val($('#id_formset_right-TOTAL_FORMS').val() - 1);
            } else {
                $('div.table-right:last').css("display", "none");
            }
            if ($('#id_formset_left-TOTAL_FORMS').val() > 1) {
                $('div.table-left:last').remove();
                $('#id_formset_left-TOTAL_FORMS').val($('#id_formset_left-TOTAL_FORMS').val() - 1);
            } else {
                $('div.table-left:last').css("display", "none");
            }
            if ($('#id_formset_code-TOTAL_FORMS').val() > 1) {
                $('div.table-code:last').remove();
                $('#id_formset_code-TOTAL_FORMS').val($('#id_formset_code-TOTAL_FORMS').val() - 1);
            } else {
                $('div.table-code:last').css("display", "none");
            }
            $('#dynamic-table tr.gradeX:last').css("display", "none");
            $('#items_error').css("display", "none");
        } else if (request_method == 'POST') {
            var right_label = $('#id_formset_right-0-label').val();
            var right_somevalue = $('#id_formset_right-0-value').val();
            if (right_label == "" && right_somevalue == "") {
                $('div.table-right:last').css("display", "none");
            }
            var left_label = $('#id_formset_left-0-label').val();
            var left_somevalue = $('#id_formset_left-0-value').val();
            if (left_label == "" && left_somevalue == "") {
                $('div.table-left:last').css("display", "none");
            }
            var code_label = $('#id_formset_code-0-label').val();
            var code_somevalue = $('#id_formset_code-0-value').val();
            if (code_label == "" && code_somevalue == "") {
                $('div.table-code:last').css("display", "none");
            }
            var item = $('#id_formset_item-0-item').val();
            var quantity = $('#id_formset_item-0-quantity').val();
            var price = $('#id_formset_item-0-price').val();
            var amount = $('#id_formset_item-0-amount').val();
            if (quantity == "" && price == "" && amount == "") {
                $('#dynamic-table tr.gradeX:last').css("display", "none");
                $('#items_error').removeAttr('style');
            } else $('#items_error').css("display", "none");
        }
    }

    $(document).on('click', "[class^=removerow-left]", function () {
        if ($('#id_formset_left-TOTAL_FORMS').val() == 1) {
            var total = $('#id_formset_left-TOTAL_FORMS').val();
            $('div.table-left:last').find(':input').each(function () {
                var name = $(this).attr('name').replace('-' + total + '-', '-' + (total - 1) + '-');
                var id = 'id_' + name;
                $(this).attr({'name': name, 'id': id}).val('').removeAttr('value');
            });
            $(this).parents("div.table-left:last").css("display", "none")
        } else {
            var minus = $('input[name=formset_left-TOTAL_FORMS]').val() - 1;
            $('#id_formset_left-TOTAL_FORMS').val(minus);
            $(this).parents("div.table-left").remove();
            var i = 0;
            $('div.table-left').each(function () {
                var $tds = $(this).find('input');
                var label = $tds[0].name;
                var value = $tds[1].name;
                if (label.replace(/[^\d.]/g, '') == 0) {
                    i++;
                } else {
                    for (i; i < minus; i++) {
                        $tds[0].name = label.replace(/\d+/g, i);
                        $tds[0].id = 'id_' + $tds[0].name;
                        $tds[1].name = value.replace(/\d+/g, i);
                        $tds[1].id = 'id_' + $tds[1].name;
                        i++;
                        break;
                    }
                }
            });
        }
    });

    $(document).on('click', "[class^=removerow-right]", function () {
        if ($('#id_formset_right-TOTAL_FORMS').val() == 1) {
            var total = $('#id_formset_right-TOTAL_FORMS').val();
            $('div.table-right:last').find(':input').each(function () {
                var name = $(this).attr('name').replace('-' + total + '-', '-' + (total - 1) + '-');
                var id = 'id_' + name;
                $(this).attr({'name': name, 'id': id}).val('').removeAttr('value');
            });
            $(this).parents("div.table-right").css("display", "none");
        } else {
            var minus = $('input[name=formset_right-TOTAL_FORMS]').val() - 1;
            $('#id_formset_right-TOTAL_FORMS').val(minus);
            $(this).parents("div.table-right").remove();
            var i = 0;
            $('div.table-right').each(function () {
                var $tds = $(this).find('input');
                var label = $tds[0].name;
                var value = $tds[1].name;
                if (label.replace(/[^\d.]/g, '') == 0) {
                    i++;
                } else {
                    for (i; i < minus; i++) {
                        $tds[0].name = label.replace(/\d+/g, i);
                        $tds[0].id = 'id_' + $tds[0].name;
                        $tds[1].name = value.replace(/\d+/g, i);
                        $tds[1].id = 'id_' + $tds[1].name;
                        i++;
                        break;
                    }
                }
            });
        }
    });

    $(document).on('click', "[class^=removerow-code]", function () {
        if ($('#id_formset_code-TOTAL_FORMS').val() == 1) {
            var total = $('#id_formset_code-TOTAL_FORMS').val();
            $('div.table-code:last').find(':input').each(function () {
                var name = $(this).attr('name').replace('-' + total + '-', '-' + (total - 1) + '-');
                var id = 'id_' + name;
                $(this).attr({'name': name, 'id': id}).val('').removeAttr('value');
            });
            $(this).parents("div.table-code").css("display", "none")
        } else {
            var minus = $('input[name=formset_code-TOTAL_FORMS]').val() - 1;
            $('#id_formset_code-TOTAL_FORMS').val(minus);
            $(this).parents("div.table-code").remove();
            var i = 0;
            $('div.table-code').each(function () {
                var $tds = $(this).find('input');
                var label = $tds[0].name;
                var value = $tds[1].name;
                if (label.replace(/[^\d.]/g, '') == 0) {
                    i++;
                } else {
                    for (i; i < minus; i++) {
                        $tds[0].name = label.replace(/\d+/g, i);
                        $tds[0].id = 'id_' + $tds[0].name;
                        $tds[1].name = value.replace(/\d+/g, i);
                        $tds[1].id = 'id_' + $tds[1].name;
                        i++;
                        break;
                    }
                }
            });
        }
    });
});


//Load tax rate
$(document).ready(function () {
    $('#id_tax').change(function () {
        var taxid = parseInt($(this).val());
        if (isNaN(taxid)) {
            $('#id_tax_amount').val(0);
            $('#id_total').val(parseFloat($('#id_tax_amount').val()) - parseFloat($('#id_discount').val()) + parseFloat($('#id_subtotal').val()));
        } else {
            $.ajax({
                method: "POST",
                url: '/orders/load_tax/',
                dataType: 'JSON',
                data: {
                    'csrfmiddlewaretoken': $('input[name="csrfmiddlewaretoken"]').val(),
                    'tax_id': taxid,
                },
                success: function (json) {
                    var tax_amount = (parseFloat(json) * parseFloat($('#id_subtotal').val())) / 100;
                    $('#id_tax_amount').val(tax_amount.toFixed(2));
                    var total = parseFloat($('#id_subtotal').val()) + tax_amount - parseFloat($('#id_discount').val());
                    $('#id_total').val(total.toFixed(2));
                }
            });
        }
    });
});

//Load and edit inline Customer information
$(document).ready(function () {
    var hdCustomerId = $('#hdCustomerId').val();
    loadCustomerInfo(hdCustomerId);

    var callback = function () {
        $.ajax({
            method: "POST",
            url: '/orders/customer_search_by_code/',
            dataType: 'JSON',
            data: {
                'csrfmiddlewaretoken': $('input[name="csrfmiddlewaretoken"]').val(),
                'customer_code': $("#form_customer_code").val()
            },
            responseTime: 200,
            response: function (settings) {
                if (settings.data.value) {
                    this.responseText = '{"success": true}';
                }
                else {
                    this.responseTime = '{"success: false, "msg": "required"}';
                }
            },
            success: function (json) {
                console.log(json);
                $('#hdCustomerId').val(json['id']);
                $("#form_customer_code").val(json['code']);
                $('#customer_name').editable('destroy');
                $('#customer_address').editable('destroy');
                $('#customer_email').editable('destroy');

                $('#customer_name').attr('data-pk', json['id']);
                $('#customer_name').text(json['name']);
                $('#customer_address').text(json['address']);

                $('#customer_email').attr('data-pk', json['id']);
                $('#customer_email').text(json['email']);
                $('#customer_payment_term').text('Payment Term: ' + json['term'] + ' days');
                $('#customer_payment_mode').text('Payment Mode: ' + json['payment_mode']);
                $('#customer_credit_limit').text('Credit Limit: ' + json['credit_limit']);
                loadCustomerInfo(json['id']);
                $('#id_tax').find('option').removeAttr("selected");
                $('#id_tax').find('option').removeAttr("disabled");
                $('#id_tax').find('option[value="' + json['tax_id'] + '"]').attr("selected", "selected");
                $('#id_tax').val(json['tax_id']);
                $('#id_tax option:not(:selected)').attr('disabled', true);
                // load tax again
                var taxid = parseInt($('#id_tax').val());
                if (isNaN(taxid)) {
                    $('#id_tax_amount').val(0);
                    $('#id_total').val(parseFloat($('#id_tax_amount').val()) - parseFloat($('#id_discount').val()) + parseFloat($('#id_subtotal').val()));
                } else {
                    $.ajax({
                        method: "POST",
                        url: '/orders/load_tax/',
                        dataType: 'JSON',
                        data: {
                            'csrfmiddlewaretoken': $('input[name="csrfmiddlewaretoken"]').val(),
                            'tax_id': taxid,
                        },
                        success: function (json) {
                            var tax_amount = (parseFloat(json) * parseFloat($('#id_subtotal').val())) / 100;
                            $('#id_tax_amount').val(tax_amount.toFixed(2));
                            var total = parseFloat($('#id_subtotal').val()) + tax_amount - parseFloat($('#id_discount').val());
                            $('#id_total').val(total.toFixed(2));
                        }
                    });
                }
                $('#id_currency').find('option').removeAttr('selected');
                $('#id_currency').find('option').removeAttr('disabled');
                $('#id_currency option[value=' + json['currency_id'] + ']').attr('selected', 'selected');
                $('#id_currency').val(json['currency_id']);
                $('#id_currency option:not(:selected)').attr('disabled', true);
            }
        })
    };
    $("#form_customer_code").keypress(function (e) {
        if (e.which == 13) {
            e.preventDefault();
            callback();
        }
    });
    $('#btnSearchCustomer').on('click', function () {
        var datatbl = $('#customer-table').DataTable();
        datatbl.destroy();
        $('#customer-table').dataTable({
            "iDisplayLength": 5,
            "bLengthChange": false,
            "order": [[0, "desc"]],
            "serverSide": true,
            "ajax": {
                "url": "/orders/customers_list_as_json/"
            },
            "columns": [
                {"data": "code", "sClass": "text-left"},
                {"data": "name", "sClass": "text-left"},
                {"data": "payment_term", "sClass": "text-left"},
                {"data": "payment_mode", "sClass": "text-left"},
                {"data": "credit_limit", "sClass": "text-left"},
                {
                    "orderable": false,
                    "data": null,
                    "render": function (data, type, full, meta) {
                        return '<input type="radio" name="choices" id="' +
                            full.id + '" class="call-checkbox" value="' + full.id + '">';
                    }
                }
            ]
        });
    });

    $('#btnCustomerSelect').on('click', function () {
        var customer_select_id = $("input[name='choices']:checked").attr('id');

        $('#hdCustomerId').val(customer_select_id);

        $('#id_currency option[value=' + customer_select_id + ']').attr('selected', 'selected');

        var nRow = $("input[name='choices']:checked").parents('tr')[0];
        var jqInputs = $('td', nRow);
        $("#form_customer_code").val(jqInputs[0].innerText);

        $(this).attr('data-dismiss', 'modal');
        callback();
    });

    function loadCustomerInfo(hdCustomerId) {
        $.fn.editable.defaults.mode = 'inline';

        //make status editable
        $('#customer_name').editable({
            type: 'text',
            pk: hdCustomerId,
            url: '/orders/change_customer/' + hdCustomerId + '/',
            title: 'Enter customer name',
            success: function (response, newValue) {
                if (!response) {
                    return "Unknown error!"
                }
                if (response.success === false) {
                    return respond.msg;
                }
            }
        });

        $('#customer_address').editable({
            type: 'text',
            pk: hdCustomerId,
            url: '/orders/change_customer/' + hdCustomerId + '/',
            title: 'Enter customer address',
            success: function (response, newValue) {
                if (!response) {
                    return "Unknown error!"
                }
                if (response.success === false) {
                    return respond.msg;
                }
            }
        });

        $('#customer_email').editable({
            type: 'text',
            pk: hdCustomerId,
            url: '/orders/change_customer/' + hdCustomerId + '/',
            title: 'Enter customer email',
            validate: function (value) {
                var valid = valib.String.isEmailLike(value)
                if (valid == false) return 'Please insert valid email'
            },
            success: function (response, newValue) {
                if (!response) {
                    return "Unknown error!"
                }
                if (response.success === false) {
                    return respond.msg;
                }
            }
        });
    };

    // $('#id_customer').change(function () {
    //     var customerid = parseInt($(this).val());
    //     console.log(customerid);
    //     $.ajax({
    //         method: "POST",
    //         url: '/orders/customer/',
    //         dataType: 'JSON',
    //         data: {
    //             'csrfmiddlewaretoken': $('input[name="csrfmiddlewaretoken"]').val(),
    //             'customer_id': customerid,
    //         },
    //         responseTime: 200,
    //         response: function (settings) {
    //             if (settings.data.value) {
    //                 this.responseText = '{"success": true}';
    //             } else {
    //                 this.responseText = '{"success": false, "msg": "required"}';
    //             }
    //         },
    //         success: function (json) {
    //             console.log(json);
    //             $('#hdCustomerId').val(json['id']);
    //
    //             $('#customer_name').editable('destroy');
    //             $('#customer_address').editable('destroy');
    //             $('#customer_email').editable('destroy');
    //
    //             $('#customer_name').attr('data-pk', json['id']);
    //             $('#customer_name').text(json['name']);
    //             $('#customer_address').text(json['address']);
    //             $('#customer_email').attr('data-pk', json['id']);
    //             $('#customer_email').text(json['email']);
    //
    //             $('#customer_payment_term').text('Payment Term: ' + json['term'] + ' days');
    //             $('#customer_payment_mode').text('Payment Mode: ' + json['payment_mode']);
    //             $('#customer_credit_limit').text('Credit Limit: ' + json['credit_limit']);
    //             loadCustomerInfo(json['id']);
    //         }
    //     });
    // });
});
//Event check checkbox
$('input[type=checkbox]').click(function () {
    if ($(this).is(':checked'))
        $(this).attr('checked', 'checked');
    else
        $(this).removeAttr('checked');
});
//Add order item
$(document).ready(function () {
    if ($('#id_formset_item-TOTAL_FORMS').val() > 1) {
        var display = $('#dynamic-table tr.gradeX:last').css("display");
        if (display == 'none') {
            $('#dynamic-table tr.gradeX:last').removeAttr("style");
            $('#dynamic-table tr.gradeX:last').remove();
            $('#id_formset_item-TOTAL_FORMS').val($('#id_formset_item-TOTAL_FORMS').val() - 1);
        }
    }

    //return false;
    $('#btnAddItems').on('click', function () {
        var allVals = [];
        var table = $('#tblData').DataTable();
        var rowcollection = table.$(".call-checkbox:checked", {"page": "all"});
        rowcollection.each(function (index, elem) {
            var row = table.row('#' + elem.id).data();
            console.log(row.currency_id + ' ' + row.currency_code);
            allVals.push({
                id: row.item_id, //Item ID
                price: $(elem).val(),
                item_code: row.part_no, //Item Code
                name: row.item_name, //Item Name
                refer_number: row.refer_number,
                refer_line: row.refer_line,
                supplier_code: row.supplier_code,
                location_code: row.location_code,
                part_gp: row.part_gp,
                unit_price: row.sales_price,
                currency: row.currency_code,
                location_id: row.location_id,
                currency_id: row.currency_id,
                uom: row.unit,
                supplier_id: row.supplier_id,
                order_quantity: row.order_qty,
                delivery_quantity: row.delivery_qty,
                customer_po_no: row.customer_po_no
            });
            table.row('#' + elem.id).node().setAttribute("style", "display: none");
        });
        if (allVals.length > 0) {
            cloneMore('#dynamic-table tr.gradeX:last', 'formset_item', allVals);
            $('input[checked=checked]').each(function () {
                $(this).removeAttr('checked');
            });
        }
        $(this).attr('data-dismiss', 'modal');
        $('#items_error').css('display', 'none');
        //Change currency
        var currency_id = parseInt($('#id_currency option:selected').val());
        var currency_name = $('#id_currency option:selected').text();
        var arrItems = [];
        $('#dynamic-table tr.gradeX').each(function () {
            currentRow = $(this).closest('tr').find('input');
            arrItems.push({
                item_id: currentRow[3].value,
                currency_id: currentRow[10].value
            });
        });
        changeCurrency(arrItems, currency_id, currency_name);
        fnEnableButton();
        // validationItemsFormset();
    });
    // insert orderitem by SO num
    function addOrderItemBySO(so_number) {
        var exclude_item_array = [];
        var exclude_item_list = {};
        var hdCustomerId = $('#hdCustomerId').val();

        $('#dynamic-table tr.gradeX').each(function () {
            var display = $(this).css("display");
            currentRow = $(this).closest('tr').find('input');
            if (display != 'none') {
                exclude_item_array.push(currentRow[3].value);
            }
        });
        if (exclude_item_array.length > 0) {
            exclude_item_list = JSON.stringify(exclude_item_array);
        }
        $.ajax({
            method: "POST",
            url: '/orders/get_orderitems_by_so_no/',
            dataType: 'JSON',
            data: {
                'csrfmiddlewaretoken': $('input[name="csrfmiddlewaretoken"]').val(),
                'so_number': so_number,
                'customer_id': hdCustomerId,
                'exclude_item_list': exclude_item_list
            },
            success: function (json) {
                console.log(json);
                var allVals = [];
                $.each(json, function (i, item) {
                    allVals.push({
                        id: item.item_id, //Item ID
                        price: item.sales_price,
                        item_code: item.item_code, //Item Code
                        name: item.item_name, //Item Name
                        refer_number: item.refer_number,
                        refer_line: item.refer_line,
                        supplier_code: item.supplier_code,
                        location_code: item.location_code,
                        part_gp: item.part_gp,
                        unit_price: item.sales_price,
                        currency: item.currency_code,
                        location_id: item.location_id,
                        currency_id: item.currency_id,
                        uom: item.uom,
                        supplier_id: item.supplier_id,
                        order_quantity: item.quantity,
                        delivery_quantity: item.delivery_quantity,
                        customer_po_no: item.customer_po_no
                    });
                });
                cloneMore('#dynamic-table tr.gradeX:last', 'formset_item', allVals);
                //Change currency
                var currency_id = parseInt($('#id_currency option:selected').val());
                var currency_name = $('#id_currency option:selected').text();
                var arrItems = [];
                $('#dynamic-table tr.gradeX').each(function () {
                    currentRow = $(this).closest('tr').find('input');
                    arrItems.push({
                        item_id: currentRow[3].value,
                        currency_id: currentRow[10].value
                    });
                });
                changeCurrency(arrItems, currency_id, currency_name);
                fnEnableButton();
                // set customer code
                $('#form_customer_code').val(json[0]['customer_code']);
                // change customer according to the sales order
                var e = jQuery.Event("keypress");
                e.which = 13;
                $("#form_customer_code").trigger(e);
            }
        });
    }

    $('#txtSONo').on('keypress', function (e) {
        if (e.which == 13) {
            addOrderItemBySO($('#txtSONo').val());
            $('#items_error').css('display', 'none');
        }
    });

    function cloneMore(selector, type, allVals) {
        debugger;
        var display = $(selector).css("display");
        var order_type = $('#order_type').text();
        var sum = 0;
        var i = 0;
        var item_id = 0;
        // if no item in orderitem table
        if (display == 'none') {
            //show first row of table and set Item, Price of dialog
            $(selector).removeAttr("style")
            // $(selector).find("option").each(function () {
            //     $(this).removeAttr('selected');
            //     if ($(this).val() == allVals[i].id) {
            //         $(this).prop("selected", true);
            //         // $(this).attr("selected", true);
            //     }
            // });
            $(selector).find('label').each(function () {
                var name = $(this).attr('name').replace('-' + (total - 1) + '-', '-' + total + '-');
                var id = 'id_' + name;
                $(this).attr({'name': name, 'id': id}).val('').removeAttr('checked');
            });
            findInput = $(selector).find('input');
            // currentItem = $(selector).closest('tr').find('option:selected');
            currentLabel = $(selector).closest('tr').find('label');
            //add value to Input
            findInput[0].value = 1;
            findInput[1].value = allVals[i].item_code; //Item Code
            findInput[2].value = allVals[i].name; //Item Name
            findInput[3].value = allVals[i].id; // Item ID
            findInput[4].value = allVals[i].customer_po_no; // Item ID
            findInput[5].value = allVals[i].order_quantity - allVals[i].delivery_quantity;
            findInput[6].value = parseFloat(allVals[i].unit_price).toFixed(2);
            findInput[9].value = allVals[i].currency;
            findInput[10].value = allVals[i].currency_id;
            findInput[11].value = allVals[i].part_gp;
            findInput[12].value = allVals[i].supplier_code;
            findInput[13].value = allVals[i].supplier_id;
            findInput[14].value = allVals[i].location_code;
            findInput[15].value = allVals[i].location_id;
            findInput[16].value = allVals[i].refer_number;
            findInput[17].value = allVals[i].refer_line;
            var order_quantity = parseFloat(allVals[i].order_quantity);
            findInput[18].value = order_quantity.toFixed(2);
            findInput[19].value = allVals[i].delivery_quantity;
            findInput[20].value = allVals[i].uom;

            currentLabel[0].textContent = findInput[0].value; // Line Number
            currentLabel[1].textContent = findInput[1].value; // Item Code
            currentLabel[2].textContent = findInput[2].value; // Item Name
            currentLabel[3].textContent = findInput[6].value; // Price
            currentLabel[5].textContent = findInput[9].value; // Currency Code
            currentLabel[6].textContent = findInput[11].value; // Part Group
            currentLabel[7].textContent = findInput[12].value; // Supplier Code
            currentLabel[8].textContent = findInput[14].value; // Location Code
            currentLabel[9].textContent = findInput[16].value; // Refer Number
            currentLabel[10].textContent = findInput[17].value; // Refer Line
            currentLabel[11].textContent = findInput[18].value; // Order Quantity
            currentLabel[12].textContent = findInput[19].value; // Delivery Quantity
            currentLabel[13].textContent = findInput[20].value; // UOM
            // calculate total, subtotal
            sum += parseInt(findInput[8].value);
            $('#id_subtotal').val(sum);
            if (isNaN(sum)) {
                sum = 0;
                $('#id_subtotal').val(0);
                $('#id_total').val(0);
            }
            if ($('#id_tax_amount').val()) {
                sum += parseFloat($('#id_tax_amount').val());
            }
            if ($('#id_discount').val()) {
                sum -= parseFloat($('#id_discount').val());
            }
            $('#id_total').val(sum);
            //if selected items > 1
            i = 1;
        }
        $('#btnSave').removeAttr('disabled');
        for (i; i < allVals.length; i++) {

            if (allVals[i].id != 0) {
                var newElement = $(selector).clone(true);
                var total = $('#id_' + type + '-TOTAL_FORMS').val();
                newElement.removeAttr("style")
                newElement.find(':input').each(function () {
                    var name = $(this).attr('name').replace('-' + (total - 1) + '-', '-' + total + '-');
                    var id = 'id_' + name;
                    $(this).attr({'name': name, 'id': id}).val('').removeAttr('checked');
                });
                newElement.find('label').each(function () {
                    var name = $(this).attr('name').replace('-' + (total - 1) + '-', '-' + total + '-');
                    var id = 'id_' + name;
                    $(this).attr({'name': name, 'id': id}).val('').removeAttr('checked');
                });
                //Set selected items of dialog to Item Column
                // var value = allVals[i].id;
                // newElement.find("option").each(function () {
                //     $(this).removeAttr('selected');
                //     if ($(this).val() == value) {
                //         $(this).attr("selected", true);
                //     }
                // });
                //Set selected price of dialog to Price Column
                var a = newElement.find('input');
                currentRow = newElement.closest('tr').find('label');
                if (a.length > 1) {
                    a[0].value = parseInt($(selector).find('input')[0].value) + 1;
                    a[1].value = allVals[i].item_code;
                    a[2].value = allVals[i].name;
                    a[3].value = allVals[i].id;
                    a[4].value = allVals[i].customer_po_no;
                    a[5].value = allVals[i].order_quantity - allVals[i].delivery_quantity;
                    a[6].value = parseFloat(allVals[i].unit_price).toFixed(2);
                    a[9].value = allVals[i].currency;
                    a[10].value = allVals[i].currency_id;
                    a[11].value = allVals[i].part_gp;
                    a[12].value = allVals[i].supplier_code;
                    a[13].value = allVals[i].supplier_id;
                    a[14].value = allVals[i].location_code;
                    a[15].value = allVals[i].location_id;
                    a[16].value = allVals[i].refer_number;
                    a[17].value = allVals[i].refer_line;
                    var order_quantity = parseFloat(allVals[i].order_quantity);
                    a[18].value = order_quantity.toFixed(2);
                    a[19].value = allVals[i].delivery_quantity;
                    a[20].value = allVals[i].uom;

                    currentRow[0].textContent = a[0].value; // Line Number
                    currentRow[1].textContent = a[1].value; // Item Code
                    currentRow[2].textContent = a[2].value; // Item Name
                    currentRow[3].textContent = a[6].value; // Price
                    currentRow[5].textContent = a[9].value; // Currency Code
                    currentRow[6].textContent = a[11].value; // Part Group
                    currentRow[7].textContent = a[12].value; // Supplier Code
                    currentRow[8].textContent = a[14].value; // Location Code
                    currentRow[9].textContent = a[16].value; // Refer Number
                    currentRow[10].textContent = a[17].value; // Refer Line
                    currentRow[11].textContent = a[18].value; // Order Quantity
                    currentRow[12].textContent = a[19].value; // Delivery Quantity
                    currentRow[13].textContent = a[20].value; // UOM

                    sum = parseInt($('#id_subtotal').val());
                    sum += parseInt(a[8].value);
                    $('#id_subtotal').val(sum);
                    if (isNaN(sum)) {
                        sum = 0;
                        $('#id_subtotal').val(0);
                        $('#id_total').val(0);
                    }
                    if ($('#id_tax_amount').val()) {
                        sum += parseFloat($('#id_tax_amount').val());
                    }
                    if ($('#id_discount').val()) {
                        sum -= parseFloat($('#id_discount').val());
                    }
                    $('#id_total').val(sum);
                }
                // newElement.find('label').each(function () {
                //     var newFor = $(this).attr('for').replace('-' + (total - 1) + '-', '-' + total + '-');
                //     $(this).attr('for', newFor);
                // });
                total++;
                $('#id_' + type + '-TOTAL_FORMS').val(total);
                $(selector).after(newElement);

            }
        }
    }

    $(document).on('click', "[class^=removerow]", function (event) {
        currentRow = $(this).closest('tr').find('input');
        item_id = currentRow[3].value;
        if ($('#id_formset_item-TOTAL_FORMS').val() == 1) {
            $(this).closest('tr').css("background-color", "");
            $(this).closest('tr').css('display', 'none');
            $('#id_subtotal').val(0);
            $('#id_total').val(0);
            fnDisableButton()
        } else {
            fnEnableButton();
            var minus = $('input[name=formset_item-TOTAL_FORMS]').val() - 1;
            $('#id_formset_item-TOTAL_FORMS').val(minus);
            $(this).parents("tr").remove();
        }

        $('#dynamic-table tr.gradeX').each(function (rowIndex, r) {
            $(this).find('td').each(function (colIndex, c) {
                $.each(this.childNodes, function (i, elem) {
                    if (elem.nodeName == 'INPUT' || elem.nodeName == 'LABEL') {
                        if (colIndex == 0) {
                            elem.innerHTML = rowIndex + 1;
                            elem.value = rowIndex + 1;
                        }
                        elem.attributes.name.nodeValue = elem.attributes.name.nodeValue.replace(/\d+/g, rowIndex);
                        elem.id = elem.id.replace(/\d+/g, rowIndex);
                    }
                });
            });
        });
        calculateTotal();
    });
});

//Company Information
$(document).ready(function () {
    //toggle `popup` / `inline` mode
    $.fn.editable.defaults.mode = 'inline';
    //Get company id
    var company_id = $('#company_id').val();
    //make status editable
    $('#companyname').editable({
        type: 'text',
        pk: company_id,
        url: '/orders/change_company/' + company_id + '/',
        title: 'Enter company name',
        success: function (response, newValue) {

            if (!response) {
                return "Unknown error!"
            }
            if (response.success === false) {
                return respond.msg;
            }
        }
    });

    $('#address').editable({
        type: 'text',
        pk: company_id,
        url: '/orders/change_company/' + company_id + '/',
        title: 'Enter company address',
        success: function (response, newValue) {

            if (!response) {
                return "Unknown error!"
            }
            if (response.success === false) {
                return respond.msg;
            }
        }
    });

    $('#email').editable({
        type: 'text',
        pk: company_id,
        url: '/orders/change_company/' + company_id + '/',
        title: 'Enter company email',
        validate: function (value) {
            var valid = valib.String.isEmailLike(value)
            if (valid == false) return 'Please insert valid email'
        },
        success: function (response, newValue) {

            if (!response) {
                return "Unknown error!"
            }
            if (response.success === false) {
                return respond.msg;
            }
        }
    });

});
//Supplier Information
$(document).ready(function (hdSupplierId) {
    var hdSupplierId = $('#hdSupplierId').val();

    loadSupplierInfo(hdSupplierId);

    function loadSupplierInfo(hdSupplierId) {
        $.fn.editable.defaults.mode = 'inline';

        //make status editable
        $('#supplier_name').editable({
            type: 'text',
            pk: hdSupplierId,
            url: '/orders/change_supplier/' + hdSupplierId + '/',
            title: 'Enter supplier name',
            success: function (response, newValue) {
                if (!response) {
                    return "Unknown error!"
                }
                if (response.success === false) {
                    return respond.msg;
                }
            }
        });

        $('#supplier_address').editable({
            type: 'text',
            pk: hdSupplierId,
            url: '/orders/change_supplier/' + hdSupplierId + '/',
            title: 'Enter supplier address',
            success: function (response, newValue) {
                if (!response) {
                    return "Unknown error!"
                }
                if (response.success === false) {
                    return respond.msg;
                }
            }
        });

        $('#supplier_email').editable({
            type: 'text',
            pk: hdSupplierId,
            url: '/orders/change_supplier/' + hdSupplierId + '/',
            title: 'Enter supplier email',
            validate: function (value) {
                var valid = valib.String.isEmailLike(value)
                if (valid == false) return 'Please insert valid email'
            },
            success: function (response, newValue) {
                if (!response) {
                    return "Unknown error!"
                }
                if (response.success === false) {
                    return respond.msg;
                }
            }
        });
    };

    $('#id_supplier').change(function () {
        var supplier_id = parseInt($(this).val());
        $.ajax({
            method: "POST",
            url: '/orders/supplier/',
            dataType: 'JSON',
            data: {
                'csrfmiddlewaretoken': $('input[name="csrfmiddlewaretoken"]').val(),
                'supplier_id': supplier_id,
            },
            responseTime: 200,
            response: function (settings) {
                if (settings.data.value) {
                    this.responseText = '{"success": true}';
                } else {
                    this.responseText = '{"success": false, "msg": "required"}';
                }
            },
            success: function (json) {
                console.log(json);
                $('#hdSupplierId').val(json['id']);

                $('#supplier_name').editable('destroy');
                $('#supplier_address').editable('destroy');
                $('#supplier_email').editable('destroy');

                $('#supplier_name').attr('data-pk', json['id']);
                $('#supplier_name').text(json['name']);
                $('#supplier_address').attr('data-pk', json['id']);
                $('#supplier_address').text(json['address']);
                $('#supplier_email').attr('data-pk', json['id']);
                $('#supplier_email').text(json['email']);

                loadSupplierInfo(json['id']);

                //filter product by supplier
                var $tableSel = $('#tblData').dataTable();
                $tableSel.fnFilter(json['name']);
            }
        });
    });
});
//event handle for input discount
$('#id_discount').change(function () {
    if ($(this).val() != '') {
        var sum = parseFloat($('#id_subtotal').val()) + parseFloat($('#id_tax_amount').val());
        sum -= parseInt(this.value);
        $('#id_total').val(sum);
    } else {
        var sum = 0;
        $('#dynamic-table tr.gradeX').each(function () {
            var $tds = $(this).find('input');
            amount = $tds[8].value;
            sum += parseInt(amount);
            $('#id_subtotal').val(sum);
            var total = sum + parseFloat($('#id_tax_amount').val())
            $('#id_total').val(total);
        })
    }
});

//event change quantity
$('#dynamic-table tr.gradeX').each(function () {
    currentRow = $(this).closest('tr').find('input');
    currentColumn = $(this).closest('tr').find('td');
    $mainElement = '#' + currentRow[5].id; // ID of Quantity Column
    var order_type = $('#order_type').text();
    var is_generate = $('#is_generate').text();
    var order_id = $('#order_id').text();
    $($mainElement).change(function () {
        debugger;
        currentRow = $(this).closest('tr').find('input');
        var quantity = currentRow[5].value; // Quantity Value
        var price = currentRow[6].value; // Price Value
        var order_quantity = currentRow[18].value;
        var new_delivery_quantity = parseFloat(currentRow[5].value) + parseFloat(currentRow[19].value);
        if (quantity < 1) {
            $('#minimum_order_error').removeAttr('style');
            $('#minimum_order_error').text('The quantity of product ' + currentRow[1].value + ' must greater than 0');
            $(this).closest('tr').attr('style', 'background-color: yellow !important');
            $('#dynamic-table tr.gradeX').each(function () {
                $(this).closest('tr').find('input').not(currentRow[5]).attr('disabled', true);
            });
            fnDisableButton();
        } else if (new_delivery_quantity > order_quantity) {
            $('#minimum_order_error').removeAttr('style');
            $('#minimum_order_error').text('The number quantity to delivery is not valid.');
            $(this).closest('tr').attr('style', 'background-color: yellow !important');
            $('#dynamic-table tr.gradeX').each(function () {
                $(this).closest('tr').find('input').not(currentRow[5]).attr('disabled', true);
            });
            fnDisableButton();
        } else {
            $('#dynamic-table tr.gradeX').each(function () {
                $(this).closest('tr').find('input').removeAttr('disabled');
            });
            $(this).closest('tr').removeAttr('style');
            $('#items_error').css('display', 'none');
            $('#minimum_order_error').css('display', 'none');
            fnEnableButton();
        }
        calculateTotal();
    });
});

// event change price
$('#dynamic-table tr.gradeX').find('input').each(function () {
    currentRow = $(this).closest('tr').find('input');
    // currentItem = $(this).closest('tr').find('option:selected');
    $priceElement = '#' + currentRow[6].id; // ID of Price Column
    $($priceElement).change(function () {
        currentRow = $(this).closest('tr').find('input');
        currentRow[8].value = Number(currentRow[5].value * currentRow[6].value * currentRow[7].value).toFixed(2);
        if (currentRow[6].value > 0) { // Check Price Value
            $('#validate_error').css('display', 'none');
            currentRow.parents('tr').removeAttr('style');
            $('#dynamic-table tr.gradeX').each(function () {
                $(this).closest('tr').find('input').removeAttr('disabled');
            });
            fnEnableButton();
            // validationItemsFormset();
        } else {
            $('#validate_error').text('Price of product must greater than 0 and not none');
            $('#validate_error').removeAttr('style');
            currentRow.parents('tr').attr('style', 'background-color: red !important');
            $('#dynamic-table tr.gradeX').each(function () {
                $(this).closest('tr').find('input').not(currentRow[6]).attr('disabled', true);
            });
            fnDisableButton();
        }
        calculateTotal();
    });
});

//change amount event
$('#dynamic-table tr.gradeX').each(function () {
    currentRow = $(this).closest('tr').find('input');
    $amountElement = '#' + currentRow[8].id; // Get ID of Amount Column
    $($amountElement).change(function () {
        $tds = $(this).closest('tr').find('input');
        if ($tds[8].value < 0) {
            $(this).closest('tr').attr('style', 'background-color: red !important');
            $('#items_error').text('The product ' + $tds[1].value + ' must have amount greater than 0');
            $('#items_error').removeAttr('style');
            $('#dynamic-table tr.gradeX').each(function () {
                $(this).closest('tr').find('input').not($tds[8]).attr('disabled', true);
            });
            fnDisableButton();
        } else {
            var subtotal = 0;
            $('#items_error').css('display', 'none');
            $(this).closest('tr').removeAttr('style');
            $('#dynamic-table tr.gradeX').each(function () {
                var $tdsInput = $(this).find('input');
                $(this).closest('tr').find('input').removeAttr('disabled');
                subtotal += parseFloat($tdsInput[8].value);
            });
            $('#id_subtotal').val(subtotal.toFixed(2));
            var total = subtotal + parseFloat($('#id_tax_amount').val()) - parseFloat($('#id_discount').val())
            $('#id_total').val(total.toFixed(2));
            fnEnableButton();
        }
    });
});

//change exchange rate event
$('#dynamic-table tr.gradeX').each(function () {
    currentRow = $(this).closest('tr').find('input');
    $exchangeElement = '#' + currentRow[7].id; // Get ID of Exchange Rate Column
    $($exchangeElement).change(function () {
        $tds = $(this).closest('tr').find('input');
        if ($(this)[0].value <= 0) {
            $(this).closest('tr').attr('style', 'background-color: red !important');
            $('#items_error').text('Exchange Rate must have value greater than 0');
            $('#items_error').removeAttr('style');
            $('#dynamic-table tr.gradeX').each(function () {
                $(this).closest('tr').find('input').not($tds[7]).attr('disabled', true);
            });
            fnDisableButton();
        } else {
            $('#items_error').css('display', 'none');
            $(this).closest('tr').removeAttr('style');
            $('#dynamic-table tr.gradeX').each(function () {
                $(this).closest('tr').find('input').removeAttr('disabled');
            });
            fnEnableButton();
        }
        calculateTotal();
    });
});

// event copy customer address
$('#btnCopyCustomer').click(function () {
    var hdCustomerId = $('#hdCustomerId').val();
    $.ajax({
        method: "POST",
        url: '/orders/customer/',
        dataType: 'JSON',
        data: {
            'csrfmiddlewaretoken': $('input[name="csrfmiddlewaretoken"]').val(),
            'customer_id': hdCustomerId,
        },
        success: function (json) {
            console.log(json);
            $('#id_name').val($('#customer_name').text());
            $('#id_address').val($('#customer_address').text());
            $('#id_email').val($('#customer_email').text());
            $('#id_code').val(json['code']);
            $('#id_phone').val(json['phone']);
            $('#id_fax').val(json['fax']);
        }
    });
});

// event change address
$('#id_customer_address').change(function () {
    var address_id = $(this).val();
    $.ajax({
        method: "POST",
        url: '/orders/change_address/',
        dataType: 'JSON',
        data: {
            'csrfmiddlewaretoken': $('input[name="csrfmiddlewaretoken"]').val(),
            'address_id': address_id,
        },
        success: function (json) {
            console.log(json);
            if ($('#id_customer_address option:selected').text() == "") {
                $('#id_name').val("");
                $('#id_address').val("");
                $('#id_email').val("");
                $('#id_code').val("");
                $('#id_phone').val("");
                $('#id_fax').val("");
                $('#id_attention').val("");
                $('#id_note_1').val("");
                $('#id_note_2').val("");
            } else {
                $('#id_name').val($('#id_customer_address option:selected').text());
                $('#id_address').val(json['address']);
                $('#id_email').val(json['email']);
                $('#id_code').val(json['code']);
                $('#id_phone').val(json['phone']);
                $('#id_fax').val(json['fax']);
                $('#id_attention').val(json['attention']);
                $('#id_note_1').val(json['note_1']);
                $('#id_note_2').val(json['note_2']);
            }
        }
    });
});

function validationItemsFormset() {
    $('#dynamic-table tr.gradeX').find('input').each(function () {
        var order_type = $('#order_type').text();
        var order_id = $('#order_id').text();
        var display = $('#dynamic-table tr.gradeX:last').css("display");
        currentItem = $(this).closest('tr').find('option:selected');
        currentRow = $(this).closest('tr').find('input');
        inputQuantity = '#' + currentRow[6].id;
        var count = $('#dynamic-table').find('tr').filter(function () {
            var colors = ["#ffc0cb", "rgb(255, 192, 203)"];
            return $.inArray($(this).css('background-color'), colors) !== -1;
        }).length;
        if (display == 'none') {
            $('#items_error').css('display', 'none');
        } else {
            if (order_id == "" && order_type == 1) {
                if (parseInt(currentRow[6].value, 10) > parseInt(currentRow[9].value, 10)) {
                    $(this).closest('tr').attr('style', 'background-color: pink !important');
                    $('#items_error').text('The product don’t have enough stock quantity!');
                    $('#items_error').removeAttr('style');
                    calculateTotal();
                }
                if (currentRow[10].value == "" || currentRow[10].value == "None") {
                    $(this).closest('tr').attr('style', 'background-color: red !important');
                    $('#validate_error').text('Price of product must greater than 0 and not none');
                    $('#validate_error').removeAttr('style');
                    $('#btnSave').attr('disabled', true);
                }
                if (count > 0) {
                    $('#items_error').removeAttr('style');
                    $('#items_error').text('The product don’t have enough stock quantity!');
                } else {
                    $('#items_error').css('display', 'none');
                }
                ;
            } else {
                if (parseInt(currentRow[6].value, 10) > parseInt(currentRow[9].value, 10)) {
                    $(this).closest('tr').attr('style', 'background-color: pink !important');
                    // $('#items_error').text('The product have been removed from stock!');
                    $('#items_error').text('The product don’t have enough stock quantity!');
                    $('#items_error').removeAttr('style');
                    calculateTotal();
                }
                if (count > 0) {
                    $('#items_error').removeAttr('style');
                    // $('#items_error').text('The product have been removed from stock');
                    $('#items_error').text('The product don’t have enough stock quantity!');
                } else {
                    $('#items_error').css('display', 'none');
                }
                ;
            }
            ;
            $('#dynamic-table tr.gradeX').each(function () {
                var table = $('#tblData').DataTable();
                var data = table.rows().data();
                currentRow = $(this).closest('tr').find('input');
                item_id = currentRow[2].value;
                data.each(function (value, index) {
                    if (value[9] == item_id) {
                        var row = table.row('#' + item_id).node();
                        row.setAttribute("style", "display: none");
                    }
                });
            });
            // if (currentRow[8].value == "" || currentRow[8].value == "None") {
            //     currentRow[9].value = 0;
            //     $('#validate_error').text('Price of product ' + currentItem[0].text + ' must greater than 0 and not none');
            //     $('#validate_error').removeAttr('style');
            //     $('#btnSave').attr('disabled', true);
            // }
        }
    });
    $('#id_customer_address').find('option').each(function () {
        if ($('#id_name').val() == $(this).text()) {
            $(this).attr('selected', true);
        }
    });
}

function changeCurrency(arrItems, currency_id, currency_name) {
    $.ajax({
        method: "POST",
        url: '/orders/load_currency/',
        dataType: 'JSON',
        data: {
            'csrfmiddlewaretoken': $('input[name="csrfmiddlewaretoken"]').val(),
            // 'arrItems[]': JSON.stringify(arrItems),
            'arrItems': JSON.stringify(arrItems),
            'currency_id': currency_id,
        },
        success: function (json) {
            debugger;
            console.log(json);
            var item_currency_not_match = [];
            var sale_price = 0;
            var amount = 0;
            var purchase_price = 0;
            for (var i in json) {
                if (json[i].constructor === Object) {
                    $('#dynamic-table tr.gradeX').each(function () {
                        currentRow = $(this).closest('tr').find('input');
                        currentLabel = $(this).closest('tr').find('label');
                        currentItemName = currentRow[2].value; // Get Item Name
                        if (currentRow[3].value == json[i].id) { // Check Item ID
                            if (json[i].rate == 0 || json[i].sale_price == "") {
                                // item_currency_not_match.push({item: currentItemName, currency: json[i].currency});
                                item_currency_not_match.push('Can not get Exchange Rate from ' + json[i].currency + ' to ' + currency_name);
                                currentRow[7].value = 0;
                                currentRow[8].value = 0;
                                currentLabel[4].textContent = currentRow[8].value;
                                $('.lblCurrency').text(json[i].currency);
                            } else {
                                sale_price = currentRow[5].value * currentRow[6].value; // Calculate Quantity * Sale Price
                                currentRow[7].value = parseFloat(json[i].rate).toFixed(2); // Set Exchange Rate
                                amount = currentRow[7].value * sale_price; // Set Sale Price * Exchange Rate
                                currentRow[8].value = amount.toFixed(2); // Set Amount
                                currentLabel[4].textContent = currentRow[8].value; // Amount
                                $('.lblCurrency').text(json['symbol']);
                            }
                        }
                    });
                }
            }
            ;
            // console.log(item_currency_not_match);
            if (item_currency_not_match.length > 0) {
                $("#currency_error").text("");
                var uniqueNames = [];
                $.each(item_currency_not_match, function (i, el) {
                    if ($.inArray(el, uniqueNames) === -1) uniqueNames.push(el);
                });
                for (i = 0; i < uniqueNames.length; i++) {
                    document.getElementById('currency_error').innerHTML += uniqueNames[i] + '<br>';
                }
                $('#currency_error').removeAttr('style');
                fnDisableButton();
                $('#dynamic-table tr.gradeX').each(function () {
                    $(this).closest('tr').find('input').attr('disabled', true);
                });
            } else {
                $('#currency_error').css('display', 'none');
                fnEnableButton();
                $('#dynamic-table tr.gradeX').each(function () {
                    $(this).closest('tr').find('input').removeAttr('disabled');
                });
            }
            calculateTotal();
        }
    });
}

// event change currency
$('#id_currency').change(function () {
    var currency_id = parseInt($(this).val());
    var currency_name = $('#id_currency option:selected').text();
    var arrItems = [];
    $('#dynamic-table tr.gradeX').each(function () {
        currentRow = $(this).closest('tr').find('input');
        arrItems.push({
            item_id: currentRow[3].value, // Get ID of Item
            currency_id: currentRow[10].value // Get ID of Currency
        });
    });
    changeCurrency(arrItems, currency_id, currency_name);
});

//event generate document number by order date
$('#id_order_date').change(function () {
    var order_date = $('#id_order_date').val();
    var order_type = $('#order_type').text();
    var document_number = $('#id_document_number').val()
    $.ajax({
        method: "POST",
        url: '/orders/generate_document_number/',
        dataType: 'text',
        data: {
            'csrfmiddlewaretoken': $('input[name="csrfmiddlewaretoken"]').val(),
            'order_date': order_date,
            'order_type': order_type,
            'document_number': document_number
        },
        success: function (data) {
            $('#id_document_number').val(data);
        }
    });
});


$(document).ready(function () {
    // var order_id = $('#order_id').text();
    // if (order_id != "") {
    //     $('#dynamic-table tr.gradeX').each(function () {
    //         currentRow = $(this).closest('tr').find('input');
    //         $mainElement = '#' + currentRow[5].id;
    //         if (parseFloat(currentRow[18].value) == parseFloat(currentRow[19].value)) {
    //             $(this).closest('tr').find($mainElement).attr('disabled', true);
    //         }
    //     });
    // }

    // $("#search_input").keypress(function (e) {
    //     var key = e.which;
    //     if (key == 13) {
    //         fnSearchDOInvocieItem();
    //     }
    // });
    $('#btnSearchItem').on('click', function () {
        fnSearchDOInvocieItem();
    });
    $('#btnOpenItemDialog').on('click', function () {
        var dataTable = $('#tblData').dataTable();
        dataTable.fnClearTable(this);
        fnSearchDOInvocieItem();
    });
    // Search Countries dialog
    $('#tblDataCountry').dataTable({
        "iDisplayLength": 5,
        "bLengthChange": false,
        "order": [[1, "desc"], [0, "desc"]]
    });
    $('.check-valid-item-data').on('click', function () {
        var countRowVisible = $('#dynamic-table tr.gradeX:visible').length;
        if (countRowVisible == 0) {
            $('#items_error').text('Please select products before save!');
            $('#items_error').removeAttr('style');
            fnDisableButton();
        }
    });
    $('#btnSave').on('click', function () {
        var countRowVisible = $('#dynamic-table tr.gradeX:visible').length;
        if (countRowVisible == 0) {
            $('#items_error').text('Please select products before save!');
            $('#items_error').removeAttr('style');
            fnDisableButton();
        }
    });
});

var origin_country_code;
var origin_country_id;

$('.btnOpenCountryDialog').on('click', function () {
    currentRow = $(this).closest('tr').find('input');
    origin_country_code = currentRow[21].id;
    origin_country_id = currentRow[22].id;
});

$('#btnAddCountries').on('click', function () {
    var table = $('#tblDataCountry').DataTable();
    var country_selected = table.$(".call-radio:checked", {"page": "all"});
    $('#' + origin_country_code).val(country_selected[0].value);
    $('#' + origin_country_id).val(country_selected[0].id);
    $(this).attr('data-dismiss', 'modal');
    $('input[type=radio]').each(function () {
        $(this).prop('checked', false);
    });
});

function fnSearchDOInvocieItem() {
    // var data = $('#search_input').val();
    var customerID = $('#hdCustomerId').val();
    var exclude_item_array = [];
    var exclude_item_list = {};
    $('#dynamic-table tr.gradeX').each(function () {
        var display = $(this).css("display");
        currentRow = $(this).closest('tr').find('input');
        if (display != 'none') {
            exclude_item_array.push(currentRow[3].value);
        }
    });
    if (exclude_item_array.length > 0) {
        exclude_item_list = JSON.stringify(exclude_item_array);
    }

    // if (data.length == 0)
    //     data = '0';
    // $.ajax({
    //     method: "POST",
    //     url: '/orders/DOInvoice_item_search/' + customerID + '/' + data + '/',
    //     dataType: 'JSON',
    //     data: {
    //         'csrfmiddlewaretoken': $('input[name="csrfmiddlewaretoken"]').val(),
    //         'customer_id': customerID,
    //         'search_condition': data,
    //         'exclude_item_list': exclude_item_list
    //     },
    //     responseTime: 200,
    //     success: function (data) {
    //         debugger;
    //         if (data.length > 0) {
    //             var dataTable = $('#tblData').dataTable();
    //             oSettings = dataTable.fnSettings();
    //             dataTable.fnClearTable(this);
    //             for (var i = 0; i < data.length; i++) {
    //                 //dataTable.oApi._fnAddData(oSettings, data[i]);
    //                 var row = dataTable.fnAddData(
    //                     [data[i].item_id,// item_id
    //                         data[i].part_no, // part_no
    //                         data[i].item_name, // item_name
    //                         data[i].ref_number, // ref_number
    //                         data[i].ref_line, // ref_line
    //                         data[i].supplier_code, // supplier_code
    //                         data[i].location_code, // location_code
    //                         data[i].part_gp, // part_gp
    //                         data[i].sales_price, // sales_price
    //                         data[i].currency, // currency
    //                         data[i].location_id, // location_id
    //                         data[i].currency_id, // currency_id
    //                         data[i].line_id, // line_id
    //                         data[i].uom, // uom
    //                         data[i].supplier_code_id, //supplier_code_id
    //                         data[i].order_quantity, //order_quantity
    //                         data[i].delivery_quantity, //delivery_quantity
    //                         data[i].customer_po_no, //customer_po_no
    //                         '<input type="checkbox" name="choices" class="call-checkbox" value="'
    //                         + data[i].sales_price + '">']);
    //                 // dataTable.$('tr')[i+1].id = data[i].line_id;
    //             }
    //             oSettings.aiDisplay = oSettings.aiDisplayMaster.slice();
    //             dataTable.fnDraw();
    //             dataTable.$('tr', {"page": "all"}).each(function (rowIndex, r) {
    //                 r.id = data[rowIndex].line_id;
    //                 $(this).find('td').each(function (colIndex, c) {
    //                     if (colIndex == 0 || colIndex == 10 || colIndex == 11 || colIndex == 12 || colIndex == 13 || colIndex == 14) {
    //                         c.style.display = 'none';
    //                     }
    //                     if (colIndex == 15) {
    //                         c.childNodes[0].id = data[rowIndex].line_id;
    //                     }
    //                 });
    //             });
    //             dataTable.$('.call-checkbox', {"page": "all"}).each(function (rowIndex, r) {
    //                 r.id = data[rowIndex].line_id;
    //             });
    //             dataTable.$('tr').css('backgroundColor', 'white');
    //             dataTable.$('td').css('text-align', 'left');
    //             dataTable.$('td:last').css('text-align', 'center');
    //             dataTable.$('th:last').css('text-align', 'center');
    //             dataTable.$('td').removeClass('sorting_1');
    //
    //         }
    //         else {
    //             var dataTable = $('#tblData').dataTable();
    //             dataTable.fnClearTable(this);
    //             $('#search_input').val('');
    //         }
    //     }
    // });

    var datatbl = $('#tblData').DataTable();
    datatbl.destroy();
    var list_url = $('#list_url').text();
    $('#tblData').DataTable({
        "iDisplayLength": 5,
        "bLengthChange": false,
        "order": [[1, "desc"], [0, "desc"]],
        "serverSide": true,
        "ajax": {
            "url": list_url,
            "data": {
                "csrfmiddlewaretoken": $('input[name="csrfmiddlewaretoken"]').val(),
                "customer_id": customerID,
                "exclude_item_list": exclude_item_list
            }
        },
        "rowId": "line_id",
        "columns": [
            {
                "data": "item_id",
                "className": "hide_column"
            },
            {"data": "part_no", "sClass": "text-left"},
            {"data": "item_name", "sClass": "text-left"},
            {"data": "refer_number", "sClass": "text-left"},
            {"data": "refer_line", "sClass": "text-left"},
            {"data": "supplier_code", "sClass": "text-left"},
            {"data": "location_code", "sClass": "text-left"},
            {"data": "part_gp", "sClass": "text-left"},
            {"data": "sales_price", "sClass": "text-right"},
            {"data": "currency_code", "sClass": "text-left"},
            {
                "data": "location_id",
                "className": "hide_column"
            },
            {
                "data": "currency_id",
                "className": "hide_column"
            },
            {
                "data": "line_id",
                "className": "hide_column"
            },
            {
                "data": "unit",
                "className": "hide_column"
            },
            {
                "data": "supplier_id",
                "className": "hide_column"
            },
            {"data": "order_qty"},
            {"data": "delivery_qty"},
            {"data": "customer_po_no"},
            {
                "orderable": false,
                "data": null,
                "render": function (data, type, row, meta) {
                    return '<input type="checkbox" name="choices" id="' + row.line_id + '"'
                        + 'class="call-checkbox" value="' + row.sales_price + '"></td>';
                }
            }
        ]
    });
}

function fnDisableButton() {
    $('#btnPrint').attr('disabled', true);
    $('#btnSave').attr('disabled', true);
    $('#btnSend').attr('disabled', true);
    $('#btnSendForEdit').attr('disabled', true);
}

function fnEnableButton() {
    $('#btnPrint').removeAttr('disabled');
    $('#btnSave').removeAttr('disabled');
    $('#btnSend').removeAttr('disabled');
    $('#btnSendForEdit').removeAttr('disabled');
}

// calculate subtotal and total
function calculateTotal() {
    var subtotal = 0;
    var total = 0;
    $('#dynamic-table tr.gradeX').each(function () {
        var $tds = $(this).find('input');
        var label = $(this).find('label');
        price = $tds[6].value * $tds[7].value
        amount = $tds[5].value * price;
        $tds[8].value = parseFloat(amount.toFixed(2));
        label[4].textContent = $tds[8].value;
        subtotal += parseFloat(amount);
        $('#id_subtotal').val(subtotal.toFixed(2));
        if ($('#id_discount').val() == '' || $('#id_discount').val() == null) {
            total = subtotal + parseFloat($('#id_tax_amount').val());
        } else {
            total = subtotal + parseFloat($('#id_tax_amount').val()) - parseFloat($('#id_discount').val());
        }
        $('#id_total').val(total.toFixed(2));
    });
}

$('#id_delivery').change(function () {
    var delivery_id = parseInt($(this).val());
    $.ajax({
        method: "POST",
        url: '/orders/change_address/',
        dataType: 'JSON',
        data: {
            'csrfmiddlewaretoken': $('input[name="csrfmiddlewaretoken"]').val(),
            'delivery_id': delivery_id,
        },
        responseTime: 200,
        response: function (settings) {
            if (settings.data.value) {
                this.responseText = '{"success": true}';
            } else {
                this.responseText = '{"success": false, "msg": "required"}';
            }
        },
        success: function (json) {
            debugger;
            console.log(json);
            $('#id_name').val(json['name']);
            $('#id_code').val(json['code']);
            $('#id_address').val(json['address']);
            $('#id_note_1').val(json['note_1']);
            $('#id_contact_name').val(json['contact_name']);
            $('#id_contact_company').val(json['contact_company']);
            $('#id_contact_designation').val(json['contact_designation']);
            $('#id_contact_attention').val(json['contact_attention']);
            $('#id_contact_phone').val(json['contact_phone']);
            $('#id_contact_fax').val(json['contact_fax']);
            $('#id_contact_email').val(json['contact_email']);
            $('#id_contact_web').val(json['contact_web']);
            $('#id_contact_address').val(json['contact_address']);
            $('#id_contact_remark').val(json['contact_remark']);
        }
    });
});