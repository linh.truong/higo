/**
 * Created by tho.pham on 11/1/2016.
 */

$(document).ready(function () {
    if (request_method == 'GET') {
        if ($('#id_formset_item-TOTAL_FORMS').val() > 1) {
            $('#dynamic-table tr.gradeX:last').remove();
            $('#id_formset_item-TOTAL_FORMS').val($('#id_formset_item-TOTAL_FORMS').val() - 1);
            $('#items_error').css("display", "none");
        } else {
            $('#dynamic-table tr.gradeX:last').css("display", "none");
            $('#items_error').removeAttr('style');
        }
    } else if (request_method == 'POST') {
        var item = $('#id_formset_item-0-item').val();
        var quantity = $('#id_formset_item-0-quantity').val();
        var price = $('#id_formset_item-0-price').val();
        var amount = $('#id_formset_item-0-amount').val();
        if (quantity == "" && price == "" && amount == "") {
            $('#dynamic-table tr.gradeX:last').css("display", "none");
            $('#items_error').removeAttr('style');
        } else $('#items_error').css("display", "none");
    }
    if (stock_trans_id != '') {
        $('#id_io_flag option:not(:selected)').attr('disabled', true);
        var io_flag = $('#id_io_flag option:selected').text();
        if (io_flag == 'IN') {
            $('#id_in_location').prop('readonly', false);
            $('#id_out_location').prop('readonly', true);
        } else if (io_flag == 'OUT') {
            $('#id_in_location').prop('readonly', true);
            $('#id_out_location').prop('readonly', false);
        } else if (io_flag == 'Transfer') {
            $('#id_in_location').prop('readonly', false);
            $('#id_out_location').prop('readonly', false);
        } else {
            $('#id_in_location').prop('readonly', true);
            $('#id_out_location').prop('readonly', true);
        }
    }
    if ($('#id_transaction_code_id').val() == '') {
        $('#btnOpenItemDialog').attr('disabled', true);
    }
    if (status_id == '1') {
        $('#btnOpenItemDialog').removeAttr('disabled');
    }
});

var datatbl = $('#tblTransCode').DataTable();

$("#id_transaction_code").keypress(function (e) {
    if (e.which == 13) {
        e.preventDefault();
        $.ajax({
            method: "POST",
            url: '/orders/customer_search_by_code/',
            dataType: 'JSON',
            data: {
                'csrfmiddlewaretoken': $('input[name="csrfmiddlewaretoken"]').val(),
                'customer_code': $("#form_customer_code").val()
            },
            responseTime: 200,
            response: function (settings) {
                if (settings.data.value) {
                    this.responseText = '{"success": true}';
                }
                else {
                    this.responseTime = '{"success: false, "msg": "required"}';
                }
            },
            success: function (json) {
                console.log(json);
                $('#hdCustomerId').val(json['id']);
                $("#form_customer_code").val(json['code']);
                $('#customer_name').editable('destroy');
                $('#customer_address').editable('destroy');
                $('#customer_email').editable('destroy');

                $('#customer_name').attr('data-pk', json['id']);
                $('#customer_name').text(json['name']);
                $('#customer_address').text(json['address']);

                $('#customer_email').attr('data-pk', json['id']);
                $('#customer_email').text(json['email']);
                $('#customer_payment_term').text('Payment Term: ' + json['term'] + ' days');
                $('#customer_payment_mode').text('Payment Mode: ' + json['payment_mode']);
                $('#customer_credit_limit').text('Credit Limit: ' + json['credit_limit']);
                loadCustomerInfo(json['id']);
                $('#id_tax').find('option').removeAttr("selected");
                $('#id_tax').find('option').removeAttr("disabled");
                $('#id_tax').find('option[value="' + json['tax_id'] + '"]').attr("selected", "selected");
                $('#id_tax').val(json['tax_id']);
                $('#id_tax option:not(:selected)').attr('disabled', true);
                // load tax again
                var taxid = parseInt($('#id_tax').val());
                if (isNaN(taxid)) {
                    $('#id_tax_amount').val(0);
                    $('#id_total').val(parseFloat($('#id_tax_amount').val()) - parseFloat($('#id_discount').val()) + parseFloat($('#id_subtotal').val()));
                } else {
                    $.ajax({
                        method: "POST",
                        url: '/orders/load_tax/',
                        dataType: 'JSON',
                        data: {
                            'csrfmiddlewaretoken': $('input[name="csrfmiddlewaretoken"]').val(),
                            'tax_id': taxid,
                        },
                        success: function (json) {
                            var tax_amount = (parseFloat(json) * parseFloat($('#id_subtotal').val())) / 100;
                            $('#id_tax_amount').val(tax_amount.toFixed(6));
                            var total = parseFloat($('#id_subtotal').val()) + tax_amount - parseFloat($('#id_discount').val());
                            $('#id_total').val(total.toFixed(6));
                        }
                    });
                }
                $('#id_currency').find('option').removeAttr('selected');
                $('#id_currency').find('option').removeAttr('disabled');
                $('#id_currency option[value=' + json['currency_id'] + ']').attr('selected', 'selected');
                $('#id_currency').val(json['currency_id']);
                $('#id_currency option:not(:selected)').attr('disabled', true);
            }
        })
    }
});

$('#btnSearchTransCode').on('click', function () {
    // $('#modalTransaction').modal('show');
    $('#tblTransCode').DataTable().destroy();
    $('#tblTransCode').dataTable({
        "order": [[0, "desc"]],
        "serverSide": true,
        "ajax": {
            "url": load_transaction_code_list,
        },
        "columns": [
            {"data": "update_date"},
            {"data": "code"},
            {"data": "name"},
            {"data": "io_flag"},
            {"data": "price_flag", "visible": false},
            {"data": "doc_type", "visible": false},
            {
                "orderable": false,
                "data": null,
                "render": function (data, type, full, meta) {
                    if (full.auto_generate == 'True') {
                        var mSpan = '<span class="label label-success label-mini">True</span>'
                        return mSpan
                    }
                    else {
                        var mSpan = '<span class="label label-danger label-mini">False</span>'
                        return mSpan
                    }
                }
            },
            {"data": "ics_prefix", "visible": false},
            {
                "orderable": false,
                "data": null,
                "render": function (data, type, full, meta) {
                    return '<input type="radio" name="choices" id="' +
                        full.id + '" class="call-checkbox" value="' + full.id + '">';
                }
            }
        ]
    });
});


var dataLocation = $('#tblLocation').DataTable();

function load_location(exclude_location_id) {
    $('#tblLocation').dataTable({
        "order": [[0, "desc"]],
        "serverSide": true,
        "ajax": {
            "url": load_location_list,
            "data": {
                "exclude_location_id": exclude_location_id
            },
        },
        "columns": [
            {"data": "code"},
            {"data": "name"},
            {"data": "address"},
            {
                "orderable": false,
                "data": null,
                "render": function (data, type, full, meta) {
                    return '<input type="radio" name="choice-location" id="' +
                        full.id + '" class="call-checkbox" value="' + full.code + '">';
                }
            }
        ]
    });
}


$('#id_in_location').on('click', function () {
    if ($(this).prop('readonly') == false) {
        $('#detectLocation').val($(this).data('id'));
        $('#modalLocation').modal('show');
        $('#tblLocation').DataTable().destroy();
        var exclude_location_id = 0;
        if ($('#id_out_location_id').val() != '') {
            exclude_location_id = $('#id_out_location_id').val();
        }
        load_location(exclude_location_id);
    }
});


$('#btnAddLocation').on('click', function () {
    var location_id = $("input[name='choice-location']:checked").attr('id');
    var location_code = $("input[name='choice-location']:checked").val();
    if ($('#detectLocation').val() == 'id_in_location') {
        $('#id_in_location_id').val(location_id);
        $('#id_in_location').val(location_code);
    } else {
        $('#id_out_location_id').val(location_id);
        $('#id_out_location').val(location_code);
    }
});


$('#id_out_location').on('click', function () {
    if ($(this).prop('readonly') == false) {
        $('#detectLocation').val($(this).data('id'));
        $('#modalLocation').modal('show');
        $('#tblLocation').DataTable().destroy();
        var exclude_location_id = 0;
        if ($('#id_in_location_id').val() != '') {
            exclude_location_id = $('#id_in_location_id').val();
        }
        load_location(exclude_location_id);
    }
});


$('#btnAddTransCode').on('click', function () {
    $('#id_in_location').val('');
    $('#id_in_location_id').val('');
    $('#id_out_location').val('');
    $('#id_out_location_id').val('');
    var selected_id = $("input[name='choices']:checked").attr('id');
    if (selected_id != undefined) {
        $('#btnOpenItemDialog').removeAttr('disabled');
        var nRow = $("input[name='choices']:checked").parents('tr')[0];
        var jqInputs = $('td', nRow);
        $('#tblTransCode').DataTable().rows(nRow).every(function () {
            var data = this.data();
            $('#id_transaction_code').val(data['code']);
            $('#id_transaction_code_id').val(selected_id);
            if (data['auto_generate'] == "False") {
                $('#id_document_number').prop('readonly', false);
            } else {
                $('#id_document_number').prop('readonly', true);
            }
            $('#id_io_flag option').each(function () {
                $(this).removeAttr('selected');
                $(this).removeAttr('disabled');
                if ($(this).text() == data['io_flag']) {
                    $(this).attr('selected', 'selected');
                    if (data['io_flag'] == 'IN') {
                        $('#id_in_location').prop('readonly', false);
                        $('#id_out_location').prop('readonly', true);
                    } else if (data['io_flag'] == 'OUT') {
                        $('#id_in_location').prop('readonly', true);
                        $('#id_out_location').prop('readonly', false);
                    } else if (data['io_flag'] == 'Transfer') {
                        $('#id_in_location').prop('readonly', false);
                        $('#id_out_location').prop('readonly', false);
                    } else {
                        $('#id_in_location').prop('readonly', true);
                        $('#id_out_location').prop('readonly', true);
                    }
                }
                $('#id_io_flag option:not(:selected)').attr('disabled', true);
            });
        });
    }
});


$('#btnOpenItemDialog').on('click', function () {
    var in_location_id = $('#id_in_location_id').val();
    var out_location_id = $('#id_out_location_id').val();
    $('#tbldataItems').DataTable().destroy();
    $('#tbldataItems').dataTable({
        "order": [[0, "desc"]],
        "serverSide": true,
        "ajax": {
            "url": load_stock_transaction_items_list,
            "data": {
                "in_location_id": in_location_id,
                "out_location_id": out_location_id
            },
        },
        "columns": [
            {"data": "item"},
            {"data": "item_code"},
            {"data": "stock_qty"},
            {"data": "location"},
            {
                "orderable": false,
                "data": null,
                "render": function (data, type, row, meta) {
                    return '<input type="checkbox" name="choices" id="' + row.id + '"'
                        + 'class="call-checkbox" value="' + row.item_code + '"></td>';
                }
            }
        ]
    });
});


function cloneMore(selector, type, allVals) {
    var i = 0;
    var display = $(selector).css("display");
    if (display == 'none') {
        //show first row of table and set Item, Price of dialog
        $(selector).removeAttr("style")
        $(selector).find('label').each(function () {
            var name = $(this).attr('name').replace('-' + (total - 1) + '-', '-' + total + '-');
            var id = 'id_' + name;
            $(this).attr({'name': name, 'id': id}).val('').removeAttr('checked');
        });
        findInput = $(selector).find('input');
        currentLabel = $(selector).closest('tr').find('label');
        //add value to Input
        findInput[0].value = 1;
        findInput[1].value = allVals[i].item_code; //Item Code
        findInput[2].value = allVals[i].item_id; //Item Code
        findInput[4].value = 0;
        findInput[7].value = allVals[i].stock_qty;

        currentLabel[0].textContent = findInput[0].value; // Line Number
        currentLabel[1].textContent = findInput[1].value; // Item Code

        //if selected items > 1
        i = 1;
    }
    $('#btnSave').removeAttr('disabled');
    for (i; i < allVals.length; i++) {

        if (allVals[i].id != 0) {
            var newElement = $(selector).clone(true);
            var total = $('#id_' + type + '-TOTAL_FORMS').val();
            newElement.removeAttr("style")
            newElement.find(':input').each(function () {
                var name = $(this).attr('name').replace('-' + (total - 1) + '-', '-' + total + '-');
                var id = 'id_' + name;
                $(this).attr({'name': name, 'id': id}).val('').removeAttr('checked');
            });
            newElement.find('label').each(function () {
                var name = $(this).attr('name').replace('-' + (total - 1) + '-', '-' + total + '-');
                var id = 'id_' + name;
                $(this).attr({'name': name, 'id': id}).val('').removeAttr('checked');
            });

            //Set selected price of dialog to Price Column
            var a = newElement.find('input');
            currentRow = newElement.closest('tr').find('label');
            if (a.length > 1) {
                a[0].value = parseInt($(selector).find('input')[0].value) + 1;
                a[1].value = allVals[i].item_code;
                a[2].value = allVals[i].item_id;
                a[4].value = 0;
                a[7].value = allVals[i].stock_qty;

                currentRow[0].textContent = a[0].value; // Line Number
                currentRow[1].textContent = a[1].value; // Item Code
            }
            total++;
            $('#id_' + type + '-TOTAL_FORMS').val(total);
            $(selector).after(newElement);
        }
    }
};


$('#btnAddItems').on('click', function () {
    var allVals = [];
    var table = $('#tbldataItems').DataTable();
    var rowcollection = table.$(".call-checkbox:checked", {"page": "all"});
    rowcollection.each(function (index, elem) {
        allVals.push({
            item_id: elem.id, //Item ID
            item_code: elem.value,
            stock_qty: elem.parentElement.parentElement.cells[2].textContent
        });
    });
    if (allVals.length > 0) {
        cloneMore('#dynamic-table tr.gradeX:last', 'formset_item', allVals);
    }
    $(this).attr('data-dismiss', 'modal');
    $('#items_error').css('display', 'none');
});

function fnDisableButton() {
    $('#btnSave').attr('disabled', true);
    $('#btnSend').attr('disabled', true);
    $('#btnSendForEdit').attr('disabled', true);
    $('#btnDelete').attr('disabled', true);
}

function fnEnableButton() {
    $('#btnSave').removeAttr('disabled');
    $('#btnSend').removeAttr('disabled');
    $('#btnSendForEdit').removeAttr('disabled');
    $('#btnDelete').removeAttr('disabled');
}

// event change quantity
$('#dynamic-table tr.gradeX').each(function () {
    currentRow = $(this).closest('tr').find('input');
    $mainElement = '#' + currentRow[3].id;
    var order_type = $('#order_type').text();
    var order_id = $('#order_id').text();
    $($mainElement).change(function () {
        var out_location_id = $('#id_out_location_id').val();
        currentRow = $(this).closest('tr').find('input');
        currentLabel = $(this).closest('tr').find('label');
        var quantity = currentRow[3].value;
        var price = currentRow[4].value;
        var amount = 0;
        if (quantity < 1) {
            $('#items_error').removeAttr('style');
            $('#items_error').text('The quantity of product ' + currentRow[1].value + ' must greater than 0');
            $(this).closest('tr').attr('style', 'background-color: yellow !important');
            currentRow[5].value = 0;
            currentLabel[2].textContent = 0;
            fnDisableButton();
            $('#dynamic-table tr.gradeX').each(function () {
                $(this).closest('tr').find('input').not(currentRow[3]).attr('disabled', true);
            });
        } else if (out_location_id != '') {
            if (parseInt(quantity) > parseInt(currentRow[7].value)) {
                $('#items_error').removeAttr('style');
                $('#items_error').text('The quantity of product ' + currentRow[1].value + ' must less than stock Quantity (' + currentRow[7].value + ')');
                $(this).closest('tr').attr('style', 'background-color: yellow !important');
                currentRow[5].value = 0;
                currentLabel[2].textContent = 0;
                fnDisableButton();
                $('#dynamic-table tr.gradeX').each(function () {
                    $(this).closest('tr').find('input').not(currentRow[3]).attr('disabled', true);
                });
            } else {
                amount = quantity * price
                currentRow[5].value = amount.toFixed(6);
                currentLabel[2].textContent = currentRow[5].value;
                $('#dynamic-table tr.gradeX').each(function () {
                    $(this).closest('tr').find('input').removeAttr('disabled');
                });
                $(this).closest('tr').removeAttr('style');
                $('#items_error').css('display', 'none');
                $('#items_error').css('display', 'none');
                fnEnableButton();
            }
        } else {
            amount = quantity * price;
            currentRow[5].value = amount.toFixed(6);
            currentLabel[2].textContent = currentRow[5].value;
            $('#dynamic-table tr.gradeX').each(function () {
                $(this).closest('tr').find('input').removeAttr('disabled');
            });
            $(this).closest('tr').removeAttr('style');
            $('#items_error').css('display', 'none');
            $('#items_error').css('display', 'none');
            fnEnableButton();
        }
    });
});


// event change price
$('#dynamic-table tr.gradeX').find('input').each(function () {
    currentRow = $(this).closest('tr').find('input');
    $priceElement = '#' + currentRow[4].id;
    $($priceElement).change(function () {
        currentRow = $(this).closest('tr').find('input');
        currentLabel = $(this).closest('tr').find('label');
        var amount = 0;
        if (currentRow[4].value < 0) {
            $('#items_error').removeAttr('style');
            $('#items_error').text('The price of product ' + currentRow[1].value + ' must greater than 0');
            $(this).closest('tr').attr('style', 'background-color: yellow !important');
            currentRow[5].value = 0;
            currentLabel[2].textContent = 0;
            fnDisableButton();
            $('#dynamic-table tr.gradeX').each(function () {
                $(this).closest('tr').find('input').not(currentRow[4]).attr('disabled', true);
            });
        } else {
            amount = currentRow[3].value * currentRow[4].value;
            currentRow[5].value = amount.toFixed(6);
            currentLabel[2].textContent = currentRow[5].value;
            $('#dynamic-table tr.gradeX').each(function () {
                $(this).closest('tr').find('input').removeAttr('disabled');
            });
            $(this).closest('tr').removeAttr('style');
            $('#items_error').css('display', 'none');
            $('#items_error').css('display', 'none');
            fnEnableButton();
        }
    });
});