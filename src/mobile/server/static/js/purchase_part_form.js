/**
 * Created by trung.phan on 10/20/2016.
 */

var dataSet = [
    // [ "Tiger Nixon", "System Architect", "Edinburgh", "5421", "2011/04/25", "$320,800", "", "", "1"],
    // [ "Garrett Winters", "Accountant", "Tokyo", "8422", "2011/07/25", "$170,750", "", "", "2"],
];

var exclude_supplier = [];
var supplier_table_list = $('#supplier-table-list').DataTable();
var exclude_item_list = {};
var btnSaveItem = $('#btnSaveItem');
var emptySupplierError = $('#empty_supplier_error');
var item_id = $('#item_id');
var existing_suppliers = $('#existing_suppliers');
var today = new Date();
var dd = today.getDate();
var mm = today.getMonth() + 1;
var yyyy = today.getFullYear();
var hdSuppliersJson = $('#suppliers_json');
if (dd < 10) {
    dd = '0' + dd
}
if (mm < 10) {
    mm = '0' + mm
}


$(document).ready(function () {
    $('form input').on('keypress', function (e) {
        return e.which !== 13;
    });
    emptySupplierError.css("display", "none");
    // initialize list of suppliers on btn open dialog
    function get_supplierslist_DT() {
        var filter = $('#txtFilter').val();
        if (exclude_supplier.length > 0) {
            exclude_item_list = JSON.stringify(exclude_supplier);
        }
        supplier_table_list = $('#supplier-table-list').DataTable({
            "order": [[0, "desc"]],
            "bLengthChange": false,
            "iDisplayLength": 5,
            "serverSide": true,
            "ajax": {
                "url": "/items/supplierlist/pagination",
                "type": 'POST',
                "data": {
                    "csrfmiddlewaretoken": $('input[name="csrfmiddlewaretoken"]').val(),
                    "exclude_item_list": exclude_item_list,
                    "filter": filter
                }
            },
            "columns": [
                {"data": "code", "sClass": "text-left"},
                {"data": "name", "sClass": "text-left"},
                {"data": "country_code", "sClass": "text-left"},
                {"data": "currency_code", "sClass": "text-left"},
                {
                    "orderable": false,
                    "data": null,
                    "render": function (data, type, full, meta) {
                        return '<input type="checkbox" name="choices" id="' + full.id + '" class="call-checkbox" value="' + full.currency_id + '">';
                    }
                }
            ]
        });
    }

    $('#txtFilter').on('keypress', function (e) {
        if (e.which === 13) {
            var txtFilter = $('#txtFilter').val();
            $.ajax({
                method: "POST",
                url: '/items/get_supplier_info1/',
                dataType: 'JSON',
                data: {
                    'csrfmiddlewaretoken': $('input[name="csrfmiddlewaretoken"]').val(),
                    'supplier_code': txtFilter,
                },
                responseTime: 200,
                success: function (json) {
                    console.log(json);
                    if (json['Fail']) {
                        alert("Can not find Supplier. Please type correct Supplier Code!")
                    }
                    if (json['id']) {
                        var new_data = [json['code'], json['name'], json['currency_code'], '', '', '', '', '', json['id']];
                        dataSet.push(new_data);
                        supplier_table.row.add(new_data).draw(false);
                        exclude_supplier.push(json['id']);

                        var class_effective_date = $('.effective-date');
                        // class_effective_date.datepicker('destroy');
                        class_effective_date.datepicker({
                            format: 'yyyy-mm-dd',
                            autoclose: true
                        });
                        validate_form();
                    }
                    $('#txtFilter').val('');
                }
            });
        }
    });

    $('#id_code').on('keypress', function (e) {
        if (e.which === 13) {
            var item_code = $('#id_code').val();
            $.ajax({
                method: "POST",
                url: '/items/get_item_info/',
                dataType: 'JSON',
                data: {
                    'csrfmiddlewaretoken': $('input[name="csrfmiddlewaretoken"]').val(),
                    'item_code': item_code,
                    'item_type': 2,
                },
                responseTime: 200,
                complete: function (xmlHttp) {
                    if (xmlHttp.status == 278) {
                        window.location.href = xmlHttp.getResponseHeader("Location").replace(/\?.*$/, "?next=" + window.location.pathname);
                    }
                },
                success: function (json) {
                    console.log(json);
                    $('#id_code').next('.inputs').focus();
                }
            });
        }
    });
    // supplier_table_list = get_supplierslist_DT();
    // send ajax to get all suppliers in modal
    $('#btnAddSupplierDialog').on('click', function () {
        if (supplier_table_list) {
            supplier_table_list.clear();
            supplier_table_list.destroy();
        }
        get_supplierslist_DT();
    });
    // Initialize datatable
    if (item_id.val()) {
        dataSet = eval(existing_suppliers.val());
        for (i = 0; i < dataSet.length; i++) {
            exclude_supplier.push(dataSet[i][8]);
        }
    }
    var supplier_table = $('#supplier_table').DataTable({
        data: dataSet,
        "paging": false,
        "info": false,
        "filter": false,
        "columnDefs": [
            {
                "targets": 0,
                "sClass": "small-width"
            },
            {
                "targets": 1,
                "sClass": "medium-width"
            },
            {
                "targets": 2,
                "sClass": "tiny-width"
            },
            {
                "targets": 8,
                "orderable": false,
                "data": null,
                "sClass": "custom_align",
                "defaultContent": '<button type="button" class="removerow btn btn-white fa fa-minus code_style" value="Remove"></button>'
                // '<div class="btn-group dropup">' +
                //     '<button type="button" class="btn btn-primary btn-sm">Remove</button>' +
                // '</div>'
            },
            {
                "targets": 3,
                "sClass": "small-width",
                "render": function (data, type, row) {
                    return '<input name="' + row[8] + '" class="form-control pos-num small-width" type="number" step="0.000001" value="' + data + '" required>'
                }
            },
            {
                "targets": 4,
                "sClass": "small-width",
                "render": function (data, type, row) {
                    return '<input name="' + row[8] + '" class="form-control small-width" type="number" step="0.01" value="' + data + '" required>'
                }
            },
            {
                "targets": 6,
                "sClass": "small-width",
                "render": function (data, type, row) {
                    return '<input name="' + row[8] + '" class="form-control pos-num small-width" type="number" step="0.000001" value="' + data + '" required>'
                }
            },
            {
                "targets": 5,
                "sClass": "small-width",
                "render": function (data, type, row) {
                    return '<input name="' + row[8] + '" class="form-control form-control-inline input-medium default-date-picker effective-date small-width" value="' + data + '" required>'
                }
            },
            {
                "targets": 7,
                "sClass": 'small-width',
                "render": function (data, type, row) {
                    return yyyy + '-' + mm + '-' + dd
                }
            }
        ],
        "order": []
    });
    $('.effective-date').datepicker({
        format: 'yyyy-mm-dd',
        autoclose: true
    });
    // button remove supplier
    $('#supplier_table tbody').on('click', 'button', function () {
        var row = supplier_table.row($(this).parents('tr'));
        data = row.data();
        dataSet = jQuery.grep(dataSet, function (n, i) {
            return n[8] !== data[8];
        });
        supplier_table.row($(this).parents('tr')).remove().draw(false);
        // pop this supplier id out of exclude_supplier list
        var index = exclude_supplier.indexOf(data[8]);
        if (index > -1) {
            exclude_supplier.splice(index, 1);
        }
    });
    $('#supplier_table tbody').on('change', 'input.pos-num', function () {
        debugger;
        console.log($(this).prop('name'));
        var e = $(this);
        if ($(this).val() > 0) {
            $(this).parents('tr').removeAttr('style');
            $('#supplier_table tr').each(function () {
                $(this).find('input').removeAttr('disabled');
                $(this).find('.removerow').removeAttr('disabled');
            });
            $('#btnSaveItem').removeAttr('disabled');
            $('#btnAddSupplierDialog').removeAttr('disabled');
            if ($('#item_form_submit > div:nth-child(15) > div > a.btn.btn-danger')) {
                $('#item_form_submit > div:nth-child(15) > div > a.btn.btn-danger').removeAttr('style');
            }
            // $('#btnDelete').removeAttr('disabled');
        } else {
            $(this).parents('tr').attr('style', 'background-color: red !important');
            $('#btnSaveItem').attr('disabled', true);
            $('#btnAddSupplierDialog').attr('disabled', true);
            if ($('#item_form_submit > div:nth-child(15) > div > a.btn.btn-danger')) {
                $('#item_form_submit > div:nth-child(15) > div > a.btn.btn-danger').css('display', 'none');
            }
            $('#supplier_table tr').each(function () {
                $(this).find('input').not(e).attr('disabled', true);
                $(this).find('.removerow').attr('disabled', true);
            })
        }
    });
    // Button Select Supplier
    $('#btnSupplierSelect').on('click', function () {
        // var currency_select_id = $("input[name='choices']:checked").val();
        // var supplier_select_id = $("input[name='choices']:checked").attr('id');
        // var nRow = $("input[name='choices']:checked").parents('tr')[0];
        // var jqInputs = $('td', nRow);

        var rowcollection = supplier_table_list.$(".call-checkbox:checked", {"page": "all"});
        rowcollection.each(function (index, elem) {
            var row = $(this).parents('tr');
            var row_tds = row.find('td');
            supplier_id = row.find('td').last().find('input').attr('id');
            var new_data = [row_tds[0].innerText, row_tds[1].innerText, row_tds[3].innerText, '', '', '', '', '', supplier_id.toString()];
            dataSet.push(new_data);

            supplier_table.row.add(new_data).draw(false);

            // row.attr("style", "display: none");
            row.find('td').last().find('input').removeAttr('checked');
            exclude_supplier.push(supplier_id);
        });

        $(this).attr('data-dismiss', 'modal');

        var class_effective_date = $('.effective-date');
        // class_effective_date.datepicker('destroy');
        class_effective_date.datepicker({
            format: 'yyyy-mm-dd',
            autoclose: true
        });
        validate_form();
    });
    function validate_form() {
        if (dataSet.length > 0) {
            btnSaveItem.removeAttr('disabled');
            emptySupplierError.css('display', 'none');
            if ($('#item_form_submit > div:nth-child(15) > div > a.btn.btn-danger')) {
                $('#item_form_submit > div:nth-child(15) > div > a.btn.btn-danger').removeAttr('style');
            }
            // return true;
        } else {
            btnSaveItem.attr('disabled', true);
            emptySupplierError.removeAttr('style');
            $('html, body').animate({
                scrollTop: emptySupplierError.offset().top
            }, 1000);
            if ($('#item_form_submit > div:nth-child(15) > div > a.btn.btn-danger')) {
                $('#item_form_submit > div:nth-child(15) > div > a.btn.btn-danger').css('display', 'none');
            }
            return false;
        }
    }

    // button submit form
    btnSaveItem.on('click', function () {
        if (dataSet.length > 0) {
            btnSaveItem.removeAttr('disabled');
            emptySupplierError.css('display', 'none');
            if ($('#item_form_submit > div:nth-child(15) > div > a.btn.btn-danger')) {
                $('#item_form_submit > div:nth-child(15) > div > a.btn.btn-danger').removeAttr('style');
            }
            var data = supplier_table.$('input, select').serialize();
            // dataSet = [];
            // supplier_table.rows().every(function(rowIdx, tableLoop, rowLoop){
            //     dataSet.push(this.data());
            //     console.log(dataSet);
            // });

            var myArray = data.toString().split('&');
            var myData = [];
            for (var i = 0; i < myArray.length; i++) {
                var new_element = myArray[i].split('=');
                if (myData.length > 0) {
                    var find_element = $.grep(myData, function (n, i) {
                        return n[0] == new_element[0];
                    });
                    if (find_element.length > 0) {
                        find_element[0].push(new_element[1]);
                    } else {
                        myData.push([new_element[0], new_element[1]]);
                    }
                } else {
                    myData.push([new_element[0], new_element[1]]);
                }
            }
            hdSuppliersJson.val(JSON.stringify(myData));
        } else {
            btnSaveItem.attr('disabled', true);
            emptySupplierError.removeAttr('style');
            $('html, body').animate({
                scrollTop: emptySupplierError.offset().top
            }, 1000);
            if ($('#item_form_submit > div:nth-child(15) > div > a.btn.btn-danger')) {
                $('#item_form_submit > div:nth-child(15) > div > a.btn.btn-danger').css('display', 'none');
            }
            return false;
        }
    });
});
