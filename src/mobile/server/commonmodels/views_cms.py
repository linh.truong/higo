#!/usr/bin/env python
# -*- coding: utf-8 -*-
import json
import datetime

import re
from django.contrib import messages
from django.db import connection
from django.db.models import Q
from django.http import HttpResponseRedirect, HttpResponse
from django.shortcuts import render

from utils.message import get_msg_content
from utils import constants as const
from utils import utility as util
from foods.forms import FoodInfoForm, FoodCatInfoForm
from topics.forms import TopicInfoForm, TopicCatInfoForm
from coupons.forms import CouponInfoForm
from commonmodels.models import Foods, FoodCategories, Topics, TopicCategories, Coupons, Prizes


# ---------------------------------------------------------------
# ======================= COMMON FUNCTION =======================
# ---------------------------------------------------------------

# Convert model to dictionary
def model_to_dict_display(model_data, map_disp):
    _dict = {}

    # build mapping relation fields
    _rel_map = {}
    for _map in map_disp:
        k = _map.rfind('_')
        _field = _map[:k]
        _key = _map[k + 1:]
        if _field not in _rel_map:
            _rel_map[_field] = {_key: _map}
        else:
            _rel_map[_field].update({_key: _map})

    # loop each model items for convert to dict
    for field in model_data._meta.get_fields():
        if field.is_relation is True:  # relation field
            _founds = [_rel_map.__getitem__(i) for i in _rel_map if i == field.name]
            if not _founds:
                continue
            _obj = model_data.__getattribute__(field.name)
            for key, _map in _founds[0].items():
                _dict[_map] = _obj.__getattribute__(key)

        elif field.get_internal_type() == 'DateTimeField':  # datetime field
            # convert date to string
            _org = model_data.__getattribute__(field.name)
            _val = _org.strftime("%Y-%m-%d") if _org else ''
            _dict[field.name] = _val
        elif field.get_internal_type() == 'TimeField':  # time field
            # convert time to string
            _org = model_data.__getattribute__(field.name)
            if _org == datetime.time(0, 0):
                _dict[field.name] = "00:00"
            else:
                _val = _org.strftime("%H:%M") if _org else ''
                _dict[field.name] = _val

        else:
            _dict[field.name] = model_data.__getattribute__(field.name).__str__()

    return _dict


# Get all data to display in CMS list view
def get_list_display(request, abs_model, mapping):
    if request.is_ajax():
        draw = request.GET['draw']
        start = request.GET['start']
        length = request.GET['length']
        search = request.GET['search[value]']

        # Get all list by common
        _curr_res_id = mapping.get('curr_res_id', None)
        if _curr_res_id is not None:
            news_list = abs_model.objects.filter(restaurant_id=_curr_res_id, is_delete=const.ACTIVE_FLAG)
        else:
            news_list = abs_model.objects.filter(is_delete=const.ACTIVE_FLAG)

        # Count total record
        records_total = news_list.count()

        # Search
        if search:  # Filter data base on search
            fields = mapping.get('search', None)
            if fields:
                queries = []

                for f in fields:
                    # build Q structure
                    kwargs = {str('%s__contains' % f): str('%s' % search)}
                    queries.append(Q(**kwargs))

                # make Q queries
                if len(queries) > 0:
                    q = Q()
                    for query in queries:
                        q = q | query

                    # filter QuerySet
                    news_list = news_list.filter(q)

        # Filtered counter
        records_filtered = news_list.count()

        # Order by list_limit base on order_dir and order_column
        news_column = request.GET['order[0][column]']
        map_sort = mapping.get('sort', {})
        column_name = map_sort.get(int(news_column), 'id')

        news_dir = request.GET['order[0][dir]']
        if news_dir == "asc":
            res_list = news_list.order_by(column_name, '-id')[int(start):(int(start) + int(length))]
        else:
            res_list = news_list.order_by('-' + column_name, '-id')[int(start):(int(start) + int(length))]

        # Create data list
        map_disp = mapping.get('map', [])
        records = []
        number = int(start) if start else 0
        for res_item in res_list:
            data = {}
            number += 1
            data["stt"] = number

            # convert model to dict
            _dict = model_to_dict_display(res_item, map_disp)
            data.update(_dict)

            # bind for display
            records.append(data)

        # Bind response data
        content = {
            "draw": draw,
            "data": records,
            "recordsTotal": records_total,
            "recordsFiltered": records_filtered
        }
        json_content = json.dumps(content, ensure_ascii=False)
        return HttpResponse(json_content, content_type='application/json')
    else:
        return HttpResponseRedirect(const.CMS_LOGIN_URL)


# Using stored procedure for get all data to display in CMS list view
def prc_get_list_display(request, abs_model, mapping, prc_name, *params):
    if request.is_ajax():
        cursor = None

        try:
            draw = request.GET['draw']
            start = request.GET['start']
            length = request.GET['length']
            search = request.GET['search[value]']

            # Get all list by common
            cursor = connection.cursor()
            if len(params) > 0:
                ret = cursor.callproc(prc_name, params)
            else:
                ret = cursor.callproc(prc_name)

            # grab the results
            results = cursor.fetchall()
            cursor.close()

            # wrap the results up into model objects
            prc_list = [abs_model(*row) for row in results]

            # collect list of ids
            id_list = [rec.id for rec in prc_list]

            # acquire query set
            news_list = abs_model.objects.filter(id__in=id_list)

            # Count total record
            records_total = news_list.__len__()

            # Search
            if search:  # Filter data base on search
                fields = mapping.get('search', None)
                if fields:
                    queries = []

                    for f in fields:
                        # build Q structure
                        kwargs = {str('%s__contains' % f): str('%s' % search)}
                        queries.append(Q(**kwargs))

                    # make Q queries
                    if len(queries) > 0:
                        q = Q()
                        for query in queries:
                            q = q | query

                        # filter QuerySet
                        news_list = news_list.filter(q)

            # Filtered counter
            records_filtered = news_list.__len__()

            # Order by list_limit base on order_dir and order_column
            news_column = request.GET['order[0][column]']
            map_sort = mapping.get('sort', {})
            column_name = map_sort.get(int(news_column), 'id')

            news_dir = request.GET['order[0][dir]']
            if news_dir == "asc":
                res_list = news_list.order_by(column_name, '-id')[int(start):(int(start) + int(length))]
            else:
                res_list = news_list.order_by('-' + column_name, '-id')[int(start):(int(start) + int(length))]

            # Create data list
            map_disp = mapping.get('map', [])
            records = []
            number = int(start) if start else 0
            for res_item in res_list:
                data = {}
                number += 1
                data["stt"] = number

                # convert model to dict
                _dict = model_to_dict_display(res_item, map_disp)
                data.update(_dict)

                # bind for display
                records.append(data)

            # Bind response data
            content = {
                "draw": draw,
                "data": records,
                "recordsTotal": records_total,
                "recordsFiltered": records_filtered
            }
            json_content = json.dumps(content, ensure_ascii=False)
            return HttpResponse(json_content, content_type='application/json')

        except Exception as e:
            print("error" + str(e))
            messages.add_message(request, messages.ERROR, e, extra_tags=abs_model._meta.object_name)
            return render(request, 'error.html')

        finally:
            cursor.close()

    else:
        return HttpResponseRedirect(const.CMS_LOGIN_URL)


# Process insert or update data in CMS new & edit
def action_insert_or_update(request, abs_model, abs_form, url_name, _id=None):
    url_return = url_name + '/add.html' if not _id else url_name + '/edit.html'
    url_next = '/cms/' + url_name + '/edit/'
    tag_name = 'add_' + url_name if not _id else 'edit_' + url_name

    # Init Form
    if request.method == 'GET':
        context = {}

        if 'delete' in request.path:
            model = abs_model.objects.get(pk=_id)
            model.is_delete = True
            model.save()
            return HttpResponseRedirect(request.META.get('HTTP_REFERER'))

        if not _id:
            # Insert mode
            model = abs_model()
            form_info = abs_form()
        else:
            # Update mode
            if util.is_num(_id) is False:
                messages.add_message(request, messages.ERROR, url_name + ' Id incorrect', extra_tags=tag_name)
                return render(request, 'error.html')

            model = abs_model.objects.get(pk=_id)

            # check relation & typical fields
            _keys = abs_model._meta.get_fields()
            _typical_list = ['week_days', 'DateTimeField', 'TimeField']

            # analyze typical fields
            _founds = [i.name for e in _typical_list for i in _keys if e == i.name or e == i.get_internal_type()]

            for _found in _founds:
                # mapping multi selected Week_days
                if _found == 'week_days':
                    model.week_days = model.week_days.split(',')

                # format date field
                elif re.search('date', _found):
                    _org = model.__getattribute__(_found)
                    _cnv = _org.strftime("%Y-%m-%d") if _org else ''
                    model.__setattr__(_found, _cnv)

                # format time field
                elif re.search('time', _found):
                    _org = model.__getattribute__(_found)
                    if _org == datetime.time(0, 0):
                        model.__setattr__(_found, "00:00")
                    else:
                        _cnv = _org.strftime("%H:%M") if _org else ''
                        model.__setattr__(_found, _cnv)

            form_info = abs_form(instance=model)

        # Set return context
        context[url_name] = model
        context['form_info'] = form_info
        context['request_method'] = request.method

        return render(request, url_return, context)

    # Do Action
    else:
        url_return = url_name + '/list.html'
        url_next = '/cms/' + url_name + '/list/'
        context = {}

        if 'delete' in url_name:
            model = abs_model.objects.get(pk=_id)
            model.is_delete = True
            model.save()
            return HttpResponseRedirect(request.META.get('HTTP_REFERER'))

        if not _id:
            # Insert mode
            model = abs_model()

        else:
            # Update mode
            if util.is_num(_id) is False:
                messages.add_message(request, messages.ERROR, url_name + ' Id incorrect', extra_tags=tag_name)
                return render(request, 'error.html')

            model = abs_model.objects.get(pk=_id)
            # set active
            if request.POST.get('isDelete'):
                model.is_delete = const.ACTIVE_FLAG
            else:
                model.is_delete = const.DEL_FLAG

        try:
            form_info = abs_form(request.POST, instance=model)

            if form_info.is_valid():

                model = form_info.save(commit=False)

                # Check duplicate update Food category
                if url_name == 'foods_cat':
                    model == FoodCategories.objects.get(pk=_id)
                    form_food_cat = FoodCatInfoForm(request.POST, instance=model)
                    check_dup_food_cat = chk_dup_upd(FoodCategories, form_food_cat, model.id, ['name', 'name_cn'])
                    if check_dup_food_cat:
                        messages.error(request, check_dup_food_cat['errors'])
                        context[url_name] = model
                        context['form_info'] = form_food_cat
                        context['request_method'] = request.method
                        return render(request, 'foods_cat/edit.html', context)

                # Check duplicate update Food
                if url_name == 'foods':
                    model == Foods.objects.get(pk=_id)
                    form_food = FoodInfoForm(request.POST, instance=model)
                    check_dup_food = chk_dup_upd(Foods, form_food, model.id, ['name', 'name_cn'])
                    if check_dup_food:
                        messages.error(request, check_dup_food['errors'])
                        context[url_name] = model
                        context['form_info'] = form_food
                        context['request_method'] = request.method
                        return render(request, 'foods/edit.html', context)

                # Check duplicate update Topic category
                if url_name == 'topics_cat':
                    model == TopicCategories.objects.get(pk=_id)
                    form_topic_cat = TopicCatInfoForm(request.POST, instance=model)
                    check_dup_topic_cat = chk_dup_upd(TopicCategories, form_topic_cat, model.id,
                                                      ['name', 'name_cn'])
                    if check_dup_topic_cat:
                        messages.error(request, check_dup_topic_cat['errors'])
                        context[url_name] = model
                        context['form_info'] = form_topic_cat
                        context['request_method'] = request.method
                        return render(request, 'topics_cat/edit.html', context)

                # Check duplicate update Topic
                if url_name == 'topics':
                    model == Topics.objects.get(pk=_id)
                    form_topic = TopicInfoForm(request.POST, instance=model)
                    check_dup_topic = chk_dup_upd(Topics, form_topic, model.id, ['subject', 'subject_cn'])
                    if check_dup_topic:
                        messages.error(request, check_dup_topic['errors'])
                        context[url_name] = model
                        context['form_info'] = form_topic
                        context['request_method'] = request.method
                        return render(request, 'topics/edit.html', context)

                # Check duplicate update Coupon
                if url_name == 'coupons':
                    model == Coupons.objects.get(pk=_id)
                    form_cp = CouponInfoForm(request.POST, instance=model)
                    check_dup_cp = chk_dup_upd(Coupons, form_cp, model.id, ['number', 'name', 'name_cn'])
                    if check_dup_cp:
                        messages.error(request, check_dup_cp['errors'])
                        context[url_name] = model
                        context['form_info'] = form_cp
                        context['request_method'] = request.method
                        return render(request, 'coupons/edit.html', context)

                # check relation & typical fields
                _keys = abs_model._meta.get_fields()
                _typical_list = ['restaurant', 'week_days', 'image_path']

                # analyze typical fields
                _founds = [i.name for e in _typical_list for i in _keys if e == i.name]

                for _found in _founds:
                    # mapping with session Restaurant
                    if _found == 'restaurant':
                        model.restaurant_id = request.session[const.SESSION_CMS_CURR_RES_ID]

                    # mapping multi selected Week_days
                    elif _found == 'week_days':
                        model.week_days = ','.join(request.POST.getlist('week_days'))

                    # mapping uploading image
                    elif _found == 'image_path':
                        image_file = request.FILES.get('image_path', False)
                        if image_file:
                            model.image_path.delete(False)
                            model.image_path.save(image_file.name, image_file, False)

                # Save to DB
                model.save()

                return HttpResponseRedirect(url_next)

            else:
                messages.add_message(request, messages.ERROR, form_info.errors, extra_tags=tag_name)

                context[url_name] = model
                context['form_info'] = form_info
                context['request_method'] = request.method
                return render(request, url_return, context)

        except Exception as e:
            print("error" + str(e))
            messages.add_message(request, messages.ERROR, e, extra_tags=tag_name)
            return render(request, 'error.html')


# ---------------------------------------------------------------
# ======================= COMMON CHECKING =======================
# ---------------------------------------------------------------


# Check duplicate for insert
def chk_dup_ins(model, form, check_list):
    context = {}
    keys = []

    # detect string or list
    if isinstance(check_list, str):
        keys.append(check_list)
    elif isinstance(check_list, list):
        keys = check_list

    # bind condition
    for f in keys:
        v = form.data.get(f, '')

        # search data
        _obj = util.dynamic_query(model, [f], [v], 'and')

        # search founded -> existed in DB
        if _obj:
            context['errors'] = get_msg_content('insert_fail_existed', f.title(), v)
            context['form_info'] = form
            return context

    return context


# Check duplicate for update
def chk_dup_upd(model, form, upd_id, check_list):
    context = {}
    keys = []

    # detect string or list
    if isinstance(check_list, str):
        keys.append(check_list)
    elif isinstance(check_list, list):
        keys = check_list

    # bind condition
    params = []
    for f in keys:
        v = form.data.get(f, '')

        if v:
            parm = util.QueryParm(f, v, False, 'or')
            params.append(parm)

        if len(params) > 0:
            parm = util.QueryParm('pk', upd_id, True, 'and')
            params.append(parm)

        # search data
        _obj = util.dynamic_ctm_query(model, params)

        # search founded -> existed in DB
        if _obj:
            context['errors'] = get_msg_content('update_fail_existed', f.title(), v)
            context['form_info'] = form
            return context

    return context
