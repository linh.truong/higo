from django.contrib import admin

# Register your models here.
from .models import *


class AreasAdmin(admin.ModelAdmin):
    list_display = (
        'name',
        'name_cn',
        'is_delete',
    )
    search_fields = ('name',)
admin.site.register(Areas, AreasAdmin)


class ContactsAdmin(admin.ModelAdmin):
    list_display = (
        'restaurant',
        'name',
        'email',
        'is_delete',
    )
    search_fields = ('name', 'email',)
admin.site.register(Contacts, ContactsAdmin)


class CouponDetailsAdmin(admin.ModelAdmin):
    list_display = (
        'coupon',
        'food',
        'quantity',
        'is_delete',
    )
    search_fields = ('quantity',)
admin.site.register(CouponDetails, CouponDetailsAdmin)


class CustomerCouponsAdmin(admin.ModelAdmin):
    list_display = (
        'customer',
        'coupon',
        'register_date',
        'status',
        'is_delete',
    )
    search_fields = ('register_date', 'status',)
admin.site.register(CustomerCoupons, CustomerCouponsAdmin)


class CustomerFoodsAdmin(admin.ModelAdmin):
    list_display = (
        'food',
        'customer',
        'use_date',
        'quantity',
        'price',
        'is_delete',
    )
    search_fields = ('use_date', 'quantity', 'price',)
admin.site.register(CustomerFoods, CustomerFoodsAdmin)


class CustomerPointsAdmin(admin.ModelAdmin):
    list_display = (
        'customer',
        'res_point',
        'use_date',
        'is_delete',
    )
    search_fields = ('customer', 'res_point', 'use_date',)
admin.site.register(CustomerPoints, CustomerPointsAdmin)


class CustomerPrizesAdmin(admin.ModelAdmin):
    list_display = (
        'customer',
        'prize',
        'use_date',
        'is_delete',
    )
    search_fields = ('customer', 'prize', 'use_date',)
admin.site.register(CustomerPrizes, CustomerPrizesAdmin)


class CustomerVotesAdmin(admin.ModelAdmin):
    list_display = (
        'session',
        'vote_item',
        'rate',
        'is_delete',
    )
    search_fields = ('vote_item', 'rate',)
admin.site.register(CustomerVotes, CustomerVotesAdmin)


class DeviceInfoAdmin(admin.ModelAdmin):
    list_display = (
        'device_token',
        'type',
        'is_push_notify',
        'is_delete',
    )
    search_fields = ('device_token', 'type', 'is_push_notify',)
admin.site.register(DeviceInfo, DeviceInfoAdmin)


class FoodCategoriesAdmin(admin.ModelAdmin):
    list_display = (
        'name',
        'name_cn',
        'is_delete',
    )
    search_fields = ('name',)
admin.site.register(FoodCategories, FoodCategoriesAdmin)


class FoodItemsAdmin(admin.ModelAdmin):
    list_display = (
        'food',
        'item',
        'is_delete',
    )
    # search_fields = ('code', 'title', 'price',)
admin.site.register(FoodItems, FoodItemsAdmin)


class FoodPricesAdmin(admin.ModelAdmin):
    list_display = (
        'food',
        'qr_code',
        'price',
        'is_delete',
    )
    search_fields = ('price',)
admin.site.register(FoodPrices, FoodPricesAdmin)


class FoodTypesAdmin(admin.ModelAdmin):
    list_display = (
        'name',
        'name_cn',
        'is_delete',
    )
    search_fields = ('name',)
admin.site.register(FoodTypes, FoodTypesAdmin)


class RestaurantPointsAdmin(admin.ModelAdmin):
    list_display = (
        'restaurant',
        'code',
        'issue_date',
        'point',
        'is_delete',
    )
    search_fields = ('restaurant', 'code', 'issue_date', 'point',)
admin.site.register(RestaurantPoints, RestaurantPointsAdmin)


class StationsAdmin(admin.ModelAdmin):
    list_display = (
        'station_cd',
        'name',
        'name_cn',
        'line_cd',
        'post',
        'is_delete',
    )
    search_fields = ('name', 'line_cd', 'post',)
admin.site.register(Stations, StationsAdmin)


class SupplierItemsAdmin(admin.ModelAdmin):
    list_display = (
        'supplier',
        'item',
        'unit_price',
        'quantity_per_day',
        'is_delete',
    )
    search_fields = ('unit_price', 'quantity_per_day',)
admin.site.register(SupplierItems, SupplierItemsAdmin)


class SupplierOrdersAdmin(admin.ModelAdmin):
    list_display = (
        'supplier',
        'item',
        'order_date',
        'quantity',
        'status',
        'is_delete',
    )
    search_fields = ('order_date', 'quantity', 'status',)
admin.site.register(SupplierOrders, SupplierOrdersAdmin)


class TopicCategoriesAdmin(admin.ModelAdmin):
    list_display = (
        'name',
        'name_cn',
        'is_delete',
    )
    search_fields = ('code', 'name',)
admin.site.register(TopicCategories, TopicCategoriesAdmin)


class VoteItemsAdmin(admin.ModelAdmin):
    list_display = (
        'restaurant',
        'name',
        'name_cn',
        'is_delete',
    )
    search_fields = ('name',)
admin.site.register(VoteItems, VoteItemsAdmin)


class VoteSessionsAdmin(admin.ModelAdmin):
    list_display = (
        'customer',
        'vote_date',
        'is_delete',
    )
    search_fields = ('vote_date',)
admin.site.register(VoteSessions, VoteSessionsAdmin)


class OperationsRestaurantsAdmin(admin.ModelAdmin):
    list_display = (
        'operation',
        'restaurant',
        'description',
        'is_delete',
    )
    search_fields = ('operation', 'restaurant', 'description',)
admin.site.register(OperationsRestaurants, OperationsRestaurantsAdmin)


class PrizesAdmin(admin.ModelAdmin):
    list_display = (
        'request_point',
        'from_date',
        'to_date',
        'start_time',
        'end_time',
        'week_days',
        'is_delete',
    )
    search_fields = ('request_point', 'from_date', 'to_date', 'start_time', 'end_time', 'week_days',)
admin.site.register(Prizes, PrizesAdmin)


class OperationsAdmin(admin.ModelAdmin):
    list_display = (
        'type_id',
        'first_name',
        'last_name',
        'email',
        'phone',
        'is_delete',
    )
    search_fields = ('type_id', 'first_name', 'last_name', 'email', 'phone',)
admin.site.register(Operations, OperationsAdmin)


class ReservationsAdmin(admin.ModelAdmin):
    list_display = (
        'booking_id',
        'hash'
    )
    search_fields = ('hash', 'booking_id')
admin.site.register(Reservations, ReservationsAdmin)


class ItemsAdmin(admin.ModelAdmin):
    list_display = (
        'restaurant_id',
        'name',
        'name_cn',
        'is_delete',
    )
    search_fields = ('name',)
admin.site.register(Items, ItemsAdmin)


class RestaurantsAdmin(admin.ModelAdmin):
    list_display = (
        'code',
        'name',
        'email',
        'phone',
        'address',
        'is_delete',
    )
    search_fields = ('code', 'name', 'email', 'phone',)
admin.site.register(Restaurants, RestaurantsAdmin)


class CustomersAdmin(admin.ModelAdmin):
    list_display = (
        'name',
        'birthday',
        'sex',
        'email',
        'is_delete',
    )
    search_fields = ('name', 'birthday', 'sex', 'email',)
admin.site.register(Customers, CustomersAdmin)


class FoodsAdmin(admin.ModelAdmin):
    list_display = (
        'name',
        'current_order',
        'quantity_per_day',
        'week_days',
        'is_delete',
    )
    search_fields = ('name', 'current_order', 'quantity_per_day', 'week_days',)
admin.site.register(Foods, FoodsAdmin)


class TopicsAdmin(admin.ModelAdmin):
    list_display = (
        'subject',
        'open_date',
        'close_date',
        'is_delete',
    )
    search_fields = ('subject', 'open_date', 'close_date',)
admin.site.register(Topics, TopicsAdmin)


class SuppliersAdmin(admin.ModelAdmin):
    list_display = (
        'name',
        'address',
        'email',
        'phone',
        'is_delete',
    )
    search_fields = ('name', 'address', 'email', 'phone',)
admin.site.register(Suppliers, SuppliersAdmin)


class CouponsAdmin(admin.ModelAdmin):
    list_display = (
        'number',
        'title',
        'from_date',
        'to_date',
        'current_register',
        'quantity',
        'is_delete',
    )
    search_fields = ('number', 'from_date', 'to_date', 'quantity',)
admin.site.register(Coupons, CouponsAdmin)


class SettingsAdmin(admin.ModelAdmin):
    list_display = (
        'code',
        'value',
        'description',
        'is_delete',
    )
    search_fields = ('code', 'value', 'description',)
admin.site.register(Settings, SettingsAdmin)
