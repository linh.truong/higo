# This is an auto-generated Django model module.
# You'll have to do the following manually to clean this up:
#   * Rearrange models' order
#   * Make sure each model has one field with primary_key=True
#   * Make sure each ForeignKey has `on_delete` set to the desired behavior.
#   * Remove `managed = False` lines if you wish to allow Django to create, modify, and delete the table
# Feel free to rename the models, but don't rename db_table values or field names.
from __future__ import unicode_literals
from utils import constants as const
from django.db import models


# Define image store by  model
def coupons_file_name(instance, filename):
    return '/'.join(['coupons', filename])


def customers_file_name(instance, filename):
    return '/'.join(['customers', filename])


def food_cat_file_name(instance, filename):
    return '/'.join(['food_categories', filename])


def foods_file_name(instance, filename):
    return '/'.join(['foods', filename])


def items_file_name(instance, filename):
    return '/'.join(['items', filename])


def restaurants_file_name(instance, filename):
    return '/'.join(['restaurants', filename])


def operations_file_name(instance, filename):
    return '/'.join(['operations', filename])


def topics_file_name(instance, filename):
    return '/'.join(['topics', filename])


def prizes_file_name(instance, filename):
    return '/'.join(['prizes', filename])


def membership_file_name(instance, filename):
    return '/'.join(['memberships', filename])


class Areas(models.Model):
    name = models.CharField(max_length=50)
    name_cn = models.CharField(max_length=50)
    create_date = models.DateTimeField(auto_now_add=True)
    update_date = models.DateTimeField(auto_now=True)
    update_by = models.CharField(max_length=50, blank=True, null=True)
    is_delete = models.IntegerField(default=0, choices=const.CMS_RCD_STATUS)

    class Meta:
        managed = False
        db_table = 'areas'


class Contacts(models.Model):
    restaurant = models.ForeignKey('Restaurants', on_delete=models.CASCADE)
    name = models.CharField(max_length=100)
    email = models.CharField(max_length=50)
    phone = models.CharField(max_length=20)
    content = models.CharField(max_length=2000)
    status = models.IntegerField()
    create_date = models.DateTimeField(auto_now_add=True)
    update_date = models.DateTimeField(auto_now=True)
    update_by = models.CharField(max_length=50, blank=True, null=True)
    is_delete = models.IntegerField(default=0, choices=const.CMS_RCD_STATUS)

    class Meta:
        managed = False
        db_table = 'contacts'


class CouponDetails(models.Model):
    coupon = models.ForeignKey('Coupons', on_delete=models.CASCADE)
    food = models.ForeignKey('Foods', on_delete=models.CASCADE)
    quantity = models.IntegerField()
    create_date = models.DateTimeField(auto_now_add=True)
    update_date = models.DateTimeField(auto_now=True)
    update_by = models.CharField(max_length=50, blank=True, null=True)
    is_delete = models.IntegerField(default=0, choices=const.CMS_RCD_STATUS)

    class Meta:
        managed = False
        db_table = 'coupon_details'


class Coupons(models.Model):
    restaurant = models.ForeignKey('Restaurants', on_delete=models.CASCADE)
    number = models.CharField(max_length=20)
    title = models.CharField(max_length=200)
    title_cn = models.CharField(max_length=200)
    name = models.CharField(max_length=200)
    name_cn = models.CharField(max_length=200)
    price_off = models.FloatField(blank=True, null=True)
    price_minus = models.FloatField(blank=True, null=True)
    from_date = models.DateTimeField()
    to_date = models.DateTimeField()
    start_time = models.TimeField(blank=True, null=True)
    end_time = models.TimeField(blank=True, null=True)
    week_days = models.CharField(max_length=50, blank=True, null=True)
    current_register = models.IntegerField(default=0)
    quantity = models.IntegerField(default=1000000)
    image_path = models.ImageField(upload_to=coupons_file_name, null=True, height_field='image_height',
                                   width_field='image_width')
    image_width = models.IntegerField(default=200)
    image_height = models.IntegerField(default=200)
    coupon_type = models.IntegerField()
    comment = models.CharField(max_length=500, blank=True, null=True)
    comment_cn = models.CharField(max_length=500, blank=True, null=True)
    condition = models.CharField(max_length=2000, blank=True, null=True)
    condition_cn = models.CharField(max_length=2000, blank=True, null=True)
    is_mail_notify = models.IntegerField(default=1)
    is_sms_notify = models.IntegerField(default=1)
    is_app_notify = models.IntegerField(default=1)
    is_already_send = models.IntegerField(default=0)
    hash_tags = models.CharField(max_length=200, blank=True, null=True)
    create_date = models.DateTimeField(auto_now_add=True)
    update_date = models.DateTimeField(auto_now=True)
    update_by = models.CharField(max_length=50, blank=True, null=True)
    is_delete = models.IntegerField(default=0, choices=const.CMS_RCD_STATUS)

    class Meta:
        managed = False
        db_table = 'coupons'
        verbose_name_plural = '6 - Coupons'


class CustomerCoupons(models.Model):
    customer = models.ForeignKey('Customers', on_delete=models.CASCADE)
    coupon = models.ForeignKey(Coupons, on_delete=models.CASCADE)
    register_date = models.DateTimeField()
    use_date = models.DateTimeField(blank=True, null=True)
    status = models.IntegerField()
    description = models.CharField(max_length=500, blank=True, null=True)
    create_date = models.DateTimeField(auto_now_add=True)
    update_date = models.DateTimeField(auto_now=True)
    update_by = models.CharField(max_length=50, blank=True, null=True)
    is_delete = models.IntegerField(default=0, choices=const.CMS_RCD_STATUS)

    class Meta:
        managed = False
        db_table = 'customer_coupons'


class CustomerFoods(models.Model):
    food = models.ForeignKey('Foods', on_delete=models.CASCADE)
    customer = models.ForeignKey('Customers', on_delete=models.CASCADE)
    use_date = models.DateTimeField()
    quantity = models.IntegerField()
    price = models.DecimalField(max_digits=19, decimal_places=2)
    create_date = models.DateTimeField(auto_now_add=True)
    update_date = models.DateTimeField(auto_now=True)
    update_by = models.CharField(max_length=50, blank=True, null=True)
    is_delete = models.IntegerField(default=0, choices=const.CMS_RCD_STATUS)

    class Meta:
        managed = False
        db_table = 'customer_foods'


class CustomerPoints(models.Model):
    customer = models.ForeignKey('Customers', on_delete=models.CASCADE)
    res_point = models.ForeignKey('RestaurantPoints', on_delete=models.CASCADE)
    use_date = models.DateTimeField()
    create_date = models.DateTimeField(auto_now_add=True)
    update_date = models.DateTimeField(auto_now=True)
    update_by = models.CharField(max_length=50, blank=True, null=True)
    is_delete = models.IntegerField(default=0, choices=const.CMS_RCD_STATUS)

    class Meta:
        managed = False
        db_table = 'customer_points'


class CustomerPrizes(models.Model):
    customer = models.ForeignKey('Customers', on_delete=models.CASCADE)
    prize = models.ForeignKey('Prizes', on_delete=models.CASCADE)
    tracking_no = models.IntegerField(blank=True, null=True)
    delivery_type = models.IntegerField(blank=True, null=True)
    name = models.CharField(max_length=50, blank=True, null=True)
    phone = models.CharField(max_length=20, blank=True, null=True)
    email = models.CharField(max_length=50, blank=True, null=True)
    address = models.CharField(max_length=200, blank=True, null=True)
    use_date = models.DateTimeField()
    delivery_date = models.DateTimeField(blank=True, null=True)
    receive_date = models.DateTimeField(blank=True, null=True)
    status = models.IntegerField(blank=True, null=True)
    language = models.CharField(max_length=20, blank=True, null=True)
    create_date = models.DateTimeField(auto_now_add=True)
    update_date = models.DateTimeField(auto_now=True)
    update_by = models.CharField(max_length=50, blank=True, null=True)
    is_delete = models.IntegerField(default=0, choices=const.CMS_RCD_STATUS)

    class Meta:
        managed = False
        db_table = 'customer_prizes'


class CustomerVotes(models.Model):
    session = models.ForeignKey('VoteSessions', on_delete=models.CASCADE)
    vote_item = models.ForeignKey('VoteItems', on_delete=models.CASCADE)
    rate = models.IntegerField()
    create_date = models.DateTimeField(auto_now_add=True)
    update_date = models.DateTimeField(auto_now=True)
    update_by = models.CharField(max_length=50, blank=True, null=True)
    is_delete = models.IntegerField(default=0, choices=const.CMS_RCD_STATUS)

    class Meta:
        managed = False
        db_table = 'customer_votes'


class Customers(models.Model):
    user_id = models.IntegerField()
    area = models.ForeignKey(Areas, on_delete=models.CASCADE)
    restaurant = models.ForeignKey('Restaurants', on_delete=models.CASCADE)
    number = models.IntegerField(blank=True, null=True)
    name = models.CharField(max_length=50)
    image_path = models.ImageField(upload_to=customers_file_name, null=True)
    age = models.IntegerField()
    birthday = models.DateTimeField(blank=True, null=True)
    zip_code = models.CharField(max_length=10, blank=True, null=True)
    sex = models.IntegerField()
    address = models.CharField(max_length=200, blank=True, null=True)
    gps_data = models.CharField(max_length=200, blank=True, null=True)
    email = models.CharField(max_length=50)
    login_password = models.CharField(max_length=50)
    phone = models.CharField(max_length=20)
    last_transaction_date = models.DateTimeField()
    current_point = models.IntegerField(default=0)
    total_point = models.IntegerField(default=0)
    is_mail_notify = models.IntegerField(default=1)
    is_sms_notify = models.IntegerField(default=1)
    access_token = models.CharField(max_length=60, blank=True, null=True)
    recover_password = models.CharField(max_length=20, blank=True, null=True)
    level_up_date = models.DateTimeField(blank=True, null=True)
    num_visit = models.IntegerField(default=0)
    create_date = models.DateTimeField(auto_now_add=True)
    update_date = models.DateTimeField(auto_now=True)
    update_by = models.CharField(max_length=50, blank=True, null=True)
    is_delete = models.IntegerField(default=0, choices=const.CMS_RCD_STATUS)

    class Meta:
        managed = False
        db_table = 'customers'
        verbose_name_plural = '1 - Customers'


class DeviceInfo(models.Model):
    customer_id = models.IntegerField(blank=True, null=True)
    device_token = models.CharField(max_length=500)
    type = models.IntegerField()
    is_push_notify = models.IntegerField(default=1)
    create_date = models.DateTimeField(auto_now_add=True)
    update_date = models.DateTimeField(auto_now=True)
    update_by = models.CharField(max_length=50, blank=True, null=True)
    is_delete = models.IntegerField(default=0, choices=const.CMS_RCD_STATUS)

    class Meta:
        managed = False
        db_table = 'device_info'


class FoodCategories(models.Model):
    name = models.CharField(max_length=50)
    name_cn = models.CharField(max_length=50, blank=True, null=True)
    image_path = models.ImageField(upload_to=food_cat_file_name, null=True)
    create_date = models.DateTimeField(auto_now_add=True)
    update_date = models.DateTimeField(auto_now=True)
    update_by = models.CharField(max_length=50, blank=True, null=True)
    is_delete = models.IntegerField(default=0, choices=const.CMS_RCD_STATUS)

    class Meta:
        managed = False
        db_table = 'food_categories'


class FoodItems(models.Model):
    food = models.ForeignKey('Foods', on_delete=models.CASCADE)
    item = models.ForeignKey('Items', on_delete=models.CASCADE)
    comment = models.CharField(max_length=200, blank=True, null=True)
    comment_cn = models.CharField(max_length=200, blank=True, null=True)
    create_date = models.DateTimeField(auto_now_add=True)
    update_date = models.DateTimeField(auto_now=True)
    update_by = models.CharField(max_length=50, blank=True, null=True)
    is_delete = models.IntegerField(default=0, choices=const.CMS_RCD_STATUS)

    class Meta:
        managed = False
        db_table = 'food_items'


class FoodPrices(models.Model):
    food = models.ForeignKey('Foods', on_delete=models.CASCADE)
    qr_code = models.CharField(max_length=20, blank=True, null=True)
    price_date = models.DateTimeField()
    price = models.DecimalField(max_digits=19, decimal_places=2)
    comment = models.CharField(max_length=200, blank=True, null=True)
    comment_cn = models.CharField(max_length=200, blank=True, null=True)
    create_date = models.DateTimeField(auto_now_add=True)
    update_date = models.DateTimeField(auto_now=True)
    update_by = models.CharField(max_length=50, blank=True, null=True)
    is_delete = models.IntegerField(default=0, choices=const.CMS_RCD_STATUS)

    class Meta:
        managed = False
        db_table = 'food_prices'


class FoodTypes(models.Model):
    name = models.CharField(max_length=50)
    name_cn = models.CharField(max_length=50, blank=True, null=True)
    create_date = models.DateTimeField(auto_now_add=True)
    update_date = models.DateTimeField(auto_now=True)
    update_by = models.CharField(max_length=50, blank=True, null=True)
    is_delete = models.IntegerField(default=0, choices=const.CMS_RCD_STATUS)

    class Meta:
        managed = False
        db_table = 'food_types'


class Foods(models.Model):
    restaurant = models.ForeignKey('Restaurants', on_delete=models.CASCADE)
    category = models.ForeignKey('FoodCategories', on_delete=models.CASCADE)
    type = models.ForeignKey(FoodTypes, on_delete=models.CASCADE)
    name = models.CharField(max_length=50)
    name_cn = models.CharField(max_length=50)
    price = models.DecimalField(max_digits=19, decimal_places=2)
    description = models.CharField(max_length=2000, blank=True, null=True)
    description_cn = models.CharField(max_length=2000, blank=True, null=True)
    start_time = models.TimeField(blank=True, null=True)
    end_time = models.TimeField(blank=True, null=True)
    current_order = models.IntegerField(blank=True, null=True, default=0)
    quantity_per_day = models.IntegerField(blank=True, null=True)
    week_days = models.CharField(max_length=50, blank=True, null=True)
    image_path = models.ImageField(upload_to=foods_file_name, null=True, height_field='image_height',
                                   width_field='image_width')
    image_width = models.IntegerField(default=200)
    image_height = models.IntegerField(default=200)
    hash_tags = models.CharField(max_length=200, blank=True, null=True)
    create_date = models.DateTimeField(auto_now_add=True)
    update_date = models.DateTimeField(auto_now=True)
    update_by = models.CharField(max_length=50, blank=True, null=True)
    is_delete = models.IntegerField(default=0, choices=const.CMS_RCD_STATUS)

    class Meta:
        managed = False
        db_table = 'foods'
        verbose_name_plural = '3 - Foods'


class Items(models.Model):
    category = models.ForeignKey(FoodCategories, on_delete=models.CASCADE)
    restaurant_id = models.IntegerField()
    name = models.CharField(max_length=50)
    name_cn = models.CharField(max_length=50, blank=True, null=True)
    image_path = models.ImageField(upload_to=items_file_name, null=True)
    create_date = models.DateTimeField(auto_now_add=True)
    update_date = models.DateTimeField(auto_now=True)
    update_by = models.CharField(max_length=50, blank=True, null=True)
    is_delete = models.IntegerField(default=0, choices=const.CMS_RCD_STATUS)

    class Meta:
        managed = False
        db_table = 'items'
        verbose_name_plural = '4 - Items'


class Memberships(models.Model):
    device_id = models.CharField(max_length=500, blank=True, null=True)
    number = models.IntegerField(blank=True, null=True)
    barcode = models.CharField(max_length=50, blank=True, null=True)
    qr_code = models.CharField(max_length=50, blank=True, null=True)
    type = models.IntegerField()
    create_date = models.DateTimeField(auto_now_add=True)
    update_date = models.DateTimeField(auto_now=True)
    update_by = models.CharField(max_length=50, blank=True, null=True)
    is_delete = models.IntegerField(default=0, choices=const.CMS_RCD_STATUS)

    class Meta:
        managed = False
        db_table = 'memberships'


class MembershipStatus(models.Model):
    name = models.CharField(max_length=50)
    name_cn = models.CharField(max_length=50)
    content = models.CharField(max_length=2000, blank=True, null=True)
    content_cn = models.CharField(max_length=2000, blank=True, null=True)
    short_content = models.CharField(max_length=1000, blank=True, null=True)
    short_content_cn = models.CharField(max_length=1000, blank=True, null=True)
    short_content_2 = models.CharField(max_length=1000, blank=True, null=True)
    short_content_2_cn = models.CharField(max_length=1000, blank=True, null=True)
    content_special = models.CharField(max_length=2000, blank=True, null=True)
    content_special_cn = models.CharField(max_length=2000, blank=True, null=True)
    goal_point = models.IntegerField()
    image_path = models.ImageField(upload_to=membership_file_name, null=True)
    hex_color = models.CharField(max_length=50, blank=True, null=True)
    hex_color_benefit_1 = models.CharField(max_length=50, blank=True, null=True)
    hex_color_benefit_2 = models.CharField(max_length=50, blank=True, null=True)
    create_date = models.DateTimeField(auto_now_add=True)
    update_date = models.DateTimeField(auto_now=True)
    update_by = models.CharField(max_length=50, blank=True, null=True)
    is_delete = models.IntegerField(default=0, choices=const.CMS_RCD_STATUS)

    class Meta:
        managed = False
        db_table = 'membership_statuses'


class Operations(models.Model):
    user_id = models.IntegerField()
    area = models.ForeignKey(Areas, on_delete=models.CASCADE)
    type_id = models.IntegerField(choices=const.CMS_ROLE_LIST)
    last_name = models.CharField(max_length=30)
    first_name = models.CharField(max_length=30)
    image_path = models.ImageField(upload_to=operations_file_name, null=True)
    birthday = models.DateTimeField(blank=True, null=True)
    sex = models.IntegerField()
    zip_code = models.CharField(max_length=10, blank=True, null=True)
    address = models.CharField(max_length=500, blank=True, null=True)
    email = models.CharField(max_length=50)
    login_password = models.CharField(max_length=50)
    phone = models.CharField(max_length=20)
    last_res_id = models.IntegerField(blank=True, null=True)
    create_date = models.DateTimeField(auto_now_add=True)
    update_date = models.DateTimeField(auto_now=True)
    update_by = models.CharField(max_length=50, blank=True, null=True)
    is_delete = models.IntegerField(default=0, choices=const.CMS_RCD_STATUS)

    class Meta:
        managed = False
        db_table = 'operations'
        verbose_name_plural = '8 - Operations'


class OperationsRestaurants(models.Model):
    operation = models.ForeignKey(Operations, on_delete=models.CASCADE)
    restaurant = models.ForeignKey('Restaurants', on_delete=models.CASCADE)
    description = models.CharField(max_length=500, blank=True, null=True)
    description_cn = models.CharField(max_length=500, blank=True, null=True)
    create_date = models.DateTimeField(auto_now_add=True)
    update_date = models.DateTimeField(auto_now=True)
    update_by = models.CharField(max_length=50, blank=True, null=True)
    is_delete = models.IntegerField(default=0, choices=const.CMS_RCD_STATUS)

    class Meta:
        managed = False
        db_table = 'operations_restaurants'


class Prizes(models.Model):
    restaurant = models.ForeignKey('Restaurants', on_delete=models.CASCADE)
    request_point = models.IntegerField()
    title = models.CharField(max_length=200)
    title_cn = models.CharField(max_length=200)
    content = models.CharField(max_length=500)
    content_cn = models.CharField(max_length=500)
    condition = models.CharField(max_length=2000, blank=True, null=True)
    condition_cn = models.CharField(max_length=2000, blank=True, null=True)
    from_date = models.DateTimeField()
    to_date = models.DateTimeField()
    start_time = models.TimeField(blank=True, null=True)
    end_time = models.TimeField(blank=True, null=True)
    week_days = models.CharField(max_length=50, blank=True, null=True)
    current_register = models.IntegerField(default=0)
    quantity = models.IntegerField()
    image_path = models.ImageField(upload_to=prizes_file_name, null=True, height_field='image_height',
                                   width_field='image_width')
    image_width = models.IntegerField(default=200)
    image_height = models.IntegerField(default=200)
    link_address = models.CharField(max_length=500, blank=True, null=True)
    is_mail_notify = models.IntegerField(default=1)
    is_sms_notify = models.IntegerField(default=1)
    is_app_notify = models.IntegerField(default=1)
    is_already_send = models.IntegerField(default=0)
    delivery_type = models.IntegerField(default=1, blank=True, null=True)
    delivery_duration_days = models.IntegerField(default=3, blank=True, null=True)
    hash_tags = models.CharField(max_length=200, blank=True, null=True)
    create_date = models.DateTimeField(auto_now_add=True)
    update_date = models.DateTimeField(auto_now=True)
    update_by = models.CharField(max_length=50, blank=True, null=True)
    is_delete = models.IntegerField(default=0, choices=const.CMS_RCD_STATUS)

    class Meta:
        managed = False
        db_table = 'prizes'

    def __str__(self):
        return self.title


class Reservations(models.Model):
    booking_id = models.IntegerField()
    customer_id = models.IntegerField(blank=True, null=True)
    client_id = models.IntegerField()
    event_id = models.IntegerField()
    unit_id = models.IntegerField()
    status_id = models.IntegerField()
    start_date_time = models.DateTimeField()
    end_date_time = models.DateTimeField()
    number_of_seat = models.IntegerField()
    hash = models.CharField(max_length=200)
    code = models.CharField(max_length=50)
    is_confirmed = models.IntegerField()
    create_date = models.DateTimeField(auto_now_add=True)
    update_date = models.DateTimeField(auto_now=True)
    update_by = models.CharField(max_length=50, blank=True, null=True)
    is_delete = models.IntegerField(default=0, choices=const.CMS_RCD_STATUS)

    class Meta:
        managed = False
        db_table = 'reservations'


class RestaurantPoints(models.Model):
    restaurant = models.ForeignKey('Restaurants', on_delete=models.CASCADE)
    code = models.CharField(unique=True, max_length=30)
    issue_date = models.DateTimeField()
    point = models.IntegerField()
    qr_code = models.CharField(max_length=50)
    comment = models.CharField(max_length=500, blank=True, null=True)
    comment_cn = models.CharField(max_length=500, blank=True, null=True)
    create_date = models.DateTimeField(auto_now_add=True)
    update_date = models.DateTimeField(auto_now=True)
    update_by = models.CharField(max_length=50, blank=True, null=True)
    is_delete = models.IntegerField(default=0, choices=const.CMS_RCD_STATUS)

    class Meta:
        managed = False
        db_table = 'restaurant_points'


class RestaurantSchedules(models.Model):
    restaurant = models.ForeignKey('Restaurants', on_delete=models.CASCADE)
    start_week_day = models.IntegerField()
    end_week_day = models.IntegerField()
    start_time = models.TimeField()
    end_time = models.TimeField()
    latest_order_time = models.TimeField()
    create_date = models.DateTimeField(auto_now_add=True)
    update_date = models.DateTimeField(auto_now=True)
    update_by = models.CharField(max_length=50, blank=True, null=True)
    is_delete = models.IntegerField(default=0, choices=const.CMS_RCD_STATUS)

    class Meta:
        managed = False
        db_table = 'restaurant_schedules'


class Restaurants(models.Model):
    area = models.ForeignKey('Areas', on_delete=models.CASCADE)
    station = models.ForeignKey('Stations', on_delete=models.CASCADE)
    sb_company_login = models.CharField(max_length=100)
    sb_username = models.CharField(max_length=50)
    sb_user_password = models.CharField(max_length=50)
    sb_api_key = models.CharField(max_length=500)
    code = models.CharField(max_length=10)
    name = models.CharField(max_length=100)
    name_cn = models.CharField(max_length=100, blank=True, null=True)
    image_path = models.ImageField(upload_to=restaurants_file_name, null=True)
    contact_person = models.CharField(max_length=100, blank=True, null=True)
    email = models.CharField(max_length=50)
    phone = models.CharField(max_length=20)
    address = models.CharField(max_length=500)
    address_cn = models.CharField(max_length=500, blank=True, null=True)
    description = models.CharField(max_length=2000, blank=True, null=True)
    description_cn = models.CharField(max_length=2000, blank=True, null=True)
    google_map_url = models.CharField(max_length=500, blank=True, null=True)
    longitude = models.DecimalField(max_digits=11, decimal_places=8, blank=True, null=True)
    latitude = models.DecimalField(max_digits=11, decimal_places=8, blank=True, null=True)
    week_days = models.CharField(max_length=50, blank=True, null=True)
    start_time = models.TimeField(blank=True, null=True)
    end_time = models.TimeField(blank=True, null=True)
    limit_days_book = models.IntegerField(blank=True, null=True)
    confirm_reserve = models.CharField(max_length=500, blank=True, null=True)
    confirm_reserve_cn = models.CharField(max_length=500, blank=True, null=True)
    create_date = models.DateTimeField(auto_now_add=True)
    update_date = models.DateTimeField(auto_now=True)
    update_by = models.CharField(max_length=50, blank=True, null=True)
    is_delete = models.IntegerField(default=0, choices=const.CMS_RCD_STATUS)

    class Meta:
        managed = False
        db_table = 'restaurants'
        verbose_name_plural = '2 - Restaurants'


class Settings(models.Model):
    code = models.CharField(max_length=20)
    value = models.CharField(max_length=2000)
    value_cn = models.CharField(max_length=2000, blank=True, null=True)
    description = models.CharField(max_length=2000, blank=True, null=True)
    description_cn = models.CharField(max_length=2000, blank=True, null=True)
    create_date = models.DateTimeField(auto_now_add=True)
    update_date = models.DateTimeField(auto_now=True)
    update_by = models.CharField(max_length=50, blank=True, null=True)
    is_delete = models.IntegerField(default=0, choices=const.CMS_RCD_STATUS)

    class Meta:
        managed = False
        db_table = 'settings'


class Stations(models.Model):
    station_cd = models.IntegerField()
    name = models.CharField(max_length=200)
    name_cn = models.CharField(max_length=200, blank=True, null=True)
    line_cd = models.IntegerField(blank=True, null=True)
    post = models.CharField(max_length=20, blank=True, null=True)
    address = models.CharField(max_length=500, blank=True, null=True)
    address_cn = models.CharField(max_length=500, blank=True, null=True)
    lon = models.FloatField(blank=True, null=True)
    lat = models.FloatField(blank=True, null=True)
    create_date = models.DateTimeField(auto_now_add=True)
    update_date = models.DateTimeField(auto_now=True)
    update_by = models.CharField(max_length=50, blank=True, null=True)
    is_delete = models.IntegerField(default=0, choices=const.CMS_RCD_STATUS)

    class Meta:
        managed = False
        db_table = 'stations'


class SupplierItems(models.Model):
    supplier = models.ForeignKey('Suppliers', on_delete=models.CASCADE)
    item = models.ForeignKey(Items, on_delete=models.CASCADE)
    from_date = models.DateTimeField(blank=True, null=True)
    to_date = models.DateTimeField(blank=True, null=True)
    unit_price = models.DecimalField(max_digits=19, decimal_places=2)
    quantity_per_day = models.IntegerField(blank=True, null=True)
    create_date = models.DateTimeField(auto_now_add=True)
    update_date = models.DateTimeField(auto_now=True)
    update_by = models.CharField(max_length=50, blank=True, null=True)
    is_delete = models.IntegerField(default=0, choices=const.CMS_RCD_STATUS)

    class Meta:
        managed = False
        db_table = 'supplier_items'


class SupplierOrders(models.Model):
    supplier = models.ForeignKey('Suppliers', on_delete=models.CASCADE)
    item = models.ForeignKey(Items, on_delete=models.CASCADE)
    order_date = models.DateTimeField()
    receive_date = models.DateTimeField(blank=True, null=True)
    quantity = models.IntegerField()
    status = models.IntegerField()
    comment = models.CharField(max_length=500, blank=True, null=True)
    comment_cn = models.CharField(max_length=500, blank=True, null=True)
    create_date = models.DateTimeField(auto_now_add=True)
    update_date = models.DateTimeField(auto_now=True)
    update_by = models.CharField(max_length=50, blank=True, null=True)
    is_delete = models.IntegerField(default=0, choices=const.CMS_RCD_STATUS)

    class Meta:
        managed = False
        db_table = 'supplier_orders'


class Suppliers(models.Model):
    restaurant = models.ForeignKey(Restaurants, on_delete=models.CASCADE)
    area = models.ForeignKey(Areas, on_delete=models.CASCADE)
    name = models.CharField(max_length=100)
    name_cn = models.CharField(max_length=100, blank=True, null=True)
    address = models.CharField(max_length=500)
    address_cn = models.CharField(max_length=500, blank=True, null=True)
    postal_code = models.CharField(max_length=20, blank=True, null=True)
    email = models.CharField(max_length=50)
    phone = models.CharField(max_length=20)
    fax = models.CharField(max_length=20, blank=True, null=True)
    create_date = models.DateTimeField(auto_now_add=True)
    update_date = models.DateTimeField(auto_now=True)
    update_by = models.CharField(max_length=50, blank=True, null=True)
    is_delete = models.IntegerField(default=0, choices=const.CMS_RCD_STATUS)

    class Meta:
        managed = False
        db_table = 'suppliers'
        verbose_name_plural = '5 - Suppliers'


class TopicCategories(models.Model):
    name = models.CharField(max_length=50)
    name_cn = models.CharField(max_length=50, blank=True, null=True)
    create_date = models.DateTimeField(auto_now_add=True)
    update_date = models.DateTimeField(auto_now=True)
    update_by = models.CharField(max_length=50, blank=True, null=True)
    is_delete = models.IntegerField(default=0, choices=const.CMS_RCD_STATUS)

    class Meta:
        managed = False
        db_table = 'topic_categories'


class Topics(models.Model):
    restaurant = models.ForeignKey(Restaurants, on_delete=models.CASCADE)
    category = models.ForeignKey(TopicCategories, related_name='categories', on_delete=models.CASCADE)
    subject = models.CharField(max_length=500)
    subject_cn = models.CharField(max_length=500)
    body = models.CharField(max_length=4000)
    body_cn = models.CharField(max_length=4000)
    image_path = models.ImageField(upload_to=topics_file_name, null=True, height_field='image_height',
                                   width_field='image_width')
    image_width = models.IntegerField(default=200)
    image_height = models.IntegerField(default=200)
    link_address = models.CharField(max_length=500, blank=True, null=True)
    open_date = models.DateTimeField()
    close_date = models.DateTimeField()
    num_like = models.IntegerField(default=0)
    num_share = models.IntegerField(default=0)
    is_mail_notify = models.IntegerField(default=1)
    is_sms_notify = models.IntegerField(default=1)
    is_app_notify = models.IntegerField(default=1)
    is_already_send = models.IntegerField(default=0)
    hash_tags = models.CharField(max_length=200, blank=True, null=True)
    create_date = models.DateTimeField(auto_now_add=True)
    update_date = models.DateTimeField(auto_now=True)
    update_by = models.CharField(max_length=50, blank=True, null=True)
    is_delete = models.IntegerField(default=0, choices=const.CMS_RCD_STATUS)

    class Meta:
        managed = False
        db_table = 'topics'
        verbose_name_plural = '7 - Topics'


class VoteItems(models.Model):
    restaurant = models.ForeignKey(Restaurants, on_delete=models.CASCADE)
    name = models.CharField(max_length=200)
    name_cn = models.CharField(max_length=200, blank=True, null=True)
    comment = models.CharField(max_length=2000, blank=True, null=True)
    comment_cn = models.CharField(max_length=2000, blank=True, null=True)
    create_date = models.DateTimeField(auto_now_add=True)
    update_date = models.DateTimeField(auto_now=True)
    update_by = models.CharField(max_length=50, blank=True, null=True)
    is_delete = models.IntegerField(default=0, choices=const.CMS_RCD_STATUS)

    class Meta:
        managed = False
        db_table = 'vote_items'


class VoteSessions(models.Model):
    customer = models.ForeignKey(Customers, on_delete=models.CASCADE)
    vote_date = models.DateTimeField()
    comment = models.CharField(max_length=2000, blank=True, null=True)
    create_date = models.DateTimeField(auto_now_add=True)
    update_date = models.DateTimeField(auto_now=True)
    update_by = models.CharField(max_length=50, blank=True, null=True)
    is_delete = models.IntegerField(default=0, choices=const.CMS_RCD_STATUS)

    class Meta:
        managed = False
        db_table = 'vote_sessions'


class Seasons(models.Model):
    name = models.CharField(max_length=100, blank=True, null=True)
    name_cn = models.CharField(max_length=100, blank=True, null=True)
    from_date = models.DateTimeField()
    to_date = models.DateTimeField()
    check_date = models.DateTimeField(blank=True, null=True)
    create_date = models.DateTimeField(auto_now_add=True)
    update_date = models.DateTimeField(auto_now=True)
    update_by = models.CharField(max_length=50, blank=True, null=True)
    is_delete = models.IntegerField(default=0, choices=const.CMS_RCD_STATUS)

    class Meta:
        managed = False
        db_table = 'seasons'

    def __str__(self):
        return self.name


# -------------------------------------------------------------
# ======================= UPLOAD FOLDER =======================
# -------------------------------------------------------------
class Document(models.Model):
    file = models.FileField(upload_to='documents/%Y/%m/%d')


class UplCoupon(models.Model):
    file = models.FileField(upload_to='coupons')


class UplCustomer(models.Model):
    file = models.FileField(upload_to='customers')


class UplFood(models.Model):
    file = models.FileField(upload_to='foods')


class UplItem(models.Model):
    file = models.FileField(upload_to='items')


class UplRestaurant(models.Model):
    file = models.FileField(upload_to='restaurants')


class UplTopic(models.Model):
    file = models.FileField(upload_to='topics')


class UplMembership(models.Model):
    file = models.FileField(upload_to='memberships')


