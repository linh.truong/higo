"""toyosu URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.10/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url
import commonmodels.views as service

urlpatterns = [
    url(r'^api/login/sign_in$', service.sign_in),
    url(r'^api/login/sign_out$', service.sign_out),
    # url(r'^api/login/reset_password$', service.req_reset_password),
    # url(r'^api/login/change_password', service.req_change_password),

    url(r'^api/login/forgot_password$', service.req_forgot_password),

    url(r'^api/login/change_language$', service.change_language),

    url(r'^srv_upload/$', service.srv_upload, name='srv_upload'),
    url(r'^api/related/getlist$', service.get_related_items),
    url(r'^api/related/getlist_v2$', service.get_related_items_v2),
    url(r'^api/import/insert_pos_data', service.insert_pos_data),
]
