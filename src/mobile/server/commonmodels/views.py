#!/usr/bin/env python
# -*- coding: utf-8 -*-
import re

from dateutil.tz import tzlocal, tzutc
from django.contrib.auth import authenticate, login, logout
from django.contrib.sessions.models import Session
from django.contrib.auth.models import User
from django.apps import apps
from django.core.mail import EmailMultiAlternatives
from django.db import connection
from django.db.models import ForeignKey, Q
from django.forms import model_to_dict
from django.http import JsonResponse, HttpResponseRedirect
from django.shortcuts import render
from django.template import Context
from django.template.loader import get_template
from django.urls import reverse
from django.utils import translation
from django.views.decorators.csrf import csrf_exempt
import json
import dateutil.parser
from datetime import datetime
from commonmodels.forms import UploadFileForm
from commonmodels.models import Customers, Restaurants, Document, Topics
from utils import constants as const
from utils.constants import FILTER_LIST, MEDIA_URL
from higo.settings_dev import DEFAULT_FROM_EMAIL
from utils.message import get_msg_content
from utils.errcode import ErrCode as eCODE
from utils.utility import dynamic_query, dynamic_fk_query, get_curr_timezone, dictfetchall, make_instance, QueryParm, \
    dynamic_ctm_query


# ---------------------------------------------------------------
# ======================= COMMON FUNCTION =======================
# ---------------------------------------------------------------
# [Common function] Get POST value by param name
def get_req_param(request, name):
    # get by POST method (web)
    result = request.POST.get(name, '')
    if not result:
        # get by request.body (IOS)
        body_unicode = request.body.decode('utf-8')
        try:
            data = json.loads(body_unicode)
            result = data.get(name, '')
        except Exception as e:
            print(e)
            pass

    # parse JSON to dict
    try:
        if not isinstance(result, (dict, list)):
            result = json.loads(result)
    except (ValueError, TypeError):
        pass
    return result


# [Common function] Get current log-in user from SESSION
def get_auth_info(request):
    _auth_id = request.session.get(const.SESSION_U_ID, '')

    return _auth_id


# [Common function] Convert string from request to local time
def convert_to_local_time(string_time):
    timestamp_datetime = datetime.strptime(string_time, "%Y-%m-%d %H:%M:%S")
    timestamp_utc = timestamp_datetime.replace(tzinfo=tzutc())
    converted_timestamp = timestamp_utc.astimezone(tzlocal())
    return converted_timestamp


# -------------------------------------------------------------------
# ======================= COMMON BINDING DATA =======================
# -------------------------------------------------------------------
def json_to_model(model, data):
    for key in data.keys():
        if get_fk_model(model, key) is None:
            # map regular field
            model.__setattr__(key, data[key])
        else:
            # map relation object
            _id = data.get(key, 0)

            # find relation name
            _rel_obj = model._meta.get_field(key).related_model()
            _rel_name = _rel_obj._meta.object_name
            # find relation model
            _app_config = apps.get_app_config(model._meta.app_label)
            _model = _app_config.get_model(_rel_name)

            # get relation data
            _obj = _model.objects.get(pk=_id, is_delete=const.ACTIVE_FLAG)

            # binding
            model.__setattr__(key, _obj)

    return model


def form_to_model(model, data):
    for key in data.keys():
        if get_fk_model(model, key) is None:
            # map regular field
            model.__setattr__(key, data[key])
        else:
            # map relation object
            _id = data.get(key, 0)

            # find relation name
            _rel_obj = model._meta.get_field(key).related_model()
            _rel_name = _rel_obj._meta.object_name
            # find relation model
            _app_config = apps.get_app_config(model._meta.app_label)
            _model = _app_config.get_model(_rel_name)

            # get relation data
            _obj = _model.objects.get(pk=_id, is_delete=const.ACTIVE_FLAG)

            # binding
            model.__setattr__(key, _obj)

    return model


def get_fk_model(model, f_name):
    """
    returns None if not foreignkey, otherwise the relevant model

    @param model    Model       model object
    @param f_name   string      field name
    """
    try:
        field_object = model._meta.get_field(f_name)
        if isinstance(field_object, ForeignKey):
            return field_object.rel.to
    except Exception:
        pass

    return None


def model_to_dict_with_path(request, model_data, filter_list=None):
    _dict = model_to_dict(model_data)

    _chk_list = []
    _chk_path = [const.IMG_PATH, const.URL_PATH]  # append img path
    _chk_time = [const.START_TIME, const.END_TIME]  # format time display
    _chk_list.extend(_chk_path + _chk_time)

    _founds = [i for e in _chk_list for i in _dict if e in i]

    for _found in _founds:
        if _found in _chk_path:
            _build_path = _dict.get(_found)

            if _build_path is not None and not _build_path.__str__().startswith('http'):
                # _build_path = aqr_current_path(request) + MEDIA_URL \
                #               + model_data._meta.object_name.lower() + '/' + _build_path
                _build_path = aqr_current_path(request) + MEDIA_URL + _build_path.__str__()
                _dict.__setitem__(_found, _build_path)
        elif _found in _chk_time:
            _build_time = _dict.get(_found)
            _build_time = _build_time.strftime('%H:%M') if _build_time else '00:00'
            _dict.__setitem__(_found, _build_time)

    if filter_list:
        _founds = [i for e in filter_list for i in _dict if re.search(e, i)]
        for _found in _founds:
            _dict.pop(_found, None)

    return _dict


def model_to_dict_filter(request, model_data):
    return model_to_dict_with_path(request, model_data, FILTER_LIST)


# create authenticate token
def create_authen_token(request, user_id):
    request.session['user_id'] = user_id
    request.session.set_expiry(const.SESSION_TIME_OUT)
    request.session.save()
    token = request.session.session_key
    request.session['auth_token'] = token
    return token


# customize authentication Python
def is_authorize(request):
    # _timeout_valid = False
    session_data = {}
    res = {"error": 0, "message": "", "data": {}}
    if hasattr(request, 'session'):
        auth_token = get_req_param(request, 'auth_token')
        session_obj = Session.objects.filter(session_key=auth_token)

        if len(session_obj) > 0:
            # if datetime.now(timezone.utc) < session_obj[0].expire_date:
            #     _timeout_valid = True
            #     session_obj[0].expire_date = datetime.now(timezone.utc) + timedelta(hours=const.SESSION_TIME_OUT)
            #     session_obj[0].save()
            session_data = session_obj[0].get_decoded()
            request.session = session_data

    # if not _timeout_valid:
    #     res["error"] = eCODE.RES_TIMEOUT
    #     res['message'] = 'Session has expired please login again.'
    #     return JsonResponse(res)
    user_id = get_req_param(request, 'id') or None
    session_user_id = session_data['user_id'] if 'user_id' in session_data else None

    if user_id != session_user_id:
        res["error"] = eCODE.RES_NOT_FOUND
        res['message'] = 'An internal exception has occurred. Please try again or contact administrator.'
        return JsonResponse(res)
    customer = Customers.objects.filter(id=user_id, is_delete=const.ACTIVE_FLAG)

    if len(customer) == 0:
        res["error"] = eCODE.RES_FAIL
        res['message'] = 'You do not have enough access privileges for this operation.'
        return JsonResponse(res)

    return True


# -------------------------------------------------------------------
# ======================= COMMON SELECT QUERY =======================
# -------------------------------------------------------------------
def db_get_all(request, abs_model):
    res = {"error": 0, "message": "", "data": []}
    _limit = get_req_param(request, 'limit')
    _limit = 100 if _limit == '' else _limit  # if limit null get 100 record
    _offset = get_req_param(request, 'offset')
    _offset = 0 if _offset == '' else _offset

    full_list = abs_model.objects.filter(is_delete=const.ACTIVE_FLAG)
    _total = full_list.count()
    res_list = full_list.order_by('-' + 'create_date')[int(_offset):int(_limit + _offset)]

    res["error"] = eCODE.RES_SUCCESS
    res["total"] = _total
    for res_detail in res_list:
        res["data"].append(model_to_dict_filter(request, res_detail))

    return JsonResponse(res)


def db_get_all_with_sort_asc(request, abs_model, column):
    res = {"error": 0, "message": "", "data": []}
    _limit = get_req_param(request, 'limit')
    _limit = 100 if _limit == '' else _limit  # if limit null get 100 record
    _offset = get_req_param(request, 'offset')
    _offset = 0 if _offset == '' else _offset

    full_list = abs_model.objects.filter(is_delete=const.ACTIVE_FLAG)
    _total = full_list.count()
    res_list = full_list.order_by(column)[int(_offset):int(_limit + _offset)]

    res["error"] = eCODE.RES_SUCCESS
    res["total"] = _total
    for res_detail in res_list:
        res["data"].append(model_to_dict_filter(request, res_detail))

    return JsonResponse(res)


def db_get_all_with_sort_desc(request, abs_model, column):
    res = {"error": 0, "message": "", "data": []}
    _limit = get_req_param(request, 'limit')
    _limit = 100 if _limit == '' else _limit  # if limit null get 100 record
    _offset = get_req_param(request, 'offset')
    _offset = 0 if _offset == '' else _offset

    full_list = abs_model.objects.filter(is_delete=const.ACTIVE_FLAG)
    _total = full_list.count()
    res_list = full_list.order_by('-' + column)[int(_offset):int(_limit + _offset)]

    res["error"] = eCODE.RES_SUCCESS
    res["total"] = _total
    for res_detail in res_list:
        res["data"].append(model_to_dict_filter(request, res_detail))

    return JsonResponse(res)


# get customer list with res info
def db_get_ctm(request, abs_model):
    res = {"error": 0, "message": "", "data": []}
    _limit = get_req_param(request, 'limit')
    _limit = 100 if _limit == '' else _limit  # if limit null get 100 record
    _offset = get_req_param(request, 'offset')
    _offset = 0 if _offset == '' else _offset

    full_list = abs_model.objects.filter(is_delete=const.ACTIVE_FLAG)
    _total = full_list.count()
    res_list = full_list.order_by('-' + 'create_date')[int(_offset):int(_limit + _offset)]

    res["error"] = eCODE.RES_SUCCESS
    res["total"] = _total
    for res_detail in res_list:
        res_id = res_detail.restaurant_id
        # get res info
        res_info = Restaurants.objects.get(pk=res_id, is_delete=const.ACTIVE_FLAG)

        res_dict = model_to_dict_filter(request, res_detail)
        res_dict["res_name"] = res_info.name
        res_dict["res_name_cn"] = res_info.name_cn
        res_dict["res_avatar"] = aqr_current_path(request) + MEDIA_URL + "/" + str(res_info.image_path)
        res["data"].append(res_dict)

    return JsonResponse(res)


def db_get_by_id(request, abs_model):
    res = {"error": 0, "message": "", "data": {}}
    _id = get_req_param(request, 'id')
    _model_name = abs_model.__name__[:-1]
    if _id:
        try:
            res_data = abs_model.objects.get(pk=_id, is_delete=const.ACTIVE_FLAG)
            if res_data:
                res["error"] = eCODE.RES_SUCCESS
                res["data"] = model_to_dict_filter(request, res_data)
            else:
                res["error"] = eCODE.SCH_NOT_FOUND
                res['message'] = get_msg_content('data_not_found', _model_name, str(_id))
        except Exception as e:
            res["error"] = eCODE.RES_FAIL
            res["message"] = str(e)
    else:
        res["error"] = eCODE.REQ_ID_REQUIRED
        res["message"] = get_msg_content('action_id_required', _model_name)

    return JsonResponse(res)


def db_get_by_id_with_token(request, abs_model):
    res = {"error": 0, "message": "", "data": {}}
    auth_flg = is_authorize(request)
    if not auth_flg:
        return auth_flg
    _id = get_req_param(request, 'id')
    _model_name = abs_model.__name__[:-1]
    if _id:
        try:
            res_data = abs_model.objects.get(pk=_id, is_delete=const.ACTIVE_FLAG)
            if res_data:
                res["error"] = eCODE.RES_SUCCESS
                res["data"] = model_to_dict_filter(request, res_data)
            else:
                res["error"] = eCODE.SCH_NOT_FOUND
                res['message'] = get_msg_content('data_not_found', _model_name, str(_id))
        except Exception as e:
            res["error"] = eCODE.RES_FAIL
            res["message"] = str(e)
    else:
        res["error"] = eCODE.REQ_ID_REQUIRED
        res["message"] = get_msg_content('action_id_required', _model_name)

    return JsonResponse(res)


def db_get_by_id_ctm(request, _id, abs_model, filter_name):
    res = {"error": 0, "message": "", "data": {}}
    _model_name = abs_model.__name__[:-1]
    if _id:
        try:
            res_data = abs_model.objects.get(pk=_id, is_delete=const.ACTIVE_FLAG)
            if res_data:
                res["error"] = eCODE.RES_SUCCESS
                res["data"] = model_to_dict_filter(request, res_data)
            else:
                res["error"] = eCODE.SCH_NOT_FOUND
                res['message'] = get_msg_content('data_not_found', _model_name, str(_id))
        except Exception as e:
            res["error"] = eCODE.RES_FAIL
            res["message"] = str(e)
    else:
        res["error"] = eCODE.REQ_ID_REQUIRED
        res["message"] = get_msg_content('action_id_required', _model_name)

    return JsonResponse(res)


def db_get_by_cond(request, abs_model, conditions=None):
    res = {"error": 0, "message": "", "data": {}}
    try:
        if conditions is not None:
            _operator = conditions.pop('operator', 'and')
            _condition = conditions
        else:
            req_cond = json.loads(get_req_param(request, 'query'))
            _condition = req_cond['condition']
            _operator = req_cond.get('operator', 'and')

        if _condition:
            # append check ACTIVE_FLAG
            if _condition.keys().__contains__('is_delete') is False:
                _condition['is_delete'] = const.ACTIVE_FLAG

            # collect fields and values
            _fields = _condition.keys()
            _values = _condition.values()

            # search data
            res_list = dynamic_query(abs_model, _fields, _values, _operator)

            # binding response
            res["error"] = eCODE.RES_SUCCESS
            if len(res_list) > 0:
                res["data"] = model_to_dict_filter(request, res_list[0])
            else:
                res['message'] = get_msg_content('data_empty')
        else:
            res["error"] = eCODE.REQ_MISSING_RGS
            res["message"] = get_msg_content('action_missing_condition')
    except Exception as e:
        res["error"] = eCODE.RES_FAIL
        res["message"] = str(e)

    return JsonResponse(res)


def db_get_by_cond_with_token(request, abs_model, conditions=None):
    res = {"error": 0, "message": "", "data": {}}
    auth_flg = is_authorize(request)
    if not auth_flg:
        return auth_flg
    try:
        if conditions is not None:
            _operator = conditions.pop('operator', 'and')
            _condition = conditions
        else:
            req_cond = json.loads(get_req_param(request, 'query'))
            _condition = req_cond['condition']
            _operator = req_cond.get('operator', 'and')

        if _condition:
            # append check ACTIVE_FLAG
            if _condition.keys().__contains__('is_delete') is False:
                _condition['is_delete'] = const.ACTIVE_FLAG

            # collect fields and values
            _fields = _condition.keys()
            _values = _condition.values()

            # search data
            res_list = dynamic_query(abs_model, _fields, _values, _operator)

            # binding response
            res["error"] = eCODE.RES_SUCCESS
            if len(res_list) > 0:
                res["data"] = model_to_dict_filter(request, res_list[0])
            else:
                res['message'] = get_msg_content('data_empty')
        else:
            res["error"] = eCODE.REQ_MISSING_RGS
            res["message"] = get_msg_content('action_missing_condition')
    except Exception as e:
        res["error"] = eCODE.RES_FAIL
        res["message"] = str(e)

    return JsonResponse(res)


def db_get_by_pk(request, abs_model, foreign_info):
    res = {"error": 0, "message": "", "data": []}
    _limit = get_req_param(request, 'limit')
    _limit = 100 if _limit == '' else _limit  # if limit null get 100 record
    _offset = get_req_param(request, 'offset')
    _offset = 0 if _offset == '' else _offset

    try:
        for key, arr_val in foreign_info.items():
            _val_map = []

            # detect string or list
            if isinstance(arr_val, (str, int)):
                _val_map.append(arr_val)
            elif isinstance(arr_val, list):
                # _val_map.extend(arr_val)
                # disallow multi values
                res["error"] = eCODE.REQ_SINGLE_REQUIRED
                res["message"] = get_msg_content('request_single_required')
                return JsonResponse(res)

            if len(_val_map) <= 0:
                res["error"] = eCODE.REQ_ID_REQUIRED
                res["message"] = get_msg_content('action_id_required', key.title())
                return JsonResponse(res)
            else:
                for value in _val_map:
                    _fields = []
                    _types = []
                    _values = []

                    # append pk & check ACTIVE_FLAG
                    _fields.extend((key, key))
                    _types.extend(('__id', '__is_delete'))
                    _values.extend((value, const.ACTIVE_FLAG))

                    # append check ACTIVE_FLAG in current model
                    _fields.append('is_delete')
                    _types.append('')
                    _values.append(const.ACTIVE_FLAG)

                    # search data
                    full_list = dynamic_fk_query(abs_model, _fields, _types, _values, 'and')
                    _total = full_list.count()
                    res_list = full_list.order_by()[int(_offset):int(_limit + _offset)]

                    # check request is multiple values or individual
                    if len(_val_map) == 1:
                        # individual search condition
                        for res_detail in res_list:
                            res["data"].append(model_to_dict_filter(request, res_detail))
                    else:
                        # multiple search
                        _colet = []
                        for res_detail in res_list:
                            _colet.append(model_to_dict_filter(request, res_detail))

                        # binding
                        _qs = {str(value): _colet}
                        res["data"].append(_qs.copy())

        # binding response
        res["error"] = eCODE.RES_SUCCESS
        res["total"] = _total
        if len(res["data"]) <= 0:
            res['message'] = get_msg_content('data_empty')
    except Exception as e:
        res["error"] = eCODE.RES_FAIL
        res["message"] = str(e)

    return JsonResponse(res)


def db_get_by_multi_pk(request, abs_model, foreign_info):
    res = {"error": 0, "message": "", "data": {}}

    try:
        for key, arr_val in foreign_info.items():
            _val_map = []

            # detect string or list
            if isinstance(arr_val, (str, int)):
                # disallow single value
                res["error"] = eCODE.REQ_MULTI_REQUIRED
                res["message"] = get_msg_content('request_multi_required')
                return JsonResponse(res)
            elif isinstance(arr_val, list):
                _val_map.extend(arr_val)

            if len(_val_map) <= 0:
                res["error"] = eCODE.REQ_ID_REQUIRED
                res["message"] = get_msg_content('action_id_required', key.title())
                return JsonResponse(res)
            else:
                for value in _val_map:
                    _fields = []
                    _types = []
                    _values = []

                    # append pk & check ACTIVE_FLAG
                    _fields.extend((key, key))
                    _types.extend(('__id', '__is_delete'))
                    _values.extend((value, const.ACTIVE_FLAG))

                    # append check ACTIVE_FLAG in current model
                    _fields.append('is_delete')
                    _types.append('')
                    _values.append(const.ACTIVE_FLAG)

                    # search data
                    res_list = dynamic_fk_query(abs_model, _fields, _types, _values, 'and')

                    # multiple search
                    _colet = []
                    for res_detail in res_list:
                        _colet.append(model_to_dict_filter(request, res_detail))

                    # binding
                    res["data"][str(value)] = _colet

        # binding response
        res["error"] = eCODE.RES_SUCCESS
        if len(res["data"]) <= 0:
            res['message'] = get_msg_content('data_empty')
    except Exception as e:
        res["error"] = eCODE.RES_FAIL
        res["message"] = str(e)

    return JsonResponse(res)


def prc_get_by_multi_pk(request, abs_model, prc_name):
    res = {"error": 0, "message": ""}

    # acquire arguments
    _ids = get_req_param(request, 'id')
    _time = get_req_param(request, 'date')
    _time = get_curr_timezone() if _time == '' else _time
    _limit = get_req_param(request, 'limit')
    _limit = 100 if _limit == '' else _limit  # if limit null get 100 record
    _offset = get_req_param(request, 'offset')
    _offset = 0 if _offset == '' else _offset

    cursor = None

    try:
        _is_idv_rsl = True
        _ids_map = []
        # detect search value is single or multiple
        if isinstance(_ids, (str, int)):
            res['data'] = []
            _ids_map.append(_ids)
        elif isinstance(_ids, list):
            _is_idv_rsl = False
            res['data'] = {}
            _ids_map.extend(_ids)

        for _id in _ids_map:
            cursor = connection.cursor()
            ret = cursor.callproc(prc_name, (_id, _time, _limit, _offset))

            # fetch from qr to dict
            results = dictfetchall(cursor)
            cursor.close()

            # map dict to model
            res_list = [make_instance(abs_model(), item) for item in results]

            # Set total
            _total = res_list[0].total
            res["total"] = _total if _total else 0
            if _is_idv_rsl:
                # individual search condition
                for res_detail in res_list:
                    res["data"].append(model_to_dict_filter(request, res_detail))
            else:
                # multiple search
                _colet = []
                for res_detail in res_list:
                    # merge expected image path response
                    _colet.append(model_to_dict_filter(request, res_detail))

                # binding
                res["data"][str(_id)] = _colet

        # binding response
        res["error"] = eCODE.RES_SUCCESS
        if len(res["data"]) <= 0:
            res['message'] = get_msg_content('data_empty')

    except Exception as e:
        res["error"] = eCODE.RES_FAIL
        res["message"] = str(e)

    finally:
        cursor.close()

    return JsonResponse(res)


# Get data by using stored procedure
def prc_get_data_by_store(request, abs_model, prc_name, *params):
    res = {"error": 0, "message": ""}
    cursor = None
    try:
        cursor = connection.cursor()
        if len(params) > 0:
            ret = cursor.callproc(prc_name, params)
        else:
            ret = cursor.callproc(prc_name)
        # fetch from qr to dict
        results = dictfetchall(cursor)

        if len(results) > 0:
            results = replace_path(request, abs_model, results)
            if len(results) > 1:
                res["data"] = []
                res["data"].extend(results)
            else:
                res["data"] = {}
                res["data"].update(results[0])
        else:
            res['message'] = get_msg_content('data_empty')
    except Exception as e:
        res["error"] = eCODE.RES_FAIL
        res["message"] = str(e)
    finally:
        cursor.close()

    return JsonResponse(res)


# Get data by using stored procedure
def prc_get_data_without_path_by_store(request, abs_model, prc_name, *params):
    res = {"error": 0, "message": ""}
    cursor = None
    try:
        cursor = connection.cursor()
        if len(params) > 0:
            ret = cursor.callproc(prc_name, params)
        else:
            ret = cursor.callproc(prc_name)
        # fetch from qr to dict
        results = dictfetchall(cursor)

        if len(results) > 0:
            results = replace_path_without_model(request, results)
            if len(results) > 1:
                res["data"] = []
                res["data"].extend(results)
            else:
                res["data"] = {}
                res["data"].update(results[0])
        else:
            res['message'] = get_msg_content('data_empty')
    except Exception as e:
        res["error"] = eCODE.RES_FAIL
        res["message"] = str(e)
    finally:
        cursor.close()

    return JsonResponse(res)


# Replace image or url path
def replace_path(request, abs_model, data):
    _chk_path = ['image_path', 'url']
    _founds = [k for e in _chk_path for i in data for k in i if re.search(e, k)]

    for _found in _founds:
        for dt in data:
            _build_path = dt.get(_found)

            if _build_path is not None and not _build_path.startswith('http'):
                _build_path = aqr_current_path(request) + MEDIA_URL \
                              + abs_model._meta.object_name.lower() + '/' + _build_path
                dt.__setitem__(_found, _build_path)
    return data


# Replace image or url path
def replace_path_without_model(request, data):
    _chk_path = ['image_path', 'url']
    _founds = [k for e in _chk_path for i in data for k in i if re.search(e, k)]

    for _found in _founds:
        for dt in data:
            _build_path = dt.get(_found)

            if _build_path is not None and not _build_path.startswith('http'):
                _build_path = aqr_current_path(request) + MEDIA_URL \
                              + _build_path
                dt.__setitem__(_found, _build_path)
    return data


# --------------------------------------------------------------------
# ======================= COMMON EXECUTE QUERY =======================
# --------------------------------------------------------------------
def db_insert_or_update(request, model, act_name):
    res = {"error": 0, "message": "", "data": {}}
    try:
        # save to database
        model.save()
        res["error"] = eCODE.RES_SUCCESS
        res["message"] = get_msg_content('action_success', act_name)
        res["data"] = model_to_dict_filter(request, model)
        print(res)
    except KeyError as e:
        print(e)
        res["error"] = eCODE.REQ_MISSING_RGS
        res["message"] = get_msg_content('request_missing_args', str(e))
    except Exception as e:
        print(e)
        res["error"] = eCODE.RES_FAIL
        res["message"] = str(e)

    return JsonResponse(res)


def db_insert(request, abs_model, check_dup=None):
    res = {"error": 0, "message": "", "data": {}}
    try:
        _data = get_req_param(request, "data")
        _model = abs_model()

        if check_dup is not None:
            # check duplicate
            check_res = db_chk_dup_ins(abs_model, _data, check_dup)
            if check_res:
                return JsonResponse(check_res)

        # binding info
        _model = json_to_model(_model, _data)

        # save to database
        return db_insert_or_update(request, _model, 'Insert')
    except Exception as e:
        print(e)
        res["error"] = eCODE.RES_FAIL
        res["message"] = str(e)

    return JsonResponse(res)


def db_update(request, abs_model, check_dup=None):
    res = {"error": 0, "message": "", "data": {}}
    try:
        _data = get_req_param(request, "data")

        # get original data
        _id = _data.get('id', '')
        _model = abs_model.objects.get(pk=_id, is_delete=const.ACTIVE_FLAG)

        if check_dup is not None:
            # check duplicate
            check_res = db_chk_dup_upd(abs_model, _data, check_dup)
            if check_res:
                return JsonResponse(check_res)

        # binding info
        _model = json_to_model(_model, _data)

        # save to database
        return db_insert_or_update(request, _model, 'Update')
    except Exception as e:
        print(e)
        res["error"] = eCODE.RES_FAIL
        res["message"] = str(e)

    return JsonResponse(res)


def db_delete(request, abs_model):
    res = {"error": 0, "message": "", "data": {}}
    _model_name = abs_model.__name__[:-1].title()
    _id = get_req_param(request, 'id')
    if _id:
        try:
            _model = abs_model.objects.get(pk=_id, is_delete=const.ACTIVE_FLAG)
            if _model:
                # set delete algorithm
                _model.is_delete = const.DEL_FLAG
                _model.save()
                res["error"] = eCODE.RES_SUCCESS
                res["message"] = get_msg_content('delete_success', _model_name + 'Id', str(_id))
            else:
                res["error"] = eCODE.SCH_NOT_FOUND
                res["message"] = get_msg_content('data_not_found', _model_name + 'Id', str(_id))
        except Exception as e:
            res["error"] = eCODE.RES_FAIL
            res["message"] = get_msg_content('delete_fail_exception', _model_name, str(e))
    else:
        res["error"] = eCODE.REQ_ID_REQUIRED
        res["message"] = get_msg_content('action_id_required', _model_name + 'Id')

    print(res)
    return JsonResponse(res)


# ---------------------------------------------------------------
# ======================= COMMON CHECKING =======================
# ---------------------------------------------------------------
def db_chk_dup_ins(model, data, check_list):
    res = {}
    keys = []

    # detect string or list
    if isinstance(check_list, str):
        keys.append(check_list)
    elif isinstance(check_list, list):
        keys = check_list

    # bind condition
    for f in keys:
        v = data.get(f, '')

        # search data
        _obj = dynamic_query(model, [f], [v], 'and')

        # search founded -> existed in DB
        if _obj:
            res["error"] = eCODE.INS_EXISTED
            res['message'] = get_msg_content('insert_fail_existed', f.title(), v)
            return res

    return res


def db_chk_dup_upd(model, data, check_list):
    res = {}
    keys = []

    # detect string or list
    if isinstance(check_list, str):
        keys.append(check_list)
    elif isinstance(check_list, list):
        keys = check_list

    # bind condition
    params = []
    for f in keys:
        v = data.get(f, '')

        if v:
            parm = QueryParm(f, v, False, 'or')
            params.append(parm)

        if len(params) > 0:
            parm = QueryParm('pk', data.get('id', ''), True, 'and')
            params.append(parm)

        # search data
        _obj = dynamic_ctm_query(model, params)

        # search founded -> existed in DB
        if _obj:
            res["error"] = eCODE.INS_EXISTED
            res['message'] = get_msg_content('update_fail_existed', f.title(), v)
            return res

    return res


def db_chk_in_used(check_info, *models):
    res = {}

    # acquire arguments
    f, v = check_info.popitem()
    _obj_name = str(f[:-4]).title()

    if v:
        for model in models:
            _model_name = model.__name__[:-1].title()

            # search data
            _obj = dynamic_query(model, [f], [v], 'and')

            # search founded -> already in used
            if _obj:
                res["error"] = eCODE.DEL_USED
                res['message'] = get_msg_content('delete_fail_used', _obj_name + ' ID', str(v), _model_name)
                return res
    else:
        res["error"] = eCODE.REQ_ID_REQUIRED
        res["message"] = get_msg_content('action_id_required', _obj_name)

    return res


def db_chk_dup_ins_ctm_no_pk(model, data, check_list):
    res = {}
    keys = []

    # detect string or list
    if isinstance(check_list, str):
        keys.append(check_list)
    elif isinstance(check_list, list):
        keys = check_list

        # bind condition
        params = []
        for f in keys:
            v = data.get(f, '')

            if v:
                parm = QueryParm(f, v, False, 'and')
                params.append(parm)

        # search data
        _obj = dynamic_ctm_query(model, params)

        # search founded -> existed in DB
        if _obj:
            res["error"] = eCODE.INS_EXISTED
            res['message'] = get_msg_content('insert_fail_existed', f.title(), v)
        return res

    return res


# -------------------------------------------------------
# ======================= SIGN-IN =======================
# -------------------------------------------------------
# Home page
@csrf_exempt
def api_home(request):
    res = {"error": 1, "message": get_msg_content('api_invalid')}
    return JsonResponse(res)


# Sign in - sign out
@csrf_exempt
def sign_in(request):
    # acquire parameters
    res = {}
    username = get_req_param(request, 'username')
    password = get_req_param(request, 'password')

    if username and password:
        # synchronize with Customer
        cus_list = Customers.objects.filter(email=username, is_delete=const.ACTIVE_FLAG)

        if cus_list:
            # authorize login info
            current_user = authenticate(username=username, password=password)

            if current_user is not None:
                if current_user.is_active:
                    # register login state
                    login(request, current_user)

                    user_info = {}
                    _customer = cus_list[0]

                    # bind session info
                    request.session[const.SESSION_U_ID] = _customer.id
                    request.session[const.SESSION_U_MAP] = current_user.id
                    request.session[const.SESSION_U_AUTH] = False

                    # bind data for response
                    user_info["user_id"] = _customer.id
                    user_info["username"] = _customer.name
                    user_info["is_super_user"] = current_user.is_superuser
                    user_info["auth_token"] = create_authen_token(request, user_info["user_id"])
                    if current_user.is_superuser:
                        user_info["permission"] = "Administrator"
                    else:
                        user_info["permission"] = "Regular user"
                    # user_info["super_user"] = request.session[const.SESSION_U_AUTH]

                    # append cus info
                    user_info.update(model_to_dict_filter(request, _customer))

                    res["error"] = eCODE.RES_SUCCESS
                    res["message"] = get_msg_content('login_success')
                    res["data"] = user_info

                else:
                    res["error"] = eCODE.RES_FAIL
                    res["message"] = get_msg_content('login_acc_inactivated')
            else:
                res["error"] = eCODE.LGN_AUTH_FAILED
                res["message"] = get_msg_content('login_fail')
        else:
            res["error"] = eCODE.RES_FAIL
            res["message"] = get_msg_content('login_fail')
    else:
        res["error"] = eCODE.RES_FAIL
        res["message"] = get_msg_content('login_u_pwd_required')

    return JsonResponse(res)


@csrf_exempt
def sign_out(request):
    res = {}

    logout(request)
    res["error"] = eCODE.RES_SUCCESS
    res["message"] = get_msg_content('logout_success')

    return JsonResponse(res)


# ----------------------------------------------------------------
# ======================= PASSWORD CONTROL =======================
# ----------------------------------------------------------------
# Reset password
@csrf_exempt
def req_reset_password(request):
    res = {}
    user_info = {}

    try:
        # get current User info
        _uid = request.session[const.SESSION_U_MAP]
        _cus_id = get_req_param(request, 'user_id')
        user = User.objects.get(pk=_uid)

        # make random password
        _random_pwd = User.objects.make_random_password()

        # apply new password
        user.set_password(_random_pwd)
        user.save()

        customer = Customers.objects.get(pk=_cus_id, is_delete=const.ACTIVE_FLAG)
        customer.recover_password = _random_pwd
        customer.save()

        user_info["user_id"] = _cus_id
        user_info["recover_password"] = _random_pwd

        res["error"] = eCODE.RES_SUCCESS
        res["message"] = get_msg_content('password_reset_success')
        res["data"] = user_info
    except Exception as e:
        print(e)
        res["error"] = eCODE.RES_FAIL
        res["message"] = str(e)

    return JsonResponse(res)


# Change password
@csrf_exempt
def req_change_password(request):
    res = {}
    user_info = {}

    try:
        # get Customer info
        _cusid = get_req_param(request, 'user_id')
        _old_password = get_req_param(request, 'old_password')
        _new_password = get_req_param(request, 'new_password')

        # get sync User info
        customer = Customers.objects.get(pk=_cusid, is_delete=const.ACTIVE_FLAG)
        _uid = customer.user_id

        # apply change password
        user = User.objects.get(pk=_uid)
        if user.check_password(_old_password):
            user.set_password(_new_password)
            user.save()
        else:
            res["error"] = eCODE.RES_FAIL
            res["message"] = get_msg_content('password_change_fail_old_wrong')
            return JsonResponse(res)

        customer.login_password = _new_password
        customer.recover_password = ''
        customer.save()

        user_info["user_id"] = _cusid
        user_info["new_password"] = _new_password

        res["error"] = eCODE.RES_SUCCESS
        res["message"] = get_msg_content('password_change_success')
        res["data"] = user_info
    except Exception as e:
        print(e)
        res["error"] = eCODE.RES_FAIL
        res["message"] = str(e)

    return JsonResponse(res)


# Forgot password (reset with random password)
@csrf_exempt
def req_forgot_password(request):
    res = {}

    try:
        # Check existed email or phone
        cus_info = get_req_param(request, 'cus_info')
        lang = get_req_param(request, 'lang')
        # search data
        res_list = Customers.objects.filter(Q(email=cus_info) | Q(phone=cus_info) & Q(is_delete=const.ACTIVE_FLAG))

        if len(res_list) > 0:
            cus = res_list[0]

            # create token
            _token = make_token()
            # make random password
            _random_pwd = User.objects.make_random_password()

            # save token
            cus.recover_password = _random_pwd
            cus.access_token = _token
            cus.save()

            # send email
            send_mail_reset_pwd(lang, aqr_current_path(request), cus.name, cus.email, _random_pwd, _token)

            res["error"] = eCODE.RES_SUCCESS
            res["message"] = 'Success'
        else:
            res["error"] = eCODE.RES_FAIL
            res["message"] = 'Invalid email or phone, please try again!'

    except Exception as e:
        print(e)
        res["error"] = eCODE.RES_FAIL
        res["message"] = str(e)

    return JsonResponse(res)


# Make random token for sending email to customer
def make_token():
    _token = User.objects.make_random_password(30)

    # check token existed
    chk = Customers.objects.filter(recover_password=_token)

    if chk:
        return make_token()

    return _token


# Send email reset password
def send_mail_reset_pwd(lang, path, user_name, to_email, random_pwd, token):
    try:
        _lang = str(lang).lower()
        subject = 'roann style club Apps - Reminder Password' if _lang == 'en' else 'roann style club 程式 - 提醒密碼'
        to = [to_email]
        from_email = DEFAULT_FROM_EMAIL

        plaintext = get_template('email/forgot_pwd_en.txt' if _lang == 'en' else 'email/forgot_pwd_cn.txt')
        html_form = get_template('email/forgot_pwd_en.html' if _lang == 'en' else 'email/forgot_pwd_cn.html')

        srv_path = path
        _tmp_token = User.objects.make_random_password(60)
        url = srv_path + '/customer/passwordchange' \
                         '?token=' + _tmp_token + ';csrf=' + token + ';lang=' + _lang

        ctx = Context({
            'user': user_name,
            'new_password': random_pwd,
            'url': url
        })

        text_content = plaintext.render(ctx)
        html_content = html_form.render(ctx)
        msg = EmailMultiAlternatives(subject, html_content, from_email=from_email, to=to)
        msg.attach_alternative(text_content, "text/html")
        msg.content_subtype = 'html'
        msg.send()

    except Exception as e:
        raise Exception(e)

    return True


# Send email reserve
def send_mail_reserve(request):
    try:
        _data = get_req_param(request, "data")
        lang = get_req_param(request, 'lang')
        _lang = str(lang).lower()
        subject = 'Delivery Prize Confirmation' if _lang == 'en' else '豊洲水產海鮮丼專門店訂位確認'
        from_email = DEFAULT_FROM_EMAIL
        to = [_data['email']]
        num_pax = str(_data['num_adult'] + _data['num_child'])
        res_id = _data['restaurant']
        res_obj = Restaurants.objects.get(pk=res_id)
        res_name = res_obj.name if _lang == 'en' else res_obj.name_cn
        res_address = res_obj.address if _lang == 'en' else res_obj.address_cn
        reserve_type = 'Normal table' if _data['reserve_type'] == 0 else 'VIP room'
        rv_date = dateutil.parser.parse(_data['reserve_date']).strftime("%B %d, %Y")
        rv_time = dateutil.parser.parse(_data['reserve_time']).strftime("%H:%M")

        plaintext = get_template('email/reserve_en.txt' if _lang == 'en' else 'email/reserve_cn.txt')
        html_form = get_template('email/reserve_en.html' if _lang == 'en' else 'email/reserve_cn.html')

        ctx = Context({
            'name': _data['name'],
            'phone': _data['phone'],
            'email': _data['email'],
            'reserve_type': reserve_type,
            'reserve_date': rv_date,
            'reserve_time': rv_time,
            'num_pax': num_pax,
            'num_adult': _data['num_adult'],
            'num_child': _data['num_child'],
            'comment': _data['comment'],
            'res_name': res_name,
            'res_phone': res_obj.phone,
            'res_email': res_obj.email,
            'res_address': res_address,
        })

        text_content = plaintext.render(ctx)
        html_content = html_form.render(ctx)
        msg = EmailMultiAlternatives(subject, text_content, from_email=from_email, to=to)
        msg.attach_alternative(html_content, "text/html")
        msg.send()

    except Exception as e:
        raise Exception(e)

    return True


# Send email reserve operator
def send_mail_reserve_operator(request):
    try:
        _data = get_req_param(request, "data")
        lang = get_req_param(request, 'lang')
        _lang = str(lang).lower()
        subject = 'New Booking' if _lang == 'en' else '新預訂'
        from_email = DEFAULT_FROM_EMAIL
        res_id = _data['restaurant']
        res_obj = Restaurants.objects.get(pk=res_id)
        to = [res_obj.email]
        num_pax = str(_data['num_adult'] + _data['num_child'])
        res_id = _data['restaurant']
        res_obj = Restaurants.objects.get(pk=res_id)
        res_name = res_obj.name if _lang == 'en' else res_obj.name_cn
        reserve_type = 'Normal table' if _data['reserve_type'] == 0 else 'VIP room'
        rv_date = dateutil.parser.parse(_data['reserve_date']).strftime("%B %d, %Y")
        rv_time = dateutil.parser.parse(_data['reserve_time']).strftime("%H:%M")

        plaintext = get_template('email/reserve_ope_en.txt' if _lang == 'en' else 'email/reserve_ope_cn.txt')
        html_form = get_template('email/reserve_ope_en.html' if _lang == 'en' else 'email/reserve_ope_cn.html')

        ctx = Context({
            'name': _data['name'],
            'phone': _data['phone'],
            'email': _data['email'],
            'reserve_type': reserve_type,
            'reserve_date': rv_date,
            'reserve_time': rv_time,
            'num_pax': num_pax,
            'num_adult': _data['num_adult'],
            'num_child': _data['num_child'],
            'comment': _data['comment'],
            'res_name': res_name,
        })

        text_content = plaintext.render(ctx)
        html_content = html_form.render(ctx)
        msg = EmailMultiAlternatives(subject, text_content, from_email=from_email, to=to)
        msg.attach_alternative(html_content, "text/html")
        msg.send()

    except Exception as e:
        raise Exception(e)

    return True


# Get running path of server
def aqr_current_path(request):
    current_path = request._get_scheme() + '://' + request._get_raw_host()
    return current_path


# Get user info by token
def aqr_u_info_by_token(token):
    cus = None

    try:
        # get user info by token
        cus_list = Customers.objects.filter(access_token=token)

        if cus_list:
            cus = cus_list[0]

    except Exception as e:
        raise Exception(e)

    return cus


# Apply reset password
def active_new_pwd(token):
    _new_token = None
    try:
        # get user info by token
        cus = aqr_u_info_by_token(token)

        if cus:
            _uid = cus.user_id
            _new_pwd = cus.recover_password

            # get auth user
            user = User.objects.get(pk=_uid)

            # apply new password
            user.set_password(_new_pwd)
            user.save()

            # update token
            _new_token = make_token()
            cus.login_password = _new_pwd
            cus.recover_password = None
            cus.access_token = _new_token
            cus.save()

    except Exception as e:
        print(e)

    return _new_token


def apy_new_pwd(token, pwd_new):
    try:
        # get user info by token
        cus = aqr_u_info_by_token(token)

        if cus:
            _uid = cus.user_id

            # get auth user
            user = User.objects.get(pk=_uid)

            # apply new password
            user.set_password(pwd_new)
            user.save()

            # update token
            cus.login_password = pwd_new
            cus.access_token = None
            cus.save()
        else:
            return False

    except Exception as e:
        print(e)
        return False

    return True


# -----------------------------------------------------------
# ======================= UPLOAD FILE =======================
# -----------------------------------------------------------
@csrf_exempt
def upload_file(request):
    res = {}
    # if request.method != 'POST':
    #     res["error"] = eCODE.RES_FAIL
    #     res["message"] = str(e)
    #     return HttpResponseNotAllowed('Only POST here')
    #
    # form = UploadFileForm(request.POST, request.FILES)
    # if not form.is_valid():
    #     res["error"] = eCODE.RES_FAIL
    #     res["message"] = str(e)
    #     return HttpResponseServerError("Invalid call")
    #
    # handle_uploaded_file(request.FILES['file'])
    #
    # res["error"] = eCODE.RES_FAIL
    # res["message"] = str(e)
    # return HttpResponse('OK')
    return


# -----------------------------------------------------------------------
# ======================= LOCATE LANGUAGE CONTROL =======================
# -----------------------------------------------------------------------
# Change system display language
@csrf_exempt
def change_language(request):
    res = {}
    lang_code = get_req_param(request, 'lang_code')
    if lang_code:
        translation.activate(lang_code)
        request.session[translation.LANGUAGE_SESSION_KEY] = lang_code
        res["error"] = eCODE.RES_SUCCESS
        res["message"] = get_msg_content('lang_change_success')
    else:
        res["error"] = eCODE.RES_FAIL
        res["message"] = get_msg_content('lang_change_fail')

    return JsonResponse(res)


# -------------------------------------------------------------
# ======================= Errors handle =======================
# -------------------------------------------------------------
def page_not_found(request):
    # Dict to pass to template, data could come from DB query
    values_for_template = {}
    return render(request, 'errors/404.html', values_for_template, status=404)


def server_error(request):
    # Dict to pass to template, data could come from DB query
    values_for_template = {}
    return render(request, 'errors/500.html', values_for_template, status=500)


def bad_request(request):
    # Dict to pass to template, data could come from DB query
    values_for_template = {}
    return render(request, 'errors/400.html', values_for_template, status=400)


def permission_denied(request):
    # Dict to pass to template, data could come from DB query
    values_for_template = {}
    return render(request, 'errors/403.html', values_for_template, status=403)


def srv_upload(request):
    # Handle file upload
    if request.method == 'POST':
        form = UploadFileForm(request.POST, request.FILES)
        if form.is_valid():
            new_file = Document(file=request.FILES['file'])
            new_file.save()

            # Redirect to the document list after POST
            return HttpResponseRedirect(reverse('srv_upload'))
    else:
        form = UploadFileForm()  # A empty, unbound form

    # Load documents for the list page
    documents = Document.objects.all()

    # Render list page with the documents and the form
    return render(
        request,
        'upload/list.html',
        {'documents': documents, 'form': form}
    )


# Get related items
@csrf_exempt
def get_related_items(request):
    restaurant_id = get_req_param(request, "restaurant_id")
    obj_id = get_req_param(request, "obj_id")
    obj_type = get_req_param(request, "obj_type")
    hash_tags = get_req_param(request, "hash_tags")
    limit = get_req_param(request, "limit")
    res = prc_get_data_without_path_by_store(request, Topics, 'search_related_items', restaurant_id,
                                             obj_id, obj_type,
                                             hash_tags, limit)

    return res


# Get related items version 2
@csrf_exempt
def get_related_items_v2(request):
    restaurant_id = get_req_param(request, "restaurant_id")
    customer_id = get_req_param(request, "customer_id")
    obj_id = get_req_param(request, "obj_id")
    obj_type = get_req_param(request, "obj_type")
    hash_tags = get_req_param(request, "hash_tags")
    limit = get_req_param(request, "limit")
    res = prc_get_data_without_path_by_store(request, Topics, 'search_related_items_v2', restaurant_id, customer_id,
                                             obj_id, obj_type,
                                             hash_tags, limit)

    return res


# Insert pos+ data
@csrf_exempt
def insert_pos_data(request):
    string = get_req_param(request, "string")
    res = prc_get_data_without_path_by_store(request, Topics, 'import_pos', string)

    return res


# --------------------------------------------------------------
# ======================= CMS- Core  =======================
# --------------------------------------------------------------

# Get all list
def cms_get_all(request, abs_model):
    res = []
    res_list = abs_model.objects.filter(is_delete=const.ACTIVE_FLAG)

    for res_detail in res_list:
        res.append(res_detail)

    return res


# Get all list for json
def cms_get_all_json(request, abs_model):
    res_list = abs_model.objects.filter(is_delete=const.ACTIVE_FLAG)
    return res_list
