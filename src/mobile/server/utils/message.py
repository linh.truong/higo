#!/usr/bin/env python
# -*- coding: utf-8 -*-
from django.utils.translation import ugettext_lazy as _

hash_messages = \
    {
        'api_invalid': _(u'Invalid API URL. Please contact Admin.'),
        'request_restricted': _(u'Restricted request! Please sign-in again.'),
        'request_not_permission': _(u'User do not have permission to modify!'),
        'request_missing_args': _(u'The request is missing argument: %s.'),
        'request_single_required': _(u'The value of request must be a single value.'),
        'request_multi_required': _(u'The value of request must be a list.'),

        'lang_change_success': _(u'System language has been changed.'),
        'lang_change_fail': _(u'Some thing wrong when change system language.'),

        'login_success': _(u'Login successfully.'),
        'login_fail': _(u'Invalid login. Please contact your manager.'),
        'login_u_pwd_required': _(u'Please input user and password.'),
        'login_acc_inactivated': _(u'Your account is inactivated. Please contact your manager.'),
        'login_acc_not_permission': _(u'Your account is not have permission. Please contact your manager.'),
        'login_required': _(u'You are not logged in! Please sign-in to use this function.'),
        'logout_success': _(u'You are logged out.'),

        'password_reset_success': _(u'Password is completed reset. Please read the email for new password.'),
        'password_change_success': _(u'Password is successfully changed.'),
        'password_change_fail_old_wrong': _(u'Old password is wrong. Please try again.'),

        'action_id_required': _(u'%s ID is required.'),
        'action_missing_condition': _(u'There is no condition for searching.'),
        'action_success': _(u'%s successfully.'),
        'action_fail': _(u'%s failed.'),

        'insert_fail_existed': _(u'%s : %s is existed in database.'),
        'update_fail_existed': _(u'%s : %s is existed in database.'),

        'delete_success': _(u'%s : %s is deleted.'),
        'delete_fail_used': _(u'%s : %s is already used in %s.'),
        'delete_fail_deleted': _(u'This %s : %s was deleted in database.'),
        'delete_fail_exception': _(u'Delete %s failed : %s.'),

        'data_not_found': _(u'This %s : %s is not existed in database or deleted.'),
        'data_empty': _(u'Empty data.'),

        'user_point_not_enough': _(u'Your points is not enough to charge. Please try with another once.'),
        'empty_stock': _(u'Your selected item is empty now. Please choose another once.'),

        'update_boss_error': _(u'Update boss for user %s unsuccessfully.'),

        'action_required': _(u'%s is required.'),

        'action_used_exp': _(u'%s is already used or expire.'),

        'used_qr_code_success': _(u'Active QR Code successfully your point is : %s '),

        'not_found': _(u'%s is not existed in database or deleted.'),
    }


def get_msg_content(msg_id=None, *value):
    if msg_id in hash_messages and not value:
        return hash_messages[msg_id]
    elif msg_id in hash_messages and value:
        return hash_messages[msg_id] % value
    else:
        return ''
