class ErrCode:
    RES_SUCCESS = 0                 # response state SUCCESSFUL
    RES_FAIL = 1                    # response state FAILED
    RES_NOT_FOUND = 2               # response state NOT FOUND

    REQ_RESTRICTED = 80             # login required
    REQ_NOT_PERMISSION = 90         # user login not have permission
    REQ_MISSING_RGS = 88            # the request is missing argument
    REQ_ID_REQUIRED = 89            # the request is missing id parameter
    REQ_SINGLE_REQUIRED = 91             # the value of request must be a single value
    REQ_MULTI_REQUIRED = 91              # the value of request must be a list

    LGN_U_P_REQUIRED = 11           # login - user name & password required
    LGN_INACTIVATED = 12            # login - user login is inactivated
    LGN_U_NOT_PERMISSION = 13       # login - user login not have permission to use app
    LGN_REQUIRED = 18               # login required for using function
    LGN_AUTH_FAILED = 19            # login failed

    SCH_NOT_FOUND = 21              # search - data not found

    INS_EXISTED = 31                # insert - already existed
    INS_LACK_POINT = 37             # insert - user point is not enough for using
    INS_EMPTY_STOCK = 38            # insert - quantity is empty

    UPD_NOT_EXIST = 41              # update - not exist data to update

    DEL_NOT_EXIST = 51              # delete - not exist data to delete
    DEL_USED = 52                   # delete - delete item is already in used

    MINUS_POINT_FAIL = 69           # minus point error

    UNKNOWN = 99                    # unknown error
    RES_TIMEOUT = 105               # timeout
    SIMPLYBOOK_FULL_TABLE = -32054  # simplybook table type full
    SIMPLYBOOK_FULL_ALL = 6969      # simplybook table type full
