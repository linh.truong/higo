# Mode
SYSTEM_MODE = 'higo.settings_dev'  # LOCAL: higo.settings_dev; STG: higo.settings_stg; PROD: higo.settings_prod

# LOCAL DB
LOCAL_DB_NAME = 'tsuapi'
LOCAL_DB_HOST = 'localhost'
LOCAL_DB_USER = 'root'
LOCAL_DB_PWD = 'vertrigo'

# STG DB
STG_DB_NAME = 'higo_mb_stg'
STG_DB_HOST = 'localhost'
STG_DB_USER = 'higo_mb_stg'
STG_DB_PWD = 'trbFhTqdJ7m6xxnM'

# DEV DB
DEV_DB_NAME = 'higo_mb_dev'
DEV_DB_HOST = 'vl00044'
DEV_DB_USER = 'inteasia'
DEV_DB_PWD = '12345678'

# PROD DB
PROD_DB_NAME = 'higo_mb_prod'
PROD_DB_HOST = 'localhost'
PROD_DB_USER = 'higo_mb_prod'
PROD_DB_PWD = 'UVrUs4Yj5fV2FHTR'

# Multiple restaurant config
MULTIPLE_RES = '0'  # 0: Single; 1: Multiple
DEL_FLAG = 1
ACTIVE_FLAG = 0

# Format output response
IMG_PATH = 'image_path'
URL_PATH = 'google_map_url'
START_TIME = 'start_time'
END_TIME = 'end_time'

# Session storage
HASH_SESSION_KEY = '_auth_user_hash'
SESSION_U_ID = '_auth_sign_in_id'
SESSION_U_MAP = '_auth_sign_in_mapping_id'
SESSION_U_AUTH = '_auth_sign_in_auth'
SESSION_TIME_OUT = 99999999

# Session CMS storage
SESSION_CMS_U_ID = 'cms_auth_sign_in_id'
SESSION_CMS_U_MAP = 'cms_auth_sign_in_mapping_id'
SESSION_CMS_R_MAP = 'cms_auth_sign_in_mapping_role'
SESSION_CMS_R_NAME_MAP = 'cms_auth_sign_in_mapping_role_name'
SESSION_CMS_U_AUTH = 'cms_auth_sign_in_auth'
SESSION_CMS_U_FNAME = 'cms_auth_sign_in_mapping_fname'
SESSION_CMS_CURR_RES_ID = 'cms_auth_sign_in_curr_res_id'
SESSION_CMS_RES_LIST = 'res_list'

# CMS constant
CMS_LOGIN_URL = '/cms/login/'

CMS_ROLE_MENTE = 1

CMS_ROLE_LIST = (
    (1, 'MENTE'),
    (2, 'RES GROUP'),
    (3, 'RES'),
)

CMS_RCD_STATUS = (
    (0, 'ACTIVE'),
    (1, 'DELETED'),
)

CMS_NOTIFY_STATUS = (
    (0, 'No Receive'),
    (1, 'Receive'),
)

CMS_SEND_STATUS = (
    (0, 'Postpone'),
    (1, 'Was sent'),
)

CMS_COUPONS_TYPE = (
    (1, 'Welcome'),
    (2, 'Second visit'),
    (3, 'Birthday'),
    (4, 'Other'),
)

CMS_RESERVE_TYPE = (
    (0, 'Normal table'),
    (1, 'VIP room'),
)

CMS_STATE = (
    (1, 'Request'),
    (2, 'Processing'),
    (3, 'Finished'),
    (4, 'Cancel'),
)

CMS_WEEK_DAYS = (
    (1, 'Sunday'),
    (2, 'Monday'),
    (3, 'Tuesday'),
    (4, 'Wednesday'),
    (5, 'Thursday'),
    (6, 'Friday'),
    (7, 'Saturday'),
)

CMS_GENDER_TYPE = (
    (0, 'Male'),
    (1, 'Female'),
    (2, 'Unknown'),
)

CMS_PRIZE_DELIVERY_STATE = (
    (1, 'Not Delivery'),
    (2, 'Sent'),
    (3, 'Received'),
    (4, 'Canceled'),
)

CMS_COUPON_STATUS = (
    (1, 'Registered'),
    (2, 'Used'),
)

FILTER_LIST = ['user_id', 'password', 'token']

# Define media folder
MEDIA_URL = '/static/media/'
MEDIA_CUSTOMER = "/static/media/customers/"
MEDIA_FOOD = "/static/media/foods/"
MEDIA_TOPIC = "/static/media/topics/"
MEDIA_COUPON = "/static/media/coupons/"
MEDIA_ITEM = "/static/media/items/"
MEDIA_MEMBERSHIP = "/static/media/memberships/"
# MEDIA_BARCODE = "/static/media/memberships/barcode/"
# MEDIA_QRCODE = "/static/media/memberships/qrcode/"

# Mail info
DEFAULT_FROM_EMAIL = 'noreply@discoverjp.info'
DEFAULT_OPE_EMAIL = 'info@roann.hk'

# Simplybook
BOOKING_URL = 'http://user-api.simplybook.me'

# Instagram
INSTAGRAM_URL = 'https://api.instagram.com/v1/users/self/media/recent?access_token='
