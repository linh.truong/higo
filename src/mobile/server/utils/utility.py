from datetime import datetime, timedelta

from dateutil.tz import tzlocal
from dateutil.tz import tzutc
from django.contrib.sessions.models import Session
from django.utils import timezone
from functools import wraps

from django.core.checks import messages
from django.http import HttpResponseRedirect, JsonResponse

from django.utils.decorators import available_attrs

from utils import constants as const
from utils.message import get_msg_content
from utils.errcode import ErrCode as eCODE
import hashlib
from xlrd import open_workbook


# -------------------------------------------------------------
# ======================= AUTHORIZATION =======================
# -------------------------------------------------------------
# Common check authorize
def is_customer(login_url='/managers/login/'):
    def decorator(view):
        @wraps(view, assigned=available_attrs(view))
        def wrapper(request, *args, **kwargs):
            valid = False
            if request.user.is_authenticated():
                try:
                    current_customer = request.session['current_customer']
                    return view(request, *args, **kwargs)

                except KeyError:
                    messages.error(request, 'Please Login')
                    return HttpResponseRedirect(request, login_url)
            else:
                messages.error(request, 'Please Login')
                return HttpResponseRedirect(login_url)

        return wrapper

    return decorator


# Common check authorize on each request
def is_authorize(function=None):
    """
    Decorator for views that checks that the user is logged in.
    """
    actual_decorator = user_passes_test(
        lambda u: u.is_authenticated
    )
    if function:
        return actual_decorator(function)
    return actual_decorator


def user_passes_test(test_func):
    """
    Decorator for views that checks that the user passes the given test,
    redirecting to the log-in page if necessary. The test should be a callable
    that takes the user object and returns True if the user passes.
    """

    def decorator(view_func):
        @wraps(view_func, assigned=available_attrs(view_func))
        def _wrapped_view(request, *args, **kwargs):
            if test_func(request.user):
                return view_func(request, *args, **kwargs)
            # res = {"error": eCODE.REQ_RESTRICTED, "message": get_msg_content('request_restricted')}
            res = {"error": eCODE.LGN_REQUIRED, "message": get_msg_content('login_required')}
            return JsonResponse(res)

        return _wrapped_view

    return decorator


# Common check super user
def is_super_user(function=None):
    actual_decorator = permission_check(
        lambda r: r is True
    )
    if function:
        return actual_decorator(function)
    return actual_decorator


def permission_check(test_func):
    def decorator(view_func):
        @wraps(view_func, assigned=available_attrs(view_func))
        def _wrapped_view(request, *args, **kwargs):
            if test_func(request.session.get(const.SESSION_U_AUTH, False)):
                return view_func(request, *args, **kwargs)
            res = {"error": eCODE.REQ_NOT_PERMISSION, "message": get_msg_content('request_not_permission')}
            return JsonResponse(res)

        return _wrapped_view

    return decorator


# ---------------------------------------------------------------
# ======================= COMMON FUNCTION =======================
# ---------------------------------------------------------------
def dictfetchall(cursor):
    """Returns all rows from a cursor as a dict"""
    desc = cursor.description
    return [
        dict(zip([col[0] for col in desc], row))
        for row in cursor.fetchall()
    ]


def make_instance(instance, values):
    """
    Copied from eviscape.com

    generates an instance for dict data coming from an sp

    expects:
        instance - empty instance of the model to generate
        values -   dictionary from a stored procedure with keys that are named like the
                   model's attributes
    use like:
        evis = InstanceGenerator(Evis(), evis_dict_from_SP)

    >>> make_instance(Evis(), {'evi_id': '007', 'evi_subject': 'J. Bond, Architect'})
    <Evis: J. Bond, Architect>

    """
    attributes = filter(lambda x: not x.startswith('_'), instance.__dict__.keys())

    for a in attributes:
        try:
            # field names from oracle sp are UPPER CASE
            # we want to put PIC_ID in pic_id etc.
            setattr(instance, a, values[a.upper()])
            del values[a.upper()]
        except:
            pass

    # add any values that are not in the model as well
    for v in values.keys():
        setattr(instance, v, values[v])
        # print('setting %s to %s' % (v, values[v]))

    return instance


# -------------------------------------------------------------
# ======================= DYNAMIC QUERY =======================
# -------------------------------------------------------------
def dynamic_fk_query(model, fields, types, values, operator):
    """
     Takes arguments & constructs Qs for filter()
     We make sure we don't construct empty filters that would
        return too many results
     We return an empty dict if we have no filters so we can
        still return an empty response from the view
    """
    from django.db.models import Q

    queries = []
    for (f, t, v) in zip(fields, types, values):
        # We only want to build a Q with a value
        if v != "":
            kwargs = {str('%s%s' % (f, t)): str('%s' % v)}
            queries.append(Q(**kwargs))

    # Make sure we have a list of filters
    if len(queries) > 0:
        q = Q()
        # AND/OR awareness
        for query in queries:
            if operator == "and":
                q = q & query
            elif operator == "or":
                q = q | query
            else:
                q = None
        if q:
            # We have a Q object, return the QuerySet
            return model.objects.filter(q)
    else:
        # Return an empty result
        return {}


def dynamic_query(model, fields, values, operator):
    from django.db.models import Q

    queries = []

    for (f, v) in zip(fields, values):
        # We only want to build a Q with a value
        if v != "":
            kwargs = {str('%s' % f): str('%s' % v)}
            queries.append(Q(**kwargs))

    # Make sure we have a list of filters
    if len(queries) > 0:
        q = Q()
        # AND/OR awareness
        for query in queries:
            if operator == "and":
                q = q & query
            elif operator == "or":
                q = q | query
            else:
                q = None
        if q:
            # We have a Q object, return the QuerySet
            return model.objects.filter(q)
    else:
        # Return an empty result
        return {}


def dynamic_ctm_query(model, params):
    from django.db.models import Q
    q = Q()

    for parm in params:
        kwargs = {str('%s' % parm.field): str('%s' % parm.value)}

        if parm.not_equal is False:
            query = Q(**kwargs)
        else:
            query = ~Q(**kwargs)

        if parm.operator == 'and':
            q = q & query
        elif parm.operator == 'or':
            q = q | query

    if q:
        # We have a Q object, return the QuerySet
        return model.objects.filter(q)
    else:
        # Return an empty result
        return {}


# ---------------------------------------------------------
# ======================= DATE TIME =======================
# ---------------------------------------------------------
def get_curr_date() -> object:
    _now = datetime.now()
    return _now


def get_curr_timezone():
    _now = timezone.now()
    return _now


def count_rm_day(to_date=None, from_date=None):
    date_format = "%m/%d/%Y"
    _to_date = datetime.strptime(to_date.strftime("%m/%d/%Y"), date_format)
    if from_date is not None:
        _curr_date = datetime.strptime(from_date().strftime("%m/%d/%Y"), date_format)
    else:
        _curr_date = datetime.strptime(get_curr_date().strftime("%m/%d/%Y"), date_format)

    _rm_date = _to_date - _curr_date
    return _rm_date.days


class QueryParm(object):
    def __init__(self, field, value, not_equal=False, operator='and'):
        self.field = field
        self.value = value
        self.not_equal = not_equal
        self.operator = operator


# Check & convert string to number
def is_num(data):
    try:
        int(data)
        return True
    except ValueError:
        return False


# hash string to md5
def res_info_md5_hash(res_info):
    m = hashlib.md5()
    m.update(res_info.encode('utf-8'))
    return str(m.hexdigest())


# read from excel file
def read_from_excel_file(file_name):
    excel_data = ''
    try:
        wb = open_workbook(file_contents=file_name)
        col_name_format = ['Customer ID', 'Customer Name', 'Membership ID', 'Gender', 'Age', 'E-Mail', 'Tel.', 'ByDay',
                           'ByBill', 'Product ID', 'Product Name', 'Combo Code', 'Quantity', 'Sales $']
        col_name_list = []
        col_titles = wb.sheets()[0].row(0)
        if len(col_titles) == 14:
            for col in col_titles:
                col_name_list.append(col.value)
        if col_name_list == col_name_format:
            for s in wb.sheets():
                for row in range(1, s.nrows):
                    row_data = ''
                    col_names = s.row(0)
                    for name, col in zip(col_names, range(s.ncols)):
                        value = s.cell(row, col).value
                        try:
                            value = str(int(value))
                        except:
                            pass
                        if row_data:
                            row_data += '|' + value.replace("\'", "***")
                        else:
                            row_data = value.replace("\'", "***")

                    excel_data += row_data + ';'
            excel_data = excel_data[:-1]
    except Exception as e:
        print(e)

    return excel_data
