from django import forms
from django.forms import ModelChoiceField

from commonmodels.models import Customers, Areas, CustomerPrizes, Prizes, Coupons, Seasons
from utils import constants as const

import datetime


# Make area list
class AreaModelChoiceField(ModelChoiceField):
    def label_from_instance(self, obj):
        try:
            return obj.name
        except AttributeError:
            return obj.name


# Customer form
class CustomersInfoForm(forms.ModelForm):
    area = AreaModelChoiceField(
        queryset=None, empty_label=None, required=True, widget=forms.Select(attrs={'class': 'form-control'}))

    number = forms.CharField(
        widget=forms.TextInput(attrs={'class': 'form-control', 'required': 'required', 'readonly': 'readonly'}))

    name = forms.CharField(
        widget=forms.TextInput(attrs={'class': 'form-control', 'required': 'required'}), max_length=50)

    age = forms.IntegerField(
        widget=forms.TextInput(attrs={'class': 'form-control cms_form_number', 'required': 'required'}))

    birthday = forms.DateTimeField(
        widget=forms.DateTimeInput(
            attrs={'class': 'form-control form-control-inline input-medium default-date-picker'},
            format="%Y-%m-%d"), required=False)

    current_point = forms.IntegerField(
        widget=forms.TextInput(attrs={'class': 'form-control cms_form_number', 'required': 'required'}))

    total_point = forms.IntegerField(
        widget=forms.TextInput(attrs={'class': 'form-control cms_form_number', 'readonly': 'readonly'}), required=False)

    num_visit = forms.IntegerField(
        widget=forms.TextInput(attrs={'class': 'form-control cms_form_number', 'readonly': 'readonly'}), required=False)

    level_up_date = forms.DateTimeField(
        widget=forms.DateTimeInput(attrs={'class': 'form-control cms_form_number', 'readonly': 'readonly'},
                                   format="%Y-%m-%d"), required=False)

    sex = forms.ChoiceField(
        choices=const.CMS_GENDER_TYPE, required=True, widget=forms.Select(attrs={'class': 'form-control'}))

    address = forms.CharField(widget=forms.Textarea(attrs={'class': 'form-control', 'rows': 3}), max_length=200,
                              required=False)

    gps_data = forms.CharField(
        widget=forms.TextInput(attrs={'class': 'form-control'}), max_length=200, required=False)

    email = forms.EmailField(
        widget=forms.EmailInput(attrs={'class': 'form-control', 'required': 'required'}), max_length=50)

    login_password = forms.CharField(
        widget=forms.TextInput(attrs={'class': 'form-control', 'required': 'required', 'type': 'password'}),
        max_length=50)

    phone = forms.CharField(
        widget=forms.TextInput(attrs={'class': 'form-control cms_form_number', 'required': 'required'}), max_length=20)

    is_mail_notify = forms.ChoiceField(
        choices=const.CMS_NOTIFY_STATUS, required=True, widget=forms.Select(attrs={'class': 'form-control'}))

    is_sms_notify = forms.ChoiceField(
        choices=const.CMS_NOTIFY_STATUS, required=True, widget=forms.Select(attrs={'class': 'form-control'}))

    class Meta:
        model = Customers
        fields = (
            'area', 'number', 'name', 'age', 'current_point', 'sex', 'address', 'gps_data',
            'email', 'login_password', 'phone', 'is_mail_notify', 'is_sms_notify', 'level_up_date',
            'num_visit', 'total_point', 'birthday'
        )

    def __init__(self, *args, **kwargs):
        super(CustomersInfoForm, self).__init__(*args, **kwargs)
        self.fields['area'].queryset = Areas.objects.filter(is_delete=const.ACTIVE_FLAG)
        self.fields['sex'].initial = 2
        self.fields['is_mail_notify'].initial = 0
        self.fields['is_sms_notify'].initial = 0


# Customer Prize edit form
class CustomerPrizeForm(forms.ModelForm):
    tracking_no = forms.CharField(
        widget=forms.TextInput(attrs={'class': 'form-control cms_form_number'}), required=False)

    # customer = forms.CharField(
    #     widget=forms.TextInput(attrs={'class': 'form-control', 'required': 'required', 'readonly': 'readonly'}),
    #     max_length=50)

    name = forms.CharField(
        widget=forms.TextInput(attrs={'class': 'form-control', 'required': 'required', 'readonly': 'readonly'}),
        max_length=50)

    email = forms.EmailField(
        widget=forms.EmailInput(attrs={'class': 'form-control', 'required': 'required', 'readonly': 'readonly'}),
        max_length=50)

    phone = forms.CharField(
        widget=forms.TextInput(
            attrs={'class': 'form-control cms_form_number', 'required': 'required', 'readonly': 'readonly'}),
        max_length=20)

    address = forms.CharField(widget=forms.Textarea(attrs={'class': 'form-control', 'rows': 3}), max_length=200,
                              required=False)
    prize = forms.ModelChoiceField(
        queryset=Prizes.objects.all(), empty_label=None, required=True, widget=forms.Select(
            attrs={'class': 'form-control', 'readonly': True}))

    use_date = forms.DateTimeField(
        widget=forms.DateTimeInput(
            attrs={'class': 'form-control cms_form_number', 'readonly': 'readonly'},
            format="%Y-%m-%d"
        ))

    delivery_date = forms.DateTimeField(
        required=False,
        widget=forms.DateTimeInput(
            attrs={'class': 'form-control form-control-inline input-medium default-date-picker'},
            format="%Y-%m-%d"))

    receive_date = forms.DateTimeField(
        required=False,
        widget=forms.DateTimeInput(
            attrs={'class': 'form-control form-control-inline input-medium default-date-picker'},
            format="%Y-%m-%d"))

    status = forms.CharField(
        widget=forms.TextInput(attrs={'class': 'form-control', 'required': 'required', 'readonly': 'readonly'}))

    customer = forms.ModelChoiceField(
        queryset=Customers.objects.all(), empty_label=None, required=True, widget=forms.Select(
            attrs={'class': 'form-control', }))

    class Meta:
        model = CustomerPrizes
        fields = (
            'tracking_no', 'name', 'email', 'phone', 'address', 'prize', 'use_date',
            'delivery_date', 'receive_date', 'status', 'customer',
        )


# Customer Gift search form
class CustomerGiftForm(forms.Form):
    season = forms.ModelChoiceField(
        queryset=Seasons.objects.all(), empty_label=None, required=True, widget=forms.Select(
            attrs={'class': 'form-control', }))
