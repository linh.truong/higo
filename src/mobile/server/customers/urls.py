"""toyosu URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.10/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url,include
import customers.views as services
urlpatterns = [
    # ---------------------------------------------------------
    # ======================= COUPONS =========================
    # ---------------------------------------------------------

    # List
    url(r'^cms/customers/list/$', services.load_list_customers_page, name='customers_list'),
    url(r'^cms/customers/customers_prize/list/$', services.load_customers_prize_list, name='customers_prize_list'),
    url(r'^cms/customers/customers_coupon/list/$', services.load_customers_coupon_list, name='customers_coupon_list'),
    url(r'^cms/customers/customers_gift/$', services.load_customer_gift_page, name='customers_gift_page'),
    url(r'^cms/pos/pos_integration/$', services.pos_integration, name='pos_integration'),
    url(r'^customers_list_json/$', services.get_list_customers_as_json, name='get_customers_list_as_json'),
    url(r'^customer_prize_list_json/$', services.get_list_customer_prize_as_json, name='get_list_customer_prize_as_json'),
    url(r'^customer_coupon_list_json/$', services.get_list_customer_coupon_as_json, name='get_list_customer_coupon_as_json'),
    # Insert
    url(r'^cms/customers/add/$', services.add_customers, name='add_customer'),
    # Edit
    url(r'^cms/customers/edit/([\d\D]+)/$', services.edit_customers, name='edit_customer'),
    url(r'^cms/customers_prize/edit/([\d\D]+)/$', services.edit_customers_prize, name='edit_customers_prize'),
    url(r'^cms/customers_coupon/edit/([\d\D]+)/$', services.edit_customers_coupon, name='edit_customers_coupon'),
    # Delete
    url(r'^cms/customers/delete/([\d\D]+)/$', services.delete_customer, name='delete_customer'),

]
