import datetime

from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.core.mail import EmailMultiAlternatives
from django.http import HttpResponseRedirect, HttpResponse
from django.shortcuts import render
from django.contrib.auth.models import User
from django.db import transaction
from django.template.loader import get_template
from django.template import Context

import commonmodels.views_cms as cms
from commonmodels.models import Customers, CustomerPrizes, Prizes, CustomerCoupons, Topics
from customers.forms import CustomersInfoForm, CustomerPrizeForm, CustomerGiftForm
from utils import constants as const
from utils import utility as util
import commonmodels.views as cmn
import json
import csv
from collections import OrderedDict
import traceback
import logging

logger = logging.getLogger(__name__)

# ---------------------------------------------------------
# ====================== CUSTOMERS ==========================
# ---------------------------------------------------------

# URL define
from utils.utility import read_from_excel_file

list_url = '/cms/customers/list'
add_url = '/cms/customers/add'
edit_url = '/cms/customers/edit/'


# Load list customers page
@login_required(login_url=const.CMS_LOGIN_URL)
def load_list_customers_page(request):
    return render(request, 'customers/list.html')


# Load list customers page
@login_required(login_url=const.CMS_LOGIN_URL)
def load_customers_prize_list(request):
    return render(request, 'customers/customer_prize_list.html')


# Load list customers page
@login_required(login_url=const.CMS_LOGIN_URL)
def load_customers_coupon_list(request):
    return render(request, 'customers/customer_coupon_list.html')


# Load list customers page
# @login_required(login_url=const.CMS_LOGIN_URL)
# def load_customer_gift_page(request):
#     season_list = Seasons.objects.all()
#     content = {
#         'season_list': season_list
#     }
#     return render(request, 'customers/customer_gift.html', content)


# get customers list by json
@login_required(login_url=const.CMS_LOGIN_URL)
def get_list_customers_as_json(request):
    # add curr_res_id params if you need
    _res_id = request.session[const.SESSION_CMS_CURR_RES_ID]
    mapping = {
        'curr_res_id': _res_id,
        'search': ['name', 'area__name', 'number', 'restaurant__name', 'phone', 'email', 'birthday', 'current_point'],
        'sort': {
            0: 'id',
            1: 'area',
            2: 'number',
            3: 'name',
            4: 'phone',
            5: 'email',
            6: 'birthday',
            7: 'sex',
            8: 'current_point',
        },
        # Map foreign key
        'map': ['area_id', 'area_name', 'restaurant_id', 'restaurant_name']
    }
    return cms.get_list_display(request, Customers, mapping)


# get customer prize list by json
@login_required(login_url=const.CMS_LOGIN_URL)
def get_list_customer_prize_as_json(request):
    # add curr_res_id params if you need
    # _res_id = request.session[const.SESSION_CMS_CURR_RES_ID]
    mapping = {
        # 'curr_res_id': _res_id,
        'search': ['name', 'tracking_no', 'customer__number', 'phone', 'email', ],
        'sort': {
            0: 'id',
            1: 'tracking_no',
            2: 'name',
            3: 'phone',
            4: 'email',
            5: 'prize',
            6: 'use_date',
            7: 'status',
        },
        # Map foreign key
        'map': ['customer_number', 'prize_title']
    }
    return cms.get_list_display(request, CustomerPrizes, mapping)


# get customer prize list by json
@login_required(login_url=const.CMS_LOGIN_URL)
def get_list_customer_coupon_as_json(request):
    # add curr_res_id params if you need
    # _res_id = request.session[const.SESSION_CMS_CURR_RES_ID]
    mapping = {
        # 'curr_res_id': _res_id,
        'search': [
            'customer__name',
            'customer__number',
            'customer__phone',
            'customer__email',
            'coupon__number',
            'coupon__name',
        ],
        'sort': {
            0: 'id',
            1: 'customer_name',
            2: 'status',
        },
        # Map foreign key
        'map': [
            'customer_id',
            'customer_number',
            'customer_name',
            'customer_phone',
            'customer_email',
            'coupon_id',
            'coupon_number',
            'coupon_name'
        ]
    }
    return cms.get_list_display(request, CustomerCoupons, mapping)


# get customer_gift list
@login_required(login_url=const.CMS_LOGIN_URL)
def load_customer_gift_page(request):
    context = {}
    if request.method == 'POST':
        form_info = CustomerGiftForm(request.POST)
        if form_info.is_valid():
            season = form_info.cleaned_data['season']
            res = cmn.prc_get_data_without_path_by_store(request, Topics, 'get_gifts', season.id)
            response_data = json.loads(res.content.decode('utf-8'))  # convert res back to dict
            # Create the HttpResponse object with the appropriate CSV header.
            now = datetime.datetime.now()
            file_name = 'customer_gift_list_' + season.name.lower() + '_' + now.strftime('%Y%m%d%H%M%S') + '.csv'
            response = HttpResponse(content_type='text/csv')
            response['Content-Disposition'] = 'attachment; filename="' + file_name + '"'
            try:
                key_format = [
                    'membership_no',
                    'customer_type',
                    'level_up_date',
                    'customer_name',
                    'email',
                    'phone',
                    'address',
                    'area_name',
                    'life_point',
                    'current_point',
                ]
                columns_name = [
                    'Membership No',
                    'Customer Type',
                    'Level Up Date',
                    'Customer Name',
                    'Email',
                    'Phone',
                    'Address',
                    'Area Name',
                    'Life Point',
                    'Current Point',
                ]
                user_data = response_data['data']
                writer = csv.writer(response)
                writer.writerow(columns_name)

                try:
                    if isinstance(user_data, list):
                        user_data[0].pop('id', None)
                        # columns_name = user_data[0].keys()
                        for user in user_data:
                            user.pop('id', None)
                            ordered_data = OrderedDict((k, user[k]) for k in key_format)
                            temp = []
                            for index, value in ordered_data.items():
                                temp.append(value)
                            writer.writerow(temp)
                    else:
                        user_data.pop('id', None)
                        ordered_data = OrderedDict((k, user_data[k]) for k in key_format)
                        temp = []
                        for index, value in ordered_data.items():
                            temp.append(value)
                        writer.writerow(temp)
                    return response
                except Exception as e:
                    print(e)

                    messages.add_message(
                        request, messages.ERROR,
                        'There is some problem with genering csv file. Please try again.'
                    )
                    form_info = CustomerGiftForm()
                    context['form_info'] = form_info
                    return render(request, 'customers/customer_gift.html', context)
            except Exception as e:
                print(traceback.format_exc())
                print(e)
                messages.add_message(request, messages.WARNING, 'There is no customer have gifts in ' + season.name)
                form_info = CustomerGiftForm()
                context['form_info'] = form_info
                return render(request, 'customers/customer_gift.html', context)
    else:
        form_info = CustomerGiftForm()
        context['form_info'] = form_info
        return render(request, 'customers/customer_gift.html', context)


# get customer_gift list
@login_required(login_url=const.CMS_LOGIN_URL)
def pos_integration(request):
    context = {}
    if request.method == 'POST':
        try:
            file_import = request.FILES.get('file_import', '')
            file_type = file_import.content_type
            if file_type == 'application/vnd.ms-excel' \
                    or file_type == 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet':
                excel_data = read_from_excel_file(file_import.read())
                if excel_data:
                    res = cmn.prc_get_data_without_path_by_store(request, Topics, 'import_pos', excel_data)
                    response_data = json.loads(res.content.decode('utf-8'))
                    print(response_data)
                    try:
                        not_exist_user_list = response_data['data']
                        if not_exist_user_list:
                            message_list = ''
                            if isinstance(not_exist_user_list, list):
                                for user in not_exist_user_list:
                                    message_list += '</br>' + user['content']
                            else:
                                message_list = '</br>' + not_exist_user_list['content']
                            messages.add_message(request, messages.WARNING, message_list)
                        else:
                            messages.add_message(request, messages.SUCCESS, '</br> Import report successfully')
                    except Exception as e:
                        print(e)  # all data is processed
                        messages.add_message(request, messages.SUCCESS, '</br> Import report successfully')
                else:
                    messages.add_message(request, messages.WARNING,
                                         'Please input template headers: "Customer ID, Customer Name, Membership ID, Gender, Age, E-Mail, Tel., ByDay, ByBill, Product ID, Product Name, Combo Code, Quantity, Sales $"')
            else:
                messages.add_message(request, messages.ERROR,
                                     'Report must be only excel file. Please upload a correct file type.')
        except Exception as e:
            print(e)
            messages.add_message(request, messages.ERROR, 'There is something wrong with getting import file')
        return render(request, 'customers/pos_integration.html', context)
    else:
        return render(request, 'customers/pos_integration.html', context)


# customer add page
@login_required(login_url=const.CMS_LOGIN_URL)
def add_customers(request):
    context = {}
    model = Customers
    if request.method == 'POST':
        try:
            form_info = CustomersInfoForm(request.POST)
            if form_info.is_valid():
                # check duplicate
                check_res = cms.chk_dup_ins(Customers, form_info, ['email', 'phone'])
                if check_res:
                    messages.error(request, check_res['errors'])
                    context['customers'] = model
                    context['form_info'] = form_info
                    context['request_method'] = request.method
                    return render(request, 'customers/add.html', context)

                # create django user belong to Customer info
                curr_user = User.objects.create_user(username=form_info.cleaned_data["email"],
                                                     email=form_info.cleaned_data["email"],
                                                     password=form_info.cleaned_data["login_password"])

                customer = form_info.save(commit=False)

                # Manual field
                customer.user_id = curr_user.id
                customer.restaurant_id = request.session['cms_auth_sign_in_curr_res_id']
                customer.save()

                # Get & Set image_path
                image_file = request.FILES.get('image_path', False)
                if image_file:
                    customer.image_path.delete(False)
                    customer.image_path.save(image_file.name, image_file)
                else:
                    customer.image_path = ""
                    customer.save()
                return HttpResponseRedirect('/cms/customers/list/')
            else:
                print("error" + str(form_info.errors))
                messages.add_message(request, messages.ERROR, form_info.errors, extra_tags='add_customer')
        except Exception as e:
            print("error" + str(e))
            messages.add_message(request, messages.ERROR, e, extra_tags='add_customer')
        return render(request, 'error.html')

    else:
        context = {}
        form_info = CustomersInfoForm()
        # Set return context
        context['form_info'] = form_info
        context['request_method'] = request.method

        return render(request, 'customers/add.html', context)


# customer Edit page
@login_required(login_url=const.CMS_LOGIN_URL)
@transaction.atomic
def edit_customers(request, cus_id):
    try:
        if util.is_num(cus_id):
            # Get customer by id
            customer = Customers.objects.get(pk=cus_id)
            context = {}
            old_current_point = customer.current_point
            if request.method == 'POST':

                form_info = CustomersInfoForm(request.POST, instance=customer)

                if form_info.is_valid():
                    # check duplicate table user
                    check_user_res = cms.chk_dup_upd(User, form_info, customer.user_id, 'email')
                    if check_user_res:
                        messages.error(request, check_user_res['errors'])
                        context['customers'] = customer
                        context['form_info'] = form_info
                        context['request_method'] = request.method
                        return render(request, 'customers/edit.html', context)

                    # check duplicate table customer
                    check_cus_res = cms.chk_dup_upd(Customers, form_info, customer.id, ['email', 'phone'])
                    if check_cus_res:
                        messages.error(request, check_cus_res['errors'])
                        context['customers'] = customer
                        context['form_info'] = form_info
                        context['request_method'] = request.method
                        return render(request, 'customers/edit.html', context)

                    # update django user belong to Customer info

                    dj_user = User.objects.get(pk=customer.user_id)
                    if str(dj_user.username) != str(form_info.cleaned_data["email"]):
                        dj_user.username = form_info.cleaned_data["email"]
                        dj_user.email = form_info.cleaned_data["email"]
                        dj_user.save()

                    customer = form_info.save(commit=False)

                    # Manual
                    if customer.birthday:
                        customer.age = datetime.datetime.now().year - customer.birthday.year
                    else:
                        customer.age = None
                    customer.user_id = dj_user.id
                    customer.restaurant_id = request.session[const.SESSION_CMS_CURR_RES_ID]
                    new_current_point = customer.current_point
                    if new_current_point > old_current_point:
                        customer.total_point += (new_current_point - old_current_point)

                    # Get & Set image_path
                    image_file = request.FILES.get('image_path', False)
                    if image_file:
                        customer.image_path.delete(False)
                        customer.image_path.save(image_file.name, image_file)
                    if request.POST.get('isDelete'):
                        customer.is_delete = const.ACTIVE_FLAG
                    else:
                        customer.is_delete = const.DEL_FLAG

                    customer.save()

                    return HttpResponseRedirect('/cms/customers/list/')
                else:
                    print("error" + str(form_info.errors))
                    messages.add_message(request, messages.ERROR, form_info.errors, extra_tags='edit_customer')
                return render(request, 'error.html')
            else:
                if customer.restaurant_id == request.session[const.SESSION_CMS_CURR_RES_ID]:
                    context = {}
                    form_info = CustomersInfoForm(instance=customer)
                    # Set return context
                    context['customers'] = customer
                    context['form_info'] = form_info
                    context['request_method'] = request.method
                    return render(request, 'customers/edit.html', context)
                else:
                    messages.add_message(request, messages.ERROR, 'This Customer not belong to this Restaurants',
                                         extra_tags='edit_customer')

        else:
            messages.add_message(request, messages.ERROR, 'Customer Id incorrect', extra_tags='edit_customer')
    except Exception as e:
        print("error" + str(e))
        messages.add_message(request, messages.ERROR, e, extra_tags='edit_customer')
    return render(request, 'error.html')


# customer prize edit page
@login_required(login_url=const.CMS_LOGIN_URL)
@transaction.atomic
def edit_customers_prize(request, cus_prize_id):
    try:
        if util.is_num(cus_prize_id):
            # Get customer by id
            customer_prize = CustomerPrizes.objects.get(pk=cus_prize_id)
            current_tracking_no = customer_prize.tracking_no
            current_address = customer_prize.address
            current_delivery_date = customer_prize.delivery_date
            context = {}

            if request.method == 'POST':

                form_info = CustomerPrizeForm(request.POST, instance=customer_prize)

                if form_info.is_valid():
                    # check tracking no.
                    tracking_no = form_info.cleaned_data['tracking_no']
                    address = form_info.cleaned_data['address']
                    delivery_date = form_info.cleaned_data['delivery_date']
                    if (tracking_no and tracking_no != str(current_tracking_no)) or (
                                address != current_address) or (delivery_date != current_delivery_date):
                        prize = Prizes.objects.get(pk=customer_prize.prize.id, is_delete=const.ACTIVE_FLAG)
                        _lang = customer_prize.language

                        # config mail
                        _string_tracking_no = '#' + str(tracking_no)
                        subject = 'ro ann style club: Order Confirmation for Prize' + _string_tracking_no if _lang == 'en' else '割烹 櫓杏:訂單確認獎' + _string_tracking_no
                        from_email = const.DEFAULT_FROM_EMAIL
                        to_email = [form_info.cleaned_data['email']]

                        # prepare data for mail
                        # tracking_no = tracking_no_rd
                        prize_title = prize.title if _lang == 'en' else prize.title_cn
                        delivery_type = 'Delivery' if customer_prize.delivery_type == 1 else 'Get direction'
                        # res_id = prize.restaurant
                        res_obj = prize.restaurant
                        shop_info_name = res_obj.name if _lang == 'en' else res_obj.name_cn
                        shop_info_email = res_obj.email
                        shop_info_phone = res_obj.phone
                        shop_info_address = res_obj.address if _lang == 'en' else res_obj.address_cn

                        plaintext = get_template('email/delivery_prize_with_tracking_no_en.txt' if _lang == 'en'
                                                 else 'email/delivery_prize_with_tracking_no_cn.txt')
                        html_form = get_template('email/delivery_prize_with_tracking_no_en.html' if _lang == 'en'
                                                 else 'email/delivery_prize_with_tracking_no_cn.html')

                        ctx = Context({
                            'cus_name': form_info.cleaned_data['name'],
                            'cus_phone': form_info.cleaned_data['phone'],
                            'cus_email': form_info.cleaned_data['email'],
                            'cus_address': form_info.cleaned_data['address'],
                            'tracking_no': tracking_no,
                            'title': prize_title,
                            'request_point': prize.request_point,
                            'link_address': prize.link_address,
                            'delivery_type': delivery_type,
                            'shop_info_name': shop_info_name,
                            'shop_info_email': shop_info_email,
                            'shop_info_phone': shop_info_phone,
                            'shop_info_address': shop_info_address,
                        })

                        text_content = plaintext.render(ctx)
                        html_content = html_form.render(ctx)
                        msg = EmailMultiAlternatives(subject, text_content, from_email=from_email, to=to_email)
                        msg.attach_alternative(html_content, "text/html")
                        msg.send()

                        instance = form_info.save(commit=False)
                        instance.status = 2
                        instance.save()
                    else:
                        instance = form_info.save(commit=False)
                        if not tracking_no:
                            instance.tracking_no = None
                            instance.status = 1
                        instance.save()

                    return HttpResponseRedirect('/cms/customers/customers_prize/list/')
                else:
                    print("error" + str(form_info.errors))
                    messages.add_message(request, messages.ERROR, form_info.errors, extra_tags='edit_customer')
                return render(request, 'error.html')
            else:
                context = {}
                form_info = CustomerPrizeForm(instance=customer_prize)
                # Set return context
                context['customers'] = customer_prize
                context['form_info'] = form_info
                context['request_method'] = request.method
                return render(request, 'customers/customer_prize_edit.html', context)

        else:
            messages.add_message(request, messages.ERROR, 'Customer Id incorrect', extra_tags='edit_customer')
    except Exception as e:
        print("error" + str(e))
        messages.add_message(request, messages.ERROR, e, extra_tags='edit_customer')
    return render(request, 'error.html')


# customer coupon edit page
@login_required(login_url=const.CMS_LOGIN_URL)
@transaction.atomic
def edit_customers_coupon(request, cus_coupon_id):
    try:
        if util.is_num(cus_coupon_id):
            customer_coupon = CustomerCoupons.objects.get(pk=cus_coupon_id)
            if customer_coupon.status == 1:
                customer_coupon.status = 2
            else:
                customer_coupon.status = 1
            customer_coupon.save()
            return HttpResponseRedirect('/cms/customers/customers_coupon/list/')
    except Exception as e:
        print("error" + str(e))
        messages.add_message(request, messages.ERROR, e, extra_tags='edit_customer')
    return render(request, 'error.html')


# Delete customer
@login_required(login_url=const.CMS_LOGIN_URL)
def delete_customer(request, cus_id):
    return cms.action_insert_or_update(request, Customers, CustomersInfoForm, 'delete_customer', cus_id)
