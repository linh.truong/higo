"""toyosu URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.10/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url
from django.conf.urls.static import static

from higo import settings_dev as settings

import api.views_coupon as svc_coupon
import api.views_customer as svc_customer
import api.views_food as svc_food
import api.views_location as svc_location
import api.views_restaurant as svc_restaurant
import api.views_topic as svc_topic
import api.views_setting as svc_setting
import api.views_reservation as svc_reserve
import api.views_point as svc_point
import api.views_prize as svc_prize

urlpatterns = [
                  # --------------------------------------------------------
                  # ======================= SETTINGS =======================
                  # --------------------------------------------------------
                  url(r'^api/setting/getlist$', svc_setting.get_settings_list),
                  url(r'^api/setting/get_by_code$', svc_setting.get_setting_by_code),

                  # -------------------------------------------------------
                  # ======================= COUPONS =======================
                  # -------------------------------------------------------
                  # Coupon Details
                  url(r'^api/coupon_dtl/getlist$', svc_coupon.get_coupon_details_list),
                  url(r'^api/coupon_dtl/getinfo$', svc_coupon.get_coupon_detail_by_id),
                  url(r'^api/coupon_dtl/get_by_condition$', svc_coupon.get_coupon_detail_by_condition),
                  url(r'^api/coupon_dtl/insert$', svc_coupon.insert_coupon_detail),
                  url(r'^api/coupon_dtl/update$', svc_coupon.update_coupon_detail),
                  url(r'^api/coupon_dtl/delete$', svc_coupon.delete_coupon_detail),

                  # Coupons
                  url(r'^api/coupon/getlist$', svc_coupon.get_coupons_list),
                  url(r'^api/coupon/getlist_ctm$', svc_coupon.get_coupon_list_ctm),
                  url(r'^api/coupon/getinfo$', svc_coupon.get_coupon_by_id),
                  url(r'^api/coupon/get_by_condition$', svc_coupon.get_coupon_by_condition),
                  url(r'^api/coupon/insert$', svc_coupon.insert_coupon),
                  url(r'^api/coupon/update$', svc_coupon.update_coupon),
                  url(r'^api/coupon/delete$', svc_coupon.delete_coupon),
                  url(r'^api/coupon/getlist_by_cus_id', svc_coupon.get_coupon_list_with_cus_id),

                  # ---------------------------------------------------------
                  # ======================= CUSTOMERS =======================
                  # ---------------------------------------------------------
                  # Customer Coupons
                  url(r'^api/cus_cpn/getlist$', svc_customer.get_customer_coupons_list),
                  url(r'^api/cus_cpn/getinfo$', svc_customer.get_customer_coupon_by_id),
                  url(r'^api/cus_cpn/get_by_condition$', svc_customer.get_customer_coupon_by_condition),
                  url(r'^api/cus_cpn/insert$', svc_customer.insert_customer_coupon),
                  url(r'^api/cus_cpn/update$', svc_customer.update_customer_coupon),
                  url(r'^api/cus_cpn/delete$', svc_customer.delete_customer_coupon),

                  # Customers
                  url(r'^api/cus/getlist$', svc_customer.get_customers_list),
                  url(r'^api/cus/getinfo$', svc_customer.get_customer_by_id),
                  url(r'^api/cus/get_by_condition$', svc_customer.get_customer_by_condition),
                  url(r'^api/cus/insert$', svc_customer.insert_customer),
                  url(r'^api/cus/update$', svc_customer.update_customer),
                  url(r'^api/cus/delete$', svc_customer.delete_customer),

                  # Devices
                  url(r'^api/device/get_notify$', svc_customer.get_push_notify),
                  url(r'^api/device/set_notify$', svc_customer.set_push_notify),

                  # -----------------------------------------------------
                  # ======================= FOODS     =======================
                  # -----------------------------------------------------
                  # Food category
                  url(r'^api/food_cat/getlist$', svc_food.get_food_category_list),
                  url(r'^api/food_cat/getinfo$', svc_food.get_food_by_id),
                  url(r'^api/food_cat/get_by_condition$', svc_food.get_food_cat_by_condition),
                  url(r'^api/food_cat/insert$', svc_food.insert_food),
                  url(r'^api/food_cat/update$', svc_food.update_food),
                  url(r'^api/food_cat/delete$', svc_food.delete_food),

                  # Food Type
                  url(r'^api/food_type/getlist$', svc_food.get_food_types_list),
                  url(r'^api/food_type/getinfo$', svc_food.get_food_type_by_id),
                  url(r'^api/food_type/get_by_condition$', svc_food.get_food_type_by_condition),
                  url(r'^api/food_type/insert$', svc_food.insert_food_type),
                  url(r'^api/food_type/update$', svc_food.update_food_type),
                  url(r'^api/food_type/delete$', svc_food.delete_food_type),

                  # Food Price
                  url(r'^api/food_price/getlist$', svc_food.get_food_prices_list),
                  url(r'^api/food_price/getinfo$', svc_food.get_food_price_by_id),
                  url(r'^api/food_price/get_by_condition$', svc_food.get_food_price_by_condition),
                  url(r'^api/food_price/insert$', svc_food.insert_food_price),
                  url(r'^api/food_price/update$', svc_food.update_food_price),
                  url(r'^api/food_price/delete$', svc_food.delete_food_price),

                  # Food
                  url(r'^api/food/getlist$', svc_food.get_foods_list),
                  url(r'^api/food/getinfo$', svc_food.get_food_by_id),
                  url(r'^api/food/get_by_condition$', svc_food.get_food_by_condition),
                  url(r'^api/food/getlist_by_cat$', svc_food.get_foods_by_category),
                  url(r'^api/food/getlist_by_multi_cat$', svc_food.get_foods_by_multi_categories),
                  url(r'^api/food/insert$', svc_food.insert_food),
                  url(r'^api/food/update$', svc_food.update_food),
                  url(r'^api/food/delete$', svc_food.delete_food),

                  # -----------------------------------------------------
                  # ======================= ITEMS =======================
                  # -----------------------------------------------------


                  # ---------------------------------------------------------
                  # ======================= LOCATIONS =======================
                  # ---------------------------------------------------------
                  # Areas
                  url(r'^api/area/getlist$', svc_location.get_areas_list),
                  url(r'^api/area/getinfo$', svc_location.get_area_by_id),
                  url(r'^api/area/get_by_condition$', svc_location.get_area_by_condition),

                  # Stations
                  url(r'^api/station/getlist$', svc_location.get_stations_list),
                  url(r'^api/station/getinfo$', svc_location.get_station_by_id),
                  url(r'^api/station/get_by_condition$', svc_location.get_station_by_condition),

                  # ------------------------------------------------------
                  # ======================= POINTS =======================
                  # ------------------------------------------------------
                  # Customer Points
                  url(r'^api/cus_point/getlist$', svc_point.get_customer_points_list),
                  url(r'^api/cus_point/getinfo$', svc_point.get_customer_point_by_id),
                  url(r'^api/cus_point/insert$', svc_point.insert_customer_point),
                  url(r'^api/cus_point/update$', svc_point.update_customer_point),
                  url(r'^api/cus_point/delete$', svc_point.delete_customer_point),
                  url(r'^api/cus_point/use_qr_code$', svc_point.use_point_by_qr_code),

                  # Restaurant Points
                  url(r'^api/res_point/getlist$', svc_point.get_restaurant_points_list),
                  url(r'^api/res_point/getinfo$', svc_point.get_restaurant_point_by_id),
                  url(r'^api/res_point/insert$', svc_point.insert_restaurant_point),
                  url(r'^api/res_point/update$', svc_point.update_restaurant_point),
                  url(r'^api/res_point/delete$', svc_point.delete_restaurant_point),

                  # ------------------------------------------------------
                  # ======================= PRIZES =======================
                  # ------------------------------------------------------
                  # Customer Prizes
                  url(r'^api/cus_prize/getlist$', svc_prize.get_customer_prizes_list),
                  url(r'^api/cus_prize/getinfo$', svc_prize.get_customer_prize_by_id),
                  url(r'^api/cus_prize/insert$', svc_prize.insert_customer_prize),
                  url(r'^api/cus_prize/update$', svc_prize.update_customer_prize),
                  url(r'^api/cus_prize/use_prize$', svc_prize.cus_choose_prize),
                  url(r'^api/cus_prize/delete$', svc_prize.delete_customer_prize),

                  # Prizes
                  url(r'^api/prize/getlist$', svc_prize.get_prizes_list),
                  url(r'^api/prize/getinfo$', svc_prize.get_prize_by_id),
                  url(r'^api/prize/insert$', svc_prize.insert_prize),
                  url(r'^api/prize/update$', svc_prize.update_prize),
                  url(r'^api/prize/delete$', svc_prize.delete_prize),

                  # # ------------------------------------------------------------
                  # # ======================= RESERVATIONS =======================
                  # # ------------------------------------------------------------
                  # url(r'^api/reserve/getlist$', svc_reserve.get_reservation_list),
                  # url(r'^api/reserve/getinfo$', svc_reserve.get_reservation_by_id),
                  # url(r'^api/reserve/insert$', svc_reserve.insert_reservation),
                  # url(r'^api/reserve/update$', svc_reserve.update_reservation),
                  # url(r'^api/reserve/delete$', svc_reserve.delete_reservation),

                  # -----------------------------------------------------------
                  # ======================= RESTAURANTS =======================
                  # -----------------------------------------------------------
                  # Restaurants
                  url(r'^api/res/getlist$', svc_restaurant.get_restaurants_list),
                  url(r'^api/res/getinfo$', svc_restaurant.get_restaurant_by_id),
                  url(r'^api/res/get_by_condition$', svc_restaurant.get_restaurant_by_condition),
                  url(r'^api/res/insert$', svc_restaurant.insert_restaurant),
                  url(r'^api/res/update$', svc_restaurant.update_restaurant),
                  url(r'^api/res/delete$', svc_restaurant.delete_restaurant),

                  # Company info
                  url(r'^api/comp/getinfo$', svc_restaurant.get_comp_info),

                  # Contacts
                  url(r'^api/contact/getlist$', svc_restaurant.get_contacts_list),
                  url(r'^api/contact/getinfo$', svc_restaurant.get_contact_by_id),
                  url(r'^api/contact/insert$', svc_restaurant.insert_contact),
                  url(r'^api/contact/update$', svc_restaurant.update_contact),
                  url(r'^api/contact/delete$', svc_restaurant.delete_contact),

                  # ------------------------------------------------------
                  # ======================= TOPICS =======================
                  # ------------------------------------------------------
                  # Topic Categories
                  url(r'^api/topic_cat/getlist$', svc_topic.get_topic_categories_list),
                  url(r'^api/topic_cat/getinfo$', svc_topic.get_topic_category_by_id),
                  url(r'^api/topic_cat/get_by_condition$', svc_topic.get_topic_category_by_condition),
                  url(r'^api/topic_cat/insert$', svc_topic.insert_topic_category),
                  url(r'^api/topic_cat/update$', svc_topic.update_topic_category),
                  url(r'^api/topic_cat/delete$', svc_topic.delete_topic_category),

                  # Topics
                  url(r'^api/topic/getlist$', svc_topic.get_topics_list),
                  url(r'^api/topic/getinfo$', svc_topic.get_topic_by_id),
                  url(r'^api/topic/get_by_condition$', svc_topic.get_topic_by_condition),
                  url(r'^api/topic/getlist_by_cat$', svc_topic.get_topics_by_category),
                  url(r'^api/topic/getlist_by_multi_cat$', svc_topic.get_topics_by_multi_categories),
                  url(r'^api/topic/insert$', svc_topic.insert_topic),
                  url(r'^api/topic/update$', svc_topic.update_topic),
                  url(r'^api/topic/delete$', svc_topic.delete_topic),

                  # -------------------------------------------------------
                  # ======================= MEMBERSHIPS =======================
                  # -------------------------------------------------------
                  url(r'^api/cus/membership$', svc_customer.get_membership),

                  # -------------------------------------------------------
                  # ======================= MEMBERSHIP STATUS =======================
                  # -------------------------------------------------------
                  url(r'^api/mem_status/getlist', svc_customer.get_membership_status_list),

                  # -------------------------------------------------------
                  # ======================= SIMPLYBOOK =======================
                  # -------------------------------------------------------
                  url(r'^api/simplybook/get_book_info$', svc_reserve.get_book_info),
                  url(r'^api/simplybook/get_reserve_time$', svc_reserve.get_reserve_time),
                  url(r'^api/simplybook/booking$', svc_reserve.booking),
                  url(r'^api/simplybook/booking_new$', svc_reserve.booking_new),

                  # -------------------------------------------------------
                  # ======================= EC Site =======================
                  # -------------------------------------------------------
                  url(r'^api/cus_point/ec_site_point$', svc_point.ec_site_add_point),
                  url(r'^api/cus_point/ec_site_minus_point$', svc_point.ec_site_minus_point),

                  # -------------------------------------------------------
                  # ======================= NEWS INSTAGRAM =======================
                  # -------------------------------------------------------
                  url(r'^api/instagram/getdata$', svc_topic.get_instagram_data),

              ] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
