#!/usr/bin/env python
# -*- coding: utf-8 -*-

from django.views.decorators.csrf import csrf_exempt
from commonmodels.models import Settings
import commonmodels.views as cmn


# ---------------------------------------------------------
# ======================= SETTING =========================
# ---------------------------------------------------------
# Get Setting by code
@csrf_exempt
def get_setting_by_code(request):
    return cmn.db_get_by_cond(request, Settings, {'code': cmn.get_req_param(request, 'code')})


# Get list all Setting
@csrf_exempt
def get_settings_list(request):
    return cmn.db_get_all(request, Settings)