#!/usr/bin/env python
# -*- coding: utf-8 -*-
from django.http import JsonResponse
from django.views.decorators.csrf import csrf_exempt

from utils.utility import is_authorize, get_curr_date
from commonmodels.models import Reservations, Restaurants
import commonmodels.views as cmn
from jsonrpcclient.http_client import HTTPClient
from utils import constants as const
from utils.errcode import ErrCode as eCODE
from datetime import datetime, timedelta
import json


# -----------------------------------------------------------
# ======================= RESERVATION =======================
# -----------------------------------------------------------
# Get list all Reservations
@csrf_exempt
def get_reservation_list(request):
    return cmn.db_get_all(request, Reservations)


# Get Reservation by id
@csrf_exempt
def get_reservation_by_id(request):
    return cmn.db_get_by_id(request, Reservations)


# Insert Reservation
@csrf_exempt
def insert_reservation(request):
    res = cmn.db_insert(request, Reservations)
    cmn.send_mail_reserve(request)
    cmn.send_mail_reserve_operator(request)
    return res


# Update Reservation
@csrf_exempt
def update_reservation(request):
    return cmn.db_update(request, Reservations)


# Delete Reservation
# params: categoryId
@csrf_exempt
@is_authorize
def delete_reservation(request):
    return cmn.db_delete(request, Reservations)


# Get booking info
@csrf_exempt
def get_book_info(request):
    res = {"error": 0, "message": "", "event": [], "unit": [], "working_date": [], "additional": {}}
    try:
        # get params
        _res_id = cmn.get_req_param(request, 'resId')

        # get restaurant information
        restaurant = Restaurants.objects.filter(pk=_res_id, is_delete=const.ACTIVE_FLAG).first()
        _sb_company_login = restaurant.sb_company_login
        _sb_api_key = restaurant.sb_api_key

        # get param working date
        _working_from_date = get_curr_date()
        _working_from_str = _working_from_date.strftime('%Y-%m-%d')
        _working_duration = restaurant.limit_days_book
        _working_to_date = datetime.today() + timedelta(days=_working_duration)
        _working_to_str = _working_to_date.strftime("%Y-%m-%d")

        # token and header for normal use
        token = HTTPClient(const.BOOKING_URL + '/login').request('getToken', _sb_company_login, _sb_api_key)
        headers = {
            'Content-Type': 'application/json',
            'Accept': 'application/json',
            'X-Company-Login': _sb_company_login,
            'X-Token': token,
        }

        client = HTTPClient(const.BOOKING_URL)
        client.session.headers.update(headers)
        event_list = client.request('getEventList')
        event_list_sorted = sorted(event_list.items(), key=lambda x: int(x[1]['position']), reverse=False)
        unit_list = client.request('getUnitList')
        unit_list_sorted = sorted(unit_list.items(), key=lambda x: int(x[1]['position']), reverse=False)
        additional_list = client.request('getAdditionalFields', '1')
        # list of event
        for key, event_detail in event_list_sorted:
            res["event"].append(event_detail)
        # list of unit
        for key, unit_detail in unit_list_sorted:
            res["unit"].append(unit_detail)
        # list of additional
        res["additional"] = additional_list

        working_days_list = client.request('getWorkDaysInfo', _working_from_str, _working_to_str)
        for i, record in enumerate(working_days_list):
            if list(working_days_list.values())[i]:
                day = {
                    'date': list(working_days_list.keys())[i],
                    'time': list(working_days_list.values())[i]
                }
            res["working_date"].append(day)

        return JsonResponse(res)
    except Exception as e:
        res["error"] = eCODE.RES_FAIL
        res["message"] = str(e)
        print(e)
    return JsonResponse(res)


@csrf_exempt
def get_reserve_time(request):
    res = {"error": 0, "message": "", "data": {}}
    try:
        # get params
        _res_id = cmn.get_req_param(request, 'resId')
        _reserve_date = cmn.get_req_param(request, 'reserveDate')
        _unit_id = cmn.get_req_param(request, 'unitId')
        _event_id = cmn.get_req_param(request, 'eventId')
        restaurant = Restaurants.objects.filter(pk=_res_id, is_delete=const.ACTIVE_FLAG).first()
        _sb_company_login = restaurant.sb_company_login
        _sb_api_key = restaurant.sb_api_key

        # token and header for normal use
        token = HTTPClient(const.BOOKING_URL + '/login').request('getToken', _sb_company_login, _sb_api_key)
        headers = {
            'Content-Type': 'application/json',
            'Accept': 'application/json',
            'X-Company-Login': _sb_company_login,
            'X-Token': token,
        }

        client = HTTPClient(const.BOOKING_URL)
        client.session.headers.update(headers)
        time_list = client.request('getWorkDaysInfo', _reserve_date, _reserve_date, _unit_id, _event_id, 1)
        for time_order in time_list:
            result = time_list.get(time_order)
            res["data"] = result
        return JsonResponse(res)
    except Exception as e:
        res["error"] = eCODE.RES_FAIL
        res["message"] = str(e)

    return JsonResponse(res)


@csrf_exempt
def recur_booking(client, event_id, unit_id, date, time, client_data, additional_field, number_of_participants):
    try:
        response_data = client.request('book', event_id, unit_id, date, time, client_data, additional_field,
                                       number_of_participants, )
    except Exception as e:
        _res_content = client.last_response.content.decode("utf-8")
        _json_res = json.loads(_res_content)
        response_data = _json_res['error']['code']

    return response_data


@csrf_exempt
def booking(request):
    res = {"error": 0, "message": "", "data": {}}
    try:
        # get params
        _res_id = cmn.get_req_param(request, 'resId')
        restaurant = Restaurants.objects.filter(pk=_res_id, is_delete=const.ACTIVE_FLAG).first()
        _sb_company_login = restaurant.sb_company_login
        _sb_api_key = restaurant.sb_api_key
        # token and header for normal use
        token = HTTPClient(const.BOOKING_URL + '/login').request('getToken', _sb_company_login, _sb_api_key)
        headers = {
            'Content-Type': 'application/json',
            'Accept': 'application/json',
            'X-Company-Login': _sb_company_login,
            'X-Token': token,
        }

        client = HTTPClient(const.BOOKING_URL)
        client.session.headers.update(headers)
        event_id = cmn.get_req_param(request, 'eventId')
        unit_id = cmn.get_req_param(request, 'unitId')
        date = cmn.get_req_param(request, 'date')
        time = cmn.get_req_param(request, 'time')
        client_data = {
            'name': cmn.get_req_param(request, 'cusName'),
            'email': cmn.get_req_param(request, 'cusEmail'),
            'phone': cmn.get_req_param(request, 'cusPhone'),
        }
        additional_field = {}
        additional_field_list = cmn.get_req_param(request, 'additional')
        for field_detail in additional_field_list:
            additional_name = field_detail['name']
            additional_value = field_detail['values']
            additional_field[additional_name] = additional_value

        number_of_participants = cmn.get_req_param(request, 'count')
        _number_of_seat = cmn.get_req_param(request, 'numberOfSeat')
        book_info = client.request('book', event_id, unit_id, date, time, client_data, additional_field,
                                   number_of_participants, )
        res["data"] = book_info['bookings'][0]
        res["error"] = eCODE.RES_SUCCESS
        # save data to reservation table
        reservation = Reservations()
        booking_data = book_info['bookings'][0]
        reservation.customer_id = cmn.get_req_param(request, 'customerId')
        reservation.booking_id = booking_data['id']
        reservation.client_id = booking_data['client_id']
        reservation.unit_id = booking_data['unit_id']
        reservation.event_id = booking_data['event_id']
        reservation.status_id = 1 if res["error"] == eCODE.RES_SUCCESS else 0
        _start_date_time = booking_data['start_date_time']
        reservation.start_date_time = cmn.convert_to_local_time(_start_date_time)
        _end_date_time = booking_data['end_date_time']
        reservation.end_date_time = cmn.convert_to_local_time(_end_date_time)
        reservation.number_of_seat = _number_of_seat if _number_of_seat else 1
        reservation.hash = booking_data['hash']
        reservation.code = booking_data['code']
        reservation.is_confirmed = booking_data['is_confirmed']
        # save to database
        reservation.save()
    except Exception as e:
        res["error"] = eCODE.RES_FAIL
        res["message"] = str(e)

    return JsonResponse(res)


@csrf_exempt
def booking_new(request):
    res = {"error": 0, "message": "", "data": {}}
    try:
        # get params
        _res_id = cmn.get_req_param(request, 'resId')
        restaurant = Restaurants.objects.filter(pk=_res_id, is_delete=const.ACTIVE_FLAG).first()
        _sb_company_login = restaurant.sb_company_login
        _sb_api_key = restaurant.sb_api_key
        # token and header for normal use
        token = HTTPClient(const.BOOKING_URL + '/login').request('getToken', _sb_company_login, _sb_api_key)
        headers = {
            'Content-Type': 'application/json',
            'Accept': 'application/json',
            'X-Company-Login': _sb_company_login,
            'X-Token': token,
        }

        client = HTTPClient(const.BOOKING_URL)
        client.session.headers.update(headers)
        event_id = cmn.get_req_param(request, 'eventId')
        unit_list = cmn.get_req_param(request, 'unitList')
        date = cmn.get_req_param(request, 'date')
        time = cmn.get_req_param(request, 'time')
        client_data = {
            'name': cmn.get_req_param(request, 'cusName'),
            'email': cmn.get_req_param(request, 'cusEmail'),
            'phone': cmn.get_req_param(request, 'cusPhone'),
        }
        additional_field = {}
        additional_field_list = cmn.get_req_param(request, 'additional')
        for field_detail in additional_field_list:
            additional_name = field_detail['name']
            additional_value = field_detail['values']
            additional_field[additional_name] = additional_value

        number_of_participants = cmn.get_req_param(request, 'count')
        _number_of_seat = cmn.get_req_param(request, 'numberOfSeat')

        book_success = False
        for unit in unit_list:
            unit_id = unit['id']
            book_info = recur_booking(client, event_id, unit_id, date, time, client_data, additional_field,
                                      number_of_participants)
            if book_info == eCODE.SIMPLYBOOK_FULL_TABLE:
                res["message"] = 'Selected time start is not available'
            else:
                book_success = True
                break

        if book_success:
            res["data"] = book_info['bookings'][0]
            res["error"] = eCODE.RES_SUCCESS
            res["message"] = 'Booking success'
            # save data to reservation table
            reservation = Reservations()
            booking_data = book_info['bookings'][0]
            reservation.customer_id = cmn.get_req_param(request, 'customerId')
            reservation.booking_id = booking_data['id']
            reservation.client_id = booking_data['client_id']
            reservation.unit_id = booking_data['unit_id']
            reservation.event_id = booking_data['event_id']
            reservation.status_id = 1 if res["error"] == eCODE.RES_SUCCESS else 0
            _start_date_time = booking_data['start_date_time']
            reservation.start_date_time = cmn.convert_to_local_time(_start_date_time)
            _end_date_time = booking_data['end_date_time']
            reservation.end_date_time = cmn.convert_to_local_time(_end_date_time)
            reservation.number_of_seat = _number_of_seat if _number_of_seat else 1
            reservation.hash = booking_data['hash']
            reservation.code = booking_data['code']
            reservation.is_confirmed = booking_data['is_confirmed']
            # save to database
            reservation.save()
        else:
            res["error"] = eCODE.SIMPLYBOOK_FULL_ALL
            res["message"] = 'All table are full in this time. Please pick another time.'
    except Exception as e:
        res["error"] = eCODE.RES_FAIL
        res["message"] = str(e)
        print(e)
    return JsonResponse(res)
