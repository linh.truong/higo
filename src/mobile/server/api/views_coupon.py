#!/usr/bin/env python
# -*- coding: utf-8 -*-
from datetime import timedelta

from django.http import JsonResponse
from django.views.decorators.csrf import csrf_exempt
from utils.utility import is_authorize, is_super_user
from commonmodels.models import Coupons, CouponDetails, Customers, CustomerCoupons
from commonmodels.views import get_req_param
import commonmodels.views as cmn
from utils import constants as const
from utils import utility as util
from utils.errcode import ErrCode as eCODE


# --------------------------------------------------------------
# ======================= COUPON DETAILS =======================
# --------------------------------------------------------------
# Get list all Coupon Details
@csrf_exempt
def get_coupon_details_list(request):
    return cmn.db_get_all(request, CouponDetails)


# Get Coupon Detail by id
@csrf_exempt
def get_coupon_detail_by_id(request):
    return cmn.db_get_by_id(request, CouponDetails)


# Get Coupon Detail by condition
@csrf_exempt
def get_coupon_detail_by_condition(request):
    return cmn.db_get_by_cond(request, CouponDetails)


# Insert Coupon Detail
@csrf_exempt
def insert_coupon_detail(request):
    return cmn.db_insert(request, CouponDetails)


# Update Coupon Detail
@csrf_exempt
@is_authorize
@is_super_user
def update_coupon_detail(request):
    return cmn.db_update(request, CouponDetails)


# Delete Coupon Detail
# params: detailId
@csrf_exempt
@is_authorize
@is_super_user
def delete_coupon_detail(request):
    return cmn.db_delete(request, CouponDetails)


# -------------------------------------------------------
# ======================= COUPONS =======================
# -------------------------------------------------------
# Get list all Coupons
@csrf_exempt
def get_coupons_list(request):
    return cmn.db_get_all(request, Coupons)


# Get list all Coupon
@csrf_exempt
def get_coupon_list_ctm(request):
    res = {"error": 0, "message": "", "data": []}
    _limit = get_req_param(request, 'limit')
    _limit = 100 if _limit == '' else _limit  # if limit null get 100 record
    _offset = get_req_param(request, 'offset')
    _offset = 0 if _offset == '' else _offset
    full_list = Coupons.objects.filter(is_delete=const.ACTIVE_FLAG)
    res_list = full_list.order_by('-' + 'create_date')[int(_offset):int(_limit + _offset)]
    count_cp = 0
    for res_detail in res_list:
        _rm_day = util.count_rm_day(res_detail.to_date)
        price_off_str = res_detail.price_off
        if price_off_str is not None:
            res_detail.price_off = int(price_off_str)
        price_minus_str = res_detail.price_minus
        if price_minus_str is not None:
            res_detail.price_minus = int(price_minus_str)
        if _rm_day >= 0:
            _res_temp = cmn.model_to_dict_filter(request, res_detail)
            _res_temp['remain_day'] = _rm_day
            res["data"].append(_res_temp)
            count_cp += 1
            res["error"] = eCODE.RES_SUCCESS
    res["total"] = count_cp
    return JsonResponse(res)


# Get list coupon customize
@csrf_exempt
def get_coupon_list_with_cus_id(request):
    res = {"error": 0, "message": "", "data": []}
    _limit = get_req_param(request, 'limit')
    _limit = 100 if _limit == '' else _limit  # if limit null get 100 record
    _offset = get_req_param(request, 'offset')
    _offset = 0 if _offset == '' else _offset
    # array coupon type ( only Other type )
    coupon_type_list = [4]
    # array coupon used
    coupon_id_used_list = []
    # get param customer id
    _cusId = cmn.get_req_param(request, 'cusId')
    # get customer obj
    customer = Customers.objects.get(pk=_cusId, is_delete=const.ACTIVE_FLAG)
    # check condition
    # customer created date
    _createDate = customer.create_date
    # current date
    _date = util.get_curr_date().strftime("%Y-%m-%d %H:%M:%S")
    _today = cmn.convert_to_local_time(_date)
    # get customer birthday
    _birthday = customer.birthday
    # get customer number visit
    _numVisit = customer.num_visit
    # calculate welcome date range
    _welcome_date_range = _today - _createDate
    # 1. welcome
    if _welcome_date_range and _welcome_date_range < timedelta(days=30):
        # if in range > append to coupon type list
        coupon_type_list.append(1)
    # 2. 2st visit
    if _numVisit and _numVisit == 2:
        # if num_visit ==2 > append to coupon type list
        coupon_type_list.append(2)
    # 3. birthday in month
    if _birthday and _birthday.month == _today.month:
        # if birthday in current month > append to coupon type list
        coupon_type_list.append(3)
    # get used coupon by customer id
    cus_coupon_used = CustomerCoupons.objects.filter(customer_id=_cusId, is_delete=const.ACTIVE_FLAG)
    for cc in cus_coupon_used:
        coupon_id_used_list.append(cc.coupon_id)
    # get list coupon by customer minus used coupon
    cp_list = Coupons.objects.filter(coupon_type__in=coupon_type_list,
                                     is_delete=const.ACTIVE_FLAG).exclude(id__in=coupon_id_used_list)
    _total = cp_list.count()
    for cp in cp_list:
        _rm_day = util.count_rm_day(cp.to_date)
        if _rm_day < 0:
            _total -= 1
    res_list = cp_list.order_by('coupon_type')[int(_offset):int(_limit + _offset)]
    for res_detail in res_list:
        _rm_day = util.count_rm_day(res_detail.to_date)
        # check price off
        price_off_str = res_detail.price_off
        if price_off_str is not None:
            res_detail.price_off = int(price_off_str)
        # check price minus
        price_minus_str = res_detail.price_minus
        if price_minus_str is not None:
            res_detail.price_minus = int(price_minus_str)
        # check remain days
        if _rm_day >= 0:
            _res_temp = cmn.model_to_dict_filter(request, res_detail)
            _res_temp['remain_day'] = _rm_day
            res["data"].append(_res_temp)
            res["error"] = eCODE.RES_SUCCESS
    res["total"] = _total
    return JsonResponse(res)


# Get Coupon by id
@csrf_exempt
def get_coupon_by_id(request):
    return cmn.db_get_by_id(request, Coupons)


# Get Coupon by condition
@csrf_exempt
def get_coupon_by_condition(request):
    return cmn.db_get_by_cond(request, Coupons)


# Insert Coupon
@csrf_exempt
@is_authorize
@is_super_user
def insert_coupon(request):
    return cmn.db_insert(request, Coupons, 'number')


# Update Coupon
@csrf_exempt
@is_authorize
@is_super_user
def update_coupon(request):
    return cmn.db_update(request, Coupons, 'number')


# Delete Coupon
# params: couponId
@csrf_exempt
@is_authorize
@is_super_user
def delete_coupon(request):
    # check already in used
    check_info = {'coupon__id': get_req_param(request, 'id')}
    check_res = cmn.db_chk_in_used(check_info, CouponDetails)
    if check_res:
        return JsonResponse(check_res)

    # exec delete process
    return cmn.db_delete(request, CouponDetails)
