#!/usr/bin/env python
# -*- coding: utf-8 -*-
from django.views.decorators.csrf import csrf_exempt
from django.http import JsonResponse
from utils.utility import is_authorize, is_super_user
from commonmodels.models import Topics, TopicCategories, Settings
from commonmodels.views import get_req_param
import commonmodels.views as cmn
from utils import constants as const
import requests
import json
from utils.errcode import ErrCode as eCODE


# --------------------------------------------------------------
# ======================= TOPIC CATEGORY =======================
# --------------------------------------------------------------
# Get list all Topic Categories
@csrf_exempt
def get_topic_categories_list(request):
    return cmn.db_get_all(request, TopicCategories)


# Get Topic Category by id
@csrf_exempt
def get_topic_category_by_id(request):
    return cmn.db_get_by_id(request, TopicCategories)


# Get Topic Category by condition
@csrf_exempt
def get_topic_category_by_condition(request):
    return cmn.db_get_by_cond(request, TopicCategories)


# Insert Topic Category
@csrf_exempt
@is_authorize
@is_super_user
def insert_topic_category(request):
    return cmn.db_insert(request, TopicCategories, 'code')


# Update Topic Category
@csrf_exempt
@is_authorize
@is_super_user
def update_topic_category(request):
    return cmn.db_update(request, TopicCategories)


# Delete Topic Category
# params: categoryId
@csrf_exempt
@is_authorize
@is_super_user
def delete_topic_category(request):
    return cmn.db_delete(request, TopicCategories)


# -----------------------------------------------------
# ======================= TOPIC =======================
# -----------------------------------------------------
# Get list all Topics
@csrf_exempt
def get_topics_list(request):
    return cmn.db_get_ctm(request, Topics)


# Get Topic by id
@csrf_exempt
def get_topic_by_id(request):
    return cmn.db_get_by_id(request, Topics)


# Get Topic by condition
@csrf_exempt
def get_topic_by_condition(request):
    return cmn.db_get_by_cond(request, Topics)


# Get list Topics by category
@csrf_exempt
def get_topics_by_category(request):
    foreign_info = {'category': get_req_param(request, 'id')}
    return cmn.db_get_by_pk(request, Topics, foreign_info)


# Get list Topics by multi categories
@csrf_exempt
def get_topics_by_multi_categories(request):
    foreign_info = {'category': get_req_param(request, 'id')}
    return cmn.db_get_by_multi_pk(request, Topics, foreign_info)


# Insert Topic
@csrf_exempt
@is_authorize
@is_super_user
def insert_topic(request):
    return cmn.db_insert(request, Topics, 'subject')


# Update Topic
@csrf_exempt
@is_authorize
@is_super_user
def update_topic(request):
    return cmn.db_update(request, Topics)


# Delete Topic
# params: topicId
@csrf_exempt
@is_authorize
@is_super_user
def delete_topic(request):
    return cmn.db_delete(request, Topics)


# Get list news related Instagram
@csrf_exempt
def get_instagram_data(request):
    res = {"error": 0, "message": "", "data": []}
    # get token
    setting = Settings.objects.filter(code='insta_access_token', is_delete=const.ACTIVE_FLAG).first()
    insta_access_token = setting.value
    count_url = '&count='
    max_id_url = '&max_id='
    count_val = 6

    try:
        # is load more
        next_max_id = cmn.get_req_param(request, 'next_max_id')
        if next_max_id:
            api_url = const.INSTAGRAM_URL + insta_access_token + count_url + str(count_val) + max_id_url + next_max_id
        # not load
        else:
            api_url = const.INSTAGRAM_URL + insta_access_token + count_url + str(count_val)

        r = requests.get(api_url)
        _json_res = json.loads(r.text)
        res['error'] = eCODE.RES_SUCCESS
        res['data'] = _json_res['data']
        res['pagination'] = _json_res['pagination']
    except Exception as e:
        res['error'] = eCODE.RES_FAIL
        res['message'] = e
        print(e)
    return JsonResponse(res)
