#!/usr/bin/env python
# -*- coding: utf-8 -*-
from django.db.models import Q
from django.http import JsonResponse
from django.views.decorators.csrf import csrf_exempt
from django.contrib.auth.models import User
from django.db import transaction

from utils.utility import is_authorize, is_super_user, get_curr_timezone
from commonmodels.models import Customers, VoteSessions, CustomerCoupons, DeviceInfo, Memberships, MembershipStatus
from commonmodels.views import get_req_param
import commonmodels.views as cmn
import utils.constants as const
from utils.message import get_msg_content
from utils.errcode import ErrCode as eCODE
# from utils.constants import MEDIA_BARCODE, MEDIA_QRCODE

import random


# import qrcode
# import barcode
# from barcode.writer import ImageWriter


# ----------------------------------------------------------------
# ======================= CUSTOMER COUPONS =======================
# ----------------------------------------------------------------
# Get list all Customer Coupons
@csrf_exempt
def get_customer_coupons_list(request):
    return cmn.db_get_all(request, CustomerCoupons)


# Get Customer Coupon by id
@csrf_exempt
def get_customer_coupon_by_id(request):
    return cmn.db_get_by_id(request, CustomerCoupons)


# Get Customer Coupon by condition
@csrf_exempt
def get_customer_coupon_by_condition(request):
    return cmn.db_get_by_cond(request, CustomerCoupons)


# Insert Customer Coupon
@csrf_exempt
def insert_customer_coupon(request):
    res = {"error": 0, "message": "", "data": {}}
    auth_flg = cmn.is_authorize(request)
    if not auth_flg:
        return auth_flg
    try:
        _coupon_id = cmn.get_req_param(request, 'coupon_id')
        _cus_id = request.session.get(const.SESSION_U_ID, None)

        if not _coupon_id:
            res["error"] = eCODE.REQ_ID_REQUIRED
            res["message"] = get_msg_content('action_id_required', 'Coupon')
        else:
            customer_coupon = CustomerCoupons.objects.filter(customer_id=_cus_id, coupon_id=_coupon_id,
                                                             is_delete=const.ACTIVE_FLAG)
            if customer_coupon.count() > 0:
                res["error"] = eCODE.RES_FAIL
                res["message"] = "Error"
            else:
                cus_coupon = CustomerCoupons()
                cus_coupon.customer_id = _cus_id
                cus_coupon.coupon_id = _coupon_id
                cus_coupon.register_date = get_curr_timezone()
                cus_coupon.use_date = get_curr_timezone()
                cus_coupon.status = 1
                cus_coupon.save()
                # response data
                res["error"] = eCODE.RES_SUCCESS
                res["message"] = get_msg_content('action_success', 'Insert')
                res["data"] = cmn.model_to_dict_filter(request, cus_coupon)
    except Exception as e:
        res["error"] = eCODE.RES_FAIL
        res["message"] = str(e)
        print(e)
    return JsonResponse(res)


# Update Customer Coupon
@csrf_exempt
@is_authorize
def update_customer_coupon(request):
    return cmn.db_update(request, CustomerCoupons)


# Delete Customer Coupon
# params: cus_couponId
@csrf_exempt
@is_authorize
def delete_customer_coupon(request):
    return cmn.db_delete(request, CustomerCoupons)


# -----------------------------------------------------------
# ======================= DEVICE INFO =======================
# -----------------------------------------------------------
# Get push notify info
@csrf_exempt
def get_push_notify(request):
    res = {"error": 0, "message": "", "data": []}
    _type = get_req_param(request, 'type')

    res_list = DeviceInfo.objects.filter(type=_type, is_delete=const.ACTIVE_FLAG)

    res["error"] = eCODE.RES_SUCCESS
    for res_detail in res_list:
        res["data"].append(cmn.model_to_dict(res_detail))

    return JsonResponse(res)


# Set push notify info
@csrf_exempt
def set_push_notify(request):
    res = {"error": 0, "message": ""}
    try:
        _cus_id = request.session.get(const.SESSION_U_ID, None)
        _data = get_req_param(request, "data")
        _device_token = _data.get('device_token', None)
        _type = _data.get('type', None)
        _is_push_notify = _data.get('is_push_notify', 1)

        # search existed device
        device_list = DeviceInfo.objects.filter(device_token=_device_token, type=_type, is_delete=const.ACTIVE_FLAG)

        if device_list:
            # update case
            device = device_list[0]
        else:
            # insert case
            device = DeviceInfo()
            device.device_token = _device_token
            device.type = _type

        # bind data
        device.is_push_notify = _is_push_notify
        device.save()

        # apply to all devices if existed CusId
        app_list = DeviceInfo.objects.filter(Q(device_token=_device_token) & Q(is_delete=const.ACTIVE_FLAG))

        with transaction.atomic():
            for app in app_list:
                app.is_push_notify = _is_push_notify
                app.save()

        res["error"] = eCODE.RES_SUCCESS
        res["message"] = get_msg_content('action_success', 'Set push notify')

    except KeyError as e:
        print(e)
        res["error"] = eCODE.REQ_MISSING_RGS
        res["message"] = get_msg_content('request_missing_args', str(e))
    except Exception as e:
        print(e)
        res["error"] = eCODE.RES_FAIL
        res["message"] = str(e)

    return JsonResponse(res)


# ---------------------------------------------------------
# ======================= CUSTOMERS =======================
# ---------------------------------------------------------
# Get list all Customers
@csrf_exempt
def get_customers_list(request):
    return cmn.db_get_all(request, Customers)


# Get Customer by id
@csrf_exempt
def get_customer_by_id(request):
    res = {"error": 0, "message": "", "data": {}}
    auth_flg = cmn.is_authorize(request)
    if not auth_flg:
        return auth_flg
    _cus_id = get_req_param(request, 'id')
    if _cus_id:
        try:
            # get current point form login user
            customer = Customers.objects.get(pk=_cus_id, is_delete=const.ACTIVE_FLAG)
            total_point = customer.total_point
            mem_status = MembershipStatus.objects.filter(is_delete=const.ACTIVE_FLAG).order_by('goal_point')
            remain_point = 0
            i = 0
            for ms in mem_status:
                if i < len(mem_status) - 1:
                    if (ms.goal_point <= total_point) and (mem_status[i + 1].goal_point > total_point):
                        remain_point = mem_status[i + 1].goal_point - total_point
                        break
                    i += 1

            if customer:
                res["error"] = eCODE.RES_SUCCESS
                res["data"] = cmn.model_to_dict_filter(request, customer)
                res["data"]['mem_status_name'] = mem_status[i].name
                res["data"]['mem_status_name_cn'] = mem_status[i].name_cn
                if remain_point > 0:
                    res["data"]['mem_status_next_name'] = mem_status[i + 1].name
                    res["data"]['mem_status_next_name_cn'] = mem_status[i + 1].name_cn
                    res["data"]['mem_status_next_level_point'] = mem_status[i + 1].goal_point
                else:
                    res["data"]['mem_status_next_name'] = None
                    res["data"]['mem_status_next_name_cn'] = None
                    res["data"]['mem_status_next_level_point'] = None
                res["data"]['mem_status_content'] = mem_status[i].content
                res["data"]['mem_status_content_cn'] = mem_status[i].content_cn
                res["data"]['mem_status_short_content'] = mem_status[i].short_content
                res["data"]['mem_status_short_content_cn'] = mem_status[i].short_content_cn
                res["data"]['mem_status_short_content_2'] = mem_status[i].short_content_2
                res["data"]['mem_status_short_content_2_cn'] = mem_status[i].short_content_2_cn
                res["data"]['mem_status_content_special'] = mem_status[i].content_special
                res["data"]['mem_status_content_special_cn'] = mem_status[i].content_special_cn
                res["data"]['mem_status_goal_point'] = mem_status[i].goal_point
                res["data"]['mem_status_remain_point'] = remain_point
                res["data"]['mem_status_image_path'] = cmn.aqr_current_path(request) + const.MEDIA_URL + str(
                    mem_status[i].image_path)
                res["data"]['mem_status_hex_color'] = mem_status[i].hex_color
                res["data"]['mem_status_hex_color_benefit_1'] = mem_status[i].hex_color_benefit_1
                res["data"]['mem_status_hex_color_benefit_2'] = mem_status[i].hex_color_benefit_2
            else:
                res["error"] = eCODE.SCH_NOT_FOUND
                res['message'] = get_msg_content('data_not_found', Customers, str(_cus_id))
        except Exception as e:
            res["error"] = eCODE.RES_FAIL
            res["message"] = str(e)
    else:
        res["error"] = eCODE.REQ_ID_REQUIRED
        res["message"] = get_msg_content('action_id_required', Customers)

    return JsonResponse(res)


# Get Customer by condition
@csrf_exempt
def get_customer_by_condition(request):
    return cmn.db_get_by_cond_with_token(request, Customers)


# Insert Customer
@csrf_exempt
def insert_customer(request):
    res = {"error": 0, "message": "", "data": {}}
    try:
        _data = get_req_param(request, "data")
        customer = Customers()

        # check duplicate
        check_res = cmn.db_chk_dup_ins(Customers, _data, ['email', 'phone'])
        if check_res:
            return JsonResponse(check_res)

        num_length = 8
        number = random.randint(10 ** (num_length - 1), (10 ** (num_length) - 1))
        check_number_info = {'number': number}
        check_exist_number = cmn.db_chk_dup_ins_ctm_no_pk(Customers, check_number_info, ['number'])
        while check_exist_number:
            number = random.randint(10 ** (num_length - 1), (10 ** (num_length) - 1))
            check_number_info = {'number': number}
            check_exist_number = cmn.db_chk_dup_ins_ctm_no_pk(Memberships, check_number_info, ['number'])

        # create django user belong to Customer info
        curr_user = User.objects.create_user(username=_data["email"],
                                             email=_data["email"],
                                             password=_data["login_password"])

        # binding info
        customer = cmn.json_to_model(customer, _data)
        # custom
        customer.user_id = curr_user.id
        customer.number = number

        # save to database
        return cmn.db_insert_or_update(request, customer, 'Insert')
    except Exception as e:
        print(e)
        res["error"] = 1
        res["message"] = str(e)

    return JsonResponse(res)


# Update Customer
@csrf_exempt
# @is_authorize
def update_customer(request):
    res = {"error": 0, "message": "", "data": {}}
    auth_flg = cmn.is_authorize(request)
    if not auth_flg:
        return auth_flg
    try:
        _data = get_req_param(request, "data")

        # get original data
        _id = _data.get('id', '')
        customer = Customers.objects.get(pk=_id, is_delete=const.ACTIVE_FLAG)

        # check duplicate
        check_res = cmn.db_chk_dup_upd(Customers, _data, 'phone')
        if check_res:
            return JsonResponse(check_res)

        # binding info
        customer = cmn.json_to_model(customer, _data)

        # save to database
        return cmn.db_insert_or_update(request, customer, 'Update')
    except Exception as e:
        print(e)
        res["error"] = 1
        res["message"] = str(e)

    return JsonResponse(res)


# Delete Customer
# params: customerId
@csrf_exempt
@is_authorize
@is_super_user
def delete_customer(request):
    # check already in used
    check_info = {'customer__id': get_req_param(request, 'id')}
    check_res = cmn.db_chk_in_used(check_info, VoteSessions, CustomerCoupons)
    if check_res:
        return JsonResponse(check_res)

    # exec delete process
    return cmn.db_delete(request, Customers)


# ---------------------------------------------------------
# ======================= MEMBERSHIPS =======================
# ---------------------------------------------------------
# Get membership card information
@csrf_exempt
def get_membership(request):
    res = {"error": 0, "message": "", "data": {}}
    device_id = get_req_param(request, 'device_id')
    type = get_req_param(request, 'type')
    try:
        check_device_info = {'device_id': device_id}
        check_exist_member = cmn.db_chk_dup_ins_ctm_no_pk(Memberships, check_device_info, ['device_id'])
        if check_exist_member:
            res_data = Memberships.objects.filter(device_id=device_id, is_delete=const.ACTIVE_FLAG).last()
            if res_data:
                res["error"] = eCODE.RES_SUCCESS
                res["data"] = cmn.model_to_dict_filter(request, res_data)
            else:
                res["error"] = eCODE.SCH_NOT_FOUND
                res['message'] = get_msg_content('data_not_found', Memberships, str(device_id))
            return JsonResponse(res)

        num_length = 8
        number = random.randint(10 ** (num_length - 1), (10 ** (num_length) - 1))
        check_number_info = {'number': number}
        check_exist_number = cmn.db_chk_dup_ins_ctm_no_pk(Memberships, check_number_info, ['number'])
        while check_exist_number:
            number = random.randint(10 ** (num_length - 1), (10 ** (num_length) - 1))
            check_number_info = {'number': number}
            check_exist_number = cmn.db_chk_dup_ins_ctm_no_pk(Memberships, check_number_info, ['number'])
        member_ship = Memberships()
        member_ship.number = number
        member_ship.device_id = device_id
        member_ship.type = type
        member_ship.save()

        res["error"] = eCODE.RES_SUCCESS
        res["data"] = cmn.model_to_dict_filter(request, member_ship)

        # cmn.db_insert_or_update()
        # # Generate barcode
        # path_barcode = MEDIA_BARCODE + device_id + '_barcode' + '.png'
        # menber_barcode = barcode.get('ean13', str(number), writer=ImageWriter())
        # barcode_file = menber_barcode.save(path_barcode, 'PNG')
        #
        # # Generate qrcode
        # path_qrcode = MEDIA_QRCODE + device_id + '_qrcode' + '.png'
        # menber_qrcode = qrcode.make(str(number))
        # qrcode_file = menber_qrcode.make_image(fill_color="black", back_color="white")

        return JsonResponse(res)

    except Exception as e:
        print(e)
        res["error"] = 1
        res["message"] = str(e)

    return JsonResponse(res)


# Get list all membership status
@csrf_exempt
def get_membership_status_list(request):
    return cmn.db_get_all(request, MembershipStatus)
