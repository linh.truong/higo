#!/usr/bin/env python
# -*- coding: utf-8 -*-
from django.db import connection
from django.http import JsonResponse
from django.views.decorators.csrf import csrf_exempt

from higo.settings_dev import DEBUG
from utils.utility import is_authorize, is_super_user, get_curr_timezone
from commonmodels.models import Foods, Items, FoodPrices, FoodItems, CustomerFoods
from commonmodels.models import CouponDetails
from commonmodels.models import FoodCategories
from commonmodels.models import FoodTypes

from commonmodels.views import get_req_param
import commonmodels.views as cmn


# ---------------------------------------------------------------
# ======================= FOOD CATEGORIES =======================
# ---------------------------------------------------------------
# Get all food categories
@csrf_exempt
def get_food_category_list(request):
    return cmn.db_get_all(request, FoodCategories)


# Get food category by id
@csrf_exempt
def get_food_cat_by_id(request):
    return cmn.db_get_by_id(request, FoodCategories)


# Get food category by condition
@csrf_exempt
def get_food_cat_by_condition(request):
    return cmn.db_get_by_cond(request, FoodCategories)


# Insert Food Category
@csrf_exempt
@is_authorize
@is_super_user
def insert_food_cat(request):
    return cmn.db_insert(request, FoodCategories, 'name')


# Update Food Category
@csrf_exempt
@is_authorize
@is_super_user
def update_food_cat(request):
    return cmn.db_update(request, FoodCategories)


# Delete Food Category
# params: categoryId
@csrf_exempt
@is_authorize
@is_super_user
def delete_food_cat(request):
    # check already in used
    check_info = {'category__id': get_req_param(request, 'id')}
    check_res = cmn.db_chk_in_used(check_info, Foods, Items)
    if check_res:
        return JsonResponse(check_res)

    # exec delete process
    return cmn.db_delete(request, FoodCategories)


# ----------------------------------------------------------
# ======================= FOOD TYPES =======================
# ----------------------------------------------------------
# Get list all Food Types
@csrf_exempt
def get_food_types_list(request):
    return cmn.db_get_all(request, FoodTypes)


# Get Food Type by id
@csrf_exempt
def get_food_type_by_id(request):
    return cmn.db_get_by_id(request, FoodTypes)


# Get Food Type by condition
@csrf_exempt
def get_food_type_by_condition(request):
    return cmn.db_get_by_cond(request, FoodTypes)


# Insert Food Type
@csrf_exempt
@is_authorize
@is_super_user
def insert_food_type(request):
    return cmn.db_insert(request, FoodTypes, 'name')


# Update Food Type
@csrf_exempt
@is_authorize
@is_super_user
def update_food_type(request):
    return cmn.db_update(request, FoodTypes)


# Delete Food Type
# params: categoryId
@csrf_exempt
@is_authorize
@is_super_user
def delete_food_type(request):
    # check already in used
    check_info = {'type__id': get_req_param(request, 'id')}
    check_res = cmn.db_chk_in_used(check_info, Foods, Foods)
    if check_res:
        return JsonResponse(check_res)

    # exec delete process
    return cmn.db_delete(request, FoodTypes)


# ----------------------------------------------------------
# ======================= FOOD PRICES =======================
# ----------------------------------------------------------
# Get list all Food Prices
@csrf_exempt
def get_food_prices_list(request):
    return cmn.db_get_all(request, FoodPrices)


# Get Food Price by id
@csrf_exempt
def get_food_price_by_id(request):
    return cmn.db_get_by_id(request, FoodPrices)


# Get Food Price by condition
@csrf_exempt
def get_food_price_by_condition(request):
    return cmn.db_get_by_cond(request, FoodPrices)


# Insert Food Price
@csrf_exempt
@is_authorize
@is_super_user
def insert_food_price(request):
    return cmn.db_insert(request, FoodPrices)


# Update Food Price
@csrf_exempt
@is_authorize
@is_super_user
def update_food_price(request):
    return cmn.db_update(request, FoodPrices)


# Delete Food Price
# params: priceId
@csrf_exempt
@is_authorize
@is_super_user
def delete_food_price(request):
    return cmn.db_delete(request, FoodPrices)


# -----------------------------------------------------
# ======================= FOODS =======================
# -----------------------------------------------------
# Get all Foods
@csrf_exempt
def get_foods_list(request):
    return cmn.db_get_all(request, Foods)


# Get Food by id
@csrf_exempt
def get_food_by_id(request):
    return cmn.db_get_by_id(request, Foods)


# Get Food by condition
@csrf_exempt
def get_food_by_condition(request):
    return cmn.db_get_by_cond(request, Foods)


# Get list Foods by category
@csrf_exempt
def get_foods_by_category(request):
    foreign_info = {'category': get_req_param(request, 'id')}
    return cmn.db_get_by_pk(request, Foods, foreign_info)


# Get list Foods by multi categories
@csrf_exempt
def get_foods_by_multi_categories(request):
    return cmn.prc_get_by_multi_pk(request, Foods, 'search_foods_cat')


# Insert food
@csrf_exempt
@is_authorize
@is_super_user
def insert_food(request):
    return cmn.db_insert(request, Foods, 'name')


# Update food
@csrf_exempt
@is_authorize
@is_super_user
def update_food(request):
    return cmn.db_update(request, Foods)


# Delete food
# params: foodId
@csrf_exempt
@is_authorize
@is_super_user
def delete_food(request):
    # check already in used
    check_info = {'food_id': get_req_param(request, 'id')}
    check_res = cmn.db_chk_in_used(check_info, FoodItems, FoodPrices, CustomerFoods, CouponDetails)
    if check_res:
        return JsonResponse(check_res)

    # exec delete process
    return cmn.db_delete(request, Foods)
