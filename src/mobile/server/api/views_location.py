#!/usr/bin/env python
# -*- coding: utf-8 -*-

from django.views.decorators.csrf import csrf_exempt
from commonmodels.models import Areas, Stations
import commonmodels.views as cmn


# -------------------------------------------------------
# ======================= AREAS =========================
# -------------------------------------------------------
# Get list all Areas
@csrf_exempt
def get_areas_list(request):
    return cmn.db_get_all(request, Areas)


# Get Area by id
@csrf_exempt
def get_area_by_id(request):
    return cmn.db_get_by_id(request, Areas)


# Get Area by condition
@csrf_exempt
def get_area_by_condition(request):
    return cmn.db_get_by_cond(request, Areas)


# ----------------------------------------------------------
# ======================= STATIONS =========================
# ----------------------------------------------------------
# Get list all Stations
@csrf_exempt
def get_stations_list(request):
    return cmn.db_get_all(request, Stations)


# Get Station by id
@csrf_exempt
def get_station_by_id(request):
    return cmn.db_get_by_id(request, Stations)


# Get Station by condition
@csrf_exempt
def get_station_by_condition(request):
    return cmn.db_get_by_cond(request, Stations)
