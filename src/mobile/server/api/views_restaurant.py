#!/usr/bin/env python
# -*- coding: utf-8 -*-
import json

from django.contrib.auth.models import User
from django.http import JsonResponse
from django.views.decorators.csrf import csrf_exempt

from higo.settings_dev import MEDIA_URL
from utils import constants
from utils.utility import is_authorize, is_super_user, get_curr_date
from commonmodels.models import Restaurants, Contacts, Foods, Coupons, VoteItems, Suppliers
import commonmodels.views as cmn
from utils import constants as const
import re


# -------------------------------------------------------------
# ======================= RESTAURANTS =========================
# -------------------------------------------------------------
# Get list all Restaurants
@csrf_exempt
def get_restaurants_list(request):
    return cmn.db_get_all(request, Restaurants)


# Get Restaurant by id
@csrf_exempt
def get_restaurant_by_id(request):
    return cmn.db_get_by_id(request, Restaurants)


# Get Restaurant by condition
@csrf_exempt
def get_restaurant_by_condition(request):
    return cmn.db_get_by_cond(request, Restaurants)


# Insert Restaurant
@csrf_exempt
@is_authorize
@is_super_user
def insert_restaurant(request):
    res = {"error": 0, "message": "", "data": {}}
    try:
        _data = cmn.get_req_param(request, "data")
        restaurant = Restaurants()

        # check duplicate
        check_res = cmn.db_chk_dup_ins(Restaurants, _data, ['code', 'email', 'phone'])
        if check_res:
            return JsonResponse(check_res)

        # create django user belong to Customer info
        curr_user = User.objects.create_user(username=_data["code"],
                                             email=_data["email"],
                                             password=_data["login_password"])

        # binding info
        restaurant = cmn.json_to_model(restaurant, _data)
        # custom
        restaurant.user_id = curr_user.id

        # save to database
        return cmn.db_insert_or_update(request, restaurant, 'Insert')
    except Exception as e:
        print(e)
        res["error"] = 1
        res["message"] = str(e)

    return JsonResponse(res)


# Update Restaurant
@csrf_exempt
@is_authorize
@is_super_user
def update_restaurant(request):
    res = {"error": 0, "message": "", "data": {}}
    try:
        _data = cmn.get_req_param(request, "data")

        # get original data
        _id = _data.get('id', '')
        restaurant = Restaurants.objects.get(pk=_id, is_delete=constants.ACTIVE_FLAG)

        # binding info
        restaurant = cmn.json_to_model(restaurant, _data)

        # save to database
        return cmn.db_insert_or_update(request, restaurant, 'Update')
    except Exception as e:
        print(e)
        res["error"] = 1
        res["message"] = str(e)

    return JsonResponse(res)


# Delete Restaurant
# params: restaurantId
@csrf_exempt
@is_authorize
@is_super_user
def delete_restaurant(request):
    # check already in used
    check_info = {'restaurant__id': cmn.get_req_param(request, 'id')}
    check_res = cmn.db_chk_in_used(check_info, Contacts, Foods, Coupons, VoteItems, Suppliers)
    if check_res:
        return JsonResponse(check_res)

    # exec delete process
    return cmn.db_delete(request, Restaurants)


# ----------------------------------------------------------
# ======================= CONTACTS =========================
# ----------------------------------------------------------
# Get list all Contacts
@csrf_exempt
def get_contacts_list(request):
    return cmn.db_get_all(request, Contacts)


# Get Contact by id
@csrf_exempt
def get_contact_by_id(request):
    return cmn.db_get_by_id(request, Contacts)


# Insert Contact Category
@csrf_exempt
def insert_contact(request):
    return cmn.db_insert(request, Contacts)


# Update Contact Category
@csrf_exempt
def update_contact(request):
    return cmn.db_update(request, Contacts)


# Delete Contact Category
# params: contactId
@csrf_exempt
def delete_contact(request):
    # exec delete process
    return cmn.db_delete(request, Contacts)


# ----------------------------------------------------------
# ======================= COMPANY =========================
# ----------------------------------------------------------
# Get company settings
@csrf_exempt
def get_comp_info(request):
    return cmn.prc_get_data_by_store(request, Restaurants, 'search_comp_info')
