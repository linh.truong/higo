#!/usr/bin/env python
# -*- coding: utf-8 -*-
from django.db import transaction
from django.contrib.auth.models import User
from django.http import JsonResponse
from django.views.decorators.csrf import csrf_exempt

from utils import constants
from utils.utility import is_authorize, get_curr_timezone, get_curr_date
from commonmodels.models import RestaurantPoints, Customers, CustomerPoints, MembershipStatus
import commonmodels.views as cmn
from utils.message import get_msg_content
from utils.errcode import ErrCode as eCODE


# -----------------------------------------------------------------
# ======================= CUSTOMER POINTS =========================
# -----------------------------------------------------------------
# Get list all Customer Points
@csrf_exempt
def get_customer_points_list(request):
    return cmn.db_get_all(request, CustomerPoints)


# Get Customer Point by id
@csrf_exempt
def get_customer_point_by_id(request):
    return cmn.db_get_by_id(request, CustomerPoints)


# Insert Customer Point
@csrf_exempt
def insert_customer_point(request):
    return cmn.db_insert(request, CustomerPoints)


# Update Customer Point
@csrf_exempt
def update_customer_point(request):
    return cmn.db_update(request, CustomerPoints)


# Delete Customer Point
# params: pointId
@csrf_exempt
def delete_customer_point(request):
    # exec delete process
    return cmn.db_delete(request, CustomerPoints)


# -------------------------------------------------------------------
# ======================= RESTAURANT POINTS =========================
# -------------------------------------------------------------------
# Get list all Restaurant Points
@csrf_exempt
def get_restaurant_points_list(request):
    return cmn.db_get_all(request, RestaurantPoints)


# Get Restaurant Point by id
@csrf_exempt
def get_restaurant_point_by_id(request):
    return cmn.db_get_by_id(request, RestaurantPoints)


# Get Restaurant Point by condition
@csrf_exempt
def get_restaurant_point_by_condition(request):
    return cmn.db_get_by_cond(request, RestaurantPoints)


# Insert Restaurant Point
@csrf_exempt
def insert_restaurant_point(request):
    res = {"error": 0, "message": "", "data": {}}
    try:
        _data = cmn.get_req_param(request, "data")
        res_point = RestaurantPoints()

        # make random code
        _tmp_code = User.objects.make_random_password(10)

        # binding info
        res_point = cmn.json_to_model(res_point, _data)
        # custom
        _res_code = res_point.restaurant._meta.__getattribute__('code')
        res_point.qr_code = build_qr_code(_res_code, _tmp_code, res_point.issue_date, res_point.point)

        # save to database
        return cmn.db_insert_or_update(request, res_point, 'Insert')
    except Exception as e:
        print(e)
        res["error"] = 1
        res["message"] = str(e)

    return JsonResponse(res)


def build_qr_code(*args):
    result = 'res_code: %s; code: %s; issue_date: %s; point: %s' % args
    return result


# Update Restaurant Point
@csrf_exempt
def update_restaurant_point(request):
    res = {"error": 0, "message": "", "data": {}}
    try:
        _data = cmn.get_req_param(request, "data")

        # get original data
        _id = _data.get('id', '')
        res_point = RestaurantPoints.objects.get(pk=_id, is_delete=constants.ACTIVE_FLAG)

        # binding info
        res_point = cmn.json_to_model(res_point, _data)

        # save to database
        return cmn.db_insert_or_update(request, res_point, 'Update')
    except Exception as e:
        print(e)
        res["error"] = 1
        res["message"] = str(e)

    return JsonResponse(res)


# Delete Restaurant Point
# params: pointId
@csrf_exempt
@is_authorize
def delete_restaurant_point(request):
    # check already in used
    check_info = {'res_point__id': cmn.get_req_param(request, 'id')}
    check_res = cmn.db_chk_in_used(check_info, CustomerPoints)
    if check_res:
        return JsonResponse(check_res)

    # exec delete process
    return cmn.db_delete(request, RestaurantPoints)


# Use QR CODE
# params: qr_code
@csrf_exempt
# @is_authorize
@transaction.atomic
def use_point_by_qr_code(request):
    res = {"error": 0, "message": "", "point": 0}
    auth_flg = cmn.is_authorize(request)
    if not auth_flg:
        return auth_flg
    try:
        # Get curr customer request
        _cus_id = request.session.get(constants.SESSION_U_ID, None)

        qr_code_req = cmn.get_req_param(request, 'qr_code')
        if not qr_code_req:
            res["error"] = eCODE.REQ_ID_REQUIRED
            res["message"] = get_msg_content('action_required', 'QR Code')
        elif not _cus_id:
            res["error"] = eCODE.LGN_REQUIRED
            res["message"] = get_msg_content('login_required')
        else:
            # Get restaurant point info by qr code
            _res_point_lst = RestaurantPoints.objects.filter(qr_code=qr_code_req, is_delete=constants.ACTIVE_FLAG)
            if _res_point_lst:
                _res_point = _res_point_lst[0]
                # Get curr customer info & membership status
                _curr_cus = Customers.objects.get(pk=_cus_id, is_delete=constants.ACTIVE_FLAG)
                mem_status = MembershipStatus.objects.filter(is_delete=constants.ACTIVE_FLAG,
                                                             goal_point__gte=_curr_cus.total_point).order_by(
                    'goal_point').first()
                # check already in used
                check_info = {'res_point__id': _res_point.id
                    , 'customer_id': _cus_id}
                check_res = cmn.db_chk_dup_ins_ctm_no_pk(CustomerPoints, check_info, ['res_point__id', 'customer_id'])
                if check_res:
                    check_res['error'] = eCODE.RES_FAIL
                    check_res['message'] = get_msg_content('action_used_exp', 'This Qr Code')
                    return JsonResponse(check_res)

                # Update current point
                _goal_point = mem_status.goal_point
                new_point_life = _curr_cus.total_point + _res_point.point
                # set level up date
                if new_point_life >= _goal_point:
                    _curr_cus.level_up_date = cmn.get_curr_timezone()
                _curr_cus.current_point = _curr_cus.current_point + _res_point.point
                _curr_cus.total_point = new_point_life
                _curr_cus.save()

                # Update history
                _cus_point = CustomerPoints()
                _cus_point.customer = _curr_cus
                _cus_point.res_point = _res_point
                _curr_time = get_curr_timezone()
                _cus_point.use_date = _curr_time
                _cus_point.save()
                res["error"] = eCODE.RES_SUCCESS
                res["current_point"] = _curr_cus.current_point
                res["total_point"] = _curr_cus.total_point
                res["res_point"] = _res_point.point
                res["message"] = get_msg_content('used_qr_code_success', _curr_cus.current_point)
            else:
                res["error"] = eCODE.RES_NOT_FOUND
                res["message"] = get_msg_content('not_found', 'QR Code')
                return JsonResponse(res)
    # exec delete process
    except Exception as e:
        print(e)
        res["error"] = eCODE.RES_FAIL
        res["message"] = str(e)

    return JsonResponse(res)


# params: cusNumber, ecPoint
@csrf_exempt
@transaction.atomic
def ec_site_add_point(request):
    res = {"error": 0, "message": "", "data": {}}
    try:
        # Get curr customer info
        _cus_number = cmn.get_req_param(request, 'cusNumber')
        _ec_point = cmn.get_req_param(request, 'ecPoint')
        if cmn.get_req_param(request, 'lastTransactionDate'):
            _last_transaction = cmn.get_req_param(request, 'lastTransactionDate')
        else:
            _last_transaction = get_curr_date().strftime("%Y-%m-%d %H:%M:%S")

        if not _ec_point:
            res["error"] = eCODE.REQ_ID_REQUIRED
            res["message"] = get_msg_content('action_required', 'Point')
        elif not _cus_number:
            res["error"] = eCODE.LGN_REQUIRED
            res["message"] = get_msg_content('login_required')

        customer = Customers.objects.filter(number=_cus_number, is_delete=constants.ACTIVE_FLAG).first()
        mem_status = MembershipStatus.objects.filter(is_delete=constants.ACTIVE_FLAG,
                                                     goal_point__gte=customer.total_point).order_by(
            'goal_point').first()
        if customer:
            _current_point = customer.current_point
            _total_point = customer.total_point
            new_point = _current_point + _ec_point
            new_point_life = _total_point + _ec_point
            # set level up date
            if mem_status:
                _goal_point = mem_status.goal_point
                if new_point_life >= _goal_point:
                    customer.level_up_date = cmn.get_curr_timezone()

            customer.current_point = new_point
            customer.total_point = new_point_life
            customer.last_transaction_date = cmn.convert_to_local_time(_last_transaction)
            customer.save()

            customer_dict = cmn.model_to_dict_filter(request, customer)
            res["error"] = eCODE.RES_SUCCESS
            res['data'] = customer_dict
        else:
            res["error"] = eCODE.RES_NOT_FOUND
            res["message"] = get_msg_content('not_found', 'Customer')
            return JsonResponse(res)
            # exec delete process
    except Exception as e:
        print(e)
        res["error"] = eCODE.RES_FAIL
        res["message"] = str(e)

    return JsonResponse(res)


@csrf_exempt
@transaction.atomic
def ec_site_minus_point(request):
    res = {"error": 0, "message": "", "data": {}}
    try:
        # Get curr customer info
        _cus_number = cmn.get_req_param(request, 'cusNumber')
        _ec_point = cmn.get_req_param(request, 'ecPoint')
        if cmn.get_req_param(request, 'lastTransactionDate'):
            _last_transaction = cmn.get_req_param(request, 'lastTransactionDate')
        else:
            _last_transaction = get_curr_date().strftime("%Y-%m-%d %H:%M:%S")

        if not _ec_point:
            res["error"] = eCODE.REQ_ID_REQUIRED
            res["message"] = get_msg_content('action_required', 'Point')
        elif not _cus_number:
            res["error"] = eCODE.LGN_REQUIRED
            res["message"] = get_msg_content('login_required')

        customer = Customers.objects.filter(number=_cus_number, is_delete=constants.ACTIVE_FLAG).first()

        if customer:
            _current_point = customer.current_point
            if _current_point >= _ec_point:
                minus_point = _current_point - _ec_point
                customer.current_point = minus_point
                customer.last_transaction_date = cmn.convert_to_local_time(_last_transaction)
                customer.save()

                customer_dict = cmn.model_to_dict_filter(request, customer)
                res["error"] = eCODE.RES_SUCCESS
                res['data'] = customer_dict
            else:
                res["error"] = eCODE.MINUS_POINT_FAIL
                res["data"]["current_point"] = _current_point
                res["message"] = "Your point not enough to use"
                return JsonResponse(res)
        else:
            res["error"] = eCODE.RES_NOT_FOUND
            res["message"] = get_msg_content('not_found', 'Customer')
            return JsonResponse(res)
            # exec delete process
    except Exception as e:
        print(e)
        res["error"] = eCODE.RES_FAIL
        res["message"] = str(e)

    return JsonResponse(res)
