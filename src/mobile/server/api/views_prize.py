#!/usr/bin/env python
# -*- coding: utf-8 -*-
from django.db import transaction
from django.http import JsonResponse
from django.views.decorators.csrf import csrf_exempt
from django.template import Context
from django.template.loader import get_template
from django.core.mail import EmailMultiAlternatives
from utils.utility import is_authorize, get_curr_timezone
from commonmodels.models import CustomerPrizes, Prizes, Customers
import commonmodels.views as cmn
import utils.constants as const
from utils.message import get_msg_content
from utils.errcode import ErrCode as eCODE
import random


# -----------------------------------------------------------------
# ======================= CUSTOMER PRIZES =========================
# -----------------------------------------------------------------
# Get list all Customer Prizes
@csrf_exempt
def get_customer_prizes_list(request):
    return cmn.db_get_all(request, CustomerPrizes)


# Get Customer Prize by id
@csrf_exempt
def get_customer_prize_by_id(request):
    return cmn.db_get_by_id(request, CustomerPrizes)


# Insert Customer Prize
@csrf_exempt
def insert_customer_prize(request):
    return cmn.db_insert(request, CustomerPrizes)


# Update Customer Prize
@csrf_exempt
def update_customer_prize(request):
    return cmn.db_update(request, CustomerPrizes)


# Customer choose prize insert
@csrf_exempt
# @is_authorize
@transaction.atomic
def cus_choose_prize(request):
    res = {"error": 0, "message": "", "data": {}}
    auth_flg = cmn.is_authorize(request)
    if not auth_flg:
        return auth_flg
    try:
        _prize_id = cmn.get_req_param(request, 'prize_id')
        lang = cmn.get_req_param(request, 'lang')
        _lang = str(lang).lower()
        _cus_id = request.session.get(const.SESSION_U_ID, None)

        if not _prize_id:
            res["error"] = eCODE.REQ_ID_REQUIRED
            res["message"] = get_msg_content('action_id_required', 'Prize')
        elif not _cus_id:
            res["error"] = eCODE.LGN_REQUIRED
            res["message"] = get_msg_content('login_required')
        else:
            # get prize info
            prize = Prizes.objects.get(pk=_prize_id, is_delete=const.ACTIVE_FLAG)
            _request_point = prize.request_point
            _curr_register = prize.current_register
            _quantity = prize.quantity

            # get current point form login user
            usr = Customers.objects.get(pk=_cus_id, is_delete=const.ACTIVE_FLAG)
            _usr_point = usr.current_point
            _usr_tt_point = usr.total_point

            # check equivalent point
            if _usr_point < _request_point:
                res["error"] = eCODE.INS_LACK_POINT
                res["message"] = get_msg_content('user_point_not_enough')
            elif _quantity <= _curr_register:
                res["error"] = eCODE.INS_EMPTY_STOCK
                res["message"] = get_msg_content('empty_stock')
            else:
                # request body
                _data = cmn.get_req_param(request, "data")
                # update remain point
                _remain_point = _usr_point - _request_point
                usr.current_point = _remain_point
                usr.save()

                # # random tracking no & check exist
                # num_length = 8
                # tracking_no_rd = random.randint(10 ** (num_length - 1), (10 ** num_length - 1))
                # check_number_info = {'tracking_no': tracking_no_rd}
                # check_exist_number = cmn.db_chk_dup_ins_ctm_no_pk(CustomerPrizes, check_number_info, ['tracking_no'])
                # while check_exist_number:
                #     tracking_no_rd = random.randint(10 ** (num_length - 1), (10 ** num_length - 1))
                #     check_number_info = {'tracking_no': tracking_no_rd}
                #     check_exist_number = cmn.db_chk_dup_ins_ctm_no_pk(CustomerPrizes, check_number_info, ['tracking_no'])

                # insert chosen prize info
                cus_prize = CustomerPrizes()
                cus_prize.customer = usr
                cus_prize.prize = prize
                # cus_prize.tracking_no = tracking_no_rd
                cus_prize.delivery_type = _data['delivery_type']
                cus_prize.name = _data['name']
                cus_prize.phone = _data['phone']
                cus_prize.email = _data['email']
                cus_prize.address = _data['address']
                cus_prize.status = _data['status']
                cus_prize.language = _lang
                _curr_time = get_curr_timezone()
                cus_prize.use_date = _curr_time
                cus_prize.save()

                # addition quantity of register item
                prize.current_register = _curr_register + 1
                prize.save()

                # config mail
                subject = 'ro ann style club: Order Confirmation for Prize' if _lang == 'en' else '割烹 櫓杏:訂單確認獎'
                from_email = const.DEFAULT_FROM_EMAIL
                to_email = [_data['email']]

                # prepare data for mail
                # tracking_no = tracking_no_rd
                prize_title = prize.title if _lang == 'en' else prize.title_cn
                delivery_type = 'Delivery' if _data['delivery_type'] == 1 else 'Get direction'
                # res_id = prize.restaurant
                res_obj = prize.restaurant
                shop_info_name = res_obj.name if _lang == 'en' else res_obj.name_cn
                shop_info_email = res_obj.email
                shop_info_phone = res_obj.phone
                shop_info_address = res_obj.address if _lang == 'en' else res_obj.address_cn

                plaintext = get_template('email/delivery_prize_en.txt' if _lang == 'en'
                                         else 'email/delivery_prize_cn.txt')
                html_form = get_template('email/delivery_prize_en.html' if _lang == 'en'
                                         else 'email/delivery_prize_cn.html')

                ctx = Context({
                    'cus_name': _data['name'],
                    'cus_phone': _data['phone'],
                    'cus_email': _data['email'],
                    'cus_address': _data['address'],
                    # 'tracking_no': tracking_no,
                    'title': prize_title,
                    'request_point': prize.request_point,
                    'link_address': prize.link_address,
                    'delivery_type': delivery_type,
                    'shop_info_name': shop_info_name,
                    'shop_info_email': shop_info_email,
                    'shop_info_phone': shop_info_phone,
                    'shop_info_address': shop_info_address,
                })

                text_content = plaintext.render(ctx)
                html_content = html_form.render(ctx)
                msg = EmailMultiAlternatives(subject, text_content, from_email=from_email, to=to_email)
                msg.attach_alternative(html_content, "text/html")
                msg.send()
                # response data
                res["error"] = eCODE.RES_SUCCESS
                res["message"] = get_msg_content('action_success', 'Insert')
                res["data"] = cmn.model_to_dict_filter(request, cus_prize)
                res["data"]['current_point'] = _remain_point
                res["data"]['total_point'] = _usr_tt_point
                res["data"]['remain_point'] = _remain_point
    except Exception as e:
        res["error"] = eCODE.RES_FAIL
        res["message"] = str(e)
        print(e)
    return JsonResponse(res)


# Delete Customer Prize
# params: prizeId
@csrf_exempt
def delete_customer_prize(request):
    # exec delete process
    return cmn.db_delete(request, CustomerPrizes)


# --------------------------------------------------------
# ======================= PRIZES =========================
# --------------------------------------------------------
# Get list all Prizes
@csrf_exempt
def get_prizes_list(request):
    return cmn.db_get_all_with_sort_asc(request, Prizes, 'request_point')


# Get Prize by id
@csrf_exempt
def get_prize_by_id(request):
    return cmn.db_get_by_id(request, Prizes)


# Get Prize by condition
@csrf_exempt
def get_prize_by_condition(request):
    return cmn.db_get_by_cond(request, Prizes)


# Insert Prize
@csrf_exempt
def insert_prize(request):
    return cmn.db_insert(request, Prizes)


# Update Prize
@csrf_exempt
def update_prize(request):
    return cmn.db_update(request, Prizes)


# Delete Prize
# params: pointId
@csrf_exempt
@is_authorize
def delete_prize(request):
    # check already in used
    check_info = {'prize__id': cmn.get_req_param(request, 'id')}
    check_res = cmn.db_chk_in_used(check_info, CustomerPrizes)
    if check_res:
        return JsonResponse(check_res)

    # exec delete process
    return cmn.db_delete(request, Prizes)
