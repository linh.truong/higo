from django.contrib.auth import authenticate, login
from django.contrib.auth import logout
from django.contrib.auth.decorators import login_required
from django.db import connection
from django.http import JsonResponse, HttpResponseRedirect
from django.shortcuts import render_to_response, render
from django.template import RequestContext
from django.views.decorators.csrf import csrf_exempt

import commonmodels.views as cmn
from commonmodels.models import Operations
from commonmodels.models import Restaurants
from utils import constants as const
from utils.constants import FILTER_LIST
from utils.message import get_msg_content
from utils.errcode import ErrCode as eCODE

# Forgot password template define
forgot_pwd_en = 'customers/recover_password_en.html'
forgot_pwd_cn = 'customers/recover_password_cn.html'


# --------------------------------------------------------------
# ======================= CMS-LOGIN ============================
# --------------------------------------------------------------


# CMS Login page
@login_required(login_url=const.CMS_LOGIN_URL)
def home_load(request):
    context = {}
    ope_id = request.session.get(const.SESSION_CMS_U_ID, '')
    if ope_id:
        user_info = get_cms_user_info(request, ope_id)
        if user_info:
            context['state'] = ''
            context['username'] = user_info
            return render(request, 'home.html', context)
    else:
        return HttpResponseRedirect(const.CMS_LOGIN_URL)


# CMS Home page
def login_do(request):
    state = ''
    context = {}
    if request.POST:

        username = cmn.get_req_param(request, 'username')
        password = cmn.get_req_param(request, 'password')

        if username and password:
            # synchronize with Customer
            ope_list = Operations.objects.filter(email=username, is_delete=const.ACTIVE_FLAG)

            if ope_list:
                # authorize login info
                current_user = authenticate(username=username, password=password)

                if current_user is not None:
                    if current_user.is_active:
                        # register login state
                        login(request, current_user)

                        _operator = ope_list[0]

                        # Get rest list by role
                        if _operator.type_id == const.CMS_ROLE_MENTE:
                            res_list = get_all_res()
                        else:
                            res_list = get_res_list_by_role(request, _operator.type_id)

                        # bind session info
                        request.session[const.SESSION_CMS_U_ID] = _operator.id
                        request.session[const.SESSION_CMS_U_MAP] = _operator.email
                        request.session[const.SESSION_CMS_U_FNAME] = _operator.first_name
                        request.session[const.SESSION_CMS_R_MAP] = _operator.type_id
                        request.session[const.SESSION_CMS_R_NAME_MAP] = _operator.get_type_id_display()
                        request.session[const.SESSION_CMS_CURR_RES_ID] = _operator.last_res_id
                        request.session[const.SESSION_CMS_RES_LIST] = res_list
                        request.session[const.SESSION_CMS_U_AUTH] = False

                        return HttpResponseRedirect('/cms/')
                    # return render(request,'home.html',{'next':'cms'
                    #                         ,'state':state
                    #                         ,'user':user_info
                    #                         ,'res_list':res_list
                    #                          ,'last_res_id':_operator.last_res_id})

                    else:
                        state = get_msg_content('login_acc_inactivated')
                else:
                    state = get_msg_content('login_fail')
            else:
                state = get_msg_content('login_fail')
        else:
            state = get_msg_content('login_u_pwd_required')

        context['next'] = ''
        context['state'] = state
        context['username'] = username
        context['password'] = password

        return render(request, 'login.html', context)
    context['next'] = ''
    context['state'] = state
    return render(request, 'login.html', context)


# Do logout CMS
def logout_ope_do(request):
    logout(request)
    context = {}
    state = "Please login..."
    context['state'] = state
    return HttpResponseRedirect(const.CMS_LOGIN_URL, context)


# Get user info
def get_cms_user_info(request, ope_id):
    user_info = {}
    try:
        ope_obj = Operations.objects.get(pk=ope_id, is_delete=const.ACTIVE_FLAG)
        if ope_obj is not None:
            # bind user info
            user_info["user_id"] = ope_obj.id
            user_info["username"] = ope_obj.email
            user_info["display_name"] = ope_obj.first_name
            user_info["role_name"] = ope_obj.get_type_id_display()
            user_info["role_id"] = ope_obj.type_id
            user_info["last_res_id"] = ope_obj.last_res_id

            # append cus info
            user_info.update(cmn.model_to_dict_with_path(request, ope_obj, FILTER_LIST))
    except Exception as e:
        print("Error" + str(e))
        logout(request)
        return HttpResponseRedirect(const.CMS_LOGIN_URL)
    return user_info


# Get restaurant list by operations
def get_res_list_by_role(request, type_id):
    cursor = None
    results = {}
    try:
        cursor = connection.cursor()
        _ret = cursor.callproc('search_ope_res', (type_id,))

        # fetch from qr to dict
        results = cmn.dictfetchall(cursor)
    except Exception as e:
        print("Error: " + str(e))
    finally:
        cursor.close()
    return results


# Get all restaurant
def get_all_res():
    res = []
    res_list = Restaurants.objects.filter(is_delete=const.ACTIVE_FLAG, )
    for res_obj in res_list:
        res.append({
            "id": res_obj.id, "name": res_obj.name
        })
    return res


# Update current res
@csrf_exempt
def update_curr_rest_id(request):
    res = {"error": 0, "message": "", "status_code": 0}
    try:
        if const.SESSION_CMS_U_ID in request.session:
            _new_res_id = cmn.get_req_param(request, "res_id")
            _ope_id = request.session[const.SESSION_CMS_U_ID]
            res_model = Restaurants.objects.get(pk=_new_res_id, is_delete=const.ACTIVE_FLAG)
            if res_model:
                ope_model = Operations.objects.get(pk=_ope_id, is_delete=const.ACTIVE_FLAG)
                if ope_model:
                    # update database
                    ope_model.last_res_id = _new_res_id
                    ope_model.save()

                    # update session
                    request.session[const.SESSION_CMS_CURR_RES_ID] = ope_model.last_res_id
                    request.session.save()
                    res["error"] = eCODE.RES_SUCCESS
                    res["message"] = "Update successfully!"
        else:
            res["error"] = eCODE.RES_FAIL
            res["message"] = 'Invalid session or session expired!'
    except Exception as e:
        print(e)
        res["error"] = eCODE.RES_FAIL
        res["message"] = str(e)
    return JsonResponse(res)


# --------------------------------------------------------------
# ======================= CUSTOMER       =======================
# --------------------------------------------------------------
# Activate new password for customer
def password_change(request):
    req_csrf = request.GET.get('csrf', '')
    lang = request.GET.get('lang', 'en')
    token = ''
    try:
        # cus = Customers.objects.get(recover_password = req_csrf);
        token = cmn.active_new_pwd(req_csrf)
        if token is None:
            return render(request, forgot_pwd_en if lang == 'en' else forgot_pwd_cn, {'token': token, 'page_code': 1})
    except Exception as e:
        print("Error" + str(e))
        return render(request, forgot_pwd_en if lang == 'en' else forgot_pwd_cn, {'token': token, 'page_code': 1})
    return render(request, forgot_pwd_en if lang == 'en' else forgot_pwd_cn, {'token': token, 'page_code': 0})


# --------------------------------------------------------------
# ======================= CUSTOMER       =======================
# --------------------------------------------------------------
# update new application password for customer
def do_update_password(request):
    token = ''
    lang = request.GET.get('lang')
    context = {}
    if request.method == 'POST':
        token = request.POST.get('csfrcus')

        if token is not None:
            password_new = request.POST.get('password')
            password_confirm = request.POST.get('password_confirm')
            check = True
            while check:
                if len(password_new) < 6 or len(password_new) > 12:
                    state = "Password New must contain 6 to 12 characters!"
                    break
                # elif not re.search("[a-z]", password_new):
                #     state = "Password New must contain at least one lowercase letter (a-z)!"
                #     break
                # elif not re.search("[0-9]", password_new):
                #     state = "Password New must contain at least one number (0-9)!"
                #     break
                # elif not re.search("[A-Z]", password_new):
                #     state = "Password New must contain at least one uppercase letter (A-Z)!"
                #     break
                else:
                    if password_new == password_confirm:
                        # call update function
                        if cmn.apy_new_pwd(token, password_new):
                            context['page_code'] = 2
                            return render_to_response('customers/recover_password_en.html',
                                                      RequestContext(request,
                                                                     context)) if lang == 'en' else render_to_response(
                                'customers/recover_password_cn.html',
                                RequestContext(request, context))
                        else:
                            context['page_code'] = 1
                            return render_to_response('customers/recover_password_en.html',
                                                      RequestContext(request,
                                                                     context)) if lang == 'en' else render_to_response(
                                'customers/recover_password_cn.html',
                                RequestContext(request, context))
                    else:
                        state = 'Confirm password is not match with new password.'
                    break
            context['state'] = state
            context['token'] = token
            context['password_new'] = password_new
            context['password_confirm'] = password_confirm
            context['page_code'] = 0
            return render(request, forgot_pwd_en if lang == 'en' else forgot_pwd_cn, context)
    context['token'] = token
    context['page_code'] = 1
    return render(request, forgot_pwd_en if lang == 'en' else forgot_pwd_cn, context)
