from django.conf.urls import url, include
from django.contrib.auth.views import login
from login.views import *

urlpatterns = [
    url(r'^customer/passwordchange/$', password_change),
    url(r'^customer/dopasswordchange/$', do_update_password, name='do_update_password'),

    # --------------------------------------------------------
    # ======================= CMS ============================
    # --------------------------------------------------------
    url(r'^cms/$', home_load, name='home_load'),
    url(r'^cms/login/$', login_do, name='do_login'),
    url(r'^cms/logout_ope/$', logout_ope_do, name='do_logout_ope'),
    url(r'^cms/core/update_curr_res/$', update_curr_rest_id, name='change_curr_res_by_id'),
]
