import datetime

from django import forms
from django.forms import ModelChoiceField

from commonmodels.models import Topics, TopicCategories
from utils import constants as const


# Make category list
class CategoryModelChoiceField(ModelChoiceField):
    def label_from_instance(self, obj):
        try:
            return obj.name
        except AttributeError:
            return obj.name


# Topic form
class TopicInfoForm(forms.ModelForm):
    category = CategoryModelChoiceField(queryset=None, empty_label=None, required=True,
                                        widget=forms.Select(attrs={'class': 'form-control'}))

    subject = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control', 'required': 'required'}),
                              max_length=500,
                              error_messages={'required': 'This field is required.'})

    subject_cn = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control', 'required': 'required'}),
                                 max_length=500,
                                 error_messages={'required': 'This field is required.'})

    body = forms.CharField(widget=forms.Textarea(attrs={'class': 'form-control', 'rows': 3}), max_length=4000,
                           required=True)

    body_cn = forms.CharField(widget=forms.Textarea(attrs={'class': 'form-control', 'rows': 3}), max_length=4000,
                              required=True)

    link_address = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control'}), max_length=500,
                                   required=False)

    open_date = forms.DateTimeField(required=True,
                                    widget=forms.TextInput(
                                        attrs={
                                            'value': datetime.datetime.now().strftime('%Y-%m-%d'),
                                            'class': 'form-control form-control-inline input-medium default-date-picker'
                                        }))

    close_date = forms.DateTimeField(required=True,
                                     widget=forms.TextInput(
                                        attrs={
                                            'value': datetime.datetime.now().strftime('%Y-%m-%d'),
                                            'class': 'form-control form-control-inline input-medium default-date-picker'
                                        }))
    is_mail_notify = forms.ChoiceField(choices=const.CMS_NOTIFY_STATUS, required=True,
                                       widget=forms.Select(attrs={'class': 'form-control'}))

    is_sms_notify = forms.ChoiceField(choices=const.CMS_NOTIFY_STATUS, required=True,
                                      widget=forms.Select(attrs={'class': 'form-control'}))

    is_app_notify = forms.ChoiceField(choices=const.CMS_NOTIFY_STATUS, required=True,
                                      widget=forms.Select(attrs={'class': 'form-control'}))

    hash_tags = forms.CharField(
        widget=forms.TextInput(attrs={'class': 'form-control'}), max_length=200, required=False)

    num_like = forms.CharField(
        widget=forms.TextInput(attrs={'class': 'form-control cms_form_number', 'maxlength': 11}), required=False)

    num_share = forms.CharField(
        widget=forms.TextInput(attrs={'class': 'form-control cms_form_number', 'maxlength': 11}), required=False)

    class Meta:
        model = Topics
        fields = (
            'category', 'subject', 'subject_cn', 'body', 'body_cn', 'link_address', 'open_date',
            'close_date', 'hash_tags', 'num_like', 'num_share', 'is_mail_notify', 'is_sms_notify', 'is_app_notify')

    def __init__(self, *args, **kwargs):
        super(TopicInfoForm, self).__init__(*args, **kwargs)
        self.fields['category'].queryset = TopicCategories.objects.filter(is_delete=const.ACTIVE_FLAG)
        self.fields['hash_tags'].initial = '#news '
        self.fields['num_like'].initial = 0
        self.fields['num_share'].initial = 0


# Topic Category  form
class TopicCatInfoForm(forms.ModelForm):
    name = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control', 'required': 'required'}),
                           max_length=50,
                           error_messages={'required': 'This field is required.'})

    name_cn = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control', 'required': 'required'}),
                              max_length=50,
                              error_messages={'required': 'This field is required.'})

    class Meta:
        model = TopicCategories
        fields = ('name', 'name_cn')

    def __init__(self, *args, **kwargs):
        super(TopicCatInfoForm, self).__init__(*args, **kwargs)
