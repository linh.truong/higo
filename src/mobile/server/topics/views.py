from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.http import HttpResponseRedirect
from django.shortcuts import render

import commonmodels.views_cms as cms
from commonmodels.models import Topics, TopicCategories
from topics.forms import TopicInfoForm, TopicCatInfoForm
from utils import constants as const
from utils import utility as util


# URL define
list_url = '/cms/topics/list'
add_url = '/cms/topics/add'
edit_url = '/cms/topics/edit/'


# ---------------------------------------------------------
# ======================= TOPICS ==========================
# ---------------------------------------------------------
# Load list topic page
@login_required(login_url=const.CMS_LOGIN_URL)
def load_list_topics_page(request):
    return render(request, 'topics/list.html')


# get topics list by json
@login_required(login_url=const.CMS_LOGIN_URL)
def get_list_topics_as_json(request):
    _res_id = request.session[const.SESSION_CMS_CURR_RES_ID]
    mapping = {
        'curr_res_id': _res_id,
        'search': ['subject', 'subject_cn', 'category__name', 'category__name_cn', 'open_date', 'close_date'],
        'sort': {
            0: 'id',
            1: 'category__name',
            2: 'subject',
            3: 'subject_cn',
            4: 'open_date',
            5: 'close_date',
        },
        'map': ['category_id', 'category_name']
    }
    return cms.get_list_display(request, Topics, mapping)


# Load topic add page
@login_required(login_url=const.CMS_LOGIN_URL)
def add_topics(request):
    context = {}
    model = Topics
    if request.method == 'POST':
        try:
            form_info = TopicInfoForm(request.POST)

            if form_info.is_valid():

                # check duplicate
                check_topic = cms.chk_dup_ins(model, form_info, ['number', 'subject', 'subject_cn'])
                if check_topic:
                    messages.error(request, check_topic['errors'])
                    context['form_info'] = form_info
                    context['request_method'] = request.method
                    return render(request, 'topics/add.html', context)

                topic = form_info.save(commit=False)

                topic.restaurant_id = request.session[const.SESSION_CMS_CURR_RES_ID]

                # Get & Set image_path
                image_file = request.FILES.get('image_path', False)
                if image_file:
                    topic.image_path.delete(False)
                    topic.image_path.save(image_file.name, image_file, False)

                topic.save()
                return HttpResponseRedirect(list_url)
            else:
                print("Err" + str(form_info.errors))
                messages.add_message(request, messages.ERROR, form_info.errors, extra_tags='add_topics')
        except Exception as e:
            print("error" + str(e))
            messages.add_message(request, messages.ERROR, e, extra_tags='add_topics')
        return render(request, 'error.html')
    else:

        form_info = TopicInfoForm()
        # Set return context`
        context['form_info'] = form_info
        context['request_method'] = request.method

        return render(request, 'topics/add.html', context)


# Edit topics
@login_required(login_url=const.CMS_LOGIN_URL)
def edit_topics(request, topics_id):
    return cms.action_insert_or_update(request, Topics, TopicInfoForm, 'topics', topics_id)


# Delete topics
@login_required(login_url=const.CMS_LOGIN_URL)
def delete_topics(request, topics_id):
    return cms.action_insert_or_update(request, Topics, TopicInfoForm, 'topics', topics_id)


# ---------------------------------------------------------
# =======================TOPICS CATEGORIES=================
# ---------------------------------------------------------
# Load list topics category page
@login_required(login_url=const.CMS_LOGIN_URL)
def load_list_topics_cat_page(request):
    return render(request, 'topics_cat/list.html')


# get topics list by json
@login_required(login_url=const.CMS_LOGIN_URL)
def get_list_topics_cat_as_json(request):
    mapping = {
        'search': ['name', 'name_cn'],
        'sort': {
            0: 'id',
            1: 'name',
            2: 'name_cn',
        },
    }
    return cms.get_list_display(request, TopicCategories, mapping)


# Load topic category add page
@login_required(login_url=const.CMS_LOGIN_URL)
def add_topics_cat(request):
    context = {}
    model = TopicCategories
    if request.method == 'POST':
        try:
            form_info = TopicCatInfoForm(request.POST)

            if form_info.is_valid():

                # check duplicate
                check_topic_cat = cms.chk_dup_ins(model, form_info, ['name', 'name_cn'])
                if check_topic_cat:
                    messages.error(request, check_topic_cat['errors'])
                    context['form_info'] = form_info
                    context['request_method'] = request.method
                    return render(request, 'topics_cat/add.html', context)

                food_cat = form_info.save(commit=False)

                food_cat.save()
                return HttpResponseRedirect('/cms/topics_cat/list')
            else:
                print("Err" + str(form_info.errors))
                messages.add_message(request, messages.ERROR, form_info.errors, extra_tags='add_topics_cat')
        except Exception as e:
            print("error" + str(e))
            messages.add_message(request, messages.ERROR, e, extra_tags='add_topics_cat')
        return render(request, 'error.html')
    else:

        form_info = TopicCatInfoForm()
        # Set return context
        context['form_info'] = form_info
        context['request_method'] = request.method

        return render(request, 'topics_cat/add.html', context)


# Edit topics cat
@login_required(login_url=const.CMS_LOGIN_URL)
def edit_topics_cat(request, topics_cat_id):
    return cms.action_insert_or_update(request, TopicCategories, TopicCatInfoForm, 'topics_cat', topics_cat_id)


# Delete topics cate
@login_required(login_url=const.CMS_LOGIN_URL)
def delete_topics_cat(request, topics_cat_id):
    return cms.action_insert_or_update(request, TopicCategories, TopicCatInfoForm, 'topics_cat', topics_cat_id)