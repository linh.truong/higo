"""toyosu URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.10/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url

import topics.views as services

urlpatterns = [
    # ---------------------------------------------------------
    # ======================= TOPICS ==========================
    # ---------------------------------------------------------

    # List
    url(r'^cms/topics/list/$', services.load_list_topics_page, name='topics_list'),
    url(r'^topics_list_json/$', services.get_list_topics_as_json, name='get_topics_list_as_json'),

    # Insert
    url(r'^cms/topics/add/$', services.add_topics, name='add_topics'),

    # edit
    url(r'^cms/topics/edit/([\d\D]+)/$', services.edit_topics, name='edit_topics'),

    # delete
    url(r'^cms/topics/delete/([\d\D]+)/$', services.delete_topics, name='delete_topics'),

    # ---------------------------------------------------------
    # =======================TOPICS CATEGORIES=================
    # ---------------------------------------------------------
    # List
    url(r'^cms/topics_cat/list/$', services.load_list_topics_cat_page, name='topics_cat_list'),
    url(r'^topics_cat_list_json/$', services.get_list_topics_cat_as_json, name='get_topics_cat_list_as_json'),

    # Insert
    url(r'^cms/topics_cat/add/$', services.add_topics_cat, name='add_topics_cat'),

    # edit
    url(r'^cms/topics_cat/edit/([\d\D]+)/$', services.edit_topics_cat, name='edit_topics_cat'),

    # delete
    url(r'^cms/topics_cat/delete/([\d\D]+)/$', services.delete_topics_cat, name='delete_topics_cat'),
]
