Dear {{ cus_name }},

Thanks for using our service!
Your prize selected have been accepted; details can be found below.

■ Prize info
Tracking No.:  {{ tracking_no }}
Name:  {{ title }}
Request point:  {{ request_point }} pt
Product detail: {{ link_address }}
Prize type : {{ delivery_type }}
* Delivery will be made in 3-10 working days.

------------------------------------
■ Your details
Name:  {{ cus_name }}
Phone:  {{ cus_phone }}
Email:  {{ cus_email }}
Address: {{ cus_address }}
------------------------------------

Your sincerely,

If you have any questions regarding this prize, please feel free to contacts us.

＋―――――――――――――――＋
　 ro ann style
    selections
＋―――――――――――――――＋
