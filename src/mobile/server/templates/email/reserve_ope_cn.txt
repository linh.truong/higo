New booking is registered, please check contents

■ Room info
Name:  {{ res_name }}
■ Room info
Room type:  {{ reserve_type }}
Reservation day:  {{ reserve_date }}
Reservation time:  {{ reserve_time }}
Number of pax:  {{ num_pax }}
Number of adult:  {{ num_adult }}
Number of child:  {{ num_child }}
Customer comment:  {{ comment }}

------------------------------------
■ Customer details
Name:  {{ name }}
Phone:  {{ phone }}
Email:  {{ email }}
------------------------------------
