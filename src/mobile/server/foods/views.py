from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.http import HttpResponseRedirect, HttpResponse, HttpResponseNotFound
from django.shortcuts import render

import commonmodels.views_cms as cms
from foods.forms import FoodInfoForm, FoodCatInfoForm
from utils import constants as const
from commonmodels.models import Foods, FoodCategories


# URL define
list_url = '/cms/foods/list'
add_url = '/cms/foods/add'
edit_url = '/cms/foods/edit/'


# ---------------------------------------------------------
# ======================= FOODS ==========================
# ---------------------------------------------------------

# Load food list page
@login_required(login_url=const.CMS_LOGIN_URL)
def load_food_list(request):
    return render(request, 'foods/list.html')


# Get food list as json
@login_required(login_url=const.CMS_LOGIN_URL)
def get_food_list_as_json(request):
    _res_id = request.session[const.SESSION_CMS_CURR_RES_ID]
    mapping = {
        'curr_res_id': _res_id,
        'search': ['name', 'name_cn', 'category__name', 'category__name_cn', 'price', 'hash_tags'],
        'sort': {
            0: 'id',
            1: 'category__name',
            2: 'name',
            3: 'name_cn',
            4: 'price'
        },
        'map': ['category_id', 'category_name']
    }
    return cms.get_list_display(request, Foods, mapping)


# # Load food add page
# @login_required(login_url=const.CMS_LOGIN_URL)
# def add_foods(request):
#     return cms.action_insert_or_update(request, Foods, FoodInfoForm, 'foods')


# add foods
@login_required(login_url=const.CMS_LOGIN_URL)
def add_foods(request):
    context = {}
    model = Foods
    if request.method == 'POST':
        try:
            form_info = FoodInfoForm(request.POST)

            if form_info.is_valid():

                # check duplicate
                check_food = cms.chk_dup_ins(model, form_info, ['name', 'name_cn'])
                if check_food:
                    messages.error(request, check_food['errors'])
                    context['form_info'] = form_info
                    context['request_method'] = request.method
                    return render(request, 'foods/add.html', context)

                food = form_info.save(commit=False)

                food.restaurant_id = request.session[const.SESSION_CMS_CURR_RES_ID]
                _week_days = ','.join(request.POST.getlist('week_days'))

                food.week_days = _week_days

                # Get & Set image_path
                image_file = request.FILES.get('image_path', False)
                if image_file:
                    food.image_path.delete(False)
                    food.image_path.save(image_file.name, image_file, False)

                food.save()

                return HttpResponseRedirect(list_url)
            else:
                print("Err" + str(form_info.errors))
                messages.add_message(request, messages.ERROR, form_info.errors, extra_tags='add_foods')
        except Exception as e:
            print("error" + str(e))
            messages.add_message(request, messages.ERROR, e, extra_tags='add_foods')
        return render(request, 'error.html')
    else:

        form_info = FoodInfoForm()
        # Set return context
        context['form_info'] = form_info
        context['request_method'] = request.method

        return render(request, 'foods/add.html', context)


# Edit foods
@login_required(login_url=const.CMS_LOGIN_URL)
def edit_foods(request, foods_id):
    return cms.action_insert_or_update(request, Foods, FoodInfoForm, 'foods', foods_id)


# Delete foods
@login_required(login_url=const.CMS_LOGIN_URL)
def delete_foods(request, foods_id):
    return cms.action_insert_or_update(request, Foods, FoodInfoForm, 'foods', foods_id)


# ---------------------------------------------------------
# =======================FOODS CATEGORIES=================
# ---------------------------------------------------------
# Load food list page
@login_required(login_url=const.CMS_LOGIN_URL)
def load_list_foods_cat_page(request):
    return render(request, 'foods_cat/list.html')


# Get food list as json
@login_required(login_url=const.CMS_LOGIN_URL)
def get_list_foods_cat_as_json(request):
    mapping = {
        'search': ['name', 'name_cn'],
        'sort': {
            0: 'id',
            1: 'name',
            2: 'name_cn'
        },
    }
    return cms.get_list_display(request, FoodCategories, mapping)


# Load food add page
@login_required(login_url=const.CMS_LOGIN_URL)
def add_foods_cat(request):
    context = {}
    model = FoodCategories
    if request.method == 'POST':
        try:
            form_info = FoodCatInfoForm(request.POST)

            if form_info.is_valid():

                # check duplicate
                check_food_cat = cms.chk_dup_ins(model, form_info, ['name', 'name_cn'])
                if check_food_cat:
                    messages.error(request, check_food_cat['errors'])
                    context['form_info'] = form_info
                    context['request_method'] = request.method
                    return render(request, 'foods_cat/add.html', context)

                food_cat = form_info.save(commit=False)

                food_cat.save()
                return HttpResponseRedirect('/cms/foods_cat/list')
            else:
                print("Err" + str(form_info.errors))
                messages.add_message(request, messages.ERROR, form_info.errors, extra_tags='add_foods_cat')
        except Exception as e:
            print("error" + str(e))
            messages.add_message(request, messages.ERROR, e, extra_tags='add_foods_cat')
        return render(request, 'error.html')
    else:

        form_info = FoodCatInfoForm()
        # Set return context
        context['form_info'] = form_info
        context['request_method'] = request.method

        return render(request, 'foods_cat/add.html', context)


# Edit foods cate
@login_required(login_url=const.CMS_LOGIN_URL)
def edit_foods_cat(request, foods_cat_id):
    return cms.action_insert_or_update(request, FoodCategories, FoodCatInfoForm, 'foods_cat', foods_cat_id)


# Delete foods cate
@login_required(login_url=const.CMS_LOGIN_URL)
def delete_foods_cat(request, foods_cat_id):
    return cms.action_insert_or_update(request, FoodCategories, FoodCatInfoForm, 'foods_cat', foods_cat_id)


# # Check dup name
# @csrf_token
# @login_required(login_url=const.CMS_LOGIN_URL)
# def check_duplicate_food_name(request):
#     food = Foods.objects.get(pk=id())
#     formFood = FoodInfoForm(request.POST, instance=food)
#     check_dup_food = cms.chk_dup_upd(Foods, formFood, food.id, ['name', 'name_cn'])
#     if check_dup_food:
#         # messages.error(request, 'Food name existed. Please input another name')
#         # return render(request, 'foods/edit.html', check_dup_food)
#         return HttpResponse(json.dumps(array), content_type="application/json") 
#     else:
#         return HttpResponseNotFound()