"""toyosu URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.10/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url, include
import foods.views as services

urlpatterns = [
    # ---------------------------------------------------------
    # ======================= FOODS ==========================
    # ---------------------------------------------------------
    # List
    url(r'^cms/foods/list/$', services.load_food_list, name='foods_list'),
    url(r'^foods_list_json/$', services.get_food_list_as_json, name='get_foods_list_as_json'),
    # Insert
    url(r'^cms/foods/add/$', services.add_foods, name='add_foods'),
    # edit
    url(r'^cms/foods/edit/([\d\D]+)/$', services.edit_foods, name='edit_foods'),
    # delete
    url(r'^cms/foods/delete/([\d\D]+)/$', services.delete_foods, name='delete_foods'),
    # ---------------------------------------------------------
    # =================== FOODS CATEGORY=======================
    # ---------------------------------------------------------
    # List     
    url(r'^cms/foods_cat/list/$', services.load_list_foods_cat_page, name='foods_cat_list'),
    url(r'^foods_cat_list_json/$', services.get_list_foods_cat_as_json, name='get_foods_cat_list_as_json'),
    # Insert
    url(r'^cms/foods_cat/add/$', services.add_foods_cat, name='add_foods_cat'),
    # edit
    url(r'^cms/foods_cat/edit/([\d\D]+)/$', services.edit_foods_cat, name='edit_foods_cat'),
    # delete
    url(r'^cms/foods_cat/delete/([\d\D]+)/$', services.delete_foods_cat, name='delete_foods_cat'),
    # check duplicate
    # url(r'^cms/foods/check_dup_name/$', services.check_duplicate_food_name, name='check_duplicate_food_name'),
]
