from django import forms
from django.core.validators import MaxValueValidator
from django.forms import ModelChoiceField

from commonmodels.models import Foods, FoodCategories, FoodTypes
from utils import constants as const


# Make category list
class CategoryModelChoiceField(ModelChoiceField):
    def label_from_instance(self, obj):
        try:
            return obj.name
        except AttributeError:
            return obj.name


class TypeModelChoiceField(ModelChoiceField):
    def label_from_instance(self, obj):
        try:
            return obj.name
        except AttributeError:
            return obj.name


# Food add new form
class FoodInfoForm(forms.ModelForm):
    category = CategoryModelChoiceField(
        queryset=None, empty_label=None, required=True, widget=forms.Select(attrs={'class': 'form-control'}))

    type = TypeModelChoiceField(
        queryset=None, empty_label=None, required=True, widget=forms.Select(attrs={'class': 'form-control'}))

    name = forms.CharField(
        widget=forms.TextInput(attrs={'class': 'form-control', 'required': 'required'}),
        max_length=50, error_messages={'required': 'This field is required.'})

    name_cn = forms.CharField(
        widget=forms.TextInput(attrs={'class': 'form-control', 'required': 'required'}),
        max_length=50, error_messages={'required': 'This field is required.'})

    price = forms.DecimalField(
        widget=forms.TextInput(attrs={'class': 'form-control cms_form_decimal', 'required': 'required',
                                      'maxlength': 19}),
        error_messages={'required': 'This field is required.'})

    description = forms.CharField(
        widget=forms.Textarea(attrs={'class': 'form-control', 'rows': 3}), max_length=2000, required=False)

    description_cn = forms.CharField(
        widget=forms.Textarea(attrs={'class': 'form-control', 'rows': 3}), max_length=2000, required=False)

    start_time = forms.TimeField(
        required=True,
        widget=forms.TextInput(
            attrs={'pattern': '([01]?[0-9]|2[0-3])(:[0-5][0-9])',
                   'class': 'form-control form-control-inline input-medium cms-time-picker'}))

    end_time = forms.TimeField(
        required=True,
        widget=forms.TextInput(
            attrs={'pattern': '([01]?[0-9]|2[0-3])(:[0-5][0-9])',
                   'class': 'form-control form-control-inline input-medium cms-time-picker'}))

    week_days = forms.MultipleChoiceField(
        choices=const.CMS_WEEK_DAYS, required=False, widget=forms.CheckboxSelectMultiple(
            attrs={'class': 'cms_week_days'}))

    current_order = forms.IntegerField(
        widget=forms.TextInput(attrs={'class': 'form-control cms_form_number', 'maxlength': 11}), required=False)

    quantity_per_day = forms.IntegerField(
        widget=forms.TextInput(attrs={'class': 'form-control cms_form_number', 'maxlength': 11}), required=False)

    hash_tags = forms.CharField(
        widget=forms.TextInput(attrs={'class': 'form-control'}), max_length=200, required=False)

    class Meta:
        model = Foods
        fields = (
            'category', 'type', 'name', 'name_cn', 'price', 'description', 'description_cn',
            'start_time', 'end_time', 'current_order', 'quantity_per_day', 'hash_tags', 'week_days'
        )

    def __init__(self, *args, **kwargs):
        super(FoodInfoForm, self).__init__(*args, **kwargs)
        self.fields['category'].queryset = FoodCategories.objects.filter(is_delete=const.ACTIVE_FLAG)
        self.fields['type'].queryset = FoodTypes.objects.filter(is_delete=const.ACTIVE_FLAG)
        self.fields['hash_tags'].initial = '#food '
        self.fields['week_days'].initial = [1,2,3,4,5,6,7]
        self.fields['current_order'].initial = 0
        self.fields['quantity_per_day'].initial = 1000
        self.fields['start_time'].initial = '00:00'
        self.fields['end_time'].initial = '23:30'


# Food Category form
class FoodCatInfoForm(forms.ModelForm):
    name = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control', 'required': 'required'}),
                           max_length=50,
                           error_messages={'required': 'This field is required.'})

    name_cn = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control', 'required': 'required'}),
                              max_length=50,
                              error_messages={'required': 'This field is required.'})

    class Meta:
        model = FoodCategories
        fields = ('name', 'name_cn')

    def __init__(self, *args, **kwargs):
        super(FoodCatInfoForm, self).__init__(*args, **kwargs)
