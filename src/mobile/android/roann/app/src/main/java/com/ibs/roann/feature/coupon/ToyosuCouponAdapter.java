package com.ibs.roann.feature.coupon;

import android.view.LayoutInflater;
import android.widget.ImageView;
import android.widget.TextView;

import com.ibs.roann.R;
import com.ibs.roann.feature.ToyosuApplication;
import com.ibs.roann.feature.ToyosuConstant;
import com.ibs.roann.feature.ToyosuUtils;
import com.ibs.roann.model.Coupon;
import com.ibs.roann.view.fetchable.RecycleAdapter;
import com.ibs.roann.view.fetchable.RecycleInterface;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.display.RoundedBitmapDisplayer;

import java.util.ArrayList;

import core.util.SingleTouch;

/**
 * Created by tuanle on 1/9/17.
 */

public class ToyosuCouponAdapter extends RecycleAdapter<Coupon> {

    private DisplayImageOptions options;

    public ToyosuCouponAdapter(LayoutInflater inflater, ArrayList<Coupon> items, RecycleInterface<Coupon> listener, SingleTouch singleTouch) {
        super(inflater, items, listener, singleTouch);
        options = new DisplayImageOptions.Builder()
                .displayer(new RoundedBitmapDisplayer(25)) // default
                .build();
    }

    @Override
    public int getItemViewType(int position) {
        if (items.get(position).id == -1)
            return FOOTER_TYPE;
        else if (items.get(position).id == -2)
            return HEADER_TYPE;
        return ITEM_TYPE;
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    @Override
    public int getRealItemCount() {
        int footer_count = 0;
        for (Coupon topic : items)
            if (topic.id == -1)
                footer_count++;
        return getItemCount() - footer_count;
    }

    @Override
    protected int getHeaderLayoutResource() {
        return R.layout.toyosu_coupon_header_item;
    }

    @Override
    protected int getItemLayoutResource() {
        return R.layout.toyosu_coupon_item;
    }

    @Override
    protected int getFooterLayoutResource() {
        return R.layout.toyosu_item_footer;
    }

    @Override
    protected int getInidcatorLayoutResource() {
        return 0;
    }

    @Override
    protected void bindFooterView(FooterViewHolder<Coupon> holder, Coupon data, int position) {

    }

    @Override
    protected void bindIndicatorView(IndicatorViewHolder<Coupon> holder, Coupon data, int position) {

    }

    @Override
    protected void bindHeaderView(HeaderViewHolder<Coupon> holder, Coupon data, int position) {
        ((TextView) holder.findViewById(R.id.coupon_header_item_tv_title)).setText(ToyosuApplication.getActiveActivity().getString(R.string.coupon_lb_title_header));
    }

    @Override
    protected void bindItemView(ItemViewHolder<Coupon> holder, Coupon data, int position) {
        ToyosuApplication.getImageLoader().displayImage(data.image_path, (ImageView) holder.findViewById(R.id.coupon_item_img_coupon), options);
        ((TextView) holder.findViewById(R.id.coupon_item_tv_remain)).setText(ToyosuApplication.getActiveActivity().getString(R.string.coupon_lb_remain_day, String.valueOf(data.remain_day)));
        ((TextView) holder.findViewById(R.id.coupon_item_tv_title)).setText(data.getTitle());
        ((TextView) holder.findViewById(R.id.coupon_item_tv_expired)).setText(ToyosuApplication.getActiveActivity().getString(R.string.coupon_lb_expired_date, ToyosuUtils.formatDate(ToyosuUtils.formatDateString(data.to_date), ToyosuConstant.DATE_FORMAT_LONG)));
        ((TextView) holder.findViewById(R.id.coupon_item_tv_start)).setText(ToyosuApplication.getActiveActivity().getString(R.string.coupon_lb_start_time, data.start_time));
        ((TextView) holder.findViewById(R.id.coupon_item_tv_end)).setText(ToyosuApplication.getActiveActivity().getString(R.string.coupon_lb_end_time, data.end_time));
    }
}
