package com.ibs.roann.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Provider implements Serializable {

    @SerializedName("id")
    public String id;

    @SerializedName("name")
    public String name;

}
