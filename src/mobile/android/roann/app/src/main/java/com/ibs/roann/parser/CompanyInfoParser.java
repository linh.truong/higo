package com.ibs.roann.parser;

import com.google.gson.Gson;
import com.ibs.roann.model.CompanyInformation;
import com.ibs.roann.model.api.CompanyInfoResult;

import org.json.JSONObject;

import core.base.BaseResult;
import core.util.Constant;

/**
 * Created by tuanle on 1/3/17.
 */

public class CompanyInfoParser extends ToyosuBaseParser {
    @Override
    public BaseResult parseData(String content) {
        CompanyInfoResult result = new CompanyInfoResult();

        try {
            JSONObject json = new JSONObject(content);
            result.setBackEndStatus(json.optInt(RESPONSE_ERROR_TAG));
            if (result.getBackEndStatus() == 0) {
                Gson gson = new Gson();
                JSONObject data = json.getJSONObject(RESPONSE_DATA_TAG);
                result.setCompany((gson.fromJson(data.toString(), CompanyInformation.class)));
                result.setStatus(Constant.StatusCode.OK);
            } else {
                result.setMessage(json.optString(RESPONSE_MESSAGE_TAG));
            }
            return result;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
}
