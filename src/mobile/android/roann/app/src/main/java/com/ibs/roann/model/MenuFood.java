package com.ibs.roann.model;

import com.google.gson.annotations.SerializedName;
import com.ibs.roann.feature.ToyosuConstant;

import java.io.Serializable;

import core.data.UserDataManager;
import core.util.DataKey;

/**
 * Created by tuanle on 1/5/17.
 */

public class MenuFood implements Serializable, RelatedItem {

    @SerializedName("restaurant")
    public int restaurant_id;

    @SerializedName("name_cn")
    public String name_cn;

    @SerializedName("name")
    public String name;

    @SerializedName("hash_tags")
    public String hash_tags;

    @SerializedName("week_days")
    public String week_days;

    @SerializedName("image_height")
    public int image_height;

    @SerializedName("description")
    public String description;

    @SerializedName("description_cn")
    public String description_cn;

    @SerializedName("type")
    public int type;

    @SerializedName("price")
    public double price;

    @SerializedName("image_path")
    public String image_path;

    @SerializedName("id")
    public int id = -1;

    @SerializedName("start_time")
    public String start_time;

    @SerializedName("is_delete")
    public int is_delete;

    @SerializedName("update_by")
    public String update_by;

    @SerializedName("end_time")
    public String end_time;

    @SerializedName("image_width")
    public int image_width;

    @SerializedName("category")
    public int category_id;

    @SerializedName("current_order")
    public int current_order;

    @SerializedName("quantity_per_day")
    public int quantity_per_day;

    public int getPrice() {
        return (int) price;
    }

    @Override
    public String getName() {
        return UserDataManager.getInstance().getString(DataKey.DEVICE_LANGUAGE).equals(ToyosuConstant.DEVICE_LANGUAGE_CHINESE) ? name_cn : name;
    }

    @Override
    public String getImagePath() {
        return image_path;
    }

    public String getDescription() {
        return UserDataManager.getInstance().getString(DataKey.DEVICE_LANGUAGE).equals(ToyosuConstant.DEVICE_LANGUAGE_CHINESE) ? description_cn : description;
    }
}
