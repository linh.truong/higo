package com.ibs.roann.model.api;

import com.ibs.roann.feature.ToyosuBaseResult;
import com.ibs.roann.model.Area;

import java.util.ArrayList;

/**
 * Created by tuanle on 1/3/17.
 */

public class AreasResult extends ToyosuBaseResult {

    private int total;

    private ArrayList<Area> areas;

    public AreasResult() {
        this.total = 0;
        this.areas = new ArrayList<>();
    }

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }

    public ArrayList<Area> getAreas() {
        return areas;
    }

    public void setAreas(ArrayList<Area> areas) {
        this.areas = areas;
    }
}
