package com.ibs.roann.feature.authentication;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import com.ibs.roann.R;
import com.ibs.roann.feature.ToyosuBaseFragment;
import com.ibs.roann.model.api.RecoverPasswordParam;
import com.ibs.roann.model.api.RecoverPasswordResult;

import butterknife.BindView;
import core.base.BaseResult;
import core.data.UserDataManager;
import core.dialog.GeneralDialog;
import core.util.DataKey;
import core.util.RequestTarget;
import core.util.Utils;

/**
 * Created by tuanle on 1/18/17.
 */

public class ToyosuPasswordRecoveryScreen extends ToyosuBaseFragment implements GeneralDialog.ConfirmListener {

    private static final int PASSWORD_RECOVERED_DIALOG = 9005;

    @BindView(R.id.password_recovery_tv_email_title)
    TextView password_recovery_tv_email_title;

    @BindView(R.id.password_recovery_tv_email_error)
    TextView password_recovery_tv_email_error;

    @BindView(R.id.password_recovery_ed_email)
    EditText password_recovery_ed_email;

    public static ToyosuPasswordRecoveryScreen getInstance() {
        return new ToyosuPasswordRecoveryScreen();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.toyosu_password_recovery_screen, container, false);
    }

    @Override
    public void refreshViewDataLanguage() {
    }

    @Override
    public void onInitializeViewData() {
        registerSingleAction(R.id.password_recovery_rl_submit,
                R.id.password_recovery_img_submit,
                R.id.password_recovery_rp_submit,
                R.id.password_recovery_tv_submit);

    }

    @Override
    public void onBaseFree() {
        unregisterSingleAction(R.id.password_recovery_rl_submit,
                R.id.password_recovery_img_submit,
                R.id.password_recovery_rp_submit,
                R.id.password_recovery_tv_submit);
        super.onBaseFree();
    }

    @Override
    public void reloadData() {

    }

    @Override
    public void onSingleClick(View v) {
        switch (v.getId()) {
            case R.id.password_recovery_rl_submit:
            case R.id.password_recovery_img_submit:
            case R.id.password_recovery_rp_submit:
            case R.id.password_recovery_tv_submit:
                if (validated()) {
                    requestRecoverPassword();
                }
                break;
        }
    }

    private void requestRecoverPassword() {
        makeRequest(getUniqueTag(), true, new RecoverPasswordParam(password_recovery_ed_email.getText().toString(), UserDataManager.getInstance().getString(DataKey.DEVICE_LANGUAGE)), this, RequestTarget.RECOVER_PASSWORD);
    }

    private boolean validated() {
        boolean result = true;

        if (Utils.isEmpty(password_recovery_ed_email.getText().toString().trim())) {
            showInputEmailError(getString(R.string.customer_lb_recover_password_required));
            result = false;
        } else {
            hideInputEmailError();
        }
        return result;
    }

    private void showInputEmailError(String error) {
        password_recovery_tv_email_error.setVisibility(View.VISIBLE);
        password_recovery_tv_email_title.setVisibility(View.GONE);
        password_recovery_tv_email_error.setText(error);
    }

    private void hideInputEmailError() {
        password_recovery_tv_email_error.setVisibility(View.GONE);
        password_recovery_tv_email_title.setVisibility(View.VISIBLE);
    }

    @Override
    public void onResultSuccess(BaseResult result) {
        if (result instanceof RecoverPasswordResult) {
            showAlertDialog(getActiveActivity(), PASSWORD_RECOVERED_DIALOG,
                    getGeneralDialogLayoutResource(),
                    R.mipmap.ic_app_logo,
                    getString(R.string.app_name),
                    getString(R.string.customer_lb_recover_password_success),
                    getString(R.string.common_ok),
                    null, this);
        }
    }

    @Override
    public void onConfirmed(int id, Object onWhat) {
        if (id == PASSWORD_RECOVERED_DIALOG) {
            finish();
        }
    }
}
