package com.ibs.roann.view;

import android.content.Context;
import android.graphics.Bitmap;
import android.util.AttributeSet;
import android.widget.ImageView;

public class ToyosuRecycledImageView extends ImageView {

    private Bitmap bitmap;

    public ToyosuRecycledImageView(Context context) {
        super(context);
    }

    public ToyosuRecycledImageView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    public void setImageBitmap(Bitmap bm) {
        super.setImageBitmap(bm);
        if (bitmap != null && !bitmap.isRecycled()) bitmap.recycle();
        this.bitmap = bm;
    }

    @Override
    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        recycle();
    }

    public void recycle() {
        setBackgroundResource(0);
        setBackground(null);
        setImageResource(0);
        setImageDrawable(null);
        if (bitmap != null && !bitmap.isRecycled()) {
            bitmap.recycle();
        }
        System.gc();
    }
}
