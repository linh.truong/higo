package com.ibs.roann.model;

/**
 * Created by tuanle on 2/2/17.
 */

public interface RelatedItem {

    int TYPE_TOPIC = 3;
    int TYPE_FOOD = 2;
    int TYPE_COUPON = 1;

    String getName();

    String getImagePath();

}
