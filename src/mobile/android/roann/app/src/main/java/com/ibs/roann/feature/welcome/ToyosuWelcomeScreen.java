package com.ibs.roann.feature.welcome;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;

import com.ibs.roann.R;
import com.ibs.roann.feature.ToyosuBaseActivity;
import com.ibs.roann.feature.ToyosuMainActivity;
import com.ibs.roann.model.api.SettingsParam;
import com.ibs.roann.model.api.SettingsResult;
import com.ibs.roann.view.ToyosuRecycledImageView;

import butterknife.BindView;
import core.base.BaseResult;
import core.connection.WebServiceRequester;
import core.data.UserDataManager;
import core.util.Constant;
import core.util.RequestTarget;


/**
 *
 */

public class ToyosuWelcomeScreen extends ToyosuBaseActivity implements WebServiceRequester.WebServiceResultHandler{

    @BindView(R.id.welcome_img)
    ToyosuRecycledImageView welcome_img;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.toyosu_welcome_screen);
    }

    @Override
    public void onBaseCreate() {

    }

    @Override
    public void onDeepLinking(Intent data) {

    }

    @Override
    public void onNotification(Intent data) {

    }

    @Override
    public void onInitializeViewData() {
        requestDataSettings();
    }

    private void requestDataSettings() {
        makeRequest(getClass().getName(), false, new SettingsParam(0), this, RequestTarget.SETTINGS);
    }

    @Override
    public void onResultSuccess(BaseResult result) {
        if (result instanceof SettingsResult) {
            UserDataManager.getInstance().setSettings(((SettingsResult) result).getSettings());
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    finish();
                    Intent intent = getIntent();
                    intent.setClass(ToyosuWelcomeScreen.this, ToyosuMainActivity.class);
                    startActivity(intent);
                }
            }, 2000);
        }
    }

    @Override
    public void onResultFail(BaseResult result) {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                finish();
                Intent intent = getIntent();
                intent.setClass(ToyosuWelcomeScreen.this, ToyosuMainActivity.class);
                startActivity(intent);
            }
        }, 2000);
    }

    @Override
    public void onFail(RequestTarget target, String error, Constant.StatusCode code) {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                finish();
                Intent intent = getIntent();
                intent.setClass(ToyosuWelcomeScreen.this, ToyosuMainActivity.class);
                startActivity(intent);
            }
        }, 2000);
    }

    @Override
    public void onBaseResume() {

    }

    @Override
    public void onBaseFree() {
        if (welcome_img != null)
            welcome_img.recycle();
    }

    @Override
    public void onSingleClick(View v) {

    }

    @Override
    public int getEnterInAnimation() {
        return 0;
    }

    @Override
    public int getBackOutAnimation() {
        return 0;
    }
}
