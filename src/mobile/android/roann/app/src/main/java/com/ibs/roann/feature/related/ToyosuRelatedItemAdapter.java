package com.ibs.roann.feature.related;

import android.view.LayoutInflater;
import android.widget.ImageView;
import android.widget.TextView;

import com.ibs.roann.R;
import com.ibs.roann.feature.ToyosuApplication;
import com.ibs.roann.model.RelatedItem;
import com.ibs.roann.view.fetchable.RecycleAdapter;
import com.ibs.roann.view.fetchable.RecycleInterface;

import java.util.ArrayList;

import core.util.SingleTouch;

/**
 * Created by tuanle on 2/2/17.
 */

public class ToyosuRelatedItemAdapter extends RecycleAdapter<RelatedItem> {

    public ToyosuRelatedItemAdapter(LayoutInflater inflater, ArrayList<RelatedItem> items, RecycleInterface<RelatedItem> listener, SingleTouch singleTouch) {
        super(inflater, items, listener, singleTouch);
    }

    @Override
    public int getRealItemCount() {
        return getItemCount();
    }

    @Override
    public int getItemViewType(int position) {
        return ITEM_TYPE;
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    @Override
    protected int getHeaderLayoutResource() {
        return 0;
    }

    @Override
    protected int getItemLayoutResource() {
        return R.layout.toyosu_related_item;
    }

    @Override
    protected int getFooterLayoutResource() {
        return 0;
    }

    @Override
    protected int getInidcatorLayoutResource() {
        return 0;
    }

    @Override
    protected void bindFooterView(FooterViewHolder<RelatedItem> holder, RelatedItem data, int position) {

    }

    @Override
    protected void bindIndicatorView(IndicatorViewHolder<RelatedItem> holder, RelatedItem data, int position) {

    }

    @Override
    protected void bindHeaderView(HeaderViewHolder<RelatedItem> holder, RelatedItem data, int position) {

    }

    @Override
    protected void bindItemView(ItemViewHolder<RelatedItem> holder, RelatedItem data, int position) {
        ToyosuApplication.getImageLoader().displayImage(data.getImagePath(), ((ImageView) holder.findViewById(R.id.related_item_img)));
        ((TextView) holder.findViewById(R.id.related_item_tv_name)).setText(data.getName());
    }
}
