package com.ibs.roann.model.api;

import com.ibs.roann.feature.ToyosuBaseParam;

import org.json.JSONObject;

/**
 * Created by tuanle on 1/3/17.
 */

public class UsePrizeParam extends ToyosuBaseParam {

    private int customerId;
    private int id;
    private String auth_token;

    public UsePrizeParam(int customerId, int id, String auth_token) {
        this.customerId = customerId;
        this.id = id;
        this.auth_token = auth_token;
    }

    @Override
    public byte[] makeRequestBody() {
        try {
            return new JSONObject().put(PARAM_CUSTOMER_ID_TAG, String.valueOf(customerId)).put(PARAM_PRIZE_ID_TAG, String.valueOf(id)).put(PARAM_AUTH_TOKEN_TAG, auth_token).toString().getBytes();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return new byte[0];
    }
}

