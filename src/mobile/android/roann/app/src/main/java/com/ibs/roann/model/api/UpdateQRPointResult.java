package com.ibs.roann.model.api;

import com.ibs.roann.feature.ToyosuBaseResult;

/**
 * Created by tule on 3/3/17.
 */

public class UpdateQRPointResult extends ToyosuBaseResult {

    private int current_point;

    private int total_point;

    private int res_point;

    public int getCurrent_point() {
        return current_point;
    }

    public void setCurrent_point(int current_point) {
        this.current_point = current_point;
    }

    public int getTotal_point() {
        return total_point;
    }

    public void setTotal_point(int total_point) {
        this.total_point = total_point;
    }

    public int getRes_point() {
        return res_point;
    }

    public void setRes_point(int res_point) {
        this.res_point = res_point;
    }
}
