package com.ibs.roann.model.api;

import com.ibs.roann.feature.ToyosuBaseResult;
import com.ibs.roann.model.Restaurant;

import java.util.ArrayList;

/**
 * Created by tuanle on 1/20/17.
 */

public class RestaurantsResult extends ToyosuBaseResult {

    private int total;

    private ArrayList<Restaurant> restaurants;

    public RestaurantsResult() {
        restaurants = new ArrayList<>();
    }

    public ArrayList<Restaurant> getRestaurants() {
        return restaurants;
    }

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }
}
