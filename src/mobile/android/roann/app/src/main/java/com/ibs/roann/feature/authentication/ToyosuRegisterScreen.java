package com.ibs.roann.feature.authentication;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.method.ScrollingMovementMethod;
import android.util.Patterns;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.ibs.roann.R;
import com.ibs.roann.feature.ToyosuApplication;
import com.ibs.roann.feature.ToyosuBaseFragment;
import com.ibs.roann.feature.ToyosuConstant;
import com.ibs.roann.model.Area;
import com.ibs.roann.model.api.AreasParam;
import com.ibs.roann.model.api.AreasResult;
import com.ibs.roann.model.api.RegisterCustomerParam;
import com.ibs.roann.model.api.RegisterCustomerResult;
import com.ibs.roann.model.api.SettingByCodeParam;
import com.ibs.roann.model.api.SettingByCodeResult;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import butterknife.BindView;
import core.base.BaseResult;
import core.connection.QueueServiceRequester;
import core.connection.queue.QueueElement;
import core.data.UserDataManager;
import core.dialog.GeneralDialog;
import core.util.Constant;
import core.util.DataKey;
import core.util.RequestTarget;
import core.util.Utils;
import icepick.State;

import static com.ibs.roann.feature.ToyosuConstant.MAX_AGE;
import static com.ibs.roann.feature.ToyosuConstant.MIN_AGE;

/**
 * Created by tuanle on 1/18/17.
 */

public class ToyosuRegisterScreen extends ToyosuBaseFragment implements CompoundButton.OnCheckedChangeListener, QueueServiceRequester.QueueServiceListener, AdapterView.OnItemSelectedListener, GeneralDialog.ConfirmListener, DatePickerDialog.OnDateSetListener {

    private static final int REGISTERED_SUCCESS_DIALOG = 9007;
    private static final String DATE_PICKER_DIALOG_TAG = DatePickerDialog.class.getSimpleName();

    @BindView(R.id.register_img_gender_unknown)
    View register_img_gender_unknown;

    @BindView(R.id.register_tv_gender_unknown)
    TextView register_tv_gender_unknown;

    @BindView(R.id.register_img_gender_male)
    View register_img_gender_male;

    @BindView(R.id.register_tv_gender_male)
    TextView register_tv_gender_male;

    @BindView(R.id.register_img_gender_female)
    View register_img_gender_female;

    @BindView(R.id.register_tv_gender_female)
    TextView register_tv_gender_female;

    @BindView(R.id.register_ed_name)
    EditText register_ed_name;

    @BindView(R.id.register_tv_name_title)
    TextView register_tv_name_title;

    @BindView(R.id.register_tv_name_error)
    TextView register_tv_name_error;

    @BindView(R.id.register_tv_email_title)
    TextView register_tv_email_title;

    @BindView(R.id.register_tv_email_error)
    TextView register_tv_email_error;

    @BindView(R.id.register_ed_email)
    EditText register_ed_email;

    @BindView(R.id.register_tv_phone_title)
    TextView register_tv_phone_title;

    @BindView(R.id.register_tv_phone_error)
    TextView register_tv_phone_error;

    @BindView(R.id.register_ed_phone)
    EditText register_ed_phone;

    @BindView(R.id.register_tv_password_title)
    TextView register_tv_password_title;

    @BindView(R.id.register_tv_password_error)
    TextView register_tv_password_error;

    @BindView(R.id.register_ed_password)
    EditText register_ed_password;

    @BindView(R.id.register_tv_confirm_password_title)
    TextView register_tv_confirm_password_title;

    @BindView(R.id.register_tv_confirm_password_error)
    TextView register_tv_confirm_password_error;

    @BindView(R.id.register_ed_confirm_password)
    EditText register_ed_confirm_password;

    @BindView(R.id.register_tv_birthday_title)
    TextView register_tv_birthday_title;

    @BindView(R.id.register_tv_birthday_error)
    TextView register_tv_birthday_error;

    @BindView(R.id.register_cb_terms)
    CheckBox register_cb_terms;

    @BindView(R.id.register_tv_submit)
    TextView register_tv_submit;

    @BindView(R.id.register_img_submit)
    ImageView register_img_submit;

    @BindView(R.id.register_tv_privacy)
    TextView register_tv_privacy;

    @BindView(R.id.register_sp_area)
    Spinner register_sp_area;

    @BindView(R.id.register_ll_birthday)
    LinearLayout register_ll_birthday;
    @BindView(R.id.register_tv_birthday)
    TextView register_tv_birthday;

    @State
    int gender;

    @State
    int area;

    @State
    int age;
    long birthDay = -1;

    private SimpleDateFormat request_format;
    private SimpleDateFormat format;

    private DatePickerDialog date_dialog;

    private View.OnTouchListener insideScroller;

    public static ToyosuRegisterScreen getInstance() {
        return new ToyosuRegisterScreen();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.toyosu_register_screen, container, false);
    }

    @Override
    public void refreshViewDataLanguage() {

    }

    @Override
    public void onBaseCreate() {
        super.onBaseCreate();
        request_format = new SimpleDateFormat(ToyosuConstant.DATE_FORMAT_RESPONSE);
        format = new SimpleDateFormat(ToyosuConstant.DATE_FORMAT_SHORT);
        insideScroller = new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (v.getId() == R.id.register_tv_privacy) {
                    v.getParent().requestDisallowInterceptTouchEvent(true);
                    switch (event.getAction() & MotionEvent.ACTION_MASK) {
                        case MotionEvent.ACTION_UP:
                            v.getParent().requestDisallowInterceptTouchEvent(false);
                            break;
                    }
                }
                return false;
            }
        };

    }

    @Override
    public void onInitializeViewData() {
        super.onInitializeViewData();

        refreshDatePicker();
        setGenderUnknownSelected();
        register_img_submit.setEnabled(false);
        register_cb_terms.setOnCheckedChangeListener(this);
        registerSingleAction(register_img_gender_female,
                register_tv_gender_female,
                register_img_gender_unknown,
                register_tv_gender_unknown,
                register_img_gender_male,
                register_tv_gender_male,
                register_ll_birthday,
                register_tv_submit);
        register_sp_area.setOnItemSelectedListener(this);
        register_tv_privacy.setMovementMethod(new ScrollingMovementMethod());
        register_tv_privacy.setOnTouchListener(insideScroller);
        QueueServiceRequester.registerListener(this);
        makeQueueRequest(getUniqueTag(), QueueElement.Type.RETRY, new SettingByCodeParam(SettingByCodeParam.SETTING_CODE_PRIVACY), RequestTarget.SETTING_BY_CODE);
        makeQueueRequest(getUniqueTag(), QueueElement.Type.RETRY, new AreasParam(), RequestTarget.AREAS);

    }

    @Override
    public void reloadData() {

    }

    @Override
    public void onBaseFree() {
        unregisterSingleAction(register_img_gender_female,
                register_tv_gender_female,
                register_img_gender_unknown,
                register_tv_gender_unknown,
                register_img_gender_male,
                register_tv_gender_male,
                register_ll_birthday,
                register_tv_submit);
        QueueServiceRequester.removeListener(this);
        super.onBaseFree();
    }

    private void setGenderMaleSelected() {
        register_img_gender_female.setSelected(false);
        register_tv_gender_female.setSelected(false);
        register_img_gender_unknown.setSelected(false);
        register_tv_gender_unknown.setSelected(false);
        register_img_gender_male.setSelected(true);
        register_tv_gender_male.setSelected(true);
        gender = 0;
    }

    private void setGenderFemaleSelected() {
        register_img_gender_unknown.setSelected(false);
        register_tv_gender_unknown.setSelected(false);
        register_img_gender_male.setSelected(false);
        register_tv_gender_male.setSelected(false);
        register_img_gender_female.setSelected(true);
        register_tv_gender_female.setSelected(true);
        gender = 1;
    }

    private void setGenderUnknownSelected() {
        register_img_gender_male.setSelected(false);
        register_tv_gender_male.setSelected(false);
        register_img_gender_female.setSelected(false);
        register_tv_gender_female.setSelected(false);
        register_img_gender_unknown.setSelected(true);
        register_tv_gender_unknown.setSelected(true);
        gender = 2;
    }

    @Override
    public void onSingleClick(View v) {
        switch (v.getId()) {
            case R.id.register_img_gender_male:
            case R.id.register_tv_gender_male:
                setGenderMaleSelected();
                break;
            case R.id.register_img_gender_female:
            case R.id.register_tv_gender_female:
                setGenderFemaleSelected();
                break;
            case R.id.register_img_gender_unknown:
            case R.id.register_tv_gender_unknown:
                setGenderUnknownSelected();
                break;
            case R.id.register_ll_birthday:
                if (date_dialog != null)
                    date_dialog.show(getActivity().getFragmentManager(), DATE_PICKER_DIALOG_TAG);
                break;
            case R.id.register_tv_submit:
                if (validated()) {
                    requestRegisterCustomer();
                }
                break;
        }
    }

    private void requestRegisterCustomer() {

        makeRequest(getUniqueTag(), true, new RegisterCustomerParam(
                UserDataManager.getInstance().getInt(DataKey.RESTAURANT_ID),
                ToyosuConstant.BLANK, // address
                ToyosuConstant.BLANK, // gps
                register_ed_name.getText().toString().trim(),
                register_ed_email.getText().toString().trim(),
                register_ed_phone.getText().toString().trim(),
                gender,
                age,
                register_ed_password.getText().toString().trim(),
                area,
                request_format.format(new Date(birthDay))), this, RequestTarget.REGISTER_CUSTOMER);
    }


    private void showInputBirthdayError() {
        register_tv_birthday_error.setVisibility(View.VISIBLE);
        register_tv_birthday_title.setVisibility(View.GONE);
    }

    private void hideInputBirthdayError() {
        register_tv_birthday_error.setVisibility(View.GONE);
        register_tv_birthday_title.setVisibility(View.VISIBLE);
    }

    private void showInputNameError() {
        register_tv_name_error.setVisibility(View.VISIBLE);
        register_tv_name_title.setVisibility(View.GONE);
    }

    private void hideInputNameError() {
        register_tv_name_error.setVisibility(View.GONE);
        register_tv_name_title.setVisibility(View.VISIBLE);
    }

    private void showInputPhoneEmpty() {
        register_tv_phone_error.setVisibility(View.VISIBLE);
        register_tv_phone_error.setText(getString(R.string.customer_lb_phone_required));
        register_tv_phone_title.setVisibility(View.GONE);
    }

    private void showInputPhoneError() {
        register_tv_phone_error.setVisibility(View.VISIBLE);
        register_tv_phone_error.setText(getString(R.string.customer_lb_phone_invalid));
        register_tv_phone_title.setVisibility(View.GONE);
    }

    private void hideInputPhoneError() {
        register_tv_phone_error.setVisibility(View.GONE);
        register_tv_phone_title.setVisibility(View.VISIBLE);
    }

    private void showInputEmailError(String error) {
        register_tv_email_error.setVisibility(View.VISIBLE);
        register_tv_email_title.setVisibility(View.GONE);
        register_tv_email_error.setText(error);
    }

    private void hideInputEmailError() {
        register_tv_email_error.setVisibility(View.GONE);
        register_tv_email_title.setVisibility(View.VISIBLE);
    }

    private void showInputPasswordError(String error) {
        register_tv_password_error.setVisibility(View.VISIBLE);
        register_tv_password_title.setVisibility(View.GONE);
        register_tv_password_error.setText(error);
    }

    private void hideInputPasswordError() {
        register_tv_password_error.setVisibility(View.GONE);
        register_tv_password_title.setVisibility(View.VISIBLE);
    }

    private void showInputConfirmPasswordError(String error) {
        register_tv_confirm_password_error.setVisibility(View.VISIBLE);
        register_tv_confirm_password_title.setVisibility(View.GONE);
        register_tv_confirm_password_error.setText(error);
    }

    private void hideInputConfirmPasswordError() {
        register_tv_confirm_password_error.setVisibility(View.GONE);
        register_tv_confirm_password_title.setVisibility(View.VISIBLE);
    }


    private boolean validated() {
        boolean result = true;
        if (!register_cb_terms.isChecked())
            return false;

        String name = register_ed_name.getText().toString().trim();
        if (Utils.isEmpty(name) || !name.contains(" ")) {
            showInputNameError();
            result = false;
        } else {
            hideInputNameError();
        }
        if (Utils.isEmpty(register_ed_email.getText().toString().trim())) {
            showInputEmailError(getString(R.string.customer_lb_email_required));
            result = false;
        } else {
            if (!Patterns.EMAIL_ADDRESS.matcher(register_ed_email.getText().toString().trim()).matches()) {
                showInputEmailError(getString(R.string.customer_lb_email_invalid));
                result = false;
            } else
                hideInputEmailError();
        }

        if (Utils.isEmpty(register_ed_phone.getText().toString().trim())) {
            showInputPhoneEmpty();
            result = false;
        } else {
            Pattern testPattern= Pattern.compile("^[0-9]{8,12}$");
            Matcher testPhone= testPattern.matcher(register_ed_phone.getText().toString().trim());

            if(!testPhone.matches())
            {
                showInputPhoneError();
            }else{
                hideInputPhoneError();
            }
        }

        String password = register_ed_password.getText().toString().trim();
        if (Utils.isEmpty(password)) {
            showInputPasswordError(getString(R.string.customer_register_pw_required));
            result = false;
        } else {
            if (!(password.toLowerCase().matches(ToyosuConstant.PASSWORD_NUMBER_REGEX) && password.toLowerCase().matches(ToyosuConstant.PASSWORD_CHARACTER_REGEX))) {
                showInputPasswordError(getString(R.string.customer_lb_password_complexity));
                result = false;
            } else {
                hideInputPasswordError();
            }
        }
        String confirm = register_ed_confirm_password.getText().toString().trim();
        if (!confirm.equals(password)) {
            showInputConfirmPasswordError(getString(R.string.customer_register_pw_not_match));
            result = false;
        } else
            hideInputConfirmPasswordError();

        if (birthDay == -1) {
            showInputBirthdayError();
            result = false;
        } else
            hideInputBirthdayError();

        return result;
    }

    private void refreshDatePicker() {
        Calendar max = Calendar.getInstance();
        max.set(Calendar.YEAR, max.get(Calendar.YEAR) - MIN_AGE);
        max.set(Calendar.MONTH, Calendar.DECEMBER);
        max.set(Calendar.DAY_OF_MONTH, 31);
        max.set(Calendar.HOUR_OF_DAY, 0);
        max.set(Calendar.MINUTE, 0);
        max.set(Calendar.SECOND, 0);
        max.set(Calendar.MILLISECOND, 0);

        Calendar min = Calendar.getInstance();
        min.set(Calendar.YEAR, max.get(Calendar.YEAR) - MAX_AGE);
        min.set(Calendar.MONTH, Calendar.JANUARY);
        min.set(Calendar.DAY_OF_MONTH, 1);
        min.set(Calendar.HOUR_OF_DAY, 0);
        min.set(Calendar.MINUTE, 0);
        min.set(Calendar.SECOND, 0);
        min.set(Calendar.MILLISECOND, 0);


        date_dialog = DatePickerDialog.newInstance(this, max.get(Calendar.YEAR), max.get(Calendar.MONTH), max.get(Calendar.DAY_OF_MONTH));
        date_dialog.setMaxDate(max);
        date_dialog.setMinDate(min);
        date_dialog.initialize(this, max.get(Calendar.YEAR), max.get(Calendar.MONTH), max.get(Calendar.DAY_OF_MONTH));

        if (birthDay <= 0) {
            Calendar selected_date = max;
            if (birthDay == -1 || birthDay != selected_date.getTimeInMillis()){
                birthDay = selected_date.getTimeInMillis();
            }
        }
        Calendar selected = Calendar.getInstance();
        selected.setTimeInMillis(birthDay);
        date_dialog.initialize(this, selected.get(Calendar.YEAR), selected.get(Calendar.MONTH), selected.get(Calendar.DAY_OF_MONTH));
        register_tv_birthday.setText(format.format(new Date(birthDay)));
    }

    @Override
    public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {
        Calendar selected = Calendar.getInstance();
        selected.set(Calendar.YEAR, year);
        selected.set(Calendar.MONTH, monthOfYear);
        selected.set(Calendar.DAY_OF_MONTH, dayOfMonth);
        selected.set(Calendar.HOUR_OF_DAY, 0);
        selected.set(Calendar.MINUTE, 0);
        selected.set(Calendar.SECOND, 0);
        selected.set(Calendar.MILLISECOND, 0);
        if (birthDay == -1 || birthDay != selected.getTimeInMillis()){
            birthDay = selected.getTimeInMillis();
            Calendar now = Calendar.getInstance();
            age = now.get(Calendar.YEAR) - selected.get(Calendar.YEAR);
        }
        register_tv_birthday.setText(format.format(new Date(birthDay)));

    }

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        register_img_submit.setEnabled(isChecked);
    }

    @Override
    public void onStartQueue(QueueElement element) {
        showLoadingDialog(getActiveActivity(), getLoadingDialogLayoutResource(), ToyosuApplication.getContext().getString(R.string.common_loading));
    }

    @Override
    public void onEndQueue() {

    }

    @Override
    public void onFinishQueue() {
        closeLoadingDialog();
    }

    @Override
    public void onBlockQueue(QueueElement element) {

    }

    @Override
    public void onStopQueue(ArrayList<QueueElement> remain) {

    }

    @Override
    public void onResultSuccess(BaseResult result) {
        if (result instanceof RegisterCustomerResult) {
            showAlertDialog(getActiveActivity(), REGISTERED_SUCCESS_DIALOG, getGeneralDialogLayoutResource(), R.mipmap.ic_app_logo, getString(R.string.app_name), getString(R.string.customer_msg_register_successfully), getString(R.string.common_ok), null, this);
        }
    }

    @Override
    public void onResultSuccess(BaseResult result, QueueElement element) {
        if (result instanceof SettingByCodeResult) {
            register_tv_privacy.setText(((SettingByCodeResult) result).getLegal().getValue());
        } else if (result instanceof AreasResult) {
            register_sp_area.setAdapter(new ToyosuAreaAdapter(((AreasResult) result).getAreas()));
        }
    }

    @Override
    public void onResultFail(BaseResult result, QueueElement element) {
        super.onResultFail(result);
    }

    @Override
    public void onFail(RequestTarget target, String error, Constant.StatusCode code, QueueElement element) {
        if (!(code == Constant.StatusCode.ERR_QUEUE_IN_REQUEST))
            super.onFail(target, error, code);
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        Object item = parent.getItemAtPosition(position);
        if (item instanceof Area)
            area = ((Area) item).id;
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    @Override
    public void onConfirmed(int id, Object onWhat) {
        if (id == REGISTERED_SUCCESS_DIALOG)
            finish();
    }

}
