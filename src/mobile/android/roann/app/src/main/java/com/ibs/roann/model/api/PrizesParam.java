package com.ibs.roann.model.api;

import com.ibs.roann.feature.ToyosuBaseParam;

import org.json.JSONObject;

/**
 * Created by tuanle on 1/3/17.
 */

public class PrizesParam extends ToyosuBaseParam {

    private int offset;

    public PrizesParam(int offset) {
        this.offset = offset;
    }

    @Override
    public byte[] makeRequestBody() {
        try {
            return new JSONObject().put(PARAM_LIMIT_TAG, String.valueOf(LIST_LIMIT)).put(PARAM_OFFSET_TAG, String.valueOf(offset)).toString().getBytes();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return new byte[0];
    }
}

