package com.ibs.roann.view;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.TextView;

/**
 * Created by tuanle on 1/5/17.
 */

public class ToyosuTextView extends TextView {

    public ToyosuTextView(Context context) {
        super(context);
    }

    public ToyosuTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public ToyosuTextView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }
}
