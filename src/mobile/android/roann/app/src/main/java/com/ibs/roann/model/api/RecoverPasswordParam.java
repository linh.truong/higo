package com.ibs.roann.model.api;

import com.ibs.roann.feature.ToyosuBaseParam;

import org.json.JSONObject;

/**
 * Created by tuanle on 1/3/17.
 */

public class RecoverPasswordParam extends ToyosuBaseParam {

    private static final String PARAMS_EMAIL_TAG = "cus_info";
    private static final String PARAMS_LANGUAGE_TAG = "lang";

    private String email;
    private String language;

    public RecoverPasswordParam(String email, String language) {
        this.email = email;
        this.language = language;
    }

    @Override
    public byte[] makeRequestBody() {
        try {
            return new JSONObject().put(PARAMS_EMAIL_TAG, email).put(PARAMS_LANGUAGE_TAG, language).toString().getBytes();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return new byte[0];
    }
}

