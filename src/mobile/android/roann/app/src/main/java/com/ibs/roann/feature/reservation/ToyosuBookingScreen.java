package com.ibs.roann.feature.reservation;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.Editable;
import android.text.Html;
import android.util.Log;
import android.util.Patterns;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.ibs.roann.R;
import com.ibs.roann.feature.ToyosuApplication;
import com.ibs.roann.feature.ToyosuBaseFragment;
import com.ibs.roann.feature.ToyosuBaseResult;
import com.ibs.roann.feature.ToyosuConstant;
import com.ibs.roann.model.Additional;
import com.ibs.roann.model.Booking;
import com.ibs.roann.model.HourOpen;
import com.ibs.roann.model.LegalInformation;
import com.ibs.roann.model.Provider;
import com.ibs.roann.model.Restaurant;
import com.ibs.roann.model.Service;
import com.ibs.roann.model.User;
import com.ibs.roann.model.WorkingDate;
import com.ibs.roann.model.api.HourOpenParam;
import com.ibs.roann.model.api.HourOpenResult;
import com.ibs.roann.model.api.InsertReservationParam;
import com.ibs.roann.model.api.InsertReservationResult;
import com.ibs.roann.model.api.RestaurantInfoBookParam;
import com.ibs.roann.model.api.RestaurantInfoBookResult;
import com.ibs.roann.model.api.RestaurantsParam;
import com.ibs.roann.model.api.RestaurantsResult;
import com.ibs.roann.model.api.SettingByCodeParam;
import com.ibs.roann.model.api.SettingByCodeResult;
import com.ibs.roann.view.ToyosuBookingTextDataWatcher;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;
import com.wdullaer.materialdatetimepicker.time.TimePickerDialog;
import com.wdullaer.materialdatetimepicker.time.Timepoint;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import butterknife.BindView;
import core.base.BaseResult;
import core.connection.QueueServiceRequester;
import core.connection.queue.QueueElement;
import core.data.UserDataManager;
import core.dialog.GeneralDialog;
import core.util.Constant;
import core.util.DataKey;
import core.util.RequestTarget;
import core.util.Utils;
import icepick.State;

import static com.ibs.roann.feature.ToyosuConstant.TIME_FRAME_BOOKING;

/**
 * Created by tuanle on 1/4/17.
 */

public class ToyosuBookingScreen extends ToyosuBaseFragment implements DatePickerDialog.OnDateSetListener, TimePickerDialog.OnTimeSetListener, GeneralDialog.ConfirmListener, AdapterView.OnItemSelectedListener, QueueServiceRequester.QueueServiceListener {

    private static final String DATE_PICKER_DIALOG_TAG = DatePickerDialog.class.getSimpleName();
    private static final String TIME_PICKER_DIALOG_TAG = TimePickerDialog.class.getSimpleName();
    private static final String BOOKING = "booking";
    private static final String RESTAURANT = "restaurant";
    private static final int BOOKING_CONFIRMATION_DIALOG = 9003;

    @State
    Booking booking;
    @State
    Restaurant restaurant;
    @BindView(R.id.booking_tv_name_title)
    TextView booking_tv_name_title;
    @BindView(R.id.booking_tv_name_error)
    TextView booking_tv_name_error;
    @BindView(R.id.booking_ed_name)
    EditText booking_ed_name;
    @BindView(R.id.booking_tv_email_title)
    TextView booking_tv_email_title;
    @BindView(R.id.booking_tv_email_error)
    TextView booking_tv_email_error;
    @BindView(R.id.booking_ed_email)
    EditText booking_ed_email;
    @BindView(R.id.booking_tv_phone_title)
    TextView booking_tv_phone_title;
    @BindView(R.id.booking_tv_phone_error)
    TextView booking_tv_phone_error;
    @BindView(R.id.booking_ed_phone)
    EditText booking_ed_phone;
    @BindView(R.id.booking_ll_reserve_date)
    LinearLayout booking_ll_reserve_date;
    @BindView(R.id.booking_tv_reserve_date_title)
    TextView booking_tv_reserve_date_title;
    @BindView(R.id.booking_tv_reserve_date)
    TextView booking_tv_reserve_date;
    @BindView(R.id.booking_ll_reserve_time)
    LinearLayout booking_ll_reserve_time;
    @BindView(R.id.booking_tv_reserve_time)
    TextView booking_tv_reserve_time;
    @BindView(R.id.booking_tv_reserve_time_title)
    TextView booking_tv_reserve_time_title;
    @BindView(R.id.booking_tv_pax_info)
    TextView booking_tv_pax_info;
    @BindView(R.id.booking_tv_adult_count_title)
    TextView booking_tv_adult_count_title;
    @BindView(R.id.booking_ed_adult_count)
    EditText booking_ed_adult_count;
    @BindView(R.id.booking_tv_children_count_title)
    TextView booking_tv_children_count_title;
    @BindView(R.id.booking_ed_children_count)
    EditText booking_ed_children_count;
    @BindView(R.id.booking_tv_student_count_title)
    TextView booking_tv_student_count_title;
    @BindView(R.id.booking_ed_student_count)
    EditText booking_ed_student_count;
    @BindView(R.id.booking_tv_comment_title)
    TextView booking_tv_comment_title;
    @BindView(R.id.booking_ed_comment)
    EditText booking_ed_comment;
    @BindView(R.id.booking_tv_submit)
    TextView booking_tv_submit;
    @BindView(R.id.booking_tv_adult_count_error)
    TextView booking_tv_adult_count_error;
    @BindView(R.id.booking_tv_reserve_time_error)
    TextView booking_tv_reserve_time_error;
    @BindView(R.id.booking_sp_shop)
    Spinner booking_sp_shop;
    @BindView(R.id.booking_sp_services)
    Spinner booking_sp_services;
    @BindView(R.id.booking_sp_providers)
    Spinner booking_sp_providers;
    @BindView(R.id.booking_tv_reserve_shop_title)
    TextView booking_tv_reserve_shop_title;
    @BindView(R.id.booking_tv_reserve_services_title)
    TextView booking_tv_reserve_services_title;
    @BindView(R.id.booking_tv_reserve_providers_title)
    TextView booking_tv_reserve_providers_title;
    @BindView(R.id.booking_tv_children_count_error)
    TextView booking_tv_children_count_error;
    @BindView(R.id.booking_tv_student_count_error)
    TextView booking_tv_student_count_error;
    @BindView(R.id.booking_tv_tc)
    TextView booking_tv_tc;

    @State
    User user;

    @State
    ArrayList<Additional> additionalList;

    @State
    ArrayList<Service> serviceList;

    @State
    ArrayList<Provider> providerList;

    @State
    ArrayList<WorkingDate> workingDateList;

    @State
    ArrayList<HourOpen> hourOpenList;

    @State
    Service service;

    private ToyosuRestaurantAdapter adapterRestaurant;
    private ToyosuServiceAdapter adapterService;
    private ToyosuProviderAdapter adapterProvider;

    private SimpleDateFormat format;
    private SimpleDateFormat request_format;
    private SimpleDateFormat request_format_time;
    private DatePickerDialog date_dialog;
    private TimePickerDialog time_dialog;

    private ToyosuBookingTextDataWatcher booking_ed_phone_watcher = new ToyosuBookingTextDataWatcher() {
        @Override
        public void afterTextChanged(Editable s) {
            booking.phone = s.toString().trim();
        }
    };
    private ToyosuBookingTextDataWatcher booking_ed_name_watcher = new ToyosuBookingTextDataWatcher() {
        @Override
        public void afterTextChanged(Editable s) {
            booking.name = s.toString().trim();
        }
    };
    private ToyosuBookingTextDataWatcher booking_ed_email_watcher = new ToyosuBookingTextDataWatcher() {
        @Override
        public void afterTextChanged(Editable s) {
            booking.email = s.toString().trim();
        }
    };
    private ToyosuBookingTextDataWatcher booking_ed_adult_count_watcher = new ToyosuBookingTextDataWatcher() {
        @Override
        public void afterTextChanged(Editable s) {
            if (!Utils.isEmpty(s.toString().trim()))
                booking.number_of_adult = Integer.valueOf(s.toString().trim());
        }
    };
    private ToyosuBookingTextDataWatcher booking_ed_student_count_watcher = new ToyosuBookingTextDataWatcher() {
        @Override
        public void afterTextChanged(Editable s) {
            if (!Utils.isEmpty(s.toString().trim()))
                booking.number_of_student = Integer.valueOf(s.toString().trim());
        }
    };
    private ToyosuBookingTextDataWatcher booking_ed_children_count_watcher = new ToyosuBookingTextDataWatcher() {
        @Override
        public void afterTextChanged(Editable s) {
            if (!Utils.isEmpty(s.toString().trim()))
                booking.number_of_children = Integer.valueOf(s.toString().trim());
        }
    };
    private ToyosuBookingTextDataWatcher booking_ed_comment_watcher = new ToyosuBookingTextDataWatcher() {
        @Override
        public void afterTextChanged(Editable s) {
            booking.comment = s.toString().trim();
        }
    };

    public static ToyosuBookingScreen getInstance(Booking booking, Restaurant restaurant) {
        ToyosuBookingScreen screen = new ToyosuBookingScreen();
        Bundle bundle = new Bundle();
        bundle.putSerializable(BOOKING, booking);
        bundle.putSerializable(RESTAURANT, restaurant);
        screen.setArguments(bundle);
        return screen;
    }

    private void refreshBookingType(final String date) {

        ArrayList<HourOpen> hourOpenListParam = new ArrayList<>();
        int index = 0;
        for (WorkingDate wd: workingDateList){
            if (wd.date.equals(date)) {
                hourOpenListParam = wd.hourOpens;
                break;
            }
            index++;
        }

        String dateBook = request_format.format(new Date(booking.reserve_date));
        String dateNow = request_format.format(new Date());

        ArrayList<Timepoint> times = new ArrayList<>();
        for( HourOpen hourOpen:hourOpenListParam){
            String time = hourOpen.from;
            int total = 0;
            String timeTo = hourOpen.to;
            int hourAddTo = Integer.valueOf(timeTo.split(":")[0]);
            int minuteAddTo = Integer.valueOf(timeTo.split(":")[1]);
            int totalTo = hourAddTo * 60 + minuteAddTo;      // total minutes
            totalTo -= Integer.valueOf(service.duration);            // add the desired offset

            while(totalTo >= total){

                int hourAdd = Integer.valueOf(time.split(":")[0]);
                int minuteAdd = Integer.valueOf(time.split(":")[1]);

                String timeNow = request_format_time.format(new Date());
                int hourNowAdd = Integer.valueOf(timeNow.split(":")[0]);
                int minuteNowAdd = Integer.valueOf(timeNow.split(":")[1]);

                total = hourAdd * 60 + minuteAdd;
                int totalNow = hourNowAdd * 60 + minuteNowAdd;

                if ((dateBook.equals(dateNow) && total >= totalNow) || !dateBook.equals(dateNow) ){
                    Timepoint point = new Timepoint(hourAdd, minuteAdd);
                    times.add(point);
                }

                      // total minutes
                total += TIME_FRAME_BOOKING;            // add the desired offset

//                while (total < 0) {          // fix `t` so that it's never negative
//                    total += 1440;             // 1440 minutes in a day
//                }

                int new_hour = (total / 60) % 24;  // calculate new hours
                int new_minute = total % 60;         // calculate new minutes

                String hour_format = new_hour > 9 ? "%s" : "0%s";
                String minute_format = new_minute > 9 ? "%s" : "0%s";

                time = String.format("%1$s:%2$s:%3$s", String.format(hour_format, String.valueOf(new_hour)), String.format(minute_format, String.valueOf(new_minute)), "00");

            }
        }
        Timepoint[] points = times.toArray(new Timepoint[times.size()]);
        int hour = 0;
        int minute = 0;
        if (points != null && points.length > 0) {
            time_dialog = TimePickerDialog.newInstance(this, hour = points[0].getHour(), minute = points[0].getMinute(), true);
            time_dialog.setSelectableTimes(points);
            Log.e("Time", "refreshBookingType hour: " + hour );
            Log.e("Time", "refreshBookingType minute: " + minute );
            if (booking != null) {
                String hour_format = hour > 9 ? "%s" : "0%s";
                String minute_format = minute > 9 ? "%s" : "0%s";
                booking.reserve_time = String.format("%1$s:%2$s:%3$s", String.format(hour_format, String.valueOf(hour)), String.format(minute_format, String.valueOf(minute)), "00");
                booking_tv_reserve_time.setText(booking.reserve_time);
            }

        } else {
//            Calendar now = Calendar.getInstance();
//            time_dialog = TimePickerDialog.newInstance(this, hour = now.get(Calendar.HOUR_OF_DAY), minute = now.get(Calendar.MINUTE), true);
            workingDateList.remove(index);
            refreshDatePicker();
        }
    }

    private void refreshDatePicker() {
        Calendar max = Calendar.getInstance();
//        Log.e("Date", "refreshDatePicker limit_days_book: " + restaurant.limit_days_book );
//        max.add(Calendar.DAY_OF_YEAR, restaurant.limit_days_book);
        ArrayList<Calendar> list = new ArrayList<>();
//        for (int i = 0; i < restaurant.limit_days_book; ++i) {
//            Calendar temp = Calendar.getInstance();
//            temp.add(Calendar.DAY_OF_YEAR, i);
//            list.add(temp);
//        }

        for (WorkingDate wd:workingDateList){
            Log.e("date", "refreshDatePicker: " + wd.date );
            Calendar temp_day = Calendar.getInstance();
            try {
                Date date = request_format.parse(wd.date);
                temp_day.setTime(date);
            } catch (ParseException e) {
                e.printStackTrace();
            }
            Log.e("date", "refreshDatePicker: " + temp_day.toString() );
            list.add(temp_day);
        }
        max = list.get(list.size()-1);
        Calendar[] selectables = list.toArray(new Calendar[list.size()]);
//        Calendar now = Calendar.getInstance();
        Calendar now = list.get(0);
        date_dialog = DatePickerDialog.newInstance(this, now.get(Calendar.YEAR), now.get(Calendar.MONTH), now.get(Calendar.DAY_OF_MONTH));
        date_dialog.setMaxDate(max);
        date_dialog.setMinDate(Calendar.getInstance());
        if (selectables != null && selectables.length > 0) {
            date_dialog.setSelectableDays(selectables);
            date_dialog.initialize(this, selectables[0].get(Calendar.YEAR), selectables[0].get(Calendar.MONTH), selectables[0].get(Calendar.DAY_OF_MONTH));
        }

        if (booking != null) {
            if (booking.reserve_date <= 0) {
                Calendar selected_date = list.get(0);
                selected_date.set(Calendar.YEAR, date_dialog.getSelectedDay().getYear());
                selected_date.set(Calendar.MONTH, date_dialog.getSelectedDay().getMonth());
                selected_date.set(Calendar.DAY_OF_MONTH, date_dialog.getSelectedDay().getDay());
                selected_date.set(Calendar.HOUR_OF_DAY, 0);
                selected_date.set(Calendar.MINUTE, 0);
                selected_date.set(Calendar.SECOND, 0);
                selected_date.set(Calendar.MILLISECOND, 0);
                if (booking.reserve_date == -1 || booking.reserve_date != selected_date.getTimeInMillis()){
                    booking.reserve_date = selected_date.getTimeInMillis();
 //                   requestTimeReservation();
                    refreshBookingType(request_format.format(new Date(booking.reserve_date)));
                }
                Log.e("book", "onDate first: " + booking.reserve_date );
            }
            Calendar selected = Calendar.getInstance();
            selected.setTimeInMillis(booking.reserve_date);
            date_dialog.initialize(this, selected.get(Calendar.YEAR), selected.get(Calendar.MONTH), selected.get(Calendar.DAY_OF_MONTH));
        }
    }

    private void refreshBookingViewData() {

        if (booking != null) {
            booking_ed_name.setText(booking.name);
            booking_ed_email.setText(booking.email);
            booking_ed_phone.setText(booking.phone);
            booking_tv_reserve_date.setText(format.format(new Date(booking.reserve_date)));
            booking_tv_reserve_time.setText(booking.reserve_time);
//            booking_ed_adult_count.setText(String.valueOf(booking.number_of_adult));
//            booking_ed_children_count.setText(String.valueOf(booking.number_of_children));
//            booking_ed_student_count.setText(String.valueOf(booking.number_of_student));
//            booking_ed_comment.setText(booking.comment);

        }
    }

    @Override
    public void refreshViewDataLanguage() {
        format = new SimpleDateFormat(ToyosuConstant.DATE_FORMAT_WITH_DOW, ToyosuApplication.getActiveActivity().getResources().getConfiguration().locale);
        String required = "<font color=\"#EE0000\">*</font>";
        booking_tv_reserve_shop_title.setText(R.string.shop_info_lb_title);
        String filed_name = getString(R.string.customer_lb_name);
        String filed_email = getString(R.string.customer_lb_email);
        String filed_phone = getString(R.string.customer_lb_phone);
        String filed_number_adult = getString(R.string.reservation_lb_number_of_adult);
        booking_tv_name_title.setText(Html.fromHtml(filed_name + required));
        booking_tv_email_title.setText(Html.fromHtml(filed_email + required));
        booking_tv_phone_title.setText(Html.fromHtml(filed_phone + required));
        booking_tv_reserve_date_title.setText(R.string.reservation_lb_reserve_date);
        booking_tv_reserve_time_title.setText(R.string.reservation_lb_reserve_time);
        booking_tv_adult_count_title.setText(Html.fromHtml(filed_number_adult + required));
        booking_tv_children_count_title.setText(R.string.reservation_lb_number_of_child);
        booking_tv_student_count_title.setText(R.string.reservation_lb_number_of_student);
        booking_tv_comment_title.setText(R.string.customer_lb_comment);
        booking_tv_submit.setText(R.string.reservation_btn_send_request);

        if (restaurant != null) {
            refreshDatePicker();
            refreshErrors();
        }
        refreshBookingViewData();
    }

    @Override
    public void reloadData() {

    }

    private void refreshErrors() {
        if (booking_tv_adult_count_error.getVisibility() == View.VISIBLE) {
            booking_tv_adult_count_error.setText(getString(R.string.reservation_lb_number_of_sear_invalid));
        }

        if (booking_tv_name_error.getVisibility() == View.VISIBLE) {
            booking_tv_name_error.setText(getString(R.string.customer_lb_name_required_full_name));
        }

        if (booking_tv_phone_error.getVisibility() == View.VISIBLE) {
            booking_tv_phone_error.setText(getString(R.string.customer_lb_phone_required));
        }

        if (booking_tv_email_error.getVisibility() == View.VISIBLE) {
            if (Utils.isEmpty(booking_ed_email.getText().toString().trim())) {
                booking_tv_email_error.setText(getString(R.string.customer_lb_email_required));
            } else {
                booking_tv_email_error.setText(getString(R.string.customer_lb_email_invalid));
            }
        }

        if (restaurant != null && booking_tv_reserve_time_error.getVisibility() == View.VISIBLE) {
            booking_tv_reserve_time_error.setText(getString(R.string.reservation_lb_time_invalid));
        }

        if (restaurant != null && booking_tv_adult_count_error.getVisibility() == View.VISIBLE) {
            booking_tv_adult_count_error.setText(getString(R.string.reservation_lb_number_of_sear_invalid));
        }

    }

    @Override
    public void onBaseCreate() {
        super.onBaseCreate();

        additionalList = new ArrayList<>();
        serviceList = new ArrayList<>();
        providerList = new ArrayList<>();
        workingDateList = new ArrayList<>();
        hourOpenList = new ArrayList<>();

        Bundle bundle = getArguments();
        if (bundle != null) {
            booking = (Booking) bundle.getSerializable(BOOKING);
            restaurant = (Restaurant) bundle.getSerializable(RESTAURANT);
        }

        if (booking == null)
            booking = new Booking();
        request_format = new SimpleDateFormat(ToyosuConstant.DATE_FORMAT_REQUEST_SIMPLYBOOK);
        request_format_time = new SimpleDateFormat(ToyosuConstant.TIME_FORMAT_REQUEST_SIMPLYBOOK);
    }

    public Booking getBooking() {
        return booking;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.toyosu_booking_screen, container, false);
    }

    @Override
    public void onInitializeViewData() {
        super.onInitializeViewData();
        booking_sp_shop.setOnItemSelectedListener(this);
        booking_sp_shop.setBackgroundResource(R.drawable.spinner_background);
        booking_sp_services.setOnItemSelectedListener(this);
        booking_sp_services.setBackgroundResource(R.drawable.spinner_background);
        booking_sp_providers.setOnItemSelectedListener(this);
        booking_sp_providers.setBackgroundResource(R.drawable.spinner_background);
        QueueServiceRequester.registerListener(this);
        requestRestaurants();
        if (UserDataManager.getInstance().isEnabled(DataKey.LOGGED)) {
            User user = getLoggedUser();
            if (user != null) {
                this.user = user;
                booking.name = user.name;
                booking.email = user.email;
                booking.phone = user.phone;
            }
        }
        booking_tv_reserve_services_title.setVisibility(View.GONE);
        booking_sp_services.setVisibility(View.GONE);
        booking_tv_reserve_providers_title.setVisibility(View.GONE);
        booking_sp_providers.setVisibility(View.GONE);

    }

    @Override
    public void onBaseFree() {
        QueueServiceRequester.removeListener(this);
        super.onBaseFree();
    }

    @Override
    public void onBaseResume() {
        registerSingleAction(booking_ll_reserve_date, booking_ll_reserve_time, booking_tv_submit, booking_tv_tc);
        booking_ed_comment.addTextChangedListener(booking_ed_comment_watcher);
        booking_ed_name.addTextChangedListener(booking_ed_name_watcher);
        booking_ed_email.addTextChangedListener(booking_ed_email_watcher);
        booking_ed_adult_count.addTextChangedListener(booking_ed_adult_count_watcher);
        booking_ed_children_count.addTextChangedListener(booking_ed_children_count_watcher);
        booking_ed_student_count.addTextChangedListener(booking_ed_student_count_watcher);
        booking_ed_phone.addTextChangedListener(booking_ed_phone_watcher);
        super.onBaseResume();
    }

    @Override
    protected void onBasePause() {
        unregisterSingleAction(booking_ll_reserve_date, booking_ll_reserve_time, booking_tv_submit, booking_tv_tc);
        booking_ed_comment.removeTextChangedListener(booking_ed_comment_watcher);
        booking_ed_name.removeTextChangedListener(booking_ed_name_watcher);
        booking_ed_email.removeTextChangedListener(booking_ed_email_watcher);
        booking_ed_adult_count.removeTextChangedListener(booking_ed_adult_count_watcher);
        booking_ed_children_count.removeTextChangedListener(booking_ed_children_count_watcher);
        booking_ed_student_count.removeTextChangedListener(booking_ed_student_count_watcher);
        booking_ed_phone.removeTextChangedListener(booking_ed_phone_watcher);
        super.onBasePause();
    }

    @Override
    public void onSingleClick(View v) {
        switch (v.getId()) {
            case R.id.booking_ll_reserve_date:
                if (date_dialog != null)
                    date_dialog.show(getActivity().getFragmentManager(), DATE_PICKER_DIALOG_TAG);
                break;
            case R.id.booking_ll_reserve_time:
                if (time_dialog != null)
                    time_dialog.show(getActivity().getFragmentManager(), TIME_PICKER_DIALOG_TAG);
                break;
            case R.id.booking_tv_submit:
                if (validated()) {
                    requestInsertReservation();
                }
                break;
            case R.id.booking_tv_tc:
                requestContentCondition();
                break;
        }
    }

    private void requestRestaurants() {
        makeQueueRequest(getUniqueTag(), QueueElement.Type.RETRY, new RestaurantsParam(0), RequestTarget.RESTAURANTS);
    }

    private void requestContentCondition() {
        makeQueueRequest(getUniqueTag(), QueueElement.Type.RETRY, new SettingByCodeParam(SettingByCodeParam.SETTING_RESERVATION_TC), RequestTarget.SETTING_BY_CODE);
    }

    private void requestRestaurantInfoBook(int id) {
        makeQueueRequest(getUniqueTag(), QueueElement.Type.RETRY, new RestaurantInfoBookParam(id), RequestTarget.RESTAURANT_INFO_BOOK);
    }

    private void requestTimeReservation() {
        makeQueueRequest(getUniqueTag(), QueueElement.Type.RETRY, new HourOpenParam(
                        restaurant.id,
                        request_format.format(new Date(booking.reserve_date)),
                        booking.event_id,
                        booking.unit_id),
                     RequestTarget.TIME_RESERVATION);
    }

    private void requestInsertReservation() {

        int user_id = user.id;
        int res_id = restaurant.id;
        booking.number_of_seat = booking.number_of_adult + booking.number_of_children + booking.number_of_student;

        for (Additional ad : additionalList) {
            if (ad.title.equals(getString(R.string.simlybook_lb_number_of_adult))){
                ad.values = String.valueOf(booking.number_of_adult);
            }else if (ad.title.equals(getString(R.string.simlybook_lb_number_of_child))){
                ad.values = String.valueOf(booking.number_of_children);;
            }else if (ad.title.equals(getString(R.string.simlybook_lb_number_of_student))){
                ad.values = String.valueOf(booking.number_of_student);;
            }else if (ad.title.equals(getString(R.string.simlybook_lb_comment))){
                ad.values = String.valueOf(booking.comment);
            }
        }

        booking.additionals = additionalList;

        Log.e("book", "res_id : " + res_id );
        Log.e("book", "user_id : " + user_id );
        Log.e("book", "booking.email : " + booking.email );
        Log.e("book", "event_id : " + booking.event_id );
        Log.e("book", "unit_id : " + booking.unit_id );
        Log.e("book", "reserve_time : " + booking.reserve_time );
        Log.e("book", "reserve_date : " + request_format.format(new Date(booking.reserve_date)));
        Log.e("book", "reserve_date : " + request_format.format(new Date(booking.reserve_date)));
        Log.e("book", "number_of_seat : " + booking.number_of_seat);

        makeQueueRequest(getUniqueTag(), QueueElement.Type.RETRY, new InsertReservationParam(
                        res_id,
                        user_id,
                        booking.email,
                        booking.name,
                        booking.phone,
                        booking.event_id,
                        providerList,
                        booking.reserve_time,
                        request_format.format(new Date(booking.reserve_date)),
                        booking.number_of_seat,
                        booking.additionals),
                RequestTarget.INSERT_RESERVATION);
    }

    private boolean validated() {
        boolean result = true;
        String name = booking_ed_name.getText().toString().trim();
        if (Utils.isEmpty(name) || !name.contains(" ")) {
            showInputNameError();
            result = false;
        } else {
            hideInputNameError();
        }
        if (Utils.isEmpty(booking_ed_email.getText().toString().trim())) {
            showInputEmailError(getString(R.string.customer_lb_email_required));
            result = false;
        } else {
            if (!Patterns.EMAIL_ADDRESS.matcher(booking_ed_email.getText().toString().trim()).matches()) {
                showInputEmailError(getString(R.string.customer_lb_email_invalid));
                result = false;
            } else
                hideInputEmailError();
        }

        if (Utils.isEmpty(booking_ed_phone.getText().toString().trim())) {
            showInputPhoneEmpty();
            result = false;
        } else{
            Pattern testPattern= Pattern.compile("^[0-9]{8,12}$");
            Matcher testPhone= testPattern.matcher(booking_ed_phone.getText().toString().trim());

            if(!testPhone.matches())
            {
                showInputPhoneError();
            }else{
                hideInputPhoneError();
            }
        }

        Calendar selected = Calendar.getInstance();
        selected.setTimeInMillis(booking.reserve_date);
        selected.set(Calendar.HOUR_OF_DAY, 0);
        selected.set(Calendar.MINUTE, 0);
        selected.add(Calendar.HOUR_OF_DAY, Integer.valueOf(booking.reserve_time.split(":")[0]));
        selected.add(Calendar.MINUTE, Integer.valueOf(booking.reserve_time.split(":")[1]));

        if (Utils.isEmpty(booking_ed_adult_count.getText().toString().trim())) {
            showInputAdultCountError();
            result = false;
        } else {
            int adult_number = Integer.valueOf(booking_ed_adult_count.getText().toString().trim());
            int child_number = 0;
            int student_number = 0;
            String child_number_string = booking_ed_children_count.getText().toString().trim();
            String student_number_string = booking_ed_student_count.getText().toString().trim();
            if (!Utils.isEmpty(child_number_string)) {
                child_number = Integer.valueOf(child_number_string);
            }else{
                booking.number_of_children = 0;
            }
            if (!Utils.isEmpty(student_number_string)) {
                student_number = Integer.valueOf(student_number_string);
            }else{
                booking.number_of_student = 0;
            }
            if (restaurant != null) {

            } else {
                showInputAdultCountError();
                result = false;
            }
            if (adult_number == 0 || adult_number > 99){
                showInputAdultCountError();
                result = false;
            }

            if (adult_number + child_number + student_number == 0 || adult_number + child_number + student_number > 99){
                showInputAdultCountError();
                result = false;
            }
        }

        return result;
    }

    private void hideInputReserveTimeError() {
        booking_tv_reserve_time_title.setVisibility(View.VISIBLE);
        booking_tv_reserve_time_error.setVisibility(View.GONE);
    }

    private void showInputReserveTimeError() {
        booking_tv_reserve_time_title.setVisibility(View.GONE);
        booking_tv_reserve_time_error.setVisibility(View.VISIBLE);
        booking_tv_reserve_time_error.setText(getString(R.string.reservation_lb_time_invalid));
    }

    private void hideInputAdultCountError() {
        booking_tv_adult_count_title.setVisibility(View.VISIBLE);
        booking_tv_adult_count_error.setVisibility(View.GONE);
    }

    private void showInputAdultCountError() {
        booking_tv_adult_count_title.setVisibility(View.GONE);
        booking_tv_adult_count_error.setVisibility(View.VISIBLE);
        if (restaurant != null) {
            booking_tv_adult_count_error.setText(getString(R.string.reservation_lb_number_of_sear_invalid));
        }
    }

    private void hideInputInfantCountError() {
        booking_tv_children_count_title.setVisibility(View.VISIBLE);
        booking_tv_children_count_error.setVisibility(View.GONE);
    }

    private void showInputInfantCountError() {
        booking_tv_children_count_title.setVisibility(View.GONE);
        booking_tv_children_count_error.setVisibility(View.VISIBLE);
        if (restaurant != null) {
            booking_tv_children_count_error.setText(getString(R.string.reservation_lb_noc_required));
        }
    }

    private void hideInputStudentCountError() {
        booking_tv_student_count_title.setVisibility(View.VISIBLE);
        booking_tv_student_count_error.setVisibility(View.GONE);
    }

    private void showInputStudentCountError() {
        booking_tv_student_count_title.setVisibility(View.GONE);
        booking_tv_student_count_error.setVisibility(View.VISIBLE);
        if (restaurant != null) {
            booking_tv_student_count_error.setText(getString(R.string.reservation_lb_nos_required));
        }
    }

    private void hideInputPhoneError() {
        booking_tv_phone_title.setVisibility(View.VISIBLE);
        booking_tv_phone_error.setVisibility(View.GONE);
    }

    private void showInputPhoneEmpty() {
        booking_tv_phone_error.setVisibility(View.VISIBLE);
        booking_tv_phone_error.setText(getString(R.string.customer_lb_phone_required));
        booking_tv_phone_title.setVisibility(View.GONE);
    }

    private void showInputPhoneError() {
        booking_tv_phone_error.setVisibility(View.VISIBLE);
        booking_tv_phone_error.setText(getString(R.string.customer_lb_phone_invalid));
        booking_tv_phone_title.setVisibility(View.GONE);
    }

    private void hideInputEmailError() {
        booking_tv_email_title.setVisibility(View.VISIBLE);
        booking_tv_email_error.setVisibility(View.GONE);
    }

    private void showInputEmailError(String string) {
        booking_tv_email_title.setVisibility(View.GONE);
        booking_tv_email_error.setVisibility(View.VISIBLE);
        booking_tv_email_error.setText(string);
    }

    private void hideInputNameError() {
        booking_tv_name_title.setVisibility(View.VISIBLE);
        booking_tv_name_error.setVisibility(View.GONE);
    }

    private void showInputNameError() {
        booking_tv_name_title.setVisibility(View.GONE);
        booking_tv_name_error.setVisibility(View.VISIBLE);
    }

    @Override
    public void onResultSuccess(BaseResult result, QueueElement element) {
        super.onResultSuccess(result);
        if (result instanceof RestaurantsResult) {

            ArrayList<Restaurant> restaurants = new ArrayList<>();
            restaurants = ((RestaurantsResult) result).getRestaurants();
            booking_sp_shop.setAdapter(adapterRestaurant = new ToyosuRestaurantAdapter(restaurants));
            booking_sp_shop.post(new Runnable() {
                @Override
                public void run() {
                    booking_sp_shop.setSelection(0);
                }
            });

        } else if (result instanceof RestaurantInfoBookResult) {
            additionalList = ((RestaurantInfoBookResult) result).getAdditionalList();
            serviceList = ((RestaurantInfoBookResult) result).getServiceList();
            providerList = ((RestaurantInfoBookResult) result).getProviderList();
            workingDateList = ((RestaurantInfoBookResult) result).getWorkingDateList();

            Collections.sort(workingDateList, new Comparator<WorkingDate>() {
                @Override
                public int compare(WorkingDate o1, WorkingDate o2) {
                    try {
                        return request_format.parse(o1.date).compareTo(request_format.parse(o2.date));
                    } catch (ParseException e) {
                        throw new IllegalArgumentException(e);
                    }
                }
            });

            booking_sp_services.setAdapter(adapterService = new ToyosuServiceAdapter(serviceList));
            service = serviceList.get(0);
            booking.event_id = service.id;
            booking_sp_services.post(new Runnable() {
                @Override
                public void run() {
                    booking_sp_services.setSelection(0);
                }
            });

            booking_sp_providers.setAdapter(adapterProvider = new ToyosuProviderAdapter(providerList));
            Provider provider = providerList.get(0);
            booking.unit_id = provider.id;
            booking_sp_providers.post(new Runnable() {
                @Override
                public void run() {
                    booking_sp_providers.setSelection(0);
                }
            });

            refreshViewDataLanguage();
        } else if (result instanceof InsertReservationResult) {
            showAlertDialog(getActiveActivity(),
                    BOOKING_CONFIRMATION_DIALOG,
                    getGeneralDialogLayoutResource(),
                    R.mipmap.ic_app_logo,
                    getString(R.string.app_name),
                    restaurant.getConfirmReserve(),
                    getString(R.string.common_ok),
                    null, this);
        } else if (result instanceof HourOpenResult) {
//            closeLoadingDialog();
            hourOpenList = ((HourOpenResult) result).getHourOpens();
//            refreshBookingType();
        }else if (result instanceof SettingByCodeResult) {
            LegalInformation legal = ((SettingByCodeResult) result).getLegal();
            showDecisionDialog(getActiveActivity(), -1,
                    getGeneralDialogLayoutResource(),
                    -1,
                    getString(R.string.other_lb_terms_and_condition),
                    legal.getValue(),
                    getString(R.string.common_ok),
                    null,
                    null,
                    null, null);
        }
    }

    @Override
    public void onResultFail(BaseResult result, QueueElement element) {
        super.onResultFail(result);
        if (result instanceof InsertReservationResult) {
            showInputReserveTimeError();
            int code = ((ToyosuBaseResult) result).getBackEndStatus();
            String message = ToyosuApplication.getActiveActivity().getString(R.string.reservation_lb_time_invalid);
            showAlertDialog(getActiveActivity(), -1, getGeneralDialogLayoutResource(),
                    R.mipmap.ic_app_logo,
                    getString(R.string.common_info),
                    message, getString(R.string.common_ok)
                    , null, null);
        }
    }

    @Override
    public void onFail(RequestTarget target, String error, Constant.StatusCode code, QueueElement element) {
        if (!(code == Constant.StatusCode.ERR_QUEUE_IN_REQUEST))
            super.onFail(target, error, code);
    }

    @Override
    public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {
        Calendar selected = Calendar.getInstance();
        selected.set(Calendar.YEAR, year);
        selected.set(Calendar.MONTH, monthOfYear);
        selected.set(Calendar.DAY_OF_MONTH, dayOfMonth);
        selected.set(Calendar.HOUR_OF_DAY, 0);
        selected.set(Calendar.MINUTE, 0);
        selected.set(Calendar.SECOND, 0);
        selected.set(Calendar.MILLISECOND, 0);
        Log.e("book", "onDateSet: " + booking.reserve_date );
        Log.e("book", "onDateSet selected: " + selected.getTimeInMillis() );
        if (booking.reserve_date == -1 || booking.reserve_date != selected.getTimeInMillis()){
            booking.reserve_date = selected.getTimeInMillis();
//            showLoadingDialog(getActivity(), getLoadingDialogLayoutResource(), getString(R.string.common_loading));
//            requestTimeReservation();
            refreshBookingType(request_format.format(new Date(booking.reserve_date)));
        }
        booking_tv_reserve_date.setText(format.format(new Date(booking.reserve_date)));

    }

    @Override
    public void onTimeSet(TimePickerDialog view, int hourOfDay, int minute, int second) {
        String hour_format = hourOfDay > 9 ? "%s" : "0%s";
        String minute_format = minute > 9 ? "%s" : "0%s";
        booking.reserve_time = String.format("%1$s:%2$s:%3$s", String.format(hour_format, String.valueOf(hourOfDay)), String.format(minute_format, String.valueOf(minute)), "00");
        Log.e("book", "time: " + booking.reserve_time );
        booking_tv_reserve_time.setText(booking.reserve_time);
    }

    @Override
    public void onConfirmed(int id, Object onWhat) {
        switch (id) {
            case BOOKING_CONFIRMATION_DIALOG:
                finish();
                break;
        }
    }

    public Restaurant getRestaurant() {
        return restaurant;
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        switch (parent.getId()) {
            case R.id.booking_sp_shop:
                if (adapterRestaurant != null) {
                    restaurant = adapterRestaurant.getItem(position);
                    requestRestaurantInfoBook(restaurant.id);
                }
                break;
            case R.id.booking_sp_services:
                if (adapterService != null) {
                    service = adapterService.getItem(position);
                    booking.event_id = service.id;
                    Log.e("book", "onItemSelected event: " + booking.event_id );
//                    refreshBookingType();
                }
                break;
            case R.id.booking_sp_providers:
                if (adapterProvider != null) {
                    Provider provider = adapterProvider.getItem(position);
                    booking.unit_id = provider.id;
                    Log.e("book", "onItemSelected unit: " + booking.unit_id );
                }
                break;
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    @Override
    public void onStartQueue(QueueElement element) {
        showLoadingDialog(getActiveActivity(), getLoadingDialogLayoutResource(), ToyosuApplication.getContext().getString(R.string.common_loading));
    }

    @Override
    public void onEndQueue() {

    }

    @Override
    public void onFinishQueue() {
        closeLoadingDialog();
    }

    @Override
    public void onBlockQueue(QueueElement element) {

    }

    @Override
    public void onStopQueue(ArrayList<QueueElement> remain) {

    }
}
