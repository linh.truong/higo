package com.ibs.roann.model.api;

import com.ibs.roann.feature.ToyosuBaseParam;
import com.ibs.roann.feature.ToyosuConstant;
import com.ibs.roann.model.Delivery;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;

import core.data.UserDataManager;
import core.util.DataKey;

/**
 * Created by tuanle on 1/3/17.
 */

public class PrizeDeliveryParam extends ToyosuBaseParam {

    public static final String PARAM_DATA_DELIVERY = "data";
    private static final String PARAMS_NAME_TAG = "name";
    private static final String PARAMS_EMAIL_TAG = "email";
    private static final String PARAMS_PHONE_TAG = "phone";
    private static final String PARAMS_ADDRESS_TAG = "address";
    private static final String PARAMS_DATE_TAG = "receive_date";
    private static final String PARAMS_STATUS_TAG = "status";
    private static final String PARAMS_TYPE_TAG = "delivery_type";
    private static final String PARAMS_LANG_TAG = "lang";

    private int customerId;
    private int id;
    private String auth_token;
    private Delivery delivery;

    public PrizeDeliveryParam(int customerId, int id, String auth_token, Delivery delivery) {
        this.customerId = customerId;
        this.id = id;
        this.auth_token = auth_token;
        this.delivery = delivery;
    }

    @Override
    public byte[] makeRequestBody() {
        SimpleDateFormat request_format = new SimpleDateFormat(ToyosuConstant.DATE_FORMAT_REQUEST_SIMPLYBOOK);
        JSONObject data_delivery = new JSONObject();
        try {
            data_delivery.put(PARAMS_NAME_TAG, delivery.name);
            data_delivery.put(PARAMS_EMAIL_TAG, delivery.email);
            data_delivery.put(PARAMS_PHONE_TAG, delivery.phone);
            data_delivery.put(PARAMS_ADDRESS_TAG, delivery.address);
//            if (delivery.receive_date != -1)
//                data_delivery.put(PARAMS_DATE_TAG, request_format.format(new Date(delivery.receive_date)));
//            else
//                data_delivery.put(PARAMS_DATE_TAG, request_format.format(new Date()));
            data_delivery.put(PARAMS_STATUS_TAG, 1);
            data_delivery.put(PARAMS_TYPE_TAG, delivery.delivery_type);
            if (UserDataManager.getInstance().getString(DataKey.DEVICE_LANGUAGE).equals(ToyosuConstant.DEVICE_LANGUAGE_ENGLISH)){
                data_delivery.put(PARAMS_LANG_TAG, "en");
            }else{
                data_delivery.put(PARAMS_LANG_TAG, "cn");
            }
        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        try {
            return new JSONObject().put(PARAM_CUSTOMER_ID_TAG, String.valueOf(customerId))
                    .put(PARAM_PRIZE_ID_TAG, String.valueOf(id))
                    .put(PARAM_AUTH_TOKEN_TAG, auth_token)
                    .put(PARAM_DATA_DELIVERY, data_delivery).toString().getBytes();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return new byte[0];
    }
}

