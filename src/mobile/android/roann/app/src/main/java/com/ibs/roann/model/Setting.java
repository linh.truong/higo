package com.ibs.roann.model;

import com.google.gson.annotations.SerializedName;
import com.ibs.roann.feature.ToyosuConstant;

import java.io.Serializable;

import core.data.UserDataManager;
import core.util.DataKey;

/**
 * Created by tuanle on 1/10/17.
 */

public class Setting implements Serializable {

    @SerializedName("code")
    public String code;

    @SerializedName("id")
    public int id;

    @SerializedName("value")
    public String value;

    @SerializedName("value_cn")
    public String value_cn;

    @SerializedName("is_delete")
    public int is_delete;

    @SerializedName("description")
    public String description;

    @SerializedName("description_cn")
    public String description_cn;

    @SerializedName("update_by")
    public String update_by;

    public String getValue() {
        return UserDataManager.getInstance().getString(DataKey.DEVICE_LANGUAGE).equals(ToyosuConstant.DEVICE_LANGUAGE_CHINESE) ? value_cn : value;
    }

    public String getDescription() {
        return UserDataManager.getInstance().getString(DataKey.DEVICE_LANGUAGE).equals(ToyosuConstant.DEVICE_LANGUAGE_CHINESE) ? description_cn : description;
    }
}
