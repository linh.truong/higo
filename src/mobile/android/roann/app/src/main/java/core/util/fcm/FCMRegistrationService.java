package core.util.fcm;

import android.app.IntentService;
import android.content.Intent;

import org.greenrobot.eventbus.EventBus;

/**
 * @author Tyrael
 * @version 1.0 <br>
 * @since October 2015
 */

public class FCMRegistrationService extends IntentService implements FCMController.FCMRegistrationListener {

    private static final String TAG = FCMRegistrationService.class.getSimpleName();
    private FCMController controller;

    public FCMRegistrationService() {
        super(TAG);
    }

    public FCMRegistrationService(String name) {
        super(name);
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        controller = new FCMController();
        controller.subscribe(this);
    }

    @Override
    public void onRegisteredFailed(boolean isRecoverable) {
        EventBus.getDefault().post(new FCMRegistrationFailMessage(isRecoverable));
    }

    @Override
    public void onRegisteredSuccess(String token) {
        EventBus.getDefault().post(new FCMRegistrationSuccessMessage(token));
    }
}
