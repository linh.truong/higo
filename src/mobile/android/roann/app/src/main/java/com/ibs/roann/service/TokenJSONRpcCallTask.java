package com.ibs.roann.service;

import android.os.AsyncTask;
import android.util.Log;

import core.RPC.JSONRPCClient;
import core.RPC.JSONRPCException;
import core.RPC.JSONRPCParams;

import static com.ibs.roann.feature.ToyosuConstant.URL_API_BOOKING;
import static com.ibs.roann.model.Client.YOUR_COMPANY_LOGIN;
import static com.ibs.roann.model.Client.YOUR_USER_LOGIN;
import static com.ibs.roann.model.Client.YOUR_USER_PASSWORD;

/**
 * Created by tu.le on 3/8/2017.
 */



public class TokenJSONRpcCallTask extends AsyncTask<Void, Void, String> {

    public interface TokenCallbackInterface {
        void getTokenCallback (String result);
    }

    TokenCallbackInterface callbackInterface;

    public TokenJSONRpcCallTask(TokenCallbackInterface callbackInterface) {
        this.callbackInterface = callbackInterface;
    }

    @Override
    protected String doInBackground(Void... params) {
        JSONRPCClient client = JSONRPCClient.create(URL_API_BOOKING + "login/", JSONRPCParams.Versions.VERSION_2);
        client.setConnectionTimeout(2000);
        client.setSoTimeout(2000);
        try {
            return client.callString("getUserToken", YOUR_COMPANY_LOGIN, YOUR_USER_LOGIN, YOUR_USER_PASSWORD);
        } catch (JSONRPCException rpcEx) {
            rpcEx.printStackTrace();
        }
        return null;
    }

    @Override
    protected void onPostExecute(String result) {
        super.onPostExecute(result);
        if (result != null) {
            Log.e("SimplyBook", "token: " + result );
            this.callbackInterface.getTokenCallback(result);
        }
    }
}
