package com.ibs.roann.model.api;

import com.ibs.roann.feature.ToyosuBaseResult;
import com.ibs.roann.model.MenuCategory;

import java.util.ArrayList;

/**
 * Created by tuanle on 1/3/17.
 */

public class FoodCategoriesResult extends ToyosuBaseResult {

    private ArrayList<MenuCategory> menuCategories;
    private int total;

    public FoodCategoriesResult() {
        this.menuCategories = new ArrayList<>();
        this.total = 0;
    }

    public ArrayList<MenuCategory> getMenuCategories() {
        return menuCategories;
    }

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }
}
