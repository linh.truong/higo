package com.ibs.roann.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 *
 */

public class ImageInstagram implements Serializable{

    @SerializedName("standard_resolution")
    public ImageStandard imageStandard;

    @SerializedName("thumbnail")
    public ImageThumbnail imageThumbnail;

    @SerializedName("low_resolution")
    public ImageLow imageLow;

}
