package com.ibs.roann.feature.setting;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.ibs.roann.R;
import com.ibs.roann.feature.ToyosuBaseFragment;
import com.ibs.roann.model.LegalInformation;
import com.ibs.roann.model.Setting;
import com.ibs.roann.model.api.SettingByCodeParam;
import com.ibs.roann.model.api.SettingByCodeResult;

import java.util.ArrayList;

import butterknife.BindView;
import core.base.BaseResult;
import core.data.UserDataManager;
import core.util.RequestTarget;
import icepick.State;

/**
 * Created by tuanle on 1/10/17.
 */

public class ToyosuLegalInformationScreen extends ToyosuBaseFragment {

    private static final String CODE = "code";
    private static final String TITLE = "title";

    @State
    String code;

    @State
    String title;

    @State
    LegalInformation legal;

    @BindView(R.id.legal_information_tv_text)
    TextView legal_information_tv_text;

    public static ToyosuLegalInformationScreen getInstance(String code, String title) {
        ToyosuLegalInformationScreen screen = new ToyosuLegalInformationScreen();
        Bundle bundle = new Bundle();
        bundle.putString(CODE, code);
        bundle.putString(TITLE, title);
        screen.setArguments(bundle);
        return screen;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.toyosu_legal_information_screen, container, false);
    }

    @Override
    public void onBaseCreate() {
        super.onBaseCreate();
        Bundle bundle = getArguments();

        if (bundle != null) {
            code = bundle.getString(CODE);
            title = bundle.getString(TITLE);
        }
    }

    @Override
    public void onInitializeViewData() {
        super.onInitializeViewData();
//        requestSettingByCode(code);
        ArrayList<Setting> settings = UserDataManager.getInstance().getSettings();
        for (int i = 0; i < settings.size(); ++i) {
            Log.e("setting", "onInitializeViewData: " + settings.get(i).code );
            if (settings.get(i).code.equals(code)) {
                legal_information_tv_text.setText(settings.get(i).getValue());
                break;
            }
        }
    }

    public String getTitle() {
        return title;
    }

    private void requestSettingByCode(String code) {
        makeRequest(getUniqueTag(), true, new SettingByCodeParam(code), this, RequestTarget.SETTING_BY_CODE);
    }

    @Override
    public void onResultSuccess(BaseResult result) {
        if (result instanceof SettingByCodeResult) {
            legal = ((SettingByCodeResult) result).getLegal();
            legal_information_tv_text.setText(legal.getValue());
        }
    }

    @Override
    public void refreshViewDataLanguage() {
        if (legal != null)
            legal_information_tv_text.setText(legal.getValue());
    }

    @Override
    public void reloadData() {

    }
}
