package com.ibs.roann.model.api;

import com.ibs.roann.feature.ToyosuBaseResult;
import com.ibs.roann.model.Prize;

import java.util.ArrayList;

/**
 * Created by tuanle on 1/3/17.
 */

public class PrizesResult extends ToyosuBaseResult {

    private int total;

    private ArrayList<Prize> prizes;

    public PrizesResult() {
        prizes = new ArrayList<>();
    }

    public ArrayList<Prize> getPrizes() {
        return prizes;
    }

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }
}
