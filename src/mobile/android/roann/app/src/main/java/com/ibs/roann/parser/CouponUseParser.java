package com.ibs.roann.parser;

import android.util.Log;

import com.ibs.roann.model.api.CouponUseResult;

import org.json.JSONObject;

import core.base.BaseResult;
import core.util.Constant;

/**
 * Created by tuanle on 1/3/17.
 */

public class CouponUseParser extends ToyosuBaseParser {
    @Override
    public BaseResult parseData(String content) {
        CouponUseResult result = new CouponUseResult();

        try {
            JSONObject json = new JSONObject(content);
            result.setBackEndStatus(json.optInt(RESPONSE_ERROR_TAG));
            Log.e("use", "parseData: " + result.getBackEndStatus() );
            if (result.getBackEndStatus() == 0) {

                result.setStatus(Constant.StatusCode.OK);
            } else {
                result.setMessage(json.optString(RESPONSE_MESSAGE_TAG));
            }
            return result;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
}
