package core.util.fcm;

import android.os.AsyncTask;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.firebase.iid.FirebaseInstanceId;

import core.base.BaseApplication;
import core.data.UserDataManager;
import core.util.DLog;
import core.util.DataKey;
import core.util.Utils;


@SuppressWarnings("unused")
public class FCMController {

    private static final int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;
    private static final String TAG = FCMController.class.getSimpleName();
    private String registeredId;
    private FCMRegistrationListener listener;

    /**
     * Stores the registration ID and app versionCode in the application's
     * {@code SharedPreferences}.
     *
     * @param regId registration ID
     */
    public static void storeRegistrationId(String regId) {
        String appVersion = Utils.getAppVersion();
        UserDataManager.getInstance().setString(DataKey.FCM, regId);
        UserDataManager.getInstance().setString(DataKey.VERSION, appVersion);
        UserDataManager.getInstance().setEnabled(DataKey.UPDATED, true);
    }

    public void subscribe(FCMRegistrationListener listener) {
        this.listener = listener;
        if (checkPlayServices()) {
            registeredId = getRegistrationId();
            if (Utils.isEmpty(registeredId)) {
                registerInBackground();
            } else {
                validateToken();
            }
        } else {
            if (listener != null)
                listener.onRegisteredFailed(false);
        }
    }

    /**
     * Check the device to make sure it has the Google Play Services APK. If it
     * doesn't, display a dialog that allows users to download the APK from the
     * Google Play Store or enable it in the device's system settings.
     */
    private boolean checkPlayServices() {
        int resultCode = GoogleApiAvailability.getInstance().isGooglePlayServicesAvailable(BaseApplication.getContext());
        if (resultCode != ConnectionResult.SUCCESS) {
            if (GoogleApiAvailability.getInstance().isUserResolvableError(resultCode)) {
                GoogleApiAvailability.getInstance().getErrorDialog(BaseApplication.getActiveActivity(), resultCode, PLAY_SERVICES_RESOLUTION_REQUEST).show();
            }
            return false;
        }
        return true;
    }

    /**
     * Gets the current registration ID for application on FCM service.
     * <p/>
     * If result is empty, the app needs to register.
     *
     * @return registration ID, or empty string if there is no existing
     * registration ID.
     */
    private String getRegistrationId() {
        try {
            final String registrationId = UserDataManager.getInstance().getString(DataKey.FCM);
            if (Utils.isEmpty(registrationId)) {
                DLog.d(TAG, "Registration not found.");
                return "";
            }
            // Check if app was updated; if so, it must clear the registration
            // ID
            // since the existing regID is not guaranteed to work with the new
            // app version.
            String registeredVersion = UserDataManager.getInstance().getString(
                    DataKey.VERSION);
            String currentVersion = Utils.getAppVersion();
            if (!registeredVersion.equals(currentVersion)) {
                UserDataManager.getInstance().setEnabled(DataKey.UPDATED, false);
                DLog.d(TAG, "App version changed.");
                return "";
            } else {
                DLog.d(TAG, "App is already registered with id = "
                        + registrationId);
                return registrationId;
            }
        } catch (Exception e) {
            return "";
        }
    }

    private void validateToken() {
        new AsyncTask<String, Void, String>() {

            @Override
            protected String doInBackground(String... params) {
                String newId = FirebaseInstanceId.getInstance().getToken();
                if (registeredId.equals(newId)) {
                    if (!UserDataManager.getInstance().isEnabled(DataKey.UPDATED))
                        return newId;
                } else {
                    UserDataManager.getInstance().setEnabled(DataKey.UPDATED, false);
                    return newId;
                }
                return null;
            }

            @Override
            protected void onPostExecute(String s) {
                super.onPostExecute(s);
                if (listener != null) {
                    if (Utils.isEmpty(s)) {
                        listener.onRegisteredSuccess(s);
                    } else {
                        listener.onRegisteredFailed(true);
                    }
                }
            }
        }.execute();
    }

    private void registerInBackground() {
        new AsyncTask<String, Void, String>() {

            @Override
            protected String doInBackground(String... params) {
                try {
                    registeredId = FirebaseInstanceId.getInstance().getToken();
                    if (!UserDataManager.getInstance().isEnabled(DataKey.UPDATED))
                        return registeredId;
                } catch (Exception e) {
                    // If there is an error, don't just keep trying to register.
                    // Require the user to click a button again, or perform
                    // exponential back-off.
                    e.printStackTrace();
                }
                return registeredId;
            }

            @Override
            protected void onPostExecute(String s) {
                super.onPostExecute(s);
                if (listener != null) {
                    if (Utils.isEmpty(s)) {
                        listener.onRegisteredSuccess(s);
                    } else {
                        listener.onRegisteredFailed(true);
                    }
                }
            }
        }.execute();
    }

    interface FCMRegistrationListener {

        void onRegisteredFailed(boolean isRecoverable);

        void onRegisteredSuccess(String token);
    }
}
