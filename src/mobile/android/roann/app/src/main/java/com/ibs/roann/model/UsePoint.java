package com.ibs.roann.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class UsePoint implements Serializable {

    @SerializedName("user_point")
    public int user_point;

    @SerializedName("remain_point")
    public int remain_point;

    @SerializedName("total_point")
    public int total_point;

}
