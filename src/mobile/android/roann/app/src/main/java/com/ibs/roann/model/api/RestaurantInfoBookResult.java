package com.ibs.roann.model.api;

import com.ibs.roann.feature.ToyosuBaseResult;
import com.ibs.roann.model.Additional;
import com.ibs.roann.model.Provider;
import com.ibs.roann.model.Service;
import com.ibs.roann.model.WorkingDate;

import java.util.ArrayList;

/**
 * Created by tuanle on 1/3/17.
 */

public class RestaurantInfoBookResult extends ToyosuBaseResult {

    private ArrayList<Service> serviceList;
    private ArrayList<Provider> providerList;
    private ArrayList<Additional> additionalList;
    private ArrayList<WorkingDate> workingDateList;

    public RestaurantInfoBookResult() {
        serviceList = new ArrayList<>();
        providerList = new ArrayList<>();
        additionalList = new ArrayList<>();
        workingDateList = new ArrayList<>();
    }

    public ArrayList<Service> getServiceList() {
        return serviceList;
    }

    public void setServiceList(ArrayList<Service> serviceList) {
        this.serviceList = serviceList;
    }

    public ArrayList<Provider> getProviderList() {
        return providerList;
    }

    public void setProviderList(ArrayList<Provider> providerList) {
        this.providerList = providerList;
    }

    public ArrayList<Additional> getAdditionalList() {
        return additionalList;
    }

    public void setAdditionalList(ArrayList<Additional> additionalList) {
        this.additionalList = additionalList;
    }

    public ArrayList<WorkingDate> getWorkingDateList() {
        return workingDateList;
    }

    public void setWorkingDateList(ArrayList<WorkingDate> workingDateList) {
        this.workingDateList = workingDateList;
    }
}
