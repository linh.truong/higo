package com.ibs.roann.parser;

import com.google.gson.Gson;
import com.ibs.roann.model.UsePoint;
import com.ibs.roann.model.api.UsePrizeResult;

import org.json.JSONObject;

import core.base.BaseParser;
import core.base.BaseResult;
import core.util.Constant;

import static com.ibs.roann.parser.ToyosuBaseParser.RESPONSE_DATA_TAG;
import static com.ibs.roann.parser.ToyosuBaseParser.RESPONSE_ERROR_TAG;
import static com.ibs.roann.parser.ToyosuBaseParser.RESPONSE_MESSAGE_TAG;

/**
 * Created by tuanle on 1/3/17.
 */

public class UsePrizeParser extends BaseParser {
    @Override
    public BaseResult parseData(String content) {
        UsePrizeResult result = new UsePrizeResult();

        try {
            JSONObject json = new JSONObject(content);
            result.setBackEndStatus(json.optInt(RESPONSE_ERROR_TAG));
            if (result.getBackEndStatus() == 0) {
                Gson gson = new Gson();
                JSONObject data = json.getJSONObject(RESPONSE_DATA_TAG);
                result.setUsePoint((gson.fromJson(data.toString(), UsePoint.class)));
                result.setStatus(Constant.StatusCode.OK);
            } else {
                result.setMessage(json.optString(RESPONSE_MESSAGE_TAG));
            }
            return result;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
}
