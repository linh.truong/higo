package com.ibs.roann.model.api;

import com.ibs.roann.feature.ToyosuBaseParam;
import com.ibs.roann.model.Additional;
import com.ibs.roann.model.Provider;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by tuanle on 1/3/17.
 */

public class InsertReservationParam extends ToyosuBaseParam {

    private static final String PARAMS_RESTAURANT_ID_TAG = "resId";
    private static final String PARAMS_CUSTOMER_ID_TAG = "customerId";
    private static final String PARAMS_EMAIL_TAG = "cusEmail";
    private static final String PARAMS_NAME_TAG = "cusName";
    private static final String PARAMS_PHONE_TAG = "cusPhone";
    private static final String PARAMS_EVENT_TAG = "eventId";
    private static final String PARAMS_UNIT_TAG = "unitList";
    private static final String PARAMS_RESERVE_TIME_TAG = "time";
    private static final String PARAMS_RESERVE_DATE_TAG = "date";
    private static final String PARAMS_RESERVE_NUMBER_TAG = "numberOfSeat";
    private static final String PARAMS_RESERVE_ADDITIONAL_TAG = "additional";


    private int restaurant_id;
    private Integer customer_id;
    private String email;
    private String name;
    private String phone;
    private String event_id;
    private ArrayList<Provider> providers;
    private String reserve_time;
    private String reserve_date;
    private int number_of_seat;
    private ArrayList<Additional> additionals;

    public InsertReservationParam(int restaurant_id, int customer_id, String email, String name, String phone, String event_id, ArrayList<Provider> providers, String reserve_time, String reserve_date, int number_of_seat, ArrayList<Additional> additionals) {
        this.restaurant_id = restaurant_id;
        this.customer_id = customer_id;
        this.email = email;
        this.name = name;
        this.phone = phone;
        this.event_id = event_id;
        this.providers = providers;
        this.reserve_time = reserve_time;
        this.reserve_date = reserve_date;
        this.number_of_seat = number_of_seat;
        this.additionals = additionals;
    }

    @Override
    public byte[] makeRequestBody() {

        JSONArray jsonArray = new JSONArray();
        for( Additional additional:additionals){
            JSONObject ad = new JSONObject();
            try {
                ad.put("name", additional.name);
                ad.put("values", additional.values);
            } catch (JSONException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            jsonArray.put(ad);
        }

        JSONArray jsonArrayUnit = new JSONArray();
        for( Provider provider:providers){
            JSONObject jp = new JSONObject();
            try {
                jp.put("name", provider.name);
                jp.put("id", provider.id);
            } catch (JSONException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            jsonArrayUnit.put(jp);
        }

        try {
            return new JSONObject().put(PARAMS_RESTAURANT_ID_TAG, restaurant_id)
                            .put(PARAMS_CUSTOMER_ID_TAG, customer_id)
                            .put(PARAMS_EMAIL_TAG, email)
                            .put(PARAMS_NAME_TAG, name)
                            .put(PARAMS_PHONE_TAG, phone)
                            .put(PARAMS_EVENT_TAG, event_id)
                            .put(PARAMS_UNIT_TAG, jsonArrayUnit)
                            .put(PARAMS_RESERVE_TIME_TAG, reserve_time)
                            .put(PARAMS_RESERVE_DATE_TAG, reserve_date)
                            .put(PARAMS_RESERVE_NUMBER_TAG, number_of_seat)
                            .put(PARAMS_RESERVE_ADDITIONAL_TAG, jsonArray).toString().getBytes();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return new byte[0];
    }
}

