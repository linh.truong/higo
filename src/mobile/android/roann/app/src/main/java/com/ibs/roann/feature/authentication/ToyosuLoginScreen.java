package com.ibs.roann.feature.authentication;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.util.Patterns;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.ibs.roann.R;
import com.ibs.roann.feature.ToyosuBaseFragment;
import com.ibs.roann.feature.ToyosuMainActivity;
import com.ibs.roann.model.api.LoginParam;
import com.ibs.roann.model.api.LoginResult;
import com.ibs.roann.view.ToyosuRecycledImageView;

import butterknife.BindView;
import core.base.BaseResult;
import core.data.UserDataManager;
import core.util.DataKey;
import core.util.RequestTarget;
import core.util.Utils;
import icepick.State;

import static core.base.BaseFragmentContainer.TAG;

/**
 * Created by tuanle on 1/16/17.
 */

public class ToyosuLoginScreen extends ToyosuBaseFragment {

    private static final String BACK_TO_HOME = "back_to_home";

    @BindView(R.id.login_img_logo)
    ImageView login_img_logo;

    @BindView(R.id.login_tv_email_title)
    TextView login_tv_email_title;

    @BindView(R.id.login_tv_email_error)
    TextView login_tv_email_error;

    @BindView(R.id.login_tv_password_title)
    TextView login_tv_password_title;

    @BindView(R.id.login_tv_password_error)
    TextView login_tv_password_error;

    @BindView(R.id.login_ed_email)
    EditText login_ed_email;

    @BindView(R.id.login_ed_password)
    EditText login_ed_password;

    @BindView(R.id.login_tv_submit)
    TextView login_tv_submit;

    @BindView(R.id.login_tv_forgot_password)
    TextView login_tv_forgot_password;

    @BindView(R.id.login_tv_signup)
    TextView login_tv_signup;

    @BindView(R.id.login_rp_submit)
    View login_rp_submit;

    @State
    boolean backToHome;

    public static ToyosuLoginScreen getInstance(boolean backToHome) {
        ToyosuLoginScreen screen = new ToyosuLoginScreen();
        Bundle bundle = new Bundle();
        bundle.putBoolean(BACK_TO_HOME, backToHome);
        screen.setArguments(bundle);
        return screen;
    }

    public static ToyosuLoginScreen getInstance() {
        ToyosuLoginScreen screen = new ToyosuLoginScreen();
        return screen;
    }

    @Override
    public void onBaseCreate() {
        Bundle bundle = getArguments();
        if (bundle != null) {
            backToHome = bundle.getBoolean(BACK_TO_HOME);
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.toyosu_login_screen, container, false);
    }

    @Override
    public void refreshViewDataLanguage() {
        login_tv_email_title.setText(getString(R.string.customer_lb_email));
        login_tv_password_title.setText(getString(R.string.customer_lb_password));
        login_tv_forgot_password.setText(getString(R.string.customer_hint_login_forgot_pw));
        login_tv_signup.setText(getString(R.string.customer_hint_login_sign_up));
        login_tv_submit.setText(getString(R.string.customer_lb_login));
    }

    private boolean validated() {
        boolean result = true;
        if (Utils.isEmpty(login_ed_email.getText().toString().trim())) {
            showInputEmailError(getString(R.string.customer_lb_email_required));
            result = false;
        } else {
            if (!Patterns.EMAIL_ADDRESS.matcher(login_ed_email.getText().toString().trim()).matches()) {
                showInputEmailError(getString(R.string.customer_lb_email_invalid));
                result = false;
            } else
                hideInputEmailError();
        }

        if (Utils.isEmpty(login_ed_password.getText().toString().trim())) {
            showInputPasswordError();
            result = false;
        } else {
            hideInputPasswordError();
        }

        return result;
    }

    @Override
    public void onBaseResume() {
        super.onBaseResume();
        registerSingleAction(login_rp_submit, login_tv_forgot_password, login_tv_signup);
        registerSingleAction(R.id.login_rl_submit, R.id.login_rp_submit);
    }

    @Override
    protected void onBasePause() {
        unregisterSingleAction(login_rp_submit, login_tv_forgot_password, login_tv_signup);
        unregisterSingleAction(R.id.login_rl_submit, R.id.login_rp_submit);
        super.onBasePause();
    }

    @Override
    public void onBaseFree() {
        if (login_img_logo instanceof ToyosuRecycledImageView) {
            ((ToyosuRecycledImageView) login_img_logo).recycle();
        }
        super.onBaseFree();
    }

    @Override
    public void onInitializeViewData() {
    }

    @Override
    public void reloadData() {
    }

    private void showInputEmailError(String error) {
        login_tv_email_error.setVisibility(View.VISIBLE);
        login_tv_email_title.setVisibility(View.GONE);
        login_tv_email_error.setText(error);
    }

    private void hideInputEmailError() {
        login_tv_email_error.setVisibility(View.GONE);
        login_tv_email_title.setVisibility(View.VISIBLE);
    }

    private void showInputPasswordError() {
        login_tv_password_error.setVisibility(View.VISIBLE);
        login_tv_password_title.setVisibility(View.GONE);
    }

    private void hideInputPasswordError() {
        login_tv_password_error.setVisibility(View.GONE);
        login_tv_password_title.setVisibility(View.VISIBLE);
    }

    @Override
    public void onSingleClick(View v) {
        switch (v.getId()) {
            case R.id.login_tv_submit:
            case R.id.login_rl_submit:
            case R.id.login_rp_submit:
                if (validated()) {
                    makeRequest(getUniqueTag(), true, new LoginParam(login_ed_email.getText().toString().trim(), login_ed_password.getText().toString().trim()), this, RequestTarget.LOGIN);
                }
                break;
            case R.id.login_tv_forgot_password:
                addFragment(getMainContainerId(), ToyosuPasswordRecoveryScreen.getInstance());
                break;

            case R.id.login_tv_signup:
                addFragment(getMainContainerId(), ToyosuRegisterScreen.getInstance());
                break;
        }
    }

    @Override
    public void onResultSuccess(BaseResult result) {
        Log.e("Login", "onResultSuccess:" + result.toString() );
        if (result instanceof LoginResult) {
            setLoggedUser(((LoginResult) result).getUser());
            UserDataManager.getInstance().setEnabled(DataKey.LOGGED, true);
            Log.e(TAG, "onResultSuccess point: " + ((LoginResult) result).getUser().current_point );
            if (backToHome) {
                if (getActiveActivity() instanceof ToyosuMainActivity) {
                    ((ToyosuMainActivity) getActiveActivity()).backToHome();
                }
            } else {
                finish();
            }
        }
    }
}
