package core.util;

import android.net.Uri;
import android.util.Pair;

import com.ibs.roann.parser.AreasParser;
import com.ibs.roann.parser.CategoriesParser;
import com.ibs.roann.parser.CompanyInfoParser;
import com.ibs.roann.parser.ContactUsParser;
import com.ibs.roann.parser.CouponByIdParser;
import com.ibs.roann.parser.CouponUseParser;
import com.ibs.roann.parser.CouponsParser;
import com.ibs.roann.parser.CustomerByIdParser;
import com.ibs.roann.parser.FoodByCategoryParser;
import com.ibs.roann.parser.FoodCategoriesParser;
import com.ibs.roann.parser.HourOpenParser;
import com.ibs.roann.parser.InsertReservationParser;
import com.ibs.roann.parser.InstagramParser;
import com.ibs.roann.parser.LoginParser;
import com.ibs.roann.parser.LogoutParser;
import com.ibs.roann.parser.MembershipParser;
import com.ibs.roann.parser.PrizeDeliveryParser;
import com.ibs.roann.parser.PrizesParser;
import com.ibs.roann.parser.RecoverPasswordParser;
import com.ibs.roann.parser.RegisterCustomerParser;
import com.ibs.roann.parser.RelatedItemsParser;
import com.ibs.roann.parser.RestaurantByCodeParser;
import com.ibs.roann.parser.RestaurantByIdParser;
import com.ibs.roann.parser.RestaurantInfoBookParser;
import com.ibs.roann.parser.RestaurantsParser;
import com.ibs.roann.parser.SettingByCodeParser;
import com.ibs.roann.parser.SettingsParser;
import com.ibs.roann.parser.TopicsByCategoryParser;
import com.ibs.roann.parser.TopicsParser;
import com.ibs.roann.parser.UpdateCustomerNotificationParser;
import com.ibs.roann.parser.UpdateCustomerParser;
import com.ibs.roann.parser.UpdatePasswordParser;
import com.ibs.roann.parser.UpdatePushNotificationParser;
import com.ibs.roann.parser.UpdateQRPointParser;
import com.ibs.roann.parser.UsePrizeParser;

import java.util.ArrayList;

import core.base.BaseParser;
import core.util.Constant.RequestMethod;
import core.util.Constant.RequestType;

public enum RequestTarget {
    CATEGORIES,
    TOPICS,
    TOPICS_BY_CATEGORY,
    FOOD_CATEGORIES,
    FOOD_BY_CATEGORY,
    RESTAURANTS,
    RESTAURANT_BY_ID,
    RESTAURANT_BY_CODE,
    LOG_OUT,
    COUPONS,
    COUPONS_BY_CUS_ID,
    COUPON_USE,
    RECOVER_PASSWORD,
    UPDATE_PASSWORD,
    CUSTOMER_BY_ID,
    SETTING_BY_CODE,
    INSERT_RESERVATION,
    COMPANY_INFO,
    UPDATE_CUSTOMER_NOTIFICATION,
    PRIZES,
    USE_PRIZE,
    UPDATE_PUSH_NOTIFICATION,
    UPDATE_QR_POINT,
    LOGIN,
    COUPON_BY_ID,
    AREAS,
    REGISTER_CUSTOMER,
    UPDATE_CUSTOMER,
    CONTACT_US,
    MEMBERSHIP,
    RELATED_ITEMS,
    RESTAURANT_INFO_BOOK,
    TIME_RESERVATION,
    SETTINGS,
    PRIZE_DELIVERY,
    POST_INSTAGRAM;


    public static BaseParser parser(RequestTarget target) {
        switch (target) {
            case CATEGORIES:
                return new CategoriesParser();
            case TOPICS:
                return new TopicsParser();
            case TOPICS_BY_CATEGORY:
                return new TopicsByCategoryParser();
            case FOOD_CATEGORIES:
                return new FoodCategoriesParser();
            case FOOD_BY_CATEGORY:
                return new FoodByCategoryParser();
            case RESTAURANT_BY_ID:
                return new RestaurantByIdParser();
            case RESTAURANT_BY_CODE:
                return new RestaurantByCodeParser();
            case LOG_OUT:
                return new LogoutParser();
            case COUPONS:
                return new CouponsParser();
            case COUPONS_BY_CUS_ID:
                return new CouponsParser();
            case COUPON_USE:
                return new CouponUseParser();
            case RECOVER_PASSWORD:
                return new RecoverPasswordParser();
            case CUSTOMER_BY_ID:
                return new CustomerByIdParser();
            case SETTING_BY_CODE:
                return new SettingByCodeParser();
            case INSERT_RESERVATION:
                return new InsertReservationParser();
            case COMPANY_INFO:
                return new CompanyInfoParser();
            case UPDATE_CUSTOMER_NOTIFICATION:
                return new UpdateCustomerNotificationParser();
            case PRIZES:
                return new PrizesParser();
            case USE_PRIZE:
                return new UsePrizeParser();
            case UPDATE_PUSH_NOTIFICATION:
                return new UpdatePushNotificationParser();
            case UPDATE_QR_POINT:
                return new UpdateQRPointParser();
            case LOGIN:
                return new LoginParser();
            case COUPON_BY_ID:
                return new CouponByIdParser();
            case AREAS:
                return new AreasParser();
            case REGISTER_CUSTOMER:
                return new RegisterCustomerParser();
            case UPDATE_CUSTOMER:
                return new UpdateCustomerParser();
            case CONTACT_US:
                return new ContactUsParser();
            case RESTAURANTS:
                return new RestaurantsParser();
            case MEMBERSHIP:
                return new MembershipParser();
            case UPDATE_PASSWORD:
                return new UpdatePasswordParser();
            case RELATED_ITEMS:
                return new RelatedItemsParser();
            case RESTAURANT_INFO_BOOK:
                return new RestaurantInfoBookParser();
            case TIME_RESERVATION:
                return new HourOpenParser();
            case SETTINGS:
                return new SettingsParser();
            case PRIZE_DELIVERY:
                return new PrizeDeliveryParser();
            case POST_INSTAGRAM:
                return new InstagramParser();
            default:
                return null;
        }
    }

    public static String host(RequestTarget target) {
        switch (target) {
            default:
                return Constant.SERVER_URL;
        }
    }

    public static RequestMethod method(RequestTarget target) {
        switch (target) {
            default:
                return RequestMethod.POST;
        }
    }

    public static RequestType type(RequestTarget target) {
        switch (target) {
            default:
                return RequestType.HTTP;
        }
    }

    public static int timeout(RequestTarget target) {
        switch (target) {
            default:
                return Constant.TIMEOUT_CONNECT;
        }
    }

    public static int retry(RequestTarget target) {
        switch (target) {
            default:
                return Constant.RETRY_CONNECT;
        }
    }

    public static boolean cache(RequestTarget target) {
        switch (target) {
            default:
                return false;
        }
    }

    private static String addExtraPath(String url, Pair<String, String>... extras) {
        ArrayList<String> paths = new ArrayList<>();
        if (extras != null && extras.length > 0) {
            for (Pair<String, String> extra : extras) {
                if (Utils.isEmpty(extra.first)) {
                    paths.add(extra.second);
                }
            }
        }
        if (paths.size() > 0) {
            for (int i = 0; i < paths.size(); ++i) {
                String path = paths.get(i);
                url += (i <= paths.size() - 1) ? "/" : "" + path;
            }
        }
        return url;
    }

    private static String addExtras(String url, Pair<String, String>... extras) {
        if (extras != null && extras.length > 0) {
            for (int i = 0; i < extras.length; ++i) {
                Pair<String, String> pair = extras[i];
                if (!Utils.isEmpty(pair.first)) {
                    url += (i == 0) ? "?" : "&";
                    url += String.format("%1$s=%2$s", pair.first, Uri.encode(pair.second));
                }
            }
        }
        return url;
    }

    public static String build(RequestTarget target, Pair<String, String>... extras) {
        String url = "";
        switch (target) {
            case CATEGORIES:
                url = "topic_cat/getlist";
                break;
            case TOPICS:
                url = "topic/getlist";
                break;
            case TOPICS_BY_CATEGORY:
                url = "topic/getlist_by_multi_cat";
                break;
            case FOOD_CATEGORIES:
                url = "food_cat/getlist";
                break;
            case FOOD_BY_CATEGORY:
                url = "food/getlist_by_cat";
                break;
            case RESTAURANT_BY_ID:
                url = "res/getinfo";
                break;
            case RESTAURANT_BY_CODE:
                url = "res/get_by_condition";
                break;
            case LOG_OUT:
                url = "login/sign_out";
                break;
            case COUPONS:
                url = "coupon/getlist_ctm";
                break;
            case COUPONS_BY_CUS_ID:
                url = "coupon/getlist_by_cus_id";
                break;
            case COUPON_USE:
                url = "cus_cpn/insert";
                break;
            case RECOVER_PASSWORD:
                url = "login/forgot_password";
                break;
            case CUSTOMER_BY_ID:
                url = "cus/getinfo";
                break;
            case SETTING_BY_CODE:
                url = "setting/get_by_code";
                break;
            case INSERT_RESERVATION:
                url = "simplybook/booking_new";
                break;
            case COMPANY_INFO:
                url = "comp/getinfo";
                break;
            case UPDATE_CUSTOMER_NOTIFICATION:
                url = "cus/update";
                break;
            case PRIZES:
                url = "prize/getlist";
                break;
            case USE_PRIZE:
                url = "cus_prize/use_prize";
                break;
            case UPDATE_PUSH_NOTIFICATION:
                url = "device/set_notify";
                break;
            case UPDATE_QR_POINT:
                url = "cus_point/use_qr_code";
                break;
            case LOGIN:
                url = "login/sign_in";
                break;
            case COUPON_BY_ID:
                url = "coupon/getinfo";
                break;
            case AREAS:
                url = "area/getlist";
                break;
            case REGISTER_CUSTOMER:
                url = "cus/insert";
                break;
            case UPDATE_CUSTOMER:
                url = "cus/update";
                break;
            case CONTACT_US:
                url = "contact/insert";
                break;
            case RESTAURANTS:
                url = "res/getlist";
                break;
            case MEMBERSHIP:
                url = "cus/membership";
                break;
            case UPDATE_PASSWORD:
                url = "cus/update_password";
                break;
            case RELATED_ITEMS:
                url = "related/getlist_v2";
                break;
            case RESTAURANT_INFO_BOOK:
                url = "simplybook/get_book_info";
                break;
            case TIME_RESERVATION:
                url = "simplybook/get_reserve_time";
                break;
            case SETTINGS:
                url = "setting/getlist";
                break;
            case PRIZE_DELIVERY:
                url = "cus_prize/use_prize";
                break;
            case POST_INSTAGRAM:
                url = "instagram/getdata";
                break;
            default:
                break;
        }
        return addExtras(addExtraPath(url, extras), extras);
    }
}
