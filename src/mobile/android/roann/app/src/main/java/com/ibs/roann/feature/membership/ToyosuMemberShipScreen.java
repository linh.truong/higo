package com.ibs.roann.feature.membership;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.ibs.roann.R;
import com.ibs.roann.feature.ToyosuApplication;
import com.ibs.roann.feature.ToyosuBaseFragment;
import com.ibs.roann.feature.ToyosuConstant;
import com.ibs.roann.feature.ToyosuMainActivity;
import com.ibs.roann.feature.ToyosuUtils;
import com.ibs.roann.model.User;
import com.ibs.roann.model.api.MemberShipParam;
import com.ibs.roann.model.api.MembershipResult;

import java.util.Calendar;

import butterknife.BindView;
import core.base.BaseResult;
import core.util.Constant;
import core.util.RequestTarget;

/**
 * Created by tuanle on 1/23/17.
 */

public class ToyosuMemberShipScreen extends ToyosuBaseFragment {

    @BindView(R.id.membership_tv_number)
    TextView membership_tv_number;

    @BindView(R.id.membership_img_barcode)
    ImageView membership_img_barcode;

    @BindView(R.id.membership_img_qrcode)
    ImageView membership_img_qrcode;

    @BindView(R.id.membership_tv_title)
    TextView membership_tv_title;

    @BindView(R.id.membership_tv_reload)
    TextView membership_tv_reload;

    @BindView(R.id.membership_ll_content)
    LinearLayout membership_ll_content;

    @BindView(R.id.membership_tv_info)
    TextView membership_tv_info;

    @BindView(R.id.membership_tv_terms)
    TextView membership_tv_terms;

    @BindView(R.id.membership_tv_time)
    TextView membership_tv_time;

    private User user;

    public static ToyosuMemberShipScreen getInstance() {
        return new ToyosuMemberShipScreen();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.toyosu_membership_screen, container, false);
    }

    @Override
    public void onBaseCreate() {
        super.onBaseCreate();
        user = ((ToyosuMainActivity) ToyosuApplication.getActiveActivity()).getLoggedUser();
    }

    @Override
    public void refreshViewDataLanguage() {

        if (membership_tv_reload.getVisibility() == View.VISIBLE)
            membership_tv_reload.setText(R.string.common_tap_to_reload);
        membership_tv_info.setText(R.string.membership_lb_info);
        membership_tv_title.setText(R.string.membership_lb_title);
        membership_tv_terms.setText(R.string.membership_lb_terms);
        membership_tv_time.setText(ToyosuUtils.formatDate(Calendar.getInstance().getTime(), ToyosuConstant.DATE_TIME_FORMAT));
    }

    @Override
    public void onInitializeViewData() {
        super.onInitializeViewData();
        registerSingleAction(membership_tv_reload);
//        requestMembership();
        membership_tv_reload.setVisibility(View.GONE);
        membership_ll_content.setVisibility(View.VISIBLE);
        membership_tv_number.setText(String.valueOf(user.number));
    }

    @Override
    public void reloadData() {

    }

    @Override
    public void onBaseResume() {
        super.onBaseResume();
        membership_tv_time.setText(ToyosuUtils.formatDate(Calendar.getInstance().getTime(), ToyosuConstant.DATE_TIME_FORMAT));
    }

    @Override
    public void onBaseFree() {
        unregisterSingleAction(membership_tv_reload);
        super.onBaseFree();
    }

    @Override
    public void onSingleClick(View v) {
        switch (v.getId()) {
            case R.id.membership_tv_reload:
                requestMembership();
                break;
        }
    }

    private void requestMembership() {
        makeRequest(getUniqueTag(), true, new MemberShipParam(ToyosuUtils.getUID()), this, RequestTarget.MEMBERSHIP);
    }

    @Override
    public void onResultSuccess(BaseResult result) {
        if (result instanceof MembershipResult) {
            membership_tv_reload.setVisibility(View.GONE);
            membership_ll_content.setVisibility(View.VISIBLE);
            membership_tv_number.setText(String.valueOf(((MembershipResult) result).getMembership().number));
//            ToyosuApplication.getImageLoader().displayImage(((MembershipResult) result).getMembership().barcode, membership_img_barcode);
//            ToyosuApplication.getImageLoader().displayImage(((MembershipResult) result).getMembership().qrcode, membership_img_qrcode);
        }
    }

    @Override
    public void onResultFail(BaseResult result) {
        if (result instanceof MembershipResult) {
            membership_tv_reload.setVisibility(View.VISIBLE);
            membership_ll_content.setVisibility(View.GONE);
        }
        super.onResultFail(result);
    }

    @Override
    public void onFail(RequestTarget target, String error, Constant.StatusCode code) {
        if (target == RequestTarget.MEMBERSHIP) {
            membership_tv_reload.setVisibility(View.VISIBLE);
            membership_ll_content.setVisibility(View.GONE);
        }
        super.onFail(target, error, code);
    }
}
