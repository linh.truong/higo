package com.ibs.roann.model.api;

import com.ibs.roann.feature.ToyosuBaseParam;

import org.json.JSONObject;

/**
 * Created by tuanle on 1/24/17.
 */

public class UpdatePasswordParam extends ToyosuBaseParam {

    private static final String PARAMS_NEW_PASSWORD_TAG = "new_password";
    private static final String PARAMS_OLD_PASSWORD_TAG = "old_password";

    private String old_password;
    private String new_password;

    public UpdatePasswordParam(String old_password, String new_password) {
        this.old_password = old_password;
        this.new_password = new_password;
    }

    @Override
    public byte[] makeRequestBody() {
        try {
            return new JSONObject().put(PARAMS_OLD_PASSWORD_TAG, old_password).put(PARAMS_NEW_PASSWORD_TAG, new_password).toString().getBytes();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return new byte[0];
    }
}
