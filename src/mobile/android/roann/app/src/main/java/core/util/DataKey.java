package core.util;

/**
 * public enum <br>
 * <b>DataKey</b> <br>
 * Represents the key of stored data in share preference.
 */
public enum DataKey {
    QUEUE,
    TOKEN,
    LOGGED,
    FCM,
    VERSION,
    UPDATED,
    USER,
    DEVICE_TOKEN,
    DEVICE_LANGUAGE,
    LANGUAGE_INITIALIZED,
    RESTAURANT_ID,
    SETTINGS
}
