package com.ibs.roann.model;

/**
 * Created by tu.le on 2/23/2017.
 */

public class Message {

    private String message;
    private int type;

    public Message(String message, int type) {
        this.message = message;
        this.type = type;
    }

    public Message(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }

    public int getType() {
        return type;
    }
}
