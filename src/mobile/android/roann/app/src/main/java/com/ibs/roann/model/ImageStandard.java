package com.ibs.roann.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 *
 */

public class ImageStandard implements Serializable{

    @SerializedName("url")
    public String url;

    @SerializedName("width")
    public int image_width;

    @SerializedName("height")
    public int image_height;

}
