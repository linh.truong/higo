package com.ibs.roann.parser;

import com.google.gson.Gson;
import com.ibs.roann.model.Membership;
import com.ibs.roann.model.api.MembershipResult;

import org.json.JSONObject;

import core.base.BaseResult;
import core.util.Constant;

/**
 * Created by tuanle on 1/23/17.
 */

public class MembershipParser extends ToyosuBaseParser {
    @Override
    public BaseResult parseData(String content) {
        MembershipResult result = new MembershipResult();

        try {
            JSONObject json = new JSONObject(content);
            result.setBackEndStatus(json.optInt(RESPONSE_ERROR_TAG));
            if (result.getBackEndStatus() == 0) {
                Gson gson = new Gson();
                JSONObject data = json.getJSONObject(RESPONSE_DATA_TAG);
                result.setMembership(gson.fromJson(data.toString(), Membership.class));
                result.setStatus(Constant.StatusCode.OK);
            } else {
                result.setMessage(json.optString(RESPONSE_MESSAGE_TAG));
            }
            return result;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
}
