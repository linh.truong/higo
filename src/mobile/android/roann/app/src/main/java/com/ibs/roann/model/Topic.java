package com.ibs.roann.model;


import com.google.gson.annotations.SerializedName;
import com.ibs.roann.feature.ToyosuConstant;

import java.io.Serializable;

import core.data.UserDataManager;
import core.util.DataKey;

public class Topic implements Serializable, RelatedItem {

    @SerializedName("id")
    public int id = -1;

    @SerializedName("category")
    public int category_id;

    @SerializedName("restaurant")
    public int restaurant_id;

    @SerializedName("res_avatar")
    public String restaurant_avatar_url;

    @SerializedName("res_name")
    public String restaurant_name;

    @SerializedName("res_name_cn")
    public String restaurant_name_cn;

    @SerializedName("subject")
    public String subject;

    @SerializedName("subject_cn")
    public String subject_cn;

    @SerializedName("body")
    public String body;

    @SerializedName("body_cn")
    public String body_cn;

    @SerializedName("image_path")
    public String image_url;

    @SerializedName("image_width")
    public int image_width;

    @SerializedName("image_height")
    public int image_height;

    @SerializedName("link_address")
    public String address_url;

    @SerializedName("open_date")
    public String open_date;

    @SerializedName("close_date")
    public String close_date;

    @SerializedName("num_like")
    public int like_count;

    @SerializedName("num_share")
    public int share_count;

    @SerializedName("hash_tags")
    public String hash_tags;

    public String getRestaurantName() {
        return UserDataManager.getInstance().getString(DataKey.DEVICE_LANGUAGE).equals(ToyosuConstant.DEVICE_LANGUAGE_CHINESE) ? restaurant_name_cn : restaurant_name;
    }

    public String getSubject() {
        return UserDataManager.getInstance().getString(DataKey.DEVICE_LANGUAGE).equals(ToyosuConstant.DEVICE_LANGUAGE_CHINESE) ? subject_cn : subject;
    }

    public String getBody() {
        return UserDataManager.getInstance().getString(DataKey.DEVICE_LANGUAGE).equals(ToyosuConstant.DEVICE_LANGUAGE_CHINESE) ? body_cn : body;
    }

    @Override
    public String getName() {
        return getSubject();
    }

    @Override
    public String getImagePath() {
        return image_url;
    }
}
