package com.ibs.roann.feature.news;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.ibs.roann.R;
import com.ibs.roann.feature.ToyosuBaseFragment;
import com.ibs.roann.feature.ToyosuConstant;
import com.ibs.roann.feature.ToyosuMainActivity;
import com.ibs.roann.feature.ToyosuUtils;
import com.ibs.roann.feature.coupon.ToyosuCouponDetailScreen;
import com.ibs.roann.feature.menu.ToyosuFoodDetailScreen;
import com.ibs.roann.feature.related.ToyosuRelatedItemAdapter;
import com.ibs.roann.model.Coupon;
import com.ibs.roann.model.MenuFood;
import com.ibs.roann.model.RelatedItem;
import com.ibs.roann.model.Topic;
import com.ibs.roann.model.api.RelatedItemsParam;
import com.ibs.roann.model.api.RelatedItemsResult;
import com.ibs.roann.view.ToyosuImageDialog;
import com.ibs.roann.view.ToyosuRelatedItemDivider;
import com.ibs.roann.view.fetchable.RecycleInterface;
import com.nostra13.universalimageloader.core.ImageLoader;

import butterknife.BindView;
import core.base.BaseResult;
import core.util.Constant;
import core.util.RequestTarget;
import de.hdodenhof.circleimageview.CircleImageView;
import icepick.State;

/**
 *
 */

public class ToyosuTopicDetailScreen extends ToyosuBaseFragment implements RecycleInterface<RelatedItem> {

    private static final String TOPIC = "topic";
    @BindView(R.id.topic_detail_img_restaurant_avatar)
    CircleImageView topic_detail_img_restaurant_avatar;
    @BindView(R.id.topic_detail_tv_restaurant_name)
    TextView topic_detail_tv_restaurant_name;
    @BindView(R.id.topic_detail_img_restaurant_special)
    ImageView topic_detail_img_restaurant_special;
    @BindView(R.id.topic_detail_tv_body)
    TextView topic_detail_tv_body;
    @BindView(R.id.topic_detail_tv_description)
    TextView topic_detail_tv_description;
    @BindView(R.id.topic_detail_tv_hash_tags)
    TextView topic_detail_tv_hash_tags;
    @BindView(R.id.topic_detail_tv_created)
    TextView topic_detail_tv_created;
    @BindView(R.id.topic_detail_tv_related_item_title)
    TextView topic_detail_tv_related_item_title;
    @BindView(R.id.topic_detail_screen_tv_reload)
    TextView topic_detail_screen_tv_reload;
    @State
    Topic topic;
    private ToyosuRelatedItemAdapter adapter;
    private RecyclerView topic_detail_screen_rv_related_item;
    private ToyosuImageDialog dialog;

    public static ToyosuTopicDetailScreen getInstance(Topic topic) {
        ToyosuTopicDetailScreen screen = new ToyosuTopicDetailScreen();
        Bundle bundle = new Bundle();
        bundle.putSerializable(TOPIC, topic);
        screen.setArguments(bundle);
        return screen;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.toyosu_topic_detail_screen, container, false);
    }

    @Override
    public void onBaseCreate() {
        Bundle bundle = getArguments();
        if (bundle != null) {
            topic = (Topic) bundle.getSerializable(TOPIC);
        } else {
            finish();
        }
    }

    @Override
    public void onBindView() {
        super.onBindView();
        LinearLayoutManager lm = new LinearLayoutManager(getActiveActivity(), LinearLayoutManager.HORIZONTAL, false);
        ToyosuRelatedItemDivider divider = new ToyosuRelatedItemDivider(getActiveActivity(),
                lm.getOrientation(), (int) ToyosuUtils.convertDpToPixel(getResources().getDimension(R.dimen.related_item_space), getActiveActivity()));
        topic_detail_screen_rv_related_item = (RecyclerView) findViewById(R.id.topic_detail_screen_rv_related_item);
        topic_detail_screen_rv_related_item.setLayoutManager(lm);
        topic_detail_screen_rv_related_item.addItemDecoration(divider);

    }

    @Override
    public void onInitializeViewData() {
        super.onInitializeViewData();
//        ImageLoader.getInstance().displayImage(topic.restaurant_avatar_url, topic_detail_img_restaurant_avatar);
        ImageLoader.getInstance().displayImage(topic.image_url, topic_detail_img_restaurant_special);
        refreshViewDataLanguage();
        topic_detail_tv_hash_tags.setText(topic.hash_tags);
        registerSingleAction(R.id.topic_detail_img_restaurant_special);
        registerSingleAction(topic_detail_screen_tv_reload);
        requestRelatedItems();
    }

    @Override
    public void reloadData() {

    }

    @Override
    public void onSingleClick(View v) {
        switch (v.getId()) {
            case R.id.topic_detail_screen_tv_reload:
                requestRelatedItems();
                break;
            case R.id.topic_detail_img_restaurant_special:
                dialog = new ToyosuImageDialog(getActiveActivity(), topic.image_url);
                dialog.show();
                break;
        }
    }

    private void requestRelatedItems() {
        makeRequest(getUniqueTag(), true, new RelatedItemsParam(topic.hash_tags, topic.restaurant_id, RelatedItem.TYPE_TOPIC, topic.id, 10), this, RequestTarget.RELATED_ITEMS);
    }

    @Override
    public void refreshViewDataLanguage() {
        if (topic_detail_screen_tv_reload.getVisibility() == View.VISIBLE)
            topic_detail_screen_tv_reload.setText(getString(R.string.common_tap_to_reload));
        topic_detail_tv_related_item_title.setText(getActiveActivity().getResources().getString(R.string.common_related_item_title));
        topic_detail_tv_restaurant_name.setText(topic.getRestaurantName());
        topic_detail_tv_body.setText(topic.getBody());
        topic_detail_tv_description.setText(topic.getSubject());
        topic_detail_tv_created.setText(getString(R.string.home_lb_open_date, ToyosuUtils.formatDate(ToyosuUtils.formatDateString(topic.open_date), ToyosuConstant.DATE_FORMAT_LONG)));
        if (adapter != null)
            adapter.notifyDataSetChanged();
    }

    @Override
    public void onBaseFree() {
        super.onBaseFree();
        unregisterSingleAction(R.id.topic_detail_img_restaurant_special);
        unregisterSingleAction(topic_detail_screen_tv_reload);
    }

    @Override
    public void onResultSuccess(BaseResult result) {
        if (result instanceof RelatedItemsResult) {
            adapter = new ToyosuRelatedItemAdapter(getLayoutInflater(getArguments()), ((RelatedItemsResult) result).getItems(), this, getSingleTouch());
            topic_detail_screen_rv_related_item.setAdapter(adapter);
            topic_detail_screen_tv_reload.setVisibility(View.GONE);
            topic_detail_screen_rv_related_item.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onResultFail(BaseResult result) {
        if (result instanceof RelatedItemsResult) {
            if (adapter != null) {
                adapter.notifyDataSetChanged();
                if (adapter.getRealItemCount() <= 0) {
                    topic_detail_screen_tv_reload.setVisibility(View.VISIBLE);
                    topic_detail_screen_rv_related_item.setVisibility(View.GONE);
                } else {
                    topic_detail_screen_tv_reload.setVisibility(View.GONE);
                    topic_detail_screen_rv_related_item.setVisibility(View.VISIBLE);
                }
            } else {
                topic_detail_screen_tv_reload.setVisibility(View.VISIBLE);
                topic_detail_screen_rv_related_item.setVisibility(View.GONE);
            }
        }
        super.onResultFail(result);
    }

    @Override
    public void onFail(RequestTarget target, String error, Constant.StatusCode code) {
        if (target == RequestTarget.RELATED_ITEMS) {
            if (adapter != null) {
                adapter.notifyDataSetChanged();
                if (adapter.getRealItemCount() <= 0) {
                    topic_detail_screen_tv_reload.setVisibility(View.VISIBLE);
                    topic_detail_screen_rv_related_item.setVisibility(View.GONE);
                } else {
                    topic_detail_screen_tv_reload.setVisibility(View.GONE);
                    topic_detail_screen_rv_related_item.setVisibility(View.VISIBLE);
                }
            } else {
                topic_detail_screen_tv_reload.setVisibility(View.VISIBLE);
                topic_detail_screen_rv_related_item.setVisibility(View.GONE);
            }
        }
        super.onFail(target, error, code);
    }

    @Override
    public void onItemClick(View view, RelatedItem item, int position, int type) {
        if (item instanceof Coupon) {
            ToyosuCouponDetailScreen instance = ToyosuCouponDetailScreen.getInstance((Coupon) item);
            ((ToyosuMainActivity) getActiveActivity()).setMainContainerId(R.id.activity_main_coupon_container);
            addFragment(getMainContainerId(), instance, String.format("%1$s_%2$s", instance.getUniqueTag(), String.valueOf(System.currentTimeMillis())));
            ((ToyosuMainActivity) getActiveActivity()).toggleFooter(R.id.activity_main_footer_ll_coupon_tab);
        } else if (item instanceof Topic) {
            ToyosuTopicDetailScreen instance = ToyosuTopicDetailScreen.getInstance((Topic) item);
            addFragment(getMainContainerId(), instance, String.format("%1$s_%2$s", instance.getUniqueTag(), String.valueOf(System.currentTimeMillis())));
        } else if (item instanceof MenuFood) {
            ToyosuFoodDetailScreen instance = ToyosuFoodDetailScreen.getInstance((MenuFood) item);
            ((ToyosuMainActivity) getActiveActivity()).setMainContainerId(R.id.activity_main_prize_container);
            addFragment(getMainContainerId(), instance, String.format("%1$s_%2$s", instance.getUniqueTag(), String.valueOf(System.currentTimeMillis())));
            ((ToyosuMainActivity) getActiveActivity()).toggleFooter(R.id.activity_main_footer_ll_prize_tab);
        }
    }

    public Topic getTopic() {
        return topic;
    }
}
