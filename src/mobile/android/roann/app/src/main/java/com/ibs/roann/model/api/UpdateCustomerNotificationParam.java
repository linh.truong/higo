package com.ibs.roann.model.api;

import com.ibs.roann.feature.ToyosuBaseParam;

import org.json.JSONObject;

/**
 * Created by tuanle on 1/3/17.
 */

public class UpdateCustomerNotificationParam extends ToyosuBaseParam {
    private static final String PARAMS_DATA_TAG = "data";
    private static final String PARAMS_USER_ID_TAG = "id";
    private static final String PARAMS_IS_PUSH_NOTIFY_TAG = "is_customer_notify";

    private int id;

    private boolean is_customer_notify;

    public UpdateCustomerNotificationParam(int id, boolean is_customer_notify) {
        this.id = id;
        this.is_customer_notify = is_customer_notify;
    }

    @Override
    public byte[] makeRequestBody() {
        try {
            return new JSONObject().put(PARAMS_DATA_TAG, new JSONObject().put(PARAMS_USER_ID_TAG, id).put(PARAMS_IS_PUSH_NOTIFY_TAG, is_customer_notify ? 1 : 0)).toString().getBytes();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return new byte[0];
    }
}

