package com.ibs.roann.feature;

import com.ibs.roann.R;

import core.base.BaseResult;

import static com.ibs.roann.feature.ToyosuConstant.ERR_LOGIN_FAILED;
import static com.ibs.roann.feature.ToyosuConstant.ERR_UNKNOWN;
import static com.ibs.roann.feature.ToyosuConstant.INS_EMPTY_STOCK;
import static com.ibs.roann.feature.ToyosuConstant.INS_LACK_POINT;
import static com.ibs.roann.feature.ToyosuConstant.RES_EXIST;
import static com.ibs.roann.feature.ToyosuConstant.RES_FAIL;

/**
 *
 */

public class ToyosuBaseResult extends BaseResult {
    private int back_end_status;
    private String message;
    public String messRes;

    public int getBackEndStatus() {
        return back_end_status;
    }

    public void setBackEndStatus(int back_end_status) {
        this.back_end_status = back_end_status;
    }

    public String getMessage() {
        message = "";
        switch (back_end_status) {
            case ERR_LOGIN_FAILED:
                message = ToyosuApplication.getActiveActivity().getString(R.string.customer_lb_login_fail);
                break;
            case ERR_UNKNOWN:
                message = ToyosuApplication.getActiveActivity().getString(R.string.error_server_busy);
                break;
            case INS_LACK_POINT:
                message = ToyosuApplication.getActiveActivity().getString(R.string.customer_current_point_not_enough);
                break;
            case INS_EMPTY_STOCK:
                message = ToyosuApplication.getActiveActivity().getString(R.string.other_prize_msg_not_found);
                break;
            case RES_FAIL:
                message = ToyosuApplication.getActiveActivity().getString(R.string.error_server_fail);
                break;
            case RES_EXIST:
                if (messRes.contains("Email"))
                    message = ToyosuApplication.getActiveActivity().getString(R.string.customer_lb_email_exist);
                else
                    message = ToyosuApplication.getActiveActivity().getString(R.string.customer_lb_phone_exist);
                break;
        }
        return message;
    }

    public void setMessage(String message) {
        this.messRes = message;
        this.message = message;
    }
}
