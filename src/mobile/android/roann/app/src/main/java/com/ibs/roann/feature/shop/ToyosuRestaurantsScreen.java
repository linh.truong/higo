package com.ibs.roann.feature.shop;

import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.annotation.StringRes;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.ibs.roann.R;
import com.ibs.roann.feature.ToyosuApplication;
import com.ibs.roann.feature.ToyosuBaseFragment;
import com.ibs.roann.model.Restaurant;
import com.ibs.roann.model.api.RestaurantsParam;
import com.ibs.roann.model.api.RestaurantsResult;
import com.ibs.roann.view.fetchable.MutableRecycleView;
import com.ibs.roann.view.fetchable.MutableSwipeLayout;
import com.ibs.roann.view.fetchable.RecycleInterface;

import java.util.ArrayList;

import butterknife.BindView;
import core.base.BaseResult;
import core.util.Constant;
import core.util.RequestTarget;
import icepick.State;

/**
 * Created by tuanle on 1/20/17.
 */

public class ToyosuRestaurantsScreen extends ToyosuBaseFragment implements RecycleInterface<Restaurant>, MutableRecycleView.OnRefreshListener, MutableRecycleView.OnLoadMoreListener {

    @BindView(R.id.restaurant_screen_tv_reload)
    TextView restaurant_screen_tv_reload;

    @State
    ArrayList<Restaurant> data;
    @State
    @StringRes
    int error_string_id = R.string.common_pull_to_reload;
    private MutableSwipeLayout restaurant_screen_sl_list;
    private MutableRecycleView restaurant_screen_rv_list;
    private ToyosuRestaurantAdapter adapter;

    public static ToyosuRestaurantsScreen getInstance() {
        return new ToyosuRestaurantsScreen();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.toyosu_restaurant_screen, container, false);
    }

    @Override
    public void onBaseCreate() {
        super.onBaseCreate();
        data = new ArrayList<>();
        adapter = new ToyosuRestaurantAdapter(ToyosuApplication.getActiveActivity().getLayoutInflater(), data, this, getSingleTouch());
    }

    @Override
    public void onBindView() {
        super.onBindView();
        restaurant_screen_sl_list = (MutableSwipeLayout) findViewById(R.id.restaurant_screen_sl_list);
        restaurant_screen_rv_list = (MutableRecycleView) findViewById(R.id.restaurant_screen_rv_list);
        restaurant_screen_sl_list.setEnableRefreshProgress(true);
        restaurant_screen_rv_list.setColorScheme(ContextCompat.getColor(getContext(), R.color.primary));
        restaurant_screen_rv_list.setOnRefreshListener(this);
        restaurant_screen_rv_list.setOnLoadMoreListener(this);
        restaurant_screen_rv_list.setAdapter(adapter);
    }

    @Override
    public void onInitializeViewData() {
        super.onInitializeViewData();
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                restaurant_screen_rv_list.onRefresh();
            }
        }, 1000);
    }

    @Override
    public void reloadData() {

    }

    @Override
    public void onItemClick(View view, Restaurant item, int position, int type) {
        if (item != null && item.id != -1)
            addFragment(getMainContainerId(), ToyosuRestaurantDetailScreen.getInstance(item));
    }

    @Override
    public void onRefresh() {
        requestRestaurants(0);
    }

    @Override
    public boolean shouldOverrideLoadMore() {
        return true;
    }

    @Override
    public void onLoadMore() {
        if (data != null)
            requestRestaurants(adapter.getRealItemCount());
    }

    @Override
    public boolean shouldOverrideRefresh() {
        return true;
    }

    @Override
    public int getEnterInAnimation() {
        return 0;
    }

    private void requestRestaurants(int offset) {
        makeRequest(getUniqueTag(), false, new RestaurantsParam(offset), this, RequestTarget.RESTAURANTS);
    }

    @Override
    public void onResultSuccess(BaseResult result) {
        if (result instanceof RestaurantsResult) {
            if (restaurant_screen_rv_list.isRefreshing()) {
                restaurant_screen_rv_list.setLimit(((RestaurantsResult) result).getTotal());
                if (data != null) {
                    data.clear();
                    data.addAll(((RestaurantsResult) result).getRestaurants());
                    data.add(new Restaurant()); // footer
                }
                restaurant_screen_rv_list.onRefreshComplete();
            } else if (restaurant_screen_rv_list.isLoadingMore()) {
                if (data != null) {
                    for (int i = 0; i < data.size(); i++) {
                        if (data.get(i).id == -1)
                            data.remove(i);
                    }
                    data.addAll(((RestaurantsResult) result).getRestaurants());
                    data.add(new Restaurant()); // footer
                }
                restaurant_screen_rv_list.onLoadMoreComplete();
            } else {
                adapter.notifyDataSetChanged();
            }
            if (adapter.getRealItemCount() <= 0) {
                restaurant_screen_tv_reload.setText(error_string_id = R.string.common_pull_to_reload);
                restaurant_screen_tv_reload.setVisibility(View.VISIBLE);
                restaurant_screen_rv_list.setVisibility(View.GONE);
            } else {
                restaurant_screen_tv_reload.setVisibility(View.GONE);
                restaurant_screen_rv_list.setVisibility(View.VISIBLE);
            }
        }
    }

    @Override
    public void onResultFail(BaseResult result) {
        if (result instanceof RestaurantsResult) {
            if (restaurant_screen_rv_list.isRefreshing()) {
                restaurant_screen_rv_list.onRefreshComplete();
            } else if (restaurant_screen_rv_list.isLoadingMore()) {
                restaurant_screen_rv_list.onLoadMoreComplete();
            } else {
                adapter.notifyDataSetChanged();
            }
        }
        if (adapter.getRealItemCount() <= 0) {
            restaurant_screen_tv_reload.setText(error_string_id = R.string.common_error_pull_to_reload);
            restaurant_screen_tv_reload.setVisibility(View.VISIBLE);
            restaurant_screen_rv_list.setVisibility(View.GONE);
        } else {
            restaurant_screen_tv_reload.setVisibility(View.GONE);
            restaurant_screen_rv_list.setVisibility(View.VISIBLE);
        }
        super.onResultFail(result);
    }

    @Override
    public void onFail(RequestTarget target, String error, Constant.StatusCode code) {
        switch (target) {
            case RESTAURANTS:
                if (restaurant_screen_rv_list.isRefreshing()) {
                    restaurant_screen_rv_list.onRefreshComplete();
                } else if (restaurant_screen_rv_list.isLoadingMore()) {
                    restaurant_screen_rv_list.onLoadMoreComplete();
                } else {
                    adapter.notifyDataSetChanged();
                }
                if (adapter.getRealItemCount() <= 0) {
                    restaurant_screen_tv_reload.setText(error_string_id = R.string.common_error_pull_to_reload);
                    restaurant_screen_tv_reload.setVisibility(View.VISIBLE);
                    restaurant_screen_rv_list.setVisibility(View.GONE);
                } else {
                    restaurant_screen_tv_reload.setVisibility(View.GONE);
                    restaurant_screen_rv_list.setVisibility(View.VISIBLE);
                }
                break;
        }
        super.onFail(target, error, code);
    }

    @Override
    public void refreshViewDataLanguage() {
        restaurant_screen_tv_reload.setText(error_string_id);

        if (adapter != null)
            adapter.notifyDataSetChanged();
    }
}
