package com.ibs.roann.model.api;

import com.ibs.roann.feature.ToyosuBaseParam;

import org.json.JSONObject;

/**
 * Created by tuanle on 1/3/17.
 */

public class CompanyInfoParam extends ToyosuBaseParam {
    @Override
    public byte[] makeRequestBody() {
        try {
            return new JSONObject().toString().getBytes();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return new byte[0];
    }
}

