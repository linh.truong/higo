package com.ibs.roann.feature;

import android.content.res.Configuration;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.os.Build;

import com.ibs.roann.R;
import com.nostra13.universalimageloader.cache.memory.impl.LargestLimitedMemoryCache;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;

import java.util.Locale;

import core.base.BaseApplication;
import core.data.UserDataManager;
import core.util.Constant;
import core.util.DataKey;

/**
 * Created by tuanle on 1/3/17.
 */

public class ToyosuApplication extends BaseApplication {

    private static ImageLoader imageLoader;

    public static ImageLoader getImageLoader() {
        return imageLoader;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        imageLoader = initImageLoader(new ImageLoaderConfiguration.Builder(
                getContext()).memoryCache(new LargestLimitedMemoryCache(1024 * 1024))
                .diskCacheSize(Constant.DISC_CACHE_SIZE)
                .diskCacheFileCount(Constant.DISC_CACHE_COUNT)
                .diskCacheExtraOptions(480, 320, null)
                .threadPoolSize(2)
                .denyCacheImageMultipleSizesInMemory()
                .threadPriority(Thread.MAX_PRIORITY)
                .defaultDisplayImageOptions(new DisplayImageOptions.Builder()
                        .imageScaleType(ImageScaleType.EXACTLY)
                        .cacheInMemory(Constant.MEMORY_CACHE)
                        .cacheOnDisk(Constant.DISC_CACHE)
                        .showImageOnFail(R.drawable.ic_no_image)
                        .showImageForEmptyUri(R.drawable.ic_no_image)
                        .bitmapConfig(Bitmap.Config.RGB_565).build()).build());
        initLocale();
    }

    private void initLocale() {
        if (!UserDataManager.getInstance().isEnabled(DataKey.LANGUAGE_INITIALIZED)) {
            UserDataManager.getInstance().setEnabled(DataKey.LANGUAGE_INITIALIZED, true);
            if (getContext().getResources().getConfiguration().locale.getLanguage().equals(ToyosuConstant.DEVICE_LANGUAGE_CHINESE)) {
                UserDataManager.getInstance().setString(DataKey.DEVICE_LANGUAGE, ToyosuConstant.DEVICE_LANGUAGE_CHINESE);
            } else {
                UserDataManager.getInstance().setString(DataKey.DEVICE_LANGUAGE, ToyosuConstant.DEVICE_LANGUAGE_ENGLISH);
            }
        }
        setLocale(UserDataManager.getInstance().getString(DataKey.DEVICE_LANGUAGE));
    }

    private void setLocale(String language) {
        Resources resources = getResources();
        Locale locale = new Locale(language);
        Locale.setDefault(locale);
        final Configuration config = new Configuration(resources.getConfiguration());
        config.locale = locale;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
            config.setLayoutDirection(config.locale);
        }
        resources.updateConfiguration(config,
                resources.getDisplayMetrics());
    }
}
