package com.ibs.roann.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by tuanle on 1/12/17.
 */

public class User implements Serializable {

    @SerializedName("address")
    public String address;

    @SerializedName("gps_data")
    public String gps_data;

    @SerializedName("image_path")
    public String image_path;

    @SerializedName("name")
    public String name;

    @SerializedName("total_point")
    public int total_point;

    @SerializedName("current_point")
    public int current_point;

    @SerializedName("id")
    public int id;

    @SerializedName("is_sms_notify")
    public int is_sms_notify;

    @SerializedName("is_mail_notify")
    public int is_mail_notify;

    @SerializedName("phone")
    public String phone;

    @SerializedName("age")
    public int age;

    @SerializedName("zip_code")
    public String zip_code;

    @SerializedName("area")
    public int area_id;

    @SerializedName("restaurant")
    public int restaurant_id;

    @SerializedName("update_by")
    public String update_by;

    @SerializedName("email")
    public String email;

    @SerializedName("sex")
    public int sex;

    @SerializedName("is_delete")
    public int is_delete;

    @SerializedName("last_transaction_date")
    public String last_transaction_date;

    @SerializedName("number")
    public int number;

    @SerializedName("auth_token")
    public String auth_token;

    @SerializedName("birthday")
    public String birthday;

}
