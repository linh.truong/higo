package com.ibs.roann.feature.menu;

import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.ibs.roann.R;
import com.ibs.roann.feature.ToyosuApplication;
import com.ibs.roann.model.MenuCategory;

import java.util.ArrayList;
import java.util.HashMap;

import core.connection.WebServiceRequester;
import icepick.State;

/**
 * Created by tuanle on 1/5/17.
 */

public class ToyosuMenuCategoryAdapter extends PagerAdapter {

    @State
    ArrayList<MenuCategory> data;

    private HashMap<String, ToyosuMenuCategoryItem> views;

    private WebServiceRequester.WebServiceResultHandler handler;

    public ToyosuMenuCategoryAdapter(ArrayList<MenuCategory> data, WebServiceRequester.WebServiceResultHandler handler) {
        super();
        this.data = data;
        this.views = new HashMap<>();
        this.handler = handler;
    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        ToyosuMenuCategoryItem view = views.get("Page_Category_Id_" + data.get(position).id);
        if (view == null) {
            view = (ToyosuMenuCategoryItem) LayoutInflater.from(ToyosuApplication.getActiveActivity()).inflate(R.layout.toyosu_menu_category_item, container, false);
            view.setCategoryId(data.get(position).id);
            view.setWebServiceResultHandler(handler);
            container.addView(view);
            views.put("Page_Category_Id_" + data.get(position).id, view);
        }
        return view;
    }


    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return data.get(position).getName();
    }

    @Override
    public int getItemPosition(Object object) {
        return POSITION_NONE;
    }

    public HashMap<String, ToyosuMenuCategoryItem> getViews() {
        return views;
    }

    public void clearViews() {
        if (views != null) {
            for (String key : views.keySet()) {
                ToyosuMenuCategoryItem item = views.get(key);
                views.remove(item);
                item = null;
            }
            views.clear();
        }
    }
}
