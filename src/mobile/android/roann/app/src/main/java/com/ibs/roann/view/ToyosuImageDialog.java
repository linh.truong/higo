package com.ibs.roann.view;

import android.content.Context;
import android.view.View;
import android.widget.ImageView;

import com.ibs.roann.R;
import com.ibs.roann.feature.ToyosuApplication;

import core.base.BaseDialog;
import core.util.SingleClick;

/**
 * Created by tuanle on 1/10/17.
 */

public class ToyosuImageDialog extends BaseDialog implements SingleClick.SingleClickListener {

    private String image_url;

    public ToyosuImageDialog(Context context, String image_url) {
        super(context, R.style.TranslucentDialog);
        setCancelable(true);
        setCanceledOnTouchOutside(true);
        this.image_url = image_url;
        setContentView(R.layout.toyosu_image_dialog);
    }

    @Override
    protected void onBaseCreate() {
        getSingleClick().setListener(this);
    }

    @Override
    protected void onBindView() {
        ImageView image = (ImageView) findViewById(R.id.image_dialog_img);
//        image.setOnClickListener(getSingleClick());
        ToyosuApplication.getImageLoader().displayImage(image_url, image);
    }

    @Override
    public void onSingleClick(View v) {
        switch (v.getId()) {
            case R.id.image_dialog_img:
//                dismiss();
                break;
        }
    }
}
