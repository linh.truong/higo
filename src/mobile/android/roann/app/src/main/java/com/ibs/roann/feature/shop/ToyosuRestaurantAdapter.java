package com.ibs.roann.feature.shop;

import android.view.LayoutInflater;
import android.widget.ImageView;
import android.widget.TextView;

import com.ibs.roann.R;
import com.ibs.roann.feature.ToyosuApplication;
import com.ibs.roann.model.Restaurant;
import com.ibs.roann.view.fetchable.RecycleAdapter;
import com.ibs.roann.view.fetchable.RecycleInterface;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.display.RoundedBitmapDisplayer;

import java.util.ArrayList;

import core.util.SingleTouch;

/**
 * Created by tuanle on 1/20/17.
 */

public class ToyosuRestaurantAdapter extends RecycleAdapter<Restaurant> {

    private DisplayImageOptions options;

    public ToyosuRestaurantAdapter(LayoutInflater inflater, ArrayList<Restaurant> items, RecycleInterface<Restaurant> listener, SingleTouch singleTouch) {
        super(inflater, items, listener, singleTouch);
        options = new DisplayImageOptions.Builder()
                .displayer(new RoundedBitmapDisplayer(25)) // default
                .build();
    }

    @Override
    public int getItemViewType(int position) {
        if (items.get(position).id == -1)
            return FOOTER_TYPE;
        return ITEM_TYPE;
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    @Override
    public int getRealItemCount() {
        int footer_count = 0;
        for (Restaurant item : items)
            if (item.id == -1)
                footer_count++;
        return getItemCount() - footer_count;
    }

    @Override
    protected int getHeaderLayoutResource() {
        return 0;
    }

    @Override
    protected int getItemLayoutResource() {
        return R.layout.toyosu_restaurant_item;
    }

    @Override
    protected int getFooterLayoutResource() {
        return R.layout.toyosu_item_footer;
    }

    @Override
    protected int getInidcatorLayoutResource() {
        return 0;
    }

    @Override
    protected void bindFooterView(FooterViewHolder<Restaurant> holder, Restaurant data, int position) {

    }

    @Override
    protected void bindIndicatorView(IndicatorViewHolder<Restaurant> holder, Restaurant data, int position) {

    }

    @Override
    protected void bindHeaderView(HeaderViewHolder<Restaurant> holder, Restaurant data, int position) {

    }

    @Override
    protected void bindItemView(ItemViewHolder<Restaurant> holder, Restaurant data, int position) {
        ToyosuApplication.getImageLoader().displayImage(data.image_path, (ImageView) holder.findViewById(R.id.restaurant_item_img_restaurant), options);
        ((TextView) holder.findViewById(R.id.restaurant_item_tv_name)).setText(data.getName());
        ((TextView) holder.findViewById(R.id.restaurant_item_tv_address)).setText(ToyosuApplication.getActiveActivity().getString(R.string.restaurant_item_lb_address, data.getAddress()));
        ((TextView) holder.findViewById(R.id.restaurant_item_tv_phone)).setText(ToyosuApplication.getActiveActivity().getString(R.string.restaurant_item_lb_phone, data.phone));
    }
}
