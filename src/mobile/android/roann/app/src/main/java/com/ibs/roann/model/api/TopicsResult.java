package com.ibs.roann.model.api;

import com.ibs.roann.feature.ToyosuBaseResult;
import com.ibs.roann.model.Topic;

import java.util.ArrayList;

/**
 * Created by tuanle on 1/3/17.
 */

public class TopicsResult extends ToyosuBaseResult {

    private ArrayList<Topic> topics;
    private int total;

    public TopicsResult() {
        this.topics = new ArrayList<>();
        this.total = 0;
    }

    public ArrayList<Topic> getTopics() {
        return topics;
    }

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }
}
