package com.ibs.roann.model.api;

import com.ibs.roann.feature.ToyosuBaseResult;
import com.ibs.roann.model.LegalInformation;

/**
 * Created by tuanle on 1/3/17.
 */

public class SettingByCodeResult extends ToyosuBaseResult {

    private LegalInformation legal = new LegalInformation();

    public LegalInformation getLegal() {
        return legal;
    }

    public void setLegal(LegalInformation legal) {
        this.legal = legal;
    }
}
