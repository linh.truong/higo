package com.ibs.roann.view;

import android.content.Context;
import android.support.v7.widget.AppCompatCheckBox;
import android.util.AttributeSet;

/**
 * Created by tuanle on 1/11/17.
 */

public class ToyosuCheckBox extends AppCompatCheckBox {

    public ToyosuCheckBox(Context context) {
        super(context);
    }

    public ToyosuCheckBox(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public ToyosuCheckBox(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

}
