package com.ibs.roann.feature.coupon;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.ibs.roann.R;
import com.ibs.roann.feature.ToyosuApplication;
import com.ibs.roann.feature.ToyosuBaseFragment;
import com.ibs.roann.feature.ToyosuConstant;
import com.ibs.roann.feature.ToyosuMainActivity;
import com.ibs.roann.feature.ToyosuUtils;
import com.ibs.roann.feature.menu.ToyosuFoodDetailScreen;
import com.ibs.roann.feature.news.ToyosuTopicDetailScreen;
import com.ibs.roann.feature.related.ToyosuRelatedItemAdapter;
import com.ibs.roann.model.Coupon;
import com.ibs.roann.model.MenuFood;
import com.ibs.roann.model.Message;
import com.ibs.roann.model.RelatedItem;
import com.ibs.roann.model.Topic;
import com.ibs.roann.model.User;
import com.ibs.roann.model.api.CouponUseParam;
import com.ibs.roann.model.api.CouponUseResult;
import com.ibs.roann.model.api.RelatedItemsByCustomerParam;
import com.ibs.roann.model.api.RelatedItemsResult;
import com.ibs.roann.view.ToyosuImageDialog;
import com.ibs.roann.view.ToyosuRelatedItemDivider;
import com.ibs.roann.view.fetchable.RecycleInterface;

import org.greenrobot.eventbus.EventBus;

import butterknife.BindView;
import core.base.BaseResult;
import core.dialog.GeneralDialog;
import core.util.Constant;
import core.util.RequestTarget;
import icepick.State;

import static com.ibs.roann.feature.ToyosuConstant.QUALITY_RELATED_ITEM;
import static com.ibs.roann.feature.ToyosuConstant.RES_FAIL;

/**
 * Created by tuanle on 1/9/17.
 */

public class ToyosuCouponDetailScreen extends ToyosuBaseFragment implements RecycleInterface<RelatedItem>, GeneralDialog.ConfirmListener  {

    private static final String COUPON = "coupon";
    private static final String INDEX_COUPONS = "index";

    private static final int USE_COUPON_SUCCESS = 9505;

    private static final int USE_COUPON_ERROR = 9506;

    @BindView(R.id.coupon_detail_img_coupon)
    ImageView coupon_detail_img_coupon;

    @BindView(R.id.coupon_detail_tv_coupon_title)
    TextView coupon_detail_tv_coupon_title;

    @BindView(R.id.coupon_detail_tv_off)
    TextView coupon_detail_tv_off;

    @BindView(R.id.coupon_detail_tv_coupon_name)
    TextView coupon_detail_tv_coupon_name;

    @BindView(R.id.coupon_detail_tv_coupon_expired)
    TextView coupon_detail_tv_coupon_expired;

    @BindView(R.id.coupon_detail_tv_condition)
    TextView coupon_detail_tv_condition;

    @BindView(R.id.coupon_detail_tv_coupon_expired_title)
    TextView coupon_detail_tv_coupon_expired_title;

    @BindView(R.id.coupon_detail_tv_condition_title)
    TextView coupon_detail_tv_condition_title;

    @BindView(R.id.coupon_detail_tv_related_item_title)
    TextView coupon_detail_tv_related_item_title;

    @BindView(R.id.coupon_detail_screen_tv_reload)
    TextView coupon_detail_screen_tv_reload;

    @BindView(R.id.coupon_detail_tv_use)
    TextView coupon_detail_tv_use;

    @State
    Coupon coupon;
    User user;
    private RecyclerView coupon_detail_screen_rv_related_item;
    private ToyosuRelatedItemAdapter adapter;
    private ToyosuImageDialog dialog;

    public static ToyosuCouponDetailScreen getInstance(Coupon coupon) {
        ToyosuCouponDetailScreen screen = new ToyosuCouponDetailScreen();
        Bundle bundle = new Bundle();
        bundle.putSerializable(COUPON, coupon);
        screen.setArguments(bundle);
        return screen;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.toyosu_coupon_detail_screen, container, false);
    }

    @Override
    public void onBaseCreate() {
        super.onBaseCreate();
        Bundle bundle = getArguments();
        if (bundle != null) {
            coupon = (Coupon) bundle.getSerializable(COUPON);
        }
        user = ((ToyosuMainActivity) ToyosuApplication.getActiveActivity()).getLoggedUser();
    }

    @Override
    public void onBindView() {
        super.onBindView();
        LinearLayoutManager lm = new LinearLayoutManager(getActiveActivity(), LinearLayoutManager.HORIZONTAL, false);
        ToyosuRelatedItemDivider divider = new ToyosuRelatedItemDivider(getActiveActivity(),
                lm.getOrientation(), (int) ToyosuUtils.convertDpToPixel(getResources().getDimension(R.dimen.related_item_space), getActiveActivity()));
        coupon_detail_screen_rv_related_item = (RecyclerView) findViewById(R.id.coupon_detail_screen_rv_related_item);
        coupon_detail_screen_rv_related_item.setLayoutManager(lm);
        coupon_detail_screen_rv_related_item.addItemDecoration(divider);
    }

    @Override
    public void onInitializeViewData() {
        super.onInitializeViewData();
        ToyosuApplication.getImageLoader().displayImage(coupon.image_path, coupon_detail_img_coupon);
        refreshViewDataLanguage();
        registerSingleAction(coupon_detail_img_coupon, coupon_detail_screen_tv_reload, coupon_detail_tv_use);
        requestRelatedItems();
    }

    @Override
    public void reloadData() {

    }

    private void requestRelatedItems() {
        makeRequest(getUniqueTag(), true, new RelatedItemsByCustomerParam(coupon.hash_tags, coupon.restaurant_id, RelatedItem.TYPE_COUPON, coupon.id, QUALITY_RELATED_ITEM, user.id), this, RequestTarget.RELATED_ITEMS);
    }

    @Override
    public void onSingleClick(View v) {
        switch (v.getId()) {
            case R.id.coupon_detail_screen_tv_reload:
                requestRelatedItems();
                break;
            case R.id.coupon_detail_img_coupon:
                dialog = new ToyosuImageDialog(getActiveActivity(), coupon.image_path);
                dialog.show();
                break;
            case R.id.coupon_detail_tv_use:
                Log.e("use", "onSingleClick customer: " + user.id );
                Log.e("use", "onSingleClick coupon: " + coupon.id );
                Log.e("use", "onSingleClick token: " + user.auth_token );
                makeRequest(getUniqueTag(), true, new CouponUseParam(user.id, coupon.id, user.auth_token), this, RequestTarget.COUPON_USE);
                break;
        }
    }

    @Override
    public void refreshViewDataLanguage() {
        coupon_detail_tv_related_item_title.setText(getString(R.string.common_related_item_title));
        if (coupon_detail_screen_tv_reload.getVisibility() == View.VISIBLE) {
            coupon_detail_screen_tv_reload.setText(getString(R.string.common_tap_to_reload));
        }
        coupon_detail_tv_condition_title.setText(R.string.coupon_lb_condition);
        coupon_detail_tv_coupon_expired_title.setText(R.string.coupon_lb_expired_date_upper);
        coupon_detail_tv_coupon_title.setText(coupon.getTitle());
        coupon_detail_tv_coupon_name.setText(coupon.getName());
        coupon_detail_tv_condition.setText(coupon.getCondition());
        if (coupon.price_minus == -1)
            coupon_detail_tv_off.setText(getString(R.string.coupon_lb_sale_off, ToyosuConstant.DEFAULT_CURRENCY, String.valueOf(coupon.price_off)));
        else
            coupon_detail_tv_off.setText(getString(R.string.coupon_lb_sale_off, String.valueOf(coupon.price_minus), "%"));
        coupon_detail_tv_coupon_expired.setText(String.format("%1$s %2$s", ToyosuUtils.formatDate(ToyosuUtils.formatDateString(coupon.to_date), ToyosuConstant.DATE_FORMAT_LONG), coupon.end_time));
        if (adapter != null)
            adapter.notifyDataSetChanged();
    }

    @Override
    public void onBaseFree() {
        super.onBaseFree();
        unregisterSingleAction(coupon_detail_img_coupon, coupon_detail_screen_tv_reload, coupon_detail_tv_use);
    }

    @Override
    public void onResultSuccess(BaseResult result) {
        super.onResultSuccess(result);
        if (result instanceof RelatedItemsResult) {
            adapter = new ToyosuRelatedItemAdapter(getLayoutInflater(getArguments()), ((RelatedItemsResult) result).getItems(), this, getSingleTouch());
            coupon_detail_screen_rv_related_item.setAdapter(adapter);
            coupon_detail_screen_tv_reload.setVisibility(View.GONE);
            coupon_detail_screen_rv_related_item.setVisibility(View.VISIBLE);
        }else if(result instanceof CouponUseResult) {
            String message = getString(R.string.coupon_lb_used_success);
            showAlertDialog(getActiveActivity(), USE_COUPON_SUCCESS, getGeneralDialogLayoutResource(),
                    R.mipmap.ic_app_logo,
                    getString(R.string.common_info),
                    message, getString(R.string.common_ok)
                    , null, this);
        }
    }

    @Override
    public void onResultFail(BaseResult result) {
        super.onResultFail(result);
        if (result instanceof RelatedItemsResult) {
            if (adapter != null) {
                adapter.notifyDataSetChanged();
                if (adapter.getRealItemCount() <= 0) {
                    coupon_detail_screen_tv_reload.setVisibility(View.VISIBLE);
                    coupon_detail_screen_rv_related_item.setVisibility(View.GONE);
                } else {
                    coupon_detail_screen_tv_reload.setVisibility(View.GONE);
                    coupon_detail_screen_rv_related_item.setVisibility(View.VISIBLE);
                }
            } else {
                coupon_detail_screen_tv_reload.setVisibility(View.VISIBLE);
                coupon_detail_screen_rv_related_item.setVisibility(View.GONE);
            }
        }else if(result instanceof CouponUseResult) {
            int code = ((CouponUseResult) result).getBackEndStatus();
            String message = "";
            switch (code) {
                case RES_FAIL:
                    message = ToyosuApplication.getActiveActivity().getString(R.string.error_connection_fail);
                    break;
                default:
                    message = ToyosuApplication.getActiveActivity().getString(R.string.error_server_busy);
                    break;
            }
            showAlertDialog(getActiveActivity(), USE_COUPON_ERROR, getGeneralDialogLayoutResource(),
                    R.mipmap.ic_app_logo,
                    getString(R.string.common_info),
                    message, getString(R.string.common_ok)
                    , null, this);
        }
    }

    @Override
    public void onFail(RequestTarget target, String error, Constant.StatusCode code) {
        if (target == RequestTarget.RELATED_ITEMS) {
            if (adapter != null) {
                adapter.notifyDataSetChanged();
                if (adapter.getRealItemCount() <= 0) {
                    coupon_detail_screen_tv_reload.setVisibility(View.VISIBLE);
                    coupon_detail_screen_rv_related_item.setVisibility(View.GONE);
                } else {
                    coupon_detail_screen_tv_reload.setVisibility(View.GONE);
                    coupon_detail_screen_rv_related_item.setVisibility(View.VISIBLE);
                }
            } else {
                coupon_detail_screen_tv_reload.setVisibility(View.VISIBLE);
                coupon_detail_screen_rv_related_item.setVisibility(View.GONE);
            }
        }
        super.onFail(target, error, code);
    }

    @Override
    public void onConfirmed(int id, Object onWhat) {
        if (id == USE_COUPON_SUCCESS) {
            EventBus.getDefault().post(new Message("remove coupon", coupon.id));
            finish();
        }else if (id == USE_COUPON_ERROR){
            finish();
        }
    }

    @Override
    public void onItemClick(View view, RelatedItem item, int position, int type) {
        if (item instanceof Coupon) {
            ToyosuCouponDetailScreen instance = ToyosuCouponDetailScreen.getInstance((Coupon) item);
            addFragment(getMainContainerId(), instance, String.format("%1$s_%2$s", instance.getUniqueTag(), String.valueOf(System.currentTimeMillis())));
        } else if (item instanceof Topic) {
            ToyosuTopicDetailScreen instance = ToyosuTopicDetailScreen.getInstance((Topic) item);
            ((ToyosuMainActivity) getActiveActivity()).setMainContainerId(R.id.activity_main_home_container);
            addFragment(getMainContainerId(), instance, String.format("%1$s_%2$s", instance.getUniqueTag(), String.valueOf(System.currentTimeMillis())));
            ((ToyosuMainActivity) getActiveActivity()).toggleFooter(R.id.activity_main_footer_ll_home_tab);
        } else if (item instanceof MenuFood) {
            ToyosuFoodDetailScreen instance = ToyosuFoodDetailScreen.getInstance((MenuFood) item);
            ((ToyosuMainActivity) getActiveActivity()).setMainContainerId(R.id.activity_main_prize_container);
            addFragment(getMainContainerId(), instance, String.format("%1$s_%2$s", instance.getUniqueTag(), String.valueOf(System.currentTimeMillis())));
            ((ToyosuMainActivity) getActiveActivity()).toggleFooter(R.id.activity_main_footer_ll_prize_tab);
        }
    }

    public Coupon getCoupon() {
        return coupon;
    }
}
