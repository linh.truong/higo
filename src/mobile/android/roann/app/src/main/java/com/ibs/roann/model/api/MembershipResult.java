package com.ibs.roann.model.api;

import com.ibs.roann.feature.ToyosuBaseResult;
import com.ibs.roann.model.Membership;

/**
 * Created by tuanle on 1/23/17.
 */

public class MembershipResult extends ToyosuBaseResult {

    private Membership membership;

    public Membership getMembership() {
        return membership;
    }

    public void setMembership(Membership membership) {
        this.membership = membership;
    }
}
