package com.ibs.roann.model;

import android.util.Log;

import core.RPC.JSONRPCClient;
import core.RPC.JSONRPCException;
import core.RPC.JSONRPCParams;

public class Client {

    private JSONRPCClient client;
    private String host;

    public static final String YOUR_COMPANY_LOGIN = "toyosu";
    public static final String YOUR_USER_LOGIN = "admin";
    public static final String YOUR_USER_PASSWORD = "123456";

    public Client(String host){
        Log.d("Info:", "Va a enlazar"); //Debug

        // Inicializar cliente
        // create(java.lang.String uri, JSONRPCParams.Versions version Create a JSONRPCClient from a given uri
        client = JSONRPCClient.create(host, JSONRPCParams.Versions.VERSION_2);

        Log.d("Info:", "Enlazado"); //Debug
    }


    public String getUserToken(){

        // Set the connection timeout
        // setConnectionTimeout(int connectionTimeout)
        client.setConnectionTimeout(2000);
        client.setSoTimeout(2000);
        String result;

        try {
            // Petición de String
            result = client.callString("getUserToken", YOUR_COMPANY_LOGIN, YOUR_USER_LOGIN, YOUR_USER_PASSWORD);
        } catch (JSONRPCException e) {
            e.printStackTrace();
            result = null;
        }

        return result;
    }
}
