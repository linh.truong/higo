package com.ibs.roann.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 *
 */

public class Instagram implements Serializable{

    @SerializedName("link")
    public String link;

    @SerializedName("caption")
    public Caption caption;

    @SerializedName("id")
    public String post_id = "-1";

    @SerializedName("created_time")
    public String created_time;

    @SerializedName("likes")
    public Likes likes;

    @SerializedName("comments")
    public Comments comments;

    @SerializedName("images")
    public ImageInstagram imageInstagram;

}
