package com.ibs.roann.feature.reservation;

import android.database.DataSetObserver;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.SpinnerAdapter;
import android.widget.TextView;

import com.ibs.roann.R;
import com.ibs.roann.feature.ToyosuApplication;
import com.ibs.roann.model.Service;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by tuanle on 2/2/17.
 */

public class ToyosuServiceAdapter implements SpinnerAdapter {

    private ArrayList<Service> data;
    private LayoutInflater inflater;

    public ToyosuServiceAdapter(ArrayList<Service> data) {
        this.data = data;
        this.inflater = ToyosuApplication.getActiveActivity().getLayoutInflater();
    }

    @Override
    public void registerDataSetObserver(DataSetObserver observer) {

    }

    @Override
    public void unregisterDataSetObserver(DataSetObserver observer) {

    }

    public ArrayList<Service> getData() {
        return data;
    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public Service getItem(int position) {
        return data.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ToyosuServiceAdapter.IndicatorViewHolder holder;
        View view = convertView;
        if (view != null) {
            holder = (ToyosuServiceAdapter.IndicatorViewHolder) view.getTag();
        } else {
            view = inflater.inflate(R.layout.toyosu_booking_restaurant_indicator, parent, false);
            holder = new ToyosuServiceAdapter.IndicatorViewHolder(view);
            view.setTag(holder);
        }
        holder.booking_restaurant_indicator_tv_name.setText(getItem(position).name);
        return view;
    }

    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {
        ToyosuServiceAdapter.ItemViewHolder holder;
        View view = convertView;
        if (view != null) {
            holder = (ToyosuServiceAdapter.ItemViewHolder) view.getTag();
        } else {
            view = inflater.inflate(R.layout.toyosu_booking_restaurant_item, parent, false);
            holder = new ToyosuServiceAdapter.ItemViewHolder(view);
            view.setTag(holder);
        }
        holder.booking_restaurant_item_tv_name.setText(getItem(position).name);
        return view;
    }

    @Override
    public int getItemViewType(int position) {
        return 1;
    }

    @Override
    public int getViewTypeCount() {
        return 1;
    }

    @Override
    public boolean isEmpty() {
        return false;
    }

    static class ItemViewHolder {

        @BindView(R.id.booking_restaurant_item_tv_name)
        TextView booking_restaurant_item_tv_name;

        public ItemViewHolder(View view) {
            ButterKnife.bind(this, view);
        }
    }

    static class IndicatorViewHolder {

        @BindView(R.id.booking_restaurant_indicator_tv_name)
        TextView booking_restaurant_indicator_tv_name;

        public IndicatorViewHolder(View view) {
            ButterKnife.bind(this, view);
        }
    }

}
