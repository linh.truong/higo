package com.ibs.roann.view;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.ImageView;

/**
 * Created by tuanle on 1/6/17.
 */

public class ToyosuSquareImageView extends ImageView {
    public ToyosuSquareImageView(Context context) {
        super(context);
    }

    public ToyosuSquareImageView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        int width = getMeasuredWidth();
        setMeasuredDimension(width, width);
    }
}
