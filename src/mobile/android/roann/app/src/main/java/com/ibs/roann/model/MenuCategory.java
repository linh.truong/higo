package com.ibs.roann.model;

import com.google.gson.annotations.SerializedName;
import com.ibs.roann.feature.ToyosuConstant;

import java.io.Serializable;

import core.data.UserDataManager;
import core.util.DataKey;

/**
 * Created by tuanle on 1/5/17.
 */

public class MenuCategory implements Serializable {

    @SerializedName("id")
    public int id;

    @SerializedName("name_cn")
    public String name_cn;

    @SerializedName("name")
    public String name;

    @SerializedName("is_delete")
    public int is_delete;

    @SerializedName("update_by")
    public String update_by;

    @SerializedName("image_path")
    public String image_path;

    public String getName() {
        return UserDataManager.getInstance().getString(DataKey.DEVICE_LANGUAGE).equals(ToyosuConstant.DEVICE_LANGUAGE_CHINESE) ? name_cn : name;
    }
}
