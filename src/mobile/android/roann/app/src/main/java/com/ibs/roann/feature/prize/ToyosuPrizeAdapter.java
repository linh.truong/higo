package com.ibs.roann.feature.prize;

import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.ibs.roann.R;
import com.ibs.roann.feature.ToyosuApplication;
import com.ibs.roann.feature.ToyosuConstant;
import com.ibs.roann.feature.ToyosuMainActivity;
import com.ibs.roann.feature.ToyosuUtils;
import com.ibs.roann.model.Prize;
import com.ibs.roann.model.User;
import com.ibs.roann.view.fetchable.RecycleAdapter;
import com.ibs.roann.view.fetchable.RecycleInterface;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.display.RoundedBitmapDisplayer;

import java.util.ArrayList;

import core.data.UserDataManager;
import core.util.DataKey;
import core.util.SingleTouch;

/**
 * Created by tuanle on 2/3/17.
 */

public class ToyosuPrizeAdapter extends RecycleAdapter<Prize> {

    private User user;
    private DisplayImageOptions options;

    public ToyosuPrizeAdapter(LayoutInflater inflater, ArrayList<Prize> items, RecycleInterface<Prize> listener, SingleTouch singleTouch) {
        super(inflater, items, listener, singleTouch);
        user = ((ToyosuMainActivity) ToyosuApplication.getActiveActivity()).getLoggedUser();
        options = new DisplayImageOptions.Builder()
                .displayer(new RoundedBitmapDisplayer(25)) // default
                .build();
    }

    public void setUser(){
        user = ((ToyosuMainActivity) ToyosuApplication.getActiveActivity()).getLoggedUser();
        notifyDataSetChanged();
    }

    @Override
    public int getRealItemCount() {
        int footer_count = 0;
        for (Prize prize : items)
            if (prize.id == -1 || prize.id == -2)
                footer_count++;
        return getItemCount() - footer_count;
    }

    @Override
    public int getItemViewType(int position) {
        if (items.get(position).id == -1)
            return FOOTER_TYPE;
        else if (items.get(position).id == -2)
            return HEADER_TYPE;
        return ITEM_TYPE;
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    @Override
    protected int getHeaderLayoutResource() {
        return R.layout.toyosu_prize_header_item;
    }

    @Override
    protected int getItemLayoutResource() {
        return R.layout.toyosu_prize_item;
    }

    @Override
    protected int getFooterLayoutResource() {
        return R.layout.toyosu_item_footer;
    }

    @Override
    protected int getInidcatorLayoutResource() {
        return 0;
    }

    @Override
    protected void bindFooterView(FooterViewHolder<Prize> holder, Prize data, int position) {

    }

    @Override
    protected void bindIndicatorView(IndicatorViewHolder<Prize> holder, Prize data, int position) {

    }

    @Override
    protected void bindHeaderView(HeaderViewHolder<Prize> holder, Prize data, int position) {
        if (UserDataManager.getInstance().isEnabled(DataKey.LOGGED)) {
            ((TextView) holder.findViewById(R.id.prize_header_item_tv_point)).setText(String.valueOf(user.current_point));
        }else{
            holder.findViewById(R.id.prize_header_item_view).setVisibility(View.GONE);
        }
    }

    @Override
    protected void bindItemView(ItemViewHolder<Prize> holder, Prize data, int position) {
        ToyosuApplication.getImageLoader().displayImage(data.image_path, (ImageView) holder.findViewById(R.id.prize_item_img), options);
        ((TextView) holder.findViewById(R.id.prize_item_tv_title)).setText(data.getTitle());
        ((TextView) holder.findViewById(R.id.prize_item_tv_point)).setText(ToyosuApplication.getActiveActivity().getString(R.string.other_prize_lb_request_point, String.valueOf(data.request_point)));
        ((TextView) holder.findViewById(R.id.prize_item_tv_date)).setText(ToyosuApplication.getActiveActivity().getString(R.string.prize_start_date, ToyosuUtils.formatDate(ToyosuUtils.formatDateString(data.from_date), ToyosuConstant.DATE_FORMAT_VERY_SHORT)));
        ((TextView) holder.findViewById(R.id.coupon_item_tv_start)).setText(ToyosuApplication.getActiveActivity().getString(R.string.coupon_lb_start_time, data.start_time));
        ((TextView) holder.findViewById(R.id.coupon_item_tv_end)).setText(ToyosuApplication.getActiveActivity().getString(R.string.coupon_lb_end_time, data.end_time));
    }
}
