package core.connection;

import android.util.Pair;

import core.base.BaseApplication;
import core.base.BaseProperties;
import core.base.Param;
import core.connection.WebServiceRequester.WebServiceResultHandler;
import core.connection.queue.QueueElement;
import core.connection.request.FileRequest;
import core.connection.request.ParallelServiceRequest;
import core.connection.request.QueueServiceRequest;
import core.connection.request.WebServiceRequest;
import core.util.DLog;
import core.util.RequestTarget;

@SuppressWarnings({"BooleanMethodIsAlwaysInverted", "unused"})
public class Requester {

    private static final String TAG = "Requester";

    /**
     * This method is to start a single network request, prior request will be cancelled
     *
     * @param tag     Indicator of invoker for this request
     * @param target  The request target including host, timeout, retry policy, parser, method, protocol,
     *                cache policy and built url based on extra
     * @param content The parameter for this request including headers and request body
     * @param handler The handler for response result
     * @param extras  The extra parameters to build on the request url
     */
    public static boolean startWSRequest(String tag, RequestTarget target, Param content, WebServiceResultHandler handler, Pair<String, String>... extras) {

        try {
            WebServiceRequest request;
            if (BaseProperties.wsRequester == null)
                BaseProperties.wsRequester = WebServiceRequester
                        .getInstance(BaseApplication.getContext());
            request = new WebServiceRequest(tag, RequestTarget.type(target),
                    RequestTarget.method(target), RequestTarget.host(target), target,
                    RequestTarget.build(target, extras), content,
                    BaseProperties.wsRequester, RequestTarget.parser(target), handler);
            BaseProperties.wsRequester.startRequest(request);
            DLog.d(TAG, request.getRequestMethod().name().toUpperCase()
                    + " >> " + request.getUrl());
            return true;
        } catch (Exception ex) {
            ex.printStackTrace();
            DLog.d(TAG, "Request canceled!");
            return false;
        }
    }

    /**
     * This method is to start a chain of requests orderly, new requests will be added to a queue and executed afterward
     *
     * @param tag     Indicator of invoker for this request
     * @param target  The request target including host, timeout, retry policy, parser, method, protocol,
     *                cache policy and built url based on extra
     * @param type    The queue type for this request
     * @param content The parameter for this request including headers and request body
     * @param extras  The extra parameters to build on the request url
     */
    public static boolean startQueueRequest(String tag, RequestTarget target,
                                            QueueElement.Type type, Param content, Pair<String, String>... extras) {
        try {
            QueueServiceRequest request;
            if (BaseProperties.queueRequester == null)
                BaseProperties.queueRequester = QueueServiceRequester
                        .getInstance(BaseApplication.getContext());
            request = new QueueServiceRequest(tag, RequestTarget.type(target),
                    RequestTarget.method(target), RequestTarget.host(target), target,
                    RequestTarget.build(target, extras), content, RequestTarget.parser(target),
                    BaseProperties.queueRequester);
            BaseProperties.queueRequester.addQueueRequest(new QueueElement(
                    request, type));
            DLog.d(TAG, request.getRequestMethod().name().toUpperCase()
                    + " >> " + request.getUrl());
            return true;
        } catch (Exception ex) {
            ex.printStackTrace();
            DLog.d(TAG, "Queue request canceled!");
            return false;
        }
    }

    /**
     * This method is to start (up to 4) requests at a same time, new requests will be added to queue and executed afterward
     *
     * @param tag     Indicator of invoker for this request
     * @param target  The request target including host, timeout, retry policy, parser, method, protocol,
     *                cache policy and built url based on extra
     * @param content The parameter for this request including headers and request body
     * @param extras  The extra parameters to build on the request url
     */
    public static boolean startParallelRequest(String tag, RequestTarget target,
                                               Param content, Pair<String, String>... extras) {
        try {
            ParallelServiceRequest request;
            if (BaseProperties.parallelRequester == null)
                BaseProperties.parallelRequester = ParallelServiceRequester
                        .getInstance(BaseApplication.getContext());
            request = new ParallelServiceRequest(tag, RequestTarget.type(target),
                    RequestTarget.method(target), RequestTarget.host(target), target,
                    RequestTarget.build(target, extras), content, RequestTarget.parser(target),
                    BaseProperties.parallelRequester);
            ParallelServiceRequester.addRequest(request);
            DLog.d(TAG, request.getRequestMethod().name().toUpperCase()
                    + " >> " + request.getUrl());
            return true;
        } catch (Exception ex) {
            ex.printStackTrace();
            DLog.d(TAG, "Parallel request canceled!");
            return false;
        }
    }

    /**
     * This method is to start (up to 4) file requests at a same time and store them into a predefined location
     *
     * @param tag       Indicator of invoker for this request
     * @param target    The request target including host, timeout, retry policy, parser, method, protocol,
     *                  cache policy and built url based on extra
     * @param content   The parameter for this request including headers and request body
     * @param extras    The extra parameters to build on the request url
     * @param path      The path to store the file
     * @param name      The name of the downloaded file
     * @param extension The extension of the downloaded file
     */
    public static boolean startFileRequest(String tag, RequestTarget target,
                                           Param content, String path, String name, String extension, Pair<String, String>... extras) {
        try {
            FileRequest request;
            if (BaseProperties.fileRequester == null)
                BaseProperties.fileRequester = FileRequester
                        .getInstance(BaseApplication.getContext());
            request = new FileRequest(tag, RequestTarget.type(target),
                    RequestTarget.method(target), RequestTarget.host(target), target,
                    RequestTarget.build(target, extras), content, BaseProperties.fileRequester, path, name, extension);
            FileRequester.addRequest(request);
            DLog.d(TAG, request.getRequestMethod().name().toUpperCase()
                    + " >> " + request.getUrl() + " >> " + request.getFilePath());
            return true;
        } catch (Exception ex) {
            ex.printStackTrace();
            DLog.d(TAG, "File request canceled!");
            return false;
        }
    }
}
