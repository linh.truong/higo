package com.ibs.roann.feature.menu;

import android.app.Activity;
import android.content.Context;
import android.os.Handler;
import android.support.annotation.StringRes;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.GridLayoutManager;
import android.util.AttributeSet;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.ibs.roann.R;
import com.ibs.roann.feature.ToyosuApplication;
import com.ibs.roann.feature.ToyosuMainActivity;
import com.ibs.roann.model.MenuFood;
import com.ibs.roann.model.api.FoodByCategoryParam;
import com.ibs.roann.model.api.FoodByCategoryResult;
import com.ibs.roann.view.fetchable.MutableRecycleView;
import com.ibs.roann.view.fetchable.MutableSwipeLayout;
import com.ibs.roann.view.fetchable.RecycleInterface;

import java.util.ArrayList;

import core.base.BaseActivity;
import core.base.BaseFragment;
import core.base.BaseResult;
import core.connection.WebServiceRequester;
import core.util.Constant;
import core.util.RequestTarget;
import icepick.State;

/**
 *
 */

public class ToyosuMenuCategoryItem extends LinearLayout implements RecycleInterface<MenuFood>, MutableRecycleView.OnRefreshListener, MutableRecycleView.OnLoadMoreListener, WebServiceRequester.WebServiceResultHandler {

    @State
    int category_id;

    @State
    ArrayList<MenuFood> data;

    @State
    boolean isInitialized = false;

    @State
    @StringRes
    int error_string_id = R.string.common_pull_to_reload;

    private MutableRecycleView menu_category_item_rv_list;
    private MutableSwipeLayout menu_category_item_sl_list;
    private TextView menu_screen_tv_reload;
    private ToyosuMenuCategoryFoodAdapter adapter;
    private WebServiceRequester.WebServiceResultHandler handler;


    public ToyosuMenuCategoryItem(Context context) {
        super(context);
    }

    public ToyosuMenuCategoryItem(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public ToyosuMenuCategoryItem(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }


    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        data = new ArrayList<>();
        adapter = new ToyosuMenuCategoryFoodAdapter(ToyosuApplication.getActiveActivity().getLayoutInflater(), data, this, null);
        menu_category_item_sl_list = (MutableSwipeLayout) findViewById(R.id.menu_category_item_sl_list);
        menu_category_item_rv_list = (MutableRecycleView) findViewById(R.id.menu_category_item_rv_list);
        menu_screen_tv_reload = (TextView) findViewById(R.id.menu_screen_tv_reload);
        menu_category_item_rv_list.setLayoutManager(new GridLayoutManager(ToyosuApplication.getActiveActivity(), 2));
        menu_category_item_sl_list.setEnableRefreshProgress(true);
        menu_category_item_rv_list.setColorScheme(ContextCompat.getColor(getContext(), R.color.primary));
        menu_category_item_rv_list.setAdapter(adapter);
        menu_category_item_rv_list.setOnRefreshListener(this);
        menu_category_item_rv_list.setOnLoadMoreListener(this);
    }

    @Override
    public void onResultSuccess(BaseResult result) {
        if (result instanceof FoodByCategoryResult) {
            isInitialized = true;
            if (menu_category_item_rv_list.isRefreshing()) {
                menu_category_item_rv_list.setLimit(((FoodByCategoryResult) result).getTotal());
                if (data != null) {
                    data.clear();
                    data.addAll(((FoodByCategoryResult) result).getFoods());
                    if (data.size() % 2 != 0) {
                        data.add(new MenuFood()); // footer
                    }
                    data.add(new MenuFood()); // footer
                    data.add(new MenuFood()); // footer
                }
                menu_category_item_rv_list.onRefreshComplete();
            } else if (menu_category_item_rv_list.isLoadingMore()) {
                if (data != null) {
                    ArrayList<MenuFood> temp = new ArrayList<>();
                    for (int i = 0; i < data.size(); i++) {
                        if (data.get(i).id == -1)
                            temp.add(data.get(i));
                    }
                    data.removeAll(temp);
                    data.addAll(((FoodByCategoryResult) result).getFoods());
                    if (data.size() % 2 != 0) {
                        data.add(new MenuFood()); // footer
                    }
                    data.add(new MenuFood()); // footer
                    data.add(new MenuFood()); // footer
                }
                menu_category_item_rv_list.onLoadMoreComplete();
            } else {
                adapter.notifyDataSetChanged();
            }
            if (adapter.getRealItemCount() <= 0) {
                menu_screen_tv_reload.setText(error_string_id = R.string.common_pull_to_reload);
                menu_screen_tv_reload.setVisibility(View.VISIBLE);
                menu_category_item_rv_list.setVisibility(View.GONE);
            } else {
                menu_screen_tv_reload.setVisibility(View.GONE);
                menu_category_item_rv_list.setVisibility(View.VISIBLE);
            }
            invalidate();
        }
    }

    @Override
    public void onResultFail(BaseResult result) {
        if (result instanceof FoodByCategoryResult) {
            isInitialized = true;
            if (menu_category_item_rv_list.isRefreshing()) {
                menu_category_item_rv_list.onRefreshComplete();
            } else if (menu_category_item_rv_list.isLoadingMore()) {
                menu_category_item_rv_list.onLoadMoreComplete();
            } else {
                adapter.notifyDataSetChanged();
            }
            if (adapter.getRealItemCount() <= 0) {
                menu_screen_tv_reload.setText(error_string_id = R.string.common_error_pull_to_reload);
                menu_screen_tv_reload.setVisibility(View.VISIBLE);
                menu_category_item_rv_list.setVisibility(View.GONE);
            } else {
                menu_screen_tv_reload.setVisibility(View.GONE);
                menu_category_item_rv_list.setVisibility(View.VISIBLE);
            }
            invalidate();
            handler.onResultFail(result);
        }
    }

    @Override
    public void onFail(RequestTarget target, String error, Constant.StatusCode code) {
        switch (target) {
            case FOOD_BY_CATEGORY:
                isInitialized = true;
                if (menu_category_item_rv_list.isRefreshing()) {
                    menu_category_item_rv_list.onRefreshComplete();
                } else if (menu_category_item_rv_list.isLoadingMore()) {
                    menu_category_item_rv_list.onLoadMoreComplete();
                } else {
                    adapter.notifyDataSetChanged();
                }

                if (adapter.getRealItemCount() <= 0) {
                    menu_screen_tv_reload.setText(error_string_id = R.string.common_error_pull_to_reload);
                    menu_screen_tv_reload.setVisibility(View.VISIBLE);
                    menu_category_item_rv_list.setVisibility(View.GONE);
                } else {
                    menu_screen_tv_reload.setVisibility(View.GONE);
                    menu_category_item_rv_list.setVisibility(View.VISIBLE);
                }
                invalidate();
                handler.onFail(target, error, code);
                break;

        }
    }

    @Override
    public void onItemClick(View view, MenuFood item, int position, int type) {
        if (item != null && item.id != -1) {
            Activity activity = ToyosuApplication.getActiveActivity();
            if (activity instanceof ToyosuMainActivity) {
                BaseFragment fragment = ((ToyosuMainActivity) activity).getTopFragment(((ToyosuMainActivity) activity).getMainContainerId());
                if (fragment instanceof ToyosuMenuScreen) {
                    fragment.addFragment(((ToyosuMainActivity) activity).getMainContainerId(), ToyosuFoodDetailScreen.getInstance(item));
                }
            }
        }
    }

    @Override
    public void onRefresh() {
        requestFoodByCategory(0, category_id);
    }

    @Override
    public boolean shouldOverrideLoadMore() {
        return true;
    }

    @Override
    public void onLoadMore() {
        if (data != null)
            requestFoodByCategory(adapter.getRealItemCount(), category_id);
    }

    @Override
    public boolean shouldOverrideRefresh() {
        return true;
    }

    private void requestFoodByCategory(int offset, int category_id) {
        Activity activity = ToyosuApplication.getActiveActivity();
        if (activity instanceof BaseActivity) {
            ((BaseActivity) activity).makeRequest("Page_Category_Id_" + category_id, false, new FoodByCategoryParam(offset, category_id), this, RequestTarget.FOOD_BY_CATEGORY);
        }
    }

    public void reload() {
        if (menu_category_item_rv_list != null) {
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    menu_category_item_rv_list.onRefresh();
                }
            }, 500);
        }
    }

    public boolean isInitialized() {
        return isInitialized;
    }

    public void setCategoryId(int category_id) {
        this.category_id = category_id;
    }

    public void refreshViewDataLanguage() {
        menu_screen_tv_reload.setText(error_string_id);

        if (adapter != null)
            adapter.notifyDataSetChanged();

        if (menu_category_item_rv_list != null) {
            menu_category_item_rv_list.setVisibility(GONE);
            menu_category_item_rv_list.setVisibility(VISIBLE);
            menu_category_item_rv_list.invalidate();
        }
    }

    public void setWebServiceResultHandler(WebServiceRequester.WebServiceResultHandler webServiceResultHandler) {
        this.handler = webServiceResultHandler;
    }
}
