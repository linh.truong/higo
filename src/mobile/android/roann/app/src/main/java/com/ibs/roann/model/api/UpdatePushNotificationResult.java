package com.ibs.roann.model.api;

import com.ibs.roann.feature.ToyosuBaseResult;
import com.ibs.roann.model.User;

/**
 * Created by tuanle on 1/3/17.
 */

public class UpdatePushNotificationResult extends ToyosuBaseResult {

    private User user;

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}
