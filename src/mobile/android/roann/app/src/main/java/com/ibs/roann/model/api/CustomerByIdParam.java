package com.ibs.roann.model.api;

import com.ibs.roann.feature.ToyosuBaseParam;

import org.json.JSONObject;

/**
 * Created by tuanle on 1/3/17.
 */

public class CustomerByIdParam extends ToyosuBaseParam {
    private int customerId;
    private String auth_token;

    public CustomerByIdParam(int customerId, String auth_token) {
        this.customerId = customerId;
        this.auth_token = auth_token;
    }

    @Override
    public byte[] makeRequestBody() {
        try {
            return new JSONObject().put(PARAM_CUSTOMER_ID_TAG, String.valueOf(customerId)).put(PARAM_AUTH_TOKEN_TAG, String.valueOf(auth_token)).toString().getBytes();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return new byte[0];
    }
}

