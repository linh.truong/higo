package com.ibs.roann.parser;

import com.google.gson.Gson;
import com.ibs.roann.model.UserUpdate;
import com.ibs.roann.model.api.UpdateCustomerResult;

import org.json.JSONObject;

import core.base.BaseResult;
import core.util.Constant;

/**
 * Created by tuanle on 1/3/17.
 */

public class UpdateCustomerParser extends ToyosuBaseParser {
    @Override
    public BaseResult parseData(String content) {
        UpdateCustomerResult result = new UpdateCustomerResult();

        try {
            JSONObject json = new JSONObject(content);
            result.setBackEndStatus(json.optInt(RESPONSE_ERROR_TAG));
            if (result.getBackEndStatus() == 0) {
                Gson gson = new Gson();
                JSONObject data = json.getJSONObject(RESPONSE_DATA_TAG);
                result.setUserUpdate((gson.fromJson(data.toString(), UserUpdate.class)));
                result.setStatus(Constant.StatusCode.OK);
            } else {
                result.setMessage(json.optString(RESPONSE_MESSAGE_TAG));
            }
            return result;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
}
