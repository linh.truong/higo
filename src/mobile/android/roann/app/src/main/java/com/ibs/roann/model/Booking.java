package com.ibs.roann.model;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by tuanle on 1/13/17.
 */

public class Booking implements Serializable {

    public String name;

    public String email;

    public String phone;

    public String event_id;

    public String unit_id;

    public long reserve_date = -1;

    public String reserve_time = null;

    public int number_of_seat;

    public ArrayList<Additional> additionals;

    public int number_of_adult;

    public int number_of_children;

    public int number_of_student;

    public String comment;

}
