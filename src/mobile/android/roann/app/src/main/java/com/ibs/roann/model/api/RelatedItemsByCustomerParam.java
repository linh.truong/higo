package com.ibs.roann.model.api;

import com.ibs.roann.feature.ToyosuBaseParam;

import org.json.JSONObject;

/**
 * Created by tuanle on 2/2/17.
 */

public class RelatedItemsByCustomerParam extends ToyosuBaseParam {
    private static final String PARAMS_HASH_TAGS_TAG = "hash_tags";
    private static final String PARAMS_RESTAURANT_ID_TAG = "restaurant_id";
    private static final String PARAMS_OBJECT_TYPE_TAG = "obj_type";
    private static final String PARAMS_OBJECT_ID_TAG = "obj_id";
    private static final String PARAMS_CUSTOMER_ID_TAG = "customer_id";

    private String hash_tags;
    private int restaurant_id;
    private int obj_type;
    private int obj_id;
    private int cus_id;
    private int limit;

    public RelatedItemsByCustomerParam(String hash_tags, int restaurant_id, int obj_type, int obj_id, int limit, int cus_id) {
        this.hash_tags = hash_tags;
        this.restaurant_id = restaurant_id;
        this.obj_type = obj_type;
        this.obj_id = obj_id;
        this.limit = limit;
        this.cus_id = cus_id;
    }

    @Override
    public byte[] makeRequestBody() {
        try {
            return new JSONObject()
                    .put(PARAMS_RESTAURANT_ID_TAG, restaurant_id)
                    .put(PARAMS_HASH_TAGS_TAG, hash_tags)
                    .put(PARAMS_OBJECT_ID_TAG, obj_id)
                    .put(PARAMS_OBJECT_TYPE_TAG, obj_type)
                    .put(PARAM_LIMIT_TAG, limit)
                    .put(PARAMS_CUSTOMER_ID_TAG, cus_id).toString().getBytes();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return new byte[0];
    }
}
