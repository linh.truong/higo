package com.ibs.roann.model;

import com.google.gson.annotations.SerializedName;
import com.ibs.roann.feature.ToyosuConstant;

import java.io.Serializable;

import core.data.UserDataManager;
import core.util.DataKey;

/**
 * Created by tuanle on 2/3/17.
 */

public class Prize implements Serializable {

    @SerializedName("is_delete")
    public int is_delete;

    @SerializedName("start_time")
    public String start_time;

    @SerializedName("title_cn")
    public String title_cn;

    @SerializedName("update_by")
    public String update_by;

    @SerializedName("restaurant")
    public int restaurant;

    @SerializedName("current_register")
    public int current_register;

    @SerializedName("content_cn")
    public String content_cn;

    @SerializedName("quantity")
    public int quantity;

    @SerializedName("to_date")
    public String to_date;

    @SerializedName("condition")
    public String condition;

    @SerializedName("title")
    public String title;

    @SerializedName("is_app_notify")
    public int is_app_notify;

    @SerializedName("week_days")
    public String week_days;

    @SerializedName("id")
    public int id = -1;

    @SerializedName("from_date")
    public String from_date;

    @SerializedName("is_already_send")
    public int is_already_send;

    @SerializedName("content")
    public String content;

    @SerializedName("is_mail_notify")
    public int is_mail_notify;

    @SerializedName("condition_cn")
    public String condition_cn;

    @SerializedName("image_path")
    public String image_path;

    @SerializedName("hash_tags")
    public String hash_tags;

    @SerializedName("is_sms_notify")
    public int is_sms_notify;

    @SerializedName("request_point")
    public int request_point;

    @SerializedName("end_time")
    public String end_time;

    @SerializedName("link_address")
    public String link_address;

    @SerializedName("delivery_duration_days")
    public int delivery_duration_days;

    @SerializedName("delivery_type")
    public int delivery_type;

    public String getTitle() {
        return UserDataManager.getInstance().getString(DataKey.DEVICE_LANGUAGE).equals(ToyosuConstant.DEVICE_LANGUAGE_CHINESE) ? title_cn : title;
    }

    public String getCondition() {
        return UserDataManager.getInstance().getString(DataKey.DEVICE_LANGUAGE).equals(ToyosuConstant.DEVICE_LANGUAGE_CHINESE) ? condition_cn : condition;
    }

    public String getContent() {
        return UserDataManager.getInstance().getString(DataKey.DEVICE_LANGUAGE).equals(ToyosuConstant.DEVICE_LANGUAGE_CHINESE) ? content_cn : content;
    }

}
