package com.ibs.roann.parser;

import com.google.gson.Gson;
import com.ibs.roann.model.Area;
import com.ibs.roann.model.api.AreasResult;

import org.json.JSONArray;
import org.json.JSONObject;

import core.base.BaseResult;
import core.util.Constant;

/**
 * Created by tuanle on 1/3/17.
 */

public class AreasParser extends ToyosuBaseParser {
    @Override
    public BaseResult parseData(String content) {
        AreasResult result = new AreasResult();

        try {
            JSONObject json = new JSONObject(content);
            result.setBackEndStatus(json.optInt(RESPONSE_ERROR_TAG));
            if (result.getBackEndStatus() == 0) {
                result.setTotal(json.optInt(RESPONSE_TOTAL_TAG));
                Gson gson = new Gson();
                JSONArray data = json.getJSONArray(RESPONSE_DATA_TAG);
                for (int i = 0; i < data.length(); ++i) {
                    result.getAreas().add(gson.fromJson(data.get(i).toString(), Area.class));
                }
                result.setStatus(Constant.StatusCode.OK);
            } else {
                result.setMessage(json.optString(RESPONSE_MESSAGE_TAG));
            }
            return result;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
}
