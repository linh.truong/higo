package com.ibs.roann.model.api;

import com.ibs.roann.feature.ToyosuBaseParam;

import org.json.JSONObject;

/**
 * Created by tuanle on 1/19/17.
 */

public class UpdateCustomerParam extends ToyosuBaseParam {

    private static final String PARAMS_DATA_TAG = "data";
    private static final String PARAMS_NAME_TAG = "name";
    private static final String PARAMS_EMAIL_TAG = "email";
    private static final String PARAMS_PHONE_TAG = "phone";
    private static final String PARAMS_GENDER_TAG = "sex";
    private static final String PARAMS_AGE_TAG = "age";
    private static final String PARAMS_ADDRESS_TAG = "address";
    private static final String PARAMS_GPS_TAG = "gps_data";
    private static final String PARAMS_RESTAURANT_TAG = "restaurant";
    private static final String PARAMS_ID_TAG = "id";
    private static final String PARAMS_AREA_TAG = "area";
    private static final String PARAMS_BIRTHDAY_TAG = "birthday";

    private int id;

    private int restaurant_id;

    private String address;

    private String gps_data;

    private String name;

    private String email;

    private String phone;

    private int gender;

    private int age;

    private int area;

    private String auth_token;

    private String birthday;

    public UpdateCustomerParam(int id, int restaurant_id, String address, String gps_data, String name, String email, String phone, int gender, int age, int area, String auth_token, String birthday) {
        this.id = id;
        this.restaurant_id = restaurant_id;
        this.address = address;
        this.gps_data = gps_data;
        this.name = name;
        this.email = email;
        this.phone = phone;
        this.gender = gender;
        this.age = age;
        this.area = area;
        this.auth_token = auth_token;
        this.birthday = birthday;
    }

//    @Override
//    public HashMap<String, String> makeRequestHeaders() {
//        HashMap<String, String> headers = super.makeRequestHeaders();
//        headers.put("ClientType", "android");
//        return headers;
//    }

    @Override
    public byte[] makeRequestBody() {
        try {
            return new JSONObject().put(PARAMS_DATA_TAG, new JSONObject()
                    .put(PARAMS_ID_TAG, id)
                    .put(PARAMS_GPS_TAG, gps_data)
                    .put(PARAMS_ADDRESS_TAG, address)
                    .put(PARAMS_RESTAURANT_TAG, restaurant_id)
                    .put(PARAMS_NAME_TAG, name)
                    .put(PARAMS_EMAIL_TAG, email)
                    .put(PARAMS_PHONE_TAG, phone)
                    .put(PARAMS_AREA_TAG, area)
                    .put(PARAMS_GENDER_TAG, gender)
                    .put(PARAMS_AGE_TAG, age)
                    .put(PARAM_CUSTOMER_ID_TAG, id)
                    .put(PARAM_AUTH_TOKEN_TAG, auth_token)
                    .put(PARAMS_BIRTHDAY_TAG, birthday)).toString().getBytes();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return new byte[0];
    }
}
