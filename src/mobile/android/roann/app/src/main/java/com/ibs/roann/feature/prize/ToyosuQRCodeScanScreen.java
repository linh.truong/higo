package com.ibs.roann.feature.prize;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.ibs.roann.feature.ToyosuBaseActivity;

import me.dm7.barcodescanner.zbar.Result;
import me.dm7.barcodescanner.zbar.ZBarScannerView;

/**
 * Created by tuanle on 2/3/17.
 */

public class ToyosuQRCodeScanScreen extends ToyosuBaseActivity implements ZBarScannerView.ResultHandler {

    public static final String QR_CONTENT = "qr_content";
    private ZBarScannerView scanner;

    @Override
    public void onBackPressed() {
        setResult(Activity.RESULT_CANCELED, null);
        finish();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        scanner = new ZBarScannerView(this);
        setContentView(scanner);
    }

    @Override
    protected void onResume() {
        super.onResume();
        scanner.setResultHandler(this); // Register ourselves as a handler for scan results.
        scanner.startCamera();
    }

    @Override
    protected void onPause() {
        super.onPause();
        scanner.stopCamera();
    }

    @Override
    public void onBaseCreate() {

    }

    @Override
    public void onDeepLinking(Intent data) {

    }

    @Override
    public void onNotification(Intent data) {

    }

    @Override
    public void onInitializeViewData() {

    }

    @Override
    public void onBaseResume() {

    }

    @Override
    public void onBaseFree() {

    }

    @Override
    public void onSingleClick(View v) {

    }

    @Override
    public void handleResult(Result result) {
        Intent data = new Intent();
        data.putExtra(QR_CONTENT, result.getContents());
        setResult(Activity.RESULT_OK, data);
        finish();
    }
}
