package com.ibs.roann.model;

import com.google.gson.annotations.SerializedName;
import com.ibs.roann.feature.ToyosuConstant;

import java.io.Serializable;

import core.data.UserDataManager;
import core.util.DataKey;

/**
 * Created by tuanle on 1/9/17.
 */

public class Coupon implements Serializable, RelatedItem {

    @SerializedName("id")
    public int id = -1;

    @SerializedName("restaurant")
    public int restaurant_id;

    @SerializedName("number")
    public String number;

    @SerializedName("name")
    public String name;

    @SerializedName("name_cn")
    public String name_cn;

    @SerializedName("title")
    public String title;

    @SerializedName("title_cn")
    public String title_cn;

    @SerializedName("price_off")
    public int price_off = -1;

    @SerializedName("price_minus")
    public int price_minus = -1;

    @SerializedName("from_date")
    public String from_date;

    @SerializedName("to_date")
    public String to_date;

    @SerializedName("start_time")
    public String start_time;

    @SerializedName("end_time")
    public String end_time;

    @SerializedName("week_days")
    public String week_days;

    @SerializedName("current_register")
    public int current_register;

    @SerializedName("quantity")
    public int quantity;

    @SerializedName("image_path")
    public String image_path;

    @SerializedName("image_width")
    public int image_width;

    @SerializedName("image_height")
    public int image_height;

    @SerializedName("coupon_type")
    public int coupon_type;

    @SerializedName("comment")
    public String comment;

    @SerializedName("comment_cn")
    public String comment_cn;

    @SerializedName("condition")
    public String condition;

    @SerializedName("condition_cn")
    public String condition_cn;

    @SerializedName("remain_day")
    public int remain_day;

    @SerializedName("hash_tags")
    public String hash_tags;

    @Override
    public String getName() {
        return UserDataManager.getInstance().getString(DataKey.DEVICE_LANGUAGE).equals(ToyosuConstant.DEVICE_LANGUAGE_CHINESE) ? name_cn : name;
    }

    @Override
    public String getImagePath() {
        return image_path;
    }

    public String getTitle() {
        return UserDataManager.getInstance().getString(DataKey.DEVICE_LANGUAGE).equals(ToyosuConstant.DEVICE_LANGUAGE_CHINESE) ? title_cn : title;
    }

    public String getComment() {
        return UserDataManager.getInstance().getString(DataKey.DEVICE_LANGUAGE).equals(ToyosuConstant.DEVICE_LANGUAGE_CHINESE) ? comment_cn : comment;
    }

    public String getCondition() {
        return UserDataManager.getInstance().getString(DataKey.DEVICE_LANGUAGE).equals(ToyosuConstant.DEVICE_LANGUAGE_CHINESE) ? condition_cn : condition;
    }
}
