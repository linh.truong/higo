package com.ibs.roann.feature.mypage;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.GradientDrawable;
import android.graphics.drawable.LayerDrawable;
import android.graphics.drawable.RotateDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.style.RelativeSizeSpan;
import android.text.style.StyleSpan;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.ibs.roann.R;
import com.ibs.roann.feature.ToyosuApplication;
import com.ibs.roann.feature.ToyosuBaseFragment;
import com.ibs.roann.feature.ToyosuConstant;
import com.ibs.roann.feature.ToyosuMainActivity;
import com.ibs.roann.feature.ToyosuUtils;
import com.ibs.roann.model.LegalInformation;
import com.ibs.roann.model.Setting;
import com.ibs.roann.model.User;
import com.ibs.roann.model.UserInfo;
import com.ibs.roann.model.api.CustomerByIdParam;
import com.ibs.roann.model.api.CustomerByIdResult;
import com.ibs.roann.model.api.SettingByCodeParam;
import com.ibs.roann.model.api.SettingByCodeResult;
import com.ibs.roann.view.ToyosuTextView;

import java.util.ArrayList;

import butterknife.BindView;
import core.base.BaseResult;
import core.data.UserDataManager;
import core.util.RequestTarget;
import core.util.SimpleSpanBuilder;

import static android.text.Html.fromHtml;


public class ToyosuMyPageScreen extends ToyosuBaseFragment {

    @BindView(R.id.circularProgressBar)
    ProgressBar circularProgressBar;
    @BindView(R.id.img_member)
    ImageView img_member;
    @BindView(R.id.tv_member_type)
    TextView tv_member_type;
    @BindView(R.id.tv_member_point)
    TextView tv_member_point;
    @BindView(R.id.tv_member_point_remain)
    TextView tv_member_point_remain;
    @BindView(R.id.tv_member_lv_up)
    TextView tv_member_lv_up;
    @BindView(R.id.tv_member_name)
    TextView tv_member_name;
    @BindView(R.id.tv_member_no)
    TextView tv_member_no;
    @BindView(R.id.tv_member_current_point)
    TextView tv_member_current_point;
    @BindView(R.id.tv_member_last_transaction)
    TextView tv_member_last_transaction;

    @BindView(R.id.tv_member_description)
    TextView tv_member_description;
    @BindView(R.id.my_page_name_customer_title)
    TextView my_page_name_customer_title;
    @BindView(R.id.my_page_membership_title)
    TextView my_page_membership_title;
    @BindView(R.id.my_page_current_point_title)
    TextView my_page_current_point_title;
    @BindView(R.id.my_page_last_transaction_title)
    TextView my_page_last_transaction_title;

    private User user;
    private UserInfo userInfo;

    private boolean isShowPopup = false;

    public static ToyosuMyPageScreen getInstance() {
        return new ToyosuMyPageScreen();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.toyosu_my_page_screen, container, false);
    }

    @Override
    public void onBaseCreate() {
        super.onBaseCreate();
        user = ((ToyosuMainActivity) ToyosuApplication.getActiveActivity()).getLoggedUser();
    }

    @Override
    public void onBindView() {
        super.onBindView();
    }

    @Override
    public void onInitializeViewData() {
        super.onInitializeViewData();
        requestPointMember();
//        String message = getString(R.string.customer_lb_refresh_notice);
//        showAlertDialog(getActiveActivity(), -1, getGeneralDialogLayoutResource(),
//                R.mipmap.ic_app_logo,
//                getString(R.string.common_info),
//                message, getString(R.string.common_ok)
//                , null, null);
    }

    @Override
    public int getEnterInAnimation() {
        return 0;
    }

    @Override
    public void onSingleClick(View v) {

    }

    @Override
    public void refreshViewDataLanguage() {
        tv_member_description.setText(R.string.customer_lb_status_holding_point);
        my_page_name_customer_title.setText(R.string.customer_lb_name);
        my_page_membership_title.setText(R.string.customer_lb_membership_title);
        my_page_current_point_title.setText(R.string.customer_lb_current_point_title);
        my_page_last_transaction_title.setText(R.string.customer_lb_last_transaction);
    }

    @Override
    public void reloadData() {
        requestPointMember();
    }

    private void requestPointMember() {
        Log.e("User", "requestPointMember user id: " + user.id );
        makeRequest(getUniqueTag(), true, new CustomerByIdParam(user.id, user.auth_token), this, RequestTarget.CUSTOMER_BY_ID);
    }

    @Override
    public void onResultSuccess(BaseResult result) {
        if (result instanceof CustomerByIdResult) {
            userInfo = ((CustomerByIdResult) result).getUserInfo();
            user.current_point = userInfo.current_point;
            user.total_point = userInfo.total_point;
            Log.e("current_point", "onResultSuccess: " + userInfo.current_point);
            user.last_transaction_date = userInfo.last_transaction_date;
            setLoggedUser(user);
            int point = user.total_point;

            ToyosuApplication.getImageLoader().displayImage(userInfo.mem_status_image_path, img_member);
            LayerDrawable layerDrawable = (LayerDrawable) getDrawableResource(R.drawable.progressbar_bronze);
            RotateDrawable rotateDrawable = (RotateDrawable) layerDrawable
                    .findDrawableByLayerId(R.id.progress);
            GradientDrawable gradientDrawable = (GradientDrawable) rotateDrawable.getDrawable();
            gradientDrawable.setColor(Color.parseColor(userInfo.mem_status_hex_color)); // change color
            if(userInfo.mem_status_remain_point > 0) {
                tv_member_point_remain.setText(String.valueOf(userInfo.mem_status_remain_point) + " pt");
                tv_member_lv_up.setText(ToyosuApplication.getActiveActivity().getString(R.string.customer_lb_until_status, userInfo.getMemStatusNextName()));
                tv_member_type.setText(userInfo.getMemStatusName());
                circularProgressBar.setProgressDrawable(layerDrawable);
                circularProgressBar.setProgress(1);
                circularProgressBar.setMax(userInfo.mem_status_next_level_point);
            }else{
                tv_member_point_remain.setText(ToyosuApplication.getActiveActivity().getString(R.string.customer_lb_top_level_cong));
                tv_member_lv_up.setText(ToyosuApplication.getActiveActivity().getString(R.string.customer_lb_until_top_level));
                tv_member_type.setText(userInfo.getMemStatusName());
                circularProgressBar.setProgressDrawable(layerDrawable);
                circularProgressBar.setProgress(1);
                circularProgressBar.setMax(point);
            }

            circularProgressBar.setProgress(point);
            tv_member_point.setText(String.valueOf(user.total_point));
            tv_member_name.setText(user.name);
            tv_member_no.setText(String.valueOf(user.number));
            tv_member_current_point.setText(String.valueOf(user.current_point));
            tv_member_last_transaction.setText(ToyosuUtils.formatDate(ToyosuUtils.formatDateString(user.last_transaction_date), ToyosuConstant.DATE_TIME_FORMAT));
/*
            if ( point < POINT_MEMBER_BRONZE) {
                img_member.setImageResource(R.drawable.crown_bronze);
                int point_remain = POINT_MEMBER_BRONZE - point;
                tv_member_point_remain.setText(String.valueOf(point_remain) + " pt");
                tv_member_lv_up.setText(ToyosuApplication.getActiveActivity().getString(R.string.customer_lb_until_status, ToyosuApplication.getActiveActivity().getString(R.string.customer_lb_status_silver)));
                tv_member_type.setText(ToyosuApplication.getActiveActivity().getString(R.string.customer_lb_status_bronze));
                Drawable drawable = getDrawableResource(R.drawable.progressbar_bronze);
                circularProgressBar.setProgressDrawable(drawable);
                circularProgressBar.setProgress(1);
                circularProgressBar.setMax(POINT_MEMBER_BRONZE);
            }else if (point < POINT_MEMBER_SILVER) {
                img_member.setImageResource(R.drawable.crown_silver);
                int point_remain = POINT_MEMBER_SILVER - point;
                tv_member_point_remain.setText(String.valueOf(point_remain) + " pt");
                tv_member_lv_up.setText(ToyosuApplication.getActiveActivity().getString(R.string.customer_lb_until_status, ToyosuApplication.getActiveActivity().getString(R.string.customer_lb_status_gold)));
                tv_member_type.setText(ToyosuApplication.getActiveActivity().getString(R.string.customer_lb_status_silver));
                Drawable drawable = getDrawableResource(R.drawable.progressbar_silver);
                circularProgressBar.setProgressDrawable(drawable);
                circularProgressBar.setProgress(1);
                circularProgressBar.setMax(POINT_MEMBER_SILVER);
            }else if ( point < POINT_MEMBER_GOLD) {
                img_member.setImageResource(R.drawable.ceown_gold);
                int point_remain = POINT_MEMBER_GOLD - point;
                tv_member_point_remain.setText(String.valueOf(point_remain) + " pt");
                tv_member_lv_up.setText(ToyosuApplication.getActiveActivity().getString(R.string.customer_lb_until_status, ToyosuApplication.getActiveActivity().getString(R.string.customer_lb_status_diamond)));
                tv_member_type.setText(ToyosuApplication.getActiveActivity().getString(R.string.customer_lb_status_gold));
                Drawable drawable = getDrawableResource(R.drawable.progressbar_gold);
                circularProgressBar.setProgressDrawable(drawable);
                circularProgressBar.setProgress(1);
                circularProgressBar.setMax(POINT_MEMBER_GOLD);
            }else {
                img_member.setImageResource(R.drawable.diamond);
                tv_member_point_remain.setText(ToyosuApplication.getActiveActivity().getString(R.string.customer_lb_top_level_cong));
                tv_member_lv_up.setText(ToyosuApplication.getActiveActivity().getString(R.string.customer_lb_until_top_level));
                tv_member_type.setText(ToyosuApplication.getActiveActivity().getString(R.string.customer_lb_status_diamond));
                Drawable drawable = getDrawableResource(R.drawable.progressbar_diamond);
                circularProgressBar.setProgressDrawable(drawable);
                circularProgressBar.setProgress(1);
                circularProgressBar.setMax(point);
            }
            circularProgressBar.setProgress(point);
            tv_member_point.setText(String.valueOf(user.current_point));
            tv_member_name.setText(user.name);
            tv_member_no.setText(String.valueOf(user.number));
            tv_member_last_transaction.setText(ToyosuUtils.formatDate(ToyosuUtils.formatDateString(user.last_transaction_date), ToyosuConstant.DATE_TIME_FORMAT));
*/
            if (isShowPopup){
                showDialog();
            }
        }

        if (result instanceof SettingByCodeResult) {
            LegalInformation li = ((SettingByCodeResult) result).getLegal();
            String url = li.value;
            Intent intent= new Intent(Intent.ACTION_VIEW, Uri.parse(url));
            startActivity(intent);
        }
    }

    @SuppressWarnings("deprecation")
    public Drawable getDrawableResource(int id){
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP){
            return ToyosuApplication.getContext().getDrawable(id);
        }
        else{
            return ToyosuApplication.getContext().getResources().getDrawable(id);
        }
    }

    public void openURL() {
//        makeRequest(getUniqueTag(), true, new SettingByCodeParam(SettingByCodeParam.SETTING_CODE_WEB_URL), this, RequestTarget.SETTING_BY_CODE);
        ArrayList<Setting> settings = UserDataManager.getInstance().getSettings();
        for (int i = 0; i < settings.size(); ++i) {
            Log.e("setting", "onInitializeViewData: " + settings.get(i).code );
            if (settings.get(i).code.equals(SettingByCodeParam.SETTING_CODE_WEB_URL)) {
                String url = settings.get(i).getValue();
                Intent intent= new Intent(Intent.ACTION_VIEW, Uri.parse(url));
                startActivity(intent);
                break;
            }
        }
    }

    public void shouPopupCard(){
        isShowPopup = true;
        requestPointMember();
    }

    private void showDialog() {
        isShowPopup = false;
        final Dialog dialog = new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_card);
        View decorView = dialog.getWindow().getDecorView();
        decorView.setBackgroundResource(android.R.color.transparent);
        dialog.setCancelable(true);
        dialog.setCanceledOnTouchOutside(true);

        ImageView ivDialog = (ImageView) dialog.findViewById(R.id.img_member_dialog);
        ToyosuTextView tvMemberTypeDialog = (ToyosuTextView) dialog.findViewById(R.id.tv_member_type_dialog);
        ToyosuTextView tvMemberPointDialog = (ToyosuTextView) dialog.findViewById(R.id.tv_member_point_dialog);
        ToyosuTextView tvMemberNameDialog = (ToyosuTextView) dialog.findViewById(R.id.tv_member_name_dialog);
        ToyosuTextView tvMemberNoDialog = (ToyosuTextView) dialog.findViewById(R.id.tv_member_no_dialog);
        ToyosuTextView tvMemberCurrentPointDialog = (ToyosuTextView) dialog.findViewById(R.id.tv_member_current_point_dialog);
        ToyosuTextView tvMemberStatusContentDialog = (ToyosuTextView) dialog.findViewById(R.id.tv_member_status_content_dialog);
        ToyosuTextView member_tv_tc = (ToyosuTextView) dialog.findViewById(R.id.member_tv_tc);
        member_tv_tc.setPaintFlags(member_tv_tc.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);

        ToyosuTextView tv_benefit_content_first = (ToyosuTextView ) dialog.findViewById(R.id.tv_benefit_content_first);
        ToyosuTextView tv_benefit_content_second = (ToyosuTextView) dialog.findViewById(R.id.tv_benefit_content_second);

        int point = user.total_point;

        ToyosuApplication.getImageLoader().displayImage(userInfo.mem_status_image_path, ivDialog);
        tvMemberTypeDialog.setText(userInfo.getMemStatusName());
        /*
        if ( point < POINT_MEMBER_BRONZE) {
            ivDialog.setImageResource(R.drawable.crown_bronze_big);
            tvMemberTypeDialog.setText(ToyosuApplication.getActiveActivity().getString(R.string.customer_lb_status_bronze));
        }else if (point < POINT_MEMBER_SILVER) {
            ivDialog.setImageResource(R.drawable.crown_silver_big);
            tvMemberTypeDialog.setText(ToyosuApplication.getActiveActivity().getString(R.string.customer_lb_status_silver));
        }else if ( point < POINT_MEMBER_GOLD) {
            ivDialog.setImageResource(R.drawable.crown_gold_big);
            tvMemberTypeDialog.setText(ToyosuApplication.getActiveActivity().getString(R.string.customer_lb_status_gold));
        }else {
            ivDialog.setImageResource(R.drawable.diamond_big);
            tvMemberTypeDialog.setText(ToyosuApplication.getActiveActivity().getString(R.string.customer_lb_status_diamond));
        }
        */
        tvMemberPointDialog.setText(String.valueOf(point));
        tvMemberNameDialog.setText(user.name);
        tvMemberNoDialog.setText(String.valueOf(user.number));
        tvMemberCurrentPointDialog.setText(String.valueOf(user.current_point));

        Log.e("card", "showDialog: " + userInfo.getMemStatusShortContent2() );

        String shortContent = fromHtml(userInfo.getMemStatusShortContent()).toString();
        Log.e("card", "showDialog shortContent: " + shortContent );
        String[] parts = shortContent.split(" ");
        SimpleSpanBuilder ssbShortContent = new SimpleSpanBuilder();
        int length = parts.length - 1;
        for (int i= 0; i < length; i ++){
            Log.e("card", "showDialog shortContent i: " + i + parts[i] );
            if(i == 0){
                ssbShortContent.append(parts[i], new StyleSpan(Typeface.BOLD),new RelativeSizeSpan(1.5f));
            }else if (i == 1){
                ssbShortContent.append(parts[i], new RelativeSizeSpan(1.25f));
                ssbShortContent.append("\n");
            }else{
                ssbShortContent.append(parts[i]);
            }
            if (i != length -1)
             ssbShortContent.append(" ");
        }
        Log.e("card", "ssbShortContent : " + ssbShortContent.build() );
        tv_benefit_content_first.setText(ssbShortContent.build());

        String shortContent2 = fromHtml(userInfo.getMemStatusShortContent2()).toString();
        Log.e("card", "showDialog shortContent2: " + shortContent2 );
        String[] parts2 = shortContent2.split(" ");
        SimpleSpanBuilder ssbShortContent2 = new SimpleSpanBuilder();
        int length2 = parts2.length - 1;
        for (int i= 0; i < length2; i ++){
            Log.e("card", "showDialog shortContent2 i: " + i + parts2[i] );
            if(i == 0 || i == 1){
                ssbShortContent2.append(parts2[i], new RelativeSizeSpan(1.5f));
            }else{
                ssbShortContent2.append(parts2[i]);
            }
            if (i != length2 -1)
                ssbShortContent2.append(" ");
        }
        Log.e("card", "ssbShortContent2 : " + ssbShortContent2.build() );
        tv_benefit_content_second.setText(ssbShortContent2.build());

        tv_benefit_content_first.setBackgroundColor(Color.parseColor(userInfo.mem_status_hex_color_benefit_1));
        tv_benefit_content_second.setBackgroundColor(Color.parseColor(userInfo.mem_status_hex_color_benefit_2));

        tvMemberStatusContentDialog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                showDecisionDialog(getActiveActivity(), -1,
                        getGeneralDialogLayoutResource(),
                        -1,
                        getString(R.string.other_lb_terms_and_condition),
                        userInfo.getMemStatusContent(),
                        getString(R.string.common_ok),
                        null,
                        null,
                        null, null);
            }
        });

        member_tv_tc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ArrayList<Setting> settings = UserDataManager.getInstance().getSettings();
                String code_benefit_tandc = "benefit_tandc";
                String content_benefit_tandc = null;
                for (int i = 0; i < settings.size(); ++i) {
                    if (settings.get(i).code.equals(code_benefit_tandc)) {
                        content_benefit_tandc = settings.get(i).getValue();
                        break;
                    }
                }
                dialog.dismiss();
                showDecisionDialog(getActiveActivity(), -1,
                        getGeneralDialogLayoutResource(),
                        -1,
                        getString(R.string.member_terms_and_condition),
                        content_benefit_tandc,
                        getString(R.string.common_ok),
                        null,
                        null,
                        null, null);
            }
        });

        dialog.show();
    }

}
