package com.ibs.roann.feature.menu;

import android.view.LayoutInflater;
import android.widget.ImageView;
import android.widget.TextView;

import com.ibs.roann.R;
import com.ibs.roann.feature.ToyosuApplication;
import com.ibs.roann.feature.ToyosuConstant;
import com.ibs.roann.model.MenuFood;
import com.ibs.roann.view.fetchable.RecycleAdapter;
import com.ibs.roann.view.fetchable.RecycleInterface;

import java.util.ArrayList;

import core.util.SingleTouch;

/**
 * Created by tuanle on 1/5/17.
 */

public class ToyosuMenuCategoryFoodAdapter extends RecycleAdapter<MenuFood> {

    public ToyosuMenuCategoryFoodAdapter(LayoutInflater inflater, ArrayList<MenuFood> items, RecycleInterface<MenuFood> listener, SingleTouch singleTouch) {
        super(inflater, items, listener, singleTouch);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    @Override
    public int getRealItemCount() {
        int footer_count = 0;
        for (MenuFood food : items)
            if (food.id == -1)
                footer_count++;
        return getItemCount() - footer_count;
    }

    @Override
    public int getItemViewType(int position) {
        if (items.get(position).id == -1)
            return FOOTER_TYPE;
        return ITEM_TYPE;
    }

    @Override
    protected int getHeaderLayoutResource() {
        return 0;
    }

    @Override
    protected int getItemLayoutResource() {
        return R.layout.toyosu_menu_category_food_item;
    }

    @Override
    protected int getFooterLayoutResource() {
        return R.layout.toyosu_item_footer;
    }

    @Override
    protected int getInidcatorLayoutResource() {
        return 0;
    }

    @Override
    protected void bindFooterView(FooterViewHolder<MenuFood> holder, MenuFood data, int position) {

    }

    @Override
    protected void bindIndicatorView(IndicatorViewHolder<MenuFood> holder, MenuFood data, int position) {

    }

    @Override
    protected void bindHeaderView(HeaderViewHolder<MenuFood> holder, MenuFood data, int position) {

    }

    @Override
    protected void bindItemView(ItemViewHolder<MenuFood> holder, MenuFood data, int position) {
        ToyosuApplication.getImageLoader().displayImage(data.image_path, (ImageView) holder.findViewById(R.id.menu_category_food_item_img_food));
        ((TextView) holder.findViewById(R.id.menu_category_food_item_tv_name)).setText(data.getName());
        ((TextView) holder.findViewById(R.id.menu_category_food_item_tv_price)).setText(String.format("%1$s %2$s", ToyosuConstant.DEFAULT_CURRENCY, String.valueOf(data.getPrice())));
    }
}
