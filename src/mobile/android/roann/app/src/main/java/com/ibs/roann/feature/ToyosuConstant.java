package com.ibs.roann.feature;

import core.util.Constant;

/**
 * Created by tuanle on 1/4/17.
 */

public class ToyosuConstant extends Constant {

    public static final String DEVICE_LANGUAGE_ENGLISH = "en";
    public static final String DEVICE_LANGUAGE_CHINESE = "cn";
    public static final String DATE_FORMAT_WITH_DOW = "EEEE, MMM dd, yyyy";
    public static final String DATE_FORMAT_REQUEST = "yyyy-MM-dd'T'HH:mm:ssZZZZZ";
    public static final String DATE_FORMAT_REQUEST_SIMPLYBOOK = "yyyy-MM-dd";
    public static final String TIME_FORMAT_REQUEST_SIMPLYBOOK = "HH:mm:ss";
    public static final String DATE_FORMAT_RESPONSE = "yyyy-MM-dd'T'HH:mm:ss'Z'";
    public static final String DATE_FORMAT_SHORT = "yyyy/MM/dd";
    public static final String DATE_FORMAT_LONG = "MMMM dd, yyyy";
    public static final String DATE_FORMAT_VERY_SHORT = "MMM d, yyyy";
    public static final String DATE_TIME_FORMAT = "MMMM dd, yyyy 'at' HH:mm";
    public static final String PASSWORD_NUMBER_REGEX = ".*[0-9].*";
    public static final String PASSWORD_CHARACTER_REGEX = ".*[a-z].*";

    public static final String DEFAULT_CURRENCY = "HK$";

    public static final int STATUS_LOGIN = 991;

    public static final int ERR_LOGIN_FAILED = 19;
    public static final int ERR_UNKNOWN = 99;
    public static final int INS_LACK_POINT = 37;
    public static final int  INS_EMPTY_STOCK = 38;
    public static final int  RES_FAIL = 1;
    public static final int  RES_NOT_FOUND = 2;
    public static final int  RES_EXIST = 31;

    public static final int  POINT_MEMBER_BRONZE = 1000;
    public static final int  POINT_MEMBER_SILVER = 3000;
    public static final int  POINT_MEMBER_GOLD = 7000;
    public static final int  POINT_MEMBER_MAX = 10000;

    public static final String URL_API_BOOKING = "http://user-api.simplybook.me/";

    public static final int  TIME_FRAME_BOOKING = 15;

    public static final int  QUALITY_RELATED_ITEM = 30;

    public static final int  MIN_AGE = 12;
    public static final int  MAX_AGE = 99;

    public static final int NUMBER_INSTAGRAM = 6;

    public static final String APP_VERSION = "1.0.18";
}
