package com.ibs.roann.model;

import com.google.gson.annotations.SerializedName;
import com.ibs.roann.feature.ToyosuConstant;

import java.io.Serializable;

import core.data.UserDataManager;
import core.util.DataKey;

/**
 * Created by tuanle on 1/10/17.
 */

public class CompanyInformation implements Serializable {

    @SerializedName("company_email")
    public String company_email;

    @SerializedName("company_map_url")
    public String company_map_url;

    @SerializedName("company_phone")
    public String company_phone;

    @SerializedName("company_description_cn")
    public String company_description_cn;

    @SerializedName("company_description")
    public String company_description;

    @SerializedName("company_name")
    public String company_name;

    @SerializedName("company_name_cn")
    public String company_name_cn;

    @SerializedName("company_address")
    public String company_address;

    @SerializedName("company_address_cn")
    public String company_address_cn;

    @SerializedName("privacy_policy")
    public String privacy_policy;

    @SerializedName("privacy_policy_cn")
    public String privacy_policy_cn;

    @SerializedName("company_image_path")
    public String company_image_path;

    public String getCompanyDescription() {
        return UserDataManager.getInstance().getString(DataKey.DEVICE_LANGUAGE).equals(ToyosuConstant.DEVICE_LANGUAGE_CHINESE) ? company_description_cn : company_description;
    }

    public String getCompanyName() {
        return UserDataManager.getInstance().getString(DataKey.DEVICE_LANGUAGE).equals(ToyosuConstant.DEVICE_LANGUAGE_CHINESE) ? company_name_cn : company_name;
    }

    public String getCompanyAddress() {
        return UserDataManager.getInstance().getString(DataKey.DEVICE_LANGUAGE).equals(ToyosuConstant.DEVICE_LANGUAGE_CHINESE) ? company_address_cn : company_address;
    }

    public String getPrivacyPolicy() {
        return UserDataManager.getInstance().getString(DataKey.DEVICE_LANGUAGE).equals(ToyosuConstant.DEVICE_LANGUAGE_CHINESE) ? privacy_policy_cn : privacy_policy;
    }
}
