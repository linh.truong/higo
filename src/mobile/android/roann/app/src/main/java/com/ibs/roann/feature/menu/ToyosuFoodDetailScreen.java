package com.ibs.roann.feature.menu;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.ibs.roann.R;
import com.ibs.roann.feature.ToyosuApplication;
import com.ibs.roann.feature.ToyosuBaseFragment;
import com.ibs.roann.feature.ToyosuConstant;
import com.ibs.roann.feature.ToyosuMainActivity;
import com.ibs.roann.feature.ToyosuUtils;
import com.ibs.roann.feature.coupon.ToyosuCouponDetailScreen;
import com.ibs.roann.feature.news.ToyosuTopicDetailScreen;
import com.ibs.roann.feature.related.ToyosuRelatedItemAdapter;
import com.ibs.roann.model.Coupon;
import com.ibs.roann.model.MenuFood;
import com.ibs.roann.model.RelatedItem;
import com.ibs.roann.model.Topic;
import com.ibs.roann.model.api.RelatedItemsParam;
import com.ibs.roann.model.api.RelatedItemsResult;
import com.ibs.roann.view.ToyosuImageDialog;
import com.ibs.roann.view.ToyosuRelatedItemDivider;
import com.ibs.roann.view.fetchable.RecycleInterface;

import butterknife.BindView;
import core.base.BaseResult;
import core.util.Constant;
import core.util.RequestTarget;
import icepick.State;

/**
 *
 */

public class ToyosuFoodDetailScreen extends ToyosuBaseFragment implements RecycleInterface<RelatedItem> {

    private static final String FOOD = "food";
    @BindView(R.id.menu_detail_tv_name)
    TextView menu_detail_tv_name;
    @BindView(R.id.menu_detail_img_food)
    ImageView menu_detail_img_food;
    @BindView(R.id.menu_detail_tv_price)
    TextView menu_detail_tv_price;
    @BindView(R.id.menu_detail_tv_description)
    TextView menu_detail_tv_description;
    @BindView(R.id.menu_detail_tv_related_item_title)
    TextView menu_detail_tv_related_item_title;
    @BindView(R.id.menu_detail_screen_tv_reload)
    TextView menu_detail_screen_tv_reload;
    @State
    MenuFood food;
    private RecyclerView menu_detail_screen_rv_related_item;
    private ToyosuRelatedItemAdapter adapter;
    private ToyosuImageDialog dialog;

    public static ToyosuFoodDetailScreen getInstance(MenuFood food) {
        ToyosuFoodDetailScreen screen = new ToyosuFoodDetailScreen();
        Bundle bundle = new Bundle();
        bundle.putSerializable(FOOD, food);
        screen.setArguments(bundle);
        return screen;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.toyosu_menu_detail_screen, container, false);
    }

    @Override
    public void onBaseCreate() {
        super.onBaseCreate();
        Bundle bundle = getArguments();
        if (bundle != null) {
            food = (MenuFood) bundle.getSerializable(FOOD);
        }
    }

    @Override
    public void onBindView() {
        super.onBindView();
        LinearLayoutManager lm = new LinearLayoutManager(getActiveActivity(), LinearLayoutManager.HORIZONTAL, false);
        ToyosuRelatedItemDivider divider = new ToyosuRelatedItemDivider(getActiveActivity(),
                lm.getOrientation(), (int) ToyosuUtils.convertDpToPixel(getResources().getDimension(R.dimen.related_item_space), getActiveActivity()));
        menu_detail_screen_rv_related_item = (RecyclerView) findViewById(R.id.menu_detail_screen_rv_related_item);
        menu_detail_screen_rv_related_item.setLayoutManager(lm);
        menu_detail_screen_rv_related_item.addItemDecoration(divider);
    }

    @Override
    public void onInitializeViewData() {
        super.onInitializeViewData();
        refreshViewDataLanguage();
        menu_detail_tv_price.setText(String.format("%1$s %2$s", ToyosuConstant.DEFAULT_CURRENCY, String.valueOf(food.getPrice())));
        ToyosuApplication.getImageLoader().displayImage(food.image_path, menu_detail_img_food);
        registerSingleAction(menu_detail_img_food, menu_detail_screen_tv_reload);
        requestRelatedItems();
    }

    @Override
    public void onSingleClick(View v) {
        switch (v.getId()) {
            case R.id.menu_detail_screen_tv_reload:
                requestRelatedItems();
                break;
            case R.id.menu_detail_img_food:
                dialog = new ToyosuImageDialog(getActiveActivity(), food.image_path);
                dialog.show();
                break;
        }
    }

    private void requestRelatedItems() {
        makeRequest(getUniqueTag(), true, new RelatedItemsParam(food.hash_tags, food.restaurant_id, RelatedItem.TYPE_FOOD, food.id, 10), this, RequestTarget.RELATED_ITEMS);
    }

    @Override
    public void refreshViewDataLanguage() {
        if (menu_detail_screen_tv_reload.getVisibility() == View.VISIBLE)
            menu_detail_screen_tv_reload.setText(getString(R.string.common_tap_to_reload));
        menu_detail_tv_related_item_title.setText(getActiveActivity().getResources().getString(R.string.common_related_item_title));
        menu_detail_tv_name.setText(food.getName());
        menu_detail_tv_description.setText(food.getDescription());
        if (adapter != null)
            adapter.notifyDataSetChanged();
    }

    @Override
    public void reloadData() {

    }

    @Override
    public void onBaseFree() {
        super.onBaseFree();
        unregisterSingleAction(menu_detail_img_food, menu_detail_screen_tv_reload);
    }

    @Override
    public void onResultSuccess(BaseResult result) {
        if (result instanceof RelatedItemsResult) {
            adapter = new ToyosuRelatedItemAdapter(getLayoutInflater(getArguments()), ((RelatedItemsResult) result).getItems(), this, getSingleTouch());
            menu_detail_screen_rv_related_item.setAdapter(adapter);
            menu_detail_screen_tv_reload.setVisibility(View.GONE);
            menu_detail_screen_rv_related_item.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onResultFail(BaseResult result) {
        if (result instanceof RelatedItemsResult) {
            if (adapter != null) {
                adapter.notifyDataSetChanged();
                if (adapter.getRealItemCount() <= 0) {
                    menu_detail_screen_tv_reload.setVisibility(View.VISIBLE);
                    menu_detail_screen_rv_related_item.setVisibility(View.GONE);
                } else {
                    menu_detail_screen_tv_reload.setVisibility(View.GONE);
                    menu_detail_screen_rv_related_item.setVisibility(View.VISIBLE);
                }
            } else {
                menu_detail_screen_tv_reload.setVisibility(View.VISIBLE);
                menu_detail_screen_rv_related_item.setVisibility(View.GONE);
            }
        }
        super.onResultFail(result);
    }

    @Override
    public void onFail(RequestTarget target, String error, Constant.StatusCode code) {
        if (target == RequestTarget.RELATED_ITEMS) {
            if (adapter != null) {
                adapter.notifyDataSetChanged();
                if (adapter.getRealItemCount() <= 0) {
                    menu_detail_screen_tv_reload.setVisibility(View.VISIBLE);
                    menu_detail_screen_rv_related_item.setVisibility(View.GONE);
                } else {
                    menu_detail_screen_tv_reload.setVisibility(View.GONE);
                    menu_detail_screen_rv_related_item.setVisibility(View.VISIBLE);
                }
            } else {
                menu_detail_screen_tv_reload.setVisibility(View.VISIBLE);
                menu_detail_screen_rv_related_item.setVisibility(View.GONE);
            }
        }
        super.onFail(target, error, code);
    }

    @Override
    public void onItemClick(View view, RelatedItem item, int position, int type) {
        if (item instanceof Coupon) {
            ToyosuCouponDetailScreen instance = ToyosuCouponDetailScreen.getInstance((Coupon) item);
            ((ToyosuMainActivity) getActiveActivity()).setMainContainerId(R.id.activity_main_coupon_container);
            addFragment(getMainContainerId(), instance, String.format("%1$s_%2$s", instance.getUniqueTag(), String.valueOf(System.currentTimeMillis())));
            ((ToyosuMainActivity) getActiveActivity()).toggleFooter(R.id.activity_main_footer_ll_coupon_tab);
        } else if (item instanceof Topic) {
            ToyosuTopicDetailScreen instance = ToyosuTopicDetailScreen.getInstance((Topic) item);
            ((ToyosuMainActivity) getActiveActivity()).setMainContainerId(R.id.activity_main_home_container);
            addFragment(getMainContainerId(), instance, String.format("%1$s_%2$s", instance.getUniqueTag(), String.valueOf(System.currentTimeMillis())));
            ((ToyosuMainActivity) getActiveActivity()).toggleFooter(R.id.activity_main_footer_ll_home_tab);
        } else if (item instanceof MenuFood) {
            ToyosuFoodDetailScreen instance = ToyosuFoodDetailScreen.getInstance((MenuFood) item);
            addFragment(getMainContainerId(), instance, String.format("%1$s_%2$s", instance.getUniqueTag(), String.valueOf(System.currentTimeMillis())));
        }
    }

    public MenuFood getFood() {
        return food;
    }
}
