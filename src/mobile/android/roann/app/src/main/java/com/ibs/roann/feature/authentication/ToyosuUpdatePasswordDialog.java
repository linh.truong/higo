package com.ibs.roann.feature.authentication;

import android.content.Context;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.ibs.roann.R;
import com.ibs.roann.feature.ToyosuConstant;
import com.ibs.roann.feature.ToyosuUtils;

import butterknife.BindView;
import butterknife.ButterKnife;
import core.base.BaseDialog;

/**
 * Created by tuanle on 1/25/17.
 */

public class ToyosuUpdatePasswordDialog extends BaseDialog {

    @BindView(R.id.update_password_et_old_password)
    EditText update_password_et_old_password;

    @BindView(R.id.update_password_et_new_password)
    EditText update_password_et_new_password;

    @BindView(R.id.update_password_et_confirm_password)
    EditText update_password_et_confirm_password;

    @BindView(R.id.update_password_tv_yes)
    View update_password_tv_yes;

    @BindView(R.id.update_password_tv_no)
    View update_password_tv_no;

    @BindView(R.id.update_password_tv_old)
    TextView update_password_tv_old;

    @BindView(R.id.update_password_tv_old_error)
    TextView update_password_tv_old_error;

    @BindView(R.id.update_password_tv_new)
    TextView update_password_tv_new;

    @BindView(R.id.update_password_tv_new_error)
    TextView update_password_tv_new_error;

    @BindView(R.id.update_password_tv_confirm)
    TextView update_password_tv_confirm;

    @BindView(R.id.update_password_tv_confirm_error)
    TextView update_password_tv_confirm_error;
    private OnUpdatePasswordListener listener;

    public ToyosuUpdatePasswordDialog(Context context, OnUpdatePasswordListener listener) {
        super(context);
        this.listener = listener;
        View view = View.inflate(getContext(), R.layout.toyosu_update_password_dialog, null);
        ButterKnife.bind(this, view);
        setContentView(view);
    }

    @Override
    protected void onBaseCreate() {

    }

    @Override
    protected void onBindView() {
        update_password_tv_yes.setOnClickListener(getSingleClick());
        update_password_tv_no.setOnClickListener(getSingleClick());
    }

    private boolean validated() {
        boolean result = true;
        String old_password = update_password_et_old_password.getText().toString().trim();
        if (ToyosuUtils.isEmpty(old_password)) {
            result = false;
            showInputOldPasswordError(getContext().getString(R.string.customer_register_pw_required));
        } else {
            hideInputOldPasswordError();
        }

        String password = update_password_et_new_password.getText().toString().trim();
        if (ToyosuUtils.isEmpty(password)) {
            showInputPasswordError(getContext().getString(R.string.customer_register_pw_required));
            result = false;
        } else {
            if (!(password.toLowerCase().matches(ToyosuConstant.PASSWORD_NUMBER_REGEX) && password.toLowerCase().matches(ToyosuConstant.PASSWORD_CHARACTER_REGEX))) {
                showInputPasswordError(getContext().getString(R.string.customer_lb_password_complexity));
                result = false;
            } else {
                hideInputPasswordError();
            }
        }
        String confirm = update_password_et_confirm_password.getText().toString().trim();
        if (!confirm.equals(password)) {
            showInputConfirmPasswordError(getContext().getString(R.string.customer_register_pw_not_match));
            result = false;
        } else
            hideInputConfirmPasswordError();

        return result;
    }

    @Override
    public void onSingleClick(View v) {
        switch (v.getId()) {
            case R.id.update_password_tv_yes:
                if (!validated())
                    return;
                if (listener != null)
                    listener.onUpdatePasswordClick(update_password_et_old_password.getText().toString().trim(), update_password_et_new_password.getText().toString().trim());
            case R.id.update_password_tv_no:
                dismiss();
                break;
        }
    }

    private void showInputOldPasswordError(String error) {
        update_password_tv_old_error.setVisibility(View.VISIBLE);
        update_password_tv_old.setVisibility(View.GONE);
        update_password_tv_old_error.setText(error);
    }

    private void hideInputOldPasswordError() {
        update_password_tv_old_error.setVisibility(View.GONE);
        update_password_tv_old.setVisibility(View.VISIBLE);
    }

    private void showInputPasswordError(String error) {
        update_password_tv_new_error.setVisibility(View.VISIBLE);
        update_password_tv_new.setVisibility(View.GONE);
        update_password_tv_new_error.setText(error);
    }

    private void hideInputPasswordError() {
        update_password_tv_new_error.setVisibility(View.GONE);
        update_password_tv_new.setVisibility(View.VISIBLE);
    }

    private void showInputConfirmPasswordError(String error) {
        update_password_tv_confirm_error.setVisibility(View.VISIBLE);
        update_password_tv_confirm.setVisibility(View.GONE);
        update_password_tv_confirm_error.setText(error);
    }

    private void hideInputConfirmPasswordError() {
        update_password_tv_confirm_error.setVisibility(View.GONE);
        update_password_tv_confirm.setVisibility(View.VISIBLE);
    }

    public interface OnUpdatePasswordListener {
        void onUpdatePasswordClick(String old_password, String new_password);
    }
}
