package core.base;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.AnimRes;
import android.support.annotation.DrawableRes;
import android.support.annotation.IdRes;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.util.Pair;
import android.view.View;
import android.view.ViewGroup;

import butterknife.ButterKnife;
import butterknife.Unbinder;
import core.connection.WebServiceRequester.WebServiceResultHandler;
import core.connection.queue.QueueElement;
import core.dialog.GeneralDialog.ConfirmListener;
import core.dialog.GeneralDialog.DecisionListener;
import core.util.ActionTracker;
import core.util.RequestTarget;
import core.util.SingleClick;
import core.util.SingleClick.SingleClickListener;
import core.util.SingleTouch;
import core.util.Utils;


@SuppressWarnings("unused")
public abstract class BaseFragment extends Fragment implements
        BaseInterface, SingleClickListener {

    /**
     * The flag to indicate all stack of fragments should resume when the host
     * activity is resuming. If true then all stacks will call resume, false
     * only the top fragment will call resume. Change true or false depends on
     * the behavior
     */
    private static final boolean isAllAttachedToActivityLifeCycle = false;

    /**
     * The single click to handle click action for this screen
     */
    private SingleClick singleClick = null;

    /**
     * Local active activity, in case the getActivity return null;
     */
    private BaseActivity activeActivity;

    /**
     * The unbinder of Butterknife to unbind views when the fragment view is destroyed
     */
    private Unbinder unbinder;

    /*
     * ANDROID LIFECYCLE
     */

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        onBaseCreate();
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if (view != null)
            view.setClickable(true);
        onBindView();
        onInitializeViewData();
    }

    @Override
    public void onAttach(Context activity) {
        super.onAttach(activity);
        if (activity instanceof BaseActivity) {
            activeActivity = (BaseActivity) activity;
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        if (isAllAttachedToActivityLifeCycle) {
            // EventBus.getDefault().register(this);
            ActionTracker.enterScreen(getTag(), ActionTracker.Screen.FRAGMENT);
            onBaseResume();
        } else {
            resumeCurrentFragment();
        }
    }

    @Override
    public void startActivityForResult(Intent intent, int requestCode) {
        if (getActivity() != null)
            getActivity().startActivityForResult(intent, requestCode);
        else if (getActiveActivity() != null)
            getActiveActivity().startActivityForResult(intent, requestCode);
        else
            activeActivity.startActivityForResult(intent, requestCode);
    }

    @Override
    public void startActivityForResult(Intent intent, int requestCode, @Nullable Bundle options) {
        if (getActivity() != null
                && getActivity() instanceof BaseActivity) {
            getActivity().startActivityForResult(intent, requestCode, options);
        } else if (getActiveActivity() != null
                && getActiveActivity() instanceof BaseActivity) {
            getActiveActivity().startActivityForResult(intent, requestCode, options);
        } else if (activeActivity != null) {
            activeActivity.startActivityForResult(intent, requestCode, options);
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        if (isAllAttachedToActivityLifeCycle) {
            onBasePause();
        } else {
            pauseCurrentFragment();
        }
    }


    @Override
    public void onDetach() {
        super.onDetach();
        activeActivity = null;
        onBaseFree();
        Utils.unbindDrawables(getView());
        Utils.nullViewDrawablesRecursive(getView());
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if (unbinder != null)
            unbinder.unbind();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
//        BaseApplication.getRefWatcher().watch(this);
    }

    /*
     * BASE INTERFACE
     */

    @Override
    public void onBindView() {
        unbinder = ButterKnife.bind(this, getView());
        /* Views are bind by Butterknife, override this for more actions on binding views */
    }

    @Override
    public String getResourceString(int id, Object... args) {
        if (getActivity() != null
                && getActivity() instanceof BaseActivity)
            return ((BaseActivity) getActivity())
                    .getResourceString(id, args);
        else if (getActiveActivity() != null
                && getActiveActivity() instanceof BaseActivity)
            return ((BaseActivity) getActiveActivity())
                    .getResourceString(id, args);
        else
            return activeActivity.getResourceString(id, args);
    }

    @Override
    public final void registerSingleAction(View... views) {
        for (View view : views) {
            if (view != null) {
                view.setOnClickListener(null);
                view.setOnTouchListener(null);
                if (!isExceptionalView(view)) {
                    view.setOnClickListener(getSingleClick());
                    view.setOnTouchListener(getSingleTouch());
                }
            }
        }
    }

    @Override
    public final void unregisterSingleAction(View... views) {
        for (View view : views)
            if (view != null) {
                view.setOnClickListener(null);
                view.setOnTouchListener(null);
            }
    }

    @Override
    public final void registerSingleAction(@IdRes int... ids) {
        for (int id : ids) {
            View view = findViewById(id);
            if (view != null && !isExceptionalView(view)) {
                view.setOnClickListener(null);
                view.setOnTouchListener(null);
                view.setOnClickListener(getSingleClick());
                view.setOnTouchListener(getSingleTouch());
            }
        }
    }

    @Override
    public void unregisterSingleAction(@IdRes int... ids) {
        for (int id : ids) {
            View view = findViewById(id);
            if (view != null) {
                view.setOnClickListener(null);
                view.setOnTouchListener(null);
            }
        }
    }

    @Override
    public final Activity getActiveActivity() {
        return BaseApplication.getActiveActivity();
    }

    @Override
    public final Context getBaseContext() {
        return BaseApplication.getContext();
    }

    @Override
    public final void showAlertDialog(Context context, int id, @LayoutRes int layout, @DrawableRes int icon,
                                      String title, String message, String confirm,
                                      Object onWhat, ConfirmListener listener) {
        if (getActivity() != null
                && getActivity() instanceof BaseActivity)
            ((BaseActivity) getActivity()).showAlertDialog(
                    getActivity(), id, getGeneralDialogLayoutResource(), icon, title, message, confirm, onWhat, listener);
        else if (getActiveActivity() != null
                && getActiveActivity() instanceof BaseActivity)
            ((BaseActivity) getActiveActivity())
                    .showAlertDialog(getActiveActivity(), id, getGeneralDialogLayoutResource(), icon,
                            title, message, confirm, onWhat, listener);
        else
            activeActivity.showAlertDialog(activeActivity, id, getGeneralDialogLayoutResource(), icon,
                    title, message, confirm, onWhat, listener);
    }

    @Override
    public final void showLoadingDialog(Context context, @LayoutRes int layout, String loading) {
        if (getActivity() != null
                && getActivity() instanceof BaseActivity)
            ((BaseActivity) getActivity())
                    .showLoadingDialog(getActivity(), getLoadingDialogLayoutResource(), loading);
        else if (getActiveActivity() != null
                && getActiveActivity() instanceof BaseActivity)
            ((BaseActivity) getActiveActivity())
                    .showLoadingDialog(getActiveActivity(), getLoadingDialogLayoutResource(), loading);
        else
            activeActivity.showLoadingDialog(activeActivity, getLoadingDialogLayoutResource(), loading);
    }

    @Override
    public final void showDecisionDialog(Context context, int id, @LayoutRes int layout, @DrawableRes int icon,
                                         String title, String message, String yes, String no, String cancel,
                                         Object onWhat, DecisionListener listener) {
        if (getActivity() != null
                && getActivity() instanceof BaseActivity)
            ((BaseActivity) getActivity()).showDecisionDialog(
                    getActivity(), id, getGeneralDialogLayoutResource(), 0, title, message, yes, no,
                    null, onWhat, listener);
        else if (getActiveActivity() != null
                && getActiveActivity() instanceof BaseActivity)
            ((BaseActivity) getActiveActivity())
                    .showDecisionDialog(getActiveActivity(), id, getGeneralDialogLayoutResource(), 0,
                            title, message, yes, no, null, onWhat, listener);
        else
            activeActivity.showDecisionDialog(activeActivity, id, getGeneralDialogLayoutResource(), 0,
                    title, message, yes, no, null, onWhat, listener);
    }

    @Override
    public final void closeLoadingDialog() {
        if (getActivity() != null
                && getActivity() instanceof BaseActivity)
            ((BaseActivity) getActivity()).closeLoadingDialog();
        else if (getActiveActivity() != null
                && getActiveActivity() instanceof BaseActivity)
            ((BaseActivity) getActiveActivity())
                    .closeLoadingDialog();
        else
            activeActivity.closeLoadingDialog();
    }

    @Override
    public boolean isExceptionalView(View view) {
        return BaseProperties.isExceptionalView(view);
    }

    @Override
    @SafeVarargs
    public final void makeFileRequest(String tag, String path, String name, String extension,
                                      RequestTarget target, Param content, Pair<String, String>... extras) {
        if (getActivity() != null
                && getActivity() instanceof BaseActivity)
            ((BaseActivity) getActivity())
                    .makeFileRequest(tag, path, name, extension, target, content, extras);
        else if (getActiveActivity() != null
                && getActiveActivity() instanceof BaseActivity)
            ((BaseActivity) getActiveActivity())
                    .makeFileRequest(tag, path, name, extension, target, content, extras);
        else
            activeActivity.makeFileRequest(tag, path, name, extension, target, content, extras);
    }

    @Override
    @SafeVarargs
    public final void makeRequest(String tag, boolean loading, Param content,
                                  WebServiceResultHandler handler, RequestTarget target,
                                  Pair<String, String>... extras) {
        if (getActivity() != null
                && getActivity() instanceof BaseActivity)
            ((BaseActivity) getActivity()).makeRequest(tag,
                    loading, content, handler, target, extras);
        else if (getActiveActivity() != null
                && getActiveActivity() instanceof BaseActivity)
            ((BaseActivity) getActiveActivity()).makeRequest(
                    tag, loading, content, handler, target, extras);
        else
            activeActivity.makeRequest(tag, loading, content, handler, target,
                    extras);
    }

    @Override
    @SafeVarargs
    public final void makeQueueRequest(String tag, QueueElement.Type type, Param content,
                                       RequestTarget target, Pair<String, String>... extras) {
        if (getActivity() != null
                && getActivity() instanceof BaseActivity)
            ((BaseActivity) getActivity()).makeQueueRequest(tag, type, content, target, extras);
        else if (getActiveActivity() != null
                && getActiveActivity() instanceof BaseActivity)
            ((BaseActivity) getActiveActivity()).makeQueueRequest(tag, type, content, target, extras);
        else
            activeActivity.makeQueueRequest(tag, type, content, target, extras);
    }

    @Override
    @SafeVarargs
    public final void makeParallelRequest(String tag, Param content, RequestTarget target, Pair<String, String>... extras) {
        if (getActivity() != null
                && getActivity() instanceof BaseActivity)
            ((BaseActivity) getActivity()).makeParallelRequest(tag, content, target, extras);
        else if (getActiveActivity() != null
                && getActiveActivity() instanceof BaseActivity)
            ((BaseActivity) getActiveActivity()).makeParallelRequest(tag, content, target, extras);
        else
            activeActivity.makeParallelRequest(tag, content, target, extras);
    }

    @Override
    public final void cancelWebServiceRequest(String tag) {
        if (getActivity() != null
                && getActivity() instanceof BaseActivity)
            ((BaseActivity) getActivity())
                    .cancelWebServiceRequest(tag);
        else if (getActiveActivity() != null
                && getActiveActivity() instanceof BaseActivity)
            ((BaseActivity) getActiveActivity())
                    .cancelWebServiceRequest(tag);
        else
            activeActivity.cancelWebServiceRequest(tag);
    }

    @Override
    public final SingleClick getSingleClick() {
        if (singleClick == null) {
            singleClick = new SingleClick();
            singleClick.setListener(this);
        }
        return singleClick;
    }

    @Override
    public final SingleTouch getSingleTouch() {
        if (getActivity() != null
                && getActivity() instanceof BaseActivity)
            return ((BaseActivity) getActivity())
                    .getSingleTouch();
        else if (getActiveActivity() != null
                && getActiveActivity() instanceof BaseActivity)
            return ((BaseActivity) getActiveActivity())
                    .getSingleTouch();
        else
            return activeActivity.getSingleTouch();
    }

    @LayoutRes
    @Override
    public int getGeneralDialogLayoutResource() {
        int layout;
        if (getActivity() != null
                && getActivity() instanceof BaseActivity)
            layout = ((BaseActivity) getActivity()).getGeneralDialogLayoutResource();
        else if (getActiveActivity() != null
                && getActiveActivity() instanceof BaseActivity)
            layout = ((BaseActivity) getActiveActivity()).getGeneralDialogLayoutResource();
        else
            layout = activeActivity.getGeneralDialogLayoutResource();

        return layout;
    }

    @LayoutRes
    @Override
    public int getLoadingDialogLayoutResource() {
        int layout;
        if (getActivity() != null
                && getActivity() instanceof BaseActivity)
            layout = ((BaseActivity) getActivity()).getLoadingDialogLayoutResource();
        else if (getActiveActivity() != null
                && getActiveActivity() instanceof BaseActivity)
            layout = ((BaseActivity) getActiveActivity()).getLoadingDialogLayoutResource();
        else
            layout = activeActivity.getLoadingDialogLayoutResource();

        return layout;
    }

    @AnimRes
    @Override
    public int getEnterInAnimation() {
        return -1;
    }

    @AnimRes
    @Override
    public int getBackInAnimation() {
        return -1;
    }

    @AnimRes
    @Override
    public int getEnterOutAnimation() {
        return -1;
    }

    @AnimRes
    @Override
    public int getBackOutAnimation() {
        return -1;
    }

    /*
     * BASE FRAGMENT
     */

    @IdRes
    public final int getMainContainerId() {
        if (getActivity() != null
                && getActivity() instanceof BaseActivity)
            return ((BaseActivity) getActivity())
                    .getMainContainerId();
        else if (getActiveActivity() != null
                && getActiveActivity() instanceof BaseActivity)
            return ((BaseActivity) getActiveActivity())
                    .getMainContainerId();
        else
            return activeActivity.getMainContainerId();
    }

    public String getUniqueTag() {
        return getClass().getSimpleName();
    }

    protected void onBasePause() {
        // EventBus.getDefault().unregister(this);
        cancelWebServiceRequest(null);
        closeLoadingDialog();
    }

    protected final View findViewById(@IdRes int id) {
        if (getView() != null) {
            return getView().findViewById(id);
        }
        return null;
    }

    protected final void finish() {
        if (getView() != null && getView().getParent() != null) {
            int containerId = ((ViewGroup) getView().getParent()).getId();
            if (containerId != View.NO_ID && containerId >= 0) {
                if (getActivity() != null
                        && getActivity() instanceof BaseActivity)
                    ((BaseActivity) getActivity()).backStack(
                            containerId, null);
                else if (getActiveActivity() != null
                        && getActiveActivity() instanceof BaseActivity)
                    ((BaseActivity) getActiveActivity()).backStack(
                            containerId, null);
                else
                    activeActivity.backStack(containerId, null);
            }
        }
    }

    protected final boolean requestAndroidPermissions(@NonNull String[] permissions, int requestCode) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            try {
                if (getActivity() != null
                        && getActivity() instanceof BaseActivity) {
                    ActivityCompat.requestPermissions(getActivity(), permissions, requestCode);
                } else if (getActiveActivity() != null
                        && getActiveActivity() instanceof BaseActivity) {
                    ActivityCompat.requestPermissions(getActiveActivity(), permissions, requestCode);
                } else if (activeActivity != null) {
                    ActivityCompat.requestPermissions(activeActivity, permissions, requestCode);
                }
                return true;
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return false;
    }

    public final void addMultipleFragments(@IdRes int containerId, BaseFragment... fragments) {
        if (getActivity() != null
                && getActivity() instanceof BaseActivity)
            ((BaseActivity) getActivity()).addMultipleFragments(
                    containerId, fragments);
        else if (getActiveActivity() != null
                && getActiveActivity() instanceof BaseActivity)
            ((BaseActivity) getActiveActivity()).addMultipleFragments(
                    containerId, fragments);
        else
            activeActivity.addMultipleFragments(containerId, fragments);
    }

    public final void addFragment(@IdRes int containerId, BaseFragment fragment) {
        if (getActivity() != null
                && getActivity() instanceof BaseActivity)
            ((BaseActivity) getActivity()).addFragment(
                    containerId, fragment);
        else if (getActiveActivity() != null
                && getActiveActivity() instanceof BaseActivity)
            ((BaseActivity) getActiveActivity()).addFragment(
                    containerId, fragment);
        else
            activeActivity.addFragment(containerId, fragment);
    }

    public final void addFragmentResult(@IdRes int containerId, BaseFragment fragment, int code) {
        if (getActivity() != null
                && getActivity() instanceof BaseActivity)
            ((BaseActivity) getActivity()).addFragmentResult(
                    containerId, fragment, code);
        else if (getActiveActivity() != null
                && getActiveActivity() instanceof BaseActivity)
            ((BaseActivity) getActiveActivity()).addFragmentResult(
                    containerId, fragment, code);
        else
            activeActivity.addFragmentResult(containerId, fragment, code);
    }

    public final void addFragment(@IdRes int containerId, BaseFragment fragment, String tag) {
        if (getActivity() != null
                && getActivity() instanceof BaseActivity)
            ((BaseActivity) getActivity()).addFragment(
                    containerId, fragment, tag);
        else if (getActiveActivity() != null
                && getActiveActivity() instanceof BaseActivity)
            ((BaseActivity) getActiveActivity()).addFragment(
                    containerId, fragment, tag);
        else
            activeActivity.addFragment(containerId, fragment, tag);
    }

    public final void replaceFragment(@IdRes int containerId,
                                      BaseFragment fragment, boolean clearStack) {
        if (getActivity() != null
                && getActivity() instanceof BaseActivity)
            ((BaseActivity) getActivity()).replaceFragment(
                    containerId, fragment, clearStack);
        else if (getActiveActivity() != null
                && getActiveActivity() instanceof BaseActivity)
            ((BaseActivity) getActiveActivity())
                    .replaceFragment(containerId, fragment, clearStack);
        else
            activeActivity.replaceFragment(containerId, fragment,
                    clearStack);
    }

    private void pauseCurrentFragment() {
        if (getView() != null && getView().getParent() != null) {
            int containerId = ((ViewGroup) getView().getParent()).getId();
            if (getActivity() != null
                    && getActivity() instanceof BaseActivity) {
                BaseFragment top = ((BaseActivity) getActivity())
                        .getTopFragment(containerId);
                if (top != null && !Utils.isEmpty(top.getTag())
                        && getTag().equals(top.getTag())) {
                    top.onBasePause();
                }
            } else if (getActiveActivity() != null
                    && getActiveActivity() instanceof BaseActivity) {
                BaseFragment top = ((BaseActivity) getActiveActivity())
                        .getTopFragment(containerId);
                if (top != null && !Utils.isEmpty(top.getTag())
                        && getTag().equals(top.getTag())) {
                    top.onBasePause();
                }
            } else if (activeActivity != null) {
                BaseFragment top = activeActivity
                        .getTopFragment(containerId);
                if (top != null && !Utils.isEmpty(top.getTag())
                        && getTag().equals(top.getTag())) {
                    top.onBasePause();
                }
            }
        }
    }

    private void resumeCurrentFragment() {
        if (getView() != null && getView().getParent() != null) {
            int containerId = ((ViewGroup) getView().getParent()).getId();
            if (getActivity() != null
                    && getActivity() instanceof BaseActivity) {
                BaseFragment top = ((BaseActivity) getActivity())
                        .getTopFragment(containerId);
                if (top != null && !Utils.isEmpty(top.getTag())
                        && getTag().equals(top.getTag())) {
                    // EventBus.getDefault().register(this);
                    ActionTracker.enterScreen(getTag(), ActionTracker.Screen.FRAGMENT);
                    onBaseResume();
                }
            } else if (getActiveActivity() != null
                    && getActiveActivity() instanceof BaseActivity) {
                BaseFragment top = ((BaseActivity) getActiveActivity())
                        .getTopFragment(containerId);
                if (top != null && !Utils.isEmpty(top.getTag())
                        && getTag().equals(top.getTag())) {
                    // EventBus.getDefault().register(this);
                    ActionTracker.enterScreen(getTag(), ActionTracker.Screen.FRAGMENT);
                    onBaseResume();
                }
            } else if (activeActivity != null) {
                BaseFragment top = activeActivity
                        .getTopFragment(containerId);
                if (top != null && !Utils.isEmpty(top.getTag())
                        && getTag().equals(top.getTag())) {
                    // EventBus.getDefault().register(this);
                    ActionTracker.enterScreen(getTag(), ActionTracker.Screen.FRAGMENT);
                    onBaseResume();
                }
            }
        }
    }
}
