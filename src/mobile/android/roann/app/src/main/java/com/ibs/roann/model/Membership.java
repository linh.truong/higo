package com.ibs.roann.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by tuanle on 1/23/17.
 */

public class Membership implements Serializable {

    @SerializedName("number")
    public int number;

    @SerializedName("barcode")
    public String barcode;

    @SerializedName("qr_code")
    public String qrcode;
}
