package com.ibs.roann.feature.prize;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.Editable;
import android.text.Html;
import android.util.Log;
import android.util.Patterns;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.ibs.roann.R;
import com.ibs.roann.feature.ToyosuApplication;
import com.ibs.roann.feature.ToyosuBaseFragment;
import com.ibs.roann.feature.ToyosuBaseResult;
import com.ibs.roann.feature.ToyosuConstant;
import com.ibs.roann.model.Delivery;
import com.ibs.roann.model.Prize;
import com.ibs.roann.model.Setting;
import com.ibs.roann.model.User;
import com.ibs.roann.model.api.PrizeDeliveryParam;
import com.ibs.roann.model.api.PrizeDeliveryResult;
import com.ibs.roann.view.ToyosuBookingTextDataWatcher;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import butterknife.BindView;
import core.base.BaseResult;
import core.data.UserDataManager;
import core.dialog.GeneralDialog;
import core.util.DataKey;
import core.util.RequestTarget;
import core.util.Utils;
import icepick.State;

import static com.ibs.roann.feature.ToyosuConstant.INS_EMPTY_STOCK;
import static com.ibs.roann.feature.ToyosuConstant.INS_LACK_POINT;


public class ToyosuPrizeDelivery extends ToyosuBaseFragment implements DatePickerDialog.OnDateSetListener, GeneralDialog.ConfirmListener {

    private static final String DATE_PICKER_DIALOG_TAG = DatePickerDialog.class.getSimpleName();
    private static final int POINT_DELIVERY_SUCCESS = 8003;
    private static final int POINT_DELIVERY_ERROR = 8004;
    private static final String DELIVERY = "delivery";
    private static final String PRIZE = "prize";
    private static final String PRIZE_START_DATE = "prize_start_date";
    private static final String PRIZE_END_DATE = "prize_end_date";
    
    @BindView(R.id.delivery_tv_name_title)
    TextView delivery_tv_name_title;
    @BindView(R.id.delivery_tv_name_error)
    TextView delivery_tv_name_error;
    @BindView(R.id.delivery_ed_name)
    EditText delivery_ed_name;
    @BindView(R.id.delivery_tv_email_title)
    TextView delivery_tv_email_title;
    @BindView(R.id.delivery_tv_email_error)
    TextView delivery_tv_email_error;
    @BindView(R.id.delivery_ed_email)
    EditText delivery_ed_email;
    @BindView(R.id.delivery_tv_phone_title)
    TextView delivery_tv_phone_title;
    @BindView(R.id.delivery_tv_phone_error)
    TextView delivery_tv_phone_error;
    @BindView(R.id.delivery_ed_phone)
    EditText delivery_ed_phone;
    @BindView(R.id.delivery_tv_address_title)
    TextView delivery_tv_address_title;
    @BindView(R.id.delivery_tv_address_error)
    TextView delivery_tv_address_error;
    @BindView(R.id.delivery_ed_address)
    EditText delivery_ed_address;
    @BindView(R.id.delivery_ll_receive_date)
    LinearLayout delivery_ll_receive_date;
    @BindView(R.id.delivery_tv_receive_date_title)
    TextView delivery_tv_receive_date_title;
    @BindView(R.id.delivery_tv_receive_date)
    TextView delivery_tv_receive_date;
    @BindView(R.id.delivery_tv_submit)
    TextView delivery_tv_submit;

    @State
    Prize prize;
    User user;

    @State
    Delivery delivery;

    private SimpleDateFormat format;
    private DatePickerDialog date_dialog;

    private int start_date;
    private int end_date;

    private ToyosuBookingTextDataWatcher delivery_ed_phone_watcher = new ToyosuBookingTextDataWatcher() {
        @Override
        public void afterTextChanged(Editable s) {
            delivery.phone = s.toString().trim();
        }
    };
    private ToyosuBookingTextDataWatcher delivery_ed_name_watcher = new ToyosuBookingTextDataWatcher() {
        @Override
        public void afterTextChanged(Editable s) {
            delivery.name = s.toString().trim();
        }
    };
    private ToyosuBookingTextDataWatcher delivery_ed_email_watcher = new ToyosuBookingTextDataWatcher() {
        @Override
        public void afterTextChanged(Editable s) {
            delivery.email = s.toString().trim();
        }
    };
    private ToyosuBookingTextDataWatcher delivery_ed_address_watcher = new ToyosuBookingTextDataWatcher() {
        @Override
        public void afterTextChanged(Editable s) {
            delivery.address = s.toString().trim();
        }
    };

    public static ToyosuPrizeDelivery getInstance(Prize prize) {
        ToyosuPrizeDelivery screen = new ToyosuPrizeDelivery();
        Bundle bundle = new Bundle();
        bundle.putSerializable(PRIZE, prize);
        screen.setArguments(bundle);
        return screen;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.toyosu_prize_delivery_screen, container, false);
    }

    @Override
    public void onBaseCreate() {
        super.onBaseCreate();
        Bundle bundle = getArguments();
        if (bundle != null) {
            prize = (Prize) bundle.getSerializable(PRIZE);
        }

        if (delivery == null)
            delivery = new Delivery();
    }

    @Override
    public void onBindView() {
        super.onBindView();
        
    }

    @Override
    public void onInitializeViewData() {
        super.onInitializeViewData();
        if (UserDataManager.getInstance().isEnabled(DataKey.LOGGED)) {
            User user = getLoggedUser();
            if (user != null) {
                this.user = user;
                delivery.name = user.name;
                delivery.email = user.email;
                delivery.phone = user.phone;
                delivery.delivery_type = prize.delivery_type;
            }
        }

        ArrayList<Setting> settings = UserDataManager.getInstance().getSettings();
        for (int i = 0; i < settings.size(); ++i) {
            if (settings.get(i).code.equals(PRIZE_START_DATE)) {
                start_date = Integer.valueOf(settings.get(i).value);
                Log.e("delivery", "onInitializeViewData start_date: " + start_date );
                break;
            }
        }

        for (int i = 0; i < settings.size(); ++i) {
            if (settings.get(i).code.equals(PRIZE_END_DATE)) {
                end_date = Integer.valueOf(settings.get(i).value);
                Log.e("delivery", "onInitializeViewData end_date: " + end_date );
                break;
            }
        }

        refreshViewDataLanguage();
    }

    @Override
    public void reloadData() {

    }

    private void refreshDatePicker() {
        Calendar max = Calendar.getInstance();
        Log.e("Date", "refreshDatePicker limit_days_book: " + end_date );
        max.add(Calendar.DAY_OF_YEAR, start_date + prize.delivery_duration_days + end_date);
        ArrayList<Calendar> list = new ArrayList<>();

        for (int i = 0; i < end_date; ++i) {
            Calendar tempStart = Calendar.getInstance();
            tempStart.add(Calendar.DAY_OF_YEAR, start_date + prize.delivery_duration_days);
            tempStart.add(Calendar.DAY_OF_YEAR, i);
            list.add(tempStart);
        }
        Calendar[] selectables = list.toArray(new Calendar[list.size()]);
        Calendar start = Calendar.getInstance();
        start.add(Calendar.DAY_OF_YEAR, start_date + prize.delivery_duration_days);
        date_dialog = DatePickerDialog.newInstance(this, start.get(Calendar.YEAR), start.get(Calendar.MONTH), start.get(Calendar.DAY_OF_MONTH));
        date_dialog.setMaxDate(max);
        date_dialog.setMinDate(start);
        Log.e("delivery", "start date: " + format.format(new Date(start.getTimeInMillis())) );
        delivery_tv_receive_date.setText(format.format(new Date(delivery.receive_date)));

        if (selectables != null && selectables.length > 0) {
            date_dialog.setSelectableDays(selectables);
            date_dialog.initialize(this, selectables[0].get(Calendar.YEAR), selectables[0].get(Calendar.MONTH), selectables[0].get(Calendar.DAY_OF_MONTH));
        }

        if (delivery != null) {
            if (delivery.receive_date <= 0) {
                if (delivery.receive_date == -1 || delivery.receive_date != start.getTimeInMillis()){
                    delivery.receive_date = start.getTimeInMillis();
                }
                Log.e("delivery", "onDate first: " + delivery.receive_date );
            }
            Calendar selected = Calendar.getInstance();
            selected.setTimeInMillis(delivery.receive_date);
            date_dialog.initialize(this, selected.get(Calendar.YEAR), selected.get(Calendar.MONTH), selected.get(Calendar.DAY_OF_MONTH));
        }
    }

    private void refreshDeliveryViewData() {

        if (delivery != null) {
            delivery_ed_name.setText(delivery.name);
            delivery_ed_email.setText(delivery.email);
            delivery_ed_phone.setText(delivery.phone);
            delivery_ed_address.setText(delivery.address);
//            delivery_tv_receive_date.setText(format.format(new Date(delivery.receive_date)));
        }
    }

    private void refreshErrors() {

        if (delivery_tv_name_error.getVisibility() == View.VISIBLE) {
            delivery_tv_name_error.setText(getString(R.string.customer_lb_name_required_full_name));
        }

        if (delivery_tv_phone_error.getVisibility() == View.VISIBLE) {
            delivery_tv_phone_error.setText(getString(R.string.customer_lb_phone_required));
        }

        if (delivery_tv_email_error.getVisibility() == View.VISIBLE) {
            if (Utils.isEmpty(delivery_ed_email.getText().toString().trim())) {
                delivery_tv_email_error.setText(getString(R.string.customer_lb_email_required));
            } else {
                delivery_tv_email_error.setText(getString(R.string.customer_lb_email_invalid));
            }
        }

        if (delivery_tv_address_error.getVisibility() == View.VISIBLE) {
            delivery_tv_address_error.setText(getString(R.string.customer_lb_address_required));
        }

    }

    @Override
    public void refreshViewDataLanguage() {
        format = new SimpleDateFormat(ToyosuConstant.DATE_FORMAT_WITH_DOW, ToyosuApplication.getActiveActivity().getResources().getConfiguration().locale);
        String required = "<font color=\"#EE0000\">*</font>";
        String filed_name = getString(R.string.customer_lb_name);
        String filed_email = getString(R.string.customer_lb_email);
        String filed_phone = getString(R.string.customer_lb_phone);
        String filed_address = getString(R.string.customer_lb_address);
        delivery_tv_name_title.setText(Html.fromHtml(filed_name + required));
        delivery_tv_email_title.setText(Html.fromHtml(filed_email + required));
        delivery_tv_phone_title.setText(Html.fromHtml(filed_phone + required));
        delivery_tv_address_title.setText(Html.fromHtml(filed_address + required));
        delivery_tv_receive_date_title.setText(R.string.prize_delivery_lb_receive_date);
        delivery_tv_submit.setText(R.string.other_contact_us_btn_submit);

//        refreshDatePicker();
        refreshErrors();

        refreshDeliveryViewData();
    }

    @Override
    public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {
        Calendar selected = Calendar.getInstance();
        selected.set(Calendar.YEAR, year);
        selected.set(Calendar.MONTH, monthOfYear);
        selected.set(Calendar.DAY_OF_MONTH, dayOfMonth);
        selected.set(Calendar.HOUR_OF_DAY, 0);
        selected.set(Calendar.MINUTE, 0);
        selected.set(Calendar.SECOND, 0);
        selected.set(Calendar.MILLISECOND, 0);
        Log.e("delivery", "onDateSet: " + delivery.receive_date );
        Log.e("delivery", "onDateSet selected: " + selected.getTimeInMillis() );
        if (delivery.receive_date == -1 || delivery.receive_date != selected.getTimeInMillis()){
            delivery.receive_date = selected.getTimeInMillis();
        }
        delivery_tv_receive_date.setText(format.format(new Date(delivery.receive_date)));

    }

    private void requestSubmit() {
        makeRequest(getUniqueTag(), true, new PrizeDeliveryParam(user.id,prize.id, user.auth_token, delivery), this, RequestTarget.PRIZE_DELIVERY);
    }

    @Override
    public void onResultSuccess(BaseResult result) {
        super.onResultSuccess(result);
        if (result instanceof PrizeDeliveryResult) {
            int current_point = ((PrizeDeliveryResult) result).getUsePoint().remain_point;
            int total_point = ((PrizeDeliveryResult) result).getUsePoint().total_point;
            user.current_point = current_point;
            user.total_point = total_point;
            setLoggedUser(user);
            String message = getString(R.string.other_prize_lb_used);
            showAlertDialog(getActiveActivity(), POINT_DELIVERY_SUCCESS, getGeneralDialogLayoutResource(),
                    R.mipmap.ic_app_logo,
                    getString(R.string.common_info),
                    message, getString(R.string.common_ok)
                    , null, this);
        }
    }

    @Override
    public void onResultFail(BaseResult result) {
        super.onResultFail(result);
        if (result instanceof PrizeDeliveryResult) {
            int code = ((ToyosuBaseResult) result).getBackEndStatus();
            String message = "";
            switch (code) {
                case INS_LACK_POINT:
                    message = ToyosuApplication.getActiveActivity().getString(R.string.customer_current_point_not_enough);
                    break;
                case INS_EMPTY_STOCK:
                    message = ToyosuApplication.getActiveActivity().getString(R.string.other_prize_msg_not_found);
                    break;
            }
            showAlertDialog(getActiveActivity(), POINT_DELIVERY_ERROR, getGeneralDialogLayoutResource(),
                    R.mipmap.ic_app_logo,
                    getString(R.string.common_info),
                    message, getString(R.string.common_ok)
                    , null, this);
        }
        
    }

    @Override
    public void onConfirmed(int id, Object onWhat) {
        if (id == POINT_DELIVERY_SUCCESS) {
            finish();
        }
    }

    @Override
    public void onSingleClick(View v) {
        switch (v.getId()) {
            case R.id.delivery_ll_receive_date:
                if (date_dialog != null)
                    date_dialog.show(getActivity().getFragmentManager(), DATE_PICKER_DIALOG_TAG);
                break;
            case R.id.delivery_tv_submit:
                if (validated()) {
                    requestSubmit();
                }
                break;
        }
    }

    private boolean validated() {
        boolean result = true;
        String name = delivery_ed_name.getText().toString().trim();
        if (Utils.isEmpty(name) || !name.contains(" ")) {
            showInputNameError();
            result = false;
        } else {
            hideInputNameError();
        }
        if (Utils.isEmpty(delivery_ed_email.getText().toString().trim())) {
            showInputEmailError(getString(R.string.customer_lb_email_required));
            result = false;
        } else {
            if (!Patterns.EMAIL_ADDRESS.matcher(delivery_ed_email.getText().toString().trim()).matches()) {
                showInputEmailError(getString(R.string.customer_lb_email_invalid));
                result = false;
            } else
                hideInputEmailError();
        }

        if (Utils.isEmpty(delivery_ed_phone.getText().toString().trim())) {
            showInputPhoneEmpty();
            result = false;
        } else{
            Pattern testPattern= Pattern.compile("^[0-9]{8,12}$");
            Matcher testPhone= testPattern.matcher(delivery_ed_phone.getText().toString().trim());

            if(!testPhone.matches())
            {
                showInputPhoneError();
            }else{
                hideInputPhoneError();
            }
        }

        String address = delivery_ed_address.getText().toString().trim();
        if (Utils.isEmpty(address)) {
            showInputAddressError();
            result = false;
        } else {
            hideInputAddressError();
        }

        return result;
    }

    private void hideInputPhoneError() {
        delivery_tv_phone_title.setVisibility(View.VISIBLE);
        delivery_tv_phone_error.setVisibility(View.GONE);
    }

    private void showInputPhoneEmpty() {
        delivery_tv_phone_error.setVisibility(View.VISIBLE);
        delivery_tv_phone_error.setText(getString(R.string.customer_lb_phone_required));
        delivery_tv_phone_title.setVisibility(View.GONE);
    }

    private void showInputPhoneError() {
        delivery_tv_phone_error.setVisibility(View.VISIBLE);
        delivery_tv_phone_error.setText(getString(R.string.customer_lb_phone_invalid));
        delivery_tv_phone_title.setVisibility(View.GONE);
    }

    private void hideInputEmailError() {
        delivery_tv_email_title.setVisibility(View.VISIBLE);
        delivery_tv_email_error.setVisibility(View.GONE);
    }

    private void showInputEmailError(String string) {
        delivery_tv_email_title.setVisibility(View.GONE);
        delivery_tv_email_error.setVisibility(View.VISIBLE);
        delivery_tv_email_error.setText(string);
    }

    private void hideInputNameError() {
        delivery_tv_name_title.setVisibility(View.VISIBLE);
        delivery_tv_name_error.setVisibility(View.GONE);
    }

    private void showInputNameError() {
        delivery_tv_name_title.setVisibility(View.GONE);
        delivery_tv_name_error.setVisibility(View.VISIBLE);
    }

    private void hideInputAddressError() {
        delivery_tv_address_title.setVisibility(View.VISIBLE);
        delivery_tv_address_error.setVisibility(View.GONE);
    }

    private void showInputAddressError() {
        delivery_tv_address_title.setVisibility(View.GONE);
        delivery_tv_address_error.setVisibility(View.VISIBLE);
    }

    @Override
    public void onBaseResume() {
        registerSingleAction(delivery_ll_receive_date, delivery_tv_submit);
        delivery_ed_name.addTextChangedListener(delivery_ed_name_watcher);
        delivery_ed_email.addTextChangedListener(delivery_ed_email_watcher);
        delivery_ed_phone.addTextChangedListener(delivery_ed_phone_watcher);
        delivery_ed_address.addTextChangedListener(delivery_ed_address_watcher);
        super.onBaseResume();
        Log.e("delivery", "onBaseResume");
    }

    @Override
    protected void onBasePause() {
        unregisterSingleAction(delivery_ll_receive_date, delivery_tv_submit);
        delivery_ed_name.removeTextChangedListener(delivery_ed_name_watcher);
        delivery_ed_email.removeTextChangedListener(delivery_ed_email_watcher);
        delivery_ed_phone.removeTextChangedListener(delivery_ed_phone_watcher);
        delivery_ed_address.removeTextChangedListener(delivery_ed_address_watcher);
        super.onBasePause();
    }

    public Delivery getDelivery() {
        return delivery;
    }

}
