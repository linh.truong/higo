package com.ibs.roann.view.fetchable;

import android.content.Context;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.ViewParent;
import android.view.ViewTreeObserver;
import android.widget.RelativeLayout;


public class MutableRecycleView extends RecyclerView implements SwipeRefreshLayout.OnRefreshListener {

    private MutableSwipeLayout refresher;
    private LinearLayoutManager layoutManager;
    private OnScrollListener onScrollListener;
    private OnRefreshListener onRefreshListener;
    private OnLoadMoreListener onLoadMoreListener;
    private RecycleAdapter adapter;
    private boolean isLoadingMore;
    private boolean isRefreshing;
    private boolean isFirstLoadSkipped;
    private int limit;

    public MutableRecycleView(Context context) {
        super(context);
        init();
    }

    public MutableRecycleView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public MutableRecycleView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                ViewParent parent = getParent();
                if (parent != null && parent instanceof RelativeLayout) {
                    ViewParent grand = parent.getParent();
                    if (grand != null && grand instanceof MutableSwipeLayout) {
                        refresher = (MutableSwipeLayout) grand;
                        refresher.setEnabled(false);
                        refresher.setNestedScrollingEnabled(true);
                        refresher.setOnRefreshListener(MutableRecycleView.this);
                        refresher.setEnableRefreshProgress(false);
                        getViewTreeObserver().removeOnGlobalLayoutListener(this);
                    }
                }
            }
        });
    }

    private void init() {
        setHasFixedSize(true);
        setLayoutManager(new LinearLayoutManager(getContext()));
        isRefreshing = false;
        isLoadingMore = false;
    }

    public void setColorScheme(final int... colors) {
        if (refresher != null) {
            refresher.setColorSchemeColors(colors);
        } else {
            getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
                @Override
                public void onGlobalLayout() {
                    if (refresher != null) {
                        refresher.setColorSchemeColors(colors);
                        getViewTreeObserver().removeOnGlobalLayoutListener(this);
                    }
                }
            });
        }
    }

    @Override
    public void setOnScrollListener(OnScrollListener onScrollListener) {
        super.setOnScrollListener(onScrollListener);
        this.onScrollListener = onScrollListener;
    }

    public void setOnRefreshListener(OnRefreshListener onRefreshListener) {
        if (onRefreshListener != null) {
            this.onRefreshListener = onRefreshListener;
            getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
                @Override
                public void onGlobalLayout() {
                    if (refresher != null) {
                        refresher.setEnableRefreshProgress(true);
                        getViewTreeObserver().removeOnGlobalLayoutListener(this);
                    }
                }
            });
        } else {
            this.onRefreshListener = null;
            if (refresher != null) {
                refresher.setEnableRefreshProgress(false);
            }
        }
    }

    public void setOnLoadMoreListener(OnLoadMoreListener onLoadMoreListener) {
        this.onLoadMoreListener = onLoadMoreListener;
    }

    @Override
    public void setAdapter(Adapter adapter) {
        super.setAdapter(adapter);
        if (adapter instanceof RecycleAdapter)
            this.adapter = (RecycleAdapter) adapter;
    }

    public void setLayoutManager(LinearLayoutManager layoutManager) {
        super.setLayoutManager(layoutManager);
        this.layoutManager = layoutManager;
    }

    @Override
    public boolean onTouchEvent(MotionEvent e) {
        isFirstLoadSkipped = true;
        return super.onTouchEvent(e);
    }

    @Override
    public void onScrolled(int dx, int dy) {
        super.onScrolled(dx, dy);

        if (onScrollListener != null)
            onScrollListener.onScrolled(this, dx, dy);

        if (refresher != null && layoutManager != null) {
            if (layoutManager.findFirstCompletelyVisibleItemPosition() <= 0) {
                refresher.setEnabled(true);
            } else {
//                refresher.setEnabled(false);
            }
        }

        if (onLoadMoreListener != null && adapter != null && layoutManager != null && isFirstLoadSkipped) {
            int totalItemCount = adapter.getRealItemCount();
            int lastVisible = layoutManager.findLastCompletelyVisibleItemPosition();
            boolean shouldLoadMore = ((lastVisible + 1) >= totalItemCount) && totalItemCount < limit;

            if (shouldLoadMore) {
                if (!isLoadingMore) {
                    if (isRefreshing) {
                        if (onLoadMoreListener.shouldOverrideRefresh()) {
                            interruptRefresh();
                            isLoadingMore = true;
                            onLoadMoreListener.onLoadMore();
                            if (refresher != null)
                                refresher.startLoadingAnimation();
                        }
                    } else {
                        interruptRefresh();
                        isLoadingMore = true;
                        onLoadMoreListener.onLoadMore();
                        if (refresher != null)
                            refresher.startLoadingAnimation();
                    }
                }
            }
        }
    }

    public void setLimit(int limit) {
        this.limit = limit;
    }

    public boolean isRefreshing() {
        return isRefreshing;
    }

    public boolean isLoadingMore() {
        return isLoadingMore;
    }

    public void onLoadMore() {
        if (!isLoadingMore) {
            isLoadingMore = true;
            if (onLoadMoreListener != null && refresher != null) {
                refresher.startLoadingAnimation();
                onLoadMoreListener.onLoadMore();
            }
        }
    }

    public void onLoadMoreComplete() {
        interruptLoadMore();
        if (adapter != null)
            adapter.notifyDataSetChanged();
    }

    @Override
    public void onRefresh() {
        if (onRefreshListener != null && refresher != null) {
            if (!isRefreshing) {
                if (isLoadingMore) {
                    if (onRefreshListener.shouldOverrideLoadMore()) {
                        interruptLoadMore();
                        refresher.setRefreshing(true);
                        isRefreshing = true;
                        onRefreshListener.onRefresh();
                    } else {
                        refresher.setRefreshing(false);
                    }
                } else {
                    interruptLoadMore();
                    refresher.setRefreshing(true);
                    isRefreshing = true;
                    onRefreshListener.onRefresh();
                }
            }
        }
    }

    public void onRefreshComplete() {
        interruptRefresh();
        if (adapter != null)
            adapter.notifyDataSetChanged();
    }

    private void interruptRefresh() {
        if (refresher != null) {
            refresher.setRefreshing(false);
        }
        isRefreshing = false;
    }

    private void interruptLoadMore() {
        if (refresher != null)
            refresher.stopLoadingAnimation();
        isLoadingMore = false;
    }

    public interface OnRefreshListener {

        void onRefresh();

        boolean shouldOverrideLoadMore();
    }

    public interface OnLoadMoreListener {

        void onLoadMore();

        boolean shouldOverrideRefresh();
    }
}