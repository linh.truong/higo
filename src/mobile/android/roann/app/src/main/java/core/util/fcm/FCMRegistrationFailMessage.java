package core.util.fcm;

import java.io.Serializable;

public class FCMRegistrationFailMessage implements Serializable {

    private boolean isRecoverable;

    public FCMRegistrationFailMessage(boolean isRecoverable) {
        this.isRecoverable = isRecoverable;
    }

    public boolean isRecoverable() {
        return isRecoverable;
    }
}
