package com.ibs.roann.parser;

import com.google.gson.Gson;
import com.ibs.roann.model.Coupon;
import com.ibs.roann.model.MenuFood;
import com.ibs.roann.model.RelatedItem;
import com.ibs.roann.model.Topic;
import com.ibs.roann.model.api.RelatedItemsResult;

import org.json.JSONArray;
import org.json.JSONObject;

import core.base.BaseResult;
import core.util.Constant;

/**
 * Created by tuanle on 2/2/17.
 */

public class RelatedItemsParser extends ToyosuBaseParser {

    private static final String RESPONSE_OBJECT_TYPE_TAG = "object_type";

    @Override
    public BaseResult parseData(String content) {
        RelatedItemsResult result = new RelatedItemsResult();

        try {
            JSONObject json = new JSONObject(content);
            result.setBackEndStatus(json.optInt(RESPONSE_ERROR_TAG));
            if (result.getBackEndStatus() == 0) {
                Gson gson = new Gson();
                JSONArray data = json.getJSONArray(RESPONSE_DATA_TAG);
                for (int i = 0; i < data.length(); ++i) {
                    int type = data.getJSONObject(i).optInt(RESPONSE_OBJECT_TYPE_TAG);
                    switch (type) {
                        case RelatedItem.TYPE_COUPON:
                            result.getItems().add(gson.fromJson(data.get(i).toString(), Coupon.class));
                            break;
                        case RelatedItem.TYPE_FOOD:
                            result.getItems().add(gson.fromJson(data.get(i).toString(), MenuFood.class));
                            break;
                        case RelatedItem.TYPE_TOPIC:
                            result.getItems().add(gson.fromJson(data.get(i).toString(), Topic.class));
                            break;
                    }
                }
                result.setStatus(Constant.StatusCode.OK);
            } else {
                result.setMessage(json.optString(RESPONSE_MESSAGE_TAG));
            }
            return result;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
}
