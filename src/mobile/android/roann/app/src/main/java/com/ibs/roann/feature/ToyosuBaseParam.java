package com.ibs.roann.feature;

import core.base.BaseParam;

/**
 * Created by tuanle on 1/3/17.
 */

public abstract class ToyosuBaseParam extends BaseParam {

    public static final int LIST_LIMIT = 5;
    public static final String PARAM_LIMIT_TAG = "limit";
    public static final String PARAM_PRIZE_ID_TAG = "prize_id";
    public static final String PARAM_CUSTOMER_ID_TAG = "id";
    public static final String PARAM_AUTH_TOKEN_TAG = "auth_token";
    public static final String PARAM_QR_CODE_TAG = "qr_code";
    public static final String PARAM_OFFSET_TAG = "offset";
    private static final String JSON_CONTENT_TYPE_VALUE = "application/json";

    @Override
    public String makeBodyContentType() {
        return JSON_CONTENT_TYPE_VALUE;
    }
}
