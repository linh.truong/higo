package core.data;

import android.content.Context;
import android.content.SharedPreferences;

import com.ibs.roann.model.Setting;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Set;

import core.base.BaseApplication;
import core.util.DataKey;
import core.util.Utils;

/**
 * @author Tyrael
 * @version 1.0 <br>
 *          <br>
 *          <b>Class Overview</b> <br>
 *          <br>
 *          Represents a class for storing data to the shared preference <br>
 * @since January 2014
 */

@SuppressWarnings({"BooleanMethodIsAlwaysInverted", "SameParameterValue", "UnusedReturnValue", "UnusedParameters"})
public class UserDataManager {

    /**
     * The name of this storage in the system
     */
    private static final String KEY_SHARED_PREFERENCES = Utils.getSharedPreferenceKey();

    /**
     * Represent the instance of this class, only one instance can be used at a
     * time and apply for the entire application
     */
    private static UserDataManager instance;

    /**
     * The reference to SharedPreferences which actually read and write the data
     * to the storage
     */
    private final SharedPreferences prefs;

    private ArrayList<Setting> settings;

    private UserDataManager() {
        prefs = BaseApplication.getContext().getSharedPreferences(
                KEY_SHARED_PREFERENCES, Context.MODE_PRIVATE);
        this.settings = new ArrayList<>();
    }

    /**
     * This method will return the instance of this class, only one instance can
     * be used at a time
     *
     * @return The instance of this class
     */
    public static synchronized UserDataManager getInstance() {
        if (instance == null) {
            instance = new UserDataManager();
        }
        return instance;
    }

    /**
     * This method is to clear all the share preference data
     */
    public synchronized boolean clear() {
        if (prefs != null)
            return prefs.edit().clear().commit();
        return false;
    }

    /**
     * This method is to clear all the share preference data and ignore some keys
     */
    public synchronized boolean clear(DataKey... ignores) {
        if (prefs != null) {
            SharedPreferences.Editor edit = prefs.edit();
            HashMap<DataKey, Object> reserved = new HashMap<>();
            for (DataKey key : ignores) {
                Object data = prefs.getAll().get(key.name());
                reserved.put(key, data);
            }
            boolean cleared = edit.clear().commit();
            if (cleared) {
                for (DataKey key : reserved.keySet()) {
                    Object data = reserved.get(key);
                    if (data instanceof Integer) {
                        edit.putInt(key.name(), (int) data);
                    } else if (data instanceof String) {
                        edit.putString(key.name(), (String) data);
                    } else if (data instanceof Float) {
                        edit.putFloat(key.name(), (float) data);
                    } else if (data instanceof Boolean) {
                        edit.putBoolean(key.name(), (boolean) data);
                    } else if (data instanceof Long) {
                        edit.putLong(key.name(), (long) data);
                    } else if (data instanceof Set) {
                        edit.putStringSet(key.name(), (Set) data);
                    }
                }
                return edit.commit();
            } else
                return false;
        }
        return false;
    }

    /**
     * This method is to set the String Set value to the storage base on the KEY
     *
     * @param key   The key for the value, defined in <code>enum DataKey</code>
     * @param value The value for this key as a long
     * @return true if the process is success, false otherwise
     */
    public synchronized boolean setStringSet(DataKey key, Set<String> value) {
        if (prefs != null)
            return prefs.edit().putStringSet(key.name(), value).commit();
        return false;
    }

    /**
     * This method is to get a STRING SET value from the storage base on the KEY
     *
     * @param key The key for the value, defined in <code>enum DataKey</code>
     * @return The value of this key
     */
    public synchronized Set<String> getStringSet(DataKey key) {
        if (prefs != null)
            return prefs.getStringSet(key.name(), null);
        return null;
    }

    /**
     * This method is to set the Long value to the storage base on the KEY
     *
     * @param key   The key for the value, defined in <code>enum DataKey</code>
     * @param value The value for this key as a long
     * @return true if the process is success, false otherwise
     */
    public synchronized boolean setLong(DataKey key, long value) {
        if (prefs != null)
            return prefs.edit().putLong(key.name(), value).commit();
        return false;
    }

    /**
     * This method is to get a LONG value from the storage base on the KEY
     *
     * @param key The key for the value, defined in <code>enum DataKey</code>
     * @return The value of this key
     */
    public synchronized long getLong(DataKey key) {
        if (prefs != null)
            return prefs.getLong(key.name(), 0);
        return 0;
    }

    /**
     * This method is to set the FLOAT value to the storage base on the KEY
     *
     * @param key   The key for the value, defined in <code>enum DataKey</code>
     * @param value The value for this key as a float
     * @return true if the process is success, false otherwise
     */
    public synchronized boolean setFloat(DataKey key, float value) {
        if (prefs != null)
            return prefs.edit().putFloat(key.name(), value).commit();
        return false;
    }

    /**
     * This method is to get a FLOAT value from the storage base on the KEY
     *
     * @param key The key for the value, defined in <code>enum DataKey</code>
     * @return The value of this key
     */
    public synchronized float getFloat(DataKey key) {
        if (prefs != null)
            return prefs.getFloat(key.name(), 0f);
        return 0f;
    }

    /**
     * This method is to set the String value to the storage base on the KEY
     *
     * @param key   The key for the value, defined in <code>enum DataKey</code>
     * @param value The value for this key as a string
     * @return true if the process is success, false otherwise
     */
    public synchronized boolean setString(DataKey key, String value) {
        if (prefs != null)
            return prefs.edit().putString(key.name(), value).commit();
        return false;
    }

    /**
     * This method is to get a STRING value from the storage base on the KEY
     *
     * @param key The key for the value, defined in <code>enum DataKey</code>
     * @return The value of this key
     */
    public synchronized String getString(DataKey key) {
        if (prefs != null)
            return prefs.getString(key.name(), null);
        return null;
    }

    /**
     * This method is to get a INTEGER value from the storage base on the KEY
     *
     * @param key The key for the value, defined in <code>enum DataKey</code>
     * @return The value of this key
     */
    public synchronized int getInt(DataKey key) {
        if (prefs != null)
            return prefs.getInt(key.name(), 0);
        return 0;
    }

    /**
     * This method is to set the INTEGER value to the storage base on the KEY
     *
     * @param key   The key for the value, defined in <code>enum DataKey</code>
     * @param value The value for this key as a int
     * @return true if the process is success, false otherwise
     */
    public synchronized boolean setInt(DataKey key, int value) {
        if (prefs != null)
            return prefs.edit().putInt(key.name(), value).commit();
        return false;
    }

    /**
     * This method is to set the BOOLEAN value to the storage base on the KEY
     *
     * @param key   The key for the value, defined in <code>enum DataKey</code>
     * @param value The value for this key as
     *              <code>true<code> or <code>false<code>
     * @return true if the process is success, false otherwise
     */
    public synchronized boolean setEnabled(DataKey key, boolean value) {
        if (prefs != null)
            return prefs.edit().putBoolean(key.name(), value).commit();
        return false;
    }

    /**
     * This method is to get a BOOLEAN value from the storage base on the KEY
     *
     * @param key The key for the value, defined in <code>enum DataKey</code>
     * @return The value of this key
     */

    public synchronized boolean isEnabled(DataKey key) {
        if (prefs != null)
            return prefs.getBoolean(key.name(), false);
        return false;
    }

    public synchronized ArrayList<Setting> getSettings() {
        return this.settings;
    }

    public synchronized void setSettings(ArrayList<Setting> settings) {
        this.settings = settings;
    }

}
