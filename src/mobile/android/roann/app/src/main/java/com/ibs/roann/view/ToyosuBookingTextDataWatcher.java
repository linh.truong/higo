package com.ibs.roann.view;

import android.text.TextWatcher;

/**
 * Created by tuanle on 1/16/17.
 */

public abstract class ToyosuBookingTextDataWatcher implements TextWatcher {

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {

    }
}
