package com.ibs.roann.model.api;

import com.ibs.roann.feature.ToyosuBaseParam;

import org.json.JSONObject;


public class CouponsByCusIdParam extends ToyosuBaseParam {

    public static final String PARAM_CUSTOMER_ID_TAG = "cusId";

    private int offset;
    private int cusId;

    public CouponsByCusIdParam(int offset, int cusId) {
        this.offset = offset;
        this.cusId = cusId;
    }

    @Override
    public byte[] makeRequestBody() {
        try {
            return new JSONObject().put(PARAM_LIMIT_TAG, String.valueOf(LIST_LIMIT)).put(PARAM_OFFSET_TAG, String.valueOf(offset)).put(PARAM_CUSTOMER_ID_TAG, String.valueOf(cusId)).toString().getBytes();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return new byte[0];
    }
}

