package com.ibs.roann.parser;

import core.base.BaseParser;

/**
 *
 */

public abstract class ToyosuBaseParser extends BaseParser {

    protected static final String RESPONSE_TOTAL_TAG = "total";
    protected static final String RESPONSE_MESSAGE_TAG = "message";
    protected static final String RESPONSE_DATA_TAG = "data";
    protected static final String RESPONSE_ERROR_TAG = "error";
    protected static final String RESPONSE_POINT_TAG = "point";
    protected static final String RESPONSE_ADDITIONAL_TAG = "additional";
    protected static final String RESPONSE_SERVICE_TAG = "event";
    protected static final String RESPONSE_PROVIDER_TAG = "unit";
    protected static final String RESPONSE_WORKING_DATE_TAG = "working_date";
    protected static final String RESPONSE_TOTAL_POINT_TAG = "total_point";
    protected static final String RESPONSE_CURRENT_POINT_TAG = "current_point";
    protected static final String RESPONSE_RES_POINT_TAG = "res_point";

}
