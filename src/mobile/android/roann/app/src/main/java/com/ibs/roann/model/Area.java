package com.ibs.roann.model;

import com.google.gson.annotations.SerializedName;
import com.ibs.roann.feature.ToyosuConstant;

import java.io.Serializable;

import core.data.UserDataManager;
import core.util.DataKey;

/**
 * Created by tuanle on 1/19/17.
 */

public class Area implements Serializable {

    @SerializedName("update_by")
    public String update_by;

    @SerializedName("name_cn")
    public String name_cn;

    @SerializedName("name")
    public String name;

    @SerializedName("id")
    public int id;

    @SerializedName("is_delete")
    public int is_delete;

    public String getName() {
        return UserDataManager.getInstance().getString(DataKey.DEVICE_LANGUAGE).equals(ToyosuConstant.DEVICE_LANGUAGE_CHINESE) ? name_cn : name;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof Area)
            return ((Area) obj).id == id;
        return false;
    }
}
