package com.ibs.roann.model.api;

import com.ibs.roann.feature.ToyosuBaseResult;
import com.ibs.roann.model.Setting;

import java.util.ArrayList;

/**
 * Created by tuanle on 1/3/17.
 */

public class SettingsResult extends ToyosuBaseResult {

    private int total;

    private ArrayList<Setting> settings;

    public SettingsResult() {
        settings = new ArrayList<>();
    }

    public ArrayList<Setting> getSettings() {
        return settings;
    }

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }
}
