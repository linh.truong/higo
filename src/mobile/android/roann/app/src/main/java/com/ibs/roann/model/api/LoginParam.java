package com.ibs.roann.model.api;

import com.ibs.roann.feature.ToyosuBaseParam;

import org.json.JSONObject;

/**
 * Created by tuanle on 1/3/17.
 */

public class LoginParam extends ToyosuBaseParam {

    private static final String PARAMS_USERNAME_TAG = "username";
    private static final String PARAMS_PASSWORD_TAG = "password";

    private String email;
    private String password;

    public LoginParam(String email, String password) {
        this.email = email;
        this.password = password;
    }

    @Override
    public byte[] makeRequestBody() {
        try {
            return new JSONObject().put(PARAMS_USERNAME_TAG, email).put(PARAMS_PASSWORD_TAG, password).toString().getBytes();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return new byte[0];
    }
}

