package com.ibs.roann.feature.coupon;

import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.annotation.StringRes;
import android.support.v4.content.res.ResourcesCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.ibs.roann.R;
import com.ibs.roann.feature.ToyosuApplication;
import com.ibs.roann.feature.ToyosuBaseFragment;
import com.ibs.roann.feature.ToyosuMainActivity;
import com.ibs.roann.model.Coupon;
import com.ibs.roann.model.Message;
import com.ibs.roann.model.User;
import com.ibs.roann.model.api.CouponsByCusIdParam;
import com.ibs.roann.model.api.CouponsResult;
import com.ibs.roann.view.fetchable.MutableRecycleView;
import com.ibs.roann.view.fetchable.MutableSwipeLayout;
import com.ibs.roann.view.fetchable.RecycleInterface;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;

import butterknife.BindView;
import core.base.BaseResult;
import core.util.Constant;
import core.util.RequestTarget;
import icepick.State;

/**
 * Created by tuanle on 1/3/17.
 */

public class ToyosuCouponScreen extends ToyosuBaseFragment implements MutableRecycleView.OnRefreshListener, MutableRecycleView.OnLoadMoreListener, RecycleInterface<Coupon> {

    @State
    ArrayList<Coupon> data;
    User user;
    @BindView(R.id.coupon_screen_tv_reload)
    TextView coupon_screen_tv_reload;
    @State
    @StringRes
    int error_string_id = R.string.common_pull_to_reload;
    private MutableSwipeLayout coupon_screen_sl_list;
    private MutableRecycleView coupon_screen_rv_list;
    private ToyosuCouponAdapter adapter;

    public static ToyosuCouponScreen getInstance() {
        return new ToyosuCouponScreen();
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        return inflater.inflate(R.layout.toyosu_coupon_screen, container, false);
    }

    @Override
    public void onBaseCreate() {
        super.onBaseCreate();
        data = new ArrayList<>();
        user = ((ToyosuMainActivity) ToyosuApplication.getActiveActivity()).getLoggedUser();
        adapter = new ToyosuCouponAdapter(ToyosuApplication.getActiveActivity().getLayoutInflater(), data, this, getSingleTouch());
        EventBus.getDefault().register(this);

    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessage(Message event){
        if (event.getMessage().equals("remove coupon")){
            Log.e("event bus", "onMessage: " + event.getType() );
            int coupon_id = event.getType();
            int index = 0;
            for (Coupon c: data){
                if( c.id == coupon_id){
                    data.remove(index);
                    adapter.notifyDataSetChanged();
                    break;
                }
                index++;
            }

        }
    }

    @Override
    public void onBindView() {
        super.onBindView();
        coupon_screen_sl_list = (MutableSwipeLayout) findViewById(R.id.coupon_screen_sl_list);
        coupon_screen_rv_list = (MutableRecycleView) findViewById(R.id.coupon_screen_rv_list);
        coupon_screen_sl_list.setEnableRefreshProgress(true);
        coupon_screen_rv_list.setColorScheme(ResourcesCompat.getColor(getResources(), R.color.primary, null));
        coupon_screen_rv_list.setOnRefreshListener(this);
        coupon_screen_rv_list.setOnLoadMoreListener(this);
        coupon_screen_rv_list.setAdapter(adapter);
    }

    @Override
    public int getEnterInAnimation() {
        return 0;
    }

    @Override
    public void onInitializeViewData() {
        super.onInitializeViewData();
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                coupon_screen_rv_list.onRefresh();
            }
        }, 1000);

    }

    @Override
    public void reloadData() {

    }

    @Override
    public void onRefresh() {
        requestCoupons(0);
    }

    private void requestCoupons(int offset) {
        makeRequest(getUniqueTag(), false, new CouponsByCusIdParam(offset, user.id), this, RequestTarget.COUPONS_BY_CUS_ID);
    }

    @Override
    public boolean shouldOverrideLoadMore() {
        return true;
    }

    @Override
    public void onLoadMore() {
        if (data != null)
            requestCoupons(adapter.getRealItemCount());
    }

    @Override
    public boolean shouldOverrideRefresh() {
        return true;
    }

    @Override
    public void onItemClick(View view, Coupon item, int position, int type) {
        if (item != null && item.id != -1)
            addFragment(getMainContainerId(), ToyosuCouponDetailScreen.getInstance(item));
    }

    @Override
    public void onResultSuccess(BaseResult result) {
        if (result instanceof CouponsResult) {
            if (coupon_screen_rv_list.isRefreshing()) {
                coupon_screen_rv_list.setLimit(((CouponsResult) result).getTotal());
                if (data != null) {
                    if (data != null) {
                        data.clear();
                        Coupon header = new Coupon();
                        header.id = -2;
                        data.add(header); // header
                        data.addAll(((CouponsResult) result).getCoupons());
                        data.add(new Coupon()); // footer
                    }
                }
                coupon_screen_rv_list.onRefreshComplete();
            } else if (coupon_screen_rv_list.isLoadingMore()) {
                if (data != null) {
                    for (int i = 0; i < data.size(); i++) {
                        if (data.get(i).id == -1)
                            data.remove(i);
                    }
                    data.addAll(((CouponsResult) result).getCoupons());
                    data.add(new Coupon()); // footer
                }
                coupon_screen_rv_list.onLoadMoreComplete();
            } else {
                adapter.notifyDataSetChanged();
            }

            if (adapter.getRealItemCount() <= 0) {
                coupon_screen_tv_reload.setText(error_string_id = R.string.common_pull_to_reload);
                coupon_screen_tv_reload.setVisibility(View.VISIBLE);
                coupon_screen_rv_list.setVisibility(View.GONE);
            } else {
                coupon_screen_tv_reload.setVisibility(View.GONE);
                coupon_screen_rv_list.setVisibility(View.VISIBLE);
            }
        }
    }

    @Override
    public void onResultFail(BaseResult result) {
        if (result instanceof CouponsResult) {
            if (coupon_screen_rv_list.isRefreshing()) {
                coupon_screen_rv_list.onRefreshComplete();
            } else if (coupon_screen_rv_list.isLoadingMore()) {
                coupon_screen_rv_list.onLoadMoreComplete();
            } else {
                adapter.notifyDataSetChanged();
            }
            if (adapter.getRealItemCount() <= 0) {
                coupon_screen_tv_reload.setText(error_string_id = R.string.common_error_pull_to_reload);
                coupon_screen_tv_reload.setVisibility(View.VISIBLE);
                coupon_screen_rv_list.setVisibility(View.GONE);
            } else {
                coupon_screen_tv_reload.setVisibility(View.GONE);
                coupon_screen_rv_list.setVisibility(View.VISIBLE);
            }
        }
        super.onResultFail(result);
    }

    @Override
    public void onFail(RequestTarget target, String error, Constant.StatusCode code) {
        switch (target) {
            case COUPONS:
                if (coupon_screen_rv_list.isRefreshing()) {
                    coupon_screen_rv_list.onRefreshComplete();
                } else if (coupon_screen_rv_list.isLoadingMore()) {
                    coupon_screen_rv_list.onLoadMoreComplete();
                } else {
                    adapter.notifyDataSetChanged();
                }
                if (adapter.getRealItemCount() <= 0) {
                    coupon_screen_tv_reload.setText(error_string_id = R.string.common_error_pull_to_reload);
                    coupon_screen_tv_reload.setVisibility(View.VISIBLE);
                    coupon_screen_rv_list.setVisibility(View.GONE);
                } else {
                    coupon_screen_tv_reload.setVisibility(View.GONE);
                    coupon_screen_rv_list.setVisibility(View.VISIBLE);
                }
                break;
        }
        super.onFail(target, error, code);
    }

    @Override
    public void refreshViewDataLanguage() {
        coupon_screen_tv_reload.setText(error_string_id);
        if (adapter != null)
            adapter.notifyDataSetChanged();
    }

}
