package com.ibs.roann.feature.menu;

import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.view.PagerTabStrip;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.ibs.roann.R;
import com.ibs.roann.feature.ToyosuBaseFragment;
import com.ibs.roann.model.MenuCategory;
import com.ibs.roann.model.api.FoodByCategoryResult;
import com.ibs.roann.model.api.FoodCategoriesParam;
import com.ibs.roann.model.api.FoodCategoriesResult;

import java.util.ArrayList;
import java.util.HashMap;

import butterknife.BindView;
import core.base.BaseResult;
import core.util.Constant;
import core.util.RequestTarget;
import icepick.State;

/**
 * Created by tuanle on 1/3/17.
 */

public class ToyosuMenuScreen extends ToyosuBaseFragment implements ViewPager.OnPageChangeListener {

    @BindView(R.id.menu_vp_menu)
    ViewPager menu_vp_menu;

    @BindView(R.id.menu_pts_title)
    PagerTabStrip menu_pts_title;

    @BindView(R.id.menu_category_screen_tv_reload)
    TextView menu_category_screen_tv_reload;

    @State
    ArrayList<MenuCategory> categories;
    private ToyosuMenuCategoryAdapter adapter;

    public static ToyosuMenuScreen getInstance() {
        return new ToyosuMenuScreen();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.toyosu_menu_screen, container, false);
    }

    @Override
    public void onBaseCreate() {
        super.onBaseCreate();
        categories = new ArrayList<>();
        adapter = new ToyosuMenuCategoryAdapter(categories, this);
    }

    @Override
    public void onBindView() {
        super.onBindView();
        menu_vp_menu.addOnPageChangeListener(this);
        menu_vp_menu.setAdapter(adapter);
        menu_pts_title.setTabIndicatorColor(Color.WHITE);
    }

    @Override
    public void onInitializeViewData() {
        super.onInitializeViewData();
        requestMenuCategories(0);
        registerSingleAction(menu_category_screen_tv_reload);
    }

    @Override
    public int getEnterInAnimation() {
        return 0;
    }

    @Override
    public void onResultSuccess(BaseResult result) {
        if (result instanceof FoodCategoriesResult) {
            if (categories != null) {
                categories.clear();
                categories.addAll(((FoodCategoriesResult) result).getMenuCategories());
            }
            adapter.notifyDataSetChanged();
            if (adapter.getCount() <= 0) {
                menu_vp_menu.setVisibility(View.GONE);
                menu_category_screen_tv_reload.setVisibility(View.VISIBLE);
            } else {
                menu_vp_menu.setVisibility(View.VISIBLE);
                menu_category_screen_tv_reload.setVisibility(View.GONE);
            }
            if (categories.size() > 0) {
                onPageSelected(0);
            }
        }
    }

    @Override
    public void onResultFail(BaseResult result) {
        if (!(result instanceof FoodByCategoryResult) && result instanceof FoodCategoriesResult) {
            adapter.notifyDataSetChanged();
            if (adapter.getCount() <= 0) {
                menu_vp_menu.setVisibility(View.GONE);
                menu_category_screen_tv_reload.setVisibility(View.VISIBLE);
            } else {
                menu_vp_menu.setVisibility(View.VISIBLE);
                menu_category_screen_tv_reload.setVisibility(View.GONE);
            }
        }
        super.onResultFail(result);
    }

    @Override
    public void onFail(RequestTarget target, String error, Constant.StatusCode code) {
        switch (target) {
            case FOOD_CATEGORIES:
                adapter.notifyDataSetChanged();
                if (adapter.getCount() <= 0) {
                    menu_vp_menu.setVisibility(View.GONE);
                    menu_category_screen_tv_reload.setVisibility(View.VISIBLE);
                } else {
                    menu_vp_menu.setVisibility(View.VISIBLE);
                    menu_category_screen_tv_reload.setVisibility(View.GONE);
                }
                break;
        }
        super.onFail(target, error, code);
    }

    @Override
    public void refreshViewDataLanguage() {
        menu_category_screen_tv_reload.setText(R.string.common_tap_to_reload);
        if (adapter != null) {
            for (ToyosuMenuCategoryItem item : adapter.getViews().values())
                if (item != null)
                    item.refreshViewDataLanguage();
            adapter.notifyDataSetChanged();
            if (categories.size() > 0) {
                menu_vp_menu.setCurrentItem(0);
            }
        }
    }

    @Override
    public void reloadData() {

    }

    private void requestMenuCategories(int offset) {
        makeRequest(getUniqueTag(), true, new FoodCategoriesParam(offset), this, RequestTarget.FOOD_CATEGORIES);
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageSelected(int position) {
        HashMap<String, ToyosuMenuCategoryItem> views = adapter.getViews();
        if (views != null) {
            for (String key : views.keySet()) {
                if (key.equals("Page_Category_Id_" + categories.get(position).id)) {
                    ToyosuMenuCategoryItem screen = views.get(key);
                    if (screen != null && !screen.isInitialized()) {
                        screen.reload();
                    }
                    break;
                }
            }
        }
    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }

    @Override
    public void onSingleClick(View v) {
        switch (v.getId()) {
            case R.id.menu_category_screen_tv_reload:
                requestMenuCategories(0);
                break;
        }
    }

    @Override
    public void onBaseFree() {
        unregisterSingleAction(menu_category_screen_tv_reload);
        adapter.clearViews();
        super.onBaseFree();
    }
}
