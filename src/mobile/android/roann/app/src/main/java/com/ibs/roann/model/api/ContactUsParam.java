package com.ibs.roann.model.api;

import com.ibs.roann.feature.ToyosuBaseParam;

import org.json.JSONObject;

/**
 * Created by tuanle on 1/11/17.
 */

public class ContactUsParam extends ToyosuBaseParam {

    private static final String PARAMS_DATA_TAG = "data";
    private static final String PARAMS_RESTAURANT_ID_TAG = "restaurant";
    private static final String PARAMS_NAME_TAG = "name";
    private static final String PARAMS_EMAIL_TAG = "email";
    private static final String PARAMS_PHONE_TAG = "phone";
    private static final String PARAMS_MESSAGE_TAG = "content";
    private static final String PARAMS_STATUS_TAG = "status";

    private int restaurant_id;
    private String name;
    private String email;
    private String phone;
    private String message;

    public ContactUsParam(int restaurant_id, String name, String email, String phone, String message) {
        this.restaurant_id = restaurant_id;
        this.name = name;
        this.email = email;
        this.phone = phone;
        this.message = message;
    }

    @Override
    public byte[] makeRequestBody() {
        try {
            return new JSONObject().put(PARAMS_DATA_TAG, new JSONObject()
                    .put(PARAMS_RESTAURANT_ID_TAG, restaurant_id)
                    .put(PARAMS_NAME_TAG, name)
                    .put(PARAMS_EMAIL_TAG, email)
                    .put(PARAMS_PHONE_TAG, phone)
                    .put(PARAMS_STATUS_TAG, 1)
                    .put(PARAMS_MESSAGE_TAG, message)).toString().getBytes();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return new byte[0];
    }
}
