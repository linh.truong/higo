package com.ibs.roann.feature;

import android.os.Build;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.TextPaint;
import android.text.format.DateFormat;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.Date;

import core.util.Utils;

/**
 *
 */

public class ToyosuUtils extends Utils {


    public static String formatDate(Date date, String format) {
        SimpleDateFormat formatter;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR2) {
            formatter = new SimpleDateFormat(DateFormat.getBestDateTimePattern(ToyosuApplication.getActiveActivity().getResources().getConfiguration().locale, format));
        } else {
            formatter = new SimpleDateFormat(format);
        }
        return formatter.format(date);
    }

    public static Date formatDateString(String date) {
        try {
            SimpleDateFormat formatter = new SimpleDateFormat(ToyosuConstant.DATE_FORMAT_RESPONSE);
            return formatter.parse(date);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return new Date();
    }

    public static String formatDateString(long timestamp) {
        SimpleDateFormat formatter = null;
        try {
            formatter = new SimpleDateFormat(ToyosuConstant.DATE_FORMAT_SHORT);
            return formatter.format(new Date(timestamp * 1000));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return formatter.format(new Date());
    }

    public static String getUID() {
        return String.format("%1$s.%2$s", ToyosuApplication.getContext().getPackageName(), Utils.getUID());
    }

    public static void setTags(TextView pTextView, String pTagString, final int color) {
        SpannableString string = new SpannableString(pTagString);

        int start = -1;
        for (int i = 0; i < pTagString.length(); i++) {
            if (pTagString.charAt(i) == '#') {
                start = i;
            } else if (pTagString.charAt(i) == ' ' || (i == pTagString.length() - 1 && start != -1)) {
                if (start != -1) {
                    if (i == pTagString.length() - 1) {
                        i++; // case for if hash is last word and there is no
                        // space after word
                    }

                    final String tag = pTagString.substring(start, i);
                    string.setSpan(new ClickableSpan() {

                        @Override
                        public void onClick(View widget) {
                            Log.d("Hash", String.format("Clicked %s!", tag));
                        }

                        @Override
                        public void updateDrawState(TextPaint ds) {
                            // link color
                            ds.setColor(color);
                            ds.setUnderlineText(false);
                        }
                    }, start, i, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                    start = -1;
                }
            }
        }

        pTextView.setMovementMethod(LinkMovementMethod.getInstance());
        pTextView.setText(string);
    }

}
