package com.ibs.roann.model;

import com.google.gson.annotations.SerializedName;
import com.ibs.roann.feature.ToyosuConstant;

import java.io.Serializable;

import core.data.UserDataManager;
import core.util.DataKey;

/**
 * Created by tuanle on 1/10/17.
 */

public class Restaurant implements Serializable {

    @SerializedName("id")
    public int id = -1;

    @SerializedName("area")
    public int area;

    @SerializedName("station")
    public int station;

    @SerializedName("code")
    public String code;

    @SerializedName("name")
    public String name;

    @SerializedName("name_cn")
    public String name_cn;

    @SerializedName("image_path")
    public String image_path;

    @SerializedName("contact_person")
    public String contact_person;

    @SerializedName("email")
    public String email;

    @SerializedName("phone")
    public String phone;

    @SerializedName("address")
    public String address;

    @SerializedName("address_cn")
    public String address_cn;

    @SerializedName("description")
    public String information;

    @SerializedName("description_cn")
    public String information_cn;

    @SerializedName("google_map_url")
    public String google_map_url;

    @SerializedName("longitude")
    public double longitude;

    @SerializedName("latitude")
    public double latitude;

    @SerializedName("week_days")
    public String week_days;

    @SerializedName("start_time")
    public String start_time;

    @SerializedName("end_time")
    public String end_time;

    @SerializedName("limit_days_book")
    public int limit_days_book;

    @SerializedName("confirm_reserve")
    public String confirm_reserve;

    @SerializedName("confirm_reserve_cn")
    public String confirm_reserve_cn;

    public String getName() {
        return UserDataManager.getInstance().getString(DataKey.DEVICE_LANGUAGE).equals(ToyosuConstant.DEVICE_LANGUAGE_CHINESE) ? name_cn : name;
    }

    public String getAddress() {
        return UserDataManager.getInstance().getString(DataKey.DEVICE_LANGUAGE).equals(ToyosuConstant.DEVICE_LANGUAGE_CHINESE) ? address_cn : address;
    }

    public String getInformation() {
        return UserDataManager.getInstance().getString(DataKey.DEVICE_LANGUAGE).equals(ToyosuConstant.DEVICE_LANGUAGE_CHINESE) ? information_cn : information;
    }

    public String getConfirmReserve() {
        return UserDataManager.getInstance().getString(DataKey.DEVICE_LANGUAGE).equals(ToyosuConstant.DEVICE_LANGUAGE_CHINESE) ? confirm_reserve_cn : confirm_reserve;
    }
}
