package com.ibs.roann.parser;

import com.google.gson.Gson;
import com.ibs.roann.model.Additional;
import com.ibs.roann.model.Provider;
import com.ibs.roann.model.Service;
import com.ibs.roann.model.WorkingDate;
import com.ibs.roann.model.api.RestaurantInfoBookResult;

import org.json.JSONArray;
import org.json.JSONObject;

import core.base.BaseParser;
import core.base.BaseResult;
import core.util.Constant;

import static com.ibs.roann.parser.ToyosuBaseParser.RESPONSE_ADDITIONAL_TAG;
import static com.ibs.roann.parser.ToyosuBaseParser.RESPONSE_ERROR_TAG;
import static com.ibs.roann.parser.ToyosuBaseParser.RESPONSE_MESSAGE_TAG;
import static com.ibs.roann.parser.ToyosuBaseParser.RESPONSE_PROVIDER_TAG;
import static com.ibs.roann.parser.ToyosuBaseParser.RESPONSE_SERVICE_TAG;
import static com.ibs.roann.parser.ToyosuBaseParser.RESPONSE_WORKING_DATE_TAG;

/**
 * Created by tuanle on 1/3/17.
 */

public class RestaurantInfoBookParser extends BaseParser {
    @Override
    public BaseResult parseData(String content) {
        RestaurantInfoBookResult result = new RestaurantInfoBookResult();

        try {
            JSONObject json = new JSONObject(content);
            result.setBackEndStatus(json.optInt(RESPONSE_ERROR_TAG));
            if (result.getBackEndStatus() == 0) {
                Gson gson = new Gson();
                JSONArray dataAdditionals = json.getJSONArray(RESPONSE_ADDITIONAL_TAG);
                for (int i = 0; i < dataAdditionals.length(); ++i) {
                    result.getAdditionalList().add(gson.fromJson(dataAdditionals.get(i).toString(), Additional.class));
                }
                JSONArray dataServices = json.getJSONArray(RESPONSE_SERVICE_TAG);
                for (int i = 0; i < dataServices.length(); ++i) {
                    result.getServiceList().add(gson.fromJson(dataServices.get(i).toString(), Service.class));
                }
                JSONArray dataProviders = json.getJSONArray(RESPONSE_PROVIDER_TAG);
                for (int i = 0; i < dataProviders.length(); ++i) {
                    result.getProviderList().add(gson.fromJson(dataProviders.get(i).toString(), Provider.class));
                }
                JSONArray dataWorkingDates = json.getJSONArray(RESPONSE_WORKING_DATE_TAG);
                for (int i = 0; i < dataWorkingDates.length(); ++i) {
                    result.getWorkingDateList().add(gson.fromJson(dataWorkingDates.get(i).toString(), WorkingDate.class));
                }
                result.setStatus(Constant.StatusCode.OK);
            } else {
                result.setMessage(json.optString(RESPONSE_MESSAGE_TAG));
            }
            return result;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
}
