package com.ibs.roann.model.api;

import com.ibs.roann.feature.ToyosuBaseResult;
import com.ibs.roann.model.CompanyInformation;

/**
 * Created by tuanle on 1/3/17.
 */

public class CompanyInfoResult extends ToyosuBaseResult {

    private CompanyInformation company;

    public CompanyInformation getCompany() {
        return company;
    }

    public void setCompany(CompanyInformation company) {
        this.company = company;
    }
}
