package com.ibs.roann.parser;

import com.ibs.roann.model.api.UpdateQRPointResult;

import org.json.JSONObject;

import core.base.BaseParser;
import core.base.BaseResult;
import core.util.Constant;

import static com.ibs.roann.parser.ToyosuBaseParser.RESPONSE_CURRENT_POINT_TAG;
import static com.ibs.roann.parser.ToyosuBaseParser.RESPONSE_ERROR_TAG;
import static com.ibs.roann.parser.ToyosuBaseParser.RESPONSE_MESSAGE_TAG;
import static com.ibs.roann.parser.ToyosuBaseParser.RESPONSE_RES_POINT_TAG;
import static com.ibs.roann.parser.ToyosuBaseParser.RESPONSE_TOTAL_POINT_TAG;

/**
 * Created by tuanle on 1/3/17.
 */

public class UpdateQRPointParser extends BaseParser {
    @Override
    public BaseResult parseData(String content) {
        UpdateQRPointResult result = new UpdateQRPointResult();

        try {
            JSONObject json = new JSONObject(content);
            result.setBackEndStatus(json.optInt(RESPONSE_ERROR_TAG));
            if (result.getBackEndStatus() == 0) {
                result.setTotal_point(json.optInt(RESPONSE_TOTAL_POINT_TAG));
                result.setCurrent_point(json.optInt(RESPONSE_CURRENT_POINT_TAG));
                result.setRes_point(json.optInt(RESPONSE_RES_POINT_TAG));
                result.setStatus(Constant.StatusCode.OK);
            } else {
                result.setMessage(json.optString(RESPONSE_MESSAGE_TAG));
            }
            return result;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
}
