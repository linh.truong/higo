package com.ibs.roann.model.api;

import com.ibs.roann.feature.ToyosuBaseParam;

import org.json.JSONObject;

/**
 * Created by tuanle on 1/20/17.
 */

public class RestaurantsParam extends ToyosuBaseParam {

    private int offset;

    public RestaurantsParam(int offset) {
        this.offset = offset;
    }

    @Override
    public byte[] makeRequestBody() {
        try {
            return new JSONObject().put(PARAM_LIMIT_TAG, String.valueOf(2)).put(PARAM_OFFSET_TAG, String.valueOf(offset)).toString().getBytes();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return new byte[0];
    }
}
