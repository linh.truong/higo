package com.ibs.roann.model;

import java.io.Serializable;

/**
 * Created by tuanle on 1/13/17.
 */

public class Delivery implements Serializable {

    public String name;

    public String email;

    public String phone;

    public String address;

    public long receive_date = -1;

    public int delivery_type;

}
