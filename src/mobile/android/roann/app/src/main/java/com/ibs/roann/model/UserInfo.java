package com.ibs.roann.model;

import com.google.gson.annotations.SerializedName;
import com.ibs.roann.feature.ToyosuConstant;

import java.io.Serializable;

import core.data.UserDataManager;
import core.util.DataKey;

/**
 * Created by tuanle on 1/12/17.
 */

public class UserInfo implements Serializable {

    @SerializedName("current_point")
    public int current_point;

    @SerializedName("total_point")
    public int total_point;

    @SerializedName("last_transaction_date")
    public String last_transaction_date;

    @SerializedName("mem_status_image_path")
    public String mem_status_image_path;

    @SerializedName("mem_status_next_level_point")
    public int mem_status_next_level_point;

    @SerializedName("mem_status_goal_point")
    public int mem_status_goal_point;

    @SerializedName("mem_status_remain_point")
    public int mem_status_remain_point;

    @SerializedName("mem_status_name")
    public String mem_status_name;

    @SerializedName("mem_status_name_cn")
    public String mem_status_name_cn;

    @SerializedName("mem_status_next_name")
    public String mem_status_next_name;

    @SerializedName("mem_status_next_name_cn")
    public String mem_status_next_name_cn;

    @SerializedName("mem_status_content")
    public String mem_status_content;

    @SerializedName("mem_status_content_cn")
    public String mem_status_content_cn;

    @SerializedName("mem_status_content_special")
    public String mem_status_content_special;

    @SerializedName("mem_status_content_special_cn")
    public String mem_status_content_special_cn;

    @SerializedName("mem_status_hex_color")
    public String mem_status_hex_color;

    @SerializedName("mem_status_hex_color_benefit_1")
    public String mem_status_hex_color_benefit_1;

    @SerializedName("mem_status_hex_color_benefit_2")
    public String mem_status_hex_color_benefit_2;

    @SerializedName("mem_status_short_content")
    public String mem_status_short_content;

    @SerializedName("mem_status_short_content_cn")
    public String mem_status_short_content_cn;

    @SerializedName("mem_status_short_content_2")
    public String mem_status_short_content_2;

    @SerializedName("mem_status_short_content_2_cn")
    public String mem_status_short_content_2_cn;

    public String getMemStatusName() {
        return UserDataManager.getInstance().getString(DataKey.DEVICE_LANGUAGE).equals(ToyosuConstant.DEVICE_LANGUAGE_CHINESE) ? mem_status_name_cn : mem_status_name;
    }

    public String getMemStatusNextName() {
        return UserDataManager.getInstance().getString(DataKey.DEVICE_LANGUAGE).equals(ToyosuConstant.DEVICE_LANGUAGE_CHINESE) ? mem_status_next_name_cn : mem_status_next_name;
    }

    public String getMemStatusContent() {
        return UserDataManager.getInstance().getString(DataKey.DEVICE_LANGUAGE).equals(ToyosuConstant.DEVICE_LANGUAGE_CHINESE) ? mem_status_content_cn : mem_status_content;
    }

    public String getMemStatusContentSpecial() {
        return UserDataManager.getInstance().getString(DataKey.DEVICE_LANGUAGE).equals(ToyosuConstant.DEVICE_LANGUAGE_CHINESE) ? mem_status_content_special_cn : mem_status_content_special;
    }

    public String getMemStatusShortContent() {
        return UserDataManager.getInstance().getString(DataKey.DEVICE_LANGUAGE).equals(ToyosuConstant.DEVICE_LANGUAGE_CHINESE) ? mem_status_short_content_cn : mem_status_short_content;
    }

    public String getMemStatusShortContent2() {
        return UserDataManager.getInstance().getString(DataKey.DEVICE_LANGUAGE).equals(ToyosuConstant.DEVICE_LANGUAGE_CHINESE) ? mem_status_short_content_2_cn : mem_status_short_content_2;
    }

}
