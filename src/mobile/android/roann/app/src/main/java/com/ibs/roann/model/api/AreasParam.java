package com.ibs.roann.model.api;

import com.ibs.roann.feature.ToyosuBaseParam;

import org.json.JSONObject;

/**
 * Created by tuanle on 1/3/17.
 */

public class AreasParam extends ToyosuBaseParam {
    @Override
    public byte[] makeRequestBody() {
        try {
            return new JSONObject().put(PARAM_OFFSET_TAG, 0).put(PARAM_LIMIT_TAG, 1000).toString().getBytes();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return new byte[0];
    }
}

