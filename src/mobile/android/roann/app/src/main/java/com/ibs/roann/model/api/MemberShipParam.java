package com.ibs.roann.model.api;

import com.ibs.roann.feature.ToyosuBaseParam;

import org.json.JSONObject;

/**
 * Created by tu.le on 3/1/2017.
 */

public class MemberShipParam extends ToyosuBaseParam{

    private static final String PARAMS_DEVICE_ID_TAG = "device_id";
    private static final String PARAMS_TYPE_TAG = "type";
    private String device_id;


    public MemberShipParam(String device_id) {
        this.device_id = device_id;
    }

    @Override
    public byte[] makeRequestBody() {
        try {
            return new JSONObject().put(PARAMS_TYPE_TAG, 1).put(PARAMS_DEVICE_ID_TAG, device_id).toString().getBytes();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return new byte[0];
    }
}