package com.ibs.roann.model.api;

import com.ibs.roann.feature.ToyosuBaseResult;
import com.ibs.roann.model.UserUpdate;

/**
 * Created by tuanle on 1/19/17.
 */

public class UpdateCustomerResult extends ToyosuBaseResult {

    private UserUpdate userUpdate;

    public UserUpdate getUserUpdate() {
        return userUpdate;
    }

    public void setUserUpdate(UserUpdate userUpdate) {
        this.userUpdate = userUpdate;
    }
}
