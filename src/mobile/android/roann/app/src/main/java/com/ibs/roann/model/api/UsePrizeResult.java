package com.ibs.roann.model.api;

import com.ibs.roann.feature.ToyosuBaseResult;
import com.ibs.roann.model.UsePoint;

/**
 * Created by tuanle on 1/3/17.
 */

public class UsePrizeResult extends ToyosuBaseResult {

    private UsePoint usePoint;

    public UsePoint getUsePoint() {
        return usePoint;
    }

    public void setUsePoint(UsePoint usePoint) {
        this.usePoint = usePoint;
    }
}
