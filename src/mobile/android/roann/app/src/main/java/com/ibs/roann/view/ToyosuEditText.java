package com.ibs.roann.view;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.EditText;

/**
 * Created by tuanle on 1/11/17.
 */

public class ToyosuEditText extends EditText {
    public ToyosuEditText(Context context) {
        super(context);
    }

    public ToyosuEditText(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public ToyosuEditText(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }
}
