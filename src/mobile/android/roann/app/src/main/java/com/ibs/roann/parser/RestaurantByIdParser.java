package com.ibs.roann.parser;

import com.google.gson.Gson;
import com.ibs.roann.model.Restaurant;
import com.ibs.roann.model.api.RestaurantByIdResult;

import org.json.JSONObject;

import core.base.BaseResult;
import core.util.Constant;

/**
 * Created by tuanle on 1/3/17.
 */

public class RestaurantByIdParser extends ToyosuBaseParser {
    @Override
    public BaseResult parseData(String content) {

        RestaurantByIdResult result = new RestaurantByIdResult();

        try {
            JSONObject json = new JSONObject(content);
            result.setBackEndStatus(json.optInt(RESPONSE_ERROR_TAG));
            if (result.getBackEndStatus() == 0) {
                Gson gson = new Gson();
                JSONObject data = json.getJSONObject(RESPONSE_DATA_TAG);
                result.setRestaurant(gson.fromJson(data.toString(), Restaurant.class));
                result.setStatus(Constant.StatusCode.OK);
            } else {
                result.setMessage(json.optString(RESPONSE_MESSAGE_TAG));
            }
            return result;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
}
