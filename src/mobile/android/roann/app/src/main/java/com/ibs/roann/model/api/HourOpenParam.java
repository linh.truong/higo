package com.ibs.roann.model.api;

import com.ibs.roann.feature.ToyosuBaseParam;

import org.json.JSONObject;

/**
 * Created by tuanle on 1/3/17.
 */

public class HourOpenParam extends ToyosuBaseParam {

    private static final String PARAMS_RES_ID_TAG = "resId";
    private static final String PARAMS_DATE_TAG = "reserveDate";
    private static final String PARAMS_EVENT_ID_TAG = "eventId";
    private static final String PARAMS_UNIT_ID_TAG = "unitId";

    private int resId;
    private String reserveDate;
    private String eventId;
    private String unitId;

    public HourOpenParam(int resId, String reserveDate, String eventId, String unitId) {
        this.resId = resId;
        this.reserveDate = reserveDate;
        this.eventId = eventId;
        this.unitId = unitId;
    }

    @Override
    public byte[] makeRequestBody() {
        try {
            return new JSONObject().put(PARAMS_RES_ID_TAG, String.valueOf(resId))
                                    .put(PARAMS_DATE_TAG, String.valueOf(reserveDate))
                                    .put(PARAMS_EVENT_ID_TAG, String.valueOf(eventId))
                                    .put(PARAMS_UNIT_ID_TAG, String.valueOf(unitId))
                                    .toString().getBytes();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return new byte[0];
    }
}

