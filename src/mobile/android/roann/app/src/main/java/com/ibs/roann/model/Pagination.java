package com.ibs.roann.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 *
 */

public class Pagination implements Serializable{

    @SerializedName("next_max_id")
    public String next_max_id;

    @SerializedName("next_url")
    public String next_url;

}
