package com.ibs.roann.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

public class WorkingDate implements Serializable {

    @SerializedName("date")
    public String date;

    @SerializedName("time")
    public ArrayList<HourOpen> hourOpens;

}
