package com.ibs.roann.parser;

import com.ibs.roann.model.api.UpdatePasswordResult;

import org.json.JSONObject;

import core.base.BaseResult;
import core.util.Constant;

/**
 * Created by tuanle on 1/24/17.
 */

public class UpdatePasswordParser extends ToyosuBaseParser {
    @Override
    public BaseResult parseData(String content) {
        UpdatePasswordResult result = new UpdatePasswordResult();

        try {
            JSONObject json = new JSONObject(content);
            result.setBackEndStatus(json.optInt(RESPONSE_ERROR_TAG));
            if (result.getBackEndStatus() == 0) {
                result.setStatus(Constant.StatusCode.OK);
            } else {
                result.setMessage(json.optString(RESPONSE_MESSAGE_TAG));
            }
            return result;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
}
