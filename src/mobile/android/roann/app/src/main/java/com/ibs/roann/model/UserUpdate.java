package com.ibs.roann.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by tuanle on 1/12/17.
 */

public class UserUpdate implements Serializable {

    @SerializedName("name")
    public String name;

    @SerializedName("phone")
    public String phone;

    @SerializedName("address")
    public String address;

    @SerializedName("sex")
    public int sex;

    @SerializedName("age")
    public int age;

    @SerializedName("area")
    public int area;

    @SerializedName("birthday")
    public String birthday;

}
