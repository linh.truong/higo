package com.ibs.roann.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Additional implements Serializable {

    @SerializedName("id")
    public String id;

    @SerializedName("name")
    public String name;

    @SerializedName("values")
    public String values;

    @SerializedName("title")
    public String title;

    @SerializedName("type")
    public String type;

    @SerializedName("default")
    public String default_value;

}
