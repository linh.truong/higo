package com.ibs.roann.model.api;

import com.ibs.roann.feature.ToyosuBaseResult;
import com.ibs.roann.model.Instagram;
import com.ibs.roann.model.Pagination;

import java.util.ArrayList;

/**
 * Created by tuanle on 1/3/17.
 */

public class InstagramResult extends ToyosuBaseResult {

    private ArrayList<Instagram> topics;
//    private int total;
    private Pagination pagination;

    public InstagramResult() {
        this.topics = new ArrayList<>();
//        this.total = 0;
        this.pagination = new Pagination();
    }

    public ArrayList<Instagram> getTopics() {
        return topics;
    }

    public Pagination getPagination() {
        return pagination;
    }

    public void setPagination(Pagination pagination) {
        this.pagination = pagination;
    }

//    public int getTotal() {
//        return total;
//    }
//
//    public void setTotal(int total) {
//        this.total = total;
//    }
}
