package core.util.fcm;

import java.io.Serializable;

public class FCMRegistrationSuccessMessage implements Serializable {

    private String token;

    public FCMRegistrationSuccessMessage(String token) {
        this.token = token;
    }

    public String getToken() {
        return token;
    }

}
