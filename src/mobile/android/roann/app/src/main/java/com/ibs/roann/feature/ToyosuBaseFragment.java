package com.ibs.roann.feature;

import android.app.Activity;
import android.content.Intent;
import android.util.Log;
import android.view.View;

import com.ibs.roann.R;
import com.ibs.roann.model.User;

import core.base.BaseActivity;
import core.base.BaseFragment;
import core.base.BaseResult;
import core.connection.WebServiceRequester;
import core.util.Constant;
import core.util.RequestTarget;

/**
 * Created by tuanle on 1/3/17.
 */

public abstract class ToyosuBaseFragment extends BaseFragment implements WebServiceRequester.WebServiceResultHandler {
    @Override
    public void onBaseCreate() {

    }

    @Override
    public void onDeepLinking(Intent data) {

    }

    @Override
    public void onNotification(Intent data) {

    }

    @Override
    public void onInitializeViewData() {

    }

    @Override
    public void onBaseResume() {
        Log.e("ToyosuBaseFragment", "onBaseResume");
        updateActionBarStage();
    }

    @Override
    public void onBaseFree() {
    }

    @Override
    public void onResultSuccess(BaseResult result) {

    }

    @Override
    public void onResultFail(BaseResult result) {
        /*
        if (result instanceof ToyosuBaseResult) {
            String message = ((ToyosuBaseResult) result).getMessage();
            if (ToyosuUtils.isEmpty(message))
                message = getString(R.string.error_server_busy);
            showAlertDialog(getActiveActivity(), -1, getGeneralDialogLayoutResource(),
                    R.mipmap.ic_app_logo,
                    getString(R.string.common_info),
                    message, getString(R.string.common_ok)
                    , null, null);
        }
*/
    }

    @Override
    public void onFail(RequestTarget target, String error, Constant.StatusCode code) {
        /*
        String message = getString(R.string.error_server_busy);
        if (code == Constant.StatusCode.ERR_NO_INTERNET_CONNECTION) {
            message = getString(R.string.error_internet_unavailable_message);
        }
        showAlertDialog(getActiveActivity(), -1, getGeneralDialogLayoutResource(),
                R.mipmap.ic_app_logo,
                getString(R.string.app_name),
                message, getString(R.string.common_ok)
                , null, null);
                */
    }

    @Override
    public void onSingleClick(View v) {

    }

    protected void updateActionBarStage() {
        BaseActivity activity = (BaseActivity) getActiveActivity();
        if (activity instanceof ToyosuMainActivity)
            ((ToyosuMainActivity) activity).updateActionBarStage();
    }

    @Override
    public int getGeneralDialogLayoutResource() {
        return R.layout.toyosu_general_dialog;
    }

    public void changeLanguage(String language) {
        Activity activity = getActiveActivity();
        if (activity != null && activity instanceof ToyosuMainActivity) {
            ToyosuMainActivity main = (ToyosuMainActivity) activity;
            main.changeLanguage(language);
        }
    }

    public User getLoggedUser() {
        Activity activity = getActiveActivity();
        if (activity != null && activity instanceof ToyosuMainActivity) {
            ToyosuMainActivity main = (ToyosuMainActivity) activity;
            return main.getLoggedUser();
        }
        return null;
    }

    public void setLoggedUser(User user) {
        Activity activity = getActiveActivity();
        if (activity != null && activity instanceof ToyosuMainActivity) {
            ToyosuMainActivity main = (ToyosuMainActivity) activity;
            main.setLoggedUser(user);
        }
    }

    public abstract void refreshViewDataLanguage();

    public abstract void reloadData();
}
