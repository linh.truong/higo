package com.ibs.roann.feature;


import android.Manifest;
import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.ValueAnimator;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.IdRes;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.gson.Gson;
import com.ibs.roann.R;
import com.ibs.roann.feature.authentication.ToyosuLoginScreen;
import com.ibs.roann.feature.authentication.ToyosuPasswordRecoveryScreen;
import com.ibs.roann.feature.authentication.ToyosuRegisterScreen;
import com.ibs.roann.feature.authentication.ToyosuUserUpdateScreen;
import com.ibs.roann.feature.coupon.ToyosuCouponDetailScreen;
import com.ibs.roann.feature.coupon.ToyosuCouponScreen;
import com.ibs.roann.feature.membership.ToyosuMemberShipScreen;
import com.ibs.roann.feature.menu.ToyosuFoodDetailScreen;
import com.ibs.roann.feature.menu.ToyosuMenuScreen;
import com.ibs.roann.feature.mypage.ToyosuMyPageScreen;
import com.ibs.roann.feature.news.ToyosuNewsScreen;
import com.ibs.roann.feature.news.ToyosuTopicDetailScreen;
import com.ibs.roann.feature.prize.ToyosuPrizeDetailScreen;
import com.ibs.roann.feature.prize.ToyosuPrizeScreen;
import com.ibs.roann.feature.prize.ToyosuQRCodeScanScreen;
import com.ibs.roann.feature.reservation.ToyosuBookingScreen;
import com.ibs.roann.feature.setting.ToyosuContactUsScreen;
import com.ibs.roann.feature.setting.ToyosuLegalInformationScreen;
import com.ibs.roann.feature.setting.ToyosuSettingScreen;
import com.ibs.roann.feature.shop.ToyosuRestaurantDetailScreen;
import com.ibs.roann.feature.shop.ToyosuRestaurantsScreen;
import com.ibs.roann.model.User;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Locale;
import java.util.Set;

import butterknife.BindView;
import core.base.BaseFragment;
import core.base.BaseFragmentContainer;
import core.data.UserDataManager;
import core.util.Constant;
import core.util.DataKey;
import core.util.SoftKeyBoardTracker;
import icepick.State;

public class ToyosuMainActivity extends ToyosuBaseActivity implements Animation.AnimationListener, SoftKeyBoardTracker.OnKeyBoardListener {

    public static final int QR_CODE_SCAN_REQUEST_CODE = 9008;
    private static final int CAMERA_PERMISSION_REQUEST_CODE = 9007;

    @BindView(R.id.activity_main_footer_img_home)
    ImageView activity_main_footer_img_home;
    @BindView(R.id.activity_main_footer_img_prize)
    ImageView activity_main_footer_img_prize;
    @BindView(R.id.activity_main_footer_img_coupon)
    ImageView activity_main_footer_img_coupon;
    @BindView(R.id.activity_main_footer_img_shop)
    ImageView activity_main_footer_img_shop;
    @BindView(R.id.activity_main_footer_img_news)
    ImageView activity_main_footer_img_news;
    @BindView(R.id.activity_main_footer_img_setting)
    ImageView activity_main_footer_img_setting;
    @BindView(R.id.activity_main_header_img_logo)
    ImageView activity_main_header_img_logo;
    @BindView(R.id.activity_main_home_container)
    BaseFragmentContainer activity_main_home_container;
    @BindView(R.id.activity_main_prize_container)
    BaseFragmentContainer activity_main_prize_container;
    @BindView(R.id.activity_main_coupon_container)
    BaseFragmentContainer activity_main_coupon_container;
    @BindView(R.id.activity_main_shop_container)
    BaseFragmentContainer activity_main_shop_container;
    @BindView(R.id.activity_main_news_container)
    BaseFragmentContainer activity_main_news_container;
    @BindView(R.id.activity_main_setting_container)
    BaseFragmentContainer activity_main_setting_container;
    @BindView(R.id.activity_main_footer_tv_home)
    TextView activity_main_footer_tv_home;
    @BindView(R.id.activity_main_footer_tv_prize)
    TextView activity_main_footer_tv_prize;
    @BindView(R.id.activity_main_footer_tv_coupon)
    TextView activity_main_footer_tv_coupon;
    @BindView(R.id.activity_main_footer_tv_shop)
    TextView activity_main_footer_tv_shop;
    @BindView(R.id.activity_main_footer_tv_news)
    TextView activity_main_footer_tv_news;
    @BindView(R.id.activity_main_footer_tv_setting)
    TextView activity_main_footer_tv_setting;
    @BindView(R.id.activity_main_header_tv_page_title)
    TextView activity_main_header_tv_page_title;
    @BindView(R.id.activity_main_header_rp_action_bar_back)
    View activity_main_header_rp_action_bar_back;
    @BindView(R.id.activity_main_header_tv_back)
    TextView activity_main_header_tv_back;
    @BindView(R.id.activity_main_rl_footer)
    RelativeLayout activity_main_rl_footer;
    @BindView(R.id.activity_main_header_img_back)
    ImageView activity_main_header_img_back;
    @BindView(R.id.activity_main_header_ll_action_bar_back)
    LinearLayout activity_main_header_ll_action_bar_back;
    @BindView(R.id.activity_main_header_rl_right_buttons)
    RelativeLayout activity_main_header_rl_right_buttons;
    @BindView(R.id.activity_main_header_rp_card)
    View activity_main_header_rp_card;
    @BindView(R.id.activity_main_header_rp_camera)
    View activity_main_header_rp_camera;
    @BindView(R.id.activity_main_header_rp_book)
    View activity_main_header_rp_book;
    @BindView(R.id.activity_main_footer_ll_home_tab)
    View activity_main_footer_ll_home_tab;
    @BindView(R.id.activity_main_footer_ll_shop_tab)
    View activity_main_footer_ll_shop_tab;
    @BindView(R.id.activity_main_header_rp_cart)
    View activity_main_header_rp_cart;
    @BindView(R.id.main_view)
    RelativeLayout main_view;

    @State
    User user;

    @State
    boolean backToHome = false;

    private SoftKeyBoardTracker tracker;

    private Handler handler;

    private Animation ani_footer_in, ani_footer_out,
            ani_right_bt_in, ani_right_bt_out,
            ani_back_in, ani_back_out,
            ani_logo_fade_in, ani_logo_fade_out,
            ani_title_fade_in, ani_title_fade_out;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.toyosu_main_activity);
        UserDataManager.getInstance().setInt(DataKey.RESTAURANT_ID, 1); // default restaurant id for phase 1
    }

    @Override
    public void onBaseCreate() {
        ani_right_bt_in = AnimationUtils.loadAnimation(this, R.anim.slide_in_right);
        ani_right_bt_out = AnimationUtils.loadAnimation(this, R.anim.slide_out_right);
        ani_back_in = AnimationUtils.loadAnimation(this, R.anim.slide_in_left);
        ani_back_out = AnimationUtils.loadAnimation(this, R.anim.slide_out_left);
        ani_logo_fade_in = AnimationUtils.loadAnimation(this, R.anim.fade_in);
        ani_logo_fade_out = AnimationUtils.loadAnimation(this, R.anim.fade_out);
        ani_title_fade_in = AnimationUtils.loadAnimation(this, R.anim.fade_in);
        ani_title_fade_out = AnimationUtils.loadAnimation(this, R.anim.fade_out);
        ani_footer_in = AnimationUtils.loadAnimation(this, R.anim.slide_in_bottom);
        ani_footer_out = AnimationUtils.loadAnimation(this, R.anim.slide_out_bottom);
        ani_logo_fade_in.setAnimationListener(this);
        ani_logo_fade_out.setAnimationListener(this);
        ani_back_in.setAnimationListener(this);
        ani_back_out.setAnimationListener(this);
        ani_title_fade_out.setAnimationListener(this);
        ani_title_fade_in.setAnimationListener(this);
        ani_right_bt_out.setAnimationListener(this);
        ani_right_bt_in.setAnimationListener(this);
        ani_footer_in.setAnimationListener(this);
        ani_footer_out.setAnimationListener(this);
        handler = new Handler();
    }

    @Override
    public void onDeepLinking(Intent data) {

    }

    @Override
    public void onNotification(Intent data) {

    }

    @Override
    public void onInitializeViewData() {
        registerSingleAction(
                R.id.activity_main_footer_ll_home_tab,
                R.id.activity_main_footer_ll_prize_tab,
                R.id.activity_main_footer_ll_coupon_tab,
                R.id.activity_main_footer_ll_shop_tab,
                R.id.activity_main_footer_ll_news_tab,
                R.id.activity_main_footer_ll_setting_tab,
                R.id.activity_main_header_rp_action_bar_back,
                R.id.activity_main_header_rl_book_click,
                R.id.activity_main_header_rl_card_click,
                R.id.activity_main_header_img_camera,
                R.id.activity_main_header_img_cart);

        activity_main_footer_ll_home_tab.performClick();

        activity_main_header_tv_back.setVisibility(View.GONE);
        refreshViewDataLanguage();
        activity_main_header_tv_back.setVisibility(View.VISIBLE);
    }

    @Override
    protected void onLastFragmentBack(@IdRes int containerId) {
        ToyosuBaseFragment top = (ToyosuBaseFragment) getTopFragment(containerId);
        if (top instanceof ToyosuMyPageScreen
                || top instanceof ToyosuPrizeScreen
                || top instanceof ToyosuCouponScreen
                || top instanceof ToyosuSettingScreen
                || top instanceof ToyosuLoginScreen
                || top instanceof ToyosuNewsScreen
                || top instanceof ToyosuRestaurantsScreen) {
            finish();
            return;
        } else if (top instanceof ToyosuTopicDetailScreen) {
            replaceFragment(getMainContainerId(), ToyosuNewsScreen.getInstance(), true);
        } else if (top instanceof ToyosuPrizeDetailScreen) {
            replaceFragment(getMainContainerId(), ToyosuPrizeScreen.getInstance(), true);
        } else if (top instanceof ToyosuCouponDetailScreen) {
            replaceFragment(getMainContainerId(), ToyosuCouponScreen.getInstance(), true);
        }
    }

    @Override
    public void onBaseResume() {
        Log.e("Main", "onBaseResume Main " );
        tracker = new SoftKeyBoardTracker(getWindow().getDecorView().getRootView(), this);
    }

    @Override
    protected void onPause() {
        if (tracker != null)
            tracker.remove();
        super.onPause();
    }

    @Override
    public void onBaseFree() {
        unregisterSingleAction(R.id.activity_main_footer_ll_home_tab,
                R.id.activity_main_footer_ll_prize_tab,
                R.id.activity_main_footer_ll_coupon_tab,
                R.id.activity_main_footer_ll_shop_tab,
                R.id.activity_main_footer_ll_news_tab,
                R.id.activity_main_footer_ll_setting_tab,
                R.id.activity_main_header_ll_action_bar_back,
                R.id.activity_main_header_rl_book_click,
                R.id.activity_main_header_rl_card_click,
                R.id.activity_main_header_img_camera,
                R.id.activity_main_header_img_cart);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == QR_CODE_SCAN_REQUEST_CODE) {
            if (resultCode == RESULT_OK) {
                if (getTopFragment(getMainContainerId()) instanceof ToyosuPrizeScreen) {
                    ((ToyosuPrizeScreen) getTopFragment(getMainContainerId())).onQRCodeReceived(data.getStringExtra(ToyosuQRCodeScanScreen.QR_CONTENT));
                }
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == CAMERA_PERMISSION_REQUEST_CODE) {
            if (permissions[0].equals(Manifest.permission.CAMERA) && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                Intent qr = new Intent(this, ToyosuQRCodeScanScreen.class);
                startActivityForResult(qr, QR_CODE_SCAN_REQUEST_CODE);
            }
        }
    }

    @Override
    public void onSingleClick(View v) {
        switch (v.getId()) {
            case R.id.activity_main_header_img_camera:
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    if (checkSelfPermission(Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                        ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.CAMERA}, CAMERA_PERMISSION_REQUEST_CODE);
                        return;
                    }
                }
                Intent qr = new Intent(this, ToyosuQRCodeScanScreen.class);
                startActivityForResult(qr, QR_CODE_SCAN_REQUEST_CODE);

                break;
            case R.id.activity_main_footer_ll_home_tab:
                Log.e("Main", "ToyosuMyPageScreen");
                if (UserDataManager.getInstance().isEnabled(DataKey.LOGGED)) {
                    if (backToHome){
                        if (getMainContainerId() == R.id.activity_main_setting_container) {
                            popAllBackStack(R.id.activity_main_setting_container);
                            setMainContainerId(R.id.activity_main_home_container);
                        }
                        replaceFragment(getMainContainerId(), ToyosuMyPageScreen.getInstance(), true);
                        backToHome = false;
                    }else{
                        if (getMainContainerId() == R.id.activity_main_home_container) {
                            if (getFragmentCount(getMainContainerId()) > 1) {
                                backStack(getMainContainerId(), ToyosuMyPageScreen.class.getSimpleName());
                            }
                        }else {
                            setMainContainerId(R.id.activity_main_home_container);
                        }
                        if (getFragmentCount(R.id.activity_main_home_container) <= 0) {
                            addFragment(R.id.activity_main_home_container, ToyosuMyPageScreen.getInstance());
                        }else{
                            Log.e("Main", "backStack ToyosuMyPageScreen");
                            ToyosuBaseFragment screen = (ToyosuBaseFragment) getTopFragment(getMainContainerId());
                            if (screen instanceof ToyosuMyPageScreen) {
                                screen.reloadData();
                            }
                        }
                    }
                    toggleFooter(v.getId());
                }else{
                    setMainContainerId(R.id.activity_main_home_container);
                    addFragment(R.id.activity_main_home_container, ToyosuLoginScreen.getInstance(true));
                }
                break;
            case R.id.activity_main_footer_ll_prize_tab:
                if (getMainContainerId() == R.id.activity_main_prize_container) {
                    if (getFragmentCount(getMainContainerId()) > 1)
                        backStack(getMainContainerId(), ToyosuPrizeScreen.class.getSimpleName());
                } else {
                    setMainContainerId(R.id.activity_main_prize_container);
                }
                if (getFragmentCount(R.id.activity_main_prize_container) <= 0) {
                    addFragment(R.id.activity_main_prize_container, ToyosuPrizeScreen.getInstance());
                }
                toggleFooter(v.getId());
                break;
            case R.id.activity_main_footer_ll_coupon_tab:
                if (getMainContainerId() == R.id.activity_main_coupon_container) {
                    if (getFragmentCount(getMainContainerId()) > 1)
                        backStack(getMainContainerId(), ToyosuCouponScreen.class.getSimpleName());
                } else {
                    setMainContainerId(R.id.activity_main_coupon_container);
                }
                if (getFragmentCount(R.id.activity_main_coupon_container) <= 0) {
                    addFragment(R.id.activity_main_coupon_container, ToyosuCouponScreen.getInstance());
                }
                toggleFooter(v.getId());
                break;
            case R.id.activity_main_footer_ll_shop_tab:
                if (getMainContainerId() == R.id.activity_main_shop_container) {
                    if (getFragmentCount(getMainContainerId()) > 1)
                        backStack(getMainContainerId(), ToyosuRestaurantsScreen.class.getSimpleName());
                } else {
                    setMainContainerId(R.id.activity_main_shop_container);
                }
                if (getFragmentCount(R.id.activity_main_shop_container) <= 0) {
                    addFragment(R.id.activity_main_shop_container, ToyosuRestaurantsScreen.getInstance());
                }
                toggleFooter(v.getId());
                break;
            case R.id.activity_main_footer_ll_news_tab:
                if (getMainContainerId() == R.id.activity_main_news_container) {
                    if (getFragmentCount(getMainContainerId()) > 1)
                        backStack(getMainContainerId(), ToyosuNewsScreen.class.getSimpleName());
                } else {
                    setMainContainerId(R.id.activity_main_news_container);
                }
                if (getFragmentCount(R.id.activity_main_news_container) <= 0) {
                    addFragment(R.id.activity_main_news_container, ToyosuNewsScreen.getInstance());
                }
                toggleFooter(v.getId());
                break;
            case R.id.activity_main_footer_ll_setting_tab:
                if (getMainContainerId() == R.id.activity_main_setting_container) {
                    if (getFragmentCount(getMainContainerId()) > 1)
                        backStack(getMainContainerId(), ToyosuSettingScreen.class.getSimpleName());
                } else {
                    setMainContainerId(R.id.activity_main_setting_container);
                }
                if (getFragmentCount(R.id.activity_main_setting_container) <= 0) {
                    addFragment(R.id.activity_main_setting_container, ToyosuSettingScreen.getInstance());
                }
                toggleFooter(v.getId());
                break;
            case R.id.activity_main_header_ll_action_bar_back:
                if (getTopFragment(getMainContainerId()) instanceof ToyosuLoginScreen) {
                    if (getFragmentCount(getMainContainerId()) > 1)
                        backStack(getMainContainerId(), ToyosuMyPageScreen.class.getSimpleName());
                    else
                        onBackPressed();
                    toggleFooter(R.id.activity_main_footer_ll_home_tab);
                }else {
                    onBackPressed();
                }
                break;
            case R.id.activity_main_header_rl_book_click:
                transitBookingScreen();
                break;
            case R.id.activity_main_header_rl_card_click:
                if (getTopFragment(getMainContainerId()) instanceof ToyosuMyPageScreen) {
                    ((ToyosuMyPageScreen) getTopFragment(getMainContainerId())).shouPopupCard();
                    changeViewDarkness(main_view);
                }
                Log.e("Main", "onSingleClick: cart");
            //    addFragment(getMainContainerId(), ToyosuMemberShipScreen.getInstance());
                break;
            case R.id.activity_main_header_img_cart:
                if (getTopFragment(getMainContainerId()) instanceof ToyosuMyPageScreen) {
                    ((ToyosuMyPageScreen) getTopFragment(getMainContainerId())).openURL();
                }
                Log.e("Main", "onSingleClick: cart");
                break;

        }
    }

    private void transitBookingScreen() {
        ToyosuBookingScreen old = null;
        ToyosuBookingScreen instance = null;
        int oldContainer = -1;
        Set<Integer> containers = getContainers().keySet();
        for (int container : containers) {
            if (getTopFragment(container) != null && getTopFragment(container) instanceof ToyosuBookingScreen) {
                old = (ToyosuBookingScreen) getTopFragment(oldContainer = container);
                break;
            }
        }
        if (old == null) {
            instance = ToyosuBookingScreen.getInstance(null, null);
        } else {
            instance = ToyosuBookingScreen.getInstance(old.getBooking(), old.getRestaurant());
            removeFragment(oldContainer, old.getUniqueTag());
        }
        addFragment(getMainContainerId(), instance);
    }

    public void toggleFooter(@IdRes int selected) {
        resetFooterStatus();
        switch (selected) {
            case R.id.activity_main_footer_ll_home_tab:
                activity_main_footer_img_home.setSelected(true);
                activity_main_footer_tv_home.setSelected(true);
                activity_main_home_container.setVisibility(View.VISIBLE);
                break;
            case R.id.activity_main_footer_ll_prize_tab:
                activity_main_footer_img_prize.setSelected(true);
                activity_main_footer_tv_prize.setSelected(true);
                activity_main_prize_container.setVisibility(View.VISIBLE);
                break;
            case R.id.activity_main_footer_ll_coupon_tab:
                activity_main_footer_img_coupon.setSelected(true);
                activity_main_footer_tv_coupon.setSelected(true);
                activity_main_coupon_container.setVisibility(View.VISIBLE);
                break;
            case R.id.activity_main_footer_ll_shop_tab:
                activity_main_footer_img_shop.setSelected(true);
                activity_main_footer_tv_shop.setSelected(true);
                activity_main_shop_container.setVisibility(View.VISIBLE);
                break;
            case R.id.activity_main_footer_ll_news_tab:
                activity_main_footer_img_news.setSelected(true);
                activity_main_footer_tv_news.setSelected(true);
                activity_main_news_container.setVisibility(View.VISIBLE);
                break;
            case R.id.activity_main_footer_ll_setting_tab:
                activity_main_footer_img_setting.setSelected(true);
                activity_main_footer_tv_setting.setSelected(true);
                activity_main_setting_container.setVisibility(View.VISIBLE);
                break;
        }
        updateActionBarStage();
    }

    private void resetFooterStatus() {
        activity_main_footer_img_setting.setSelected(false);
        activity_main_footer_img_shop.setSelected(false);
        activity_main_footer_img_news.setSelected(false);
        activity_main_footer_img_coupon.setSelected(false);
        activity_main_footer_img_prize.setSelected(false);
        activity_main_footer_img_home.setSelected(false);
        activity_main_footer_tv_setting.setSelected(false);
        activity_main_footer_tv_shop.setSelected(false);
        activity_main_footer_tv_news.setSelected(false);
        activity_main_footer_tv_coupon.setSelected(false);
        activity_main_footer_tv_prize.setSelected(false);
        activity_main_footer_tv_home.setSelected(false);
        activity_main_setting_container.setVisibility(View.GONE);
        activity_main_shop_container.setVisibility(View.GONE);
        activity_main_news_container.setVisibility(View.GONE);
        activity_main_coupon_container.setVisibility(View.GONE);
        activity_main_prize_container.setVisibility(View.GONE);
        activity_main_home_container.setVisibility(View.GONE);
    }

    public void updateActionBarStage() {
        ToyosuBaseFragment screen = (ToyosuBaseFragment) getTopFragment(getMainContainerId());
        activity_main_header_tv_page_title.setText(Constant.BLANK);
        if (screen instanceof ToyosuMyPageScreen) {
            showCartButton();
            showCardButton();
            showFooterBar();
            hideBackButton();
            hideLogo();
            showPageTitle(getString(R.string.mypage_lb_title));
            showRightButton();
        } else if (screen instanceof ToyosuTopicDetailScreen) {
            showBookingButton();
            hideCartButton();
            showFooterBar();
            showBackButton();
            hideLogo();
            hidePageTitle();
            showRightButton();
        }
        else if (screen instanceof ToyosuMenuScreen) {
            showBookingButton();
            hideCartButton();
            showFooterBar();
            hideBackButton();
            hideLogo();
            showPageTitle(getString(R.string.prize_lb_title));
            showRightButton();
        }
        else if (screen instanceof ToyosuFoodDetailScreen) {
            showBookingButton();
            hideCartButton();
            showFooterBar();
            showBackButton();
            hideLogo();
            hidePageTitle();
            showRightButton();
        } else if (screen instanceof ToyosuCouponScreen) {
            showBookingButton();
            hideCartButton();
            showFooterBar();
            hideBackButton();
            hideLogo();
            showPageTitle(getString(R.string.coupon_lb_title));
            showRightButton();
        } else if (screen instanceof ToyosuCouponDetailScreen) {
            showBookingButton();
            hideCartButton();
            showFooterBar();
            showBackButton();
            hideLogo();
            hidePageTitle();
            showRightButton();
        } else if (screen instanceof ToyosuRestaurantDetailScreen) {
            showFooterBar();
            hideCartButton();
            showBackButton();
            hideLogo();
            showPageTitle(getString(R.string.shop_info_lb_title));
            showRightButton();
        }else if (screen instanceof ToyosuNewsScreen) {
            showBookingButton();
            showFooterBar();
            hideCartButton();
            hideBackButton();
            hideLogo();
            showPageTitle(getString(R.string.news_lb_title));
            showRightButton();
        } else if (screen instanceof ToyosuSettingScreen) {
            hideCartButton();
            showFooterBar();
            hideBackButton();
            hideLogo();
            showPageTitle(getString(R.string.other_lb_other));
            hideRightButton();
        } else if (screen instanceof ToyosuLegalInformationScreen) {
//            showBookingButton();
            hideCartButton();
            showFooterBar();
            showBackButton();
            hideLogo();
            showPageTitle(((ToyosuLegalInformationScreen) screen).getTitle());
            showRightButton();
        } else if (screen instanceof ToyosuContactUsScreen) {
//            showBookingButton();
            hideCartButton();
            showFooterBar();
            showBackButton();
            hideLogo();
            showPageTitle(getString(R.string.other_lb_contact_us));
            showRightButton();
        } else if (screen instanceof ToyosuBookingScreen) {
            showFooterBar();
            hideCartButton();
            showBackButton();
            hideLogo();
            showPageTitle(getString(R.string.reservation_lb_request));
            hideRightButton();
        } else if (screen instanceof ToyosuLoginScreen) {
            hideBackButton();
            hideCartButton();
            hideLogo();
            showPageTitle(getString(R.string.customer_lb_login));
            hideRightButton();
            hideCartButton();
            hideFooterBar();
        } else if (screen instanceof ToyosuPasswordRecoveryScreen) {
            showBackButton();
            hideCartButton();
            hideLogo();
            hidePageTitle();
            hideRightButton();
            hideFooterBar();
        } else if (screen instanceof ToyosuRegisterScreen) {
            showBackButton();
            hideCartButton();
            hideLogo();
            hidePageTitle();
            hideRightButton();
            hideFooterBar();
        } else if (screen instanceof ToyosuUserUpdateScreen) {
            showBackButton();
            hideCartButton();
            hideLogo();
            showPageTitle(getString(R.string.other_lb_update_profile));
            hideRightButton();
            showFooterBar();
        } else if (screen instanceof ToyosuRestaurantsScreen) {
            showBookingButton();
            hideCartButton();
            showFooterBar();
            showBackButton();
            hideLogo();
            showPageTitle(getString(R.string.shop_info_lb_title));
            showRightButton();
        } else if (screen instanceof ToyosuMemberShipScreen) {
            showFooterBar();
            hideCartButton();
            showBackButton();
            hideLogo();
            showPageTitle(getString(R.string.membership_lb_card));
            hideRightButton();
        } else if (screen instanceof ToyosuPrizeScreen) {
            showCameraButton();
            hideCartButton();
            showFooterBar();
            hideBackButton();
            hideLogo();
            showPageTitle(getString(R.string.prize_lb_title));
            showRightButton();
        } else if (screen instanceof ToyosuPrizeDetailScreen) {
            showFooterBar();
            hideCartButton();
            showBackButton();
            hideLogo();
            hideRightButton();
        }
    }

    @Override
    public void onAnimationStart(Animation animation) {
    }

    @Override
    public void onAnimationEnd(Animation animation) {
        if (animation.equals(ani_back_in)) {
            activity_main_header_rp_action_bar_back.setVisibility(View.VISIBLE);
        } else if (animation.equals(ani_back_out)) {
            activity_main_header_rp_action_bar_back.setVisibility(View.GONE);
        } else if (animation.equals(ani_logo_fade_in)) {
            activity_main_header_img_logo.setVisibility(View.VISIBLE);
        } else if (animation.equals(ani_logo_fade_out)) {
            activity_main_header_img_logo.setVisibility(View.GONE);
        } else if (animation.equals(ani_title_fade_out)) {
            activity_main_header_tv_page_title.setVisibility(View.GONE);
        } else if (animation.equals(ani_title_fade_in)) {
            activity_main_header_tv_page_title.setVisibility(View.VISIBLE);
        } else if (animation.equals(ani_right_bt_in)) {
            activity_main_header_rl_right_buttons.setVisibility(View.VISIBLE);
        } else if (animation.equals(ani_right_bt_out)) {
            activity_main_header_rl_right_buttons.setVisibility(View.GONE);
        } else if (animation.equals(ani_footer_in)) {
            activity_main_rl_footer.setVisibility(View.VISIBLE);
        } else if (animation.equals(ani_footer_out)) {
            activity_main_rl_footer.setVisibility(View.GONE);
        }
    }

    @Override
    public void onAnimationRepeat(Animation animation) {

    }

    @Override
    public int getEnterInAnimation() {
        return 0;
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        refreshViewDataLanguage();
        invalidateScreens();
        closeLoadingDialog();
    }

    public void changeLanguage(String language) {
        Resources resources = getResources();
        Locale locale = new Locale(language);
        Locale.setDefault(locale);
        final Configuration config = new Configuration(resources.getConfiguration());
        config.locale = locale;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
            config.setLayoutDirection(config.locale);
        }
        resources.updateConfiguration(config,
                resources.getDisplayMetrics());

        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                onConfigurationChanged(config);
            }
        });
    }

    public void invalidateScreens() {
        HashMap<Integer, ArrayList<String>> containers = getContainers();
        if (containers != null) {
            for (int containerId : containers.keySet()) {
                ArrayList<String> tags = containers.get(containerId);
                for (String tag : tags) {
                    BaseFragment fragment = getFragment(containerId, tag);
                    if (fragment instanceof ToyosuBaseFragment)
                        ((ToyosuBaseFragment) fragment).refreshViewDataLanguage();
                }
            }
        }
    }

    private void refreshViewDataLanguage() {
        ToyosuBaseFragment screen = (ToyosuBaseFragment) getTopFragment(getMainContainerId());

        if (screen instanceof ToyosuPrizeScreen) {
            activity_main_header_tv_page_title.setText(R.string.prize_lb_title);
        } else if (screen instanceof ToyosuCouponScreen) {
            activity_main_header_tv_page_title.setText(R.string.coupon_lb_title);
        } else if (screen instanceof ToyosuRestaurantDetailScreen) {
            activity_main_header_tv_page_title.setText(R.string.shop_info_lb_title);
        } else if (screen instanceof ToyosuSettingScreen) {
            activity_main_header_tv_page_title.setText(R.string.other_lb_other);
        } else if (screen instanceof ToyosuContactUsScreen) {
            activity_main_header_tv_page_title.setText(R.string.other_lb_contact_us);
        } else if (screen instanceof ToyosuBookingScreen) {
            activity_main_header_tv_page_title.setText(R.string.header_lb_booking);
        } else if (screen instanceof ToyosuLoginScreen) {
            activity_main_header_tv_page_title.setText(R.string.customer_lb_login);
        } else if (screen instanceof ToyosuUserUpdateScreen) {
            activity_main_header_tv_page_title.setText(R.string.other_lb_update_profile);
        } else if (screen instanceof ToyosuRestaurantsScreen) {
            activity_main_header_tv_page_title.setText(R.string.shop_info_lb_title);
        } else if (screen instanceof ToyosuNewsScreen) {
            activity_main_header_tv_page_title.setText(R.string.news_lb_title);
        } else if (screen instanceof ToyosuMemberShipScreen) {
            activity_main_header_tv_page_title.setText(R.string.membership_lb_card);
        }
        activity_main_header_tv_back.setText(R.string.common_back);
        activity_main_footer_tv_home.setText(R.string.mypage_lb_title);
        activity_main_footer_tv_prize.setText(R.string.prize_lb_title);
        activity_main_footer_tv_coupon.setText(R.string.coupon_lb_title);
        activity_main_footer_tv_shop.setText(R.string.shop_info_lb_title);
        activity_main_footer_tv_news.setText(R.string.news_lb_title);
        activity_main_footer_tv_setting.setText(R.string.other_lb_setting);
    }

    private void showHomeButton() {
        if (activity_main_header_rp_action_bar_back.getVisibility() == View.GONE)
            activity_main_header_rp_action_bar_back.startAnimation(ani_back_in);
        activity_main_header_tv_back.setText(ToyosuConstant.BLANK);
        activity_main_header_img_back.setImageResource(R.drawable.ic_home_back);
    }

    private void showBackButton() {
        if (activity_main_header_rp_action_bar_back.getVisibility() == View.GONE)
            activity_main_header_rp_action_bar_back.startAnimation(ani_back_in);
        activity_main_header_tv_back.setVisibility(View.GONE);
        int count = getFragmentCount(getMainContainerId());
        if (count > 2) {
            activity_main_header_tv_back.setText(R.string.common_back);
        } else if (count == 2) {
            switch (getMainContainerId()) {
                case R.id.activity_main_home_container:
                    if (getFragment(getMainContainerId(), ToyosuMyPageScreen.getInstance().getUniqueTag()) != null) {
                        activity_main_header_tv_back.setText(R.string.mypage_lb_title);
                    } else {
                        activity_main_header_tv_back.setText(R.string.common_back);
                    }
                    break;
                case R.id.activity_main_prize_container:
                    if (getFragment(getMainContainerId(), ToyosuPrizeScreen.getInstance().getUniqueTag()) != null) {
                        activity_main_header_tv_back.setText(R.string.prize_lb_title);
                    } else {
                        activity_main_header_tv_back.setText(R.string.common_back);
                    }
                    break;
                case R.id.activity_main_coupon_container:
                    if (getFragment(getMainContainerId(), ToyosuCouponScreen.getInstance().getUniqueTag()) != null) {
                        activity_main_header_tv_back.setText(R.string.coupon_lb_title);
                    } else {
                        activity_main_header_tv_back.setText(R.string.common_back);
                    }
                    break;
                case R.id.activity_main_shop_container:
                    if (getFragment(getMainContainerId(), ToyosuRestaurantsScreen.getInstance().getUniqueTag()) != null) {
                        activity_main_header_tv_back.setText(R.string.shop_info_lb_title);
                    } else {
                        activity_main_header_tv_back.setText(R.string.common_back);
                    }
                    break;
                case R.id.activity_main_news_container:
                    if (getFragment(getMainContainerId(), ToyosuNewsScreen.getInstance().getUniqueTag()) != null) {
                        activity_main_header_tv_back.setText(R.string.news_lb_title);
                    } else {
                        activity_main_header_tv_back.setText(R.string.common_back);
                    }
                    break;
                case R.id.activity_main_setting_container:
                    if (getFragment(getMainContainerId(), ToyosuSettingScreen.getInstance().getUniqueTag()) != null) {
                        activity_main_header_tv_back.setText(R.string.other_lb_setting);
                    } else {
                        activity_main_header_tv_back.setText(R.string.common_back);
                    }
                    break;
            }
        } else {
            switch (getMainContainerId()) {
                case R.id.activity_main_home_container:
                    activity_main_header_tv_back.setText(R.string.mypage_lb_title);
                    break;
                case R.id.activity_main_prize_container:
                    activity_main_header_tv_back.setText(R.string.prize_lb_title);
                    break;
                case R.id.activity_main_coupon_container:
                    activity_main_header_tv_back.setText(R.string.coupon_lb_title);
                    break;
                case R.id.activity_main_shop_container:
                    activity_main_header_tv_back.setText(R.string.shop_info_lb_title);
                    break;
                case R.id.activity_main_news_container:
                    activity_main_header_tv_back.setText(R.string.news_lb_title);
                    break;
                case R.id.activity_main_setting_container:
                    activity_main_header_tv_back.setText(R.string.other_lb_setting);
                    break;
            }
        }
        activity_main_header_tv_back.setVisibility(View.VISIBLE);
        activity_main_header_img_back.setImageResource(R.drawable.ic_back);
    }

    private void hideBackButton() {
        if (activity_main_header_rp_action_bar_back.getVisibility() == View.VISIBLE)
            activity_main_header_rp_action_bar_back.startAnimation(ani_back_out);
    }

    private void showLogo() {
        if (activity_main_header_img_logo.getVisibility() == View.GONE)
            activity_main_header_img_logo.startAnimation(ani_logo_fade_in);
    }

    private void hideLogo() {
        if (activity_main_header_img_logo.getVisibility() == View.VISIBLE)
            activity_main_header_img_logo.startAnimation(ani_logo_fade_out);
    }

    private void showPageTitle(String title) {
        if (activity_main_header_tv_page_title.getVisibility() == View.GONE) {
            activity_main_header_tv_page_title.startAnimation(ani_title_fade_in);
        }
        activity_main_header_tv_page_title.setText(title);
    }

    private void hidePageTitle() {
        if (activity_main_header_tv_page_title.getVisibility() == View.VISIBLE)
            activity_main_header_tv_page_title.startAnimation(ani_title_fade_out);
    }

    public void showRightButton() {

        if (activity_main_header_rl_right_buttons != null && activity_main_header_rl_right_buttons.getVisibility() == View.GONE)
            activity_main_header_rl_right_buttons.startAnimation(ani_right_bt_in);
    }

    public void hideRightButton() {
        if (activity_main_header_rl_right_buttons != null && activity_main_header_rl_right_buttons.getVisibility() == View.VISIBLE)
            activity_main_header_rl_right_buttons.startAnimation(ani_right_bt_out);
    }


    public void showBookingButton() {
        activity_main_header_rp_camera.setVisibility(View.GONE);
        activity_main_header_rp_book.setVisibility(View.VISIBLE);
        activity_main_header_rp_card.setVisibility(View.GONE);
    }

    public void showCardButton() {
        activity_main_header_rp_camera.setVisibility(View.GONE);
        activity_main_header_rp_book.setVisibility(View.GONE);
        activity_main_header_rp_card.setVisibility(View.VISIBLE);
    }

    public void showCartButton() {
        activity_main_header_rp_cart.setVisibility(View.VISIBLE);
    }

    public void hideCartButton() {
        activity_main_header_rp_cart.setVisibility(View.GONE);
    }

    public void showCameraButton() {
        activity_main_header_rp_card.setVisibility(View.GONE);
        activity_main_header_rp_book.setVisibility(View.GONE);
        activity_main_header_rp_camera.setVisibility(View.VISIBLE);
    }

    private void hideFooterBar() {
        if (activity_main_rl_footer != null && activity_main_rl_footer.getVisibility() == View.VISIBLE) {
            activity_main_rl_footer.startAnimation(ani_footer_out);
        }
    }

    private void showFooterBar() {
        if (activity_main_rl_footer != null && activity_main_rl_footer.getVisibility() == View.GONE) {
            activity_main_rl_footer.startAnimation(ani_footer_in);
        }
    }

    public User getLoggedUser() {
        try {
//            if (user == null) {
                user = new Gson().fromJson(UserDataManager.getInstance().getString(DataKey.USER), User.class);
//            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return user;
    }

    public void setLoggedUser(User user) {
        Log.e("User", "setLoggedUser name: " + user.name );
        UserDataManager.getInstance().setString(DataKey.USER, new Gson().toJson(user));
    }

    public void backToHome() {
        if (getTopFragment(getMainContainerId()) instanceof ToyosuLoginScreen) {
            backToHome = true;
            activity_main_footer_ll_home_tab.performClick();
        }
    }

    public void backToLogin() {
        replaceFragment(getMainContainerId(), ToyosuLoginScreen.getInstance(true), true);
    }

    @Override
    public void onKeyBoardShown(EditText focused) {
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                hideFooterBar();
            }
        }, 500);

    }

    @Override
    public int getGeneralDialogLayoutResource() {
        return R.layout.toyosu_general_dialog;
    }

    @Override
    public void onKeyBoardHidden(EditText unFocused) {
        BaseFragment top = getTopFragment(getMainContainerId());
        if (!(top instanceof ToyosuLoginScreen || top instanceof ToyosuPasswordRecoveryScreen || top instanceof ToyosuRegisterScreen)) {
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    if (activity_main_rl_footer != null && activity_main_rl_footer.getVisibility() == View.GONE) {
                        activity_main_rl_footer.setVisibility(View.VISIBLE);
                    }
                }
            }, 500);
        }
    }

    private void changeViewBrightness(final View view) {
        // Load initial and final colors.
        final int initialColor = getResources().getColor(R.color.White);
        final int finalColor = getResources().getColor(R.color.ColorBackground);

        ValueAnimator anim = ValueAnimator.ofFloat(0, 1);
        anim.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator animation) {
                // Use animation position to blend colors.
                float position = animation.getAnimatedFraction();
                int blended = blendColors(initialColor, finalColor, position);

                // Apply blended color to the view.
                view.setBackgroundColor(blended);
            }
        });

        anim.setDuration(1000).start();
    }

    private void changeViewDarkness(final View view) {
        // Load initial and final colors.
        final int initialColor = getResources().getColor(R.color.Black);
        final int finalColor = getResources().getColor(R.color.ColorBackground);

        ValueAnimator anim = ValueAnimator.ofFloat(0, 1);
        anim.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator animation) {
                // Use animation position to blend colors.
                float position = animation.getAnimatedFraction();
                int blended = blendColors(initialColor, finalColor, position);

                // Apply blended color to the view.
                view.setBackgroundColor(blended);
            }
        });

        anim.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                changeViewBrightness(view);
            }
        });

        anim.setDuration(500).start();
    }

    private int blendColors(int from, int to, float ratio) {
        final float inverseRatio = 1f - ratio;

        final float r = Color.red(to) * ratio + Color.red(from) * inverseRatio;
        final float g = Color.green(to) * ratio + Color.green(from) * inverseRatio;
        final float b = Color.blue(to) * ratio + Color.blue(from) * inverseRatio;

        return Color.rgb((int) r, (int) g, (int) b);
    }

}
