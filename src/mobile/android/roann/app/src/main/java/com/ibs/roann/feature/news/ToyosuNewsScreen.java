package com.ibs.roann.feature.news;

import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Parcelable;
import android.support.annotation.Nullable;
import android.support.annotation.StringRes;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.ibs.roann.R;
import com.ibs.roann.feature.ToyosuApplication;
import com.ibs.roann.feature.ToyosuBaseFragment;
import com.ibs.roann.model.Instagram;
import com.ibs.roann.model.Pagination;
import com.ibs.roann.model.api.InstagramResult;
import com.ibs.roann.model.api.TopicsParam;
import com.ibs.roann.model.api.TopicsResult;
import com.ibs.roann.view.fetchable.MutableRecycleView;
import com.ibs.roann.view.fetchable.MutableSwipeLayout;
import com.ibs.roann.view.fetchable.RecycleInterface;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import core.base.BaseResult;
import core.util.Constant;
import core.util.RequestTarget;
import icepick.State;

import static com.ibs.roann.feature.ToyosuConstant.NUMBER_INSTAGRAM;


public class ToyosuNewsScreen extends ToyosuBaseFragment implements MutableRecycleView.OnRefreshListener, MutableRecycleView.OnLoadMoreListener, RecycleInterface<Instagram> {

    @State
    ArrayList<Instagram> data;
    Pagination pagination;
    @BindView(R.id.home_screen_tv_reload)
    TextView home_screen_tv_reload;
    @State
    @StringRes
    int error_string_id = R.string.common_pull_to_reload;
    private MutableSwipeLayout home_screen_sl_list;
    private MutableRecycleView home_screen_rv_list;
    private ToyosuNewsInstagramAdapter adapter;

    private String max_id = "";

    public static ToyosuNewsScreen getInstance() {
        return new ToyosuNewsScreen();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.toyosu_home_screen, container, false);
    }

    @Override
    public void onBaseCreate() {
        super.onBaseCreate();
        data = new ArrayList<>();
        adapter = new ToyosuNewsInstagramAdapter(ToyosuApplication.getActiveActivity().getLayoutInflater(), data, this, getSingleTouch(), getSingleClick());
    }

    @Override
    public void onBindView() {
        super.onBindView();
        home_screen_sl_list = (MutableSwipeLayout) findViewById(R.id.home_screen_sl_list);
        home_screen_rv_list = (MutableRecycleView) findViewById(R.id.home_screen_rv_list);
        home_screen_sl_list.setEnableRefreshProgress(true);
        home_screen_rv_list.setColorScheme(ContextCompat.getColor(getContext(), R.color.primary));
        home_screen_rv_list.setOnRefreshListener(this);
        home_screen_rv_list.setOnLoadMoreListener(this);
        home_screen_rv_list.setAdapter(adapter);
    }

    @Override
    public void onInitializeViewData() {
        super.onInitializeViewData();
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                home_screen_rv_list.onRefresh();
            }
        }, 1000);
    }

    @Override
    public void reloadData() {

    }

    @Override
    public int getEnterInAnimation() {
        return 0;
    }

    @Override
    public void onRefresh() {
        requestTopics("");
    }

    private void requestTopics(String id) {
        Log.e("load posts", "requestTopics max_id: " + id );
        makeRequest(getUniqueTag(), false, new TopicsParam(id), this, RequestTarget.POST_INSTAGRAM);
    }

    @Override
    public boolean shouldOverrideLoadMore() {
        return true;
    }

    @Override
    public void onLoadMore() {
        Log.e("news", "onLoadMore next_max_id: " + pagination.next_max_id );
        if (data != null && pagination.next_max_id != null)
            max_id = pagination.next_max_id;
            requestTopics(max_id);
    }

    @Override
    public boolean shouldOverrideRefresh() {
        return true;
    }


    @Override
    public void onItemClick(View view, Instagram item, int position, int type) {
        if (item != null){
            Uri uri = Uri.parse(item.link);
            Intent likeIng = new Intent(Intent.ACTION_VIEW, uri);

            likeIng.setPackage("com.instagram.android");

            try {
                startActivity(likeIng);
            } catch (ActivityNotFoundException e) {
                startActivity(new Intent(Intent.ACTION_VIEW,
                        Uri.parse(item.link)));
            }
        }
//            addFragment(getMainContainerId(), ToyosuTopicDetailScreen.getInstance(item));
    }

    @Override
    public void onSingleClick(View v) {
        Object data = v.getTag();
        if (data != null && data instanceof Instagram) {
            Instagram topic = (Instagram) data;
            switch (v.getId()) {
                case R.id.topic_item_img_share:
            //        shareTopic(topic.address_url);
                    break;
                case R.id.topic_item_img_onigiri:
                    break;
            }
        }

    }

    private void shareTopic(String url) {
        Intent share = new Intent(Intent.ACTION_SEND);
        share.setType("text/plain");
        share.putExtra(Intent.EXTRA_SUBJECT, getString(R.string.app_name));
        share.putExtra(Intent.EXTRA_TEXT, url);
        PackageManager pm = getActiveActivity().getPackageManager();
        List<ResolveInfo> resInfo = pm.queryIntentActivities(share, 0);
        List<String> forbiddenChoices = new ArrayList<>();
        for (int i = 0; i < resInfo.size(); i++) {
            ResolveInfo ri = resInfo.get(i);
            String packageName = ri.activityInfo.packageName;
            forbiddenChoices.add(packageName);
            if (packageName.contains("twitter") || packageName.contains("facebook") || packageName.contains("com.google.android.apps.plus")) {
                forbiddenChoices.remove(packageName);
            }
        }
        startActivity(generateCustomChooserIntent(share, forbiddenChoices.toArray(new String[forbiddenChoices.size()])));
    }

    private Intent generateCustomChooserIntent(Intent prototype, String[] forbiddenChoices) {
        List<Intent> targetedShareIntents = new ArrayList<>();
        List<HashMap<String, String>> intentMetaInfo = new ArrayList<>();
        Intent chooserIntent;

        Intent dummy = new Intent(prototype.getAction());
        dummy.setType(prototype.getType());
        List<ResolveInfo> resInfo = getActiveActivity().getPackageManager().queryIntentActivities(dummy, 0);

        if (!resInfo.isEmpty()) {
            for (ResolveInfo resolveInfo : resInfo) {
                if (resolveInfo.activityInfo == null || Arrays.asList(forbiddenChoices).contains(resolveInfo.activityInfo.packageName))
                    continue;

                HashMap<String, String> info = new HashMap<>();
                info.put("packageName", resolveInfo.activityInfo.packageName);
                info.put("className", resolveInfo.activityInfo.name);
                info.put("simpleName", String.valueOf(resolveInfo.activityInfo.loadLabel(getActiveActivity().getPackageManager())));
                intentMetaInfo.add(info);
            }

            if (!intentMetaInfo.isEmpty()) {
                // sorting for nice readability
                Collections.sort(intentMetaInfo, new Comparator<HashMap<String, String>>() {
                    @Override
                    public int compare(HashMap<String, String> map, HashMap<String, String> map2) {
                        return map.get("simpleName").compareTo(map2.get("simpleName"));
                    }
                });

                // create the custom intent list
                for (HashMap<String, String> metaInfo : intentMetaInfo) {
                    Intent targetedShareIntent = (Intent) prototype.clone();
                    targetedShareIntent.setPackage(metaInfo.get("packageName"));
                    targetedShareIntent.setClassName(metaInfo.get("packageName"), metaInfo.get("className"));
                    targetedShareIntents.add(targetedShareIntent);
                }

                chooserIntent = Intent.createChooser(targetedShareIntents.remove(targetedShareIntents.size() - 1), getString(R.string.app_name));
                chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS, targetedShareIntents.toArray(new Parcelable[]{}));
                return chooserIntent;
            }
        }

        return Intent.createChooser(prototype, getString(R.string.app_name));
    }


    @Override
    public void refreshViewDataLanguage() {
        home_screen_tv_reload.setText(error_string_id);
        if (adapter != null) {
            adapter.notifyDataSetChanged();
        }
    }

    @Override
    public void onResultSuccess(BaseResult result) {
        if (result instanceof InstagramResult) {
            if (home_screen_rv_list.isRefreshing()) {
//                home_screen_rv_list.setLimit(100);
                if (data != null) {
                    data.clear();
                    data.addAll(((InstagramResult) result).getTopics());
                    data.add(new Instagram()); // footer
                    pagination = ((InstagramResult) result).getPagination();
                    if (pagination.next_max_id!=null) {
                        Log.e("data", "isRefreshing count: " + adapter.getItemCount() );
                        home_screen_rv_list.setLimit(adapter.getItemCount() + NUMBER_INSTAGRAM -1);
                        Log.e("data", "isRefreshing: " + pagination.next_max_id );
                    }else {
                        pagination = null;
                        Log.e("data", "isRefreshing: " + adapter.getItemCount() );
                        home_screen_rv_list.setLimit(adapter.getItemCount() -1);
                    }
                }
                home_screen_rv_list.onRefreshComplete();
            } else if (home_screen_rv_list.isLoadingMore()) {
                if (data != null) {
                    for (int i = 0; i < data.size(); i++) {
                        if (data.get(i).post_id.equals("-1"))
                            data.remove(i);
                    }
                    data.addAll(((InstagramResult) result).getTopics());
                    data.add(new Instagram()); // footer
                    pagination = ((InstagramResult) result).getPagination();

                    if (pagination.next_max_id!=null) {
                        Log.e("data", "isLoadingMore count: " + adapter.getItemCount() );
                        home_screen_rv_list.setLimit(adapter.getItemCount() + NUMBER_INSTAGRAM -1);
                        Log.e("data", "isLoadingMore: " + pagination.next_max_id );
                    }else {
                        pagination = null;
                        Log.e("data", "isLoadingMore count: " + adapter.getItemCount() );
                        home_screen_rv_list.setLimit(adapter.getItemCount() - 1);
                    }
                }
                home_screen_rv_list.onLoadMoreComplete();
            } else {
                adapter.notifyDataSetChanged();
            }
            if (adapter.getRealItemCount() <= 0) {
                home_screen_tv_reload.setText(error_string_id = R.string.common_pull_to_reload);
                home_screen_tv_reload.setVisibility(View.VISIBLE);
                home_screen_rv_list.setVisibility(View.GONE);
            } else {
                home_screen_tv_reload.setVisibility(View.GONE);
                home_screen_rv_list.setVisibility(View.VISIBLE);
            }
        }
    }

    @Override
    public void onResultFail(BaseResult result) {
        if (result instanceof TopicsResult) {
            if (home_screen_rv_list.isRefreshing()) {
                home_screen_rv_list.onRefreshComplete();
            } else if (home_screen_rv_list.isLoadingMore()) {
                home_screen_rv_list.onLoadMoreComplete();
            } else {
                adapter.notifyDataSetChanged();
            }
            if (adapter.getRealItemCount() <= 0) {
                home_screen_tv_reload.setText(error_string_id = R.string.common_error_pull_to_reload);
                home_screen_tv_reload.setVisibility(View.VISIBLE);
                home_screen_rv_list.setVisibility(View.GONE);
            } else {
                home_screen_tv_reload.setVisibility(View.GONE);
                home_screen_rv_list.setVisibility(View.VISIBLE);
            }
        }
        super.onResultFail(result);
    }

    @Override
    public void onFail(RequestTarget target, String error, Constant.StatusCode code) {
        switch (target) {
            case TOPICS:
                if (home_screen_rv_list.isRefreshing()) {
                    home_screen_rv_list.onRefreshComplete();
                } else if (home_screen_rv_list.isLoadingMore()) {
                    home_screen_rv_list.onLoadMoreComplete();
                } else {
                    adapter.notifyDataSetChanged();
                }
                if (adapter.getRealItemCount() <= 0) {
                    home_screen_tv_reload.setText(error_string_id = R.string.common_error_pull_to_reload);
                    home_screen_tv_reload.setVisibility(View.VISIBLE);
                    home_screen_rv_list.setVisibility(View.GONE);
                } else {
                    home_screen_tv_reload.setVisibility(View.GONE);
                    home_screen_rv_list.setVisibility(View.VISIBLE);
                }
                break;
        }
        super.onFail(target, error, code);
    }
}
