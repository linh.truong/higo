package com.ibs.roann.model.api;

import com.ibs.roann.feature.ToyosuBaseParam;

import org.json.JSONObject;

/**
 * Created by tuanle on 1/3/17.
 */

public class SettingByCodeParam extends ToyosuBaseParam {
    public static final String SETTING_CODE_PRIVACY = "privacy_policy";
    public static final String SETTING_ABOUT_POINTS = "about_point";
    public static final String SETTING_ABOUT_PRIZE = "about_prize";
    public static final String SETTING_CODE_TERMS = "terms_and_condition";
    public static final String SETTING_CODE_WEB_URL = "website_url";
    public static final String SETTING_RESERVATION_TC = "booking_tandc";
    private static final String PARAMS_CODE_TAG = "code";

    private String code;

    public SettingByCodeParam(String code) {
        this.code = code;
    }

    @Override
    public byte[] makeRequestBody() {
        try {
            return new JSONObject().put(PARAMS_CODE_TAG, code).toString().getBytes();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return new byte[0];
    }
}

