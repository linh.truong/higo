package com.ibs.roann.feature.authentication;

import android.database.DataSetObserver;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.SpinnerAdapter;
import android.widget.TextView;

import com.ibs.roann.R;
import com.ibs.roann.feature.ToyosuApplication;
import com.ibs.roann.model.Area;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by tuanle on 1/19/17.
 */

public class ToyosuAreaAdapter implements SpinnerAdapter {

    private ArrayList<Area> data;
    private LayoutInflater inflater;

    public ToyosuAreaAdapter(ArrayList<Area> data) {
        this.data = data;
        this.inflater = ToyosuApplication.getActiveActivity().getLayoutInflater();
    }

    @Override
    public void registerDataSetObserver(DataSetObserver observer) {

    }

    @Override
    public void unregisterDataSetObserver(DataSetObserver observer) {

    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public Area getItem(int position) {
        return data.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        IndicatorViewHolder holder;
        View view = convertView;
        if (view != null) {
            holder = (IndicatorViewHolder) view.getTag();
        } else {
            view = inflater.inflate(R.layout.toyosu_area_indicator, parent, false);
            holder = new IndicatorViewHolder(view);
            view.setTag(holder);
        }
        holder.area_indicator_tv_name.setText(getItem(position).getName());
        return view;
    }

    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {
        ItemViewHolder holder;
        View view = convertView;
        if (view != null) {
            holder = (ItemViewHolder) view.getTag();
        } else {
            view = inflater.inflate(R.layout.toyosu_area_item, parent, false);
            holder = new ItemViewHolder(view);
            view.setTag(holder);
        }
        holder.area_item_tv_name.setText(getItem(position).getName());
        return view;
    }

    @Override
    public int getItemViewType(int position) {
        return 1;
    }

    @Override
    public int getViewTypeCount() {
        return 1;
    }

    @Override
    public boolean isEmpty() {
        return false;
    }

    static class ItemViewHolder {

        @BindView(R.id.area_item_tv_name)
        TextView area_item_tv_name;

        public ItemViewHolder(View view) {
            ButterKnife.bind(this, view);
        }
    }

    static class IndicatorViewHolder {

        @BindView(R.id.area_indicator_tv_name)
        TextView area_indicator_tv_name;

        public IndicatorViewHolder(View view) {
            ButterKnife.bind(this, view);
        }
    }
}
