package com.ibs.roann.feature.shop;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.ibs.roann.R;
import com.ibs.roann.feature.ToyosuBaseFragment;
import com.ibs.roann.model.Restaurant;

import butterknife.BindView;
import icepick.State;

/**
 * Created by tuanle on 1/3/17.
 */

public class ToyosuRestaurantDetailScreen extends ToyosuBaseFragment implements OnMapReadyCallback {

    private static final String RESTAURANT = "restaurant";
    private static final int DIAL_PERMISSION_REQUEST_CODE = 9011;

    @BindView(R.id.restaurant_detail_tv_address)
    TextView restaurant_detail_tv_address;

    @BindView(R.id.restaurant_detail_tv_email)
    TextView restaurant_detail_tv_email;

    @BindView(R.id.restaurant_detail_tv_phone)
    TextView restaurant_detail_tv_phone;

    @BindView(R.id.restaurant_detail_tv_about)
    TextView restaurant_detail_tv_about;

    @BindView(R.id.restaurant_detail_tv_address_title)
    TextView restaurant_detail_tv_address_title;

    @BindView(R.id.restaurant_detail_tv_email_title)
    TextView restaurant_detail_tv_email_title;

    @BindView(R.id.restaurant_detail_tv_phone_title)
    TextView restaurant_detail_tv_phone_title;

    @BindView(R.id.restaurant_detail_tv_about_title)
    TextView restaurant_detail_tv_about_title;

    @State
    Restaurant restaurant;

    private GoogleMap map;

    public static ToyosuRestaurantDetailScreen getInstance(Restaurant restaurant) {
        ToyosuRestaurantDetailScreen screen = new ToyosuRestaurantDetailScreen();
        Bundle bundle = new Bundle();
        bundle.putSerializable(RESTAURANT, restaurant);
        screen.setArguments(bundle);
        return screen;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.toyosu_restaurant_detail_screen, container, false);
    }

    @Override
    public void onBaseCreate() {
        super.onBaseCreate();
        Bundle bundle = getArguments();
        if (bundle != null) {
            restaurant = (Restaurant) bundle.getSerializable(RESTAURANT);
        }
    }

    @Override
    public void onBindView() {
        super.onBindView();
        SupportMapFragment fragment = (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.restaurant_detail_map);
        showLoadingDialog(getActiveActivity(), getLoadingDialogLayoutResource(), getString(R.string.common_loading));
        if (fragment != null)
            fragment.getMapAsync(ToyosuRestaurantDetailScreen.this);
    }

    @Override
    public void onInitializeViewData() {
        super.onInitializeViewData();
        restaurant_detail_tv_phone.setText(restaurant.phone);
        restaurant_detail_tv_email.setText(restaurant.email);
        registerSingleAction(R.id.restaurant_detail_ll_email, R.id.restaurant_detail_ll_phone);
        refreshViewDataLanguage();
    }

    @Override
    public void reloadData() {

    }

    @Override
    public void onBaseFree() {
        unregisterSingleAction(R.id.restaurant_detail_ll_email, R.id.restaurant_detail_ll_phone);
        super.onBaseFree();
    }

    @Override
    public void onSingleClick(View v) {
        switch (v.getId()) {
            case R.id.restaurant_detail_ll_email:
                try {
                    Intent mail = new Intent(Intent.ACTION_SENDTO);
                    mail.setType("message/rfc822");
                    mail.setData(Uri.parse("mailto:" + restaurant.email));
                    startActivity(mail);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;
            case R.id.restaurant_detail_ll_phone:
                if (!requestAndroidPermissions(new String[]{Manifest.permission.CALL_PHONE}, DIAL_PERMISSION_REQUEST_CODE)) {
                    Intent dial = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + restaurant.phone));
                    startActivity(dial);
                }
                break;
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == DIAL_PERMISSION_REQUEST_CODE) {
            if (permissions[0].equals(Manifest.permission.CALL_PHONE) && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                Intent dial = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + restaurant.phone));
                startActivity(dial);
            }
        }
    }

    @Override
    public void onMapReady(GoogleMap map) {
        this.map = map;
        this.map.setMinZoomPreference(6.0f);
        this.map.setMaxZoomPreference(20.0f);
        if (map != null) {
            LatLng marker_position = new LatLng(restaurant.latitude, restaurant.longitude);
            LatLng focus_position = new LatLng(restaurant.latitude - 0.005, restaurant.longitude); // adjust focus camera because of the information dialog
            map.animateCamera(CameraUpdateFactory.newLatLngZoom(focus_position, 15.0f));
            MarkerOptions markerOptions = new MarkerOptions()
                    .draggable(false)
                    .position(marker_position)
                    .icon(BitmapDescriptorFactory.fromResource(R.mipmap.ic_app_logo));
            map.addMarker(markerOptions);
        }
        closeLoadingDialog();
    }

    @Override
    public void refreshViewDataLanguage() {
        restaurant_detail_tv_address_title.setText(R.string.restaurant_lb_address);
        restaurant_detail_tv_email_title.setText(R.string.restaurant_lb_email);
        restaurant_detail_tv_phone_title.setText(R.string.restaurant_lb_phone);
        restaurant_detail_tv_about_title.setText(R.string.other_lb_about_us);

        if (restaurant != null) {
            restaurant_detail_tv_about.setText(restaurant.getInformation());
            restaurant_detail_tv_address.setText(restaurant.getAddress());
        }
    }
}
