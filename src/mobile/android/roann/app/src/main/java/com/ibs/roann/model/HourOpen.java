package com.ibs.roann.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by tuanle on 1/12/17.
 */

public class HourOpen implements Serializable {

    @SerializedName("to")
    public String to;

    @SerializedName("from")
    public String from;

}
