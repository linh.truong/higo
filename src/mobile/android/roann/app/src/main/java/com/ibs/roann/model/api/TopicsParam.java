package com.ibs.roann.model.api;

import com.ibs.roann.feature.ToyosuBaseParam;

import org.json.JSONObject;

/**
 * Created by tuanle on 1/3/17.
 */

public class TopicsParam extends ToyosuBaseParam {

    private static final String PARAMS_NEXT_MAX_ID_TAG = "next_max_id";

    private String max_id;

    public TopicsParam(String max_id) {
        this.max_id = max_id;
    }

    @Override
    public byte[] makeRequestBody() {
        try {
            return new JSONObject().put(PARAMS_NEXT_MAX_ID_TAG, String.valueOf(max_id)).toString().getBytes();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return new byte[0];
    }
}

