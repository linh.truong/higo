package com.ibs.roann.model.api;

import com.ibs.roann.feature.ToyosuBaseParam;

import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by tuanle on 1/3/17.
 */

public class RegisterCustomerParam extends ToyosuBaseParam {
    private static final String PARAMS_DATA_TAG = "data";
    private static final String PARAMS_NAME_TAG = "name";
    private static final String PARAMS_EMAIL_TAG = "email";
    private static final String PARAMS_PHONE_TAG = "phone";
    private static final String PARAMS_GENDER_TAG = "sex";
    private static final String PARAMS_AGE_TAG = "age";
    private static final String PARAMS_ADDRESS_TAG = "address";
    private static final String PARAMS_GPS_TAG = "gps_data";
    private static final String PARAMS_RESTAURANT_TAG = "restaurant";
    private static final String PARAMS_PASSWORD_TAG = "login_password";
    private static final String PARAMS_AREA_TAG = "area";
    private static final String PARAMS_LAST_TRANSACTION_TAG = "last_transaction_date";
    private static final String PARAMS_BIRTHDAY_TAG = "birthday";

    private int restaurant_id;

    private String address;

    private String gps_data;

    private String name;

    private String email;

    private String phone;

    private int gender;

    private int age;

    private String password;

    private int area;

    private String last_transaction_date;

    private String birthday;

    public RegisterCustomerParam(int restaurant_id, String address, String gps_data, String name, String email, String phone, int gender, int age, String password, int area, String birthday) {
        this.restaurant_id = restaurant_id;
        this.address = address;
        this.gps_data = gps_data;
        this.name = name;
        this.email = email;
        this.phone = phone;
        this.gender = gender;
        this.age = age;
        this.password = password;
        this.area = area;
        Date date = new Date();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
        String stringDate = sdf.format(date);
        this.last_transaction_date = stringDate;
        this.birthday = birthday;
    }

    @Override
    public byte[] makeRequestBody() {
        try {
            return new JSONObject().put(PARAMS_DATA_TAG, new JSONObject()
                    .put(PARAMS_GPS_TAG, gps_data)
                    .put(PARAMS_ADDRESS_TAG, address)
                    .put(PARAMS_RESTAURANT_TAG, restaurant_id)
                    .put(PARAMS_NAME_TAG, name)
                    .put(PARAMS_EMAIL_TAG, email)
                    .put(PARAMS_PHONE_TAG, phone)
                    .put(PARAMS_PASSWORD_TAG, password)
                    .put(PARAMS_AREA_TAG, area)
                    .put(PARAMS_GENDER_TAG, gender)
                    .put(PARAMS_AGE_TAG, age)
                    .put(PARAMS_LAST_TRANSACTION_TAG, last_transaction_date)
                    .put(PARAMS_BIRTHDAY_TAG, birthday)).toString().getBytes();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return new byte[0];
    }
}

