package com.ibs.roann.model.api;

import com.ibs.roann.feature.ToyosuBaseResult;
import com.ibs.roann.model.MenuFood;

import java.util.ArrayList;

/**
 * Created by tuanle on 1/3/17.
 */

public class FoodByCategoryResult extends ToyosuBaseResult {

    private int total;

    private ArrayList<MenuFood> foods;

    public FoodByCategoryResult() {
        foods = new ArrayList<>();
        total = 0;
    }

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }

    public ArrayList<MenuFood> getFoods() {
        return foods;
    }
}
