package com.ibs.roann.parser;

import com.google.gson.Gson;
import com.ibs.roann.model.Restaurant;
import com.ibs.roann.model.api.RestaurantsResult;

import org.json.JSONArray;
import org.json.JSONObject;

import core.base.BaseResult;
import core.util.Constant;

/**
 * Created by tuanle on 1/20/17.
 */

public class RestaurantsParser extends ToyosuBaseParser {
    @Override
    public BaseResult parseData(String content) {
        RestaurantsResult result = new RestaurantsResult();

        try {
            JSONObject json = new JSONObject(content);
            result.setBackEndStatus(json.optInt(RESPONSE_ERROR_TAG));
            if (result.getBackEndStatus() == 0) {
                result.setTotal(json.optInt(RESPONSE_TOTAL_TAG));
                Gson gson = new Gson();
                JSONArray data = json.getJSONArray(RESPONSE_DATA_TAG);
                for (int i = 0; i < data.length(); ++i) {
                    result.getRestaurants().add(gson.fromJson(data.get(i).toString(), Restaurant.class));
                }
                result.setStatus(Constant.StatusCode.OK);
            } else {
                result.setMessage(json.optString(RESPONSE_MESSAGE_TAG));
            }
            return result;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
}
