package com.ibs.roann.feature.prize;

import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.annotation.StringRes;
import android.support.v4.content.res.ResourcesCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.ibs.roann.R;
import com.ibs.roann.feature.ToyosuApplication;
import com.ibs.roann.feature.ToyosuBaseFragment;
import com.ibs.roann.feature.ToyosuBaseResult;
import com.ibs.roann.feature.ToyosuMainActivity;
import com.ibs.roann.model.Prize;
import com.ibs.roann.model.User;
import com.ibs.roann.model.api.PrizesParam;
import com.ibs.roann.model.api.PrizesResult;
import com.ibs.roann.model.api.UpdateQRPointParam;
import com.ibs.roann.model.api.UpdateQRPointResult;
import com.ibs.roann.view.fetchable.MutableRecycleView;
import com.ibs.roann.view.fetchable.MutableSwipeLayout;
import com.ibs.roann.view.fetchable.RecycleInterface;

import java.util.ArrayList;

import butterknife.BindView;
import core.base.BaseResult;
import core.dialog.GeneralDialog;
import core.util.Constant;
import core.util.RequestTarget;
import icepick.State;

import static com.ibs.roann.feature.ToyosuConstant.RES_FAIL;
import static com.ibs.roann.feature.ToyosuConstant.RES_NOT_FOUND;
import static core.base.BaseFragmentContainer.TAG;

/**
 * Created by tuanle on 2/3/17.
 */

public class ToyosuPrizeScreen extends ToyosuBaseFragment implements MutableRecycleView.OnRefreshListener, MutableRecycleView.OnLoadMoreListener, RecycleInterface<Prize>, GeneralDialog.ConfirmListener {

    private static final int ADD_POINT_SUCCESS = 9205;

    private static final int ADD_POINT_ERROR = 9206;

    @State
    ArrayList<Prize> data;

    @State
    @StringRes
    int error_string_id = R.string.common_pull_to_reload;

    @BindView(R.id.prize_screen_tv_reload)
    TextView prize_screen_tv_reload;

    private MutableSwipeLayout prize_screen_sl_list;
    private MutableRecycleView prize_screen_rv_list;
    private ToyosuPrizeAdapter adapter;

    private User user;

    public static ToyosuPrizeScreen getInstance() {
        return new ToyosuPrizeScreen();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.toyosu_prize_screen, container, false);
    }

    @Override
    public void onBaseCreate() {
        super.onBaseCreate();
        user = ((ToyosuMainActivity) ToyosuApplication.getActiveActivity()).getLoggedUser();
        data = new ArrayList<>();
        adapter = new ToyosuPrizeAdapter(ToyosuApplication.getActiveActivity().getLayoutInflater(), data, this, getSingleTouch());
    }

    @Override
    public void onBindView() {
        super.onBindView();
        prize_screen_sl_list = (MutableSwipeLayout) findViewById(R.id.prize_screen_sl_list);
        prize_screen_rv_list = (MutableRecycleView) findViewById(R.id.prize_screen_rv_list);
        prize_screen_sl_list.setEnableRefreshProgress(true);
        prize_screen_rv_list.setColorScheme(ResourcesCompat.getColor(getResources(), R.color.primary, null));
        prize_screen_rv_list.setOnRefreshListener(this);
        prize_screen_rv_list.setOnLoadMoreListener(this);
        prize_screen_rv_list.setAdapter(adapter);
    }

    @Override
    public void onInitializeViewData() {
        super.onInitializeViewData();
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                prize_screen_rv_list.onRefresh();
            }
        }, 1000);
    }

    @Override
    public void reloadData() {

    }

    @Override
    public void refreshViewDataLanguage() {
        if (prize_screen_tv_reload.getVisibility() == View.VISIBLE)
            prize_screen_tv_reload.setText(error_string_id);
        if (adapter != null)
            adapter.notifyDataSetChanged();
    }

    @Override
    public void onRefresh() {
        requestPrizes(0);
    }

    @Override
    public boolean shouldOverrideLoadMore() {
        return true;
    }

    @Override
    public void onLoadMore() {
        if (data != null)
            requestPrizes(adapter.getRealItemCount());
    }

    @Override
    public boolean shouldOverrideRefresh() {
        return true;
    }

    private void requestPrizes(int offset) {
        makeRequest(getUniqueTag(), false, new PrizesParam(offset), this, RequestTarget.PRIZES);
    }

    @Override
    public void onResultSuccess(BaseResult result) {
        super.onResultSuccess(result);
        if (result instanceof PrizesResult) {
            Log.e("Prize", "onResultSuccess: " + ((PrizesResult) result).getTotal() );
            if (prize_screen_rv_list.isRefreshing()) {
                prize_screen_rv_list.setLimit(((PrizesResult) result).getTotal());
                if (data != null) {
                    data.clear();
                    Prize header = new Prize();
                    header.id = -2;
                    data.add(header); // header
                    data.addAll(((PrizesResult) result).getPrizes());
                    data.add(new Prize()); // footer
                }
                prize_screen_rv_list.onRefreshComplete();
            } else if (prize_screen_rv_list.isLoadingMore()) {
                if (data != null) {
                    for (int i = 0; i < data.size(); i++) {
                        if (data.get(i).id == -1)
                            data.remove(i);
                    }
                    data.addAll(((PrizesResult) result).getPrizes());
                    data.add(new Prize()); // footer
                }
                prize_screen_rv_list.onLoadMoreComplete();
            } else {
                adapter.notifyDataSetChanged();
            }

            if (adapter.getRealItemCount() <= 0) {
                prize_screen_tv_reload.setText(error_string_id = R.string.common_pull_to_reload);
                prize_screen_tv_reload.setVisibility(View.VISIBLE);
                prize_screen_rv_list.setVisibility(View.GONE);
            } else {
                prize_screen_tv_reload.setVisibility(View.GONE);
                prize_screen_rv_list.setVisibility(View.VISIBLE);
            }
        }

        if (result instanceof UpdateQRPointResult) {
            int total_point = ((UpdateQRPointResult) result).getTotal_point();
            int current_point = ((UpdateQRPointResult) result).getCurrent_point();
            int res_point = ((UpdateQRPointResult) result).getRes_point();
            user.current_point = current_point;
            user.total_point = total_point;
            setLoggedUser(user);
            adapter.notifyDataSetChanged();
            String message = getString(R.string.other_prize_got_point, String.valueOf(res_point));
            showAlertDialog(getActiveActivity(), ADD_POINT_SUCCESS, getGeneralDialogLayoutResource(),
                    R.mipmap.ic_app_logo,
                    getString(R.string.common_info),
                    message, getString(R.string.common_ok)
                    , null, this);
        }
    }

    @Override
    public void onResultFail(BaseResult result) {
        super.onResultFail(result);
        if (result instanceof PrizesResult) {
            if (prize_screen_rv_list.isRefreshing()) {
                prize_screen_rv_list.onRefreshComplete();
            } else if (prize_screen_rv_list.isLoadingMore()) {
                prize_screen_rv_list.onLoadMoreComplete();
            } else {
                adapter.notifyDataSetChanged();
            }
            if (adapter.getRealItemCount() <= 0) {
                prize_screen_tv_reload.setText(error_string_id = R.string.common_error_pull_to_reload);
                prize_screen_tv_reload.setVisibility(View.VISIBLE);
                prize_screen_rv_list.setVisibility(View.GONE);
            } else {
                prize_screen_tv_reload.setVisibility(View.GONE);
                prize_screen_rv_list.setVisibility(View.VISIBLE);
            }
        }
        if (result instanceof UpdateQRPointResult) {
            int code = ((ToyosuBaseResult) result).getBackEndStatus();
            String message = "";
            switch (code) {
                case RES_NOT_FOUND:
                    message = ToyosuApplication.getActiveActivity().getString(R.string.other_qr_code_not_exist);
                    break;
                case RES_FAIL:
                    message = ToyosuApplication.getActiveActivity().getString(R.string.other_qr_code_used_expired);
                    break;
            }
            showAlertDialog(getActiveActivity(), ADD_POINT_ERROR, getGeneralDialogLayoutResource(),
                    R.mipmap.ic_app_logo,
                    getString(R.string.common_info),
                    message, getString(R.string.common_ok)
                    , null, this);
        }
    }

    @Override
    public void onFail(RequestTarget target, String error, Constant.StatusCode code) {
        super.onFail(target, error, code);
        switch (target) {
            case PRIZES:
                if (prize_screen_rv_list.isRefreshing()) {
                    prize_screen_rv_list.onRefreshComplete();
                } else if (prize_screen_rv_list.isLoadingMore()) {
                    prize_screen_rv_list.onLoadMoreComplete();
                } else {
                    adapter.notifyDataSetChanged();
                }
                if (adapter.getRealItemCount() <= 0) {
                    prize_screen_tv_reload.setText(error_string_id = R.string.common_error_pull_to_reload);
                    prize_screen_tv_reload.setVisibility(View.VISIBLE);
                    prize_screen_rv_list.setVisibility(View.GONE);
                } else {
                    prize_screen_tv_reload.setVisibility(View.GONE);
                    prize_screen_rv_list.setVisibility(View.VISIBLE);
                }
                break;
            case UPDATE_QR_POINT:
                String message = getString(R.string.other_qr_code_used_expired);
                showAlertDialog(getActiveActivity(), ADD_POINT_ERROR, getGeneralDialogLayoutResource(),
                        R.mipmap.ic_app_logo,
                        getString(R.string.common_info),
                        message, getString(R.string.common_ok)
                        , null, this);
                break;
        }
    }

    @Override
    public void onItemClick(View view, Prize item, int position, int type) {
        if (item != null) {
            if (item.id == -1 || item.id == -2)
                return;
            addFragment(getMainContainerId(), ToyosuPrizeDetailScreen.getInstance(item));
        }
    }

    public void onQRCodeReceived(String content) {
        // do something here
        Log.e(TAG, "onQRCodeReceived: " + content );
        makeRequest(getUniqueTag(), true, new UpdateQRPointParam(user.id, content, user.auth_token), this, RequestTarget.UPDATE_QR_POINT);
    }

    @Override
    public void onConfirmed(int id, Object onWhat) {
        if (id == ADD_POINT_SUCCESS) {
            adapter.setUser();
        }
    }

    @Override
    public void onBaseResume() {
        super.onBaseResume();
        Log.e(TAG, "onBaseResume");
        adapter.setUser();
    }
}
