package com.ibs.roann.model.api;

import com.ibs.roann.feature.ToyosuBaseResult;
import com.ibs.roann.model.HourOpen;

import java.util.ArrayList;

/**
 * Created by tuanle on 1/3/17.
 */

public class HourOpenResult extends ToyosuBaseResult {

    private ArrayList<HourOpen> hourOpens;

    public HourOpenResult() {
        hourOpens = new ArrayList<>();
    }

    public ArrayList<HourOpen> getHourOpens() {
        return hourOpens;
    }

    public void setHourOpens(ArrayList<HourOpen> hourOpens) {
        this.hourOpens = hourOpens;
    }

}
