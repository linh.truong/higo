package com.ibs.roann.model.api;

import com.ibs.roann.feature.ToyosuBaseResult;
import com.ibs.roann.model.RelatedItem;

import java.util.ArrayList;

/**
 * Created by tuanle on 2/2/17.
 */

public class RelatedItemsResult extends ToyosuBaseResult {

    private ArrayList<RelatedItem> items;

    public RelatedItemsResult() {
        items = new ArrayList<>();
    }

    public ArrayList<RelatedItem> getItems() {
        return items;
    }
}
