package com.ibs.roann.model.api;

import com.ibs.roann.feature.ToyosuBaseParam;

import org.json.JSONObject;

/**
 * Created by tuanle on 1/3/17.
 */

public class FoodByCategoryParam extends ToyosuBaseParam {

    private static final String PARAMS_CATEGORY_TAG = "id";

    private int offset;

    private int category_id;

    public FoodByCategoryParam(int offset, int category_id) {
        this.offset = offset;
        this.category_id = category_id;
    }

    @Override
    public byte[] makeRequestBody() {
        try {
            return new JSONObject().put(PARAMS_CATEGORY_TAG, category_id)
                    .put(PARAM_LIMIT_TAG, String.valueOf(LIST_LIMIT))
                    .put(PARAM_OFFSET_TAG, String.valueOf(offset)).toString().getBytes();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return new byte[0];
    }
}

