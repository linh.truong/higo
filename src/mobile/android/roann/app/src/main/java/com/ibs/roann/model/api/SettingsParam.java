package com.ibs.roann.model.api;

import com.ibs.roann.feature.ToyosuBaseParam;

import org.json.JSONObject;

/**
 * Created by tuanle on 1/3/17.
 */

public class SettingsParam extends ToyosuBaseParam {

    private int offset;

    public SettingsParam(int offset) {
        this.offset = offset;
    }

    @Override
    public byte[] makeRequestBody() {
        try {
            return new JSONObject().put(PARAM_LIMIT_TAG, String.valueOf(100)).put(PARAM_OFFSET_TAG, String.valueOf(offset)).toString().getBytes();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return new byte[0];
    }
}

