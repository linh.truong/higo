package com.ibs.roann.feature.prize;

import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.ibs.roann.R;
import com.ibs.roann.feature.ToyosuApplication;
import com.ibs.roann.feature.ToyosuBaseFragment;
import com.ibs.roann.feature.ToyosuConstant;
import com.ibs.roann.feature.ToyosuMainActivity;
import com.ibs.roann.feature.ToyosuUtils;
import com.ibs.roann.feature.authentication.ToyosuLoginScreen;
import com.ibs.roann.model.Delivery;
import com.ibs.roann.model.Prize;
import com.ibs.roann.model.User;
import com.ibs.roann.model.api.PrizeDeliveryParam;
import com.ibs.roann.model.api.PrizeDeliveryResult;
import com.ibs.roann.view.ToyosuImageDialog;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import butterknife.BindView;
import core.base.BaseResult;
import core.data.UserDataManager;
import core.dialog.GeneralDialog;
import core.util.DataKey;
import core.util.RequestTarget;
import core.util.Utils;
import icepick.State;

import static com.ibs.roann.feature.ToyosuConstant.INS_EMPTY_STOCK;
import static com.ibs.roann.feature.ToyosuConstant.INS_LACK_POINT;

/**
 * Created by tuanle on 2/3/17.
 */

public class ToyosuPrizeDetailScreen extends ToyosuBaseFragment implements GeneralDialog.DecisionListener, GeneralDialog.ConfirmListener {

    private static final int LOGIN_DIALOG = 9104;

    private static final int USE_POINT_SUCCESS = 9105;

    private static final int USE_POINT_ERROR = 9106;

    private static final String PRIZE = "prize";

    @BindView(R.id.prize_detail_img_coupon)
    ImageView prize_detail_img_coupon;

    @BindView(R.id.prize_detail_tv_title)
    TextView prize_detail_tv_title;

    @BindView(R.id.prize_detail_tv_content)
    TextView prize_detail_tv_content;

    @BindView(R.id.prize_detail_tv_condition_title)
    TextView prize_detail_tv_condition_title;

    @BindView(R.id.prize_item_tv_date_start)
    TextView prize_item_tv_date_start;

    @BindView(R.id.prize_item_tv_date_end)
    TextView prize_item_tv_date_end;

    @BindView(R.id.prize_item_tv_start)
    TextView prize_item_tv_start;

    @BindView(R.id.prize_item_tv_end)
    TextView prize_item_tv_end;

    @BindView(R.id.prize_detail_tv_use)
    TextView prize_detail_tv_use;

    @BindView(R.id.prize_detail_tv_link)
    TextView prize_detail_tv_link;

    @State
    Prize prize;
    User user;
    Delivery delivery;

    private ToyosuImageDialog dialog;

    public static ToyosuPrizeDetailScreen getInstance(Prize prize) {
        ToyosuPrizeDetailScreen screen = new ToyosuPrizeDetailScreen();
        Bundle bundle = new Bundle();
        bundle.putSerializable(PRIZE, prize);
        screen.setArguments(bundle);
        return screen;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.toyosu_prize_detail_screen, container, false);
    }

    @Override
    public void onBaseCreate() {
        super.onBaseCreate();
        Bundle bundle = getArguments();
        if (bundle != null) {
            prize = (Prize) bundle.getSerializable(PRIZE);
        }
        user = ((ToyosuMainActivity) ToyosuApplication.getActiveActivity()).getLoggedUser();
        if (delivery == null)
            delivery = new Delivery();
    }

    @Override
    public void onInitializeViewData() {
        super.onInitializeViewData();
        ToyosuApplication.getImageLoader().displayImage(prize.image_path, prize_detail_img_coupon);
        refreshViewDataLanguage();
        registerSingleAction(prize_detail_img_coupon, prize_detail_tv_use, prize_detail_tv_link);
        if (user != null) {
            delivery.name = user.name;
            delivery.email = user.email;
            delivery.phone = user.phone;
            delivery.delivery_type = prize.delivery_type;
            delivery.address = "";
        }
    }

    @Override
    public void reloadData() {

    }

    @Override
    public void refreshViewDataLanguage() {
        prize_detail_tv_title.setText(prize.getTitle());
        prize_detail_tv_content.setText(prize.getContent());
        prize_detail_tv_condition_title.setText(String.valueOf(prize.getCondition()));
        prize_item_tv_date_start.setText(ToyosuApplication.getActiveActivity().getString(R.string.prize_start_date, ToyosuUtils.formatDate(ToyosuUtils.formatDateString(prize.from_date), ToyosuConstant.DATE_FORMAT_VERY_SHORT)));
        prize_item_tv_date_end.setText(ToyosuApplication.getActiveActivity().getString(R.string.other_prize_lb_to_date, ToyosuUtils.formatDate(ToyosuUtils.formatDateString(prize.to_date), ToyosuConstant.DATE_FORMAT_VERY_SHORT)));
        prize_item_tv_start.setText(ToyosuApplication.getActiveActivity().getString(R.string.other_prize_lb_start_time, prize.start_time));
        prize_item_tv_end.setText(ToyosuApplication.getActiveActivity().getString(R.string.other_prize_lb_end_time, prize.end_time));
        if (prize.delivery_type == 2){
//            prize_detail_tv_use.setText(ToyosuApplication.getActiveActivity().getString(R.string.prize_delivery_lb_send_delivery));
            prize_detail_tv_use.setText(ToyosuApplication.getActiveActivity().getString(R.string.other_btn_use_prize));
        }else{
            prize_detail_tv_use.setText(ToyosuApplication.getActiveActivity().getString(R.string.other_btn_use_prize));
        }

        SimpleDateFormat sdf = new SimpleDateFormat(ToyosuConstant.DATE_FORMAT_REQUEST_SIMPLYBOOK);
        Calendar day = Calendar.getInstance();
        try {
            Date date = sdf.parse(prize.from_date);
            day.setTime(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        Calendar today = Calendar.getInstance();
        long diff = today.getTimeInMillis() - day.getTimeInMillis();

        if (diff >= 0){
            prize_detail_tv_use.setEnabled(true);
            Drawable drawable = getDrawableResource(R.drawable.prize_use_enable_bg);
            prize_detail_tv_use.setBackground(drawable);
        }

    }

    @SuppressWarnings("deprecation")
    public Drawable getDrawableResource(int id){
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP){
            return ToyosuApplication.getContext().getDrawable(id);
        }
        else{
            return ToyosuApplication.getContext().getResources().getDrawable(id);
        }
    }

    @Override
    public void onSingleClick(View v) {
        switch (v.getId()) {
            case R.id.prize_detail_img_coupon:
                dialog = new ToyosuImageDialog(getActiveActivity(), prize.image_path);
                dialog.show();
                break;
            case R.id.prize_detail_tv_link:
                String url = prize.link_address;
                if(Utils.isEmpty(url)) {
                    url = "http://higo.ibsv.vn/shop";
                    Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
                    startActivity(intent);
                }else{
                    if(!url.contains("http://"))
                        url = "http://" + url;
                    Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
                    startActivity(intent);
                }
                break;
            case R.id.prize_detail_tv_use:

                if (UserDataManager.getInstance().isEnabled(DataKey.LOGGED)) {
                    if (prize.delivery_type != 2){
                        addFragment(getMainContainerId(), ToyosuPrizeDelivery.getInstance(prize));
                    }else{
                    //    makeRequest(getUniqueTag(), true, new UsePrizeParam(user.id, prize.id, user.auth_token), this, RequestTarget.USE_PRIZE);
                        makeRequest(getUniqueTag(), true, new PrizeDeliveryParam(user.id, prize.id, user.auth_token, delivery), this, RequestTarget.PRIZE_DELIVERY);
                    }
                } else {
                    showDecisionDialog(getActiveActivity(), LOGIN_DIALOG,
                            getGeneralDialogLayoutResource(),
                            R.mipmap.ic_app_logo,
                            getString(R.string.common_info),
                            getString(R.string.other_prize_msg_login_required),
                            getString(R.string.common_yes),
                            getString(R.string.common_no),
                            null,
                            null, this);
                }

                break;
        }
    }

    @Override
    public void onResultSuccess(BaseResult result) {
        super.onResultSuccess(result);
        if (result instanceof PrizeDeliveryResult) {
            int point = ((PrizeDeliveryResult) result).getUsePoint().remain_point;
            user.current_point = point;
            setLoggedUser(user);
            String message = getString(R.string.other_prize_lb_used);
            showAlertDialog(getActiveActivity(), USE_POINT_SUCCESS, getGeneralDialogLayoutResource(),
                    R.mipmap.ic_app_logo,
                    getString(R.string.common_info),
                    message, getString(R.string.common_ok)
                    , null, this);
        }
    }

    @Override
    public void onResultFail(BaseResult result) {
        super.onResultFail(result);
        if (result instanceof PrizeDeliveryResult) {
            int code = ((PrizeDeliveryResult) result).getBackEndStatus();
            String message = "";
            switch (code) {
                case INS_LACK_POINT:
                    message = ToyosuApplication.getActiveActivity().getString(R.string.customer_current_point_not_enough);
                    break;
                case INS_EMPTY_STOCK:
                    message = ToyosuApplication.getActiveActivity().getString(R.string.other_prize_msg_not_found);
                    break;
            }
            showAlertDialog(getActiveActivity(), USE_POINT_ERROR, getGeneralDialogLayoutResource(),
                    R.mipmap.ic_app_logo,
                    getString(R.string.common_info),
                    message, getString(R.string.common_ok)
                    , null, this);
        }
    }

    @Override
    public void onAgreed(int id, Object onWhat) {
        if (id == LOGIN_DIALOG) {
            addFragment(getMainContainerId(), ToyosuLoginScreen.getInstance());
        }
    }

    @Override
    public void onDisAgreed(int id, Object onWhat) {

    }

    @Override
    public void onNeutral(int id, Object onWhat) {

    }

    @Override
    public void onConfirmed(int id, Object onWhat) {
        if (id == USE_POINT_SUCCESS) {
            finish();
        }else if (id == USE_POINT_ERROR){
            finish();
        }
    }

    @Override
    public void onBaseFree() {
        super.onBaseFree();
        unregisterSingleAction(prize_detail_img_coupon, prize_detail_tv_use, prize_detail_tv_link);
    }

}
