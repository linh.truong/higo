package com.ibs.roann.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class AddPoint implements Serializable {

    @SerializedName("total_point")
    public int total_point;

    @SerializedName("current_point")
    public int current_point;

    @SerializedName("res_point")
    public int res_point;

}
