package com.ibs.roann.model.api;

import com.ibs.roann.feature.ToyosuBaseParam;

import org.json.JSONObject;

/**
 * Created by tuanle on 1/3/17.
 */

public class UpdateQRPointParam extends ToyosuBaseParam {

    private int customerId;
    private String qr_point;
    private String auth_token;

    public UpdateQRPointParam(int customerId, String qr_point, String auth_token) {
        this.customerId = customerId;
        this.qr_point = qr_point;
        this.auth_token = auth_token;
    }

    @Override
    public byte[] makeRequestBody() {
        try {
            return new JSONObject().put(PARAM_CUSTOMER_ID_TAG, String.valueOf(customerId)).put(PARAM_QR_CODE_TAG, qr_point).put(PARAM_AUTH_TOKEN_TAG, auth_token).toString().getBytes();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return new byte[0];
    }
}

