package com.ibs.roann.feature.news;

import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.widget.ImageView;
import android.widget.TextView;

import com.ibs.roann.R;
import com.ibs.roann.feature.ToyosuApplication;
import com.ibs.roann.feature.ToyosuUtils;
import com.ibs.roann.model.Instagram;
import com.ibs.roann.view.fetchable.RecycleAdapter;
import com.ibs.roann.view.fetchable.RecycleInterface;

import java.util.ArrayList;

import core.util.SingleClick;
import core.util.SingleTouch;

/**
 *
 */

public class ToyosuNewsInstagramAdapter extends RecycleAdapter<Instagram> {

    private SingleClick single_click;

    public ToyosuNewsInstagramAdapter(LayoutInflater inflater, ArrayList<Instagram> items, RecycleInterface<Instagram> listener, SingleTouch singleTouch, SingleClick singleClick) {
        super(inflater, items, listener, singleTouch);
        this.single_click = singleClick;
    }

    @Override
    protected int getHeaderLayoutResource() {
        return 0;
    }

    @Override
    protected int getItemLayoutResource() {
        return R.layout.toyosu_topic_item;
    }

    @Override
    protected int getFooterLayoutResource() {
        return R.layout.toyosu_item_footer;
    }

    @Override
    protected int getInidcatorLayoutResource() {
        return 0;
    }

    @Override
    protected void bindFooterView(FooterViewHolder<Instagram> holder, Instagram data, int position) {

    }

    @Override
    protected void bindIndicatorView(IndicatorViewHolder<Instagram> holder, Instagram data, int position) {

    }

    @Override
    protected void bindHeaderView(HeaderViewHolder<Instagram> holder, Instagram data, int position) {

    }

    @Override
    protected void bindItemView(ItemViewHolder<Instagram> holder, Instagram data, int position) {
//        ToyosuApplication.getImageLoader().displayImage(data.restaurant_avatar_url, (ImageView) holder.findViewById(R.id.topic_item_img_restaurant_avatar));
        ToyosuApplication.getImageLoader().displayImage(data.imageInstagram.imageStandard.url, (ImageView) holder.findViewById(R.id.topic_item_img_restaurant_special));
        if (data.caption != null){
            TextView caption = ((TextView) holder.findViewById(R.id.topic_item_tv_description));
            caption.setText(data.caption.text);
            int color = ContextCompat.getColor(ToyosuApplication.getActiveActivity(), R.color.primary);
            ToyosuUtils.setTags(caption, data.caption.text, color );
        }
//        ((TextView) holder.findViewById(R.id.topic_item_tv_hash_tags)).setText(data.hash_tags);
        ((TextView) holder.findViewById(R.id.topic_item_tv_restaurant_name)).setText(ToyosuApplication.getActiveActivity().getString(R.string.app_name));
        ((TextView) holder.findViewById(R.id.topic_item_tv_like)).setText(ToyosuApplication.getActiveActivity().getString(R.string.home_lb_num_like, String.valueOf(data.likes.count)));
        ((TextView) holder.findViewById(R.id.topic_item_tv_comment)).setText(ToyosuApplication.getActiveActivity().getString(R.string.home_lb_num_comment, String.valueOf(data.comments.count)));
        ((TextView) holder.findViewById(R.id.topic_item_tv_created)).setText(ToyosuApplication.getActiveActivity().getString(R.string.home_lb_open_date, String.valueOf((ToyosuUtils.formatDateString(Long.parseLong(data.created_time))))));
        ImageView topic_item_img_onigiri = (ImageView) holder.findViewById(R.id.topic_item_img_onigiri);
        ImageView topic_item_img_share = (ImageView) holder.findViewById(R.id.topic_item_img_share);
        topic_item_img_onigiri.setTag(data);
        topic_item_img_share.setTag(data);
        topic_item_img_onigiri.setOnClickListener(single_click);
        topic_item_img_share.setOnClickListener(single_click);

    }

    @Override
    public int getItemViewType(int position) {
        if (items.get(position).post_id.equals("-1"))
            return FOOTER_TYPE;
        return ITEM_TYPE;
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    @Override
    public int getRealItemCount() {
        int footer_count = 0;
        for (Instagram topic : items)
            if (topic.post_id.equals("-1"))
                footer_count++;
        return getItemCount() - footer_count;
    }
}
