package com.ibs.roann.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Point implements Serializable {

    @SerializedName("point")
    public int point;

}
