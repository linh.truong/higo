package com.ibs.roann.feature.authentication;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Patterns;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.ibs.roann.R;
import com.ibs.roann.feature.ToyosuApplication;
import com.ibs.roann.feature.ToyosuBaseFragment;
import com.ibs.roann.feature.ToyosuConstant;
import com.ibs.roann.feature.ToyosuUtils;
import com.ibs.roann.model.Area;
import com.ibs.roann.model.User;
import com.ibs.roann.model.UserUpdate;
import com.ibs.roann.model.api.AreasParam;
import com.ibs.roann.model.api.AreasResult;
import com.ibs.roann.model.api.UpdateCustomerParam;
import com.ibs.roann.model.api.UpdateCustomerResult;
import com.ibs.roann.model.api.UpdatePasswordParam;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import butterknife.BindView;
import core.base.BaseResult;
import core.connection.QueueServiceRequester;
import core.connection.queue.QueueElement;
import core.data.UserDataManager;
import core.dialog.GeneralDialog;
import core.util.Constant;
import core.util.DataKey;
import core.util.RequestTarget;
import core.util.Utils;
import icepick.State;

import static com.ibs.roann.feature.ToyosuConstant.MAX_AGE;
import static com.ibs.roann.feature.ToyosuConstant.MIN_AGE;

/**
 * Created by tuanle on 1/19/17.
 */

public class ToyosuUserUpdateScreen extends ToyosuBaseFragment implements CompoundButton.OnCheckedChangeListener, AdapterView.OnItemSelectedListener, QueueServiceRequester.QueueServiceListener, GeneralDialog.ConfirmListener, ToyosuUpdatePasswordDialog.OnUpdatePasswordListener, DatePickerDialog.OnDateSetListener {

    private static final int UPDATED_SUCCESS_DIALOG = 9007;
    private static final String DATE_PICKER_DIALOG_TAG = DatePickerDialog.class.getSimpleName();

    @BindView(R.id.user_update_img_gender_unknown)
    View user_update_img_gender_unknown;

    @BindView(R.id.user_update_tv_gender_unknown)
    TextView user_update_tv_gender_unknown;

    @BindView(R.id.user_update_img_gender_male)
    View user_update_img_gender_male;

    @BindView(R.id.user_update_tv_gender_male)
    TextView user_update_tv_gender_male;

    @BindView(R.id.user_update_img_gender_female)
    View user_update_img_gender_female;

    @BindView(R.id.user_update_tv_gender_female)
    TextView user_update_tv_gender_female;

    @BindView(R.id.user_update_ed_name)
    EditText user_update_ed_name;

    @BindView(R.id.user_update_tv_name_title)
    TextView user_update_tv_name_title;

    @BindView(R.id.user_update_tv_name_error)
    TextView user_update_tv_name_error;

    @BindView(R.id.user_update_tv_email_title)
    TextView user_update_tv_email_title;

    @BindView(R.id.user_update_tv_email_error)
    TextView user_update_tv_email_error;

    @BindView(R.id.user_update_ed_email)
    EditText user_update_ed_email;

    @BindView(R.id.user_update_tv_phone_title)
    TextView user_update_tv_phone_title;

    @BindView(R.id.user_update_tv_phone_error)
    TextView user_update_tv_phone_error;

    @BindView(R.id.user_update_ed_phone)
    EditText user_update_ed_phone;

    @BindView(R.id.user_update_tv_password_title)
    TextView user_update_tv_password_title;

    @BindView(R.id.user_update_tv_birthday_title)
    TextView user_update_tv_birthday_title;

    @BindView(R.id.user_update_tv_birthday_error)
    TextView user_update_tv_birthday_error;

    @BindView(R.id.user_update_tv_submit)
    TextView user_update_tv_submit;

    @BindView(R.id.user_update_img_submit)
    ImageView user_update_img_submit;

    Spinner user_update_sp_area;

    @BindView(R.id.user_update_ll_birthday)
    LinearLayout user_update_ll_birthday;
    @BindView(R.id.user_update_tv_birthday)
    TextView user_update_tv_birthday;

    @BindView(R.id.user_update_ed_address)
    EditText user_update_ed_address;

    @State
    User user;
    long birthDay = -1;

    private SimpleDateFormat request_format;
    private SimpleDateFormat format;

    private DatePickerDialog date_dialog;

    public static ToyosuUserUpdateScreen getInstance() {
        return new ToyosuUserUpdateScreen();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.toyosu_user_update_screen, container, false);
    }

    @Override
    public void onBaseCreate() {
        super.onBaseCreate();
        request_format = new SimpleDateFormat(ToyosuConstant.DATE_FORMAT_RESPONSE);
        format = new SimpleDateFormat(ToyosuConstant.DATE_FORMAT_SHORT);
        if (UserDataManager.getInstance().isEnabled(DataKey.LOGGED)) {
            if (user == null)
                user = getLoggedUser();
                if(user.birthday != null) {
                    Date day = ToyosuUtils.formatDateString(user.birthday);
                    Calendar c = Calendar.getInstance();
                    c.setTime(day);
                    birthDay = c.getTimeInMillis();
                }else{
                    if (user.age > 0 ){
                        Calendar calendar = Calendar.getInstance();
                        calendar.set(Calendar.YEAR, calendar.get(Calendar.YEAR) - user.age);
                        calendar.set(Calendar.MONTH, Calendar.JANUARY);
                        calendar.set(Calendar.DAY_OF_MONTH, 1);
                        calendar.set(Calendar.HOUR_OF_DAY, 0);
                        calendar.set(Calendar.MINUTE, 0);
                        calendar.set(Calendar.SECOND, 0);
                        calendar.set(Calendar.MILLISECOND, 0);
                        birthDay = calendar.getTimeInMillis();
                    }
                }
        } else {
            // user should not be here
            return;
        }
    }

    @Override
    public void refreshViewDataLanguage() {

    }

    @Override
    public void onBindView() {
        super.onBindView();
        user_update_sp_area = (Spinner) findViewById(R.id.user_update_sp_area);
    }

    @Override
    public void reloadData() {

    }

    @Override
    public void onInitializeViewData() {
        super.onInitializeViewData();
        if (user != null) {
            refreshDatePicker();
            user_update_ed_name.setText(user.name);
            user_update_ed_email.setText(user.email);
            user_update_ed_phone.setText(user.phone);
            user_update_ed_address.setText(user.address);

            user_update_ed_email.setEnabled(false);
            switch (user.sex) {
                case 0:
                    setGenderMaleSelected();
                    break;
                case 1:
                    setGenderFemaleSelected();
                    break;
                case 2:
                    setGenderUnknownSelected();
                    break;
            }
            registerSingleAction(user_update_tv_password_title,
                    user_update_img_gender_female,
                    user_update_tv_gender_female,
                    user_update_img_gender_unknown,
                    user_update_tv_gender_unknown,
                    user_update_img_gender_male,
                    user_update_tv_gender_male,
                    user_update_ll_birthday,
                    user_update_tv_submit);
            user_update_sp_area.setOnItemSelectedListener(this);
            QueueServiceRequester.registerListener(this);
            makeQueueRequest(getUniqueTag(), QueueElement.Type.RETRY, new AreasParam(), RequestTarget.AREAS);
        } else {
            // user should not be here
            finish();
        }
        user_update_tv_password_title.setVisibility(View.GONE);
    }

    @Override
    public void onBaseFree() {
        unregisterSingleAction(user_update_tv_password_title,
                user_update_img_gender_female,
                user_update_tv_gender_female,
                user_update_img_gender_unknown,
                user_update_tv_gender_unknown,
                user_update_img_gender_male,
                user_update_tv_gender_male,
                user_update_ll_birthday,
                user_update_tv_submit);
        super.onBaseFree();
    }

    private void setGenderMaleSelected() {
        user_update_img_gender_female.setSelected(false);
        user_update_tv_gender_female.setSelected(false);
        user_update_img_gender_unknown.setSelected(false);
        user_update_tv_gender_unknown.setSelected(false);
        user_update_img_gender_male.setSelected(true);
        user_update_tv_gender_male.setSelected(true);
        user.sex = 0;
    }

    private void setGenderFemaleSelected() {
        user_update_img_gender_unknown.setSelected(false);
        user_update_tv_gender_unknown.setSelected(false);
        user_update_img_gender_male.setSelected(false);
        user_update_tv_gender_male.setSelected(false);
        user_update_img_gender_female.setSelected(true);
        user_update_tv_gender_female.setSelected(true);
        user.sex = 1;
    }

    private void setGenderUnknownSelected() {
        user_update_img_gender_male.setSelected(false);
        user_update_tv_gender_male.setSelected(false);
        user_update_img_gender_female.setSelected(false);
        user_update_tv_gender_female.setSelected(false);
        user_update_img_gender_unknown.setSelected(true);
        user_update_tv_gender_unknown.setSelected(true);
        user.sex = 2;
    }

    private void showInputBirthdayError() {
        user_update_tv_birthday_error.setVisibility(View.VISIBLE);
        user_update_tv_birthday_title.setVisibility(View.GONE);
    }

    private void hideInputBirthdayError() {
        user_update_tv_birthday_error.setVisibility(View.GONE);
        user_update_tv_birthday_title.setVisibility(View.VISIBLE);
    }

    private void showInputNameError() {
        user_update_tv_name_error.setVisibility(View.VISIBLE);
        user_update_tv_name_title.setVisibility(View.GONE);
    }

    private void hideInputNameError() {
        user_update_tv_name_error.setVisibility(View.GONE);
        user_update_tv_name_title.setVisibility(View.VISIBLE);
    }

    private void showInputPhoneError() {
        user_update_tv_phone_error.setVisibility(View.VISIBLE);
        user_update_tv_phone_title.setVisibility(View.GONE);
    }

    private void hideInputPhoneError() {
        user_update_tv_phone_error.setVisibility(View.GONE);
        user_update_tv_phone_title.setVisibility(View.VISIBLE);
    }

    private void showInputEmailError(String error) {
        user_update_tv_email_error.setVisibility(View.VISIBLE);
        user_update_tv_email_title.setVisibility(View.GONE);
        user_update_tv_email_error.setText(error);
    }

    private void hideInputEmailError() {
        user_update_tv_email_error.setVisibility(View.GONE);
        user_update_tv_email_title.setVisibility(View.VISIBLE);
    }

    private boolean validated() {
        boolean result = true;

        String name = user_update_ed_name.getText().toString().trim();
        if (Utils.isEmpty(name) || !name.contains(" ")) {
            showInputNameError();
            result = false;
        } else {
            hideInputNameError();
        }
        if (Utils.isEmpty(user_update_ed_email.getText().toString().trim())) {
            showInputEmailError(getString(R.string.customer_lb_email_required));
            result = false;
        } else {
            if (!Patterns.EMAIL_ADDRESS.matcher(user_update_ed_email.getText().toString().trim()).matches()) {
                showInputEmailError(getString(R.string.customer_lb_email_invalid));
                result = false;
            } else
                hideInputEmailError();
        }

        if (Utils.isEmpty(user_update_ed_phone.getText().toString().trim())) {
            showInputPhoneError();
            result = false;
        } else
            hideInputPhoneError();

        if (birthDay == -1) {
            showInputBirthdayError();
            result = false;
        } else
            hideInputBirthdayError();

        return result;
    }

    @Override
    public void onSingleClick(View v) {
        switch (v.getId()) {
            case R.id.user_update_tv_password_title:
                new ToyosuUpdatePasswordDialog(getActiveActivity(), this).show();
                break;
            case R.id.user_update_img_gender_male:
            case R.id.user_update_tv_gender_male:
                setGenderMaleSelected();
                break;
            case R.id.user_update_img_gender_female:
            case R.id.user_update_tv_gender_female:
                setGenderFemaleSelected();
                break;
            case R.id.user_update_img_gender_unknown:
            case R.id.user_update_tv_gender_unknown:
                setGenderUnknownSelected();
                break;
            case R.id.user_update_ll_birthday:
                if (date_dialog != null)
                    date_dialog.show(getActivity().getFragmentManager(), DATE_PICKER_DIALOG_TAG);
                break;
            case R.id.user_update_tv_submit:
                if (validated()) {
                    requestUpdateCustomer();
                }
                break;
        }
    }

    private void requestUpdateCustomer() {
        makeRequest(getUniqueTag(), true, new UpdateCustomerParam(
                user.id,
                UserDataManager.getInstance().getInt(DataKey.RESTAURANT_ID),
                user_update_ed_address.getText().toString().trim(), // address
                ToyosuConstant.BLANK, // gps
                user_update_ed_name.getText().toString().trim(),
                user_update_ed_email.getText().toString().trim(),
                user_update_ed_phone.getText().toString().trim(),
                user.sex,
                user.age
                , user.area_id,
                user.auth_token,
                request_format.format(new Date(birthDay))), this, RequestTarget.UPDATE_CUSTOMER);
    }

    private void refreshDatePicker() {
        Calendar max = Calendar.getInstance();
        max.set(Calendar.YEAR, max.get(Calendar.YEAR) - MIN_AGE);
        max.set(Calendar.MONTH, Calendar.DECEMBER);
        max.set(Calendar.DAY_OF_MONTH, 31);
        max.set(Calendar.HOUR_OF_DAY, 0);
        max.set(Calendar.MINUTE, 0);
        max.set(Calendar.SECOND, 0);
        max.set(Calendar.MILLISECOND, 0);

        Calendar min = Calendar.getInstance();
        min.set(Calendar.YEAR, max.get(Calendar.YEAR) - MAX_AGE);
        min.set(Calendar.MONTH, Calendar.JANUARY);
        min.set(Calendar.DAY_OF_MONTH, 1);
        min.set(Calendar.HOUR_OF_DAY, 0);
        min.set(Calendar.MINUTE, 0);
        min.set(Calendar.SECOND, 0);
        min.set(Calendar.MILLISECOND, 0);


        date_dialog = DatePickerDialog.newInstance(this, max.get(Calendar.YEAR), max.get(Calendar.MONTH), max.get(Calendar.DAY_OF_MONTH));
        date_dialog.setMaxDate(max);
        date_dialog.setMinDate(min);
        date_dialog.initialize(this, max.get(Calendar.YEAR), max.get(Calendar.MONTH), max.get(Calendar.DAY_OF_MONTH));

        if (birthDay <= 0) {
            Calendar selected_date = max;
            if (birthDay == -1 || birthDay != selected_date.getTimeInMillis()){
                birthDay = selected_date.getTimeInMillis();
            }
        }
        Calendar selected = Calendar.getInstance();
        selected.setTimeInMillis(birthDay);
        date_dialog.initialize(this, selected.get(Calendar.YEAR), selected.get(Calendar.MONTH), selected.get(Calendar.DAY_OF_MONTH));
        user_update_tv_birthday.setText(format.format(new Date(birthDay)));
    }

    @Override
    public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {
        Calendar selected = Calendar.getInstance();
        selected.set(Calendar.YEAR, year);
        selected.set(Calendar.MONTH, monthOfYear);
        selected.set(Calendar.DAY_OF_MONTH, dayOfMonth);
        selected.set(Calendar.HOUR_OF_DAY, 0);
        selected.set(Calendar.MINUTE, 0);
        selected.set(Calendar.SECOND, 0);
        selected.set(Calendar.MILLISECOND, 0);
        if (birthDay == -1 || birthDay != selected.getTimeInMillis()){
            birthDay = selected.getTimeInMillis();
            Calendar now = Calendar.getInstance();
            user.age = now.get(Calendar.YEAR) - selected.get(Calendar.YEAR);
        }
        user_update_tv_birthday.setText(format.format(new Date(birthDay)));

    }

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        user_update_img_submit.setEnabled(isChecked);
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        Object item = parent.getItemAtPosition(position);
        if (item instanceof Area)
            user.area_id = ((Area) item).id;

    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    @Override
    public void onStartQueue(QueueElement element) {
        showLoadingDialog(getActiveActivity(), getLoadingDialogLayoutResource(), ToyosuApplication.getContext().getString(R.string.common_loading));
    }

    @Override
    public void onEndQueue() {

    }

    @Override
    public void onFinishQueue() {
        closeLoadingDialog();
    }

    @Override
    public void onBlockQueue(QueueElement element) {

    }

    @Override
    public void onStopQueue(ArrayList<QueueElement> remain) {

    }

    @Override
    public void onResultSuccess(BaseResult result) {
        if (result instanceof UpdateCustomerResult) {
            UserUpdate userUpdate = ((UpdateCustomerResult) result).getUserUpdate();
            user.name = userUpdate.name;
            user.phone = userUpdate.phone;
            user.address = userUpdate.address;
            user.sex = userUpdate.sex;
            user.age = userUpdate.age;
            user.area_id = userUpdate.area;
            user.birthday = userUpdate.birthday;
            setLoggedUser(user);
            showAlertDialog(getActiveActivity(), UPDATED_SUCCESS_DIALOG, getGeneralDialogLayoutResource(), R.mipmap.ic_app_logo, getString(R.string.app_name), getString(R.string.customer_msg_update_success), getString(R.string.common_ok), null, this);
        }
    }

    @Override
    public void onResultSuccess(BaseResult result, QueueElement element) {
        if (result instanceof AreasResult) {
            ArrayList<Area> areas = ((AreasResult) result).getAreas();
            user_update_sp_area.setAdapter(new ToyosuAreaAdapter(areas));
            for (int i = 0; i < areas.size(); ++i)
                if (areas.get(i).id == user.area_id) {
                    user_update_sp_area.setSelection(i);
                    break;
                }
        }
    }

    @Override
    public void onResultFail(BaseResult result, QueueElement element) {
        super.onResultFail(result);
    }

    @Override
    public void onFail(RequestTarget target, String error, Constant.StatusCode code, QueueElement element) {
        if (!(code == Constant.StatusCode.ERR_QUEUE_IN_REQUEST))
            super.onFail(target, error, code);
    }

    @Override
    public void onConfirmed(int id, Object onWhat) {
        if (id == UPDATED_SUCCESS_DIALOG) {
            finish();
        }
    }

    @Override
    public void onUpdatePasswordClick(String old_password, String new_password) {
        makeRequest(getUniqueTag(), true, new UpdatePasswordParam(old_password, new_password), this, RequestTarget.UPDATE_PASSWORD);
    }

}
