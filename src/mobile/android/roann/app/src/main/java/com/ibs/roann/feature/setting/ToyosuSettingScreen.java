package com.ibs.roann.feature.setting;

import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.SwitchCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.ibs.roann.R;
import com.ibs.roann.feature.ToyosuBaseFragment;
import com.ibs.roann.feature.ToyosuConstant;
import com.ibs.roann.feature.ToyosuMainActivity;
import com.ibs.roann.feature.authentication.ToyosuLoginScreen;
import com.ibs.roann.feature.authentication.ToyosuUserUpdateScreen;
import com.ibs.roann.feature.prize.ToyosuPrizeScreen;
import com.ibs.roann.feature.shop.ToyosuRestaurantsScreen;
import com.ibs.roann.model.User;
import com.ibs.roann.model.api.LogoutParam;
import com.ibs.roann.model.api.LogoutResult;
import com.ibs.roann.model.api.SettingByCodeParam;
import com.ibs.roann.model.api.UpdateCustomerNotificationParam;
import com.ibs.roann.model.api.UpdateCustomerNotificationResult;
import com.ibs.roann.model.api.UpdatePushNotificationParam;
import com.ibs.roann.model.api.UpdatePushNotificationResult;

import butterknife.BindView;
import core.base.BaseResult;
import core.data.UserDataManager;
import core.dialog.GeneralDialog;
import core.util.Constant;
import core.util.DataKey;
import core.util.RequestTarget;
import icepick.State;

import static com.ibs.roann.feature.ToyosuConstant.APP_VERSION;
import static core.base.BaseFragmentContainer.TAG;

/**
 * Created by tuanle on 1/3/17.
 */

public class ToyosuSettingScreen extends ToyosuBaseFragment implements CompoundButton.OnCheckedChangeListener, GeneralDialog.DecisionListener {

    private static final int LOGOUT_DIALOG = 9004;

    @BindView(R.id.setting_rl_profile)
    RelativeLayout setting_rl_profile;

    @BindView(R.id.setting_rl_point)
    RelativeLayout setting_rl_point;

    @BindView(R.id.setting_rl_language)
    RelativeLayout setting_rl_language;

    @BindView(R.id.setting_rl_email_notification)
    RelativeLayout setting_rl_email_notification;

    @BindView(R.id.setting_rl_push_notification)
    RelativeLayout setting_rl_push_notification;

    @BindView(R.id.setting_rl_shop_info)
    RelativeLayout setting_rl_shop_info;

    @BindView(R.id.setting_rl_company_info)
    RelativeLayout setting_rl_company_info;

    @BindView(R.id.setting_rl_about_points)
    RelativeLayout setting_rl_about_points;

    @BindView(R.id.setting_rl_about_prize)
    RelativeLayout setting_rl_about_prize;

    @BindView(R.id.setting_rl_terms)
    RelativeLayout setting_rl_terms;

    @BindView(R.id.setting_rl_privacy)
    RelativeLayout setting_rl_privacy;

    @BindView(R.id.setting_rl_app_version)
    RelativeLayout setting_rl_app_version;

    @BindView(R.id.setting_rl_contact)
    RelativeLayout setting_rl_contact;

    @BindView(R.id.setting_rl_inform)
    RelativeLayout setting_rl_inform;

    @BindView(R.id.setting_rl_logout)
    RelativeLayout setting_rl_logout;

    @BindView(R.id.setting_sw_email_notification)
    SwitchCompat setting_sw_email_notification;

    @BindView(R.id.setting_sw_push_notification)
    SwitchCompat setting_sw_push_notification;

    @BindView(R.id.setting_tv_chinese)
    TextView setting_tv_chinese;

    @BindView(R.id.setting_img_chinese)
    View setting_img_chinese;

    @BindView(R.id.setting_tv_english)
    TextView setting_tv_english;

    @BindView(R.id.setting_img_english)
    View setting_img_english;

    @BindView(R.id.setting_tv_app_version_title)
    TextView setting_tv_app_version_title;

    @BindView(R.id.setting_tv_app_version)
    TextView setting_tv_app_version;

    @BindView(R.id.setting_tv_about_points_title)
    TextView setting_tv_about_points_title;

    @BindView(R.id.setting_tv_about_prize_title)
    TextView setting_tv_about_prize_title;

    @BindView(R.id.setting_tv_shop_info_title)
    TextView setting_tv_shop_info_title;

    @BindView(R.id.setting_tv_company_info_title)
    TextView setting_tv_company_info_title;

    @BindView(R.id.setting_tv_contact_us_title)
    TextView setting_tv_contact_us_title;

    @BindView(R.id.setting_tv_inform_friend_title)
    TextView setting_tv_inform_friend_title;

    @BindView(R.id.setting_tv_language_title)
    TextView setting_tv_language_title;

    @BindView(R.id.setting_tv_logout_title)
    TextView setting_tv_logout_title;

    @BindView(R.id.setting_tv_terms_title)
    TextView setting_tv_terms_title;

    @BindView(R.id.setting_tv_privacy_title)
    TextView setting_tv_privacy_title;

    @BindView(R.id.setting_tv_information_section_title)
    TextView setting_tv_information_section_title;

    @BindView(R.id.setting_tv_mail_notify_title)
    TextView setting_tv_mail_notify_title;

    @BindView(R.id.setting_tv_push_notify_title)
    TextView setting_tv_push_notify_title;

    @BindView(R.id.setting_tv_setting_section_title)
    TextView setting_tv_setting_section_title;

    @BindView(R.id.setting_tv_support_section_title)
    TextView setting_tv_support_section_title;

    @BindView(R.id.setting_tv_update_profile_title)
    TextView setting_tv_update_profile_title;

    @BindView(R.id.setting_tv_point_title)
    TextView setting_tv_point_title;

    @BindView(R.id.setting_ll_logout)
    LinearLayout setting_ll_logout;


    @State
    User user;

    @State
    boolean isPushNotificationReset;

    @State
    boolean isCustomerNotificationReset;

    @State
    boolean isDeviceLanguageInitialized;


    public static ToyosuSettingScreen getInstance() {
        return new ToyosuSettingScreen();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.toyosu_setting_screen, container, false);
    }

    @Override
    public void onBaseCreate() {
        super.onBaseCreate();
        user = getLoggedUser();
    }

    private void selectEnglish() {
        setting_img_chinese.setSelected(false);
        setting_tv_chinese.setSelected(false);
        setting_img_english.setSelected(true);
        setting_tv_english.setSelected(true);
    }

    private void selectChinese() {
        setting_img_english.setSelected(false);
        setting_tv_english.setSelected(false);
        setting_img_chinese.setSelected(true);
        setting_tv_chinese.setSelected(true);
    }

    private boolean isEnglishSelected() {
        return setting_img_english.isSelected() && !setting_img_chinese.isSelected();
    }

    @Override
    public void onBindView() {
        super.onBindView();
        setting_sw_push_notification.setOnCheckedChangeListener(this);
        setting_sw_email_notification.setOnCheckedChangeListener(this);
        disableCustomerFeatures();
    }

    private void disableCustomerFeatures() {
        setting_rl_point.setVisibility(View.GONE);
        setting_rl_company_info.setVisibility(View.GONE);
        setting_rl_profile.setVisibility(View.GONE);
        setting_rl_email_notification.setVisibility(View.GONE);
        setting_rl_push_notification.setVisibility(View.GONE);
        setting_rl_inform.setVisibility(View.GONE);
        setting_rl_contact.setVisibility(View.GONE);
        setting_tv_support_section_title.setVisibility(View.GONE);
//        setting_ll_logout.setVisibility(View.GONE);
    }

    @Override
    public void onInitializeViewData() {
        super.onInitializeViewData();
        if (UserDataManager.getInstance().getString(DataKey.DEVICE_LANGUAGE).equals(ToyosuConstant.DEVICE_LANGUAGE_ENGLISH))
            selectEnglish();
        else
            selectChinese();
        registerSingleAction(setting_rl_profile,
                setting_rl_point,
                setting_rl_shop_info,
                setting_rl_company_info,
                setting_rl_about_points,
                setting_rl_about_prize,
                setting_rl_terms,
                setting_rl_privacy,
                setting_rl_contact,
                setting_rl_inform,
                setting_rl_logout,
                setting_tv_english,
                setting_tv_chinese,
                setting_img_chinese,
                setting_img_english);
        refreshViewDataLanguage();
    }

    @Override
    public void reloadData() {

    }

    @Override
    public void onSingleClick(View v) {
        switch (v.getId()) {
            case R.id.setting_img_chinese:
            case R.id.setting_tv_chinese:
                if (isEnglishSelected()) {
                    new ChangeLanguageTask().execute(ToyosuConstant.DEVICE_LANGUAGE_CHINESE);
                }
                selectChinese();
                break;
            case R.id.setting_tv_english:
            case R.id.setting_img_english:
                if (!isEnglishSelected()) {
                    new ChangeLanguageTask().execute(ToyosuConstant.DEVICE_LANGUAGE_ENGLISH);
                }
                selectEnglish();
                break;

            case R.id.setting_rl_profile:
                addFragment(getMainContainerId(), ToyosuUserUpdateScreen.getInstance());
                break;
            case R.id.setting_rl_point:
                addFragment(getMainContainerId(), ToyosuPrizeScreen.getInstance());
                break;
            case R.id.setting_rl_shop_info:
                addFragment(getMainContainerId(), ToyosuRestaurantsScreen.getInstance());
                break;
            case R.id.setting_rl_about_points:
                addFragment(getMainContainerId(), ToyosuLegalInformationScreen.getInstance(SettingByCodeParam.SETTING_ABOUT_POINTS, getString(R.string.other_lb_about_points)));
                break;
            case R.id.setting_rl_about_prize:
                addFragment(getMainContainerId(), ToyosuLegalInformationScreen.getInstance(SettingByCodeParam.SETTING_ABOUT_PRIZE, getString(R.string.other_lb_about_prize)));
                break;
            case R.id.setting_rl_terms:
                addFragment(getMainContainerId(), ToyosuLegalInformationScreen.getInstance(SettingByCodeParam.SETTING_CODE_TERMS, getString(R.string.other_lb_terms_and_condition)));
                break;
            case R.id.setting_rl_privacy:
                addFragment(getMainContainerId(), ToyosuLegalInformationScreen.getInstance(SettingByCodeParam.SETTING_CODE_PRIVACY, getString(R.string.other_lb_privacy_policy)));
                break;
            case R.id.setting_rl_contact:
                addFragment(getMainContainerId(), ToyosuContactUsScreen.getInstance(UserDataManager.getInstance().getInt(DataKey.RESTAURANT_ID)));
                break;
            case R.id.setting_rl_inform:
                break;
            case R.id.setting_rl_logout:

                if (UserDataManager.getInstance().isEnabled(DataKey.LOGGED)) {
                    showDecisionDialog(getActiveActivity(), LOGOUT_DIALOG,
                            getGeneralDialogLayoutResource(),
                            R.mipmap.ic_app_logo,
                            getString(R.string.app_name),
                            getString(R.string.customer_msg_logout_confirm),
                            getString(R.string.common_yes),
                            getString(R.string.common_no),
                            null,
                            null, this);
                } else {
                    addFragment(getMainContainerId(), ToyosuLoginScreen.getInstance());
                }

                break;
        }
    }

    @Override
    public void refreshViewDataLanguage() {
        setting_tv_app_version_title.setText(R.string.other_lb_app_version);
        setting_tv_app_version.setText(APP_VERSION);
        setting_tv_shop_info_title.setText(R.string.shop_info_lb_title);
        setting_tv_about_points_title.setText(R.string.other_lb_about_points);
        setting_tv_about_prize_title.setText(R.string.other_lb_about_prize);
        setting_tv_company_info_title.setText(R.string.other_lb_company_info);
        setting_tv_contact_us_title.setText(R.string.other_lb_contact_us);
        setting_tv_inform_friend_title.setText(R.string.other_lb_inform_friend);
        setting_tv_language_title.setText(R.string.other_lb_language);
        setting_tv_terms_title.setText(R.string.other_lb_terms_and_condition);
        setting_tv_privacy_title.setText(R.string.other_lb_privacy_policy);
        setting_tv_information_section_title.setText(R.string.other_lb_section_information);
        setting_tv_mail_notify_title.setText(R.string.other_lb_mail_notify);
        setting_tv_push_notify_title.setText(R.string.other_lb_push_notify);
        setting_tv_setting_section_title.setText(R.string.other_lb_setting);
        setting_tv_support_section_title.setText(R.string.other_lb_section_support);
        setting_tv_update_profile_title.setText(R.string.other_lb_update_profile);
        setting_tv_point_title.setText(getString(R.string.other_lb_point_management));
        setting_tv_chinese.setText(R.string.other_lang_china);
        setting_tv_english.setText(R.string.other_lang_english);
        refreshLoggedStatus();
    }


    @Override
    public void onBaseFree() {
        super.onBaseFree();
        unregisterSingleAction(setting_rl_profile,
                setting_rl_point,
                setting_rl_shop_info,
                setting_rl_company_info,
                setting_rl_about_points,
                setting_rl_about_prize,
                setting_rl_terms,
                setting_rl_privacy,
                setting_rl_contact,
                setting_rl_inform,
                setting_ll_logout,
                setting_tv_english,
                setting_tv_chinese,
                setting_img_chinese,
                setting_img_english);
    }

    @Override
    public void onBaseResume() {
        super.onBaseResume();
        Log.e(TAG, "onBaseResume");
        refreshLoggedStatus();
    }


    private void refreshLoggedStatus() {
        if (UserDataManager.getInstance().isEnabled(DataKey.LOGGED)) {
            setting_tv_logout_title.setText(R.string.customer_lb_logout);
            setting_rl_profile.setVisibility(View.VISIBLE);
//            setting_rl_email_notification.setVisibility(View.VISIBLE);
            setting_rl_point.setVisibility(View.GONE);
        } else {
            if (getActiveActivity() instanceof ToyosuMainActivity) {
                ((ToyosuMainActivity) getActiveActivity()).backToLogin();
            }
        }
    }

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        switch (buttonView.getId()) {
            case R.id.setting_sw_push_notification:
                if (!isPushNotificationReset && user != null)
                    requestUpdatePushNotification(user.id, isChecked);
                else
                    isPushNotificationReset = false;
                break;
            case R.id.setting_sw_email_notification:
                if (!isCustomerNotificationReset && user != null)
                    requestUpdateCustomerNotification(user.id, isChecked);
                else
                    isCustomerNotificationReset = false;
                break;
        }
    }

    private void requestUpdatePushNotification(int user_id, boolean isON) {
        makeRequest(getUniqueTag(), true, new UpdatePushNotificationParam(user_id, isON), this, RequestTarget.UPDATE_PUSH_NOTIFICATION);
    }

    private void requestUpdateCustomerNotification(int user_id, boolean isON) {
        makeRequest(getUniqueTag(), true, new UpdateCustomerNotificationParam(user_id, isON), this, RequestTarget.UPDATE_CUSTOMER_NOTIFICATION);
    }

    @Override
    public int getEnterInAnimation() {
        return 0;
    }

    @Override
    public void onResultSuccess(BaseResult result) {
        super.onResultSuccess(result);
        if (result instanceof UpdatePushNotificationResult) {
            // nothing to do here
        } else if (result instanceof UpdateCustomerNotificationResult) {
            // nothing to do here
        } else if (result instanceof LogoutResult) {
            UserDataManager.getInstance().clear(DataKey.DEVICE_LANGUAGE, DataKey.DEVICE_TOKEN, DataKey.RESTAURANT_ID);
        //    UserDataManager.getInstance().clear();
            refreshLoggedStatus();
        }
    }

    @Override
    public void onResultFail(BaseResult result) {
        super.onResultFail(result);
        if (result instanceof UpdatePushNotificationResult) {
            isPushNotificationReset = true;
            setting_sw_push_notification.setChecked(!setting_sw_push_notification.isChecked());
        } else if (result instanceof UpdateCustomerNotificationResult) {
            isCustomerNotificationReset = true;
            setting_sw_email_notification.setChecked(!setting_sw_email_notification.isChecked());
        } else if (result instanceof LogoutResult) {
            UserDataManager.getInstance().clear(DataKey.DEVICE_LANGUAGE, DataKey.DEVICE_TOKEN, DataKey.RESTAURANT_ID);
        //    UserDataManager.getInstance().clear();
            refreshLoggedStatus();
        }
    }

    @Override
    public void onFail(RequestTarget target, String error, Constant.StatusCode code) {
        super.onFail(target, error, code);
        switch (target) {
            case UPDATE_PUSH_NOTIFICATION:
                isPushNotificationReset = true;
                setting_sw_push_notification.setChecked(!setting_sw_push_notification.isChecked());
                break;
            case UPDATE_CUSTOMER_NOTIFICATION:
                isCustomerNotificationReset = true;
                setting_sw_email_notification.setChecked(!setting_sw_email_notification.isChecked());
                break;
            case LOG_OUT:
                UserDataManager.getInstance().clear(DataKey.DEVICE_LANGUAGE, DataKey.DEVICE_TOKEN, DataKey.RESTAURANT_ID);
            //    UserDataManager.getInstance().clear();
                refreshLoggedStatus();
                break;
        }
    }

    @Override
    public void onAgreed(int id, Object onWhat) {
        if (id == LOGOUT_DIALOG) {
            makeRequest(getUniqueTag(), true, new LogoutParam(), this, RequestTarget.LOG_OUT);
        }
    }

    @Override
    public void onDisAgreed(int id, Object onWhat) {

    }

    @Override
    public void onNeutral(int id, Object onWhat) {

    }


    private class ChangeLanguageTask extends AsyncTask<String, Void, Void> {

        @Override
        protected void onPreExecute() {
            showLoadingDialog(getActiveActivity(), getLoadingDialogLayoutResource(), getString(R.string.common_loading));
            super.onPreExecute();

        }

        @Override
        protected Void doInBackground(String... params) {
            UserDataManager.getInstance().setString(DataKey.DEVICE_LANGUAGE, params[0]);
            changeLanguage(params[0]);
            return null;
        }
    }
}
