package com.ibs.roann.model.api;

import com.ibs.roann.feature.ToyosuBaseResult;
import com.ibs.roann.model.Coupon;

import java.util.ArrayList;

/**
 * Created by tuanle on 1/3/17.
 */

public class CouponsResult extends ToyosuBaseResult {
    private ArrayList<Coupon> topics;
    private int total;

    public CouponsResult() {
        this.topics = new ArrayList<>();
        this.total = 0;
    }

    public ArrayList<Coupon> getCoupons() {
        return topics;
    }

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }
}
