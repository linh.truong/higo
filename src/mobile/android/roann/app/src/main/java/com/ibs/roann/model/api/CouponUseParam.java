package com.ibs.roann.model.api;

import com.ibs.roann.feature.ToyosuBaseParam;

import org.json.JSONObject;

/**
 * Created by tuanle on 1/3/17.
 */

public class CouponUseParam extends ToyosuBaseParam {

    private static final String PARAM_COUPON_ID_TAG = "coupon_id";
    private static final String PARAM_CUSTOMER_ID_TAG = "id";

    private int customer_id;
    private int coupon_id;
    private String auth_token;

    public CouponUseParam(int customerId, int couponId, String auth_token) {
        this.customer_id = customerId;
        this.coupon_id = couponId;
        this.auth_token = auth_token;
    }

    @Override
    public byte[] makeRequestBody() {

        try {
            return new JSONObject().put(PARAM_CUSTOMER_ID_TAG, customer_id)
                    .put(PARAM_COUPON_ID_TAG, coupon_id)
                    .put(PARAM_AUTH_TOKEN_TAG, auth_token).toString().getBytes();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return new byte[0];
    }
}

