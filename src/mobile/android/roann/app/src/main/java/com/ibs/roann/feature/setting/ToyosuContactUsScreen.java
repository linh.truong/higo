package com.ibs.roann.feature.setting;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.method.ScrollingMovementMethod;
import android.util.Patterns;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.ibs.roann.R;
import com.ibs.roann.feature.ToyosuBaseFragment;
import com.ibs.roann.model.User;
import com.ibs.roann.model.api.ContactUsParam;
import com.ibs.roann.model.api.ContactUsResult;
import com.ibs.roann.model.api.SettingByCodeParam;
import com.ibs.roann.model.api.SettingByCodeResult;

import butterknife.BindView;
import core.base.BaseResult;
import core.dialog.GeneralDialog;
import core.util.RequestTarget;
import core.util.Utils;
import icepick.State;

/**
 * Created by tuanle on 1/11/17.
 */

public class ToyosuContactUsScreen extends ToyosuBaseFragment implements CompoundButton.OnCheckedChangeListener, GeneralDialog.ConfirmListener {

    private static final int CONTACT_MESSAGE_SENT_DIALOG = 9013;
    private static final String RESTAURANT_ID = "restaurant_id";

    @BindView(R.id.contact_us_tv_name_title)
    TextView contact_us_tv_name_title;

    @BindView(R.id.contact_us_tv_name_error)
    TextView contact_us_tv_name_error;

    @BindView(R.id.contact_us_ed_name)
    EditText contact_us_ed_name;

    @BindView(R.id.contact_us_tv_email_title)
    TextView contact_us_tv_email_title;

    @BindView(R.id.contact_us_tv_email_error)
    TextView contact_us_tv_email_error;

    @BindView(R.id.contact_us_ed_email)
    EditText contact_us_ed_email;

    @BindView(R.id.contact_us_tv_phone_title)
    TextView contact_us_tv_phone_title;

    @BindView(R.id.contact_us_tv_phone_error)
    TextView contact_us_tv_phone_error;

    @BindView(R.id.contact_us_ed_phone)
    EditText contact_us_ed_phone;

    @BindView(R.id.contact_us_ed_message)
    EditText contact_us_ed_message;

    @BindView(R.id.contact_us_tv_privacy)
    TextView contact_us_tv_privacy;

    @BindView(R.id.contact_us_cb_terms)
    CheckBox contact_us_cb_terms;

    @BindView(R.id.contact_us_tv_message_error)
    TextView contact_us_tv_message_error;

    @BindView(R.id.contact_us_tv_message_title)
    TextView contact_us_tv_message_title;

    @BindView(R.id.contact_us_img_submit)
    ImageView contact_us_img_submit;

    @State
    int restaurant_id;

    @State
    User user;

    private View.OnTouchListener insideScroller;

    public static ToyosuContactUsScreen getInstance(int restaurant_id) {
        ToyosuContactUsScreen screen = new ToyosuContactUsScreen();
        Bundle bundle = new Bundle();
        bundle.putInt(RESTAURANT_ID, restaurant_id);
        screen.setArguments(bundle);
        return screen;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.toyosu_contact_us_screen, container, false);
    }

    @Override
    public void onBaseCreate() {
        super.onBaseCreate();
        Bundle bundle = getArguments();

        if (bundle != null) {
            restaurant_id = bundle.getInt(RESTAURANT_ID);
        }

        user = getLoggedUser();

        insideScroller = new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (v.getId() == R.id.contact_us_tv_privacy) {
                    v.getParent().requestDisallowInterceptTouchEvent(true);
                    switch (event.getAction() & MotionEvent.ACTION_MASK) {
                        case MotionEvent.ACTION_UP:
                            v.getParent().requestDisallowInterceptTouchEvent(false);
                            break;
                    }
                }
                return false;
            }
        };
    }

    @Override
    public void onInitializeViewData() {
        super.onInitializeViewData();
        requestSettingByCode(SettingByCodeParam.SETTING_CODE_PRIVACY);
        contact_us_img_submit.setEnabled(false);
        contact_us_cb_terms.setOnCheckedChangeListener(this);
        contact_us_tv_privacy.setMovementMethod(new ScrollingMovementMethod());
        contact_us_tv_privacy.setOnTouchListener(insideScroller);
        if (user != null) {
            contact_us_ed_phone.setText(user.phone);
            contact_us_ed_email.setText(user.email);
            contact_us_ed_name.setText(user.name);
        }
    }

    @Override
    public void reloadData() {

    }

    @Override
    public void onBaseResume() {
        registerSingleAction(R.id.contact_us_tv_submit);
        super.onBaseResume();
    }

    @Override
    protected void onBasePause() {
        unregisterSingleAction(R.id.contact_us_tv_submit);
        super.onBasePause();
    }

    @Override
    public void onSingleClick(View v) {
        switch (v.getId()) {
            case R.id.contact_us_tv_submit:
                if (validated()) {
                    requestContactUs(restaurant_id,
                            contact_us_ed_name.getText().toString().trim(),
                            contact_us_ed_email.getText().toString().trim(),
                            contact_us_ed_phone.getText().toString().trim(),
                            contact_us_ed_message.getText().toString().trim());
                }
                break;
        }
    }

    @Override
    public void refreshViewDataLanguage() {

    }

    private void requestContactUs(int restaurant_id, String name, String email, String phone, String message) {
        makeRequest(getUniqueTag(), true, new ContactUsParam(restaurant_id, name, email, phone, message), this, RequestTarget.CONTACT_US);
    }

    private void requestSettingByCode(String code) {
        makeRequest(getUniqueTag(), true, new SettingByCodeParam(code), this, RequestTarget.SETTING_BY_CODE);
    }

    private boolean validated() {
        boolean result = true;

        if (!contact_us_cb_terms.isChecked()) {
            return false;
        }
        String name = contact_us_ed_name.getText().toString().trim();
        if (Utils.isEmpty(name) || !name.contains(" ")) {
            showInputNameError();
            result = false;
        } else {
            hideInputNameError();
        }
        if (Utils.isEmpty(contact_us_ed_email.getText().toString().trim())) {
            showInputEmailError(getString(R.string.customer_lb_email_required));
            result = false;
        } else {
            if (!Patterns.EMAIL_ADDRESS.matcher(contact_us_ed_email.getText().toString().trim()).matches()) {
                showInputEmailError(getString(R.string.customer_lb_email_invalid));
                result = false;
            } else
                hideInputEmailError();
        }

        if (Utils.isEmpty(contact_us_ed_phone.getText().toString().trim())) {
            showInputPhoneError();
            result = false;
        } else
            hideInputPhoneError();

        if (Utils.isEmpty(contact_us_ed_message.getText().toString().trim())) {
            showInputMessageError();
            result = false;
        } else
            hideInputMessageError();
        return result;
    }

    private void showInputMessageError() {
        contact_us_tv_message_error.setVisibility(View.VISIBLE);
        contact_us_tv_message_title.setVisibility(View.GONE);
    }

    private void hideInputMessageError() {
        contact_us_tv_message_error.setVisibility(View.GONE);
        contact_us_tv_message_title.setVisibility(View.VISIBLE);
    }

    private void showInputNameError() {
        contact_us_tv_name_error.setVisibility(View.VISIBLE);
        contact_us_tv_name_title.setVisibility(View.GONE);
    }

    private void hideInputNameError() {
        contact_us_tv_name_error.setVisibility(View.GONE);
        contact_us_tv_name_title.setVisibility(View.VISIBLE);
    }

    private void showInputPhoneError() {
        contact_us_tv_phone_error.setVisibility(View.VISIBLE);
        contact_us_tv_phone_title.setVisibility(View.GONE);
    }

    private void hideInputPhoneError() {
        contact_us_tv_phone_error.setVisibility(View.GONE);
        contact_us_tv_phone_title.setVisibility(View.VISIBLE);
    }

    private void showInputEmailError(String error) {
        contact_us_tv_email_error.setVisibility(View.VISIBLE);
        contact_us_tv_email_title.setVisibility(View.GONE);
        contact_us_tv_email_error.setText(error);
    }

    private void hideInputEmailError() {
        contact_us_tv_email_error.setVisibility(View.GONE);
        contact_us_tv_email_title.setVisibility(View.VISIBLE);
    }


    @Override
    public void onResultSuccess(BaseResult result) {
        if (result instanceof SettingByCodeResult) {
            contact_us_tv_privacy.setText(((SettingByCodeResult) result).getLegal().getValue());
        } else if (result instanceof ContactUsResult) {
            showAlertDialog(getActiveActivity(), CONTACT_MESSAGE_SENT_DIALOG, getGeneralDialogLayoutResource(),
                    R.mipmap.ic_app_logo, getString(R.string.common_info),
                    getString(R.string.other_contact_us_msg_sent), getString(R.string.common_ok), null, this);
        }
    }

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        contact_us_img_submit.setEnabled(isChecked && buttonView.getId() == R.id.contact_us_cb_terms);
    }

    @Override
    public void onConfirmed(int id, Object onWhat) {
        switch (id) {
            case CONTACT_MESSAGE_SENT_DIALOG:
                finish();
                break;
        }
    }
}
