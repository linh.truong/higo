package com.ibs.roann.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 *
 */

public class Caption implements Serializable{

    @SerializedName("created_time")
    public String created_time;

    @SerializedName("text")
    public String text;

    @SerializedName("id")
    public String id;

}
