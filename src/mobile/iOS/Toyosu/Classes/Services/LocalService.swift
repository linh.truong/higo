//
//  LocalService.swift
//  higo
//
//  Created by Linh Trương on 10/27/16.
//  Copyright © 2016 IBS. All rights reserved.
//

import Foundation
import ObjectMapper
import Localize_Swift

let userInfoKey = "higo.user_info"
let authToken = "higo_auth_token"
let appFirstLoad = "toyoso.app_first_load"
let isAllowPushNoti = "higo.isAllowPushNoti"
let settingInfoKey = "higo.setting_info"

class LocalService {
    
    class func saveUserInfo(_ cusLogin: Customers?) {
        if let user = cusLogin {
            if let token = user.auth_token {
                UserDefaults.standard.set(token, forKey: authToken)
            }
            UserDefaults.standard.set(user.toJSONString(), forKey: userInfoKey)
        }else{
            UserDefaults.standard.set(nil, forKey: userInfoKey)
        }
        UserDefaults.standard.synchronize()
    }
    
    class func loadSavedUserInfo() -> Customers? {
        
        if let customerString = UserDefaults.standard.string(forKey: userInfoKey) {
            return Mapper<Customers>().map(JSONString: customerString)
        }else{
            return nil
        }
    }
    
    class func saveSettingInfo(_ settingList: [Setting]?) {
        if let settingArr = settingList {
            UserDefaults.standard.set(settingArr.toJSONString(), forKey: settingInfoKey)
        }else{
            UserDefaults.standard.set(nil, forKey: settingInfoKey)
        }
        UserDefaults.standard.synchronize()
    }
    
    class func loadSavedSettingInfo() -> [Setting]? {
        if let settingStr = UserDefaults.standard.string(forKey: settingInfoKey) {
            return Mapper<Setting>().mapArray(JSONString: settingStr)
        }else{
            return nil
        }
    }
    
    class func saveCustomerMyPage(_ cusMyPage: CustomerMyPage?) {
        if let user = cusMyPage {
            UserDefaults.standard.set(user.toJSONString(), forKey: userInfoKey)
        }else{
            UserDefaults.standard.set(nil, forKey: userInfoKey)
        }
        UserDefaults.standard.synchronize()
    }
    
    class func loadAuthToken() -> String? {
        return UserDefaults.standard.string(forKey: authToken)
    }
    
    class func saveFirstLoad(_ isFirstLoad: Bool) {
        UserDefaults.standard.set(isFirstLoad, forKey: appFirstLoad)
        UserDefaults.standard.synchronize()
    }
    
    class func loadFirstLoad() -> Bool {
        let isLoad = UserDefaults.standard.bool(forKey: appFirstLoad)
        return isLoad
    }
    
    class func saveIsAllowPushNoti(_ isPushNoti: Bool) {
        UserDefaults.standard.set(isPushNoti, forKey: isAllowPushNoti)
        UserDefaults.standard.synchronize()
    }
    
    class func loadIsAllowPushNoti() -> Bool {
        let appPushNotiResult = UserDefaults.standard.bool(forKey: isAllowPushNoti)
        return appPushNotiResult
    }
}
