//
//  APIRestaurantListResult.swift
//  Toyosu
//
//  Created by Linh Trương on 11/8/16.
//  Copyright © 2016 IBS. All rights reserved.
//

import Foundation
import ObjectMapper

class APIRestaurantByIdResult : APIServiceResult {
    var data: Restaurant?
    
    override func mapping(map: Map) {
        super.mapping(map: map)
        data <- map["data"]
    }
}
