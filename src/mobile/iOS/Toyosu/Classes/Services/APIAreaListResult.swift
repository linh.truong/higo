//
//  APIAreaListResult.swift
//  Toyosu
//
//  Created by nguyen.trung.hau on 11/16/16.
//  Copyright © 2016 Linh Trương. All rights reserved.
//

import Foundation
import ObjectMapper

class APIAreaListResult : APIServiceResult {
    var data: [Area]?
    
    override func mapping(map: Map) {
        super.mapping(map: map)
        data <- map["data"]
    }
}
