//
//  APIQRUpdateServiceResult.swift
//  Toyosu
//
//  Created by nguyen.trung.hau on 11/21/16.
//  Copyright © 2016 Linh Trương. All rights reserved.
//

import Foundation
import ObjectMapper

class APIQRUpdateServiceResult : APIServiceResult {
    
    var current_point: Int?
    var total_point: Int?
    var res_point: Int?
    
    override func mapping(map: Map) {
        super.mapping(map: map)
        
        current_point <- map["current_point"]
        total_point <- map["total_point"]
        res_point <- map["res_point"]
    }
}
