//
//  APIMembershipResult.swift
//  Toyosu
//
//  Created by Linh Trương on 1/23/17.
//  Copyright © 2017 IBS. All rights reserved.
//

import Foundation
import ObjectMapper

class APIMembershipResult : APIServiceResult {
    var data: Membership?
    
    override func mapping(map: Map) {
        super.mapping(map: map)
        
        data <- map["data"]
    }
}
