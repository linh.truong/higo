//
//  APIService.swift
//  Toyosu
//
//  Created by Linh Trương on 10/18/16.
//  Copyright © 2016 Linh Trương. All rights reserved.
//

import Foundation
import AFNetworking
import ObjectMapper

class APIService {
    
    static let sharedService = APIService ()
    
    // SERVER
    var SERVER_PATH = ServerURL.DEFAULT
    var manager : AFHTTPSessionManager!
    
    // URL
    var URL_TOPIC_GET_LIST = ServerURL.DEFAULT + "/api/topic/getlist"
    var URL_FOOD_CATE_GET_LIST = ServerURL.DEFAULT + "/api/food_cat/getlist"
    var URL_FOOD_GET_LIST_BY_CATE = ServerURL.DEFAULT + "/api/food/getlist_by_multi_cat"
//    var URL_COUPON_GET_LIST = ServerURL.DEFAULT + "/api/coupon/getlist_ctm"
    var URL_COUPON_GET_LIST_CTM = ServerURL.DEFAULT + "/api/coupon/getlist_by_cus_id"
    var URL_RESTAURANT_GET_BY_ID = ServerURL.DEFAULT + "/api/res/getinfo"
    var URL_RESTAURANT_GET_LIST = ServerURL.DEFAULT + "/api/res/getlist"
    var URL_PRIZE_GET_LIST = ServerURL.DEFAULT + "/api/prize/getlist"
    var URL_PRIZE_USE_POINT = ServerURL.DEFAULT + "/api/cus_prize/use_prize"
    var URL_CUSTOMER_RECOVER_PASSWORD = ServerURL.DEFAULT + "/api/login/forgot_password"
    var URL_INSERT_RESERVATION = ServerURL.DEFAULT + "/api/reserve/insert"
    var URL_RELATED_ITEMS_GET_LIST = ServerURL.DEFAULT + "/api/related/getlist_v2"
    //    var URL_MEMBERSHIP_CARD = ServerURL.DEFAULT + "/api/cus/membership"
    var URL_CONTACT_US = ServerURL.DEFAULT + "/api/contact/insert"
    var URL_CUSTOMER_UPDATE = ServerURL.DEFAULT + "/api/cus/update"
    var URL_SIMPLY_BOOK_GET_BOOKING = ServerURL.DEFAULT + "/api/simplybook/get_book_info"
    var URL_SIMPLY_BOOK_GET_TIME = ServerURL.DEFAULT + "/api/simplybook/get_reserve_time"
    var URL_SIMPLY_BOOK_BOOKING = ServerURL.DEFAULT + "/api/simplybook/booking_new"
    var URL_SETTING_GET_LIST = ServerURL.DEFAULT + "/api/setting/getlist"
    var URL_INSTAGRAM_GET_LIST = ServerURL.DEFAULT + "/api/instagram/getdata"
    var URL_INSERT_CUSTOMER_COUPON = ServerURL.DEFAULT + "/api/cus_cpn/insert"
    
    // function init
    init() {
        let serverURL = Foundation.URL(string: SERVER_PATH)
        manager = AFHTTPSessionManager(baseURL: serverURL)
        
        manager.requestSerializer = AFJSONRequestSerializer()
        manager.requestSerializer.timeoutInterval = Constant.REQUEST_TIMEOUT
        manager.responseSerializer = AFJSONResponseSerializer()
    }
    
    // MARK - customer insert reservation
    func useCoupon(_ cusId: Int?, _ authToken: String, _ couponId: Int?, completed: @escaping (_ result: Bool, _ code: Int?, _ message: String?) -> ()) {
        
        manager.post(
            URL_INSERT_CUSTOMER_COUPON,
            parameters: ["id": cusId, "auth_token": authToken, "coupon_id": couponId],
            progress: nil,
            success: { (requestOperation, responseObject) in
                guard let result = Mapper<APIServiceResult>().map(JSONObject: responseObject) else {
                    completed(false, nil, "error_server_fail".localized())
                    return
                }
                completed (true, result.error, result.message)
        }) { (requestOperation, error) in
            completed(false, nil, "error_parsing_fail".localized())
        }
    }
    
    
    // MARK - get data from Instagram account
    func getInstagramData( _ nextMaxId: String?, _ completed: @escaping (_ result: Bool, _ error: Int?, _ message: String?, _ data: [InstagramModel]?, _ pagination: InstagramPaginationModel?) -> ()) {
        
        manager.post(URL_INSTAGRAM_GET_LIST,
                     parameters:  ["next_max_id" : nextMaxId],
                     progress: nil,
                     success: { (requestOperation, responseObject) in
                        
            guard let result = Mapper<APIInstagramResult>().map(JSONObject: responseObject) else {
                completed(false, nil, "error_server_fail".localized(), nil, nil)
                return
            }
            completed(true, result.error, result.message, result.data, result.pagination)
        }) { (requestOperation, error) in
            completed(false, nil, "error_server_fail".localized(), nil, nil)
        }
    }
    
    // MARK - get booking information from Simplybook
    func getBooking(
        _ resId: Int,
        completed: @escaping (
        _ result: Bool,
        _ code: Int?,
        _ message: String?,
        _ sbEvent: [SimplyBookEvent]?,
        _ sbUnit: [SimplyBookUnit]?,
        _ sbAdditional: [SimplyBookAdditional]?,
        _ sbWorkingDate: [SimplyBookWorkingDate]?) -> ()) {
        
        manager.post(
            URL_SIMPLY_BOOK_GET_BOOKING,
            parameters: ["resId" : resId],
            progress: nil,
            success: { (requestOperation, responseObject) in
                guard let result = Mapper<APIGetBookingResult>().map(JSONObject: responseObject) else {
                    completed(false, nil, "error_server_fail".localized(), nil, nil, nil, nil)
                    return
                }
                completed(true, result.error, result.message, result.event, result.unit, result.additional, result.working_date)
        }) { (requestOperation, error) in
            completed(false, nil, "error_server_fail".localized(), nil, nil, nil, nil)
        }
        
    }
    
    // MARK - get reserve time from Simplybook
    func getReserveTime(
        _ resId: Int,
        _ reserveDate: String,
        _ unitId: Int,
        _ eventId: Int,
        completed: @escaping (
        _ result: Bool,
        _ code: Int?,
        _ message: String?,
        _ sbTime: [SimplyBookTime]?) -> ()) {
        
        manager.post(
            URL_SIMPLY_BOOK_GET_TIME,
            parameters: ["resId": resId,
                         "reserveDate": reserveDate,
                         "unitId": unitId,
                         "eventId": eventId],
            progress: nil,
            success: { (requestOperation, responseObject) in
                guard let result = Mapper<APIBookingTimeResult>().map(JSONObject: responseObject) else {
                    completed(false, nil, "error_server_fail".localized(), nil)
                    return
                }
                completed(true, result.error, result.message, result.data)
        }) { (requestOperation, error) in
            completed(false, nil, "error_server_fail".localized(), nil)
        }
        
    }
    
    // MARK - get reserve time from Simplybook
    func booking(_ resId: Int, _ customerId: Int, _ cusName: String, _ cusEmail: String, _ cusPhone: String, _ eventId: String, _ unitList: String, _ time: String, _ date: String, _ numberOfSeat: Int, _ additional: String, completed: @escaping (_ result: Bool, _ code: Int?, _ message: String?, _ reservation: Reservation?) -> ()) {
        
        manager.post(
            URL_SIMPLY_BOOK_BOOKING,
            parameters: ["resId": resId,
                         "customerId": customerId,
                         "cusName": cusName,
                         "cusEmail": cusEmail,
                         "cusPhone": cusPhone,
                         "eventId": eventId,
                         "unitList": unitList,
                         "time": time,
                         "date": date,
                         "numberOfSeat": numberOfSeat,
                         "additional": additional
            ],
            progress: nil,
            success: { (requestOperation, responseObject) in
                guard let result = Mapper<APIBookingResult>().map(JSONObject: responseObject) else {
                    completed(false, nil, "error_server_fail".localized(), nil)
                    return
                }
                completed(true, result.error, result.message, result.data)
        }) { (requestOperation, error) in
            completed(false, nil, "error_server_fail".localized(), nil)
        }
        
    }
    
    // MARK - get related items list by hash_tags
    func getRelatedItemsList(_ resId: Int, _ cusId: Int?, _ id: Int, _ objectTypes: Int, _ hashTags: String, _ limit: Int, completed: @escaping (_ result: Bool, _ code: Int?, _ message: String?, _ relatedItems: [CustomRelatedItems]?) -> ()) {
        
        manager.post(
            URL_RELATED_ITEMS_GET_LIST,
            parameters: ["restaurant_id" : resId, "customer_id": cusId!, "obj_id" : id, "obj_type" : objectTypes, "hash_tags" : hashTags, "limit" : limit],
            progress: nil,
            success: { (requestOperation, responseObject) in
                guard let result = Mapper<APIRelatedItemsResult>().map(JSONObject: responseObject) else {
                    completed(false, nil, "error_server_fail".localized(), nil)
                    return
                }
                completed (true, result.error, result.message, result.data)
        }) { (requestOperation, error) in
            completed(false, nil, "error_parsing_fail".localized(), nil)
        }
        
    }
    
    // MARK - get list topic
    func getTopicList(_ limit: Int, _ offset: Int, completed: @escaping (_ result: Bool, _ code: Int?, _ message: String?, _ total: Int, _ topics: [Topic]?) -> ()) {
        
        manager.post(
            URL_TOPIC_GET_LIST,
            parameters: ["limit" : limit, "offset" : offset],
            progress: nil,
            success: { (requestOperation, responseObject) in
                guard let result = Mapper<APITopicListResult>().map(JSONObject: responseObject) else {
                    completed(false, nil, "error_server_fail".localized(), 0, nil)
                    return
                }
                completed (true, result.error, result.message, result.total ?? 0, result.data)
        }) { (requestOperation, error) in
            completed(false, nil, "error_parsing_fail".localized(), 0, nil)
        }
        
    }
    
    // MARK - get list food category
    func getFoodCategoryList(_ completed: @escaping (_ result: Bool, _ code: Int?, _ message: String?, _ categories: [FoodCategories]?) -> ()) {
        
        manager.post(
            URL_FOOD_CATE_GET_LIST,
            parameters: nil,
            progress: nil,
            success: { (requestOperation, responseObject) in
                guard let result = Mapper<APIFoodCategoriesListResult>().map(JSONObject: responseObject) else {
                    completed(false, nil, "error_server_fail".localized(), nil)
                    return
                }
                completed (true, result.error, result.message, result.data)
        }) { (requestOperation, error) in
            completed(false, nil, "error_parsing_fail".localized(), nil)
        }
        
    }
    
    // MARK - get list foods by food cate_id
    func getFoodsByCateId(_ categoriesId: [Int], _ limit: Int, _ offset: Int, completed: @escaping (_ result: Bool, _ code: Int?, _ message: String?, _ total: Int, _ foods: [String: [Food]]?) -> ()) {
        
        manager.post(
            URL_FOOD_GET_LIST_BY_CATE,
            parameters: ["id" : categoriesId, "limit" : limit, "offset" : offset],
            progress: nil,
            success: { (requestOperation, responseObject) in
                guard let result = Mapper<APIFoodByCategoriesResult>().map(JSONObject: responseObject) else {
                    completed(false, nil, "error_server_fail".localized(), 0, nil)
                    return
                }
                completed (true, result.error, result.message, result.total ?? 0, result.data)
        }) { (requestOperation, error) in
            completed(false, nil, "error_parsing_fail".localized(), 0, nil)
        }
        
    }
    
//    // MARK - get list coupon custom
//    func getCouponListCustom(_ limit: Int, _ offset: Int, completed: @escaping (_ result: Bool, _ code: Int?, _ message: String?, _ total: Int, _ coupons: [Coupon]?) -> ()) {
//        
//        manager.post(
//            URL_COUPON_GET_LIST,
//            parameters: ["limit" : limit, "offset" : offset],
//            progress: nil,
//            success: { (requestOperation, responseObject) in
//                guard let result = Mapper<APICouponListResult>().map(JSONObject: responseObject) else {
//                    completed(false, nil, "error_server_fail".localized(), 0, nil)
//                    return
//                }
//                completed (true, result.error, result.message, result.total ?? 0, result.data)
//        }) { (requestOperation, error) in
//            completed(false, nil, "error_parsing_fail".localized(), 0, nil)
//        }
//        
//    }
    
    // MARK - get list coupon custom
    func getCouponListCustom(_ cusId: Int, _ limit: Int, _ offset: Int, completed: @escaping (_ result: Bool, _ code: Int?, _ message: String?, _ total: Int, _ coupons: [Coupon]?) -> ()) {
        
        manager.post(
            URL_COUPON_GET_LIST_CTM,
            parameters: ["cusId" : cusId, "limit" : limit, "offset" : offset],
            progress: nil,
            success: { (requestOperation, responseObject) in
                guard let result = Mapper<APICouponListResult>().map(JSONObject: responseObject) else {
                    completed(false, nil, "error_server_fail".localized(), 0, nil)
                    return
                }
                completed (true, result.error, result.message, result.total ?? 0, result.data)
        }) { (requestOperation, error) in
            completed(false, nil, "error_parsing_fail".localized(), 0, nil)
        }
        
    }
    
    // MARK - get restaurant by ID
    func getRestaurantById(_ restaurantId: Int, completed: @escaping (_ result: Bool, _ code: Int?, _ message: String?, _ result: Restaurant? )-> ()) {
        
        manager.post(
            URL_RESTAURANT_GET_BY_ID,
            parameters: ["id": restaurantId],
            progress: nil,
            success: { (requestOperation, responseObject) in
                guard let result = Mapper<APIRestaurantByIdResult>().map(JSONObject: responseObject) else {
                    completed(false, nil, "error_server_fail".localized(), nil)
                    return
                }
                completed (true, result.error, result.message, result.data)
        }) { (requestOperation, error) in
            completed(false, nil, "error_parsing_fail".localized(), nil)
        }
        
    }
    
    // MARK - get list shop info
    func getRestaurantList(_ limit: Int, _ offset: Int, completed: @escaping (_ result: Bool, _ code: Int?, _ message: String?, _ total: Int?, _ topics: [Restaurant]?) -> ()) {
        
        manager.post(
            URL_RESTAURANT_GET_LIST,
            parameters: ["limit" : limit, "offset" : offset],
            progress: nil,
            success: { (requestOperation, responseObject) in
                guard let result = Mapper<APIRestaurantListResult>().map(JSONObject: responseObject) else {
                    completed(false, nil, "error_server_fail".localized(), nil, nil)
                    return
                }
                completed (true, result.error, result.message, result.total ?? 0, result.data)
        }) { (requestOperation, error) in
            completed(false, nil, "error_parsing_fail".localized(), nil, nil)
        }
        
    }
    
    // MARK - get prize list
    func getPrizeList(_ limit: Int, _ offset: Int, completed: @escaping (_ result: Bool, _ code: Int?, _ message: String?, _ total: Int, _ prizes: [Prize]?) -> ()) {
        
        manager.post(
            URL_PRIZE_GET_LIST,
            parameters: ["limit" : limit, "offset" : offset],
            progress: nil,
            success: { (requestOperation, responseObject) in
                guard let result = Mapper<APIPrizeListResult>().map(JSONObject: responseObject) else {
                    completed(false, nil, "error_server_fail".localized(), 0, nil)
                    return
                }
                completed (true, result.error, result.message, result.total ?? 0, result.data)
        }) { (requestOperation, error) in
            completed(false, nil, "error_parsing_fail".localized(), 0, nil)
        }
        
    }
    
    // MARK - use prize
    func usePrize(_ cusId: Int, _ authToken: String, _ prizeId: Int, _ customerPrize: CustomerPrizes, _ language: String, completed: @escaping (_ result: Bool, _ code: Int?, _ message: String?, _ cusPrizes: CustomerPrizes?) -> ()) {
        
        manager.post(
            URL_PRIZE_USE_POINT,
            parameters: ["id": cusId, "auth_token": authToken, "prize_id": prizeId, "data": customerPrize.toJSONString()!, "lang": language],
            progress: nil,
            success: { (requestOperation, responseObject) in
                guard let result = Mapper<APICustomerPrizeResult>().map(JSONObject: responseObject) else {
                    completed(false, nil, "error_server_fail".localized(), nil)
                    return
                }
                completed (true, result.error, result.message, result.data)
        }) { (requestOperation, error) in
            completed(false, nil, "error_parsing_fail".localized(), nil)
        }
        
    }
    
    // MARK - customer insert reservation
    func insertReservation(_ reservation: Reservation, _ current_lang: String, completed: @escaping (_ result: Bool, _ code: Int?, _ message: String?) -> ()) {
        
        manager.post(
            URL_INSERT_RESERVATION,
            parameters: ["data": reservation.toJSONString(), "lang" : current_lang],
            progress: nil,
            success: { (requestOperation, responseObject) in
                guard let result = Mapper<APIServiceResult>().map(JSONObject: responseObject) else {
                    completed(false, nil, "error_server_fail".localized())
                    return
                }
                completed (true, result.error, result.message)
        }) { (requestOperation, error) in
            completed(false, nil, "error_parsing_fail".localized())
        }
    }
    
    // MARK - customer insert contact us
    func insertContactUs(_ contactUs: Contact, completed: @escaping (_ result: Bool, _ code: Int?, _ message: String?) -> ()) {
        
        manager.post(
            URL_CONTACT_US,
            parameters: ["data": contactUs.toJSONString()],
            progress: nil,
            success: { (requestOperation, responseObject) in
                guard let result = Mapper<APIServiceResult>().map(JSONObject: responseObject) else {
                    completed(false, nil, "error_server_fail".localized())
                    return
                }
                completed (true, result.error, result.message)
        }) { (requestOperation, error) in
            completed(false, nil, "error_parsing_fail".localized())
        }
    }
    
    // MARK - customer recover password
    func recoverPassword(_ customer: String, _ currentLang: String, completed: @escaping (_ result: Bool, _ code: Int?, _ message: String?) -> ()) {
        
        manager.post(
            URL_CUSTOMER_RECOVER_PASSWORD,
            parameters: ["cus_info": customer, "lang" : currentLang],
            progress: nil,
            success: { (requestOperation, responseObject) in
                
                guard let result = Mapper<APIServiceResult>().map(JSONObject: responseObject) else {
                    completed(false, nil, "error_server_fail".localized())
                    return
                }
                completed (true, result.error, result.message)
                
        }) { (requestOperation, error) in
            completed(false, nil,"error_parsing_fail".localized())
        }
    }
    
    // MARK - logout function
    func logOut(completed: @escaping (_ result: Bool, _ message: String?)-> ()) {
        
        manager.post(SERVER_PATH + "/api/login/sign_out", parameters: nil, progress: nil, success: { (requestOperation, responseObject) in
            
            completed (true, nil )
            
        }) { (requestOperation, error) in
            completed(false, nil)
        }
    }
    
    // MARK - get customer by ID
    func getCustomerById(_ customerId: Int, _ authToken: String, completed: @escaping (_ result: Bool, _ message: String?, _ result: CustomerMyPage? )-> ()) {
        manager.post(SERVER_PATH + "/api/cus/getinfo",
                     parameters: ["id": customerId, "auth_token": authToken],
                     progress: nil,
                     success: { (requestOperation, responseObject) in
                        if let results = Mapper<APICustomerByIdResult>().map(JSONObject: responseObject) {
                            
                            if let customer: CustomerMyPage = results.data
                            {
                                completed (true, nil, customer)
                            }else{
                                completed(false, "Can't found customer data", nil)
                            }
                        }
                        else{
                            completed(false, "error_parsing_fail".localized(), nil)
                        }
                        
        }) { (requestOperation, error) in
            
        }
    }
    
    // MARK - get Setting by Code
    func getSettingByCode(_ settingCode: String, _ completed: @escaping (_ result: Bool,_ code: Int?, _ message: String?, _ setting: Setting?) -> ()) {
        
        manager.post(SERVER_PATH + "/api/setting/get_by_code", parameters: ["code": settingCode], progress: nil, success: { (requestOperation, responseObject) in
            
            if let result = Mapper<APISettingResult>().map(JSONObject: responseObject) {
                completed (true, result.error, result.message, result.data)
            }else{
                completed(false, 0,"error_parsing_fail".localized(), nil)
            }
        }) { (requestOperation, error) in
            completed(false, 0, error.localizedDescription, nil)
        }
    }
    
    // MARK - get Company Info
    func getCompanyInfo(completed: @escaping (_ result: Bool, _ message: String?, _ setting: Company?) -> ()) {
        
        manager.post(SERVER_PATH + "/api/comp/getinfo", parameters: nil, progress: nil, success: { (requestOperation, responseObject) in
            if let result = Mapper<APICompanyResult>().map(JSONObject: responseObject) {
                completed (true, nil, result.data)
            }else{
                completed(false, "error_parsing_fail".localized(), nil)
            }
        }) { (requestOperation, error) in
            completed(false, error.localizedDescription, nil)
        }
    }
    
    
    // MARK - update Customer notification
    func updateCustomerNotication(_ param: String, completed: @escaping (_ result: Bool, _ message: String?) -> ()) {
        manager.post(SERVER_PATH + "/api/cus/update", parameters: ["data": param], progress: nil, success: { (requestOperation, responseObject) in
            if let result = Mapper<APIServiceResult>().map(JSONObject: responseObject) {
                completed (result.error == 0, result.message)
            }else{
                completed(false, "error_parsing_fail".localized())
            }
            
        }) { (requestOperation, error) in
            completed(false, error.localizedDescription)
        }
    }
    
    
    
    // MARK - update Customer notification
    func updatePushNotification(_ param: Any?, completed: @escaping (_ result: Bool, _ message: String?) -> ()) {
        
        manager.post(SERVER_PATH + "/api/device/set_notify", parameters: ["data": param!], progress: nil, success: { (requestOperation, responseObject) in
            if let result = Mapper<APIServiceResult>().map(JSONObject: responseObject) {
                completed (result.error == 0, result.message)
            }else{
                completed(false, "error_parsing_fail".localized())
            }
            
        }) { (requestOperation, error) in
            completed(false, error.localizedDescription)
        }
    }
    
    // MARK - update QR point
    func updateQRPoint(_ qrResult: String, _ cusId: Int, _ authToken: String, completed: @escaping (_ result: Bool,_ error: Int?, _ message: String?,_ point: Int?, _ res_point: Int?) -> ()) {
        
        manager.post(SERVER_PATH + "/api/cus_point/use_qr_code",
                     parameters: ["qr_code": qrResult, "id": cusId, "auth_token": authToken],
                     progress: nil,
                     success: { (requestOperation, responseObject) in
                        if let result = Mapper<APIQRUpdateServiceResult>().map(JSONObject: responseObject) {
                            if let point = result.current_point
                            {
                                completed (true,result.error, result.message, point, result.res_point)
                            }else{
                                completed(true, result.error, result.message, nil,nil)
                            }
                            
                        }else{
                            completed(false,nil , "error_parsing_fail".localized(), nil,nil)
                        }
                        
        }) { (requestOperation, error) in
            
            completed(false, nil, error.localizedDescription, nil,nil)
        }
        
    }
    
    // MARK - login
    func login(_ loginParam: Any, completed: @escaping (_ result: Bool, _ message: String?, _ error: Int?, _ customer: Customers?) -> ()) {
        
        manager.post(SERVER_PATH + "/api/login/sign_in", parameters: loginParam, progress: nil, success: { (requestOperation, responseObject) in
            
            if let result = Mapper<APICustomerLoginResult>().map(JSONObject: responseObject) {
                completed (true, result.message, result.error , result.data)
            }else{
                completed(false, "error_parsing_fail".localized(), nil, nil)
            }
            
        }) { (requestOperation, error) in
            completed(false, error.localizedDescription, nil, nil)
        }
    }
    
    // MARK - get area list
    func getAreaList( _ completed: @escaping (_ result: Bool, _ message: String?, _ error: Int?, _ area: [Area]?) -> ()) {
        
        manager.post(SERVER_PATH + "/api/area/getlist", parameters: nil, progress: nil, success: { (requestOperation, responseObject) in
            
            if let result = Mapper<APIAreaListResult>().map(JSONObject: responseObject) {
                completed (true, result.message, result.error, result.data)
            }else{
                completed(false, "error_parsing_fail".localized(), nil, nil)
            }
            
        }) { (requestOperation, error) in
            completed(false, error.localizedDescription, nil, nil)
        }
    }
    
    // MARK - register customer
    func registerCustomer(_ registerParam: Any? , _ completed: @escaping (_ result: Bool, _ message: String?, _ error: Int?) -> ()) {
        
        manager.post(SERVER_PATH + "/api/cus/insert", parameters: ["data": registerParam!], progress: nil, success: { (requestOperation, responseObject) in
            
            if let result = Mapper<APIServiceResult>().map(JSONObject: responseObject) {
                completed (true, result.message, result.error)
            }else{
                completed(false, "error_parsing_fail".localized(), nil)
            }
            
        }) { (requestOperation, error) in
            completed(false, error.localizedDescription, nil)
        }
    }
    
    // MARK - customer update information
    func updateCustomer(_ customer: Customers, _ authToken: String, _ id: Int, completed: @escaping (_ result: Bool, _ code: Int?, _ message: String?, _ customerReturn: CustomerMyPage?) -> ()) {
        
        manager.post(
            URL_CUSTOMER_UPDATE,
            parameters: ["data": customer.toJSONString()!, "auth_token" : authToken, "id": id],
            progress: nil,
            success: { (requestOperation, responseObject) in
                guard let result = Mapper<APICustomerByIdResult>().map(JSONObject: responseObject) else {
                    completed(false, nil, "error_server_fail".localized(), nil)
                    return
                }
                completed (true, result.error, result.message, result.data)
        }) { (requestOperation, error) in
            completed(false, nil, "error_parsing_fail".localized(), nil)
        }
    }
    
    // MARK - get setting list
    func getSettingList(_ limit: Int, _ offset: Int, completed: @escaping (_ result: Bool, _ code: Int?, _ message: String?, _ total: Int, _ setting: [Setting]?) -> ()) {
        
        manager.post(
            URL_SETTING_GET_LIST,
            parameters: ["limit" : limit, "offset" : offset],
            progress: nil,
            success: { (requestOperation, responseObject) in
                guard let result = Mapper<APISettingListResult>().map(JSONObject: responseObject) else {
                    completed(false, nil, "error_server_fail".localized(), 0, nil)
                    return
                }
                completed (true, result.error, result.message, result.total ?? 0, result.data)
        }) { (requestOperation, error) in
            completed(false, nil, "error_parsing_fail".localized(), 0, nil)
        }
        
    }
}
