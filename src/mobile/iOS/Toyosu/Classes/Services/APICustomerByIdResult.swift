//
//  APICustomerByIdResult.swift
//  Toyosu
//
//  Created by nguyen.trung.hau on 10/27/16.
//  Copyright © 2016 Linh Trương. All rights reserved.
//

import Foundation
import ObjectMapper

class APICustomerByIdResult : Mappable {
    
    var message: String?
    var error: Int?
    var data: CustomerMyPage?
    
    required init(map: Map) {
        
    }
    
    func mapping(map: Map) {
        message <- map["message"]
        error <- map["error"]
        data <- map["data"]
    }

}
