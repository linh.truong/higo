//
//  APIGetBookingResult.swift
//  Roann
//
//  Created by Linh Trương on 3/21/17.
//  Copyright © 2017 IBS. All rights reserved.
//

import Foundation
import ObjectMapper

class APIGetBookingResult : APIServiceResult {
    
    var event : [SimplyBookEvent]?
    var unit : [SimplyBookUnit]?
    var additional : [SimplyBookAdditional]?
    var working_date : [SimplyBookWorkingDate]?
    var restaurant : Restaurant?
    
    override func mapping(map: Map) {
        super.mapping(map: map)
        
        event <- map["event"]
        unit <- map["unit"]
        additional <- map["additional"]
        working_date <- map["working_date"]
        restaurant <- map["restaurant"]
    }
}
