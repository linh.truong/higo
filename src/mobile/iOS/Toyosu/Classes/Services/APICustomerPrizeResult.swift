//
//  APICustomerPrizeResult.swift
//  Roann
//
//  Created by Linh Trương on 4/5/17.
//  Copyright © 2017 IBS. All rights reserved.
//

import Foundation
import ObjectMapper

class APICustomerPrizeResult : APIServiceResult {
    
    var data: CustomerPrizes?
    
    override func mapping(map: Map) {
        super.mapping(map: map)
        
        data <- map["data"]
    }
}
