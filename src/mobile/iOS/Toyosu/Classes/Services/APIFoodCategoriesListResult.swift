//
//  APIFoodCategoriesListResult.swift
//  Toyosu
//
//  Created by Linh Trương on 10/19/16.
//  Copyright © 2016 Linh Trương. All rights reserved.
//

import Foundation
import ObjectMapper

class APIFoodCategoriesListResult : APIServiceResult {
    var data: [FoodCategories]?
    
    override func mapping(map: Map) {
        super.mapping(map: map)
        data <- map["data"]
    }
}
