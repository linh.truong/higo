//
//  APICategoriesListResult.swift
//  Toyosu
//
//  Created by Linh Trương on 10/18/16.
//  Copyright © 2016 Linh Trương. All rights reserved.
//

import Foundation
import ObjectMapper

class APICategoriesListResult : APIServiceResult {
    var data: [TopicCategories]?
    
    override func mapping(map: Map) {
        super.mapping(map: map)
        data <- map["data"]
    }
}
