//
//  APIUpdateCustomerInfo.swift
//  Toyosu
//
//  Created by Linh Trương on 10/28/16.
//  Copyright © 2016 Linh Trương. All rights reserved.
//

import Foundation
import ObjectMapper

class APIUpdateCustomerInfo : APIServiceResult {
    var data: Customers?
    
    override func mapping(map: Map) {
        data <- map["data"]
    }
}
