//
//  APIFoodByCategoriesResult.swift
//  Toyosu
//
//  Created by Linh Trương on 10/19/16.
//  Copyright © 2016 Linh Trương. All rights reserved.
//

import Foundation
import ObjectMapper

class APIFoodByCategoriesResult : APIServiceResult {
    var data: [String: [Food]]?
    var total: Int?
    
    override func mapping(map: Map) {
        super.mapping(map: map)
        
        data <- map["data"]
        total <- map["total"]
    }
}
