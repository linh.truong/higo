//
//  APICommonResult.swift
//  Toyosu
//
//  Created by Linh Trương on 11/14/16.
//  Copyright © 2016 Linh Trương. All rights reserved.
//

import Foundation
import ObjectMapper

class APICommonResult : APIServiceResult {
    var data: Common?
    
    override func mapping(map: Map) {
        super.mapping(map: map)
        data <- map["data"]
    }
}
