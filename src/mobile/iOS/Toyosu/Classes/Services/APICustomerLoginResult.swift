//
//  APICustomerLoginResult.swift
//  Roann
//
//  Created by Linh Trương on 3/14/17.
//  Copyright © 2017 IBS. All rights reserved.
//

import Foundation
import ObjectMapper

class APICustomerLoginResult : Mappable {
    
    var message: String?
    var error: Int?
    var data: Customers?
    
    required init(map: Map) {
        
    }
    
    func mapping(map: Map) {
        message <- map["message"]
        error <- map["error"]
        data <- map["data"]
    }
    
    
}
