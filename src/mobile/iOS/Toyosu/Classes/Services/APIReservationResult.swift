//
//  APIReservationResult.swift
//  Toyosu
//
//  Created by Linh Trương on 11/4/16.
//  Copyright © 2016 Linh Trương. All rights reserved.
//

import Foundation
import ObjectMapper

class APIReservationResult : APIServiceResult {
    var data: Reservation?
    
    override func mapping(map: Map) {
        super.mapping(map: map)
        data <- map["data"]
    }
}
