//
//  APIBookingTimeResult.swift
//  Roann
//
//  Created by Linh Trương on 3/21/17.
//  Copyright © 2017 IBS. All rights reserved.
//

import Foundation
import ObjectMapper

class APIBookingTimeResult : APIServiceResult {
    
    var data : [SimplyBookTime]?
    
    override func mapping(map: Map) {
        super.mapping(map: map)
        
        data <- map["data"]
    }
}
