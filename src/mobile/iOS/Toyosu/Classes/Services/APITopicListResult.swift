//
//  APITopicListResult.swift
//  Toyosu
//
//  Created by Linh Trương on 12/5/16.
//  Copyright © 2016 IBS. All rights reserved.
//

import Foundation
import ObjectMapper

class APITopicListResult : APIServiceResult {
    var data: [Topic]?
    var total: Int?
    
    override func mapping(map: Map) {
        super.mapping(map: map)
        
        data <- map["data"]
        total <- map["total"]
    }
}
