//
//  APICompanyResult.swift
//  Toyosu
//
//  Created by nguyen.trung.hau on 11/4/16.
//  Copyright © 2016 Linh Trương. All rights reserved.
//

import Foundation
import ObjectMapper

class APICompanyResult : APIServiceResult {
    var data: Company?
    
    override func mapping(map: Map) {
        super.mapping(map: map)
        data <- map["data"]
    }
}
