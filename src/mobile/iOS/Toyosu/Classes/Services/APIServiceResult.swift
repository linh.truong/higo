//
//  APIServiceResult.swift
//  Toyosu
//
//  Created by Linh Trương on 10/18/16.
//  Copyright © 2016 Linh Trương. All rights reserved.
//

import Foundation
import ObjectMapper

class APIServiceResult : Mappable {
    
    var message: String!
    var error: Int!
    
    required init(map: Map) {
        
    }
    
    func mapping(map: Map) {
        message <- map["message"]
        error <- map["error"]
    }
}
