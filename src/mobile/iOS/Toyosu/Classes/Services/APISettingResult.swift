//
//  APISettingResult.swift
//  Toyosu
//
//  Created by nguyen.trung.hau on 10/31/16.
//  Copyright © 2016 Linh Trương. All rights reserved.
//

import Foundation
import ObjectMapper

class APISettingResult : APIServiceResult {
    var data: Setting?
    
    override func mapping(map: Map) {
        super.mapping(map: map)
        
        data <- map["data"]
    }
}
