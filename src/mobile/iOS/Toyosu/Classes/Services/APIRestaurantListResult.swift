//
//  APIRestaurantListResult.swift
//  Toyosu
//
//  Created by Linh Trương on 1/20/17.
//  Copyright © 2017 IBS. All rights reserved.
//

import Foundation
import ObjectMapper

class APIRestaurantListResult : APIServiceResult {
    var data: [Restaurant]?
    var total: Int?
    
    override func mapping(map: Map) {
        super.mapping(map: map)
        
        data <- map["data"]
        total <- map["total"]
    }
}
