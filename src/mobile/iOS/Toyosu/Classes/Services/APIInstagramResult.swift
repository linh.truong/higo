//
//  APIInstagramResult.swift
//  Roann
//
//  Created by Linh Trương on 4/18/17.
//  Copyright © 2017 IBS. All rights reserved.
//

import Foundation
import ObjectMapper

class APIInstagramResult : APIServiceResult {
    var data: [InstagramModel]?
    var pagination: InstagramPaginationModel?
    
    override func mapping(map: Map) {
        super.mapping(map: map)
        
        data <- map["data"]
        pagination <- map["pagination"]
    }
}

