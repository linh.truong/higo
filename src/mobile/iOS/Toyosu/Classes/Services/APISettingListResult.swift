//
//  APISettingListResult.swift
//  Roann
//
//  Created by Linh Trương on 4/10/17.
//  Copyright © 2017 IBS. All rights reserved.
//

import Foundation
import ObjectMapper

class APISettingListResult : APIServiceResult {
    var data: [Setting]?
    var total: Int?
    
    override func mapping(map: Map) {
        super.mapping(map: map)
        
        data <- map["data"]
        total <- map["total"]
    }
}
