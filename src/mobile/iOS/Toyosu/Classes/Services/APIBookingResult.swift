//
//  APIBookingResult.swift
//  Roann
//
//  Created by Linh Trương on 3/20/17.
//  Copyright © 2017 IBS. All rights reserved.
//

import Foundation
import ObjectMapper

class APIBookingResult : APIServiceResult {
    
    var data : Reservation?
    
    override func mapping(map: Map) {
        super.mapping(map: map)
        
        data <- map["data"]
    }
}
