//
//  APIRelatedItemsResult.swift
//  Toyosu
//
//  Created by Linh Truong on 1/13/17.
//  Copyright © 2017 IBS. All rights reserved.
//

import Foundation
import ObjectMapper

class APIRelatedItemsResult : APIServiceResult {
    var data: [CustomRelatedItems]?
    
    override func mapping(map: Map) {
        super.mapping(map: map)
        
        data <- map["data"]
    }
}
