//
//  APIPrizeListResult.swift
//  Toyosu
//
//  Created by Linh Trương on 11/10/16.
//  Copyright © 2016 Linh Trương. All rights reserved.
//

import Foundation
import ObjectMapper

class APIPrizeListResult : APIServiceResult {
    var data: [Prize]?
    var total: Int?
    
    override func mapping(map: Map) {
        super.mapping(map: map)
        
        data <- map["data"]
        total <- map["total"]
    }
}
