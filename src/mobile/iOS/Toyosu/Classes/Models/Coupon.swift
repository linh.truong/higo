//
//  Coupon.swift
//  Toyosu
//
//  Created by Phu on 10/7/16.
//  Copyright © 2016 Linh Trương. All rights reserved.
//

import Foundation
import ObjectMapper

typealias CouponDictionary = [String : AnyObject]

class Coupon: NSObject, Mappable
{
    // Variable
    var id: Int?
    var restaurant_id: Int?
    var number: String?
    var name: String?
    var name_cn: String?
    var title: String?
    var title_cn: String?
    var price_off: Int?
    var price_minus: Int?
    var from_date: Date?
    var to_date: Date?
    var start_time: String?
    var end_time: String?
    var week_days: String?
    var current_register: Int?
    var quantity: Int?
    var image_path: String?
    var image_width: Int?
    var image_height: Int?
    var coupon_type: Int?
    var comment: String?
    var comment_cn: String?
    var condition: String?
    var condition_cn: String?
    var remain_day: Int?
    var hash_tags : String?
    
    override init () {}
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        id <- map["id"]
        restaurant_id <- map["restaurant"]
        number <- map["number"]
        title <- map["title"]
        title_cn <- map["title_cn"]
        name <- map["name"]
        name_cn <- map["name_cn"]
        price_off <- map["price_off"]
        price_minus <- map["price_minus"]
        quantity <- map["quantity"]
        image_path <- map["image_path"]
        image_width <- map["image_width"]
        image_height <- map["image_height"]
        coupon_type <- map["coupon_type"]
        from_date <- (map["from_date"], ISO8601DateTransform())
        to_date <- (map["to_date"], ISO8601DateTransform())
        start_time <- map["start_time"]
        end_time <- map["end_time"]
        comment <- map["comment"]
        comment_cn <- map["comment_cn"]
        condition <- map["condition"]
        condition_cn <- map["condition_cn"]
        remain_day <- map["remain_day"]
        hash_tags <- map["hash_tags"]
    }
}
