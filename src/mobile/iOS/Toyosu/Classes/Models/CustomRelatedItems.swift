//
//  CustomRelatedItems.swift
//  Toyosu
//
//  Created by Linh Truong on 1/13/17.
//  Copyright © 2017 IBS. All rights reserved.
//

import Foundation
import ObjectMapper

typealias CustomRelatedItemsDictionary = [String : AnyObject]

enum ObjectType: Int {
    case CouponType = 1
    case FoodType = 2
    case TopicType = 3
}

class CustomRelatedItems: NSObject, Mappable
{
    // Shared variable
    var type: ObjectType?
    var id: Int?
    var object_type: Int?
    var hash_tags: String?
    var restaurant_id: Int?
    var image_path: String?
    var image_width: Int?
    var image_height: Int?
    var name: String?
    var name_cn: String?
    // Coupon section
    var title: String?
    var title_cn: String?
    var to_date: Date?
    var end_time: String?
    var price_off: Int?
    var price_minus: Int?
    var condition: String?
    var condition_cn: String?
    // Food section
    var price: String?
    var information: String?
    var information_cn: String?
    // Topic section
    var res_image_path: String?
    var res_name: String?
    var res_name_cn: String?
    var subject: String?
    var subject_cn: String?
    var body: String?
    var body_cn: String?
    var open_date: Date?
    
    let topic = Topic()
    let food = Food()
    var coupon = Coupon()
    
    let objectTypeTransform = TransformOf<ObjectType, Int>(fromJSON: { (value: Int?) -> ObjectType? in
        if let val = value {
            return ObjectType(rawValue: val)
        }
        return nil
    }, toJSON: { (value: ObjectType?) -> Int? in
        // transform value from Int? to String?
        if let val = value {
            return val.rawValue
        }
        return nil
    })
    
    override init () {}
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        // Shared variable
        type <- (map["object_type"], objectTypeTransform)
        id <- map["id"]
        object_type <- map["object_type"]
        hash_tags <- map["hash_tags"]
        restaurant_id <- map["restaurant_id"]
        image_path <- map["image_path"]
        image_width <- map["image_width"]
        image_height <- map["image_height"]
        name <- map["name"]
        name_cn <- map["name_cn"]
        // Coupon section
        title <- map["title"]
        title_cn <- map["title_cn"]
        to_date <- (map["to_date"], ISO8601DateTransform())
        end_time <- map["end_time"]
        price_off <- map["price_off"]
        price_minus <- map["price_minus"]
        condition <- map["condition"]
        condition_cn <- map["condition_cn"]
        // Food section
        price <- map["price"]
        information <- map["description"]
        information_cn <- map["description_cn"]
        // Topic section
        res_image_path <- map["res_image_path"]
        res_name <- map["res_name"]
        res_name_cn <- map["res_name_cn"]
        subject <- map["subject"]
        subject_cn <- map["subject_cn"]
        body <- map["body"]
        body_cn <- map["body_cn"]
        open_date <- (map["open_date"], ISO8601DateTransform())
    }
    
    func toDetail() -> AnyObject? {
        if let t = type {
            switch t {
            case .CouponType:
                return self.toCoupon()
            case .TopicType:
                return self.toTopic()
            case .FoodType:
                return self.toFood()
            }
        }
        return nil
    }
    
    func toCoupon() -> Coupon {
        
        coupon.hash_tags = hash_tags
        coupon.restaurant_id = restaurant_id
        coupon.id = id
        coupon.image_path = image_path
        coupon.image_width = image_width
        coupon.image_height = image_height
        coupon.name = name
        coupon.name_cn = name_cn
        coupon.title = title
        coupon.title_cn = title_cn
        coupon.to_date = to_date
        coupon.end_time = end_time
        coupon.price_off = price_off
        coupon.price_minus = price_minus
        coupon.condition = condition
        coupon.condition_cn = condition_cn
        
        return coupon
    }
    
    func toFood() -> Food {
        
        food.hash_tags = hash_tags
        food.restaurant_id = restaurant_id
        food.id = id
        food.image_path = image_path
        food.image_width = image_width
        food.image_height = image_height
        food.name = name
        food.name_cn = name_cn
        food.price = price
        food.information = information
        food.information_cn = information_cn
        
        return food
    }
    
    func toTopic() -> Topic {
        
        topic.hash_tags = hash_tags
        topic.restaurant_id = restaurant_id
        topic.id = id
        topic.image_path = image_path
        topic.image_width = image_width
        topic.image_height = image_height
        topic.res_avatar = res_image_path
        topic.res_name = res_name
        topic.res_name_cn =  res_name_cn
        topic.subject = subject
        topic.subject_cn = subject_cn
        topic.body = body
        topic.body_cn = body_cn
        topic.open_date = open_date
        
        return topic
    }
}
