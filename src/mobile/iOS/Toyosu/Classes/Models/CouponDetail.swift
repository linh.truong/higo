//
//  CouponDetail.swift
//  Toyosu
//
//  Created by Linh Trương on 10/7/16.
//  Copyright © 2016 Linh Trương. All rights reserved.
//

import Foundation
import ObjectMapper

typealias CouponDetailDictionary = [String : AnyObject]

class CouponDetail: NSObject, Mappable {
    
    // Variable
    var id: Int?
    var coupon: Int?
    var food: Int?
    var quantity: Int?
    
    override init () {}
    
    convenience init(id: Int, cpnId: Int, foodId: Int, quantity: Int) {
        self.init()
        self.id = id
        self.coupon = cpnId
        self.food = foodId
        self.quantity = quantity
    }
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        id <- map["id"]
        coupon <- map["coupon"]
        food <- map["food"]
        quantity <- map["quantity"]
    }
    
}
