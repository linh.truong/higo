//
//  FoodCategories.swift
//  Toyosu
//
//  Created by Linh Trương on 10/7/16.
//  Copyright © 2016 Linh Trương. All rights reserved.
//

import Foundation
import ObjectMapper

typealias FoodCategoriesDictionary = [String : AnyObject]

class FoodCategories: NSObject, Mappable {
    var id: Int?
    var name: String?
    var name_cn: String?
    var foods: [Food]?
    
    override init() {}
    
    required convenience init?(map:Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        id <- map["id"]
        name <- map["name"]
        name_cn <- map["name_cn"]
        foods <- map["foods"]
    }
}
