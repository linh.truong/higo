
//
//  Customers.swift
//  Toyosu
//
//  Created by nguyen.trung.hau on 10/17/16.
//  Copyright © 2016 Linh Trương. All rights reserved.
//

import Foundation
import ObjectMapper

class Customers: Mappable
{
    // Variable
    var id: Int?
    var area: Int?
    var number: Int?
    var name: String?
    var restaurant: Int?
    var image_path: String?
    var age: Int?
    var birthday: Date?
    var sex: Int?
    var address: String?
    var gps_data: String?
    var email: String?
    var login_password: String?
    var phone: String?
    var current_point: Int?
    var total_point: Int?
    var is_mail_notify: Int?
    var is_sms_notify: Int?
    var recover_password: String?
    var last_transaction_date: Date?
    var auth_token: String?
    var level_up_date: Date?
    var num_visit: Int?
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    convenience init(id: Int?, area: Int?, restaurant: Int?, number: Int?, name: String?, image_path: String?, age: Int?, birthday: Date?, sex: Int?, address: String?, gps_data: String?, email: String?, login_password: String?, phone: String?, last_transaction_date: Date?, current_point: Int? = 0, total_point: Int? = 0, is_mail_notify: Int? = 1, is_sms_notify: Int? = 1, recover_password: String?)
    {
        self.init()
        self.area = area
        self.id = id
        self.restaurant = restaurant
        self.number = number
        self.name = name
        self.image_path = image_path
        self.age = age
        self.birthday = birthday
        self.sex = sex
        self.address = address
        self.gps_data = gps_data
        self.email = email
        self.login_password = login_password
        self.phone = phone
        self.last_transaction_date = last_transaction_date
        self.current_point = current_point
        self.total_point = total_point
        self.is_mail_notify = is_mail_notify
        self.is_sms_notify = is_sms_notify
        self.recover_password = recover_password
    }
    
    func mapping(map: Map) {
        id    <- map["id"]
        area    <- map["area"]
        restaurant    <- map["restaurant"]
        image_path    <- map["image_path"]
        age    <- map["age"]
        birthday <- (map["birthday"], ISO8601DateTransform())
        number <- map["number"]
        name      <- map["name"]
        sex  <- map["sex"]
        address  <- map["address"]
        gps_data     <- map["gps_data"]
        email    <-  map["email"]
        login_password    <-  map["login_password"]
        phone    <-  map["phone"]
        current_point <- map["current_point"]
        total_point <- map["total_point"]
        is_sms_notify   <-  map["is_sms_notify"]
        is_mail_notify   <-  map["is_mail_notify"]
        recover_password    <-  map["recover_password"]
        last_transaction_date <- (map["last_transaction_date"], ISO8601DateTransform())
        auth_token    <-  map["auth_token"]
        level_up_date <- (map["level_up_date"], ISO8601DateTransform())
        num_visit    <-  map["num_visit"]
    }
    
}
