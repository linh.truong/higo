//
//  InstagramPaginationModel.swift
//  Roann
//
//  Created by Linh Trương on 4/18/17.
//  Copyright © 2017 IBS. All rights reserved.
//

import Foundation
import ObjectMapper

class InstagramPaginationModel: NSObject, Mappable
{
    // Variable
    var next_url: String?
    var next_max_id: String?
    
    override init () {}
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        next_url <- map["next_url"]
        next_max_id <- map["next_max_id"]
    }
}
