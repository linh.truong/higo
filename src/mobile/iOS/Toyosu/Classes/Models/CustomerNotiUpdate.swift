//
//  Customers.swift
//  Toyosu
//
//  Created by nguyen.trung.hau on 10/17/16.
//  Copyright © 2016 Linh Trương. All rights reserved.
//

import Foundation
import ObjectMapper

typealias CustomersNotiUpdateDictionary = [String : AnyObject]


class CustomersNotiUpdate:  NSObject, Mappable
{

    var id: Int?
    var is_mail_notify: Int?
    var is_sms_notify:Int?
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    override init() {
        
    }
    
    convenience init(id: Int? ,is_mail_notify: Int?, is_sms_notify: Int?)
    {
        self.init()
        self.id = id
        self.is_mail_notify = is_mail_notify
        self.is_sms_notify = is_sms_notify
        
    }
    
    convenience init(id: Int? ,is_mail_notify: Int?)
    {
        self.init()
        self.id = id
        self.is_mail_notify = is_mail_notify
    }
    
    convenience init(id: Int? ,is_sms_notify: Int?)
    {
        self.init()
        self.id = id
        self.is_sms_notify = is_sms_notify
    }
    func mapping(map: Map) {
        id    <- map["id"]
        is_mail_notify    <- map["is_mail_notify"]
        is_sms_notify    <- map["is_sms_notify"]
    }
}




