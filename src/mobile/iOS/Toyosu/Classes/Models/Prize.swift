//
//  Prize.swift
//  Toyosu
//
//  Created by Linh Trương on 11/10/16.
//  Copyright © 2016 IBS. All rights reserved.
//

import Foundation
import ObjectMapper

typealias PrizeDictionary = [String : AnyObject]

class Prize: NSObject, Mappable
{
    // Variable
    var id: Int?
    var restaurant_id: Int?
    var request_point: Int?
    var title: String?
    var title_cn: String?
    var content: String?
    var content_cn: String?
    var condition: String?
    var condition_cn: String?
    var from_date: Date?
    var to_date: Date?
    var start_time: String?
    var end_time: String?
    var week_days: String?
    var image_path: String?
    var image_width: Int?
    var image_height: Int?
    var link_address: String?
    var current_register: Int?
    var quantity: Int?
    var delivery_type: Int?
    var delivery_duration_days: Int?
    
    override init () {}
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        id <- map["id"]
        restaurant_id <- map["restaurant_id"]
        request_point <- map["request_point"]
        title <- map["title"]
        title_cn <- map["title_cn"]
        content <- map["content"]
        content_cn <- map["content_cn"]
        condition <- map["condition"]
        condition_cn <- map["condition_cn"]
        from_date <- (map["from_date"], ISO8601DateTransform())
        to_date <- (map["to_date"], ISO8601DateTransform())
        start_time <- map["start_time"]
        end_time <- map["end_time"]
        week_days <- map["week_days"]
        image_path <- map["image_path"]
        image_width <- map["image_width"]
        image_height <- map["image_height"]
        link_address <- map["link_address"]
        current_register <- map["current_register"]
        quantity <- map["quantity"]
        delivery_type <- map["delivery_type"]
        delivery_duration_days <- map["delivery_duration_days"]
    }
}
