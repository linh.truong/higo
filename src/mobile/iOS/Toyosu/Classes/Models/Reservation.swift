//
//  Reservation.swift
//  Toyosu
//
//  Created by Linh Trương on 11/3/17.
//  Copyright © 2017 IBS. All rights reserved.
//

import Foundation
import ObjectMapper

typealias ReservationDictionary = [String : AnyObject]

class Reservation: NSObject, Mappable
{
    // Variable
    var id: Int?
    var booking_id: Int?
    var customer_id: Int?
    var client_id: Int?
    var event_id: Int?
    var unit_id: Int?
    var status_id: Int?
    var start_date_time: Date?
    var end_date_time: Date?
    var number_of_seat: Int?
    var reserve_hash: String?
    var code: String?
    var is_confirmed: Int?
    
    override init () {}
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        id <- map["id"]
        booking_id <- map["booking_id"]
        customer_id <- map["customer_id"]
        client_id <- map["client_id"]
        event_id <- map["event_id"]
        unit_id <- map["unit_id"]
        status_id <- map["status_id"]
        start_date_time <- (map["start_date_time"], ISO8601DateTransform())
        end_date_time <- (map["end_date_time"], ISO8601DateTransform())
        number_of_seat <- map["number_of_seat"]
        reserve_hash <- map["hash"]
        code <- map["code"]
        is_confirmed <- map["is_confirmed"]
    }
}
