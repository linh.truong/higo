//
//  Food.swift
//  Toyosu
//
//  Created by Phu on 10/7/16.
//  Copyright © 2016 Linh Trương. All rights reserved.
//

import Foundation
import ObjectMapper

typealias FoodDictionary = [String : AnyObject]

class Food: NSObject, Mappable {
    var id: Int?
    var restaurant_id: Int?
    var category_id: Int?
    var type_id: Int?
    var name: String?
    var name_cn: String?
    var start_time: String?
    var end_time: String?
    var current_order: Int?
    var quantity_per_day: Int?
    var week_days: String?
    var image_path: String?
    var image_width: Int?
    var image_height: Int?
    var price: String?
    var information : String?
    var information_cn : String?
    var hash_tags : String?
    
    override init () {}
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        id <- map["id"]
        category_id <- map["category"]
        name <- map["name"]
        name_cn <- map["name_cn"]
        start_time <- map["start_time"]
        end_time <- map["end_time"]
        current_order <- map["current_order"]
        quantity_per_day <- map["quantity_per_day"]
        week_days <- map["week_days"]
        image_path <- map["image_path"]
        image_width <- map["image_width"]
        image_height <- map["image_height"]
        price <- map["price"]
        information <- map["description"]
        information_cn <- map["description_cn"]
        hash_tags <- map["hash_tags"]
    }
}
