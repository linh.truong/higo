//
//  CustomerCoupons.swift
//  Roann
//
//  Created by Linh Trương on 4/24/17.
//  Copyright © 2017 IBS. All rights reserved.
//

import Foundation
import ObjectMapper

class CustomerCoupons: Mappable {
    
    var id: Int?
    var customer: Int?
    var coupon: Int?
    var register_date: Date?
    var use_date: Date?
    var status: Int?
    var description: String?
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    convenience init(id: Int?, customer: Int?, coupon: Int?, register_date: Date?, use_date: Date?, status: Int?, description: String?) {
        self.init()
        self.id = id
        self.customer = customer
        self.coupon = coupon
        self.register_date = register_date
        self.use_date = use_date
        self.status = status
        self.description = description
    }
    
    func mapping(map: Map) {
        id <- map["id"]
        customer <- map["customer"]
        coupon <- map["coupon"]
        register_date <- (map["register_date"], ISO8601DateTransform())
        use_date <- (map["use_date"], ISO8601DateTransform())
        status <- map["status"]
        description <- map["description"]
    }
    
}
