//
//  TopicCategories.swift
//  Toyosu
//
//  Created by Linh Trương on 10/6/16.
//  Copyright © 2016 Linh Trương. All rights reserved.
//

import Foundation
import ObjectMapper

typealias TopicCategoriesDictionary = [String : AnyObject]

class TopicCategories: NSObject, Mappable
{
    // Variable
    var id: Int?
    var code: String?
    var name: String?
    var name_cn: String?
    var topics: [Topic]?
    
    override init () {}
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        id <- map["id"]
        name <- map["name"]
        name_cn <- map["name_cn"]
        topics <- map["topics"]
    }
}
