//
//  Restaurant.swift
//  Toyosu
//
//  Created by nguyen.trung.hau on 10/6/16.
//  Updated by Linh Trương on 12/19/16.
//  Copyright © 2016 IBS. All rights reserved.
//

import Foundation
import MapKit
import Foundation
import ObjectMapper

typealias RestaurantDictionary = [String : AnyObject]

class Restaurant: NSObject, Mappable
{
    var id: Int?
    var area: Int?
    var station: Int?
    var code: String?
    var sb_company_login: String?
    var sb_username: String?
    var sb_user_password: String?
    var sb_api_key: String?
    var name: String?
    var name_cn: String?
    var image_path: String?
    var contact_person: String?
    var email: String?
    var phone: String?
    var address: String?
    var address_cn: String?
    var information : String?
    var information_cn : String?
    var google_map_url: String?
    var week_days: String?
    var start_time: String?
    var end_time: String?
    var limit_days_book: Int?
    var confirm_reserve: String?
    var confirm_reserve_cn: String?
    var longitude: String?
    var latitude: String?
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    override init() {
    
    }
    
    func mapping(map: Map) {
        id    <- map["id"]
        area    <- map["area"]
        station <- map["station"]
        code    <- map["code"]
        sb_company_login    <- map["sb_company_login"]
        sb_username    <- map["sb_username"]
        sb_user_password    <- map["sb_user_password"]
        sb_api_key    <- map["sb_api_key"]
        name    <- map["name"]
        name_cn <- map["name_cn"]
        image_path  <-  map["image_path"]
        contact_person  <- map["contact_person"]
        email   <- map["email"]
        phone   <- map["phone"]
        address <- map["address"]
        address_cn  <-  map["address_cn"]
        information <-  map["description"]
        information_cn  <-  map["description_cn"]
        google_map_url  <-  map["google_map_url"]
        week_days <- map["week_days"]
        start_time <- map["start_time"]
        end_time <- map["end_time"]
        limit_days_book <- map["limit_days_book"]
        confirm_reserve <- map["confirm_reserve"]
        confirm_reserve_cn <- map["confirm_reserve_cn"]
        longitude <- map["longitude"]
        latitude <- map["latitude"]
    }
}
