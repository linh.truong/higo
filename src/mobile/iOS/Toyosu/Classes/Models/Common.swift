//
//  Common.swift
//  Toyosu
//
//  Created by Linh Trương on 11/14/16.
//  Copyright © 2016 Linh Trương. All rights reserved.
//

import Foundation
import ObjectMapper

typealias CommonDictionary = [String : AnyObject]

class Common: NSObject, Mappable
{
    // Variable
    var remain_point: Int?
    
    override init () {
        
    }
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        remain_point <- map["remain_point"]
    }
}
