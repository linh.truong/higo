//
//  FoodTypes.swift
//  Toyosu
//
//  Created by Phu on 10/7/16.
//  Copyright © 2016 Linh Trương. All rights reserved.
//

import Foundation

class FoodTypes{
    var id: Int?
    var name: String?
    var name_cn: String?
    
    init(id: Int, name: String, name_cn: String) {
        self.id = id
        self.name = name
        self.name_cn = name_cn
    }

}
