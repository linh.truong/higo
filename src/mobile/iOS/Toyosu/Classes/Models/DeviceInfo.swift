//
//  DeviceInfo.swift
//  Toyosu
//
//  Created by nguyen.trung.hau on 11/15/16.
//  Copyright © 2016 IBS. All rights reserved.
//


import Foundation
import ObjectMapper

typealias DeviceInfoDictionary = [String : AnyObject]

class DeviceInfo: NSObject, Mappable
{
    // Variable
    var customer_id: Int?
    var device_token: String?
    var type: Int?
    var is_push_notify: Int?
    
    override init () {}
    
    convenience init(customer_id: Int? ,device_token: String?, type: Int?, is_push_notify: Int?)
    {
        self.init()
        self.customer_id = customer_id
        self.device_token = device_token
        self.type = type
        self.is_push_notify = is_push_notify
        
    }
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        customer_id <- map["customer_id"]
        device_token <- map["device_token"]
        type <- map["type"]
        is_push_notify <- map["is_push_notify"]
    }
}
