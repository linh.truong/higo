//
//  Topic.swift
//  Toyosu
//
//  Created by Linh Trương on 10/6/16.
//  Copyright © 2016 IBS. All rights reserved.
//

import Foundation
import ObjectMapper

typealias SettingDictionary = [String : AnyObject]

class Setting: NSObject, Mappable
{
    // Variable
    var key: String?
    var value: String?
    var value_cn: String?
    var information: String?
    
    override init () {}
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        key <- map["code"]
        value <- map["value"]
        value_cn <- map["value_cn"]
        information <- map["description"]
    }
}
