//
//  Topic.swift
//  Toyosu
//
//  Created by Linh Trương on 10/6/16.
//  Copyright © 2016 Linh Trương. All rights reserved.
//

import Foundation
import ObjectMapper

typealias TopicDictionary = [String : AnyObject]

class Topic: NSObject, Mappable
{
    // Variable
    var id: Int?
    var category_id: Int?
    var restaurant_id: Int?
    var res_avatar: String?
    var res_name: String?
    var res_name_cn: String?
    var subject: String?
    var subject_cn: String?
    var body: String?
    var body_cn: String?
    var image_path: String?
    var image_width: Int?
    var image_height: Int?
    var link_address: String?
    var open_date: Date?
    var close_date: Date?
    var num_like: Int?
    var num_share: Int?
    var hash_tags: String?
    
    override init () {}
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        id <- map["id"]
        restaurant_id <- map["restaurant"]
        res_avatar <- map["res_avatar"]
        res_name <- map["res_name"]
        res_name_cn <- map["res_name_cn"]
        category_id <- map["category"]
        subject <- map["subject"]
        subject_cn <- map["subject_cn"]
        body <- map["body"]
        body_cn <- map["body_cn"]
        image_path <- map["image_path"]
        image_width <- map["image_width"]
        image_height <- map["image_height"]
        link_address <- map["link_address"]
        open_date <- (map["open_date"], ISO8601DateTransform())
        close_date <- (map["close_date"], ISO8601DateTransform())
        num_like <- map["num_like"]
        num_share <- map["num_share"]
        hash_tags <- map["hash_tags"]
    }
}
