//
//  Company.swift
//  Toyosu
//
//  Created by nguyen.trung.hau on 11/3/16.
//  Updated by Linh Trương on 12/19/16.
//  Copyright © 2016 IBS. All rights reserved.
//

import Foundation
import ObjectMapper

class Company: NSObject, Mappable
{
    var company_map_url: String?
    var company_description	: String?
    var company_description_cn : String?
    var company_email: String?
    var company_image_path: String?
    var company_phone: String?
    var company_address: String?
    var company_name: String?
    var company_address_cn: String?
    var company_name_cn: String?
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    override init() {}
    
    func mapping(map: Map) {
        company_map_url <- map["company_map_url"]
        company_description <- map["company_description"]
        company_description_cn <- map["company_description_cn"]
        company_email <- map["company_email"]
        company_image_path <- map["company_image_path"]
        company_phone <- map["company_phone"]
        company_address <- map["company_address"]
        company_name <- map["company_name"]
        company_address_cn <- map["company_address_cn"]
        company_name_cn <- map["company_name_cn"]
    }
    
}
