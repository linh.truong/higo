//
//  Area.swift
//  Toyosu
//
//  Created by nguyen.trung.hau on 10/17/16.
//  Copyright © 2016 Linh Trương. All rights reserved.
//

import Foundation
import ObjectMapper


class Area:  NSObject, Mappable{
    
    var id: Int?
    var name: String?
    var name_cn: String?
    var is_delete: Int?
    
    init(id: Int, name: String?, name_cn: String?, is_delete: Int) {
        self.id = id
        self.name = name
        self.name_cn = name_cn
    }
    
    override init () {}
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        id <- map["id"]
        name <- map["name"]
        name_cn <- map["name_cn"]
    }
}
