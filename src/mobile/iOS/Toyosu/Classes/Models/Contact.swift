//
//  Contact.swift
//  Toyosu
//
//  Created by Linh Trương on 11/2/16.
//  Copyright © 2016 IBS. All rights reserved.
//

import Foundation
import ObjectMapper

typealias ContactDictionary = [String : AnyObject]

class Contact: NSObject, Mappable
{
    // Variable
    var id: Int?
    var restaurant_id: Int?
    var name: String?
    var email: String?
    var phone: String?
    var content: String?
    var status: Int?
    
    override init () {}
    
    required convenience init?(map: Map) {
        self.init()
    }
 
    convenience init(restaurant_id: Int, name: String?,  email: String?, phone: String?, content: String?, status: Int?)
    {
        self.init()
        self.restaurant_id = restaurant_id
        self.name = name
        self.email = email
        self.phone = phone
        self.content = content
        self.status = status
    }
    
    func mapping(map: Map) {
        id <- map["id"]
        restaurant_id <- map["restaurant"]
        name <- map["name"]
        email <- map["email"]
        phone <- map["phone"]
        content <- map["content"]
        status <- map["status"]
       
    }
}
