//
//  CustomerPoints.swift
//  Toyosu
//
//  Created by nguyen.trung.hau on 11/16/16.
//  Copyright © 2016 Linh Trương. All rights reserved.
//


import Foundation
import ObjectMapper


class CustomerQR: NSObject, Mappable
{
    // Variable
    var qr_code: String?
  

    
    override init () {
        
    }
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    convenience init(qrCode: String? )
    {
        self.init()
        self.qr_code = qrCode

    }
    
    func mapping(map: Map) {
        qr_code <- map["qr_code"]
    }
}
