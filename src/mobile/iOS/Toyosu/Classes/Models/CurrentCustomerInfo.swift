//
//  CurrentCustomerInfo.swift
//  Toyosu
//
//  Created by Linh Trương on 10/27/16.
//  Copyright © 2016 Linh Trương. All rights reserved.
//

import Foundation
import ObjectMapper

class CurrentCustomerInfo {
    
    static let shareInfo = CurrentCustomerInfo()
    var customer: Customers?
    var settingArr: [Setting]?
    
    func persistenceCurrentInfo() {
        LocalService.saveUserInfo(customer)
    }
    
    func saveCustomerInfo(_ cusLogin: Customers?) {
        LocalService.saveUserInfo(cusLogin)
    }
    
    func loadCustomerInfo() -> Customers? {
        return LocalService.loadSavedUserInfo()
    }
    
    func saveCustomerMyPage(_ cusMyPage: CustomerMyPage?) {
        LocalService.saveCustomerMyPage(cusMyPage)
    }
    
    func getToken() -> String? {
        return LocalService.loadAuthToken()
    }
    
    func saveSettingInfo(_ setting: [Setting]?) {
        LocalService.saveSettingInfo(setting)
    }
    
    func loadSettingInfo() -> [Setting]? {
        return LocalService.loadSavedSettingInfo()
    }

}
