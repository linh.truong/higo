//
//  CustomerPrizes.swift
//  Roann
//
//  Created by Linh Trương on 4/4/17.
//  Copyright © 2017 IBS. All rights reserved.
//

import Foundation
import ObjectMapper

class CustomerPrizes: Mappable {
    
    var id: Int?
    var customer: Int?
    var prize: Int?
    var delivery_type: Int?
    var name: String?
    var phone: String?
    var email: String?
    var address: String?
    var use_date: Date?
    var delivery_date: Date?
    var receive_date: Date?
    var status: Int?
    var language: String?
    var user_point: Int?
    var total_point: Int?
    var remain_point: Int?
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    convenience init(id: Int?, customer: Int?, prize: Int?, delivery_type: Int?, name: String?, phone: String?, email: String?, address: String?, use_date: Date?, delivery_date: Date? = Date(), receive_date: Date?, status: Int?) {
        self.init()
        self.id = id
        self.customer = customer
        self.prize = prize
        self.delivery_type = delivery_type
        self.name = name
        self.phone = phone
        self.email = email
        self.address = address
        self.use_date = use_date
        self.delivery_date = delivery_date
        self.receive_date = receive_date
        self.status = status
    }
    
    func mapping(map: Map) {
        id <- map["id"]
        customer <- map["customer"]
        prize <- map["prize"]
        delivery_type <- map["delivery_type"]
        name <- map["name"]
        phone <- map["phone"]
        email <- map["email"]
        address <- map["address"]
        use_date <- (map["use_date"], ISO8601DateTransform())
        delivery_date <- (map["delivery_date"], ISO8601DateTransform())
        receive_date <- (map["receive_date"], ISO8601DateTransform())
        status <- map["status"]
        language <- map["language"]
        user_point <- map["current_point"]
        remain_point <- map["remain_point"]
        total_point <- map["total_point"]
        
    }
    
}
