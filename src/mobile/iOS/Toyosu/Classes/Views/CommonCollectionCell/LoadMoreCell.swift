//
//  LoadMoreCell.swift
//  Toyosu
//
//  Created by Linh Trương on 12/2/16.
//  Copyright © 2016 IBS. All rights reserved.
//

import UIKit

class LoadMoreCell: UICollectionViewCell {

    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!

    static let cellID = "LoadMoreCell"
    static let cellSize = CGSize.init(width: 320, height: 44)
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }
}
