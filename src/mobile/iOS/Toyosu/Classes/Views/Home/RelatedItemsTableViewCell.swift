//
//  RelatedItemsTableViewCell.swift
//  Toyosu
//
//  Created by Linh Truong on 1/12/17.
//  Copyright © 2017 IBS. All rights reserved.
//

import UIKit
import AFNetworking
import Localize_Swift
import SVProgressHUD

class RelatedItemsTableViewCell: UITableViewCell {

    // Id of cell
    static let cellID = "RelatedItemCell"
    
    var allItems: [CustomRelatedItems]?
    var hashTagsDeliveryVal : String = ""
    var selectRelatedCallback: ((_ item: CustomRelatedItems, _ cell: RelatedItemsTableViewCell) -> ())?
    
    @IBOutlet weak var lbRelatedItemsTitle: UILabel!
    @IBOutlet weak var cvRelatedItems: UICollectionView!
    @IBOutlet weak var indiLoading: UIActivityIndicatorView!
    @IBOutlet weak var viewRelatedItems: UIView!
    @IBOutlet weak var viewError: UIView!
    @IBOutlet weak var lbError: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        setHomeDetailInit()
        // Reload localized after change
        NotificationCenter.default.addObserver(self, selector: #selector(RelatedItemsTableViewCell.refreshLanguage), name: NSNotification.Name(LCLLanguageChangeNotification), object: nil)
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    func refreshLanguage () {
        cvRelatedItems.reloadData()
        lbRelatedItemsTitle.text = "common_related_item_title".localized()
        lbError.text = "error_no_data".localized()
    }
    
    func setHomeDetailInit() {
        cvRelatedItems.delegate = self
        cvRelatedItems.dataSource = self
        lbRelatedItemsTitle.text = "common_related_item_title".localized()
        lbError.text = "error_no_data".localized()
        lbError.textColor = AppColor.DARK_GRAY_COLOR
    }
    
    func showIndicator (){
        indiLoading.startAnimating()
        cvRelatedItems.isHidden = true
        viewError.isHidden = true
    }
    
    func finishRefreshing () {
        if let itemArr = allItems {
            if itemArr.count == 0 {
                indiLoading.stopAnimating()
                viewError.isHidden = false
            } else {
                indiLoading.stopAnimating()
                viewError.isHidden = true
                cvRelatedItems.isHidden = false
                cvRelatedItems.reloadData()
            }
        } else {
            indiLoading.stopAnimating()
            viewError.isHidden = false
        }
    }
}

extension RelatedItemsTableViewCell : UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return allItems == nil ? 0 : allItems!.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let rowAtIndexPath = indexPath.row
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: RelatedItemsCollectionViewCell.cellID, for: indexPath) as! RelatedItemsCollectionViewCell
        
        cell.configureCellWith(allItems![rowAtIndexPath])
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {

        return CGSize(width: 150, height: 203)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let item = allItems![indexPath.row]
        selectRelatedCallback?(item, self)
    }
}
