//
//  HomeDetailTableViewCell.swift
//  Toyosu
//
//  Created by Linh Trương on 12/7/16.
//  Copyright © 2016 IBS. All rights reserved.
//

import UIKit
import AFNetworking
import Localize_Swift

class HomeDetailTableViewCell: UITableViewCell {

    // Id of cell
    static let cellID = "NewsDetailCell"
    
    // get outlet
    @IBOutlet weak var viewCardStack: UIView!
    @IBOutlet weak var resAvatar: UIImageView!
    @IBOutlet weak var resName: UILabel!
    @IBOutlet weak var lbSubject: UILabel!
    @IBOutlet weak var lbHashtags: UILabel!
    @IBOutlet weak var imgPath: UIImageView!
    @IBOutlet weak var imgViewHeight: NSLayoutConstraint!
    @IBOutlet weak var lbBody: UILabel!
    @IBOutlet weak var lbOpenDate: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        setLayout()
        presentImageView()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }
    
    func calculateImageSize(_ topic: Topic) {
        if let w = topic.image_width {
            let imgWidth = CGFloat(w)
            if let h = topic.image_height {
                let imgHeight = CGFloat(h)
                let imgViewWidth = ScreenSize.MAIN_BOUNDS.size.width - 16
                imgViewHeight.constant = (imgViewWidth * imgHeight) / imgWidth
            }
        }
    }
    
    func setLayout() {
        viewCardStack.layer.cornerRadius = 3
        viewCardStack.layer.masksToBounds = true
        resAvatar.layer.cornerRadius = resAvatar.bounds.width / 2.0
        resAvatar.layer.masksToBounds = true
        resAvatar.layer.borderColor = UIColor.white.cgColor
        resAvatar.layer.borderWidth = 1.0
    }
    
    func presentImageView(){
        imgPath?.setupForImageViewer()
        imgPath?.setupForImageViewer(UIColor.black)
    }
}
