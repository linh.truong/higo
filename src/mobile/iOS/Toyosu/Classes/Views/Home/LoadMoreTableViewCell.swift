//
//  LoadMoreTableViewCell.swift
//  Toyosu
//
//  Created by Linh Trương on 12/6/16.
//  Copyright © 2016 IBS. All rights reserved.
//

import UIKit

class LoadMoreTableViewCell: UITableViewCell {

    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    static let cellID = "LoadMoreCell"
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
