//
//  ActionTableViewCell.swift
//  Toyosu
//
//  Created by Linh Trương on 10/21/16.
//  Copyright © 2016 Linh Trương. All rights reserved.
//

import UIKit
import Social

class ActionTableViewCell: UITableViewCell {
    
    @IBOutlet weak var btnShareFB: UIButton!
    @IBOutlet weak var btnUse: UIButton!
    @IBOutlet weak var btnClose: UIButton!
    @IBOutlet weak var btnLinkToECSite: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func disableUseButton(){
        btnUse.layer.cornerRadius = 5
        btnUse.isUserInteractionEnabled = false
        btnUse.backgroundColor = UIColor.lightGray
    }

}
