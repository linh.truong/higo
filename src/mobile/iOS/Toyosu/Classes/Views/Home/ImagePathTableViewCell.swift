//
//  ImagePathTableViewCell.swift
//  Toyosu
//
//  Created by Linh Trương on 10/21/16.
//  Copyright © 2016 Linh Trương. All rights reserved.
//

import UIKit

class ImagePathTableViewCell: UITableViewCell {

    @IBOutlet weak var imgPath: UIImageView!
    @IBOutlet weak var imgViewHeight: NSLayoutConstraint!
    
    func showIMG(image_path: String?) {
        var url: URL? = nil
        if let str = image_path {
            if let u = URL.init(string: str) {
                url = u
            }
        }
        if let fUrl = url {
            imgPath.setImageWith(fUrl, placeholderImage: Constant.NIL_IMAGE)
        } else {
            imgPath.image = UIImage.init(named: "no_img")
        }
    }
    
    func calculateImageSize(_ pr: Prize) {
        if let w = pr.image_width {
            let imgWidth = CGFloat(w)
            if let h = pr.image_height {
                let imgHeight = CGFloat(h)
                let imgViewWidth = ScreenSize.MAIN_BOUNDS.size.width
                imgViewHeight.constant = (imgViewWidth * imgHeight) / imgWidth
            }
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        presentImageView()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func presentImageView(){
        imgPath?.setupForImageViewer()
        imgPath?.setupForImageViewer(UIColor.black)
    }

}
