//
//  RelatedItemsCollectionViewCell.swift
//  Toyosu
//
//  Created by Linh Truong on 1/12/17.
//  Copyright © 2017 IBS. All rights reserved.
//

import UIKit
import AFNetworking
import Localize_Swift

class RelatedItemsCollectionViewCell: UICollectionViewCell {
    
    static let cellID = "RelatedItemCell"
    
    @IBOutlet weak var imgPath: UIImageView!
    @IBOutlet weak var lbName: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        setScreenLayout()
    }
    
    // Set layout
    func setScreenLayout()
    {
        self.layer.masksToBounds = true
        self.layer.cornerRadius = 3.0
    }
    
    func configureCellWith(_ relatedItems: CustomRelatedItems)
    {
        
        var url: URL? = nil
        
        if let str = relatedItems.image_path {
            if let u = URL.init(string: str) {
                url = u
                if let fUrl = url {
                    imgPath.setImageWith(fUrl, placeholderImage: Constant.NIL_IMAGE)
                } else {
                    imgPath.image = UIImage.init(named: "no_img")
                }
            }
        }
        
        if let type = relatedItems.type {
            switch type {
            case .CouponType:
                
                if Localize.currentLanguage() == "en" {
                    lbName.text = relatedItems.title
                } else {
                    lbName.text = relatedItems.title_cn
                }
                
                break
            case .TopicType:
                if Localize.currentLanguage() == "en" {
                    lbName.text = relatedItems.subject
                } else {
                    lbName.text = relatedItems.subject_cn
                }
                
                break
            case .FoodType:
                if Localize.currentLanguage() == "en" {
                    lbName.text = relatedItems.name
                } else {
                    lbName.text = relatedItems.name_cn
                }
                
                break
            }
        }
    }
}
