//
//  InstagramTableViewCell.swift
//  Roann
//
//  Created by Linh Trương on 4/18/17.
//  Copyright © 2017 IBS. All rights reserved.
//

import UIKit

class InstagramTableViewCell: UITableViewCell {

    static let cellID = "InstagramTableViewCell"
    var url: URL? = nil
    let dateFormatter = DateFormatter()
    
    @IBOutlet weak var viewCardStack: UIView!
    @IBOutlet weak var instaAvatar: UIImageView!
    @IBOutlet weak var instaName: UILabel!
    @IBOutlet weak var instaLocation: UILabel!
    @IBOutlet weak var imagePath: UIImageView!
    @IBOutlet weak var imageHeight: NSLayoutConstraint!
    @IBOutlet weak var instaLikeIcon: UIImageView!
    @IBOutlet weak var lbLike: UILabel!
    @IBOutlet weak var instaCommentIcon: UIImageView!
    @IBOutlet weak var lbComment: UILabel!
    @IBOutlet weak var lbContent: UILabel!
    @IBOutlet weak var lbCreateDate: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
//        imagePath.layer.shadowColor = UIColor.darkGray.cgColor
//        imagePath.layer.shadowOffset = CGSize(width: 1, height: 1)
//        imagePath.layer.shadowRadius = 5.0
//        imagePath.layer.shadowOpacity = 0.5
        setViewLayout()
    }
    
    func setViewLayout(){
        viewCardStack.layer.cornerRadius = 2.5
        viewCardStack.layer.masksToBounds = true
        instaAvatar.layer.cornerRadius = instaAvatar.bounds.width / 2.0
        instaAvatar.layer.masksToBounds = true
        instaAvatar.layer.borderColor = UIColor.white.cgColor
        instaAvatar.layer.borderWidth = 1.0
        // format date
        dateFormatter.dateStyle = .long
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    // Configure parameter table cell
    func configureCellWith(_ data : InstagramModel?) {
        if let iUser = data?.user {
            // Profile picture
            if let str = iUser.profile_picture {
                if let u = URL.init(string: str) {
                    url = u
                    if let fUrl = url {
                        instaAvatar.setImageWith(fUrl, placeholderImage: Constant.NIL_IMAGE)
                    } else {
                        instaAvatar.image = UIImage.init(named: "no_img")
                    }
                }
            }
            // Username
            instaName.text = iUser.username
        } else {
            instaName.text = ""
        }
        if let iLocation = data?.location {
            // Location
            instaLocation.text = iLocation.name
        } else {
            instaLocation.text = ""
        }
        if let iImage = data?.images?.standard_resolution {
            // Calculate ratio
            if let w = iImage.width {
                let imgWidth = CGFloat(w)
                if let h = iImage.height {
                    let imgHeight = CGFloat(h)
                    let imgViewWidth = ScreenSize.MAIN_BOUNDS.size.width - 16
                    imageHeight.constant = (imgViewWidth * imgHeight) / imgWidth
                }
            }
            // Images
            if let str2 = iImage.url {
                if let u = URL.init(string: str2) {
                    url = u
                    if let fUrl = url {
                        imagePath.setImageWith(fUrl, placeholderImage: Constant.NIL_IMAGE)
                    } else {
                        imagePath.image = UIImage.init(named: "no_img")
                    }
                }
            } else {
                imagePath.image = UIImage.init(named: "no_img")
            }
        } else {
            imagePath.image = UIImage.init(named: "no_img")
        }
        if let iLike = data?.likes {
            // Likes
            lbLike.text = String(format: NSLocalizedString("home_lb_num_like".localized(), comment: ""), "\(iLike.count?.toString() ?? "")")
        } else {
            lbLike.text = ""
        }
        if let iComment = data?.comments {
            // Comments
            lbComment.text = String(format: NSLocalizedString("home_lb_num_comment".localized(), comment: ""), "\(iComment.count?.toString() ?? "")")
        } else {
            lbComment.text = ""
        }
        if let iCaption = data?.caption {
            // Caption
            self.lbContent.setRequiredAttributeHashtag(iCaption.text!)
        } else {
            lbContent.text = ""
        }
        // Date & time posted
        if let timeStapAPI = data?.created_time?.double {
            let timeStamp : TimeInterval = timeStapAPI
            let timeStampDate = Date(timeIntervalSince1970: timeStamp)
            dateFormatter.dateStyle = .full
            let dateStr = dateFormatter.string(from: timeStampDate)
            lbCreateDate.text = String(format: NSLocalizedString("home_lb_open_date".localized(), comment: ""), "\(dateStr)")
        } else {
            lbCreateDate.text = ""
        }
    }

}
