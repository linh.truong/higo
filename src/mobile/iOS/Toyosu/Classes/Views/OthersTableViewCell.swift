//
//  OthersTableViewCell.swift
//  Toyosu
//
//  Created by nguyen.trung.hau on 10/21/16.
//  Copyright © 2016 Linh Trương. All rights reserved.
//

import Foundation
import UIKit

class OtherTableViewCell: UITableViewCell {
    
    @IBOutlet weak var txtName: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
              
    }
    func setLabelName (name:String)
    {
        txtName.text = name
    }
}
