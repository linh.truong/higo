//
//  MainDatePickerView.swift
//  Roann
//
//  Created by Linh Trương on 3/22/17.
//  Copyright © 2017 IBS. All rights reserved.
//

import Foundation
import UIKit

class MainDatePickerView: UIDatePicker
{
    @IBInspectable var selectorColor: UIColor? = AppColor.PRIMARY_COLOR
    
    override func didAddSubview(_ subview: UIView) {
        super.didAddSubview(subview)
        
        if let color = selectorColor
        {
            if subview.bounds.height < 1.0
            {
                subview.backgroundColor = color
            }
        }
    }
}
