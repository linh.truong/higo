//
//  EmptyView.swift
//  Example
//
//  Created by Alexander Schuch on 29/08/14.
//  Copyright (c) 2014 Alexander Schuch. All rights reserved.
//

import UIKit
import Localize_Swift

class EmptyView: BasicPlaceholderView {
	
	let label = UILabel()

	override func setupView() {
		super.setupView()
		
		backgroundColor = UIColor.clear
		
		label.text = "error_no_data".localized()
		label.translatesAutoresizingMaskIntoConstraints = false
		centerView.addSubview(label)
		
		let views = ["label": label]
		let hConstraints = NSLayoutConstraint.constraints(withVisualFormat: "|-[label]-|", options: .alignAllCenterY, metrics: nil, views: views)
		let vConstraints = NSLayoutConstraint.constraints(withVisualFormat: "V:|-[label]-|", options: .alignAllCenterX, metrics: nil, views: views)

		centerView.addConstraints(hConstraints)
		centerView.addConstraints(vConstraints)
        
        NotificationCenter.default.addObserver(self, selector: #selector(EmptyView.refreshLanguage), name: NSNotification.Name(LCLLanguageChangeNotification), object: nil)
	}

    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    func refreshLanguage() {
        setupView()
    }
}
