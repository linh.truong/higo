//
//  PrizeListTableViewCell.swift
//  Toyosu
//
//  Created by Linh Trương on 11/10/16.
//  Copyright © 2016 Linh Trương. All rights reserved.
//

import UIKit
import Localize_Swift

class PrizeListTableViewCell: UITableViewCell {

    // Id of cell
    static let cellID = "PrizeCell"
    
    // get outlet
    @IBOutlet weak var viewCardStack: UIView!
    @IBOutlet weak var imgPath: UIImageView!
    @IBOutlet weak var lbTitle: UILabel!
    @IBOutlet weak var lbStartDate: UILabel!
    
    // define date format
    let dateFormatter = DateFormatter()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        imgPath.layer.cornerRadius = 2.5
        imgPath.layer.masksToBounds = true
        viewCardStack.layer.cornerRadius = 2.5
        viewCardStack.layer.masksToBounds = true
        dateFormatter.dateStyle = .medium
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    // Configure parameter table cell
    func configureCellWith (_ prize: Prize) {
        
        var url: URL? = nil
        
        if let str = prize.image_path {
            if let u = URL.init(string: str) {
                url = u
            }
        }
        
        if let fUrl = url {
            imgPath.setImageWith(fUrl)
        } else {
            imgPath.image = UIImage.init(named: "no_img")
        }
        
        if Localize.currentLanguage() == "en" {
            lbTitle.text = prize.title
        } else {
            lbTitle.text = prize.title_cn
        }
        
        if let fromDate = prize.from_date {
            
            lbStartDate.text = String(format: NSLocalizedString("coupon_lb_start_date".localized(), comment: ""), "\(dateFormatter.string(from: fromDate))")
        }
    }
    
}
