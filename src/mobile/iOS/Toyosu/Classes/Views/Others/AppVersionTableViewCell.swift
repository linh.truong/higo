//
//  AppVersionTableViewCell.swift
//  Toyosu
//
//  Created by nguyen.trung.hau on 11/22/16.
//  Copyright © 2016 Linh Trương. All rights reserved.
//

import Foundation
import UIKit

class AppVersionTableViewCell: UITableViewCell {    
  
    @IBOutlet weak var appVersion: UILabel!

    @IBOutlet weak var lblAppversion: UILabel!
}
