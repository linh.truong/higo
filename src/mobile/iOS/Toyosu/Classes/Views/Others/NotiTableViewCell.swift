//
//  NotiTableViewCell.swift
//  Toyosu
//
//  Created by nguyen.trung.hau on 11/28/16.
//  Copyright © 2016 Linh Trương. All rights reserved.
//


import UIKit

class NotiTableViewCell: UITableViewCell {
    

    @IBOutlet weak var notiLabel: UILabel!
    
    @IBOutlet weak var notiSw: UISwitch!
    
   
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure theswPushNotiview for the selected state
    }
    
}
