//
//  SettingReceivePushTableViewCell.swift
//  Toyosu
//
//  Created by Linh Trương on 10/27/16.
//  Copyright © 2016 Linh Trương. All rights reserved.
//

import UIKit

class SettingReceivePushTableViewCell: UITableViewCell {

    @IBOutlet weak var lbPushNoti: UILabel!
    @IBOutlet weak var swPushNoti: UISwitch!
    
    @IBOutlet weak var pushImg: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()     
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
                // Configure theswPushNotiview for the selected state
    }
    
}
