//
//  SettingReceiveEmailTableViewCell.swift
//  Toyosu
//
//  Created by Linh Trương on 10/27/16.
//  Copyright © 2016 Linh Trương. All rights reserved.
//

import UIKit

class SettingReceiveEmailTableViewCell: UITableViewCell {

    @IBOutlet weak var swEmailNoti: UISwitch!
    @IBOutlet weak var lbReceiveEmailNoti: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()       
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
