//
//  SettingLanguageTableViewCell.swift
//  Toyosu
//
//  Created by Linh Trương on 10/28/16.
//  Copyright © 2016 Linh Trương. All rights reserved.
//

import UIKit

class SettingLanguageTableViewCell: UITableViewCell {

    @IBOutlet weak var lbLanguage: UILabel!
    @IBOutlet weak var segLanguage: UISegmentedControl!
    @IBOutlet weak var lnguageImg: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setLanguages(_ languages: [String], indexLang: Int?) {
        
        segLanguage.removeAllSegments()
        for (index, lang) in languages.enumerated() {
            segLanguage.insertSegment(withTitle: lang, at: index, animated: false)
        }
        if let index = indexLang {
            segLanguage.selectedSegmentIndex = index
        }

    }

}
