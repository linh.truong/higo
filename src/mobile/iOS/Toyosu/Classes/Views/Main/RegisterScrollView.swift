//
//  registerScrollView.swift
//  Toyosu
//
//  Created by nguyen.trung.hau on 10/25/16.
//  Copyright © 2016 Linh Trương. All rights reserved.
//

import Foundation
import UIKit

class RegisterScrollView:UIScrollView{

    //hide keyboard when press outside textbox
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
  
        superview!.endEditing(true)
        
    }

}
