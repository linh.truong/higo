//
//  ReserveTimePickerView.swift
//  Toyosu
//
//  Created by Linh Trương on 12/20/16.
//  Copyright © 2016 IBS. All rights reserved.
//

import Foundation
import UIKit

class ReserveTimePickerView: UIPickerView
{
    @IBInspectable var selectorColor: UIColor? = AppColor.PRIMARY_COLOR
    
    override func didAddSubview(_ subview: UIView) {
        super.didAddSubview(subview)
        
        if let color = selectorColor
        {
            if subview.bounds.height < 1.0
            {
                subview.backgroundColor = color
            }
        }
        
    }
}
