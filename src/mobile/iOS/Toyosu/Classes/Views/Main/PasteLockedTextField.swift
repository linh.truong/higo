//
//  UnPassableTextField.swift
//  Toyosu
//
//  Created by nguyen.trung.hau on 11/16/16.
//  Copyright © 2016 Linh Trương. All rights reserved.
//

import Foundation
import UIKit

class PasteLockedTextField: UITextField {
    
    override func canPerformAction(_ action: Selector, withSender sender: Any?) -> Bool {
        if action ==  #selector(UIResponderStandardEditActions.paste(_:))
        {
            return false
        }

        return super.canPerformAction(action, withSender: sender)

    }
}
