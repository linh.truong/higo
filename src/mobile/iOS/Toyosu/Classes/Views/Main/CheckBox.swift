//
//  CheckBox.swift
//  Toyosu
//
//  Created by nguyen.trung.hau on 10/18/16.
//  Copyright © 2016 IBS. All rights reserved.
//

import Foundation
import UIKit

class CheckBox: UIButton {
    
    //images
    let checkedImage = UIImage(named: "check") //as UIImage!
    let uncheckedImage = UIImage(named: "uncheck") //as UIImage!
    
    //bool property
    var isChecked:Bool = false{
        didSet{
            if isChecked == true {
                self.setImage(checkedImage, for: .normal)
            }else {
                self.setImage(uncheckedImage, for: .normal)
            }
            
        }
    }
    
    override func awakeFromNib() {
        self.addTarget(self, action: #selector(self.buttonClicked(sender:)), for: UIControlEvents.touchDown)
        self.isChecked = false
    }
    
    
    @objc func buttonClicked(sender:UIButton) {
        if(sender == self){
            if isChecked == true {
                isChecked = false
            }else{
                isChecked = true
            }
        }
    }
}
