//
//  RegisterPickerView.swift
//  Toyosu
//
//  Created by nguyen.trung.hau on 10/25/16.
//  Copyright © 2016 IBS. All rights reserved.
//

import Foundation
import UIKit

class RegisterPickerView: UIPickerView
{
    @IBInspectable var selectorColor: UIColor? = AppColor.PRIMARY_COLOR
    
    override func didAddSubview(_ subview: UIView) {
        super.didAddSubview(subview)
        
        if let color = selectorColor
        {
            if subview.bounds.height < 1.0
            {
                subview.backgroundColor = color
            }
        }
        
    }
}
