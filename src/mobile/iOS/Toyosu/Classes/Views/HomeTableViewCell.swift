//
//  HomeTableViewCell.swift
//  Toyosu
//
//  Created by Linh Trương on 10/12/16.
//  Copyright © 2016 IBS. All rights reserved.
//

import UIKit
import AFNetworking
import Localize_Swift

protocol HomeTableViewCellDelegate {
    func like(_ index : Int)
    func share(_ index : Int)
}

class HomeTableViewCell: UITableViewCell {
    
    // Id of cell
    static let cellID = "NewsCell"
    
    // Delegate
    var delegate : HomeTableViewCellDelegate!
    var index : Int!
    
    // define variable
    var url: URL? = nil
    let dateFormatter = DateFormatter()
    var didTouchImage: ((_ cell: HomeTableViewCell, _ imageView: UIImageView) -> ())?
    
    // get outlet
    @IBOutlet weak var viewCardStack: UIView!
    @IBOutlet weak var resAvatar: UIImageView!
    @IBOutlet weak var resName: UILabel!
    @IBOutlet weak var imgPath: UIImageView!
    @IBOutlet weak var imgViewHeight: NSLayoutConstraint!
    @IBOutlet weak var btnLike: UIButton!
    @IBOutlet weak var btnShare: UIButton!
    @IBOutlet weak var imgLike: UIImageView!
    @IBOutlet weak var lbNumberOfLikes: UILabel!
    @IBOutlet weak var imgShare: UIImageView!
    @IBOutlet weak var lbNumberOfShare: UILabel!
    @IBOutlet weak var lbSubject: UILabel!
    @IBOutlet weak var lbHashTags: UILabel!
    @IBOutlet weak var lbOpenDate: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        // set screen layout
        setLayout()
        
        let tapGesture = UITapGestureRecognizer.init(target: self, action: #selector(HomeTableViewCell.didTapImage(gesture:)))
        resAvatar.addGestureRecognizer(tapGesture)
        
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
    }
    
    func calculateImageSize(_ topic: Topic) {
        if let w = topic.image_width {
            let imgWidth = CGFloat(w)
            if let h = topic.image_height {
                let imgHeight = CGFloat(h)
                let imgViewWidth = ScreenSize.MAIN_BOUNDS.size.width - 16
                imgViewHeight.constant = (imgViewWidth * imgHeight) / imgWidth
            }
        }
    }
    
    func setLayout() {
        viewCardStack.layer.cornerRadius = 2.5
        viewCardStack.layer.masksToBounds = true
        resAvatar.layer.cornerRadius = resAvatar.bounds.width / 2.0
        resAvatar.layer.masksToBounds = true
        resAvatar.layer.borderColor = UIColor.white.cgColor
        resAvatar.layer.borderWidth = 1.0
        btnLike.setImage(UIImage(named: "onigiri_icon"), for: .normal)
        btnShare.setImage(UIImage(named: "share_icon"), for: .normal)
        // format date
        dateFormatter.dateFormat = "yyyy/MM/dd"
    }
    
    func didTapImage(gesture: UITapGestureRecognizer) {
        didTouchImage?(self, resAvatar)
    }
    
    @IBAction func btnLikeTapped(_ sender: Any) {
        delegate.like(index)
    }
    
    @IBAction func btnShareTapped(_ sender: Any) {
        delegate.share(index)
    }
    
    // Configure parameter table cell
    func configureCellWith(_ topic: Topic) {
        
        if Localize.currentLanguage() == "en" {
            resName.text = topic.res_name
        } else {
            resName.text = topic.res_name_cn
        }
        
        if let str = topic.image_path {
            if let u = URL.init(string: str) {
                url = u
                if let fUrl = url {
                    imgPath.setImageWith(fUrl, placeholderImage: Constant.NIL_IMAGE)
                } else {
                    imgPath.image = UIImage.init(named: "no_img")
                }
            }
        }
        
        if Localize.currentLanguage() == "en" {
            lbSubject.text = topic.subject
        } else {
            lbSubject.text = topic.subject_cn
        }
        
        if let openDate = topic.open_date {
            lbOpenDate.text = String(format: NSLocalizedString("home_lb_open_date".localized(), comment: ""), "\(dateFormatter.string(from: openDate))")
        }
        
        if let num_like = topic.num_like {
            lbNumberOfLikes.text = String(format: NSLocalizedString("home_lb_num_like".localized(), comment: ""), "\(num_like.toString())")
        }
        
        if let num_share = topic.num_share {
            lbNumberOfShare.text = String(format: NSLocalizedString("home_lb_num_share".localized(), comment: ""), "\(num_share.toString())")
        }
        
        if let hashtag = topic.hash_tags {
            lbHashTags.text = hashtag
        }
    }
}
