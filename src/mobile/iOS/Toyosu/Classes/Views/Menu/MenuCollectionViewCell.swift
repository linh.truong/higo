//
//  MenuCollectionViewCell.swift
//  Toyosu
//
//  Created by Linh Trương on 11/24/16.
//  Copyright © 2016 IBS. All rights reserved.
//

import UIKit
import AFNetworking
import Localize_Swift

class MenuCollectionViewCell: UICollectionViewCell {
    
    // Id of cell
    static let cellID = "FoodCell"
    
    // Get outlet from Storyboard
    @IBOutlet weak var imgPath: UIImageView!
    @IBOutlet weak var lbName: UILabel!
    @IBOutlet weak var lbPrice: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        setScreenLayout()
    }
    
    // Set layout
    func setScreenLayout()
    {
        self.layer.masksToBounds = true
        self.layer.cornerRadius = 3.0
    }
    
    // Configure parameter collection cell
    func configureCellWith(_ food: Food)
    {
        
        if Localize.currentLanguage() == "en" {
            lbName.text = food.name
        } else {
            lbName.text = food.name_cn
        }
        
        if let price = food.price {
            let priceSplit = price.components(separatedBy: ".")
            let priceRound = priceSplit[0]
            lbPrice.text = Constant.CURRENCY_DOLLAR + priceRound
            
        }
        
        var url: URL? = nil
        
        if let str = food.image_path {
            if let u = URL.init(string: str) {
                url = u
            }
        }
        if let fUrl = url {
            imgPath.setImageWith(fUrl, placeholderImage: Constant.NIL_IMAGE)
        } else {
            imgPath.image = UIImage.init(named: "no_img")
        }
    }
}
