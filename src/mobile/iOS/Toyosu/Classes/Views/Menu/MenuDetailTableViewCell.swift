//
//  MenuDetailTableViewCell.swift
//  Toyosu
//
//  Created by Linh Trương on 12/14/16.
//  Copyright © 2016 IBS. All rights reserved.
//

import UIKit

class MenuDetailTableViewCell: UITableViewCell {

    // Id of cell
    static let cellID = "MenuDetailCell"
    
    // get outlet
    @IBOutlet weak var imgPath: UIImageView!
    @IBOutlet weak var imgViewHeight: NSLayoutConstraint!
    @IBOutlet weak var lbName: UILabel!
    @IBOutlet weak var lbPrice: UILabel!
    @IBOutlet weak var lbDescription: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        presentImageView()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    func calculateImageSize(_ food: Food) {
        if let w = food.image_width {
            let imgWidth = CGFloat(w)
            if let h = food.image_height {
                let imgHeight = CGFloat(h)
                let imgViewWidth = ScreenSize.MAIN_BOUNDS.size.width
                imgViewHeight.constant = (imgViewWidth * imgHeight) / imgWidth
            }
        }
    }
    
    func presentImageView(){
        imgPath?.setupForImageViewer()
        imgPath?.setupForImageViewer(UIColor.black)
    }
}
