//
//  InsetLabel.swift
//  Toyosu
//
//  Created by nguyen.trung.hau on 10/20/16.
//  Copyright © 2016 Linh Trương. All rights reserved.
//

import Foundation
import UIKit

class RestaurantInsetLabel: UILabel {
    override func drawText(in rect: CGRect) {
        super.drawText(in: UIEdgeInsetsInsetRect(rect, UIEdgeInsets(top: 0, left: 10, bottom: 0, right: 0)))
    }
}
    
