//
//  RestaurantInfoCell.swift
//  Toyosu
//
//  Created by Linh Trương on 1/20/17.
//  Copyright © 2017 IBS. All rights reserved.
//

import UIKit

class RestaurantInfoCell : UITableViewCell {
    
    static let cellId: String = "RestaurantInfoCell"
    
    @IBOutlet weak var imgView: UIImageView!
    
    @IBOutlet weak var lblTitle: UILabel!
    
    @IBOutlet weak var lblDescription: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        lblTitle.textColor = UIColor.black
        lblDescription.textColor = AppColor.PRIMARY_COLOR
    }
    
    func showData(_ img: UIImage?, _ title: String?, _ des: String?) {
        imgView.image = img
        lblTitle.text = title
        lblDescription.text = des
    }
}
