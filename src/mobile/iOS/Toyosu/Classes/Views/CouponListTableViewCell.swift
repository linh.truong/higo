//
//  CouponListTableViewCell.swift
//  Toyosu
//
//  Created by VW10002 on 10/10/16.
//  Copyright © 2016 Linh Trương. All rights reserved.
//

import UIKit
import AFNetworking
import Localize_Swift

class CouponListTableViewCell: UITableViewCell {
    
    // Id of cell
    static let cellID = "CouponCell"
    
    // define date format
    let dateFormatter = DateFormatter()
    
    // get outlet
    @IBOutlet weak var imgPath: UIImageView!
    @IBOutlet weak var lbTitle: UILabel!
    @IBOutlet weak var lbToDate: UILabel!
    @IBOutlet weak var lbStartTime: UILabel!
    @IBOutlet weak var lbEndTime: UILabel!
    @IBOutlet weak var viewStackCard: UIView!
    @IBOutlet weak var lbRemainDay: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        setLayout()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func setLayout(){
        viewStackCard.layer.masksToBounds = true
        viewStackCard.layer.cornerRadius = 2.5
        imgPath.layer.masksToBounds = true
        imgPath.layer.cornerRadius = 2.5
        dateFormatter.dateStyle = .medium
    }
    
    // Configure parameter table cell
    func configureCellWith (_ coupon: Coupon) {
        
        var url: URL? = nil
        
        if let str = coupon.image_path {
            if let u = URL.init(string: str) {
                url = u
            }
        }
        
        if let fUrl = url {
            imgPath.setImageWith(fUrl)
        } else {
            imgPath.image = UIImage.init(named: "no_img")
        }
        
        if Localize.currentLanguage() == "en" {
            lbTitle.text = coupon.title
        } else {
            lbTitle.text = coupon.title_cn
        }
        
        if let toDate = coupon.to_date {
            
            lbToDate.text = String(format: NSLocalizedString("coupon_lb_expired_date".localized(), comment: ""), "\(dateFormatter.string(from: toDate))")
        }
        
        if let startTime = coupon.start_time {
            lbStartTime.text = String(format: NSLocalizedString("coupon_lb_start_time".localized(), comment: ""), "\(startTime)")
        }
        
        if let endTime = coupon.end_time {
            lbEndTime.text = String(format: NSLocalizedString("coupon_lb_end_time".localized(), comment: ""), "\(endTime)")
        }
        
        if let remainDay = coupon.remain_day {
            if remainDay == 0 {
                lbRemainDay.text = "coupon_lb_to_day".localized()
            } else {
                lbRemainDay.text = String(format: NSLocalizedString("coupon_lb_remain_day".localized(), comment: ""), "\(remainDay)")
            }
        }
    }
}
