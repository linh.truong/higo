//
//  DottedLineHelper.swift
//  Toyosu
//
//  Created by Linh Trương on 12/13/16.
//  Copyright © 2016 IBS. All rights reserved.
//

import UIKit

class DottedLineHelper: UIView {
    
    override func awakeFromNib() {
        super.awakeFromNib()
        backgroundColor = UIColor.white
    }

    override func draw(_ rect: CGRect) {
        super.draw(rect)
        
        let margin: CGFloat = 18

        let path = UIBezierPath()
        path.move(to: CGPoint(x: margin, y: rect.height/2))
        path.addLine(to: CGPoint(x: rect.width - 0.5 * margin, y: rect.height/2))
        path.lineWidth = (rect.width - 4 * margin) / 22
        
        let dashes: [CGFloat] = [path.lineWidth * 0, path.lineWidth * 1.5]
        path.setLineDash(dashes, count: dashes.count, phase: 0)
        path.lineCapStyle = CGLineCap.round
        
        AppColor.DARK_GRAY_COLOR.setStroke()
        
        path.stroke()
    }

}
