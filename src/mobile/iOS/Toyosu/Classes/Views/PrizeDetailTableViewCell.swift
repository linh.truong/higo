//
//  PrizeDetailTableViewCell.swift
//  Roann
//
//  Created by Linh Trương on 3/28/17.
//  Copyright © 2017 IBS. All rights reserved.
//

import UIKit

class PrizeDetailTableViewCell: UITableViewCell {

    // Id of cell
    static let cellID = "PrizeDetailCell"
    // Get outlet
    @IBOutlet weak var imgPath: UIImageView!
    @IBOutlet weak var lbPointsAndTitle: UILabel!
    @IBOutlet weak var lbDescription: UILabel!
    @IBOutlet weak var lbStartDate: UILabel!
    @IBOutlet weak var btnLinkToECSite: UIButton!
    @IBOutlet weak var lbTermAndCondition: UILabel!
    @IBOutlet weak var btnUse: UIButton!
    @IBOutlet weak var imgViewHeight: NSLayoutConstraint!
    
    override func awakeFromNib() {
        super.awakeFromNib()
       
        setLayout()
        presentImageView()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    func calculateImageSize(_ prize: Prize) {
        if let w = prize.image_width {
            let imgWidth = CGFloat(w)
            if let h = prize.image_height {
                let imgHeight = CGFloat(h)
                let imgViewWidth = ScreenSize.MAIN_BOUNDS.size.width
                imgViewHeight.constant = (imgViewWidth * imgHeight) / imgWidth
            }
        }
    }
    
    func setLayout(){
        btnUse.layer.cornerRadius = 5.0
        btnUse.isUserInteractionEnabled = false
    }
    
    func presentImageView(){
        imgPath?.setupForImageViewer()
        imgPath?.setupForImageViewer(UIColor.black)
    }
    
}
