//
//  RestaurantListTableViewCell.swift
//  Toyosu
//
//  Created by Linh Trương on 10/6/16.
//  Copyright © 2016 IBS. All rights reserved.
//

import UIKit
import Localize_Swift

class RestaurantListTableViewCell: UITableViewCell {
    // Id of cell
    static let cellID = "RestaurantCell"
    
    // define date format
    let dateFormatter = DateFormatter()
    
    // get outlet
    @IBOutlet weak var viewStackCard: UIView!
    @IBOutlet weak var imgPath: UIImageView!
    @IBOutlet weak var lbName: UILabel!
    @IBOutlet weak var lbAddress: UILabel!
    @IBOutlet weak var lbPhone: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        setLayout()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func setLayout(){
        viewStackCard.layer.masksToBounds = true
        viewStackCard.layer.cornerRadius = 2.5
        imgPath.layer.masksToBounds = true
        imgPath.layer.cornerRadius = 2.5
        dateFormatter.dateStyle = .medium
    }
    
    // Configure parameter table cell
    func configureCellWith (_ rest: Restaurant) {
        
        var url: URL? = nil
        
        if let str = rest.image_path {
            if let u = URL.init(string: str) {
                url = u
            }
        }
        
        if let fUrl = url {
            imgPath.setImageWith(fUrl, placeholderImage: Constant.NIL_IMAGE)
        } else {
            imgPath.image = UIImage.init(named: "no_img")
        }
        
        if Localize.currentLanguage() == "en" {
            lbName.text = rest.name
            if let addr = rest.address {
                lbAddress.text = String(format: NSLocalizedString("restaurant_item_lb_address".localized(), comment: ""), "\(addr)")
            }
        } else {
            lbName.text = rest.name_cn
            if let addr_cn = rest.address_cn {
                lbAddress.text = String(format: NSLocalizedString("restaurant_item_lb_address".localized(), comment: ""), "\(addr_cn)")
            }
        }
        
        if let phone = rest.phone {
            lbPhone.text = String(format: NSLocalizedString("restaurant_item_lb_phone".localized(), comment: ""), "\(phone)")
        }
    }
}
