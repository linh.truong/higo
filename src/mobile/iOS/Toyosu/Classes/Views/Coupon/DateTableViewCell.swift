//
//  DateTableViewCell.swift
//  Toyosu
//
//  Created by Linh Trương on 10/24/16.
//  Copyright © 2016 Linh Trương. All rights reserved.
//

import UIKit

class DateTableViewCell: UITableViewCell {
    
    @IBOutlet weak var lbFromDate: UILabel!
    @IBOutlet weak var lbToDate: UILabel!
    @IBOutlet weak var lbStartTime: UILabel!
    @IBOutlet weak var lbEndTime: UILabel!
    @IBOutlet weak var lbOpenDate: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
