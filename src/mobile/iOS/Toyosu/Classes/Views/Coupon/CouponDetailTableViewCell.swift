//
//  CouponDetailTableViewCell.swift
//  Toyosu
//
//  Created by Linh Trương on 12/8/16.
//  Copyright © 2016 IBS. All rights reserved.
//

import UIKit
import Localize_Swift

class CouponDetailTableViewCell: UITableViewCell {

    // Id of cell
    static let cellID = "CouponDetailCell"
    
    // get outlet
    @IBOutlet weak var viewBg: UIView!
    @IBOutlet weak var viewSaleOffBg: UIView!
    @IBOutlet weak var imgPath: UIImageView!
    @IBOutlet weak var imgViewHeight: NSLayoutConstraint!
    @IBOutlet weak var lbCouponName: UILabel!
    @IBOutlet weak var lbExiredDate: UILabel!
    @IBOutlet weak var lbComment: UILabel!
    @IBOutlet weak var lbSaleOff: UILabel!
    @IBOutlet weak var lbCondition: UILabel!
    @IBOutlet weak var lbConditionTitle: UILabel!
    @IBOutlet weak var lbExpiredDateTitle: UILabel!
    @IBOutlet weak var viewDotted: UIView!
    @IBOutlet weak var btnUse: UIButton!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        setLayout()
        presentImageView()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }
    
    func calculateImageSize(_ coupon: Coupon) {
        if let w = coupon.image_width {
            let imgWidth = CGFloat(w)
            if let h = coupon.image_height {
                let imgHeight = CGFloat(h)
                let imgViewWidth = ScreenSize.MAIN_BOUNDS.size.width - 36
                imgViewHeight.constant = (imgViewWidth * imgHeight) / imgWidth
            }
        }
    }
    
    func setLayout() {
        viewSaleOffBg.layer.cornerRadius = 3
        viewSaleOffBg.layer.masksToBounds = true
        btnUse.layer.cornerRadius = 5
        btnUse.backgroundColor = AppColor.PRIMARY_COLOR
        btnUse.setTitle("other_btn_use_prize2".localized(), for: .normal)
    }
    
    func presentImageView(){
        imgPath?.setupForImageViewer()
        imgPath?.setupForImageViewer(UIColor.black)
    }
}
