//
//  CouponListViewController.swift
//  Toyosu
//
//  Created by Linh Truong on 1/10/17.
//  Copyright © 2017 IBS. All rights reserved.
//

import UIKit
import Localize_Swift
import SVProgressHUD
import StatefulViewController

private let PAGINATION_LIMIT : Int = 6

class CouponListViewController: RightBarViewController, StatefulViewController {
    
    // Define variable
    var allCoupon : [Coupon] = []
    var willViewDetailCoupon: Coupon?
    let refreshControl = UIRefreshControl()
    var totalCoupons : Int = 0
    var currentPage : Int = 0
    var isLoadingMore = false
    let customer = CurrentCustomerInfo.shareInfo.loadCustomerInfo()
    
    // Connect outlet
    @IBOutlet var tblCoupon: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setCouponInit()
        
        refreshControl.addTarget(self, action: #selector(CouponListViewController.refresh(_:)), for: UIControlEvents.valueChanged)
        tblCoupon.addSubview(refreshControl)
        
        // Reload language localized after change
        NotificationCenter.default.addObserver(self, selector: #selector(CouponListViewController.refreshLanguage), name: NSNotification.Name(LCLLanguageChangeNotification), object: nil)
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    func setupErrorViewState() {
        let failureView = ErrorView(frame: view.frame)
        failureView.tapGestureRecognizer.addTarget(self, action: #selector(CouponListViewController.refreshWithIndi))
        errorView = failureView
    }
    
    func setupEmptyViewState() {
        let emptyView = EmptyView(frame: view.frame)
        errorView = emptyView
    }
    
    func setCouponInit() {
        edgesForExtendedLayout = []
        tblCoupon.delegate = self
        tblCoupon.dataSource = self
        navigationItem.title = "coupon_lb_title".localized()
        setupInitialViewState()
        setupErrorViewState()
        setupEmptyViewState()
        setScreenLayout()
        refreshWithIndi()
    }
    
    func setScreenLayout() {
        self.view.backgroundColor = AppColor.BACKGROUND_COLOR
        edgesForExtendedLayout = []
        tblCoupon.contentInset = UIEdgeInsetsMake(4, 0, 4, 0)
    }
    
    func refreshLanguage () {
        tblCoupon.reloadData()
        navigationItem.title = "coupon_lb_title".localized()
    }
    
    func refresh(_ sender: AnyObject) {
        loadCouponWithPage(0)
    }
    
    func refreshWithIndi() {
        SVProgressHUD.show()
        loadCouponWithPage(0)
    }
    
    func finishRefreshing() {
        SVProgressHUD.dismiss()
        tblCoupon.reloadData()
        refreshControl.endRefreshing()
        isLoadingMore = false
    }
    
    func loadCouponWithPage(_ page : Int) {
        currentPage = page
        
        if let cusId = customer?.id {
            APIService.sharedService.getCouponListCustom( cusId, PAGINATION_LIMIT, currentPage * PAGINATION_LIMIT, completed: { [weak self] (result, code, message, rsTotal, rsCoupon) in
                
                guard rsTotal > 0 else {
                    SVProgressHUD.dismiss()
                    self?.transitionViewStates(loading: false, error: NSError(domain: "error_no_data".localized(), code: 1, userInfo: nil), animated: false, completion: nil)
                    return
                }
                
                guard result else {
                    SVProgressHUD.dismiss()
                    self?.transitionViewStates(loading: false, error: NSError(domain: "error_internet_unavailable_title".localized(), code: 1, userInfo: nil), animated: false, completion: nil)
                    return
                }
                
                guard code != ErrorCode.ERROR_CODE_FAIL else {
                    SVProgressHUD.dismiss()
                    self?.transitionViewStates(loading: false, error: NSError(domain: "error_server_fail".localized(), code: 1, userInfo: nil), animated: false, completion: nil)
                    return
                }
                
                guard code != ErrorCode.ERROR_CODE_NOT_FOUND else {
                    SVProgressHUD.dismiss()
                    self?.transitionViewStates(loading: false, error: NSError(domain: "error_unknown".localized(), code: 1, userInfo: nil), animated: false, completion: nil)
                    return
                }
                // load home data
                self?.endLoading(error: nil)
                self?.finishCallApiWithCoupons(rsCoupon, rsTotal)
            })
        } else {
            APIService.sharedService.logOut( completed: { (result, error) in
                guard result else {
                    let alertController = UIAlertController(title: "common_info".localized(), message: "common_connection_fail".localized(), preferredStyle: UIAlertControllerStyle.alert)
                    let okAction = UIAlertAction(title: "common_ok".localized(), style: UIAlertActionStyle.default) { (result : UIAlertAction) -> Void in
                    }
                    alertController.addAction(okAction)
                    self.present(alertController, animated: true, completion: nil)
                    return
                }
                
                CurrentCustomerInfo.shareInfo.customer = nil
                CurrentCustomerInfo.shareInfo.persistenceCurrentInfo()
                let homeStoryboard = UIStoryboard(name: "Main", bundle: nil)
                let tabBar = homeStoryboard.instantiateViewController(withIdentifier: "tabBar")
                let appDelegate = UIApplication.shared.delegate as! AppDelegate
                appDelegate.window!.rootViewController = tabBar
                
            })
        }
    }
    
    func loadMoreCoupon() {
        
        if totalCoupons > allCoupon.count {
            self.endLoading(error: nil)
            isLoadingMore = true
            loadCouponWithPage(currentPage + 1)
        } else {
            self.transitionViewStates(loading: false, error: NSError(domain: "error_internet_unavailable_title".localized(), code: 1, userInfo: nil), animated: false, completion: nil)
        }
        
    }
    
    func finishCallApiWithCoupons(_ couponList: [Coupon]?, _ total: Int) {
        
        guard let coupons = couponList else {
            self.transitionViewStates(loading: false, error: NSError(domain: "error_server_fail".localized(), code: 1, userInfo: nil), animated: false, completion: nil)
            finishRefreshing()
            return
        }
        
        self.endLoading(error: nil)
        
        if (isLoadingMore) {
            allCoupon.append(contentsOf: coupons)
        } else {
            allCoupon = coupons
        }
        
        totalCoupons = total
        finishRefreshing()
    }
    
}

class MarginLabel : UILabel {
    override func drawText(in rect: CGRect) {
        super.drawText(in: rect.insetBy(dx: 10, dy: 0))
    }
}

extension CouponListViewController: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let label = MarginLabel()
        label.text = "coupon_lb_title_header".localized()
        label.numberOfLines = 0
        label.textAlignment = .center
        label.lineBreakMode = .byWordWrapping
        label.attributedText = NSAttributedString(string: label.text!, attributes: [NSFontAttributeName: UIFont.systemFont(ofSize: 15.0, weight: UIFontWeightSemibold)])
        label.textColor = AppColor.PRIMARY_COLOR
        
        return label
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 44.0
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if allCoupon.count < totalCoupons {
            return allCoupon.count + 1
        } else {
            return allCoupon.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let rowAtIndexPath = indexPath.row
        
        if rowAtIndexPath == allCoupon.count {
            let loadMoreCell = tableView.dequeueReusableCell(withIdentifier: LoadMoreTableViewCell.cellID, for: indexPath) as! LoadMoreTableViewCell
            
            return loadMoreCell
        } else {
            let couponCell = tableView.dequeueReusableCell(withIdentifier: CouponListTableViewCell.cellID, for: indexPath) as! CouponListTableViewCell
            
            couponCell.configureCellWith(allCoupon[rowAtIndexPath])
            
            return couponCell
        }
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if (indexPath.row == allCoupon.count) {
            loadMoreCoupon()
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let rowAtIndexPath = indexPath.row
        
        if rowAtIndexPath == allCoupon.count {
            return 44.0
        } else {
            tableView.estimatedRowHeight = 120.0
            tableView.rowHeight = UITableViewAutomaticDimension
            return tableView.rowHeight
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let rowAtIndexPath = indexPath.row
        
        if rowAtIndexPath < allCoupon.count {
            willViewDetailCoupon = self.allCoupon[rowAtIndexPath]
            performSegue(withIdentifier: CouponDetailViewController.segueID, sender: nil)
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if (segue.identifier == CouponDetailViewController.segueID) {
            let couponDetailVC = segue.destination as! CouponDetailViewController
            couponDetailVC.coupon = willViewDetailCoupon
            couponDetailVC.usedCouponCallback = { [weak self] cp in
                guard let s = self else{
                    return
                }
                delay(0.5, closure: {
                    if let index = s.allCoupon.index(of: cp) {
                        s.allCoupon.remove(at: index)
                        s.totalCoupons = s.totalCoupons - 1
                        s.tblCoupon.beginUpdates()
                        s.tblCoupon.deleteRows(at: [IndexPath.init(row: index, section: 0) ], with: .automatic)
                        s.tblCoupon.endUpdates()
                    }
                })
                
            }
        }
    }
}


extension CouponListViewController {
    
    func hasContent() -> Bool {
        return allCoupon.count > 0
    }
    
}
