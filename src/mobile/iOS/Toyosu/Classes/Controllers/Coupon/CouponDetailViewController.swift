//
//  CouponDetailViewController.swift
//  Toyosu
//
//  Created by Linh Trương on 10/21/16.
//  Copyright © 2016 IBS. All rights reserved.
//

import UIKit
import AFNetworking
import Localize_Swift
import StatefulViewController
import SVProgressHUD
import PopupDialog

class CouponDetailViewController: RightBarViewController, StatefulViewController {
    
    // Id of cell
    static let segueID = "ShowCouponDetail"
    
    var coupon: Coupon?
    var url: URL? = nil
    let dateFormatter = DateFormatter()
    var isLoadingRelated: Bool = true
    var relatedList: [CustomRelatedItems] = []
    let customer = CurrentCustomerInfo.shareInfo.loadCustomerInfo()
    // Closure callback
    var usedCouponCallback : ((_ cp: Coupon) -> ())?
    
    @IBOutlet weak var tblCouponDetail: UITableView!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        couponDetailInit()
        
        // Format date time to medium stype display
        dateFormatter.dateStyle = .medium
        
        // Reload localized after change
        NotificationCenter.default.addObserver(self, selector: #selector(CouponDetailViewController.refreshLanguage), name: NSNotification.Name(LCLLanguageChangeNotification), object: nil)
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    func couponDetailInit() {
        tblCouponDetail.delegate = self
        tblCouponDetail.dataSource = self
        tblCouponDetail.backgroundColor = AppColor.BACKGROUND_COLOR
        edgesForExtendedLayout = []
        dateFormatter.dateStyle = .long
        setupInitialViewState()
        setupErrorViewState()
        backNavLocalized()
        loadRelatedItemsData()
    }
    
    func backNavLocalized() {
        let backItem = UIBarButtonItem()
        backItem.title = "common_back".localized()
        navigationItem.backBarButtonItem = backItem
    }
    
    func setupErrorViewState() {
        let failureView = ErrorView(frame: view.frame)
        failureView.tapGestureRecognizer.addTarget(self, action: #selector(CouponDetailViewController.refreshLanguage))
        errorView = failureView
    }
    
    func refreshLanguage() {
        tblCouponDetail.reloadData()
        backNavLocalized()
    }
    
    func loadRelatedItemsData() {
        if let tagList = coupon?.hash_tags {
            let resId = coupon?.restaurant_id ?? ShopInfo.ID
            let id = coupon?.id ?? 0
            let objectType = 1
            
            APIService.sharedService.getRelatedItemsList(resId, customer?.id, id, objectType, tagList, Constant.RELATED_ITEMS_LIMIT, completed: { [weak self] (result, code, message, rsRelatedItems) in
                
                guard result else {
                    self?.isLoadingRelated = false
                    self?.transitionViewStates(loading: false, error: NSError(domain: "error_internet_unavailable_title".localized(), code: 1, userInfo: nil), animated: false, completion: nil)
                    return
                }
                
                self?.endLoading(error: nil)
                if let rs = rsRelatedItems {
                    self?.relatedList.removeAll()
                    self?.relatedList.append(contentsOf: rs)
                    
                }
                self?.isLoadingRelated = false
                self?.tblCouponDetail.reloadRows(at: [IndexPath.init(row: 1, section: 0)], with: .fade)
            })
        }
    }
    
    func showRelatedItem(_ relatedItems: CustomRelatedItems) {
        if let tabVC = navigationController?.tabBarController as? TabBarViewController {
            tabVC.showRelatedItem(relatedItems)
        }
    }
    
    func useCoupon(){
        SVProgressHUD.show()
        if let authToken = CurrentCustomerInfo.shareInfo.getToken() {
            APIService.sharedService.useCoupon(customer?.id, authToken, coupon?.id) { (result, code, mess) in
                
                guard result else {
                    SVProgressHUD.dismiss()
                    self.transitionViewStates(loading: false, error: NSError(domain: "error_internet_unavailable_title".localized(), code: 1, userInfo: nil), animated: false, completion: nil)
                    return
                }
                
                guard code != ErrorCode.ERROR_CODE_FAIL else {
                    SVProgressHUD.dismiss()
                    self.transitionViewStates(loading: false, error: NSError(domain: "error_internet_unavailable_title".localized(), code: 1, userInfo: nil), animated: false, completion: nil)
                    return
                }
                
                guard code != ErrorCode.ERROR_CODE_NOT_FOUND else {
                    SVProgressHUD.dismiss()
                    self.transitionViewStates(loading: false, error: NSError(domain: "error_unknown".localized(), code: 1, userInfo: nil), animated: false, completion: nil)
                    return
                }
                
                self.endLoading()
                SVProgressHUD.dismiss()
                self.showSuccessPopup()
            }
        }
    }
    
    func showSuccessPopup() {
        // Prepare data for popup
        let title = "common_info".localized()
        let message = "coupon_lb_use_success".localized()
        
        // Create the dialog
        let popup = PopupDialog(
            title: title,
            message: message,
            buttonAlignment: .horizontal,
            transitionStyle: .zoomIn,
            gestureDismissal: true
        ) {}
        
        // Add buttons to dialog
        popup.addButtons([DefaultButton(title: "common_ok".localized()) {
            self.usedCouponCallback?(self.coupon!)
            _ = self.navigationController?.popViewController(animated: true)
            }])
        
        // Present dialog
        self.present(popup, animated: true, completion: nil)
    }
}

extension CouponDetailViewController: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return 2
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        switch indexPath.row {
        case 0:
            let cell = tableView.dequeueReusableCell(withIdentifier: CouponDetailTableViewCell.cellID, for: indexPath) as! CouponDetailTableViewCell
          
            if let cp = coupon {
                cell.calculateImageSize(cp)
            }
            
            var url: URL? = nil
            
            if let str = coupon?.image_path {
                if let u = URL.init(string: str) {
                    url = u
                }
            }
            
            if let fUrl = url {
                cell.imgPath.setImageWith(fUrl)
            } else {
                cell.imgPath.image = UIImage.init(named: "no_img")
            }
            
            if Localize.currentLanguage() == "en" {
                if let name = coupon?.name {
                    cell.lbCouponName.text = name
                }
            } else {
                if let name_cn = coupon?.name_cn {
                    cell.lbCouponName.text = name_cn
                }
            }
            
            cell.lbExpiredDateTitle.text = "coupon_lb_expired_date_upper".localized()
            
            if let date = coupon?.to_date {
                if let time = coupon?.end_time {
                    cell.lbExiredDate.text = dateFormatter.string(from: date) + " " + time
                }
            }
            
            let price_off = coupon?.price_off
            let price_minus = coupon?.price_minus
            
            if (price_off != nil || price_minus == nil) {
                if let str_price_off = price_off?.toString() {
                    cell.lbSaleOff.text = String(format: NSLocalizedString("coupon_lb_sale_off".localized(), comment: ""), "\(Constant.CURRENCY_DOLLAR_ONLY)", "\(str_price_off)")
                }
            }
            
            if (price_off == nil || price_minus != nil) {
                if let str_price_minus = price_minus?.toString() {
                    cell.lbSaleOff.text = String(format: NSLocalizedString("coupon_lb_sale_off".localized(), comment: ""), "\(str_price_minus)", "\(Constant.CURRENCY_MINUS)")
                }
            }

            if Localize.currentLanguage() == "en" {
                if let title = coupon?.title {
                    cell.lbComment.text = title
                }
            } else {
                if let title_cn = coupon?.title_cn {
                    cell.lbComment.text = title_cn
                }
            }
            
            cell.lbConditionTitle.text = "coupon_lb_condition".localized()
            
            if Localize.currentLanguage() == "en" {
                if let cond = coupon?.condition {
                    cell.lbCondition.text = cond
                }
            } else {
                if let cond_cn = coupon?.condition_cn {
                    cell.lbCondition.text = cond_cn
                }
            }
            
            cell.btnUse.addTarget(self, action: #selector(self.useCoupon) , for: .touchUpInside)
            
            return cell
        case 1:
            let cell = tableView.dequeueReusableCell(withIdentifier: RelatedItemsTableViewCell.cellID, for: indexPath) as! RelatedItemsTableViewCell
            if isLoadingRelated {
                cell.showIndicator()
            } else {
                if cell.allItems == nil {
                    cell.allItems = self.relatedList
                    cell.finishRefreshing()
                }
                
                if cell.selectRelatedCallback == nil {
                    cell.selectRelatedCallback = { [weak self] (item, cell) in
                        self?.showRelatedItem(item)
                    }
                }
            }
            
            return cell
        default:
            
            return UITableViewCell()
        }
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        switch indexPath.row {
        case 0:
            tableView.estimatedRowHeight = 500.0
            tableView.rowHeight = UITableViewAutomaticDimension
            return tableView.rowHeight
        case 1:
            return 250.0
        default:
            return 0
        }
    }
    
}
