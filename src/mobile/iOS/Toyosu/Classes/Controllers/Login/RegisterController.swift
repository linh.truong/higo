//
//  ViewController.swift
//  Toyosu
//
//  Created by nguyen.trung.hau on 10/3/16.
//  Copyright © 2016 IBS. All rights reserved.
//

import UIKit
import Localize_Swift
import SVProgressHUD
import PopupDialog
import StatefulViewController

class RegisterController : BaseViewController, UITextFieldDelegate, StatefulViewController {
    
    var customerData:  Customers?
    var registerResult: NSDictionary?
    var selectedArea : Int? = 0
    var selectedAge: Int? = Constant.CUSTOMER_MIN_AGE
    var areaArr  = [Area]()
    var dateFormatter = DateFormatter()
    var selectedBirthday : Date? = nil
    let currentCalendar = Calendar.current
    let currentDate = Date()
    
    @IBOutlet weak var txtName: UITextField!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var txtEmail: UITextField!
    @IBOutlet weak var lblEmail: UILabel!
    @IBOutlet weak var txtPhone: UITextField!
    @IBOutlet weak var lblPhone: UILabel!
    @IBOutlet weak var txtPassword: UITextField!
    @IBOutlet weak var txtConfirmPassword: UITextField!
    @IBOutlet weak var lblPassword: UILabel!
    @IBOutlet weak var lblConfirmPassword: UILabel!
    @IBOutlet weak var scrollview: UIScrollView!
    @IBOutlet weak var lblAge: UILabel!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var btnSignUp: UIButton!
    @IBOutlet weak var segCtrlGender: UISegmentedControl!
    @IBOutlet weak var scrollView: RegisterScrollView!
    @IBOutlet weak var chkBoxPolicy: CheckBox!
    @IBOutlet weak var textAreaPolicy: UITextView!
    @IBOutlet weak var pickerResidentailArea: UIPickerView!
    @IBOutlet weak var lblGender: UILabel!
    @IBOutlet weak var lblResidentailAreas: UILabel!
    @IBOutlet weak var lblPrivacyPolicy: UILabel!
    @IBOutlet weak var lblAgreement: UILabel!
    @IBOutlet weak var birthdayDatePicker: MainDatePickerView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        screenInit()
        
        NotificationCenter.default.addObserver(self, selector: #selector(RegisterController.refreshLanguage), name: NSNotification.Name(LCLLanguageChangeNotification), object: nil)
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    func refreshLanguage () {
        setLangLocalized()
    }
    
    func screenInit() {
        dismissKeyboardWhenTapped()
        setScreenLayout()
        setLangLocalized()
        setDelegate()
        changeKeyboardLayout()
        loadAreaList()
        loadPrivacyPolicyInfo()
        addDoneButtonOnKeyboard()
        configureBirthdayPicker()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        textAreaPolicy.setContentOffset(CGPoint.zero, animated: false)
    }
    
    //set delegate for all items
    func setDelegate() {
        txtName.delegate = self
        txtEmail.delegate = self
        txtPhone.delegate = self
        txtPassword.delegate = self
        txtConfirmPassword.delegate = self
        pickerResidentailArea.delegate = self
    }
    
    func setScreenLayout(){
        scrollView.backgroundColor = AppColor.BACKGROUND_COLOR
        lblName.textColor = AppColor.DARK_GRAY_COLOR
        lblEmail.textColor = AppColor.DARK_GRAY_COLOR
        lblPhone.textColor = AppColor.DARK_GRAY_COLOR
        lblPassword.textColor = AppColor.DARK_GRAY_COLOR
        lblConfirmPassword.textColor = AppColor.DARK_GRAY_COLOR
        lblGender.textColor = AppColor.DARK_GRAY_COLOR
        lblAge.textColor = AppColor.DARK_GRAY_COLOR
        lblResidentailAreas.textColor = AppColor.DARK_GRAY_COLOR
        lblPrivacyPolicy.textColor = AppColor.DARK_GRAY_COLOR
        segCtrlGender.tintColor = AppColor.PRIMARY_COLOR
        textAreaPolicy.layer.cornerRadius = 5
        btnSignUp.layer.cornerRadius = 5
        btnSignUp.backgroundColor = AppColor.PRIMARY_COLOR
        pickerResidentailArea.isOpaque = true
        pickerResidentailArea.backgroundColor = AppColor.BACKGROUND_COLOR
        btnSignUp.backgroundColor = UIColor.lightGray
        btnSignUp.isUserInteractionEnabled = false
        chkBoxPolicy.addTarget(self, action: #selector(self.policyAgreed) , for: UIControlEvents.touchUpInside)
    }
    
    func setLangLocalized(){
        lblName.setRequiredAttribute("customer_lb_name".localized())
        lblEmail.setRequiredAttribute("customer_lb_email".localized())
        lblPhone.setRequiredAttribute("customer_lb_phone".localized())
        lblAge.text = "customer_lb_birthday".localized()
        lblPassword.setRequiredAttribute("customer_lb_password".localized())
        lblConfirmPassword.setRequiredAttribute("customer_lb_confirm_passwd".localized())
        lblPrivacyPolicy.text = "customer_lb_privacy_policy".localized()
        lblResidentailAreas.text = "customer_hint_residential_areas".localized()
        lblGender.text = "customer_lb_gender".localized()
        lblAgreement.text = "customer_register_policy_agree".localized()
        btnSignUp.setTitle("customer_register_btn_sign_up".localized(), for:.normal)
        segCtrlGender.setTitle("common_sex_male".localized(), forSegmentAt: 0)
        segCtrlGender.setTitle("common_sex_female".localized(), forSegmentAt: 1)
        segCtrlGender.setTitle("common_sex_unknown".localized(), forSegmentAt: 2)
        segCtrlGender.selectedSegmentIndex = 2
    }
    
    func loadAreaList(){
        
        SVProgressHUD.show()
        
        APIService.sharedService.getAreaList { (result, message, code, data) in
            
            guard result else {
                SVProgressHUD.dismiss()
                self.transitionViewStates(loading: false, error: NSError(domain: "error_internet_unavailable_title".localized(), code: 1, userInfo: nil), animated: false, completion: nil)
                return
            }
            
            guard code != ErrorCode.ERROR_CODE_FAIL else {
                SVProgressHUD.dismiss()
                self.transitionViewStates(loading: false, error: NSError(domain: "error_server_fail".localized(), code: 1, userInfo: nil), animated: false, completion: nil)
                return
            }
            
            self.endLoading(error: nil)
            
            // Area list
            for area in data!
            {
                self.areaArr.append(area)
            }
            
            if let areaIdDefault = data?.first?.id {
                self.selectedArea = areaIdDefault
            }
            
            self.pickerResidentailArea.reloadAllComponents()
            
            SVProgressHUD.dismiss()
        }
    }
    
    func loadPrivacyPolicyInfo() {
        
        SVProgressHUD.show()
        
        guard let setting = CurrentCustomerInfo.shareInfo.loadSettingInfo() else {
            SVProgressHUD.dismiss()
            self.transitionViewStates(loading: false, error: NSError(domain: "error_server_fail".localized(), code: 1, userInfo: nil), animated: false, completion: nil)
            return
        }
        
        guard let i = setting.index(where: { $0.key == AppSetting.SETTING_PRIVACY_POLICY }) else {
            SVProgressHUD.dismiss()
            self.transitionViewStates(loading: false, error: NSError(domain: "error_server_fail".localized(), code: 1, userInfo: nil), animated: false, completion: nil)
            return
        }
        // End loading error page
        self.endLoading(error: nil)
        // Start get value from key
        let settingWithIndex : Setting = setting[i]
        // Set value to text view
        if Localize.currentLanguage() == "en" {
            self.textAreaPolicy.text = settingWithIndex.value
        } else {
            self.textAreaPolicy.text = settingWithIndex.value_cn
        }
        
        SVProgressHUD.dismiss()
    }
    
    func configureBirthdayPicker(){
        birthdayDatePicker.datePickerMode = .date
        birthdayDatePicker.locale = Locale.current
        
        var componentsMin = currentCalendar.dateComponents([.year], from: currentDate)
        var componentsMax = componentsMin
        if let yearMin = componentsMin.year {
            let yearMinimum = yearMin - Constant.CUSTOMER_MAX_AGE
            componentsMin.year = yearMinimum
            let minimumDate : Date = currentCalendar.date(from: componentsMin)!
            birthdayDatePicker.minimumDate = minimumDate
        }
        if let yearMax = componentsMax.year {
            let yearMaximum = yearMax - Constant.CUSTOMER_MIN_AGE
            componentsMax.year = yearMaximum
            let maximumDate : Date = currentCalendar.date(from: componentsMax)!
            birthdayDatePicker.maximumDate = maximumDate
        }
        
        for subview in self.birthdayDatePicker.subviews {
            
            if subview.frame.height <= 5 {
                
                subview.backgroundColor = UIColor.white
                subview.tintColor = UIColor.white
                subview.layer.borderColor = AppColor.PRIMARY_COLOR.cgColor
                subview.layer.borderWidth = 0.5            }
        }
        
        if let pickerView = self.birthdayDatePicker.subviews.first {
            
            for subview in pickerView.subviews {
                
                if subview.frame.height <= 5 {
                    subview.backgroundColor = UIColor.white
                    subview.tintColor = UIColor.white
                    subview.layer.borderColor = AppColor.PRIMARY_COLOR.cgColor
                    subview.layer.borderWidth = 0.5
                }
            }
        }
        
    }
    
    @IBAction func birthdayIndexChanged(_ sender: UIDatePicker) {
        let yearNow = currentDate.getYear()
        self.selectedBirthday = sender.date
        let yearChoose = self.selectedBirthday?.getYear()
        self.selectedAge = yearNow - yearChoose!
    }
    
    
    @IBAction func signUp(_ sender: AnyObject) {
        
        // validate for name
        if let name = txtName.text {
            
            guard name != "" else {
                txtName.becomeFirstResponder()
                lblName.text = "customer_lb_name_required".localized()
                lblName.textColor = AppColor.PRIMARY_COLOR
                return
            }
            
            guard name.characters.count > 2 else {
                txtName.becomeFirstResponder()
                lblName.text = "customer_lb_name_invalid".localized()
                lblName.textColor = AppColor.PRIMARY_COLOR
                return
            }
        }
        
        // validate for email
        if let email = txtEmail.text {
            
            guard email.characters.count > 0 else {
                txtEmail.becomeFirstResponder()
                lblEmail.text = "customer_lb_email_required".localized()
                lblEmail.textColor = AppColor.PRIMARY_COLOR
                return
            }
            
            guard email.isEmail(email: email) else {
                txtEmail.becomeFirstResponder()
                lblEmail.text = "customer_lb_email_invalid".localized()
                lblEmail.textColor = AppColor.PRIMARY_COLOR
                return
            }
        }
        
        if let phone = txtPhone.text {
            
            guard phone.isPhoneNumber(phoneNumber: phone) else {
                txtPhone.becomeFirstResponder()
                lblPhone.text = "customer_lb_phone_invalid".localized()
                lblPhone.textColor = AppColor.PRIMARY_COLOR
                return
            }
        }
        
        if let password = txtPassword.text {
            guard password != "" else {
                txtPassword.becomeFirstResponder()
                lblPassword.text = "customer_register_pw_required".localized()
                lblPassword.textColor = AppColor.PRIMARY_COLOR
                return
            }
            guard password.characters.count >= 6 else {
                txtPassword.becomeFirstResponder()
                lblPassword.text = "customer_register_pw_min_length".localized()
                lblPassword.textColor = AppColor.PRIMARY_COLOR
                return
            }
            guard checkPassComplexity(password: password) else {
                txtPassword.becomeFirstResponder()
                lblPassword.text = "customer_lb_password_complexity".localized()
                lblPassword.textColor = AppColor.PRIMARY_COLOR
                return
            }
        }
        
        if let confirmPassword = txtConfirmPassword.text {
            guard confirmPassword.characters.count >= 6 else {
                txtConfirmPassword.becomeFirstResponder()
                lblConfirmPassword.text = "customer_register_pw_min_length".localized()
                lblConfirmPassword.textColor = AppColor.PRIMARY_COLOR
                return
            }
            guard confirmPassword == txtPassword.text else {
                txtConfirmPassword.becomeFirstResponder()
                lblConfirmPassword.text = "customer_register_pw_not_match".localized()
                lblConfirmPassword.textColor = AppColor.PRIMARY_COLOR
                return
            }
        }
        
        registerCustomer()
    }
    
    func registerCustomer(){
        
        activityIndicator.startAnimating()
        
        let selectedAreaIndex = pickerResidentailArea.selectedRow(inComponent: 0)
        let selectedAreaIndexValue = areaArr[selectedAreaIndex].id
        
        customerData = Customers(id: nil,
                                 area: self.selectedArea ?? selectedAreaIndexValue,
                                 restaurant: Constant.DEFAULT_SHOP_INFO_ID,
                                 number: nil,
                                 name: txtName.text,
                                 image_path: nil,
                                 age: self.selectedAge ?? Constant.CUSTOMER_MIN_AGE,
                                 birthday: self.selectedBirthday ?? birthdayDatePicker.maximumDate,
                                 sex: segCtrlGender.selectedSegmentIndex,
                                 address: "",
                                 gps_data: "",
                                 email: txtEmail.text,
                                 login_password: txtPassword.text,
                                 phone: txtPhone.text ?? nil,
                                 last_transaction_date: Date(),
                                 current_point: nil,
                                 total_point: nil,
                                 is_mail_notify: nil,
                                 is_sms_notify: nil,
                                 recover_password: nil)
        
        let jsonCustomer = customerData!.toJSONString()
        
        // Call API
        APIService.sharedService.registerCustomer(jsonCustomer, { (result, msg, error) in
            
            guard result else {
                self.showErrorPopup()
                return
            }
            
            guard error != ErrorCode.ERROR_CODE_FAIL else {
                self.showErrorPopup()
                return
            }
            
            guard error != ErrorCode.ERROR_CODE_USER_INSERT_EXIST else {
                
                let message = msg?.characters.split(separator: " ").map(String.init)[0]
                
                if message == "Email" {
                    self.txtEmail.becomeFirstResponder()
                    self.lblEmail.text = "customer_lb_email_exist".localized()
                    self.lblEmail.textColor = AppColor.PRIMARY_COLOR
                }
                
                if message == "Phone" {
                    self.txtPhone.becomeFirstResponder()
                    self.lblPhone.text = "customer_lb_phone_exist".localized()
                    self.lblPhone.textColor = AppColor.PRIMARY_COLOR
                }
                
                return
            }
            
            self.activityIndicator.stopAnimating()
            self.showSuccessPopup()
        })
    }
    
    func showSuccessPopup() {
        // Prepare data for popup
        let title = "common_info".localized()
        let message = "customer_msg_register_successfully".localized()
        
        // Create the dialog
        let popup = PopupDialog(
            title: title,
            message: message,
            buttonAlignment: .horizontal,
            transitionStyle: .zoomIn,
            gestureDismissal: true
        ) {}
        
        // Add buttons to dialog
        popup.addButtons([DefaultButton(title: "common_ok".localized()) {
            _ = self.navigationController?.popViewController(animated: true)
            }])
        
        // Present dialog
        self.present(popup, animated: true, completion: nil)
        
    }
    
    func showErrorPopup() {
        // Prepare data for popup
        let title = "common_info".localized()
        let message = "common_connection_fail".localized()
        
        // Create the dialog
        let popup = PopupDialog(
            title: title,
            message: message,
            buttonAlignment: .horizontal,
            transitionStyle: .zoomIn,
            gestureDismissal: true
        ) {}
        
        // Add buttons to dialog
        popup.addButtons([DefaultButton(title: "common_ok".localized()) {
            }])
        
        // Present dialog
        self.present(popup, animated: true, completion: nil)
        
    }
    
    //set sign up button state
    func validButton (isValidated: DarwinBoolean){
        
        if isValidated.boolValue == true
        {
            self.btnSignUp.backgroundColor = AppColor.PRIMARY_COLOR
            self.btnSignUp.isUserInteractionEnabled = true
        }else{
            self.btnSignUp.backgroundColor = UIColor.lightGray
            self.btnSignUp.isUserInteractionEnabled = false
        }
        
    }
    
    // handle event for all textfield and checkbox
    @objc func policyAgreed() {
        
        if(self.chkBoxPolicy.isChecked == true )
        {
            validButton(isValidated: true)
        }else{
            validButton(isValidated: false)
        }
    }
    
    func addDoneButtonOnKeyboard() {
        let doneToolbar: UIToolbar = UIToolbar(frame: CGRect(x: 0, y: 0, width: 320, height: 50))
        doneToolbar.barStyle       = UIBarStyle.default
        let flexSpace              = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        let done: UIBarButtonItem  = UIBarButtonItem(title: "common_done".localized(), style: UIBarButtonItemStyle.done, target: self, action: #selector(RegisterController.doneButtonAction))
        
        var items = [UIBarButtonItem]()
        items.append(flexSpace)
        items.append(done)
        
        doneToolbar.items = items
        doneToolbar.sizeToFit()
        
        txtName.inputAccessoryView = doneToolbar
        txtEmail.inputAccessoryView = doneToolbar
        txtPhone.inputAccessoryView = doneToolbar
        txtPassword.inputAccessoryView = doneToolbar
        txtConfirmPassword.inputAccessoryView = doneToolbar
    }
    
    func doneButtonAction() {
        txtName.resignFirstResponder()
        txtEmail.resignFirstResponder()
        txtPhone.resignFirstResponder()
        txtPassword.resignFirstResponder()
        txtConfirmPassword.resignFirstResponder()
    }
    
    //change layout of keyBoard
    func changeKeyboardLayout(){
        txtName.returnKeyType = UIReturnKeyType.next
        txtEmail.returnKeyType = UIReturnKeyType.next
        txtPhone.returnKeyType = UIReturnKeyType.next
        txtPassword.returnKeyType = UIReturnKeyType.next
        txtConfirmPassword.returnKeyType = UIReturnKeyType.next
        
    }
    
    //validate textfiel length
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if textField.placeholder == "customer_lb_name".localized() {
            if (txtName.text?.characters.count)! > 49  && range.length == 0
            {
                return false
            }
        }
        if textField.placeholder == "customer_lb_email".localized() {
            if (txtEmail.text?.characters.count)! > 49  && range.length == 0
            {
                return false
            }
        }
        if textField.placeholder == "customer_lb_phone".localized() {
            if (txtPhone.text?.characters.count)! > 7  && range.length == 0
            {
                return false
            }
        }
        
        if textField.placeholder == "customer_lb_password".localized() {
            if (txtPassword.text?.characters.count)! > 11  && range.length == 0{
                return false
            }
        }
        if textField.placeholder == "customer_lb_confirm_passwd".localized() {
            if (txtConfirmPassword.text?.characters.count)! > 11  && range.length == 0{
                return false
            }
        }
        
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == self.txtName {
            self.txtEmail.becomeFirstResponder()
        }
        
        if textField == self.txtEmail {
            self.txtPhone.becomeFirstResponder()
        }
        
        if textField == self.txtPhone {
            self.txtPassword.becomeFirstResponder()
        }
        
        if textField == self.txtPassword {
            self.txtConfirmPassword.becomeFirstResponder()
        }
        
        return false
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        
        if textField == txtName {
            lblName.textColor = AppColor.DARK_GRAY_COLOR
            lblName.setRequiredAttribute("customer_lb_name".localized())
        }
        
        if textField == txtEmail {
            lblEmail.textColor = AppColor.DARK_GRAY_COLOR
            lblEmail.setRequiredAttribute("customer_lb_email".localized())
        }
        
        if textField == txtPhone {
            lblPhone.textColor = AppColor.DARK_GRAY_COLOR
            lblPhone.setRequiredAttribute("customer_lb_phone".localized())
        }
        
        if textField == txtPassword {
            lblPassword.textColor = AppColor.DARK_GRAY_COLOR
            lblPassword.setRequiredAttribute("customer_lb_password".localized())
        }
        
        if textField == txtConfirmPassword {
            lblConfirmPassword.textColor = AppColor.DARK_GRAY_COLOR
            lblConfirmPassword.setRequiredAttribute("customer_lb_confirm_passwd".localized())
        }
    }
    
    func checkPassComplexity(password: String) -> Bool{
        
        var regex  = ".*[a-z]+.*|.*[A-Z]+.*"
        var regChecker = NSPredicate(format:"SELF MATCHES %@", regex)
        let characterResult = regChecker.evaluate(with: password)
        
        regex  = ".*[0-9]+.*"
        regChecker = NSPredicate(format:"SELF MATCHES %@", regex)
        let numberResult = regChecker.evaluate(with: password)
        
        return numberResult && characterResult
    }
    
}

extension RegisterController : UIPickerViewDelegate, UIPickerViewDataSource {
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return areaArr.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        
        if Localize.currentLanguage() == "en" {
            return areaArr[row].name
        } else {
            return areaArr[row].name_cn
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        self.selectedArea = areaArr[row].id
    }
    
    func pickerView(_ pickerView: UIPickerView, viewForRow row: Int, forComponent component: Int, reusing view: UIView?) -> UIView {
        
        let pickerLabel = UILabel()
        pickerLabel.textColor = UIColor.black
        pickerLabel.font = UIFont.systemFont(ofSize: 22)
        pickerLabel.textAlignment = NSTextAlignment.center
        
        if Localize.currentLanguage() == "en" {
            pickerLabel.text = areaArr[row].name
        } else {
            pickerLabel.text = areaArr[row].name_cn
        }
//        pickerLabel.backgroundColor = UIColor.white
        
        return pickerLabel
    }
}

