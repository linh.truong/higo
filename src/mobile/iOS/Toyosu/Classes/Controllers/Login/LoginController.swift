//
//  ViewController.swift
//  Toyosu
//
//  Created by nguyen.trung.hau on 10/3/16.
//  Copyright © 2016 IBS. All rights reserved.
//

import UIKit
import Localize_Swift
import PopupDialog

class LoginController: UIViewController, UITextFieldDelegate {
    
    var sessionExpired: Bool = false
    
    var previousViewController: UIViewController?
    
    @IBOutlet weak var lbEmailStatus: UILabel!
    @IBOutlet weak var txtEmail: UITextField!
    @IBOutlet weak var lbPasswordStatus: UILabel!
    @IBOutlet weak var txtPassword: UITextField!
    @IBOutlet weak var btnLogin: UIButton!
    @IBOutlet weak var btnRegister: UIButton!
    @IBOutlet weak var btnForgotPassword: UIButton!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    override func viewDidAppear(_ animated: Bool) {
        
        //check session
        if sessionExpired == true
        {
            //show error message
            let alertController = UIAlertController(title: "common_info".localized(), message: "customer_lb_login_session_expired".localized(), preferredStyle: UIAlertControllerStyle.alert)
            
            let okAction = UIAlertAction(title: "common_ok".localized(), style: UIAlertActionStyle.default) { (result : UIAlertAction) -> Void in
            }
            alertController.addAction(okAction)
            
            self.present(alertController, animated: true, completion: nil)
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        screenInit()
        
        NotificationCenter.default.addObserver(self, selector: #selector(LoginController.refreshLanguage), name: NSNotification.Name(LCLLanguageChangeNotification), object: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        self.navigationController?.isNavigationBarHidden = false
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    func refreshLanguage() {
        setLangLocalized()
    }
    
    func screenInit() {
        setDelegate()
        setScreenLayout()
        setLangLocalized()
        addDoneButtonOnKeyboard()
        dismissKeyboardWhenTapped()
    }
    
    func setDelegate() {
        self.txtEmail.delegate = self
        self.txtPassword.delegate = self
    }
    
    func setLangLocalized() {
        self.title = "customer_lb_login".localized()
        lbEmailStatus.setRequiredAttribute("customer_lb_email".localized())
        lbPasswordStatus.setRequiredAttribute("customer_lb_password".localized())
        btnLogin.setTitle("customer_lb_login".localized(), for:.normal)
        btnForgotPassword.setTitle("customer_hint_login_forgot_pw".localized(), for: .normal)
        btnRegister.setTitle("customer_hint_login_sign_up".localized(), for: .normal)
    }
    func setScreenLayout() {
        txtEmail.returnKeyType = UIReturnKeyType.next
        txtPassword.returnKeyType = UIReturnKeyType.go
        txtEmail.addTarget(self, action: #selector(self.textFieldDidChange) , for: UIControlEvents.editingChanged)
        txtPassword.addTarget(self, action: #selector(self.textFieldDidChange) , for: UIControlEvents.editingChanged)
        btnLogin.layer.cornerRadius = 5
        btnLogin.backgroundColor = UIColor.lightGray
        btnLogin.isUserInteractionEnabled = false
        self.view.backgroundColor = AppColor.BACKGROUND_COLOR
        lbEmailStatus.textColor = AppColor.DARK_GRAY_COLOR
        lbPasswordStatus.textColor = AppColor.DARK_GRAY_COLOR
    }
    
    @IBAction func goToHome(_ sender: Any) {
        CurrentCustomerInfo.shareInfo.customer = nil
        let homeStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let tabBar = homeStoryboard.instantiateViewController(withIdentifier: "tabBar")
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.window!.rootViewController = tabBar
    }
    
    @objc private func textFieldDidChange() {
        if let phone = txtEmail?.text , let password = txtPassword?.text
        {
            if(phone != "" && password != "" ){
                btnLogin.backgroundColor = AppColor.PRIMARY_COLOR
                btnLogin.isUserInteractionEnabled = true
            }else{
                btnLogin.backgroundColor = UIColor.lightGray
                btnLogin.isUserInteractionEnabled = false
            }
        }
    }
    
    // Create a UIViewController
    static func makeLoginViewController(sessionExpired: Bool, previousViewController: UIViewController) -> LoginController {
        
        let mainStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let loginViewController = mainStoryboard.instantiateViewController(withIdentifier: "Login") as! LoginController
        
        loginViewController.sessionExpired = sessionExpired
        loginViewController.previousViewController = previousViewController
        
        return loginViewController
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if textField.placeholder == "customer_lb_email".localized() {
            if (txtEmail.text?.characters.count)! > 49  && range.length == 0
            {
                return false
            }
        }
        if textField.placeholder == "customer_lb_password".localized() {
            if (txtPassword.text?.characters.count)! > 11  && range.length == 0{
                return false
            }
        }
        return true
    }
    
    //Handdle event for next button and Go buttohn
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        if textField == txtEmail {
            txtPassword.becomeFirstResponder()
        }
        
        return false
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField == txtEmail {
            lbEmailStatus.textColor = AppColor.DARK_GRAY_COLOR
            lbEmailStatus.setRequiredAttribute("customer_lb_email".localized())
        }
        
        if textField == txtPassword {
            lbPasswordStatus.textColor = AppColor.DARK_GRAY_COLOR
            lbPasswordStatus.setRequiredAttribute("customer_lb_password".localized())
        }
    }
    
    func addDoneButtonOnKeyboard() {
        let doneToolbar: UIToolbar = UIToolbar(frame: CGRect(x: 0, y: 0, width: 320, height: 50))
        doneToolbar.barStyle       = UIBarStyle.default
        let flexSpace              = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        let done: UIBarButtonItem  = UIBarButtonItem(title: "common_done".localized(), style: UIBarButtonItemStyle.done, target: self, action: #selector(LoginController.doneButtonAction))
        
        var items = [UIBarButtonItem]()
        items.append(flexSpace)
        items.append(done)
        
        doneToolbar.items = items
        doneToolbar.sizeToFit()
        
        txtPassword.inputAccessoryView = doneToolbar
    }
    
    func doneButtonAction() {
        txtEmail.resignFirstResponder()
        txtPassword.resignFirstResponder()
    }
    
    @IBAction func btnLoginAction(_ sender: Any) {
        
        // validate for email
        if let email = txtEmail.text {
            
            guard email.characters.count > 0 else {
                txtEmail.becomeFirstResponder()
                lbEmailStatus.text = "customer_lb_email_required".localized()
                lbEmailStatus.textColor = AppColor.PRIMARY_COLOR
                return
            }
            
            guard email.isEmail(email: email) else {
                txtEmail.becomeFirstResponder()
                lbEmailStatus.text = "customer_lb_email_invalid".localized()
                lbEmailStatus.textColor = AppColor.PRIMARY_COLOR
                return
            }
        }
        
        // validate for phone number: only digits, 8 digits no less no more
        if let pw = txtPassword.text {
            
            guard pw.characters.count > 0 else {
                txtPassword.becomeFirstResponder()
                lbPasswordStatus.text = "customer_register_pw_required".localized()
                lbPasswordStatus.textColor = AppColor.PRIMARY_COLOR
                return
            }
        }
        
        login()
    }
    
    func login(){
        
        activityIndicator.startAnimating()
        
        // prevent user interract
        UIApplication.shared.beginIgnoringInteractionEvents()
        btnLogin.isUserInteractionEnabled = true
        
        // check user login info
        let loginParams: NSDictionary = ["username": txtEmail.text! as String, "password": txtPassword.text! as String]
        
        APIService.sharedService.login(loginParams, completed: { (result, msg, error, data) in
            
            guard result else {
                UIApplication.shared.endIgnoringInteractionEvents()
                self.activityIndicator.stopAnimating()
                self.showErrorPopup()
                return
            }
            
            guard error != ErrorCode.ERROR_CODE_LOGIN_FAIL else {
                self.activityIndicator.stopAnimating()
                UIApplication.shared.endIgnoringInteractionEvents()
                self.lbPasswordStatus.text = "customer_lb_login_fail".localized()
                self.lbPasswordStatus.textColor = AppColor.PRIMARY_COLOR
                return
            }
            
            guard error != ErrorCode.ERROR_CODE_FAIL else {
                self.activityIndicator.stopAnimating()
                UIApplication.shared.endIgnoringInteractionEvents()
                self.lbPasswordStatus.text = "customer_lb_login_fail".localized()
                self.lbPasswordStatus.textColor = AppColor.PRIMARY_COLOR
                return
            }
            
            guard error != ErrorCode.ERROR_CODE_LOGIN_USER_PWD_REQUIRED else {
                self.activityIndicator.stopAnimating()
                UIApplication.shared.endIgnoringInteractionEvents()
                self.lbPasswordStatus.text = "error_auth_fail".localized()
                self.lbPasswordStatus.textColor = AppColor.PRIMARY_COLOR
                return
            }
            
            guard error != ErrorCode.ERROR_CODE_LOGIN_USER_INACTIVE else {
                self.activityIndicator.stopAnimating()
                UIApplication.shared.endIgnoringInteractionEvents()
                self.lbPasswordStatus.text = "error_auth_fail".localized()
                self.lbPasswordStatus.textColor = AppColor.PRIMARY_COLOR
                return
            }
            
            guard error != ErrorCode.ERROR_CODE_LOGIN_USER_NOT_PERMISSION else {
                self.activityIndicator.stopAnimating()
                UIApplication.shared.endIgnoringInteractionEvents()
                self.lbPasswordStatus.text = "customer_lb_login_fail".localized()
                self.lbPasswordStatus.textColor = AppColor.PRIMARY_COLOR
                return
            }
            
            UIApplication.shared.endIgnoringInteractionEvents()
            CurrentCustomerInfo.shareInfo.customer = data
            CurrentCustomerInfo.shareInfo.saveCustomerInfo(data)
            
            self.activityIndicator.stopAnimating()
            self.txtPassword.text = nil
            
            self.performSegue(withIdentifier: "show_home_vc", sender: nil)
        })
    }
    
    func showErrorPopup() {
        // Prepare data for popup
        let title = "common_info".localized()
        let message = "common_connection_fail".localized()
        
        // Create the dialog
        let popup = PopupDialog(
            title: title,
            message: message,
            buttonAlignment: .horizontal,
            transitionStyle: .zoomIn,
            gestureDismissal: true
        ) {}
        
        // Add buttons to dialog
        popup.addButtons([DefaultButton(title: "common_ok".localized()) {}])
        
        // Present dialog
        self.present(popup, animated: true, completion: nil)
        
    }
    
    @objc func handleTap(gestureRecognizer: UIGestureRecognizer) {
        let registerViewController = self.storyboard!.instantiateViewController(withIdentifier: "register")
        self.navigationController?.pushViewController(registerViewController, animated: false)
    }
}

