//
//  ForgotPasswordViewController.swift
//  Toyosu
//
//  Created by Linh Trương on 10/26/16.
//  Copyright © 2016 IBS. All rights reserved.
//

import UIKit
import SVProgressHUD
import Localize_Swift
import StatefulViewController
import PopupDialog

class ForgotPasswordViewController: BaseViewController, StatefulViewController {
    
    @IBOutlet weak var btnRecovery: UIButton!
    @IBOutlet weak var txtInput: UITextField!
    @IBOutlet weak var lbStatus: UILabel!
    @IBOutlet weak var lblForgot: UILabel!
    @IBOutlet weak var lblForgotTitle: UILabel!
    @IBOutlet weak var lineView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupErrorViewState()
        setLayout()
        setLangLocalized()
        dismissKeyboardWhenTapped()
        
        NotificationCenter.default.addObserver(self, selector: #selector(ForgotPasswordViewController.refreshLanguage), name: NSNotification.Name(LCLLanguageChangeNotification), object: nil)
    }
    
    func setLayout() {
        btnRecovery.layer.cornerRadius = 5
        btnRecovery.backgroundColor = AppColor.PRIMARY_COLOR
        lbStatus.textColor = AppColor.DARK_GRAY_COLOR
        lblForgot.textColor = AppColor.DARK_GRAY_COLOR
        lblForgotTitle.textColor = AppColor.DARK_GRAY_COLOR
        lineView.backgroundColor = AppColor.DARK_GRAY_COLOR
    }
    
    func setLangLocalized() {
        lblForgotTitle.text = "customer_lb_recover_password_trouble_signin".localized()
        lblForgot.text = "customer_lb_recover_password_hint".localized()
        btnRecovery.setTitle("customer_btn_recover".localized(), for: .normal)
    }
    
    func refreshLanguage() {
        setLangLocalized()
    }
    
    func showSuccessPopup() {
        // Prepare data for popup
        let title = "common_info".localized()
        let message = "customer_lb_recover_password_success".localized()
        
        // Create the dialog
        let popup = PopupDialog(
            title: title,
            message: message,
            buttonAlignment: .horizontal,
            transitionStyle: .zoomIn,
            gestureDismissal: true
        ) {}
        
        // Add buttons to dialog
        popup.addButtons([DefaultButton(title: "common_ok".localized()) {
            _ = self.navigationController?.popViewController(animated: true)
            }])
        
        // Present dialog
        self.present(popup, animated: true, completion: nil)

    }
    
    @IBAction func pressBtnRecover(_ sender: AnyObject) {
        
        var isValid = true
        
        if let input = txtInput.text {
            if(input == ""){
                lbStatus.isHidden = false
                lbStatus.text = "customer_lb_recover_password_required".localized()
                lbStatus.textColor = AppColor.PRIMARY_COLOR
                isValid = false
            } else {
                lbStatus.isHidden = true
            }
        }
        if (isValid == true) {
            recover()
        }
    }
    
    func setupErrorViewState() {
        let failureView = ErrorView(frame: view.frame)
        failureView.tapGestureRecognizer.addTarget(self, action: #selector(ForgotPasswordViewController.refreshLanguage))
        errorView = failureView
    }
    
    func recover() {
        
        SVProgressHUD.show()
        
        if let cus_info = txtInput.text {
            let cur_lang = Localize.currentLanguage()
            APIService.sharedService.recoverPassword(cus_info, cur_lang, completed: { (result, error, message) in
                if error == ErrorCode.ERROR_CODE_SUCCESS {
                    SVProgressHUD.dismiss()
                    
                    self.showSuccessPopup()
                } else {
                    self.txtInput.becomeFirstResponder()
                    self.lbStatus.isHidden = false
                    self.lbStatus.text = "customer_lb_recover_password_fail".localized()
                    self.lbStatus.textColor = AppColor.PRIMARY_COLOR
                }
                SVProgressHUD.dismiss()
            })
            
        }
    }
    
}
