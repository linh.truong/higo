//
//  CustomerBenefitsViewController.swift
//  Roann
//
//  Created by Linh Trương on 4/13/17.
//  Copyright © 2017 IBS. All rights reserved.
//

import UIKit
import Localize_Swift

class CustomerBenefitsViewController: UIViewController {

    @IBOutlet weak var viewBackground: UIView!
    @IBOutlet weak var textView: UITextView!
    @IBOutlet weak var btnClose: UIButton!
    
    static let segueID = "ShowBenefits"
    var content : String?
    var contentCn: String?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        setScreenLayout()
        controlGesture()
        loadData()
    }

    func setScreenLayout(){
        textView.scrollRangeToVisible((NSMakeRange(0,0)))
        viewBackground.layer.cornerRadius = 15
        viewBackground.clipsToBounds = false
        textView.layer.cornerRadius = 15
        textView.clipsToBounds = false
        btnClose.layer.cornerRadius = 5
        btnClose.backgroundColor = AppColor.PRIMARY_COLOR
        btnClose.setTitle("common_btn_close".localized(), for: .normal)
    }
    
    func loadData(){
        if Localize.currentLanguage() == "en" {
            textView.text = content
        } else {
            textView.text = contentCn
        }
    }
    
    func controlGesture(){
        let tapGesture = UITapGestureRecognizer.init(target: self, action: #selector(CustomerBenefitsViewController.dismissPopup))
        self.view.addGestureRecognizer(tapGesture)
    }
    
    func dismissPopup(){
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func tapToClose(_ sender: Any) {
        dismissPopup()
    }
    
}
