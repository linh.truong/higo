//
//  CustomerBenefitTCsViewController.swift
//  Roann
//
//  Created by Linh Trương on 4/27/17.
//  Copyright © 2017 IBS. All rights reserved.
//

import UIKit
import SVProgressHUD
import Localize_Swift
import StatefulViewController

class CustomerBenefitTCsViewController: UIViewController, StatefulViewController {

    static let segueID = "ShowBenefitTC"
    
    @IBOutlet weak var viewBackground: UIView!
    @IBOutlet weak var textView: UITextView!
    @IBOutlet weak var btnClose: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        setupInitialViewState()
        setupErrorViewState()
        setScreenLayout()
        controlGesture()
        loadData()
    }
    
    func setScreenLayout(){
        textView.scrollRangeToVisible((NSMakeRange(0,0)))
        viewBackground.layer.cornerRadius = 15
        viewBackground.clipsToBounds = false
        textView.layer.cornerRadius = 15
        textView.clipsToBounds = false
        btnClose.layer.cornerRadius = 5
        btnClose.backgroundColor = AppColor.PRIMARY_COLOR
        btnClose.setTitle("common_btn_close".localized(), for: .normal)
    }
    
    func setupErrorViewState() {
        let failureView = ErrorView(frame: view.frame)
        failureView.tapGestureRecognizer.addTarget(self, action: #selector(self.loadData))
        errorView = failureView
    }
    
    func controlGesture(){
        let tapGesture = UITapGestureRecognizer.init(target: self, action: #selector(CustomerBenefitTCsViewController.dismissPopup))
        self.view.addGestureRecognizer(tapGesture)
    }
    
    func loadData(){
        
        SVProgressHUD.show()
        
        guard let setting = CurrentCustomerInfo.shareInfo.loadSettingInfo() else {
            SVProgressHUD.dismiss()
            self.transitionViewStates(loading: false, error: NSError(domain: "error_server_fail".localized(), code: 1, userInfo: nil), animated: false, completion: nil)
            return
        }
        
        guard let i = setting.index(where: { $0.key == AppSetting.SETTING_BENEFIT_TC }) else {
            SVProgressHUD.dismiss()
            self.transitionViewStates(loading: false, error: NSError(domain: "error_server_fail".localized(), code: 1, userInfo: nil), animated: false, completion: nil)
            return
        }
        // End loading error page
        self.endLoading(error: nil)
        // Start get value from key
        let settingWithIndex : Setting = setting[i]
        // Set value to text view
        if Localize.currentLanguage() == "en" {
            textView.text = settingWithIndex.value
        } else {
            textView.text = settingWithIndex.value_cn
        }
        
        SVProgressHUD.dismiss()
    }
    
    func dismissPopup(){
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func closePopup(_ sender: UIButton) {
        dismissPopup()
    }

}
