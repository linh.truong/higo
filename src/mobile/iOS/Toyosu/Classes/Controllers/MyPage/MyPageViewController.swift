//
//  MyPageViewController.swift
//  Roann
//
//  Created by Linh Trương on 3/3/17.
//  Copyright © 2017 IBS. All rights reserved.
//

import UIKit
import Localize_Swift
import SVProgressHUD
import StatefulViewController
import PopupDialog

class MyPageViewController: UIViewController, StatefulViewController {
    
    let refreshControl = UIRefreshControl()
    let dateFormatter = DateFormatter()
    var isLoading : Bool = false
    var currentBr = UIScreen.main.brightness
    var benefitContent : String?
    var benefitContentCn : String?
    var cusMyPage : CustomerMyPage?
    
    @IBOutlet weak var lbStatusAndHoldingPoint: UILabel!
    @IBOutlet weak var pointView: MemberPointView!
    @IBOutlet weak var viewCustomerInfo: UIView!
    @IBOutlet weak var lbCustomerName: UILabel!
    @IBOutlet weak var lbCustomerNameValue: UILabel!
    @IBOutlet weak var lbMembershipNo: UILabel!
    @IBOutlet weak var lbMembershipNoValue: UILabel!
    @IBOutlet weak var lbTransaction: UILabel!
    @IBOutlet weak var lbTransactionValue: UILabel!
    @IBOutlet weak var viewCardInfo: UIView!
    @IBOutlet weak var blurView: UIVisualEffectView!
    @IBOutlet weak var lbStatusCardShow: UILabel!
    @IBOutlet weak var lbHoldingPointCardShow: UILabel!
    @IBOutlet weak var lbStatusCard: UILabel!
    @IBOutlet weak var lbPointCard: UILabel!
    @IBOutlet weak var lbCustomerNameCard: UILabel!
    @IBOutlet weak var lbCustomerNameValueCard: UILabel!
    @IBOutlet weak var lbMembershipNoCard: UILabel!
    @IBOutlet weak var lbMembershipNoValueCard: UILabel!
    @IBOutlet weak var imgRanking: UIImageView!
    @IBOutlet weak var lbCustomerPoint: UILabel!
    @IBOutlet weak var lbCustomerPointValue: UILabel!
    @IBOutlet weak var lbCustomerPointCard: UILabel!
    @IBOutlet weak var lbCustomerPointValueCard: UILabel!
    @IBOutlet weak var btnShowBenefits: UIButton!
    @IBOutlet weak var viewBg: UIView!
    @IBOutlet weak var viewBgOfCard: UIView!
    @IBOutlet weak var viewBenefitFirst: UIView!
    @IBOutlet weak var viewBenefitSecond: UIView!
    @IBOutlet weak var shortDescriptionFirst: UILabel!
    @IBOutlet weak var shortDescriptionSecond: UILabel!
    @IBOutlet weak var btnShowBenefitsTC: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setHomeInit()
        
        let tapGesture = UITapGestureRecognizer.init(target: self, action: #selector(MyPageViewController.dismissMembership))
        self.view.addGestureRecognizer(tapGesture)
        
        
        NotificationCenter.default.addObserver(self, selector: #selector(MyPageViewController.refreshLanguage), name: NSNotification.Name(LCLLanguageChangeNotification), object: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        getCustomerInfo()
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    func setHomeInit() {
        self.pointView.configPointView(customize: [.indicatorWidthRatio(0.15)])
        edgesForExtendedLayout = []
        setScreenLayout()
        setLangLocalized()
        setBarButtonItem()
        setupInitialViewState()
        setupErrorViewState()
        setBarButtonItem()
    }
    
    func setScreenLayout() {
        dateFormatter.dateStyle = .long
        dateFormatter.timeStyle = .short
        blurView.isHidden = true
        viewCardInfo.isHidden = true
        viewCardInfo.layer.cornerRadius = 15
        viewBgOfCard.clipsToBounds = false
        viewBgOfCard.layer.cornerRadius = 15
        viewCardInfo.clipsToBounds = false
        self.view.backgroundColor = AppColor.BACKGROUND_COLOR
        btnShowBenefits.layer.cornerRadius = 5
    }
    
    func setLangLocalized() {
        navigationItem.title = "home_lb_title".localized()
        lbStatusAndHoldingPoint.text = "customer_lb_status_and_holding_point".localized()
        lbCustomerName.text = "customer_lb_name_my_page".localized()
        lbMembershipNo.text = "customer_lb_membership_title".localized()
        lbTransaction.text = "customer_lb_last_transaction".localized()
        lbCustomerPoint.text = "customer_lb_the_current_point".localized()
        lbCustomerPointCard.text = "customer_lb_the_current_point".localized()
        btnShowBenefits.setTitle("customer_btn_show_the_benefits".localized(), for: .normal)
        btnShowBenefitsTC.setTitle("customer_btn_show_the_benefits_tc".localized(), for: .normal)
    }
    
    func setBarButtonItem() {
        // Card membership
        let btnCard : UIButton = UIButton.init(type: .custom)
        btnCard.setBackgroundImage(UIImage(named: "btn_card"), for: .normal)
        btnCard.layer.cornerRadius = 5
        btnCard.frame = CGRect(x: 0, y: 0, width: 35, height: 30)
        btnCard.addTarget(self, action: #selector(MyPageViewController.showMembershipScreen), for: .touchUpInside)
        let barBtnRight = UIBarButtonItem(customView: btnCard)
        self.navigationItem.rightBarButtonItem = barBtnRight
        
        // Cart shopping
        let btnCart : UIButton = UIButton.init(type: .custom)
        btnCart.setBackgroundImage(UIImage(named: "btn_cart"), for: .normal)
        btnCart.layer.cornerRadius = 5
        btnCart.frame = CGRect(x: 0, y: 0, width: 35, height: 30)
        btnCart.addTarget(self, action: #selector(MyPageViewController.loadWebURL), for: .touchUpInside)
        let barBtnLeft = UIBarButtonItem(customView: btnCart)
        self.navigationItem.leftBarButtonItem = barBtnLeft
    }
    
    func showBenefit(){
        dismissMembership()
        performSegue(withIdentifier: CustomerBenefitsViewController.segueID, sender: nil)
    }
    
    func showBenefitTC(){
        dismissMembership()
        performSegue(withIdentifier: CustomerBenefitTCsViewController.segueID, sender: nil)
    }
    
    func showMembershipScreen() {
        self.isLoading = true
        getCustomerInfo()
    }
    
    func showMembership() {
        UIView.animate(withDuration: 1.0) {
            self.view.backgroundColor = UIColor.black
            self.blurView.isHidden = false
            self.viewCardInfo.isHidden = false
        }
        UIScreen.main.brightness = Constant.SCREEN_MAX_BRIGHTNESS
        self.getCustomerInfoForCard()
    }
    
    
    func dismissMembership(){
        if !viewCardInfo.isHidden {
            UIView.animate(withDuration: 0.5) {
                self.view.backgroundColor = UIColor(hexString: "#EEEEEE")
                self.blurView.isHidden = true
                self.viewCardInfo.isHidden = true
                self.isLoading = false
            }
            UIScreen.main.brightness = self.currentBr
        }
    }
    
    func setupErrorViewState() {
        let failureView = ErrorView(frame: view.frame)
        failureView.tapGestureRecognizer.addTarget(self, action: #selector(MyPageViewController.setHomeInit))
        errorView = failureView
    }
    
    func setupEmptyViewState() {
        let emptyView = EmptyView(frame: view.frame)
        errorView = emptyView
    }
    
    func refreshLanguage () {
        // Reload data table view after changed localized
        setBarButtonItem()
        setLangLocalized()
    }
    
    func loadWebURL() {
        
        SVProgressHUD.show()
        
        guard let setting = CurrentCustomerInfo.shareInfo.loadSettingInfo() else {
            SVProgressHUD.dismiss()
            self.transitionViewStates(loading: false, error: NSError(domain: "error_server_fail".localized(), code: 1, userInfo: nil), animated: false, completion: nil)
            return
        }
        
        guard let i = setting.index(where: { $0.key == AppSetting.SETTING_WEBSITE_URL }) else {
            SVProgressHUD.dismiss()
            self.transitionViewStates(loading: false, error: NSError(domain: "error_server_fail".localized(), code: 1, userInfo: nil), animated: false, completion: nil)
            return
        }
        // End loading error page
        self.endLoading(error: nil)
        // Start get value from key
        let settingWithIndex : Setting = setting[i]
        
        SVProgressHUD.dismiss()
        
        var url: URL? = nil
        
        if let str = settingWithIndex.value {
            if let u = URL.init(string: str) {
                url = u
                UIApplication.shared.openURL(url!)
            }
        }
    }
    
    func getCustomerInfo() {
        
        SVProgressHUD.show()
        self.pointView.isHidden = true
        self.viewCustomerInfo.isHidden = true
        
        if let customer = CurrentCustomerInfo.shareInfo.loadCustomerInfo() {
            if let cus_id = customer.id {
                if let authToken = CurrentCustomerInfo.shareInfo.getToken() {
                    APIService.sharedService.getCustomerById(cus_id, authToken) { (result, error, rsCustomer) in
                        
                        guard result else {
                            SVProgressHUD.dismiss()
                            self.transitionViewStates(loading: false, error: NSError(domain: "error_no_data".localized(), code: 1, userInfo: nil), animated: false, completion: nil)
                            return
                        }
                        
                        guard let cusMyPage = rsCustomer else {
                            SVProgressHUD.dismiss()
                            self.transitionViewStates(loading: false, error: NSError(domain: "error_no_data".localized(), code: 1, userInfo: nil), animated: false, completion: nil)
                            return
                        }
                        
                        self.endLoading(error: nil)
                        self.cusMyPage = cusMyPage
                        // Check level
                        if let totalPoint = cusMyPage.total_point {
                            var url: URL? = nil
                            if let str = cusMyPage.mem_status_image_path {
                                if let u = URL.init(string: str) {
                                    url = u
                                }
                            }
                            if let fUrl = url {
                                self.imgRanking.setImageWith(fUrl)
                            } else {
                                self.imgRanking.image = UIImage.init(named: "no_img")
                            }
                            let levelName : String = cusMyPage.mem_status_name!
                            let levelNameCn : String = cusMyPage.mem_status_name_cn!
                            let remainPoint : Int = cusMyPage.mem_status_remain_point!
                            let hexColor : String = cusMyPage.mem_status_hex_color!
                            // Not top level
                            if let nextLevelName = cusMyPage.mem_status_next_name, let nextLevelNameCn = cusMyPage.mem_status_next_name_cn, let nextLevelPoint = cusMyPage.mem_status_next_level_point {
                                delay(1.5) { [unowned self] in
                                    if Localize.currentLanguage() == "en" {
                                        self.lbStatusCard.text = levelName
                                        self.pointView.configPointView(customize: [
                                            .levelSuffix("pt"),
                                            .currentPoint(totalPoint),
                                            .levelName(levelName),
                                            .levelIcon((self.imgRanking.image)!),
                                            .nextLevelPoint(nextLevelPoint),
                                            .nextLevelName(nextLevelName),
                                            .circleBackgroundColor(UIColor.white),
                                            .indicatorColor(UIColor(hexString: hexColor)),
                                            .firstDescription(String(format: "customer_lb_until_level".localized(), "\(remainPoint)")),
                                            .secondDesciption(String(format: "customer_lb_to_goal_level".localized(), nextLevelName)),
                                            ], animate: true)
                                    } else {
                                        self.lbStatusCard.text = levelNameCn
                                        self.pointView.configPointView(customize: [
                                            .levelSuffix("pt"),
                                            .currentPoint(totalPoint),
                                            .levelName(levelNameCn),
                                            .levelIcon((self.imgRanking.image)!),
                                            .nextLevelPoint(nextLevelPoint),
                                            .nextLevelName(nextLevelNameCn),
                                            .circleBackgroundColor(UIColor.white),
                                            .indicatorColor(UIColor(hexString: hexColor)),
                                            .firstDescription(String(format: "customer_lb_until_level".localized(), "\(remainPoint)")),
                                            .secondDesciption(String(format: "customer_lb_to_goal_level".localized(), nextLevelNameCn)),
                                            ], animate: true)
                                    }
                                }
                                // is top level
                            } else {
                                delay(1.5) { [unowned self] in
                                    if Localize.currentLanguage() == "en" {
                                        self.lbStatusCard.text = levelName
                                        self.pointView.configPointView(customize: [
                                            .levelSuffix("pt"),
                                            .currentPoint(totalPoint),
                                            .levelName(levelName),
                                            .levelIcon((self.imgRanking.image)!),
                                            .nextLevelPoint(totalPoint),
                                            .firstDescription("customer_lb_top_level_cong".localized()),
                                            .secondDesciption("customer_lb_got_top_level".localized()),
                                            .circleBackgroundColor(UIColor.white),
                                            .indicatorColor(UIColor(hexString: hexColor))
                                            ], animate: true)
                                    } else {
                                        self.lbStatusCard.text = levelNameCn
                                        self.pointView.configPointView(customize: [
                                            .levelSuffix("pt"),
                                            .currentPoint(totalPoint),
                                            .levelName(levelNameCn),
                                            .levelIcon((self.imgRanking.image)!),
                                            .nextLevelPoint(totalPoint),
                                            .firstDescription("customer_lb_top_level_cong".localized()),
                                            .secondDesciption("customer_lb_got_top_level".localized()),
                                            .circleBackgroundColor(UIColor.white),
                                            .indicatorColor(UIColor(hexString: hexColor))
                                            ], animate: true)
                                    }
                                }
                            }
                        }
                        
                        // Load customer info
                        self.lbCustomerNameValue.text = cusMyPage.name
                        self.lbMembershipNoValue.text = cusMyPage.number?.toString()
                        self.lbCustomerPointValue.text = String(format: "customer_lb_goal_points".localized(), "\((cusMyPage.current_point)!)")
                        if let ltdDate = cusMyPage.last_transaction_date {
                            self.lbTransactionValue.text = self.dateFormatter.string(from: ltdDate)
                        }
                        
                        self.benefitContent = cusMyPage.mem_status_content
                        self.benefitContentCn = cusMyPage.mem_status_content_cn
                        
                        self.pointView.isHidden = false
                        self.viewCustomerInfo.isHidden = false
                        
                        // Save new customer information
                        CurrentCustomerInfo.shareInfo.saveCustomerMyPage(rsCustomer)
                        
                        SVProgressHUD.dismiss()
                        
                        if self.isLoading {
                            self.showMembership()
                        }
                    }
                }
            }
        } else {
            SVProgressHUD.dismiss()
            self.transitionViewStates(loading: false, error: NSError(domain: "error_server_fail".localized(), code: 1, userInfo: nil), animated: false, completion: nil)
        }
    }
    
    func getCustomerInfoForCard(){
        if let cusCard = CurrentCustomerInfo.shareInfo.loadCustomerInfo() {
            
            lbStatusCardShow.text = "customer_lb_status_holding_point".localized()
            lbHoldingPointCardShow.text = "customer_lb_status_holding_point2".localized()
            lbCustomerNameCard.text = "customer_lb_name_my_page".localized()
            lbCustomerNameValueCard.text = cusCard.name
            lbMembershipNoCard.text = "customer_lb_membership_title".localized()
            lbMembershipNoValueCard.text = cusCard.number?.toString()
            lbCustomerPointCard.text = "customer_lb_the_current_point".localized()
            lbCustomerPointValueCard.text = cusCard.current_point?.toString()
            
            if let totalPoint = cusCard.total_point {
                lbPointCard.text = "\(totalPoint.toString())"
            }
            
            let hexColor1 = self.cusMyPage?.mem_status_hex_color_benefit_1
            let hexColor2 = self.cusMyPage?.mem_status_hex_color_benefit_2
            
            viewBenefitFirst.backgroundColor = UIColor(hexString: hexColor1!)
            viewBenefitSecond.backgroundColor = UIColor(hexString: hexColor2!)
            if Localize.currentLanguage() == "en" {
                self.shortDescriptionFirst.attributedText = self.cusMyPage?.mem_status_short_content?.html2AttributedString
                self.shortDescriptionSecond.attributedText = self.cusMyPage?.mem_status_short_content_2?.html2AttributedString
            } else {
                self.shortDescriptionFirst.attributedText = self.cusMyPage?.mem_status_short_content_cn?.html2AttributedString
                self.shortDescriptionSecond.attributedText = self.cusMyPage?.mem_status_short_content_2_cn?.html2AttributedString
            }
            
            btnShowBenefits.addTarget(self, action: #selector(self.showBenefit), for: .touchUpInside)
            btnShowBenefitsTC.addTarget(self, action: #selector(self.showBenefitTC), for: .touchUpInside)
            
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if (segue.identifier == CustomerBenefitsViewController.segueID) {
            let benefitVC = segue.destination as! CustomerBenefitsViewController
            benefitVC.content = self.benefitContent
            benefitVC.contentCn = self.benefitContentCn
        }
    }
    
}

func delay(_ delay: Double, closure: @escaping ()->()) {
    let when = DispatchTime.now() + delay
    DispatchQueue.main.asyncAfter(deadline: when, execute: closure)
}
