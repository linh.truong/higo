//
//  InstagramNewsViewController.swift
//  Roann
//
//  Created by Linh Trương on 4/18/17.
//  Copyright © 2017 IBS. All rights reserved.
//

import UIKit
import Localize_Swift
import SVProgressHUD
import StatefulViewController

class InstagramNewsViewController : RightBarViewController, StatefulViewController {
    
    var allData : [InstagramModel] = []
    var pagination : InstagramPaginationModel?
    
    let refreshControl = UIRefreshControl()
    var next_max_id : String? = nil
    
    @IBOutlet var tblInstagramList: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        screenInit()
        
        refreshControl.addTarget(self, action: #selector(InstagramNewsViewController.refreshWithRemoveData), for: UIControlEvents.valueChanged)
        tblInstagramList.addSubview(refreshControl)
        
        // Reload language localized after change
        NotificationCenter.default.addObserver(self, selector: #selector(InstagramNewsViewController.refreshLanguage), name: NSNotification.Name(LCLLanguageChangeNotification), object: nil)
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    func setupErrorViewState() {
        let failureView = ErrorView(frame: view.frame)
        failureView.tapGestureRecognizer.addTarget(self, action: #selector(InstagramNewsViewController.refreshWithRemoveData))
        errorView = failureView
    }
    
    func setupEmptyViewState() {
        let emptyView = EmptyView(frame: view.frame)
        errorView = emptyView
    }
    
    func screenInit(){
        tblInstagramList.delegate = self
        tblInstagramList.dataSource = self
        setupErrorViewState()
        setupEmptyViewState()
        setScreenLayout()
        refreshWithIndicator()
    }
    
    func refreshWithRemoveData(){
        self.allData.removeAll()
        self.next_max_id = nil
        getInstagramDataAPI()
    }
    
    func refreshLanguage() {
        DispatchQueue.main.async {
            self.tblInstagramList.reloadData()
        }
    }
    
    func setScreenLayout() {
        self.view.backgroundColor = AppColor.BACKGROUND_COLOR
        edgesForExtendedLayout = []
        tblInstagramList.contentInset = UIEdgeInsetsMake(4, 0, 4, 0)
    }
    
    func refreshWithIndicator() {
        SVProgressHUD.show()
        getInstagramDataAPI()
    }
    
    func finishRefreshing() {
        SVProgressHUD.dismiss()
        DispatchQueue.main.async {
            self.tblInstagramList.reloadData()
        }
        refreshControl.endRefreshing()
    }
    
    func getInstagramDataAPI(){
        
        APIService.sharedService.getInstagramData(next_max_id) { (result, code, mess, data, paging) in
            
            guard result else {
                SVProgressHUD.dismiss()
                self.transitionViewStates(loading: false, error: NSError(domain: "error_internet_unavailable_title".localized(), code: 1, userInfo: nil), animated: false, completion: nil)
                return
            }
            
            guard code != ErrorCode.ERROR_CODE_FAIL else {
                SVProgressHUD.dismiss()
                self.transitionViewStates(loading: false, error: NSError(domain: "error_server_fail".localized(), code: 1, userInfo: nil), animated: false, completion: nil)
                return
            }
            
            guard code != ErrorCode.ERROR_CODE_NOT_FOUND else {
                SVProgressHUD.dismiss()
                self.transitionViewStates(loading: false, error: NSError(domain: "error_unknown".localized(), code: 1, userInfo: nil), animated: false, completion: nil)
                return
            }
            
            self.endLoading(error: nil)
            self.finishCallAPI(data, paging)
        }
    }
    
    func finishCallAPI(_ dataList: [InstagramModel]?, _ paging: InstagramPaginationModel?){
        
        guard let data = dataList else {
            self.transitionViewStates(loading: false, error: NSError(domain: "error_server_fail".localized(), code: 1, userInfo: nil), animated: false, completion: nil)
            finishRefreshing()
            return
        }
        
        guard let pg = paging else {
            self.transitionViewStates(loading: false, error: NSError(domain: "error_server_fail".localized(), code: 1, userInfo: nil), animated: false, completion: nil)
            finishRefreshing()
            return
        }
        
        // Finish loading error state
        self.endLoading(error: nil)
        
        // Append data
        self.allData.append(contentsOf: data)
        // Pagination
        self.pagination = pg
        self.next_max_id = pagination?.next_max_id
        // Finish
        self.finishRefreshing()
    }
    
}

extension InstagramNewsViewController : UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return allData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let rowAtIndexPath = indexPath.row
        
        if rowAtIndexPath == allData.count - 1 {
            let loadMoreCell = tableView.dequeueReusableCell(withIdentifier: LoadMoreTableViewCell.cellID, for: indexPath) as! LoadMoreTableViewCell
            
            return loadMoreCell
        } else {
            let instaCell = tableView.dequeueReusableCell(withIdentifier: InstagramTableViewCell.cellID, for: indexPath) as! InstagramTableViewCell
            
            instaCell.configureCellWith(allData[rowAtIndexPath])
            
            return instaCell
        }
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if (indexPath.row == allData.count - 1) {
            if self.next_max_id != nil {
                getInstagramDataAPI()
            }
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let rowAtIndexPath = indexPath.row
        
        if rowAtIndexPath == allData.count {
            tableView.estimatedRowHeight = 44.0
            tableView.rowHeight = UITableViewAutomaticDimension
            return tableView.rowHeight
        } else {
            tableView.estimatedRowHeight = 500.0
            tableView.rowHeight = UITableViewAutomaticDimension
            return tableView.rowHeight
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let rowAtIndexPath = indexPath.row
        var url: URL? = nil
        
        let link = allData[rowAtIndexPath].link
        
        if let str = link {
            if let u = URL.init(string: str) {
                url = u
                UIApplication.shared.openURL(url!)
            }
        }
    }
}
