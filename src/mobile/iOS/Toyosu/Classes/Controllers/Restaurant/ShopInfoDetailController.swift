//
//  ShopInfoDetailController.swift
//  Toyosu
//
//  Created by Linh Trương on 1/20/17.
//  Copyright © 2017 IBS. All rights reserved.
//

import Foundation
import MapKit
import AFNetworking
import Localize_Swift
import StatefulViewController
import MessageUI
import PopupDialog

class ShopInfoDetailController : UIViewController, StatefulViewController, MFMailComposeViewControllerDelegate {
    
    @IBOutlet weak var tblInfo: UITableView!
    @IBOutlet weak var mapInfo: MKMapView!
    
    var restaurant: Restaurant? {
        didSet {
            if isViewLoaded {
                showRestaurantPin()
                if let long = restaurant?.longitude {
                    if let lat = restaurant?.latitude {
                        zoomToLocation(Double(long)!, Double(lat)!)
                    }
                }
            }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        commonInit()
        NotificationCenter.default.addObserver(self, selector: #selector(ShopInfoDetailController.refreshLanguage), name: NSNotification.Name(LCLLanguageChangeNotification), object: nil)
    }
    
    func commonInit () {
        edgesForExtendedLayout = []
        tblInfo.delegate = self
        tblInfo.dataSource = self
        tblInfo.layer.cornerRadius = 5.0
        tblInfo.backgroundColor = AppColor.BACKGROUND_COLOR
        mapInfo.delegate = self
        showRestaurantPin()
        backNavLocalized()
        if let long = restaurant?.longitude {
            if let lat = restaurant?.latitude {
                zoomToLocation(Double(long)!, Double(lat)!)
            }
        }
        setBarButtonItem()
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    func backNavLocalized() {
        let backItem = UIBarButtonItem()
        backItem.title = "common_back".localized()
        navigationItem.backBarButtonItem = backItem
    }
    
    func setBarButtonItem() {
        let button : UIButton = UIButton.init(type: .custom)
        button.setBackgroundImage(UIImage(named: "btn_book"), for: .normal)
        button.layer.cornerRadius = 5
        button.addTarget(self, action: #selector(ShopInfoDetailController.showBookingNowScreen), for: .touchUpInside)
        button.frame = CGRect(x: 0, y: 0, width: 35, height: 30)
        let barButton = UIBarButtonItem(customView: button)
        self.navigationItem.rightBarButtonItem = barButton
        
    }
    
    func showBookingNowScreen() {
        let storyboard = UIStoryboard(name: "Coupon", bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: "SimplyBookViewController") as! SimplyBookViewController
        self.show(controller, sender: nil)
    }
    
    func refreshLanguage() {
        tblInfo.reloadData()
        setBarButtonItem()
        backNavLocalized()
    }
    
    func showRestaurantPin () {
        if let lat = restaurant?.latitude {
            if let long = restaurant?.longitude {
                let centerCoordinate = CLLocationCoordinate2D(latitude: Double(lat)!, longitude: Double(long)!)
                let annotation = RestaurantAnnotation(coordinate: centerCoordinate)
                mapInfo.addAnnotation(annotation)
            }
        }
    }
    
    func zoomToLocation(_ long: Double, _ lat :Double) {
        let heightAdjust : Double = 0.004
        let centerCoordinate = CLLocationCoordinate2D(latitude: lat - heightAdjust, longitude: long)
        let coordinateRegion = MKCoordinateRegionMakeWithDistance(centerCoordinate, 500 * 2.0, 500 * 2.0)
        mapInfo.setRegion(coordinateRegion, animated: true)
    }
    
    //email config
    func configuredMailComposeViewController() -> MFMailComposeViewController {
        let mailComposerVC = MFMailComposeViewController()
        mailComposerVC.mailComposeDelegate = self //
        if let email = restaurant?.email{
            mailComposerVC.setToRecipients([email])
        }
        return mailComposerVC
    }
    // MARK: MFMailComposeViewControllerDelegate Method
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        controller.dismiss(animated: true, completion: nil)
    }
    
//    // open Google maps
//    func openGGMap(){
//        var url: URL? = nil
//        if Localize.currentLanguage() == "en" {
//            if let addr = restaurant?.address {
//                let str = "https://www.google.com.hk/maps/search/\(addr)"
//                if let u = URL.init(string: str) {
//                    url = u
//                    UIApplication.shared.openURL(url!)
//                }
//            }
//            
//        } else {
//            if let addrCn = restaurant?.address_cn {
//                let str = "https://www.google.com.hk/maps/search/\(addrCn)"
//                if let u = URL.init(string: str) {
//                    url = u
//                    UIApplication.shared.openURL(url!)
//                }
//            }
//            
//        }
//    }
    
    // send email event
    func sendMail() {
        let mailComposeViewController = configuredMailComposeViewController()
        if MFMailComposeViewController.canSendMail() {
            self.present(mailComposeViewController, animated: true, completion: nil)
        } else {
            // self.showSendMailErrorAlert()
        }
    }
    
    func phoneCall(phoneNumber: String) {
        let formatedNumber = phoneNumber.components(separatedBy: NSCharacterSet.decimalDigits.inverted).joined(separator: "")
        let phoneUrl = "tel://\(formatedNumber)"
        let url:URL = URL(string: phoneUrl)!
        UIApplication.shared.openURL(url)
    }
}

extension ShopInfoDetailController: UITableViewDataSource, UITableViewDelegate, UIGestureRecognizerDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 4
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "RestaurantInfoCell") as! RestaurantInfoCell
        
        switch indexPath.row {
        case 0:
            if Localize.currentLanguage() == "en" {
                if let addr = restaurant?.address {
                    cell.showData(UIImage(named: "res_address"), "restaurant_lb_address".localized(), addr)
                }
            } else {
                if let addr_cn = restaurant?.address_cn {
                    cell.showData(UIImage(named: "res_address"), "restaurant_lb_address".localized(), addr_cn)
                }
            }
            
            break
        case 1:
            if let email = restaurant?.email {
                cell.showData(UIImage(named: "res_email"), "restaurant_lb_email".localized(), email)
            }
            
            break
        case 2:
            if let phone = restaurant?.phone {
                cell.showData(UIImage(named: "res_phone"), "restaurant_lb_phone".localized(), phone)
            }
            
            break
        case 3:
            if Localize.currentLanguage() == "en" {
                if let desc = restaurant?.information {
                    cell.showData(UIImage(named: "res_about"), "other_lb_about_us".localized(), desc)
                }
            } else {
                if let desc_cn = restaurant?.information_cn {
                    cell.showData(UIImage(named: "res_about"), "other_lb_about_us".localized(), desc_cn)
                }
            }
            break
        default:
            break
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        tableView.rowHeight = UITableViewAutomaticDimension
        switch indexPath.row {
        case 0:
            tableView.estimatedRowHeight = 75.0
            return tableView.rowHeight
        case 1:
            tableView.estimatedRowHeight = 65.0
            return 60
        case 2:
            tableView.estimatedRowHeight = 65.0
            return 60
        case 3:
            tableView.estimatedRowHeight = 200.0
            return tableView.rowHeight
        default:
            return UITableViewAutomaticDimension
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch indexPath.row {
        case 0:
            
            break
        case 1:
            sendMail()
            break
        case 2:
            if let phone = restaurant?.phone {
                phoneCall(phoneNumber: phone)
            }
            break
        case 3:
            break
        default:
            break
        }
    }
}

extension ShopInfoDetailController : MKMapViewDelegate {
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        if let annotation = annotation as? RestaurantAnnotation {
            
            let identifier = "RestaurantAnnotation"
            var view: MKAnnotationView
            
            if let dequeuedView = mapView.dequeueReusableAnnotationView(withIdentifier: identifier) as? MKPinAnnotationView {
                dequeuedView.annotation = annotation
                view = dequeuedView
            } else {
                view = MKAnnotationView(annotation: annotation, reuseIdentifier: identifier)
                view.image = UIImage.init(named: "pin_icon")
                view.canShowCallout = false
            }
            
            return view
        }
        return nil
    }
}

class RestaurantAnnotation : NSObject, MKAnnotation {
    
    let coordinate: CLLocationCoordinate2D
    
    init(coordinate: CLLocationCoordinate2D) {
        
        self.coordinate = coordinate
        
        super.init()
    }
}
