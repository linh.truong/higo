//
//  ShopInfoListViewController.swift
//  Toyosu
//
//  Created by Linh Trương on 1/20/17.
//  Copyright © 2017 IBS. All rights reserved.
//

import UIKit
import Localize_Swift
import SVProgressHUD
import StatefulViewController

private let PAGINATION_LIMIT : Int = 6

class ShopInfoListViewController: UIViewController, StatefulViewController {
    
    // Define variable
    var allRestaurant : [Restaurant] = []
    var willViewDetailRestaurant: Restaurant?
    let refreshControl = UIRefreshControl()
    var totalRestaurants : Int = 0
    var currentPage : Int = 0
    var isLoadingMore = false
    
    // Connect outlet
    @IBOutlet var tblRestaurant: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setRestaurantInit()
        
        refreshControl.addTarget(self, action: #selector(ShopInfoListViewController.refresh(_:)), for: UIControlEvents.valueChanged)
        tblRestaurant.addSubview(refreshControl)
        
        // Reload language localized after change
        NotificationCenter.default.addObserver(self, selector: #selector(ShopInfoListViewController.refreshLanguage), name: NSNotification.Name(LCLLanguageChangeNotification), object: nil)
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    func setupErrorViewState() {
        let failureView = ErrorView(frame: view.frame)
        failureView.tapGestureRecognizer.addTarget(self, action: #selector(ShopInfoListViewController.refreshWithIndi))
        errorView = failureView
    }
    
    func setRestaurantInit() {
        edgesForExtendedLayout = []
        tblRestaurant.delegate = self
        tblRestaurant.dataSource = self
        tblRestaurant.contentInset = UIEdgeInsetsMake(4, 0, 4, 0)
        tblRestaurant.backgroundColor = AppColor.BACKGROUND_COLOR
        navigationItem.title = "shop_info_lb_title".localized()
        setupInitialViewState()
        setupErrorViewState()
        setBarButtonItem()
        refreshWithIndi()
    }
    
    func setBarButtonItem() {
        let button : UIButton = UIButton.init(type: .custom)
        button.setBackgroundImage(UIImage(named: "btn_book"), for: .normal)
        button.layer.cornerRadius = 5
        button.addTarget(self, action: #selector(ShopInfoListViewController.showBookingNowScreen), for: .touchUpInside)
        button.frame = CGRect(x: 0, y: 0, width: 35, height: 30)
        let barButton = UIBarButtonItem(customView: button)
        self.navigationItem.rightBarButtonItem = barButton
    }
    
    func showBookingNowScreen() {
        let storyboard = UIStoryboard(name: "Coupon", bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: "SimplyBookViewController") as! SimplyBookViewController
        self.show(controller, sender: nil)
    }
    
    func refreshLanguage () {
        tblRestaurant.reloadData()
        setBarButtonItem()
        navigationItem.title = "shop_info_lb_title".localized()
    }
    
    func refresh(_ sender: AnyObject) {
        loadRestaurantWithPage(0)
    }
    
    func refreshWithIndi() {
        SVProgressHUD.show()
        loadRestaurantWithPage(0)
    }
    
    func finishRefreshing() {
        SVProgressHUD.dismiss()
        tblRestaurant.reloadData()
        refreshControl.endRefreshing()
        isLoadingMore = false
    }
    
    func loadRestaurantWithPage(_ page : Int) {
        currentPage = page
        APIService.sharedService.getRestaurantList(PAGINATION_LIMIT, currentPage * PAGINATION_LIMIT, completed: { [weak self] (result, code, message, rsTotal, rsRestaurant) in
            // load error view
            guard result else {
                SVProgressHUD.dismiss()
                self?.transitionViewStates(loading: false, error: NSError(domain: "error_internet_unavailable_title".localized(), code: 1, userInfo: nil), animated: false, completion: nil)
                return
            }
            
            guard code != ErrorCode.ERROR_CODE_FAIL else {
                SVProgressHUD.dismiss()
                self?.transitionViewStates(loading: false, error: NSError(domain: "error_server_fail".localized(), code: 1, userInfo: nil), animated: false, completion: nil)
                return
            }
            
            guard code != ErrorCode.ERROR_CODE_NOT_FOUND else {
                SVProgressHUD.dismiss()
                self?.transitionViewStates(loading: false, error: NSError(domain: "error_unknown".localized(), code: 1, userInfo: nil), animated: false, completion: nil)
                return
            }
            // load home data
            self?.endLoading(error: nil)
            self?.finishCallApiWithRestaurants(rsRestaurant, rsTotal)
        })
    }
    
    func loadMoreRestaurant() {
        
        if totalRestaurants > allRestaurant.count {
            self.endLoading(error: nil)
            isLoadingMore = true
            loadRestaurantWithPage(currentPage + 1)
        } else {
            self.transitionViewStates(loading: false, error: NSError(domain: "error_internet_unavailable_title".localized(), code: 1, userInfo: nil), animated: false, completion: nil)
        }
        
    }
    
    func finishCallApiWithRestaurants(_ restaurantList: [Restaurant]?, _ total: Int?) {
        
        guard let restaurants = restaurantList else {
            self.transitionViewStates(loading: false, error: NSError(domain: "error_server_fail".localized(), code: 1, userInfo: nil), animated: false, completion: nil)
            finishRefreshing()
            return
        }
        
        guard let tt = total else {
            self.transitionViewStates(loading: false, error: NSError(domain: "error_server_fail".localized(), code: 1, userInfo: nil), animated: false, completion: nil)
            finishRefreshing()
            return
        }
        
        self.endLoading(error: nil)
        
        if (isLoadingMore) {
            allRestaurant.append(contentsOf: restaurants)
        } else {
            allRestaurant = restaurants
        }
        
        totalRestaurants = tt
        finishRefreshing()
    }
    
}

extension ShopInfoListViewController: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if allRestaurant.count < totalRestaurants {
            return allRestaurant.count + 1
        } else {
            return allRestaurant.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let rowAtIndexPath = indexPath.row
        
        if rowAtIndexPath == allRestaurant.count {
            let loadMoreCell = tableView.dequeueReusableCell(withIdentifier: LoadMoreTableViewCell.cellID, for: indexPath) as! LoadMoreTableViewCell
            
            return loadMoreCell
        } else {
            let restaurantCell = tableView.dequeueReusableCell(withIdentifier: RestaurantListTableViewCell.cellID, for: indexPath) as! RestaurantListTableViewCell
            
            restaurantCell.configureCellWith(allRestaurant[rowAtIndexPath])
            
            return restaurantCell
        }
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if (indexPath.row == allRestaurant.count) {
            loadMoreRestaurant()
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let rowAtIndexPath = indexPath.row
        
        if rowAtIndexPath == allRestaurant.count {
            return 44.0
        } else {
            tableView.estimatedRowHeight = 120.0
            tableView.rowHeight = UITableViewAutomaticDimension
            return tableView.rowHeight
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let rowAtIndexPath = indexPath.row
        
        if rowAtIndexPath < allRestaurant.count {
            willViewDetailRestaurant = self.allRestaurant[rowAtIndexPath]
            performSegue(withIdentifier: "ShopInfoDetailController", sender: nil)
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if (segue.identifier == "ShopInfoDetailController") {
            let restaurantDetailVC = segue.destination as! ShopInfoDetailController
            restaurantDetailVC.restaurant = willViewDetailRestaurant
        }
        
    }
}
