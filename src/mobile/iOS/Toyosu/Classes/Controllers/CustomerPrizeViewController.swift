//
//  CustomerPrizeViewController.swift
//  Roann
//
//  Created by Linh Trương on 4/5/17.
//  Copyright © 2017 IBS. All rights reserved.
//

import UIKit
import SVProgressHUD
import Localize_Swift
import PopupDialog
import StatefulViewController

class CustomerPrizeViewController: UIViewController, UITextFieldDelegate, StatefulViewController {
    
    // Id of cell
    static let segueID = "ShowDeliveryInformation"
    
    var customerPrize : CustomerPrizes?
    var prize: Prize?
    var dateFormatter = DateFormatter()
    
    // get outlet
    @IBOutlet weak var lbCusName: UILabel!
    @IBOutlet weak var txtCusName: UITextField!
    @IBOutlet weak var lbCusEmail: UILabel!
    @IBOutlet weak var txtCusEmail: UITextField!
    @IBOutlet weak var lbCusPhone: UILabel!
    @IBOutlet weak var txtCusPhone: UITextField!
    @IBOutlet weak var lbCusAddr: UILabel!
    @IBOutlet weak var txtCusAddr: UITextField!
    @IBOutlet weak var pvReceiveDate: UIPickerView!
    @IBOutlet weak var btnSubmit: UIButton!
    @IBOutlet weak var lbDeliHint: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        screenInit()
    }
    
    func screenInit(){
        setScreenLayout()
        setLangLocalized()
        setDelegate()
        setupInitialViewState()
        setupErrorViewState()
        loadCustomerData()
        addDoneButtonOnKeyboard()
    }
    
    func setScreenLayout() {
        btnSubmit.backgroundColor = AppColor.PRIMARY_COLOR
        btnSubmit.layer.cornerRadius = 5.0
        dateFormatter.dateFormat = "EEEE, MMMM dd, yyyy"
        lbDeliHint.textColor = AppColor.PRIMARY_COLOR
    }
    
    func setDelegate(){
        txtCusName.delegate = self
        txtCusEmail.delegate = self
        txtCusPhone.delegate = self
        txtCusAddr.delegate = self
    }
    
    func setLangLocalized() {
        self.title = "reservation_lb_request".localized()
        lbCusName.setRequiredAttribute("reservation_lb_name".localized())
        lbCusEmail.setRequiredAttribute("reservation_lb_email".localized())
        lbCusPhone.setRequiredAttribute("reservation_lb_phone".localized())
        lbCusAddr.setRequiredAttribute("reservation_lb_address".localized())
        btnSubmit.setTitle("other_contact_us_btn_submit".localized(), for: .normal)
        lbDeliHint.text = "other_prize_delivery_hint".localized()
    }
    
    func setupErrorViewState() {
        let failureView = ErrorView(frame: view.frame)
        failureView.tapGestureRecognizer.addTarget(self, action: #selector(CustomerPrizeViewController.loadCustomerData))
        errorView = failureView
    }
    
    func refreshLanguage() {
        setLangLocalized()
        setupInitialViewState()
        setupErrorViewState()
    }
    
    func changeKeyboardLayout(){
        txtCusName.returnKeyType = UIReturnKeyType.next
        txtCusEmail.returnKeyType = UIReturnKeyType.next
        txtCusPhone.returnKeyType = UIReturnKeyType.next
        txtCusAddr.returnKeyType = UIReturnKeyType.go
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        
        if textField == txtCusName {
            lbCusName.textColor = AppColor.DARK_GRAY_COLOR
            lbCusName.setRequiredAttribute("reservation_lb_name".localized())
        }
        
        if textField == txtCusEmail {
            lbCusEmail.textColor = AppColor.DARK_GRAY_COLOR
            lbCusEmail.setRequiredAttribute("reservation_lb_email".localized())
        }
        
        if textField == txtCusPhone {
            lbCusPhone.textColor = AppColor.DARK_GRAY_COLOR
            lbCusPhone.setRequiredAttribute("reservation_lb_phone".localized())
        }
        
        if textField == txtCusAddr {
            lbCusAddr.textColor = AppColor.DARK_GRAY_COLOR
            lbCusAddr.setRequiredAttribute("reservation_lb_address".localized())
        }
        
    }
    
    func addDoneButtonOnKeyboard() {
        let doneToolbar: UIToolbar = UIToolbar(frame: CGRect(x: 0, y: 0, width: 320, height: 50))
        doneToolbar.barStyle       = UIBarStyle.default
        let flexSpace              = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        let done: UIBarButtonItem  = UIBarButtonItem(title: "common_done".localized(), style: UIBarButtonItemStyle.done, target: self, action: #selector(CustomerPrizeViewController.doneButtonAction))
        
        var items = [UIBarButtonItem]()
        items.append(flexSpace)
        items.append(done)
        
        doneToolbar.items = items
        doneToolbar.sizeToFit()
        
        txtCusPhone.inputAccessoryView = doneToolbar
    }
    
    func doneButtonAction() {
        txtCusPhone.resignFirstResponder()
    }
    
    func loadCustomerData(){
        if let customer = CurrentCustomerInfo.shareInfo.loadCustomerInfo() {
            txtCusName.text = customer.name
            txtCusEmail.text = customer.email
            txtCusPhone.text = customer.phone
            txtCusAddr.text = customer.address
        } else {
            //            self.showRequiredLoginPopup()
        }
    }
    
    // load list of date filter by limit & week_day
//    func loadReceiveDate() {
//        
//        let currentLocale = Locale.current
//        var currentCalendar = Calendar.current
//        let currentDate = Date()
//        var components = DateComponents()
//        
//        SVProgressHUD.show()
//        
//        guard let setting = CurrentCustomerInfo.shareInfo.loadSettingInfo() else {
//            SVProgressHUD.dismiss()
//            self.transitionViewStates(loading: false, error: NSError(domain: "error_server_fail".localized(), code: 1, userInfo: nil), animated: false, completion: nil)
//            return
//        }
//        
//        guard let iStart = setting.index(where: { $0.key == AppSetting.SETTING_PRIZE_START_DATE }) else {
//            SVProgressHUD.dismiss()
//            self.transitionViewStates(loading: false, error: NSError(domain: "error_server_fail".localized(), code: 1, userInfo: nil), animated: false, completion: nil)
//            return
//        }
//        
//        guard let iEnd = setting.index(where: { $0.key == AppSetting.SETTING_PRIZE_END_DATE }) else {
//            SVProgressHUD.dismiss()
//            self.transitionViewStates(loading: false, error: NSError(domain: "error_server_fail".localized(), code: 1, userInfo: nil), animated: false, completion: nil)
//            return
//        }
//        
//        self.endLoading(error: nil)
//        let settingWithStartIndex : Setting = setting[iStart]
//        let settingWithEndIndex : Setting = setting[iEnd]
//        currentCalendar.locale = currentLocale
//        components.calendar = currentCalendar
//        
//        if let prizeStart = settingWithStartIndex.value {
//            if let api_limit_date = self.prize?.delivery_duration_days {
//                if let prizeEnd = settingWithEndIndex.value {
//                    for i in 0...prizeEnd.integer
//                    {
//                        components.day = i + prizeStart.integer + api_limit_date
//                        
//                        let addDate : Date = currentCalendar.date(byAdding: components, to: currentDate)!
//                        let addDateStr = self.dateFormatter.string(from: addDate)
//                        
//                        self.receiveStrArr.append(addDateStr)
//                        self.receiveDateArr.append(addDate)
//                    }
//                    
//                    self.pvReceiveDate.reloadAllComponents()
//                }
//            }
//            SVProgressHUD.dismiss()
//        } else {
//            self.showErrorPopup()
//        }
//    }
    
    func validData(){
        if let name = txtCusName.text {
            // Returns a new string made by removing from both ends of the String characters contained in a given character set.
            let cs = CharacterSet.init(charactersIn: " ")
            let trim = name.trimmingCharacters(in: cs)
            
            // If trimmed name have contains space characters
            guard trim.characters.contains(" ") else {
                txtCusName.becomeFirstResponder()
                lbCusName.text = "customer_lb_name_required_full_name".localized()
                lbCusName.textColor = AppColor.PRIMARY_COLOR
                return
            }
        }
        
        // validate for email
        if let email = txtCusEmail.text {
            
            guard email.characters.count > 0 else {
                txtCusEmail.becomeFirstResponder()
                lbCusEmail.text = "customer_lb_email_required".localized()
                lbCusEmail.textColor = AppColor.PRIMARY_COLOR
                return
            }
            
            guard email.isEmail(email: email) else {
                txtCusEmail.becomeFirstResponder()
                lbCusEmail.text = "customer_lb_email_invalid".localized()
                lbCusEmail.textColor = AppColor.PRIMARY_COLOR
                return
            }
        }
        
        // validate for phone number: only digits, from 8 to 12
        if let phone = txtCusPhone.text {
            
            guard phone.isPhoneNumber(phoneNumber: phone) else {
                txtCusPhone.becomeFirstResponder()
                lbCusPhone.text = "customer_lb_phone_invalid".localized()
                lbCusPhone.textColor = AppColor.PRIMARY_COLOR
                return
            }
        }
        
        // validate for phone number: only digits, from 8 to 12
        if let addr = txtCusAddr.text {
            
            guard addr.characters.count > 0 else {
                txtCusAddr.becomeFirstResponder()
                lbCusAddr.text = "customer_lb_address_required".localized()
                lbCusAddr.textColor = AppColor.PRIMARY_COLOR
                return
            }
        }
        
        insertData()
    }
    
    @IBAction func submitDelivery(_ sender: UIButton) {
        validData()
    }
    
    func insertData(){
        
        SVProgressHUD.show()
        
        // Load customer information
        if let cus = CurrentCustomerInfo.shareInfo.loadCustomerInfo() {
            // Load authenticate token
            if let authToken = CurrentCustomerInfo.shareInfo.getToken() {
                // Get customer data
                let language = Localize.currentLanguage()
                let cusPrizeData = CustomerPrizes(
                    id: nil,
                    customer: cus.id,
                    prize: prize?.id,
                    delivery_type: prize?.delivery_type,
                    name: txtCusName.text,
                    phone: txtCusPhone.text,
                    email: txtCusEmail.text,
                    address: txtCusAddr.text,
                    use_date: Date(),
                    delivery_date: nil,
                    receive_date: nil,
                    status: Constant.DEFAULT_PRIZE_STATUS_NOT_DELIVERY)
                // Start call API
                APIService.sharedService.usePrize(cus.id!, authToken, (prize?.id)!, cusPrizeData, language) { (result, code, message, data) in
                    
                    guard result else {
                        SVProgressHUD.dismiss()
                        self.transitionViewStates(loading: false, error: NSError(domain: "error_internet_unavailable_title".localized(), code: 1, userInfo: nil), animated: false, completion: nil)
                        return
                    }
                    
                    guard code != ErrorCode.ERROR_CODE_FAIL else {
                        SVProgressHUD.dismiss()
                        self.transitionViewStates(loading: false, error: NSError(domain: "error_internet_unavailable_title".localized(), code: 1, userInfo: nil), animated: false, completion: nil)
                        return
                    }
                    
                    guard code != ErrorCode.ERROR_CODE_NOT_FOUND else {
                        SVProgressHUD.dismiss()
                        self.transitionViewStates(loading: false, error: NSError(domain: "error_unknown".localized(), code: 1, userInfo: nil), animated: false, completion: nil)
                        return
                    }
                    
                    guard code != ErrorCode.ERROR_CODE_USER_LACK_POINT else {
                        SVProgressHUD.dismiss()
                        let popup = CommonFunctions.initPopup(title: "common_info".localized(), message: "customer_current_point_not_enough".localized())
                        popup.addButtons([DefaultButton(title: "common_ok".localized()) {
                            _ = self.navigationController?.popViewController(animated: true)
                            }])
                        self.present(popup, animated: true, completion: nil)
                        return
                    }
                    
                    guard code != ErrorCode.ERROR_CODE_EMPTY_STOCK else {
                        SVProgressHUD.dismiss()
                        let popup = CommonFunctions.initPopup(title: "common_info".localized(), message: "other_prize_msg_not_found".localized())
                        popup.addButtons([DefaultButton(title: "common_ok".localized()) {
                            _ = self.navigationController?.popViewController(animated: true)
                            }])
                        self.present(popup, animated: true, completion: nil)
                        return
                    }
                    
                    self.endLoading()
                    SVProgressHUD.dismiss()
                    self.showConfirmPopup()
                }
            }
        } else {
            self.showRequiredLoginPopup()
        }
    }
    
    func showConfirmPopup() {
        let popup = CommonFunctions.initPopup(title: "common_info".localized(), message: "other_prize_lb_used".localized())
        // Add buttons to dialog
        popup.addButtons([DefaultButton(title: "common_ok".localized()) {
            _ = self.navigationController?.popViewController(animated: true)
            }])
        
        // Present dialog
        self.present(popup, animated: true, completion: nil)
    }
    
    func showRequiredLoginPopup() {
        let popup = CommonFunctions.initPopup(title: "common_info".localized(), message: "other_prize_msg_login_required".localized())
        
        // Add buttons to dialog
        popup.addButtons([DefaultButton(title: "common_ok".localized()) {
            CurrentCustomerInfo.shareInfo.customer = nil
            let homeStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let vc = homeStoryboard.instantiateViewController(withIdentifier: "Login")
            self.navigationController?.pushViewController(vc, animated: true)
            }])
        
        // Add buttons to dialog
        popup.addButtons([DefaultButton(title: "common_cancel".localized()) {
            }])
        
        // Present dialog
        self.present(popup, animated: true, completion: nil)
        
    }
    
    func showErrorPopup() {
        // Prepare data for popup
        let title = "common_info".localized()
        let message = "error_internet_unavailable_title".localized()
        
        // Create the dialog
        let popup = PopupDialog(
            title: title,
            message: message,
            buttonAlignment: .horizontal,
            transitionStyle: .zoomIn,
            gestureDismissal: true
        ) {}
        
        // Add buttons to dialog
        popup.addButtons([DefaultButton(title: "common_ok".localized()) {
            }])
        
        // Present dialog
        self.present(popup, animated: true, completion: nil)
        
    }
    
}
