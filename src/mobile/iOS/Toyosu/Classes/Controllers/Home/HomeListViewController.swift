//
//  HomeListViewController.swift
//  Toyosu
//
//  Created by Linh Trương on 12/1/16.
//  Copyright © 2016 IBS. All rights reserved.
//

import UIKit
import Localize_Swift
import SVProgressHUD
import StatefulViewController

private let PAGINATION_LIMIT : Int = 5

class HomeListViewController: UIViewController, HomeTableViewCellDelegate, StatefulViewController {
    
    // Define variable
    var allTopic : [Topic] = []
    var willViewDetailTopic: Topic?
    let refreshControl = UIRefreshControl()
    let imageView = UIImageView()
    var totalTopics : Int = 0
    var currentPage : Int = 0
    var isLoadingMore = false
    var isBackMenu = true
    var isLiked = false
    var isShared = false
    
    // Get outlet
    @IBOutlet weak var tblHome: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // set navigation title image
        loadHomeTitleImage()
        
        // init home
        setHomeInit()
        
        // Refresh news list
        refreshControl.addTarget(self, action: #selector(HomeListViewController.refresh(_:)), for: UIControlEvents.valueChanged)
        tblHome.addSubview(refreshControl)
        
        // Reload language localized after change
        NotificationCenter.default.addObserver(self, selector: #selector(HomeListViewController.refreshLanguage), name: NSNotification.Name(LCLLanguageChangeNotification), object: nil)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        if (!isBackMenu) {
            refreshWithIndi()
        }
        
        imageView.isHidden = false
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    func loadHomeTitleImage() {
        let image = UIImage(named: "logo_home_40.png")
        imageView.contentMode = .scaleAspectFit
        imageView.frame = CGRect(x: 0, y: 0, width: 40, height: 40)
        imageView.image = image
        navigationItem.titleView = imageView
    }
    
    //    func setBarButtonItem() {
    //        // Card membership
    //        let btnCard : UIButton = UIButton.init(type: .custom)
    //        btnCard.setBackgroundImage(UIImage(named: "btn_card"), for: .normal)
    //        btnCard.layer.cornerRadius = 5
    //        btnCard.frame = CGRect(x: 0, y: 0, width: 35, height: 30)
    //        btnCard.addTarget(self, action: #selector(HomeListViewController.showMembershipScreen), for: .touchUpInside)
    //        let barBtnRight = UIBarButtonItem(customView: btnCard)
    //        self.navigationItem.rightBarButtonItem = barBtnRight
    //
    //        // Cart shopping
    //        let btnCart : UIButton = UIButton.init(type: .custom)
    //        btnCart.setBackgroundImage(UIImage(named: "btn_cart"), for: .normal)
    //        btnCart.layer.cornerRadius = 5
    //        btnCart.frame = CGRect(x: 0, y: 0, width: 35, height: 30)
    //        btnCart.addTarget(self, action: #selector(HomeListViewController.loadWebURL), for: .touchUpInside)
    //        let barBtnLeft = UIBarButtonItem(customView: btnCart)
    //        self.navigationItem.leftBarButtonItem = barBtnLeft
    //    }
    //
    //    func showMembershipScreen() {
    //        let storyboard = UIStoryboard(name: "Home", bundle: nil)
    //        let controller = storyboard.instantiateViewController(withIdentifier: "MembershipViewController") as! MembershipViewController
    //        self.show(controller, sender: nil)
    //    }
    
    func loadWebURL() {
        
        SVProgressHUD.show()
        
        guard let setting = CurrentCustomerInfo.shareInfo.loadSettingInfo() else {
            SVProgressHUD.dismiss()
            self.transitionViewStates(loading: false, error: NSError(domain: "error_server_fail".localized(), code: 1, userInfo: nil), animated: false, completion: nil)
            return
        }
        
        guard let i = setting.index(where: { $0.key == AppSetting.SETTING_WEBSITE_URL }) else {
            SVProgressHUD.dismiss()
            self.transitionViewStates(loading: false, error: NSError(domain: "error_server_fail".localized(), code: 1, userInfo: nil), animated: false, completion: nil)
            return
        }
        // End loading error page
        self.endLoading(error: nil)
        // Start get value from key
        let settingWithIndex : Setting = setting[i]
        
        SVProgressHUD.dismiss()
        
        var url: URL? = nil
        
        if let str = settingWithIndex.value {
            if let u = URL.init(string: str) {
                url = u
                UIApplication.shared.openURL(url!)
            }
        }
    }
    
    func setupErrorViewState() {
        let failureView = ErrorView(frame: view.frame)
        failureView.tapGestureRecognizer.addTarget(self, action: #selector(HomeListViewController.refreshWithIndi))
        errorView = failureView
    }
    
    func setupEmptyViewState() {
        let emptyView = EmptyView(frame: view.frame)
        errorView = emptyView
    }
    
    func setHomeInit() {
        edgesForExtendedLayout = []
        tblHome.delegate = self
        tblHome.dataSource = self
        tblHome.contentInset = UIEdgeInsetsMake(4, 0, 4, 0)
        self.navigationItem.title = "common_back".localized()
        isBackMenu = false
        isLiked = false
        isShared = false
        imageView.isHidden = false
        //        setBarButtonItem()
        setupInitialViewState()
        setupErrorViewState()
    }
    
    func refreshLanguage () {
        // Reload data table view after changed localized
        tblHome.reloadData()
        self.navigationItem.title = "common_back".localized()
        //        setBarButtonItem()
    }
    
    func refresh(_ sender: AnyObject) {
        loadTopicWithPage(0)
    }
    
    func refreshWithIndi() {
        SVProgressHUD.show()
        loadTopicWithPage(0)
    }
    
    func finishRefreshing() {
        SVProgressHUD.dismiss()
        tblHome.reloadData()
        refreshControl.endRefreshing()
        isLoadingMore = false
    }
    
    func loadTopicWithPage(_ page : Int) {
        currentPage = page
        APIService.sharedService.getTopicList(PAGINATION_LIMIT, currentPage * PAGINATION_LIMIT, completed: { [weak self] (result, code, message, rsTotal, rsTopics) in
            
            guard rsTotal > 0 else {
                SVProgressHUD.dismiss()
                self?.transitionViewStates(loading: false, error: NSError(domain: "error_no_data".localized(), code: 1, userInfo: nil), animated: false, completion: nil)
                return
            }
            
            guard result else {
                SVProgressHUD.dismiss()
                self?.transitionViewStates(loading: false, error: NSError(domain: "error_internet_unavailable_title".localized(), code: 1, userInfo: nil), animated: false, completion: nil)
                return
            }
            
            guard code != ErrorCode.ERROR_CODE_FAIL else {
                SVProgressHUD.dismiss()
                self?.transitionViewStates(loading: false, error: NSError(domain: "error_server_fail".localized(), code: 1, userInfo: nil), animated: false, completion: nil)
                return
            }
            
            guard code != ErrorCode.ERROR_CODE_NOT_FOUND else {
                SVProgressHUD.dismiss()
                self?.transitionViewStates(loading: false, error: NSError(domain: "error_unknown".localized(), code: 1, userInfo: nil), animated: false, completion: nil)
                return
            }
            
            // load home view
            self?.endLoading(error: nil)
            self?.finishCallApiWithTopics(rsTopics, rsTotal)
        })
    }
    
    func loadMoreTopic() {
        
        if totalTopics > allTopic.count {
            self.endLoading(error: nil)
            isLoadingMore = true
            loadTopicWithPage(currentPage + 1)
        } else {
            self.transitionViewStates(loading: false, error: NSError(domain: "error_internet_unavailable_title".localized(), code: 1, userInfo: nil), animated: false, completion: nil)
        }
        
    }
    
    func finishCallApiWithTopics(_ topicList: [Topic]?, _ total: Int) {
        
        guard let topics = topicList else {
            self.transitionViewStates(loading: false, error: NSError(domain: "error_server_fail".localized(), code: 1, userInfo: nil), animated: false, completion: nil)
            finishRefreshing()
            return
        }
        
        self.endLoading(error: nil)
        
        if (isLoadingMore) {
            allTopic.append(contentsOf: topics)
        } else {
            allTopic = topics
        }
        
        totalTopics = total
        finishRefreshing()
    }
    
    func like(_ index: Int) {
        // Like
        if(!isLiked) {
            isLiked = true
            print("Liked")
            // Unlike
        } else {
            isLiked = false
            print("Unlike")
        }
    }
    
    func share(_ index: Int) {
        if let shareURL = willViewDetailTopic?.link_address {
            let vc = UIActivityViewController(activityItems: [shareURL], applicationActivities: nil)
            self.present(vc, animated: true, completion: nil)
        }
    }
    
}

extension HomeListViewController : UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if allTopic.count < totalTopics {
            return allTopic.count + 1
        } else {
            return allTopic.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let rowAtIndexPath = indexPath.row
        
        if rowAtIndexPath == allTopic.count {
            let loadMoreCell = tableView.dequeueReusableCell(withIdentifier: LoadMoreTableViewCell.cellID, for: indexPath) as! LoadMoreTableViewCell
            
            return loadMoreCell
        } else {
            let homeCell = tableView.dequeueReusableCell(withIdentifier: HomeTableViewCell.cellID, for: indexPath) as! HomeTableViewCell
            
            homeCell.delegate = self
            homeCell.index = rowAtIndexPath
            
            homeCell.calculateImageSize(self.allTopic[rowAtIndexPath])
            homeCell.configureCellWith(self.allTopic[rowAtIndexPath])
            
            return homeCell
        }
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if (indexPath.row == allTopic.count) {
            loadMoreTopic()
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let rowAtIndexPath = indexPath.row
        
        if rowAtIndexPath == allTopic.count {
            tableView.estimatedRowHeight = 44.0
            tableView.rowHeight = UITableViewAutomaticDimension
            return tableView.rowHeight
        } else {
            tableView.estimatedRowHeight = 200.0
            tableView.rowHeight = UITableViewAutomaticDimension
            return tableView.rowHeight
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let rowAtIndexPath = indexPath.row
        
        if rowAtIndexPath < allTopic.count {
            willViewDetailTopic = self.allTopic[rowAtIndexPath]
            performSegue(withIdentifier: HomeDetailViewController.segueID, sender: nil)
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if (segue.identifier == HomeDetailViewController.segueID) {
            let homeDetailVC = segue.destination as! HomeDetailViewController
            homeDetailVC.topic = willViewDetailTopic
            isBackMenu = true
        }
        
    }
}
