//
//  ReservationViewController.swift
//  Toyosu
//
//  Created by Linh Trương on 11/2/16.
//  Copyright © 2016 IBS. All rights reserved.
//

import UIKit
import SVProgressHUD
import Localize_Swift
import PopupDialog
import StatefulViewController

class ReservationViewController: UIViewController, UITextFieldDelegate, UITextViewDelegate, StatefulViewController {
    
    var dateFormatter = DateFormatter()
    var resInfo: Restaurant?
    var reserveDateArr: [String] = []
    var shopInfoArr : [String] = []
    var availableDays: [Date] = []
//    var shopInfoId: [Int] = []
    var reserveTimeNormalArr: [String] = []
    var reserveTimeVIPArr: [String] = []
    var selectedReserveTimeNormal : String? = nil
    var selectedReserveTimeVIP : String? = nil
    var numberOfPaxInt : Int?
    
    @IBOutlet weak var lbNameStatus: UILabel!
    @IBOutlet weak var txtName: UITextField!
    @IBOutlet weak var lbEmailStatus: UILabel!
    @IBOutlet weak var txtEmail: UITextField!
    @IBOutlet weak var lbPhoneStatus: UILabel!
    @IBOutlet weak var txtPhone: UITextField!
    @IBOutlet weak var segmentReserveType: UISegmentedControl!
    @IBOutlet weak var lbReserveDate: UILabel!
    @IBOutlet weak var pvReserveDate: UIPickerView!
    @IBOutlet weak var lbReserveTime: UILabel!
    @IBOutlet weak var pvReserveTime: UIPickerView!
    @IBOutlet weak var lbMinAndMax: UILabel!
    @IBOutlet weak var lbNumberOfAdult: UILabel!
    @IBOutlet weak var txtNumberOfAdult: UITextField!
    @IBOutlet weak var lbNumberOfChild: UILabel!
    @IBOutlet weak var txtNumberOfChild: UITextField!
    @IBOutlet weak var lbCommentStatus: UILabel!
    @IBOutlet weak var tvComment: UITextView!
    @IBOutlet weak var btnRequest: UIButton!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var lbShopInfo: UILabel!
    @IBOutlet weak var pvShopInfo: UIPickerView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // format date
        dateFormatter.dateFormat = "EEEE, MMM dd, yyyy"
        
        screenInit()
        
        // refresh language ( localized )
        NotificationCenter.default.addObserver(self, selector: #selector(ReservationViewController.refreshLanguage), name: NSNotification.Name(LCLLanguageChangeNotification), object: nil)
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    func screenInit() {
        // format date
        dateFormatter.dateFormat = "EEEE, MMM dd, yyyy"
        // function init
        setScreenLayout()
        setLangLocalized()
        setDelegate()
        loadRestaurantInfo()
        changeKeyboardLayout()
        dismissKeyboardWhenTapped()
        addDoneButtonOnKeyboard()
        setupInitialViewState()
        setupErrorViewState()
        loadShopInfoList()
        loadCustomerData()
    }
    
    func setScreenLayout() {
        btnRequest.layer.cornerRadius = 5
        pvShopInfo.isOpaque = true
        pvReserveDate.isOpaque = true
        pvReserveTime.isOpaque = true
    }
    
    func setLangLocalized() {
        self.title = "reservation_lb_request".localized()
        lbNameStatus.text = "customer_lb_name".localized()
        lbPhoneStatus.text = "customer_lb_phone".localized()
        lbEmailStatus.text = "customer_lb_email".localized()
        lbShopInfo.text = "shop_info_lb_title".localized()
        lbReserveDate.text = "reservation_lb_reserve_date".localized()
        lbReserveTime.text = "reservation_lb_reserve_time".localized()
        lbNumberOfAdult.text = "reservation_lb_number_of_adult".localized()
        lbNumberOfChild.text = "reservation_lb_number_of_child".localized()
        segmentReserveType.setTitle("reservation_lb_normal_table".localized(), forSegmentAt: 0)
        segmentReserveType.setTitle("reservation_lb_vip_room".localized(), forSegmentAt: 1)
        lbCommentStatus.text = "customer_lb_comment".localized()
        btnRequest.setTitle("reservation_btn_send_request".localized(), for: .normal)
    }
    
    func setDelegate() {
        self.txtName.delegate = self
        self.txtEmail.delegate = self
        self.txtPhone.delegate = self
        self.txtNumberOfAdult.delegate = self
        self.txtNumberOfChild.delegate = self
        self.tvComment.delegate = self
        self.pvShopInfo.delegate = self
        self.pvReserveDate.delegate = self
        self.pvReserveTime.delegate = self
        self.pvShopInfo.dataSource = self
        self.pvReserveDate.dataSource = self
        self.pvReserveTime.dataSource = self
    }
    
    func setupErrorViewState() {
        let failureView = ErrorView(frame: view.frame)
        failureView.tapGestureRecognizer.addTarget(self, action: #selector(ReservationViewController.refreshLanguage))
        errorView = failureView
    }
    
    func refreshLanguage() {
        setLangLocalized()
        loadRestaurantInfo()
        setupInitialViewState()
        setupErrorViewState()
    }
    
    func changeKeyboardLayout(){
        txtName.returnKeyType = UIReturnKeyType.next
        txtEmail.returnKeyType = UIReturnKeyType.next
        txtPhone.returnKeyType = UIReturnKeyType.next
        txtNumberOfAdult.returnKeyType = UIReturnKeyType.next
        txtNumberOfChild.returnKeyType = UIReturnKeyType.next
        tvComment.returnKeyType = UIReturnKeyType.next
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        if textField == txtName {
            txtEmail.becomeFirstResponder()
        }
        
        if textField == txtEmail {
            txtPhone.becomeFirstResponder()
        }
        
        if textField == txtPhone {
            txtNumberOfAdult.becomeFirstResponder()
        }
        
        if textField == txtNumberOfAdult {
            txtNumberOfChild.becomeFirstResponder()
        }
        
        if textField == txtNumberOfChild {
            tvComment.becomeFirstResponder()
        }
        
        return false
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        
        if textField == txtName {
            lbNameStatus.text = "customer_lb_name".localized()
            lbNameStatus.textColor = UIColor.darkGray
        }
        
        if textField == txtEmail {
            lbEmailStatus.text = "customer_lb_email".localized()
            lbEmailStatus.textColor = UIColor.darkGray
        }
        
        if textField == txtPhone {
            lbPhoneStatus.text = "customer_lb_phone".localized()
            lbPhoneStatus.textColor = UIColor.darkGray
        }
        
        if textField == txtNumberOfAdult {
            lbNumberOfAdult.text = "reservation_lb_number_of_adult".localized()
            lbNumberOfAdult.textColor = UIColor.darkGray
        }
        
        if textField == txtNumberOfChild {
            lbNumberOfChild.text = "reservation_lb_number_of_child".localized()
            lbNumberOfChild.textColor = UIColor.darkGray
        }
        
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView == tvComment {
            requestReservation()
        }
    }
    
    func addDoneButtonOnKeyboard() {
        let doneToolbar: UIToolbar = UIToolbar(frame: CGRect(x: 0, y: 0, width: 320, height: 50))
        doneToolbar.barStyle       = UIBarStyle.default
        let flexSpace              = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        let done: UIBarButtonItem  = UIBarButtonItem(title: "common_done".localized(), style: UIBarButtonItemStyle.done, target: self, action: #selector(ReservationViewController.doneButtonAction))
        
        var items = [UIBarButtonItem]()
        items.append(flexSpace)
        items.append(done)
        
        doneToolbar.items = items
        doneToolbar.sizeToFit()
        
        txtPhone.inputAccessoryView = doneToolbar
        txtNumberOfAdult.inputAccessoryView = doneToolbar
        txtNumberOfChild.inputAccessoryView = doneToolbar
        tvComment.inputAccessoryView = doneToolbar
    }
    
    func doneButtonAction() {
        txtPhone.resignFirstResponder()
        txtNumberOfAdult.resignFirstResponder()
        txtNumberOfChild.resignFirstResponder()
        tvComment.resignFirstResponder()
    }
    
    @IBAction func indexChanged(_ sender: UISegmentedControl) {
        if let resInfo = self.resInfo {
            switch segmentReserveType.selectedSegmentIndex {
            case 0:
                
                loadNormalTableInfo(data: resInfo)
                loadReserveTimeNormalList(data: resInfo)
                
                break
            case 1:
                
                showVIPConditionPopup()
                loadVIPRoomInfo(data: resInfo)
                loadReserveTimeVIPList(data: resInfo)
                
                break
            default:
                break
            }
        }
    }
    
    func loadRestaurantInfo() {
        
        SVProgressHUD.show()
        
        APIService.sharedService.getRestaurantById(Constant.DEFAULT_SHOP_INFO_ID, completed: { [weak self] (result, code, message, rsRestaurant) in
            
            guard result else {
                SVProgressHUD.dismiss()
                self?.transitionViewStates(loading: false, error: NSError(domain: "error_internet_unavailable_title".localized(), code: 1, userInfo: nil), animated: false, completion: nil)
                return
            }
            
            guard code != ErrorCode.ERROR_CODE_FAIL else {
                SVProgressHUD.dismiss()
                self?.transitionViewStates(loading: false, error: NSError(domain: "error_server_fail".localized(), code: 1, userInfo: nil), animated: false, completion: nil)
                return
            }
            
            guard code != ErrorCode.ERROR_CODE_NOT_FOUND else {
                SVProgressHUD.dismiss()
                self?.transitionViewStates(loading: false, error: NSError(domain: "error_unknown".localized(), code: 1, userInfo: nil), animated: false, completion: nil)
                return
            }
            
            guard let res = rsRestaurant else {
                SVProgressHUD.dismiss()
                return
            }
            
            self?.endLoading(error: nil)
            self?.resInfo = rsRestaurant
            self?.loadReserveDateList(data: res)
            
            if self?.segmentReserveType.selectedSegmentIndex == 0 {
                self?.loadNormalTableInfo(data: res)
                self?.loadReserveTimeNormalList(data: res)
            } else {
                self?.loadVIPRoomInfo(data: res)
                self?.loadReserveTimeVIPList(data: res)
            }
            
            SVProgressHUD.dismiss()
        })
    }
    
    func loadCustomerData() {
        if let customer = CurrentCustomerInfo.shareInfo.customer {
            txtName.text = customer.name
            txtEmail.text = customer.email
            txtPhone.text = customer.phone
        }
    }
    
    func showVIPConditionPopup() {
        if self.resInfo != nil {
            if Localize.currentLanguage() == "en" {
                if let vip_condition = resInfo?.vip_condition {
                    // Prepare data for popup
                    let title = "reservation_lb_vip_condition".localized()
                    let message = vip_condition
                    
                    // Create the dialog
                    let popup = PopupDialog(
                        title: title,
                        message: message,
                        buttonAlignment: .horizontal,
                        transitionStyle: .zoomIn,
                        gestureDismissal: true
                    ) {}
                    
                    // Add buttons to dialog
                    popup.addButtons([DefaultButton(title: "common_ok".localized()) {}])
                    
                    // Present dialog
                    self.present(popup, animated: true, completion: nil)
                } else {
                    print("Error load information from restaurant")
                }
            } else {
                if let vip_condition_cn = resInfo?.vip_condition_cn {
                    // Prepare data for popup
                    let title = "reservation_lb_vip_condition".localized()
                    let message = vip_condition_cn
                    
                    // Create the dialog
                    let popup = PopupDialog(
                        title: title,
                        message: message,
                        buttonAlignment: .horizontal,
                        transitionStyle: .zoomIn,
                        gestureDismissal: true
                    ) {}
                    
                    // Add buttons to dialog
                    popup.addButtons([DefaultButton(title: "OK") {}])
                    
                    // Present dialog
                    self.present(popup, animated: true, completion: nil)
                } else {
                    print("Error load information from restaurant")
                }
            }
        } else {
            print("Restaurant information not found")
        }
    }
    
    func showConfirmPopup() {
        if self.resInfo != nil {
            if Localize.currentLanguage() == "en" {
                if let confirm_reserve = resInfo?.confirm_reserve {
                    // Prepare data for popup
                    let title = "common_info".localized()
                    let message = confirm_reserve
                    
                    // Create the dialog
                    let popup = PopupDialog(
                        title: title,
                        message: message,
                        buttonAlignment: .horizontal,
                        transitionStyle: .zoomIn,
                        gestureDismissal: true
                    ) {}
                    
                    // Add buttons to dialog
                    popup.addButtons([DefaultButton(title: "common_ok".localized()) {
                        _ = self.navigationController?.popViewController(animated: true)
                        }])
                    
                    // Present dialog
                    self.present(popup, animated: true, completion: nil)
                } else {
                    print("Error load information from restaurant")
                }
            } else {
                if let confirm_reserve_cn = resInfo?.confirm_reserve_cn {
                    // Prepare data for popup
                    let title = "common_info".localized()
                    let message = confirm_reserve_cn
                    
                    // Create the dialog
                    let popup = PopupDialog(
                        title: title,
                        message: message,
                        buttonAlignment: .horizontal,
                        transitionStyle: .zoomIn,
                        gestureDismissal: true
                    ) {}
                    
                    // Add buttons to dialog
                    popup.addButtons([DefaultButton(title: "common_ok".localized()) {
                        _ = self.navigationController?.popViewController(animated: true)
                        }])
                    
                    // Present dialog
                    self.present(popup, animated: true, completion: nil)
                } else {
                    print("Error load information from restaurant")
                }
            }
        } else {
            print("Restaurant information not found")
        }
    }
    
    // load information for normal table
    func loadNormalTableInfo(data: Restaurant) {
        if let min_normal_seat = data.min_normal_seat {
            if let max_normal_seat = data.max_normal_seat {
                lbMinAndMax.text = String(format: NSLocalizedString("reservation_lb_number_of_pax_hint".localized(), comment: ""), "\(min_normal_seat.toString())", "\(max_normal_seat.toString())")
            }
        }
    }
    
    // load information for VIP room
    func loadVIPRoomInfo(data: Restaurant) {
        if let min_vip_seat = data.min_vip_seat {
            if let max_vip_seat = data.max_vip_seat {
                lbMinAndMax.text = String(format: NSLocalizedString("reservation_lb_number_of_pax_hint".localized(), comment: ""), "\(min_vip_seat.toString())", "\(max_vip_seat.toString())")
            }
        }
    }
    
    // load list of date filter by limit & week_day
    func loadReserveDateList(data: Restaurant) {
        let currentLocale = Locale.current
        var currentCalendar = Calendar.current
        let currentDate = Date()
        var components = DateComponents()
        
        currentCalendar.locale = currentLocale
        components.calendar = currentCalendar
        
        if let api_limit_date = data.limit_days_book {
            if let api_week_days = data.week_days {
                let api_week_days_arr = api_week_days.components(separatedBy: ",")
                
                for i in 0...api_limit_date - 1
                {
                    components.day = i
                    
                    let addDate : Date = currentCalendar.date(byAdding: components, to: currentDate)!
                    let addDateStr = dateFormatter.string(from: addDate)
                    
                    for api_week_day in api_week_days_arr {
                        // week_days from API
                        let weekDayAPI : Int? = api_week_day.integer
                        // week_days from array
                        let weekDay = addDate.getDayOfWeek()
                        // check weekday
                        if weekDay == weekDayAPI {
                            reserveDateArr.append(addDateStr)
                            availableDays.append(addDate)
                        }
                    }
                }
                
                pvReserveDate.reloadAllComponents()
            }
        }
    }
    
    func loadShopInfoList() {
        let PAGINATION_LIMIT = 100
        APIService.sharedService.getRestaurantList(PAGINATION_LIMIT, 0, completed: { [weak self] (result, code, message, rsTotal, rsRestaurant) in
            // load error view
            guard result else {
                SVProgressHUD.dismiss()
                self?.transitionViewStates(loading: false, error: NSError(domain: "error_internet_unavailable_title".localized(), code: 1, userInfo: nil), animated: false, completion: nil)
                return
            }
            
            guard code != ErrorCode.ERROR_CODE_FAIL else {
                SVProgressHUD.dismiss()
                self?.transitionViewStates(loading: false, error: NSError(domain: "error_server_fail".localized(), code: 1, userInfo: nil), animated: false, completion: nil)
                return
            }
            
            guard code != ErrorCode.ERROR_CODE_NOT_FOUND else {
                SVProgressHUD.dismiss()
                self?.transitionViewStates(loading: false, error: NSError(domain: "error_unknown".localized(), code: 1, userInfo: nil), animated: false, completion: nil)
                return
            }
            // load home data
            self?.endLoading(error: nil)
            
            if let resArr = rsRestaurant {
                if let firstRes = resArr.first {
                    if Localize.currentLanguage() == "en" {
                        self?.shopInfoArr.append(firstRes.name!)
                    } else {
                        self?.shopInfoArr.append(firstRes.name_cn!)
                    }
                    
                    self?.pvShopInfo.reloadAllComponents()
                }
            }
        })
    }
    
    // load list of time for normal table
    func loadReserveTimeNormalList(data: Restaurant) {
        if let reserveTime = data.normal_time {
            let rt_normal_arr = reserveTime.components(separatedBy: ",")
            for time_normal in rt_normal_arr
            {
//                reserveTimeVIPArr.removeAll()
                reserveTimeNormalArr.append(time_normal)
            }
            
            pvReserveTime.reloadAllComponents()
        }
    }
    
//    // load list of time for VIP room
//    func loadReserveTimeVIPList(data: Restaurant) {
//        if let reserveTime = data.vip_time {
//            let rt_vip_arr = reserveTime.components(separatedBy: ",")
//            for time_vip in rt_vip_arr
//            {
//                reserveTimeNormalArr.removeAll()
//                reserveTimeVIPArr.append(time_vip)
//            }
//            
//            pvReserveTime.reloadAllComponents()
//        }
//    }
    
    func insertReservation(){
        
        SVProgressHUD.show()
        SVProgressHUD.setDefaultMaskType(SVProgressHUDMaskType.black)
        
        // set type for normal table or vip room
        var reserveType : Int?
        if segmentReserveType.selectedSegmentIndex == 1 {
            reserveType = 1
        } else {
            reserveType = 0
        }
        
        // get number of adult
        var numberOfAdult : Int?
        if let nod = txtNumberOfAdult.text {
            numberOfAdult = nod.integer
        }
        
        // get number of child
        var numberOfChild : Int?
        if let noc = txtNumberOfChild.text {
            numberOfChild = noc.integer
        }
        
//        let selectedShopIndex = pvShopInfo.selectedRow(inComponent: 0)
//        let shopInfo = shopInfoId[selectedShopIndex]
        
        let selectedDateIndex = pvReserveDate.selectedRow(inComponent: 0)
        let reserveDate = availableDays[selectedDateIndex]
        
        let reserveTime : String?
        if segmentReserveType.selectedSegmentIndex == 1 {
            let selectedTimeVIPIndex = pvReserveTime.selectedRow(inComponent: 0)
            reserveTime = reserveTimeVIPArr[selectedTimeVIPIndex] + ":00"
        } else {
            let selectedTimeNormalIndex = pvReserveTime.selectedRow(inComponent: 0)
            reserveTime = reserveTimeNormalArr[selectedTimeNormalIndex] + ":00"
        }
        
        let reservationData = Reservation(
            restaurant_id: Constant.DEFAULT_SHOP_INFO_ID,
            customer_id: CurrentCustomerInfo.shareInfo.customer?.id,
            name: txtName.text,
            email: txtEmail.text,
            phone: txtPhone.text,
            reserve_type: reserveType,
            reserve_date: reserveDate,
            reserve_time: reserveTime,
            num_adult: numberOfAdult,
            num_child: numberOfChild,
            status: Constant.DEFAULT_RESERVATION_STATUS,
            comment: tvComment.text
        )
        
        let lang = Localize.currentLanguage()

        APIService.sharedService.insertReservation(reservationData, lang, completed: { (result, error, message ) in
            guard result else {
                let alertController = UIAlertController(title: "common_info".localized(), message: "common_connection_fail".localized(), preferredStyle: UIAlertControllerStyle.alert)
                let okAction = UIAlertAction(title: "common_ok".localized(), style: UIAlertActionStyle.default) { (result : UIAlertAction) -> Void in
                }
                alertController.addAction(okAction)
                self.present(alertController, animated: true, completion: nil)
                return
            }
            
            SVProgressHUD.dismiss()
            self.showConfirmPopup()
        })
    }
    
    func requestReservation(){
        
        // validate for name: accept full name with format "LastName FirstName"
        if let name = txtName.text {
            // Returns a new string made by removing from both ends of the String characters contained in a given character set.
            let cs = CharacterSet.init(charactersIn: " ")
            let trim = name.trimmingCharacters(in: cs)
            
            // If trimmed name have contains space characters
            guard trim.characters.contains(" ") else {
                txtName.becomeFirstResponder()
                lbNameStatus.text = "customer_lb_name_required_full_name".localized()
                lbNameStatus.textColor = AppColor.PRIMARY_COLOR
                return
            }
        }
        
        // validate for email
        if let email = txtEmail.text {
            
            guard email.characters.count > 0 else {
                txtEmail.becomeFirstResponder()
                lbEmailStatus.text = "customer_lb_email_required".localized()
                lbEmailStatus.textColor = AppColor.PRIMARY_COLOR
                return
            }
            
            guard email.isEmail(email: email) else {
                txtEmail.becomeFirstResponder()
                lbEmailStatus.text = "customer_lb_email_invalid".localized()
                lbEmailStatus.textColor = AppColor.PRIMARY_COLOR
                return
            }
        }
        
        // validate for phone number: only digits, 8 digits no less no more
        if let phone = txtPhone.text {
            
            guard phone.isPhoneNumber(phoneNumber: phone) else {
                txtPhone.becomeFirstResponder()
                lbPhoneStatus.text = "customer_lb_phone_invalid".localized()
                lbPhoneStatus.textColor = AppColor.PRIMARY_COLOR
                return
            }
        }
        
        // validate for number of pax
        if let adult = txtNumberOfAdult.text {
            if let child = txtNumberOfChild.text {
                // check number of adult must be digit
                guard adult.isDigit(digit: adult) else {
                    txtNumberOfAdult.becomeFirstResponder()
                    lbNumberOfAdult.text = "reservation_lb_noa_required".localized()
                    lbNumberOfAdult.textColor = AppColor.PRIMARY_COLOR
                    return
                }
                // check number of child must be digit
                guard child.isDigit(digit: child) else {
                    txtNumberOfChild.becomeFirstResponder()
                    lbNumberOfChild.text = "reservation_lb_noc_required".localized()
                    lbNumberOfChild.textColor = AppColor.PRIMARY_COLOR
                    return
                }
                // check number of adult must be more than 0
                guard adult.integer >= 1 else {
                    txtNumberOfAdult.becomeFirstResponder()
                    lbNumberOfAdult.text = "reservation_lb_noa_not_zero".localized()
                    lbNumberOfAdult.textColor = AppColor.PRIMARY_COLOR
                    return
                }
                
                // check min & max
                if segmentReserveType.selectedSegmentIndex == 1 {
                    if let min_vip_seat = resInfo?.min_vip_seat {
                        if let max_vip_seat = resInfo?.max_vip_seat {
                            numberOfPaxInt = adult.integer + child.integer
                            if let num_pax = numberOfPaxInt {
                                guard num_pax >= min_vip_seat, num_pax <= max_vip_seat else {
                                    txtNumberOfAdult.becomeFirstResponder()
                                    lbNumberOfAdult.text = String(format: NSLocalizedString("reservation_lb_the_total_invalid".localized(), comment: ""), "\(min_vip_seat.toString())", "\(max_vip_seat.toString())")
                                    lbNumberOfAdult.textColor = AppColor.PRIMARY_COLOR
                                    return
                                }
                            }
                        }
                    }
                } else {
                    if let min_normal_seat = resInfo?.min_normal_seat {
                        if let max_normal_seat = resInfo?.max_normal_seat {
                            numberOfPaxInt = adult.integer + child.integer
                            if let num_pax = numberOfPaxInt {
                                guard num_pax >= min_normal_seat, num_pax <= max_normal_seat else {
                                    txtNumberOfAdult.becomeFirstResponder()
                                    lbNumberOfAdult.text = String(format: NSLocalizedString("reservation_lb_the_total_invalid".localized(), comment: ""), "\(min_normal_seat.toString())", "\(max_normal_seat.toString())")
                                    lbNumberOfAdult.textColor = AppColor.PRIMARY_COLOR
                                    return
                                }
                            }
                        }
                    }
                }
            }
        }
        
        if let beforeMinutes = resInfo?.before_minutes {
            let currentDate = Date()
            let calendar = Calendar.current
            let addMinutesDate = calendar.date(byAdding: .minute, value: beforeMinutes, to: currentDate)
            let selectedDateIndex = pvReserveDate.selectedRow(inComponent: 0)
            let reserveDate = availableDays[selectedDateIndex]
            if let dateWithBeforeMinutes = addMinutesDate {
                let reserveTime : String?
                if segmentReserveType.selectedSegmentIndex == 1 {
                    let selectedTimeVIPIndex = pvReserveTime.selectedRow(inComponent: 0)
                    reserveTime = reserveTimeVIPArr[selectedTimeVIPIndex]
                    let reserveTimeArr = reserveTime?.components(separatedBy: ":")
                    if let timeVIPArr = reserveTimeArr {
                        let pickerHours = timeVIPArr[0].integer
                        let pickerMinutes = timeVIPArr[1].integer
                        let setReserveDate : Date = calendar.date(bySettingHour: pickerHours, minute: pickerMinutes, second: 0, of: reserveDate)!
                        guard setReserveDate >= dateWithBeforeMinutes else {
                            lbReserveTime.text = String(format: NSLocalizedString("reservation_lb_time_after".localized(), comment: ""), "\(beforeMinutes.toString())")
                            lbReserveTime.textColor = AppColor.PRIMARY_COLOR
                            return
                        }
                    }
                } else {
                    let selectedTimeNormalIndex = pvReserveTime.selectedRow(inComponent: 0)
                    reserveTime = reserveTimeNormalArr[selectedTimeNormalIndex]
                    let reserveTimeArr = reserveTime?.components(separatedBy: ":")
                    if let timeNormalArr = reserveTimeArr {
                        let pickerHours = timeNormalArr[0].integer
                        let pickerMinutes = timeNormalArr[1].integer
                        let setReserveDate : Date = calendar.date(bySettingHour: pickerHours, minute: pickerMinutes, second: 0, of: reserveDate)!
                        guard setReserveDate >= dateWithBeforeMinutes else {
                            lbReserveTime.text = String(format: NSLocalizedString("reservation_lb_time_after".localized(), comment: ""), "\(beforeMinutes.toString())")
                            lbReserveTime.textColor = AppColor.PRIMARY_COLOR
                            return
                        }
                    }
                }
            }
        }
        
        insertReservation()
        
    }
    
    @IBAction func requestReservation(_ sender: Any) {
        requestReservation()
    }
    
}

extension ReservationViewController : UIPickerViewDelegate, UIPickerViewDataSource {
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if pickerView == pvReserveDate {
            return reserveDateArr.count
        } else if pickerView == pvShopInfo {
            return shopInfoArr.count
        } else {
            if segmentReserveType.selectedSegmentIndex == 0 {
                return reserveTimeNormalArr.count
            } else {
                return reserveTimeVIPArr.count
            }
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if pickerView == pvReserveDate {
            return reserveDateArr[row]
        } else if pickerView == pvShopInfo {
            return shopInfoArr[row]
        } else {
            if segmentReserveType.selectedSegmentIndex == 0 {
                return reserveTimeNormalArr[row]
            } else {
                return reserveTimeVIPArr[row]
            }
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if pickerView == pvReserveDate {
//            selectedReserveDate = reserveDateArr[row] as String
        } else {
            if segmentReserveType.selectedSegmentIndex == 0 {
                selectedReserveTimeNormal = reserveTimeNormalArr[row] as String
            } else {
                selectedReserveTimeVIP = reserveTimeVIPArr[row] as String
            }
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, viewForRow row: Int, forComponent component: Int, reusing view: UIView?) -> UIView {
        var label = view as! UILabel!
        if label == nil {
            label = UILabel()
        }
        
        if pickerView == pvReserveDate {
            let data = reserveDateArr[row]
            let title = NSAttributedString(string: data, attributes: [NSFontAttributeName: UIFont.systemFont(ofSize: 18.0, weight: UIFontWeightRegular)])
            label?.attributedText = title
            label?.textAlignment = .center
            
            return label!
        } else if pickerView == pvShopInfo {
            let data = shopInfoArr[row]
            let title = NSAttributedString(string: data, attributes: [NSFontAttributeName: UIFont.systemFont(ofSize: 18.0, weight: UIFontWeightRegular)])
            label?.attributedText = title
            label?.textAlignment = .center
            
            return label!
        } else {
            if segmentReserveType.selectedSegmentIndex == 0 {
                let data = reserveTimeNormalArr[row]
                let title = NSAttributedString(string: data, attributes: [NSFontAttributeName: UIFont.systemFont(ofSize: 18.0, weight: UIFontWeightRegular)])
                label?.attributedText = title
                label?.textAlignment = .center
                
                return label!
            } else {
                let data = reserveTimeVIPArr[row]
                let title = NSAttributedString(string: data, attributes: [NSFontAttributeName: UIFont.systemFont(ofSize: 18.0, weight: UIFontWeightRegular)])
                label?.attributedText = title
                label?.textAlignment = .center
                
                return label!
            }
        }
    }
}
