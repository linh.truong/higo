//
//  SimplyBookViewController.swift
//  Roann
//
//  Created by Linh Trương on 3/20/17.
//  Copyright © 2017 IBS. All rights reserved.
//

import UIKit
import SVProgressHUD
import Localize_Swift
import PopupDialog
import StatefulViewController

class SimplyBookViewController: UIViewController, UITextFieldDelegate, UITextViewDelegate, StatefulViewController {
    
    // Variable
    var dateFormatter = DateFormatter()
    var shopInfoArr: [Restaurant] = []
    var eventArr : [SimplyBookEvent] = []
    var unitArr : [SimplyBookUnit] = []
    var additionalArr : [SimplyBookAdditional] = []
    var workingDateArr : [SimplyBookWorkingDate] = []
    var timeArr : [SimplyBookTime] = []
    var reserveDateArr: [String] = []
    var reserveDaysArr: [Date] = []
    var reserveTimeArr: [String] = []
    var selectedShopInfoId: Int?
    var selectedEventId: Int?
    var selectedDuration: Int?
    var selectedUnitId: Int?
    var selectedDate: String?
    var selectedDays: Date?
    var selectedTime: String?
    var confirmReserve : String?
    var confirmReserveCN : String?
    // Outlet
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var viewBackground: UIView!
    @IBOutlet weak var lbName: UILabel!
    @IBOutlet weak var txtCusName: UITextField!
    @IBOutlet weak var lbEmail: UILabel!
    @IBOutlet weak var txtCusEmail: UITextField!
    @IBOutlet weak var lbPhone: UILabel!
    @IBOutlet weak var txtCusPhone: UITextField!
    @IBOutlet weak var lbShopInfo: UILabel!
    @IBOutlet weak var pvShopInfo: RegisterPickerView!
    @IBOutlet weak var lbDurations: UILabel!
    @IBOutlet weak var pvDurations: RegisterPickerView!
    @IBOutlet weak var lbTableType: UILabel!
    @IBOutlet weak var pvTableType: RegisterPickerView!
    @IBOutlet weak var lbReserveDate: UILabel!
    @IBOutlet weak var pvReserveDate: ReserveDatePickerView!
    @IBOutlet weak var lbReserveTime: UILabel!
    @IBOutlet weak var pvReserveTime: RegisterPickerView!
    @IBOutlet weak var lbNumberOfAdult: UILabel!
    @IBOutlet weak var txtNumberOfAdult: UITextField!
    @IBOutlet weak var lbNumberOfChild: UILabel!
    @IBOutlet weak var txtNumberOfChild: UITextField!
    @IBOutlet weak var lbCommentStatus: UILabel!
    @IBOutlet weak var tvComment: UITextView!
    @IBOutlet weak var btnRequest: UIButton!
    @IBOutlet weak var pvDurationsConst: NSLayoutConstraint!
    @IBOutlet weak var pvTableTypeConst: NSLayoutConstraint!
    @IBOutlet weak var pvShopInfoConst: NSLayoutConstraint!
    @IBOutlet weak var lbDurationConst: NSLayoutConstraint!
    @IBOutlet weak var lbTableTypeConst: NSLayoutConstraint!
    @IBOutlet weak var lbShopInfoConst: NSLayoutConstraint!
    @IBOutlet weak var lbNumberOfChild2: UILabel!
    @IBOutlet weak var txtNumberOfChild2: UITextField!
    @IBOutlet weak var btnTandC: UIButton!
    @IBOutlet weak var viewTandC: UIView!
    @IBOutlet weak var tvTermAndCondition: UITextView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        screenInit()
        
        let tapGesture = UITapGestureRecognizer.init(target: self, action: #selector(SimplyBookViewController.closeTermAndCondition))
        self.view.addGestureRecognizer(tapGesture)
        
        // refresh language ( localized )
        NotificationCenter.default.addObserver(self, selector: #selector(SimplyBookViewController.refreshLanguage), name: NSNotification.Name(LCLLanguageChangeNotification), object: nil)
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    func screenInit(){
        setDelegate()
        setScreenLayout()
        setLangLocalized()
        loadCustomerData()
        setDefaultDateTime()
        getShopInfoAPI()
        changeKeyboardLayout()
        dismissKeyboardWhenTapped()
        addDoneButtonOnKeyboard()
        setupErrorViewState()
        setupInitialViewState()
    }
    
    func setDelegate() {
        self.txtCusName.delegate = self
        self.txtCusEmail.delegate = self
        self.txtCusPhone.delegate = self
        self.txtNumberOfAdult.delegate = self
        self.txtNumberOfChild.delegate = self
        self.txtNumberOfChild2.delegate = self
        self.tvComment.delegate = self
        self.pvShopInfo.delegate = self
        self.pvDurations.delegate =  self
        self.pvTableType.delegate = self
        self.pvReserveDate.delegate = self
        self.pvReserveTime.delegate = self
        self.pvShopInfo.dataSource = self
        self.pvDurations.dataSource = self
        self.pvTableType.dataSource = self
        self.pvReserveDate.dataSource = self
        self.pvReserveTime.dataSource = self
    }
    
    func setScreenLayout(){
        tvTermAndCondition.scrollRangeToVisible((NSMakeRange(0,0)))
        edgesForExtendedLayout = []
        viewTandC.layer.cornerRadius = 5
        viewTandC.isHidden = true
        pvShopInfo.isOpaque = true
        pvDurations.isOpaque = true
        pvTableType.isOpaque = true
        pvReserveDate.isOpaque = true
        pvReserveTime.isOpaque = true
        btnRequest.layer.cornerRadius = 5
        btnRequest.backgroundColor = AppColor.PRIMARY_COLOR
        pvDurations.isHidden = true
        pvDurationsConst.constant = 0
        pvTableTypeConst.constant = 0
        lbDurationConst.constant = 0
        lbTableTypeConst.constant = 0
        
    }
    
    func setLangLocalized() {
        self.title = "reservation_lb_request".localized()
        lbName.setRequiredAttribute("reservation_lb_name".localized())
        lbEmail.setRequiredAttribute("reservation_lb_email".localized())
        lbPhone.setRequiredAttribute("reservation_lb_phone".localized())
        lbShopInfo.setRequiredAttribute("reservation_lb_shop_info".localized())
        lbReserveDate.setRequiredAttribute("reservation_lb_reserve_date".localized())
        lbReserveTime.setRequiredAttribute("reservation_lb_reserve_time".localized())
        lbNumberOfAdult.setRequiredAttribute("reservation_lb_number_of_adult".localized())
        lbNumberOfChild.text = "reservation_lb_number_of_child".localized()
        lbNumberOfChild2.text = "reservation_lb_number_of_child2".localized()
        lbCommentStatus.text = "customer_lb_comment".localized()
        btnRequest.setTitle("reservation_btn_send_request".localized(), for: .normal)
        btnTandC.setTitle("reservation_btn_tandc".localized(), for: .normal)
    }
    
    func setupErrorViewState() {
        let failureView = ErrorView(frame: view.frame)
        failureView.tapGestureRecognizer.addTarget(self, action: #selector(SimplyBookViewController.getShopInfoAPI))
        errorView = failureView
    }
    
    func refreshLanguage() {
        setLangLocalized()
        setupInitialViewState()
        setupErrorViewState()
    }
    
    func setDefaultDateTime() {
        dateFormatter.dateFormat = "EEEE, MMMM dd, yyyy"
        let currentDate = Date()
        self.selectedDays = currentDate
        let currentDateStr = dateFormatter.string(from: currentDate)
        self.selectedDate = currentDateStr
    }
    
    func changeKeyboardLayout(){
        txtCusName.returnKeyType = UIReturnKeyType.next
        txtCusEmail.returnKeyType = UIReturnKeyType.next
        txtCusPhone.returnKeyType = UIReturnKeyType.next
        txtNumberOfAdult.returnKeyType = UIReturnKeyType.next
        txtNumberOfChild.returnKeyType = UIReturnKeyType.next
        txtNumberOfChild2.returnKeyType = UIReturnKeyType.next
        tvComment.returnKeyType = UIReturnKeyType.go
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        
        if textField == txtCusName {
            lbName.textColor = AppColor.DARK_GRAY_COLOR
            lbName.setRequiredAttribute("reservation_lb_name".localized())
        }
        
        if textField == txtCusEmail {
            lbEmail.textColor = AppColor.DARK_GRAY_COLOR
            lbEmail.setRequiredAttribute("reservation_lb_email".localized())
        }
        
        if textField == txtCusPhone {
            lbPhone.textColor = AppColor.DARK_GRAY_COLOR
            lbPhone.setRequiredAttribute("reservation_lb_phone".localized())
        }
        
        if textField == txtNumberOfAdult {
            lbNumberOfAdult.textColor = AppColor.DARK_GRAY_COLOR
            lbNumberOfAdult.setRequiredAttribute("reservation_lb_number_of_adult".localized())
        }
        
        if textField == txtNumberOfChild {
            lbNumberOfChild.textColor = AppColor.DARK_GRAY_COLOR
            lbNumberOfChild.text = "reservation_lb_number_of_child".localized()
        }
        
        if textField == txtNumberOfChild2 {
            lbNumberOfChild2.textColor = AppColor.DARK_GRAY_COLOR
            lbNumberOfChild2.text = "reservation_lb_number_of_child2".localized()
        }
         
    }
    
    func addDoneButtonOnKeyboard() {
        let doneToolbar: UIToolbar = UIToolbar(frame: CGRect(x: 0, y: 0, width: 320, height: 50))
        doneToolbar.barStyle       = UIBarStyle.default
        let flexSpace              = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        let done: UIBarButtonItem  = UIBarButtonItem(title: "common_done".localized(), style: UIBarButtonItemStyle.done, target: self, action: #selector(SimplyBookViewController.doneButtonAction))
        
        var items = [UIBarButtonItem]()
        items.append(flexSpace)
        items.append(done)
        
        doneToolbar.items = items
        doneToolbar.sizeToFit()
        
        txtCusPhone.inputAccessoryView = doneToolbar
        txtNumberOfAdult.inputAccessoryView = doneToolbar
        txtNumberOfChild.inputAccessoryView = doneToolbar
        txtNumberOfChild2.inputAccessoryView = doneToolbar
        tvComment.inputAccessoryView = doneToolbar
    }
    
    func doneButtonAction() {
        txtCusPhone.resignFirstResponder()
        txtNumberOfAdult.resignFirstResponder()
        txtNumberOfChild.resignFirstResponder()
        txtNumberOfChild2.resignFirstResponder()
        tvComment.resignFirstResponder()
    }
    
    func loadCustomerData(){
        if let customer = CurrentCustomerInfo.shareInfo.loadCustomerInfo() {
            txtCusName.text = customer.name
            txtCusEmail.text = customer.email
            txtCusPhone.text = customer.phone
        } else {
            self.showRequiredLoginPopup()
        }
    }
    
    func getShopInfoAPI(){
        
        let PAGINATION_LIMIT : Int = 100
        let PAGINATION_OFFSET : Int = 0
        
        SVProgressHUD.show()
        
        APIService.sharedService.getRestaurantList(PAGINATION_LIMIT, PAGINATION_OFFSET, completed: { [weak self] (result, code, message, rsTotal, rsShopInfo) in
            
            // load error view
            guard result else {
                SVProgressHUD.dismiss()
                self?.transitionViewStates(loading: false, error: NSError(domain: "error_internet_unavailable_title".localized(), code: 1, userInfo: nil), animated: false, completion: nil)
                return
            }
            
            guard code != ErrorCode.ERROR_CODE_FAIL else {
                SVProgressHUD.dismiss()
                self?.transitionViewStates(loading: false, error: NSError(domain: "error_server_fail".localized(), code: 1, userInfo: nil), animated: false, completion: nil)
                return
            }
            
            guard code != ErrorCode.ERROR_CODE_NOT_FOUND else {
                SVProgressHUD.dismiss()
                self?.transitionViewStates(loading: false, error: NSError(domain: "error_unknown".localized(), code: 1, userInfo: nil), animated: false, completion: nil)
                return
            }
            // load home data
            self?.endLoading(error: nil)
            self?.finishCallApiWithShopInfo(rsShopInfo, rsTotal)
        })
    }
    
    func finishCallApiWithShopInfo(_ shopInfoList: [Restaurant]?, _ total: Int?){
        
        guard let restaurants = shopInfoList else {
            self.transitionViewStates(loading: false, error: NSError(domain: "error_server_fail".localized(), code: 1, userInfo: nil), animated: false, completion: nil)
            SVProgressHUD.dismiss()
            return
        }
        
        guard total! > 0 else {
            self.transitionViewStates(loading: false, error: NSError(domain: "error_server_fail".localized(), code: 1, userInfo: nil), animated: false, completion: nil)
            SVProgressHUD.dismiss()
            return
        }
        
        self.endLoading(error: nil)
        self.loadShopInfo(data: restaurants)
        
        getBookingInfoAPI()
    }
    
    func getBookingInfoAPI(){
        
        SVProgressHUD.show()
        
        APIService.sharedService.getBooking(selectedShopInfoId!) { (result, code, mess, rsEvent, rsUnit, rsAdditional, rsWorkingDate) in
            
            guard result else {
                SVProgressHUD.dismiss()
                self.transitionViewStates(loading: false, error: NSError(domain: "error_internet_unavailable_title".localized(), code: 1, userInfo: nil), animated: false, completion: nil)
                return
            }
            
            guard code != ErrorCode.ERROR_CODE_FAIL else {
                SVProgressHUD.dismiss()
                self.transitionViewStates(loading: false, error: NSError(domain: "error_server_fail".localized(), code: 1, userInfo: nil), animated: false, completion: nil)
                return
            }
            
            guard code != ErrorCode.ERROR_CODE_NOT_FOUND else {
                SVProgressHUD.dismiss()
                self.transitionViewStates(loading: false, error: NSError(domain: "error_unknown".localized(), code: 1, userInfo: nil), animated: false, completion: nil)
                return
            }
            
            self.endLoading(error: nil)
            self.finishCallAPIWithGetBooking(rsEvent, rsUnit, rsAdditional, rsWorkingDate)
        }
    }
    
    func finishCallAPIWithGetBooking(_ eventList: [SimplyBookEvent]?, _ unitList: [SimplyBookUnit]?, _ additionalList: [SimplyBookAdditional]?, _ workingDateList: [SimplyBookWorkingDate]?){
        
        guard let event = eventList else {
            self.transitionViewStates(loading: false, error: NSError(domain: "error_server_fail".localized(), code: 1, userInfo: nil), animated: false, completion: nil)
            SVProgressHUD.dismiss()
            return
        }
        
        guard let unit = unitList else {
            self.transitionViewStates(loading: false, error: NSError(domain: "error_server_fail".localized(), code: 1, userInfo: nil), animated: false, completion: nil)
            SVProgressHUD.dismiss()
            return
        }
        
        guard let additional = additionalList else {
            self.transitionViewStates(loading: false, error: NSError(domain: "error_server_fail".localized(), code: 1, userInfo: nil), animated: false, completion: nil)
            SVProgressHUD.dismiss()
            return
        }
        
        guard let workingDate = workingDateList else {
            self.transitionViewStates(loading: false, error: NSError(domain: "error_server_fail".localized(), code: 1, userInfo: nil), animated: false, completion: nil)
            SVProgressHUD.dismiss()
            return
        }
        
        self.endLoading(error: nil)
        self.loadDurations(data: event)
        self.loadTableType(data: unit)
        self.loadAdditional(data: additional)
        self.loadReserveDate(data: workingDate)
        SVProgressHUD.dismiss()
    }
    
//    func getReserveTimeAPI(){
//        // Get params
//        let selectedEventIndex = pvDurations.selectedRow(inComponent: 0)
//        let selectedEventIndexValue = eventArr[selectedEventIndex].id?.integer
//        let selectedUnitIndex = pvTableType.selectedRow(inComponent: 0)
//        let selectedUnitIndexValue = unitArr[selectedUnitIndex].id?.integer
//        // Call API
//        dateFormatter.dateFormat = "yyyy-MM-dd"
//        let selectedDateParams = dateFormatter.string(from: self.selectedDays!)
//        
//        APIService.sharedService.getReserveTime(self.selectedShopInfoId!, selectedDateParams, selectedUnitId ?? selectedUnitIndexValue!, selectedEventId ?? selectedEventIndexValue!) { (result, code, mess, rsTime) in
//            // Error screen
//            guard result else {
//                SVProgressHUD.dismiss()
//                self.transitionViewStates(loading: false, error: NSError(domain: "error_internet_unavailable_title".localized(), code: 1, userInfo: nil), animated: false, completion: nil)
//                return
//            }
//            
//            guard code != ErrorCode.ERROR_CODE_FAIL else {
//                SVProgressHUD.dismiss()
//                self.transitionViewStates(loading: false, error: NSError(domain: "error_server_fail".localized(), code: 1, userInfo: nil), animated: false, completion: nil)
//                return
//            }
//            
//            guard code != ErrorCode.ERROR_CODE_NOT_FOUND else {
//                SVProgressHUD.dismiss()
//                self.transitionViewStates(loading: false, error: NSError(domain: "error_unknown".localized(), code: 1, userInfo: nil), animated: false, completion: nil)
//                return
//            }
//            // End error loading
//            self.endLoading(error: nil)
//            // Call function finish load API
//            self.finishCallAPIWithGetTime(rsTime)
//        }
//        
//    }
    
//    func finishCallAPIWithGetTime(_ timeList: [SimplyBookTime]?){
//        // Show error
//        guard let time = timeList else {
//            self.transitionViewStates(loading: false, error: NSError(domain: "error_server_fail".localized(), code: 1, userInfo: nil), animated: false, completion: nil)
//            SVProgressHUD.dismiss()
//            return
//        }
//        
//        // End error loading
//        self.endLoading(error: nil)
//        self.timeArr = time
////        self.loadReserveTime()
//        SVProgressHUD.dismiss()
//    }
    
    func loadShopInfo(data: [Restaurant]?) {
        
        for res in data! {
            self.shopInfoArr.append(res)
        }
        
        self.pvShopInfo.reloadAllComponents()
        
        if let resDefaultId = data?.first?.id {
            self.selectedShopInfoId = resDefaultId
        }
    }
    
    func loadDurations(data: [SimplyBookEvent]?){
        
        self.eventArr.removeAll()
        
        for event in data! {
            self.eventArr.append(event)
        }
        
        self.pvDurations.reloadAllComponents()
        
        if let eventDefaultId = data?.first?.id {
            self.selectedEventId = eventDefaultId.integer
        }
    }
    
    func loadTableType(data: [SimplyBookUnit]?) {
        
        self.unitArr.removeAll()
        
        for unit in data!
        {
            self.unitArr.append(unit)
        }
        
        self.pvTableType.reloadAllComponents()
        
        if let unitDefaultId = data?.first?.id {
            self.selectedUnitId = unitDefaultId.integer
        }
    }
    
    func loadReserveDate(data: [SimplyBookWorkingDate]?){
        
        self.workingDateArr = data!
        
        if let i = shopInfoArr.index(where: { $0.id == selectedShopInfoId }) {
        
        let res : Restaurant = shopInfoArr[i]
            self.confirmReserve = res.confirm_reserve
            self.confirmReserveCN = res.confirm_reserve_cn
        }
        
        // Remove before append
        self.reserveDateArr.removeAll()
        self.reserveDaysArr.removeAll()
        
        // Sort date array
        let dataSort = self.workingDateArr.sorted(by: { $0.date?.compare($1.date!) == ComparisonResult.orderedAscending })
        
        // Loop to append data
        for wd in dataSort
        {
            dateFormatter.dateFormat = "YYYY-MM-dd"
            let date = dateFormatter.date(from: wd.date!)
            self.reserveDaysArr.append(date!) // date
            dateFormatter.dateFormat = "EEEE, MMMM dd, yyyy"
            let dateStr = dateFormatter.string(from: date!)
            self.reserveDateArr.append(dateStr) // date string
        }
        
        // Reload data of picker
        self.pvReserveDate.reloadAllComponents()
        
        if let workingDateDefault = dataSort.first?.date {
            self.selectedDate = workingDateDefault
            self.loadReserveTime(date: workingDateDefault)

        }
        
    }
    
    func loadReserveTime(date: String?){
        self.reserveTimeArr.removeAll()
        if let iEvent = eventArr.index(where: { $0.id == selectedEventId?.toString() }) {
            let simplyBookEvent : SimplyBookEvent = eventArr[iEvent]
            if let iWorkingDate = workingDateArr.index(where: { $0.date == date }) {
                self.timeArr = self.workingDateArr[iWorkingDate].time!
                let dateNow = Date()
                dateFormatter.dateFormat = "YYYY-MM-dd"
                let dateNowStr = dateFormatter.string(from: dateNow)
                let dateReserveStr = date
                let fDate = dateFormatter.date(from: date!)
                self.selectedDays = fDate
                for t in self.timeArr {
                    var timeFromStr = t.from
                    let timeToStr = t.to
                    let timeToArr = timeToStr?.components(separatedBy: ":")
                    let hourToAdd = timeToArr?[0].integer
                    let minuteToAdd = timeToArr?[1].integer
                    var totalTo = hourToAdd! * 60 + minuteToAdd!
                    totalTo -= (simplyBookEvent.duration?.integer)!
                    var total : Int = 0
                    
                    while (totalTo >= total) {
                        // Rs time
                        let timeFromArr = timeFromStr?.components(separatedBy: ":")
                        let hourFromAdd = timeFromArr?[0].integer
                        let minuteFromAdd = timeFromArr?[1].integer
                        
                        total = hourFromAdd! * 60 + minuteFromAdd!
                        
                        let timeNow = Date()
                        dateFormatter.dateFormat = "HH:mm"
                        let timeNowStr = dateFormatter.string(from: timeNow)
                        
                        let timeNowArr = timeNowStr.components(separatedBy: ":")
                        let hourNowAdd = timeNowArr[0].integer
                        let minuteNowAdd = timeNowArr[1].integer
                        
                        let totalNow = hourNowAdd * 60 + minuteNowAdd
                        if ((dateReserveStr == dateNowStr && total >= totalNow) || dateReserveStr != dateNowStr ){
                            self.reserveTimeArr.append((timeFromArr?[0])! + ":" + (timeFromArr?[1])!)
                        }
                        
                        
                        total += Constant.RESERVE_TIME_INTERVAL
                        
                        
                        let newFromHour : Int = (total / 60) % 24
                        var hourFromString : String?
                        if newFromHour > 9 {
                            hourFromString = "\(newFromHour)"
                        } else {
                            hourFromString = "0\(newFromHour)"
                        }
                        
                        let newFromMinute : Int = total % 60
                        var minuteFromString : String?
                        if newFromMinute > 9 {
                            minuteFromString = "\(newFromMinute)"
                        } else {
                            minuteFromString = "0\(newFromMinute)"
                        }
                        
                        timeFromStr = hourFromString! + ":" + minuteFromString!
                        
                    }
                    
                }
                self.pvReserveTime.reloadAllComponents()
                
                if let reserveTimeDf = self.reserveTimeArr.first {
                    self.selectedTime = reserveTimeDf
                } else {
                    self.workingDateArr.remove(at: iWorkingDate)
                    self.loadReserveDate(data: self.workingDateArr)
                }
            }
        }
        
        
        
//        SVProgressHUD.dismiss()
    }
    
    func loadAdditional(data: [SimplyBookAdditional]?) {
        for add in data! {
            if add.title == SimplyBook.ADDITIONAL_NUMBER_OF_ADULT {
                lbNumberOfAdult.setRequiredAttribute("reservation_lb_number_of_adult".localized())
            }
            if add.title == SimplyBook.ADDITIONAL_NUMBER_OF_CHILD {
                lbNumberOfChild.text = "reservation_lb_number_of_child".localized()
            }
            if add.title == SimplyBook.ADDITIONAL_NUMBER_OF_CHILD2 {
                lbNumberOfChild2.text = "reservation_lb_number_of_child2".localized()
            }
            if add.title == SimplyBook.ADDITIONAL_COMMENT {
                lbCommentStatus.text = "customer_lb_comment".localized()
            }
            self.additionalArr.append(add)
        }
    }
    
    // BOOKING
    
    func booking(){
        
        SVProgressHUD.show()
        
        // Get params
        if let resId = self.selectedShopInfoId {
            if let cus = CurrentCustomerInfo.shareInfo.loadCustomerInfo() {
                dateFormatter.dateFormat = "yyyy-MM-dd"
                let customerId = cus.id
                let cusName = txtCusName.text ?? cus.name!
                let cusEmail = txtCusEmail.text ?? cus.email!
                let cusPhone = txtCusPhone.text ?? cus.phone!
                
                let eventId = self.selectedEventId?.toString()
                let unitList = self.unitArr.toJSONString()
                
                let date = self.selectedDays
                let dateStr = dateFormatter.string(from: date!)
                let time = self.selectedTime! + ":00"
                
                let numberOfAdult = txtNumberOfAdult.text?.integer ?? SimplyBook.NUMBER_OF_ADULT_DEFAULT
                let numberOfChild = txtNumberOfChild.text?.integer ?? SimplyBook.NUMBER_OF_CHILD_DEFAULT
                let numberOfChild2 = txtNumberOfChild2.text?.integer ?? SimplyBook.NUMBER_OF_CHILD_DEFAULT
                let numberOfSeat = numberOfAdult + numberOfChild + numberOfChild2
                
                for add in self.additionalArr {
                    if add.title == SimplyBook.ADDITIONAL_NUMBER_OF_ADULT {
                        add.values = numberOfAdult.toString()
                    }
                    if add.title == SimplyBook.ADDITIONAL_NUMBER_OF_CHILD {
                        add.values = numberOfChild.toString()
                    }
                    if add.title == SimplyBook.ADDITIONAL_NUMBER_OF_CHILD2 {
                        add.values = numberOfChild2.toString()
                    }
                    if add.title == SimplyBook.ADDITIONAL_COMMENT {
                        add.values = tvComment.text
                    }
                }
                
                APIService.sharedService.booking(resId,
                                                 customerId!,
                                                 cusName,
                                                 cusEmail,
                                                 cusPhone,
                                                 eventId!,
                                                 unitList!,
                                                 time,
                                                 dateStr,
                                                 numberOfSeat,
                                                 self.additionalArr.toJSONString()!)
                { (result, code, mess, rsReservation) in
                    
                    guard result else {
                        SVProgressHUD.dismiss()
                        self.transitionViewStates(loading: false, error: NSError(domain: "error_internet_unavailable_title".localized(), code: 1, userInfo: nil), animated: false, completion: nil)
                        return
                    }
                    
                    guard code != ErrorCode.ERROR_CODE_FAIL else {
                        SVProgressHUD.dismiss()
                        self.showErrorPopup()
                        return
                    }
                    
                    guard code != ErrorCode.ERROR_CODE_NOT_FOUND else {
                        SVProgressHUD.dismiss()
                        self.transitionViewStates(loading: false, error: NSError(domain: "error_unknown".localized(), code: 1, userInfo: nil), animated: false, completion: nil)
                        return
                    }
                    
                    guard code != ErrorCode.SIMPLY_BOOK_FULL_TABLE else {
                        SVProgressHUD.dismiss()
                        self.showErrorPopup()
                        return
                    }
                    
                    self.endLoading()
                    SVProgressHUD.dismiss()
                    self.showConfirmPopup()
                }
                
            } else {
                self.showRequiredLoginPopup()
            }
        } else {
            // show error can't get resId
        }
    }
    
    func requestBooking(){
        // validate for name: accept full name with format "LastName FirstName"
        if let name = txtCusName.text {
            // Returns a new string made by removing from both ends of the String characters contained in a given character set.
            let cs = CharacterSet.init(charactersIn: " ")
            let trim = name.trimmingCharacters(in: cs)
            
            // If trimmed name have contains space characters
            guard trim.characters.contains(" ") else {
                txtCusName.becomeFirstResponder()
                lbName.text = "customer_lb_name_required_full_name".localized()
                lbName.textColor = AppColor.PRIMARY_COLOR
                return
            }
        }
        
        // validate for email
        if let email = txtCusEmail.text {
            
            guard email.characters.count > 0 else {
                txtCusEmail.becomeFirstResponder()
                lbEmail.text = "customer_lb_email_required".localized()
                lbEmail.textColor = AppColor.PRIMARY_COLOR
                return
            }
            
            guard email.isEmail(email: email) else {
                txtCusEmail.becomeFirstResponder()
                lbEmail.text = "customer_lb_email_invalid".localized()
                lbEmail.textColor = AppColor.PRIMARY_COLOR
                return
            }
        }
        
        // validate for phone number: only digits, from 8 to 12
        if let phone = txtCusPhone.text {
            
            guard phone.isPhoneNumber(phoneNumber: phone) else {
                txtCusPhone.becomeFirstResponder()
                lbPhone.text = "customer_lb_phone_invalid".localized()
                lbPhone.textColor = AppColor.PRIMARY_COLOR
                return
            }
        }
        
        if let noa = txtNumberOfAdult.text {
            if var noc = txtNumberOfChild.text {
                if var noc2 = txtNumberOfChild2.text {
                    // check number of adult must be digit
                    guard noa.isDigit(digit: noa) else {
                        txtNumberOfAdult.becomeFirstResponder()
                        lbNumberOfAdult.text = "reservation_lb_noa_required".localized()
                        lbNumberOfAdult.textColor = AppColor.PRIMARY_COLOR
                        return
                    }
                    if noc != "" {
                        // check number of infant must be digit
                        guard noc.isDigit(digit: noc) else {
                            txtNumberOfChild.becomeFirstResponder()
                            lbNumberOfChild.text = "reservation_lb_noc_required".localized()
                            lbNumberOfChild.textColor = AppColor.PRIMARY_COLOR
                            return
                        }
                    }
                    if noc2 != "" {
                        // check number of element school student must be digit
                        guard noc2.isDigit(digit: noc2) else {
                            txtNumberOfChild2.becomeFirstResponder()
                            lbNumberOfChild2.text = "reservation_lb_noc2_required".localized()
                            lbNumberOfChild2.textColor = AppColor.PRIMARY_COLOR
                            return
                        }
                    }
                    // check number of adult must be more than 0
                    guard noa.integer >= 1 else {
                        txtNumberOfAdult.becomeFirstResponder()
                        lbNumberOfAdult.text = "reservation_lb_noa_not_zero".localized()
                        lbNumberOfAdult.textColor = AppColor.PRIMARY_COLOR
                        return
                    }
                    // check min & max
                    guard noa.integer < SimplyBook.MAX_SEAT else {
                        txtNumberOfAdult.becomeFirstResponder()
                        lbNumberOfAdult.text = "reservation_lb_number_of_pax_hint".localized()
                        lbNumberOfAdult.textColor = AppColor.PRIMARY_COLOR
                        return
                    }
                    // check total
                    if noc == "" {
                        noc = "0"
                    }
                    
                    if noc2 == "" {
                        noc2 = "0"
                    }
    
                    let total = noa.integer + noc.integer + noc2.integer
                    guard total < SimplyBook.MAX_SEAT else {
                        lbNumberOfAdult.text = "reservation_lb_the_total_invalid".localized()
                        lbNumberOfAdult.textColor = AppColor.PRIMARY_COLOR
                        return
                    }

                }
            }
        }
        
        guard self.selectedTime != nil else{
            self.showTimeErrorPopup()
            return
        }
    
        booking()
    }
    
    @IBAction func requestBooking(_ sender: Any) {
        self.requestBooking()
    }
    
    func showErrorPopup() {
        // Prepare data for popup
        let title = "common_info".localized()
        let message = "reservation_lb_reserve_time_not_available".localized()
        
        // Create the dialog
        let popup = PopupDialog(
            title: title,
            message: message,
            buttonAlignment: .horizontal,
            transitionStyle: .zoomIn,
            gestureDismissal: true
        ) {}
        
        // Add buttons to dialog
        popup.addButtons([DefaultButton(title: "common_ok".localized()) {
            }])
        
        // Present dialog
        self.present(popup, animated: true, completion: nil)
        
    }
    
    func showTimeErrorPopup(){
        let popup = CommonFunctions.initPopup(title: "common_info".localized(),
                                              message: "reservation_lb_reserve_time_not_available2".localized())
        
        // Add buttons to dialog
        popup.addButtons([DefaultButton(title: "common_ok".localized()) {
            }])
        
        // Present dialog
        self.present(popup, animated: true, completion: nil)
    }
    
    func showRequiredLoginPopup() {
        // Prepare data for popup
        let title = "common_info".localized()
        let message = "other_prize_msg_login_required".localized()
        
        // Create the dialog
        let popup = PopupDialog(
            title: title,
            message: message,
            buttonAlignment: .horizontal,
            transitionStyle: .zoomIn,
            gestureDismissal: true
        ) {}
        
        // Add buttons to dialog
        popup.addButtons([DefaultButton(title: "common_ok".localized()) {
            CurrentCustomerInfo.shareInfo.customer = nil
            let homeStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let vc = homeStoryboard.instantiateViewController(withIdentifier: "Login")
            self.navigationController?.pushViewController(vc, animated: true)
            }])
        
        // Add buttons to dialog
        popup.addButtons([DefaultButton(title: "common_cancel".localized()) {
            }])
        
        // Present dialog
        self.present(popup, animated: true, completion: nil)
        
    }
    
    func showConfirmPopup() {
        if Localize.currentLanguage() == "en" {
            if let confirm_reserve = self.confirmReserve {
                // Prepare data for popup
                let title = "common_info".localized()
                let message = confirm_reserve
                
                // Create the dialog
                let popup = PopupDialog(
                    title: title,
                    message: message,
                    buttonAlignment: .horizontal,
                    transitionStyle: .zoomIn,
                    gestureDismissal: true
                ) {}
                
                // Add buttons to dialog
                popup.addButtons([DefaultButton(title: "common_ok".localized()) {
                    _ = self.navigationController?.popViewController(animated: true)
                    }])
                
                // Present dialog
                self.present(popup, animated: true, completion: nil)
            }
        } else {
            if let confirm_reserve_cn = self.confirmReserveCN {
                // Prepare data for popup
                let title = "common_info".localized()
                let message = confirm_reserve_cn
                
                // Create the dialog
                let popup = PopupDialog(
                    title: title,
                    message: message,
                    buttonAlignment: .horizontal,
                    transitionStyle: .zoomIn,
                    gestureDismissal: true
                ) {}
                
                // Add buttons to dialog
                popup.addButtons([DefaultButton(title: "common_ok".localized()) {
                    _ = self.navigationController?.popViewController(animated: true)
                    }])
                
                // Present dialog
                self.present(popup, animated: true, completion: nil)
            }
        }
    }
    
    @IBAction func showTermsAndConditions(_ sender: UIButton) {
        
        SVProgressHUD.show()
        
        guard let setting = CurrentCustomerInfo.shareInfo.loadSettingInfo() else {
            SVProgressHUD.dismiss()
            self.transitionViewStates(loading: false, error: NSError(domain: "error_server_fail".localized(), code: 1, userInfo: nil), animated: false, completion: nil)
            return
        }
        
        guard let i = setting.index(where: { $0.key == AppSetting.SETTING_BOOKING_TANDC }) else {
            SVProgressHUD.dismiss()
            self.transitionViewStates(loading: false, error: NSError(domain: "error_server_fail".localized(), code: 1, userInfo: nil), animated: false, completion: nil)
            return
        }
        // End loading error page
        self.endLoading(error: nil)
        // Start get value from key
        let settingWithIndex : Setting = setting[i]
            
        if Localize.currentLanguage() == "en" {
            self.tvTermAndCondition.text = settingWithIndex.value
            self.showTermAndCondition()
        } else {
            self.tvTermAndCondition.text = settingWithIndex.value_cn
            self.showTermAndCondition()
        }
        
        SVProgressHUD.dismiss()
    }
    
    func showTermAndCondition() {
        UIView.animate(withDuration: 1.0) {
            self.viewTandC.isHidden = false
            self.viewBackground.backgroundColor = UIColor.init(red: 0, green: 0, blue: 0, alpha: 0.6)
        }
    }
    
    func closeTermAndCondition(){
        if !viewTandC.isHidden {
            UIView.animate(withDuration: 0.5) {
                self.viewBackground.backgroundColor = AppColor.BACKGROUND_COLOR
                self.viewTandC.isHidden = true
            }
        }
    }
    
}

extension SimplyBookViewController : UIPickerViewDelegate, UIPickerViewDataSource {
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if pickerView == pvShopInfo {
            return shopInfoArr.count
        } else if pickerView == pvDurations {
            return eventArr.count
        } else if pickerView == pvTableType {
            return unitArr.count
        } else if pickerView == pvReserveDate {
            return reserveDateArr.count
        } else {
            return reserveTimeArr.count
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        
        if pickerView == pvShopInfo {
            if Localize.currentLanguage() == "en" {
                return shopInfoArr[row].name
            } else {
                return shopInfoArr[row].name_cn
            }
        } else if pickerView == pvDurations {
            return eventArr[row].name
        } else if pickerView == pvTableType {
            return unitArr[row].name
        } else if pickerView == pvReserveDate {
            return reserveDateArr[row]
        } else {
            return reserveTimeArr[row]
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if pickerView == pvShopInfo {
            if shopInfoArr[row].id != self.selectedShopInfoId {
                self.selectedShopInfoId = shopInfoArr[row].id
                getBookingInfoAPI()
            }
        }
        if pickerView == pvDurations {
            if eventArr[row].id?.integer != self.selectedEventId {
                self.selectedEventId = eventArr[row].id?.integer
            }
        }
        
        if pickerView == pvTableType {
            if unitArr[row].id?.integer != self.selectedUnitId {
                self.selectedUnitId = unitArr[row].id?.integer
            }
        }
        if pickerView == pvReserveDate {
            if reserveDateArr[row] != self.selectedDate {
                self.selectedDate = reserveDateArr[row] // string
                self.selectedDays = reserveDaysArr[row] // date
                dateFormatter.dateFormat = "YYYY-MM-dd"
                let dateNowStr = dateFormatter.string(from: self.selectedDays!)
                self.loadReserveTime(date: dateNowStr)
                
            }
        }
        if pickerView == pvReserveTime {
            if reserveTimeArr[row] != self.selectedDate {
                self.selectedTime = reserveTimeArr[row]
            }
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, viewForRow row: Int, forComponent component: Int, reusing view: UIView?) -> UIView {
        var label = view as! UILabel!
        
        if label == nil {
            label = UILabel()
        }
        
        if pickerView == pvShopInfo {
            if Localize.currentLanguage() == "en" {
                let data = shopInfoArr[row].name
                let title = NSAttributedString(string: data!, attributes: [NSFontAttributeName: UIFont.systemFont(ofSize: 18.0, weight: UIFontWeightRegular)])
                label?.attributedText = title
                label?.textAlignment = .center
                
                return label!
            } else {
                let data = shopInfoArr[row].name_cn
                let title = NSAttributedString(string: data!, attributes: [NSFontAttributeName: UIFont.systemFont(ofSize: 18.0, weight: UIFontWeightRegular)])
                label?.attributedText = title
                label?.textAlignment = .center
                
                return label!
            }
        } else if pickerView == pvDurations {
            let data = eventArr[row].name
            let title = NSAttributedString(string: data!, attributes: [NSFontAttributeName: UIFont.systemFont(ofSize: 18.0, weight: UIFontWeightRegular)])
            label?.attributedText = title
            label?.textAlignment = .center
            
            return label!
        } else if pickerView == pvTableType {
            let data = unitArr[row].name
            let title = NSAttributedString(string: data!, attributes: [NSFontAttributeName: UIFont.systemFont(ofSize: 18.0, weight: UIFontWeightRegular)])
            label?.attributedText = title
            label?.textAlignment = .center
            
            return label!
        } else if pickerView == pvReserveDate {
            let data = reserveDateArr[row]
            let title = NSAttributedString(string: data, attributes: [NSFontAttributeName: UIFont.systemFont(ofSize: 18.0, weight: UIFontWeightRegular)])
            label?.attributedText = title
            label?.textAlignment = .center
            
            return label!
        } else {
            let data = reserveTimeArr[row]
            let title = NSAttributedString(string: data, attributes: [NSFontAttributeName: UIFont.systemFont(ofSize: 18.0, weight: UIFontWeightRegular)])
            label?.attributedText = title
            label?.textAlignment = .center
            
            return label!
        }
    }
}

extension UILabel {
    func setRequiredAttribute(_ string: String){
        let range = (string as NSString).range(of: "*")
        let attributedString = NSMutableAttributedString(string: string)
        attributedString.addAttribute(NSForegroundColorAttributeName, value: AppColor.PRIMARY_COLOR , range: range)
        self.attributedText = attributedString
    }
    func setRequiredAttributeHashtag(_ string: String){
        let attrStr = NSMutableAttributedString(string: string)
        let searchPattern = "#\\w+"
        var ranges: [NSRange] = [NSRange]()
        
        let regex = try! NSRegularExpression(pattern: searchPattern, options: [])
        ranges = regex.matches(in: attrStr.string, options: [], range: NSMakeRange(0, attrStr.string.characters.count)).map {$0.range}
        
        for range in ranges {
            attrStr.addAttribute(NSForegroundColorAttributeName, value: AppColor.PRIMARY_COLOR, range: NSRange(location: range.location, length: range.length))
        }
        
        self.attributedText = attrStr
    }
}
