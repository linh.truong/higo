//
//  HomeDetailViewController.swift
//  Toyosu
//
//  Created by Linh Trương on 10/11/16.
//  Copyright © 2016 IBS. All rights reserved.
//

import UIKit
import AFNetworking
import Localize_Swift
import StatefulViewController

class HomeDetailViewController: UIViewController, StatefulViewController {
    
    static let segueID = "ShowNewsDetail"
    
    var topic: Topic?
    var url: URL? = nil
    let dateFormatter = DateFormatter()
    
    var isLoadingRelated: Bool = true
    var relatedList: [CustomRelatedItems] = []
    
    @IBOutlet weak var tblNewsDetail: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        homeDetailInit()
        
        // Reload localized after change
        NotificationCenter.default.addObserver(self, selector: #selector(HomeDetailViewController.refreshLanguage), name: NSNotification.Name(LCLLanguageChangeNotification), object: nil)
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    func homeDetailInit() {
        tblNewsDetail.delegate = self
        tblNewsDetail.dataSource = self
        edgesForExtendedLayout = []
        dateFormatter.dateStyle = .long
        loadRelatedItemsData()
        setupInitialViewState()
        setupErrorViewState()
        backNavLocalized()
    }
    
    func backNavLocalized() {
        let backItem = UIBarButtonItem()
        backItem.title = "common_back".localized()
        navigationItem.backBarButtonItem = backItem
    }
    
    func setupErrorViewState() {
        let failureView = ErrorView(frame: view.frame)
        failureView.tapGestureRecognizer.addTarget(self, action: #selector(HomeDetailViewController.loadRelatedItemsData))
        errorView = failureView
    }
    
    func refreshLanguage () {
        tblNewsDetail.reloadData()
        let vc = RightBarViewController()
        vc.setBarButtonItem()
        backNavLocalized()
    }
    
    func loadRelatedItemsData() {
        if let tagList = topic?.hash_tags {
            let resId = topic?.restaurant_id ?? Constant.DEFAULT_SHOP_INFO_ID
            let id = topic?.id ?? 0
            let objectType = 3
            let customer = CurrentCustomerInfo.shareInfo.loadCustomerInfo()
            APIService.sharedService.getRelatedItemsList(resId, customer?.id, id, objectType, tagList, Constant.RELATED_ITEMS_LIMIT, completed: { [weak self] (result, code, message, rsRelatedItems) in
                guard let s = self else{
                    return
                }
                
                guard result else {
                    s.isLoadingRelated = false
                    self?.transitionViewStates(loading: false, error: NSError(domain: "error_internet_unavailable_title".localized(), code: 1, userInfo: nil), animated: false, completion: nil)
                    return
                }
                
                self?.endLoading(error: nil)
                if let pr = rsRelatedItems {
                    s.relatedList.removeAll()
                    s.relatedList.append(contentsOf: pr)
                    
                }
                s.isLoadingRelated = false
                s.tblNewsDetail.reloadRows(at: [IndexPath.init(row: 1, section: 0)], with: .fade)
            })
        }
    }
    
    func showRelatedItem(_ relatedItems: CustomRelatedItems) {
        if let tabVC = navigationController?.tabBarController as? TabBarViewController {
            tabVC.showRelatedItem(relatedItems)
        }
    }
}

extension HomeDetailViewController: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        switch indexPath.row {
        case 0:
            let cell = tableView.dequeueReusableCell(withIdentifier: HomeDetailTableViewCell.cellID, for: indexPath) as! HomeDetailTableViewCell
            
            if let tp = topic {
                cell.calculateImageSize(tp)
            }
            
            if Localize.currentLanguage() == "en" {
                if let res_name = topic?.res_name {
                    cell.resName.text = res_name
                }
            } else {
                if let res_name_cn = topic?.res_name_cn {
                    cell.resName.text = res_name_cn
                }
            }
            
            if let str = topic?.image_path {
                if let u = URL.init(string: str) {
                    url = u
                    if let fUrl = url {
                        cell.imgPath.setImageWith(fUrl, placeholderImage: Constant.NIL_IMAGE)
                    } else {
                        cell.imgPath.image = UIImage.init(named: "no_img")
                    }
                }
            }
            
            if Localize.currentLanguage() == "en" {
                if let subject = topic?.subject {
                    cell.lbSubject.text = subject
                }
                if let body = topic?.body {
                    cell.lbBody.text = body
                }
            } else {
                if let subject_cn = topic?.subject_cn {
                    cell.lbSubject.text = subject_cn
                }
                if let body_cn = topic?.body_cn {
                    cell.lbBody.text = body_cn
                }
            }
            
            if let hashtag = topic?.hash_tags {
                cell.lbHashtags.text = hashtag
            }
            
            if let openDate = topic?.open_date {
                cell.lbOpenDate.text = String(format: NSLocalizedString("home_lb_open_date".localized(), comment: ""), "\(dateFormatter.string(from: openDate))")
            }
            
            return cell
        case 1:
            let cell = tableView.dequeueReusableCell(withIdentifier: RelatedItemsTableViewCell.cellID, for: indexPath) as! RelatedItemsTableViewCell
            if isLoadingRelated {
                cell.showIndicator()
            }else {
                if cell.allItems == nil {
                    cell.allItems = self.relatedList
                    cell.finishRefreshing()
                }
                
                if cell.selectRelatedCallback == nil {
                    cell.selectRelatedCallback = { [weak self] (item, cell) in
                        self?.showRelatedItem(item)
                    }
                }
            }
            
            return cell
        default:
            return UITableViewCell()
        }
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch indexPath.row {
        case 0:
            tableView.estimatedRowHeight = 200.0
            tableView.rowHeight = UITableViewAutomaticDimension
            return tableView.rowHeight
        case 1:
            return 250.0
        default:
            return 0
        }
    }
    
}

