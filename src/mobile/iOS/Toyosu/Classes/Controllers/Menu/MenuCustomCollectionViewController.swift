//
//  MenuCustomCollectionViewController.swift
//  Toyosu
//
//  Created by Linh Trương on 11/25/16.
//  Copyright © 2016 IBS. All rights reserved.
//

import UIKit
import SVProgressHUD
import Localize_Swift
import StatefulViewController

private let PAGINATION_LIMIT : Int = 6

class MenuCustomCollectionViewController: UIViewController, CAPSPageMenuDelegate, StatefulViewController {
    
    // Define variable
    var foodCate : FoodCategories?
    var allFoods : [Food] = []
    var willViewDetailFood : Food?
    var refreshControl = UIRefreshControl()
    var pageMenu : CAPSPageMenu?
    var totalFoods : Int = 0
    var currentPage : Int = 0
    var isLoadingMore = false
    var isBackMenu = true
    
    // Get outlet collection view
    @IBOutlet weak var cvFood: UICollectionView!
    
    // Variable menu layout
    let leftAndRightPaddings: CGFloat = 20.0
    let numberOfItemsPerRow: CGFloat = 2.0
    let heigthAdjustment: CGFloat = 60.0
    let sectionInsetTop: CGFloat = 8.0
    let sectionInsetLeft: CGFloat = 8.0
    let sectionInsetBottom: CGFloat = 4.0
    let sectionInsertRight: CGFloat = 8.0
    let minSpacingForCell: CGFloat = 4.0
    let minSpacingForLine: CGFloat = 4.0
    
    // Set menu category layout
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        cvFood.collectionViewLayout.invalidateLayout()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Set collection view
        setMenuCollectionViewInit()
        
        // Refresh food list
        refreshControl.addTarget(self, action: #selector(MenuCustomCollectionViewController.refresh(_:)), for: UIControlEvents.valueChanged)
        cvFood.addSubview(refreshControl)
        
        // Reload language localized after change
        NotificationCenter.default.addObserver(self, selector: #selector(MenuCustomCollectionViewController.refreshLanguage), name: NSNotification.Name(LCLLanguageChangeNotification), object: nil)
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    func setupErrorViewState(){
        let failureView = ErrorView(frame: view.frame)
        failureView.tapGestureRecognizer.addTarget(self, action: #selector(MenuCustomCollectionViewController.refreshWithIndi))
        errorView = failureView
    }
    
    func setupEmptyViewState() {
        let emptyView = EmptyView(frame: view.frame)
        errorView = emptyView
    }
    
    func setMenuCollectionViewInit() {
        cvFood.dataSource = self
        cvFood.delegate = self
        pageMenu?.delegate = self
        isBackMenu = false
        setupInitialViewState()
        setupErrorViewState()
        setupEmptyViewState()
        refreshWithIndi()
    }
    
    func refreshLanguage () {
        // Set navigation title localized
        navigationItem.title = "menu_lb_title".localized()
        // Reload data table view after changed localized
        cvFood.reloadData()
    }
    
    func refresh(_ sender:AnyObject) {
        loadFoodWithPage(0)
    }
    
    func refreshWithIndi() {
        SVProgressHUD.show()
        loadFoodWithPage(0)
    }
    
    func finishRefreshing () {
        SVProgressHUD.dismiss()
        cvFood.reloadData()
        refreshControl.endRefreshing()
        isLoadingMore = false
    }
    
    func loadFoodWithPage(_ page : Int) {
        
        if let catId = foodCate?.id {
            currentPage = page
            APIService.sharedService.getFoodsByCateId([catId], PAGINATION_LIMIT, currentPage * PAGINATION_LIMIT, completed: { [weak self] (result, code, message, rsTotal, rsFoods) in
                // load error view
                guard rsTotal > 0 else {
                    SVProgressHUD.dismiss()
                    self?.transitionViewStates(loading: false, error: NSError(domain: "error_no_data".localized(), code: 1, userInfo: nil), animated: false, completion: nil)
                    return
                }
                guard result else {
                    SVProgressHUD.dismiss()
                    self?.transitionViewStates(loading: false, error: NSError(domain: "error_internet_unavailable_title".localized(), code: 1, userInfo: nil), animated: false, completion: nil)
                    return
                }
                
                guard code != ErrorCode.ERROR_CODE_FAIL else {
                    SVProgressHUD.dismiss()
                    self?.transitionViewStates(loading: false, error: NSError(domain: "error_server_fail".localized(), code: 1, userInfo: nil), animated: false, completion: nil)
                    return
                }
                
                guard code != ErrorCode.ERROR_CODE_NOT_FOUND else {
                    SVProgressHUD.dismiss()
                    self?.transitionViewStates(loading: false, error: NSError(domain: "error_unknown".localized(), code: 1, userInfo: nil), animated: false, completion: nil)
                    return
                }
                
                // load food data
                self?.endLoading(error: nil)
                self?.finishCallApiWithFoods(rsFoods?["\(catId)"] , rsTotal)
            })
        }
    }
    
    func loadMoreFood() {
        
        if totalFoods > allFoods.count {
            self.endLoading(error: nil)
            isLoadingMore = true
            loadFoodWithPage(currentPage + 1)
        } else {
            SVProgressHUD.dismiss()
            self.transitionViewStates(loading: false, error: NSError(domain: "error_internet_unavailable_title".localized(), code: 1, userInfo: nil), animated: false, completion: nil)
        }
        
    }
    
    func finishCallApiWithFoods(_ foodList: [Food]?, _ total: Int) {
        
        guard let foods = foodList else {
            self.transitionViewStates(loading: false, error: NSError(domain: "error_server_fail".localized(), code: 1, userInfo: nil), animated: false, completion: nil)
            finishRefreshing()
            return
        }
        
        self.endLoading(error: nil)
        
        if (isLoadingMore) {
            allFoods.append(contentsOf: foods)
        } else {
            allFoods = foods
        }
        
        totalFoods = total
        finishRefreshing()
    }
}

extension MenuCustomCollectionViewController : UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if allFoods.count < totalFoods {
            return allFoods.count + 1
        } else {
            return allFoods.count
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let rowAtIndexPath = indexPath.row
        
        if rowAtIndexPath == allFoods.count {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: LoadMoreCell.cellID, for: indexPath) as! LoadMoreCell
            
            return cell
        } else {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: MenuCollectionViewCell.cellID, for: indexPath) as! MenuCollectionViewCell
            cell.configureCellWith(allFoods[rowAtIndexPath])
            
            return cell
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        
        if (indexPath.row == allFoods.count) {
            loadMoreFood()
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let rowAtIndexPath = indexPath.row
        
        if rowAtIndexPath == allFoods.count {
            return LoadMoreCell.cellSize
        } else {
            let width = (cvFood.frame.width - leftAndRightPaddings) / numberOfItemsPerRow
            let heigth = width + heigthAdjustment

            return CGSize(width: width, height: heigth)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        
        return minSpacingForCell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        
        return minSpacingForLine
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        
        return UIEdgeInsetsMake(sectionInsetTop, sectionInsetLeft, sectionInsetBottom, sectionInsertRight)
    }
    
    // MARK - Pass to food detail
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        if indexPath.row < allFoods.count {
            let storyboard = UIStoryboard(name: "Menu", bundle: nil)
            let controllerDetail = storyboard.instantiateViewController(withIdentifier: "MenuDetailViewController") as! MenuDetailViewController
            willViewDetailFood = allFoods[indexPath.row]
            controllerDetail.food = willViewDetailFood
            self.show(controllerDetail, sender: nil)
            isBackMenu = true
        }
        
    }
}
