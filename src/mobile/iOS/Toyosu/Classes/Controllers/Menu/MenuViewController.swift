//
//  MenuViewController.swift
//  Toyosu
//
//  Created by Linh Trương on 11/28/16.
//  Copyright © 2016 IBS. All rights reserved.
//

import UIKit
import SVProgressHUD
import Localize_Swift
import StatefulViewController

class MenuViewController: UIViewController, StatefulViewController {
    
    // Define variable
    var foodCates = [FoodCategories]()
    var refreshControl = UIRefreshControl()
    var pageMenu : CAPSPageMenu?
    var controllerArray : [MenuCustomCollectionViewController] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
    
        // init menu
        setMenuInit()
        
        // Function show food category
        getFoodCates()
        
        // Reload localized after change
        NotificationCenter.default.addObserver(self, selector: #selector(MenuViewController.refreshLanguage), name: NSNotification.Name(LCLLanguageChangeNotification), object: nil)
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    func setupErrorViewState() {
        let failureView = ErrorView(frame: view.frame)
        failureView.tapGestureRecognizer.addTarget(self, action: #selector(MenuViewController.getFoodCates))
        errorView = failureView
    }
    
    func setMenuInit() {
        edgesForExtendedLayout = UIRectEdge.init(rawValue: 0)
        
        navigationItem.title = "menu_lb_title".localized()
        navigationController?.tabBarItem.title = "menu_lb_title".localized()
        
        setupInitialViewState()
        setupErrorViewState()
    }

    
    func refreshLanguage () {
        // Set navigation title localized
        navigationItem.title = "menu_lb_title".localized()
        // Reload menu after changed localized
        showFoodCates(self.foodCates)
    }
    
    func getFoodCates() {
        
        APIService.sharedService.getFoodCategoryList { [weak self] (resut, code, message, rsFoodCates) in
            
            guard resut else {
                SVProgressHUD.dismiss()
                self?.transitionViewStates(loading: false, error: NSError(domain: "error_internet_unavailable_title".localized(), code: 1, userInfo: nil), animated: false, completion: nil)
                return
            }
            
            guard code != ErrorCode.ERROR_CODE_FAIL else {
                SVProgressHUD.dismiss()
                self?.transitionViewStates(loading: false, error: NSError(domain: "error_server_fail".localized(), code: 1, userInfo: nil), animated: false, completion: nil)
                return
            }
            
            guard code != ErrorCode.ERROR_CODE_NOT_FOUND else {
                SVProgressHUD.dismiss()
                self?.transitionViewStates(loading: false, error: NSError(domain: "error_unknown".localized(), code: 1, userInfo: nil), animated: false, completion: nil)
                return
            }
            
            self?.endLoading(error: nil)
            
            if let cates = rsFoodCates {
                self?.foodCates.removeAll()
                self?.foodCates.append(contentsOf: cates)
                self?.showFoodCates(cates)
                SVProgressHUD.dismiss()
            } else {
                SVProgressHUD.dismiss()
                self?.transitionViewStates(loading: false, error: NSError(domain: "error_server_fail".localized(), code: 1, userInfo: nil), animated: false, completion: nil)
            }
        }
    }
    
    func showFoodCates(_ foodCates : [FoodCategories]) {
        
        if let page = pageMenu {
            page.view.removeFromSuperview()
            page.removeFromParentViewController()
            pageMenu = nil
        }
        
        controllerArray.removeAll() //remove
        
        let mainStoryboard: UIStoryboard = UIStoryboard(name: "Menu",bundle: nil)
        
        for cate in foodCates {
            let controller = mainStoryboard.instantiateViewController(withIdentifier: "MenuCustomCollectionViewController") as! MenuCustomCollectionViewController
            
            if Localize.currentLanguage() == "en" {
                controller.title = cate.name
            } else {
                controller.title = cate.name_cn
            }
            controller.foodCate = cate
            controllerArray.append(controller)
        }
        
        let parameters: [CAPSPageMenuOption] = [
            //color
            .viewBackgroundColor(UIColor.white),
            .scrollMenuBackgroundColor(UIColor(red: 154.0/255.0, green: 26.0/255.0, blue: 64.0/255.0, alpha: 1.0)),
            .selectedMenuItemLabelColor(UIColor.white),
            .unselectedMenuItemLabelColor(UIColor.lightGray),
            //.unselectedMenuItemLabelColor(UIColor(red: 163.0/255.0, green: 112.0/255.0, blue: 127.0/255.0, alpha: 1.0)),
            .selectionIndicatorColor(UIColor.white),
            //item font
            .menuItemFont(UIFont(name: "HelveticaNeue", size: 15)!),
            //width and heigh
            .menuHeight(44.0),
            .selectionIndicatorHeight(3.0),
            //margin and align
            .menuMargin(4.0),
            .menuItemWidthBasedOnTitleTextWidth(false)
        ]
        
        // Initialize scroll menu
        let rect = CGRect(x: 0.0, y: 0.0, width: self.view.frame.width, height: self.view.frame.height)
        pageMenu = CAPSPageMenu(viewControllers: controllerArray, frame: rect, pageMenuOptions: parameters)

        self.addChildViewController(pageMenu!)
        self.view.addSubview(pageMenu!.view)
        
        pageMenu!.didMove(toParentViewController: self)
    }
}
