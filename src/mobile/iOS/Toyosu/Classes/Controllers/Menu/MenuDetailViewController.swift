//
//  MenuDetailViewController.swift
//  Toyosu
//
//  Created by Linh Trương on 10/20/16.
//  Copyright © 2016 IBS. All rights reserved.
//

import UIKit
import AFNetworking
import Localize_Swift
import StatefulViewController

class MenuDetailViewController: UIViewController, StatefulViewController {

    static let segueID = "ShowFoodDetail"
    
    var food: Food?
    var url: URL? = nil
    
    var isLoadingRelated: Bool = true
    var relatedList: [CustomRelatedItems] = []
    
    @IBOutlet weak var tblDetail: UITableView!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        menuDetailInit()
        
        // Reload localized after change
        NotificationCenter.default.addObserver(self, selector: #selector(MenuDetailViewController.refreshLanguage), name: NSNotification.Name(LCLLanguageChangeNotification), object: nil)
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    func menuDetailInit() {
        tblDetail.delegate = self
        tblDetail.dataSource = self
        edgesForExtendedLayout = []
        loadRelatedItemsData()
        setupInitialViewState()
        setupErrorViewState()
        backNavLocalized()
    }
    
    func backNavLocalized() {
        let backItem = UIBarButtonItem()
        backItem.title = "common_back".localized()
        navigationItem.backBarButtonItem = backItem
    }
    
    func setupErrorViewState() {
        let failureView = ErrorView(frame: view.frame)
        failureView.tapGestureRecognizer.addTarget(self, action: #selector(MenuDetailViewController.loadRelatedItemsData))
        errorView = failureView
    }
    
    func refreshLanguage () {
        tblDetail.reloadData()
        backNavLocalized()
    }

    func loadRelatedItemsData() {
        if let tagList = food?.hash_tags {
            let resId = food?.restaurant_id ?? Constant.DEFAULT_SHOP_INFO_ID
            let id = food?.id ?? 0
            let objectType = 2
            let customer = CurrentCustomerInfo.shareInfo.loadCustomerInfo()
            APIService.sharedService.getRelatedItemsList(resId, customer?.id, id, objectType, tagList, Constant.RELATED_ITEMS_LIMIT, completed: { [weak self] (result, code, message, rsRelatedItems) in
                guard let s = self else{
                    return
                }
                
                guard result else {
                    s.isLoadingRelated = false
                    self?.transitionViewStates(loading: false, error: NSError(domain: "error_internet_unavailable_title".localized(), code: 1, userInfo: nil), animated: false, completion: nil)
                    return
                }
                
                self?.endLoading(error: nil)
                if let pr = rsRelatedItems {
                    s.relatedList.removeAll()
                    s.relatedList.append(contentsOf: pr)
                    
                }
                s.isLoadingRelated = false
                s.tblDetail.reloadRows(at: [IndexPath.init(row: 1, section: 0)], with: .fade)
            })
        }
    }
    
    func showRelatedItem(_ relatedItems: CustomRelatedItems) {
        if let tabVC = navigationController?.tabBarController as? TabBarViewController {
            tabVC.showRelatedItem(relatedItems)
        }
    }
}

extension MenuDetailViewController: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return 2
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        switch indexPath.row {
        case 0:
            let cell = tableView.dequeueReusableCell(withIdentifier: MenuDetailTableViewCell.cellID, for: indexPath) as! MenuDetailTableViewCell
            
            if let fd = food {
                cell.calculateImageSize(fd)
            }
            
            if let str = food?.image_path {
                if let u = URL.init(string: str) {
                    url = u
                    if let fUrl = url {
                        cell.imgPath.setImageWith(fUrl, placeholderImage: Constant.NIL_IMAGE)
                    } else {
                        cell.imgPath.image = UIImage.init(named: "no_img")
                    }
                }
            }
            
            if let fd = food {
               cell.calculateImageSize(fd)
            }
            
            if Localize.currentLanguage() == "en" {
                cell.lbName.text = food?.name
            } else {
                cell.lbName.text = food?.name_cn
            }
            
            if let price = food?.price {
                let priceSplit = price.components(separatedBy: ".")
                let priceRound = priceSplit[0]
                cell.lbPrice.text = Constant.CURRENCY_DOLLAR + priceRound
            }
            
            if Localize.currentLanguage() == "en" {
                if let des = food?.information {
                    cell.lbDescription.text = des
                }
            } else {
                if let des_cn = food?.information_cn {
                    cell.lbDescription.text = des_cn
                }
            }
            
            return cell
        case 1:
            let cell = tableView.dequeueReusableCell(withIdentifier: RelatedItemsTableViewCell.cellID, for: indexPath) as! RelatedItemsTableViewCell
            if isLoadingRelated {
                cell.showIndicator()
            }else {
                if cell.allItems == nil {
                    cell.allItems = self.relatedList
                    cell.finishRefreshing()
                }
                
                if cell.selectRelatedCallback == nil {
                    cell.selectRelatedCallback = { [weak self] (item, cell) in
                        self?.showRelatedItem(item)
                    }
                }
            }
            
            return cell
        default:
            return UITableViewCell()
        }
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        switch indexPath.row {
        case 0:
            tableView.estimatedRowHeight = 200.0
            tableView.rowHeight = UITableViewAutomaticDimension
            return tableView.rowHeight
        case 1:
            return 250.0
        default:
            return 550.0
        }
    }
    
}
