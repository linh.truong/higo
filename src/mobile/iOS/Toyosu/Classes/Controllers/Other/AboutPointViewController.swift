//
//  AboutPointViewController.swift
//  Roann
//
//  Created by Linh Trương on 3/29/17.
//  Copyright © 2017 IBS. All rights reserved.
//

import Foundation
import UIKit
import SVProgressHUD
import Localize_Swift
import StatefulViewController

class AboutPointViewController: BaseViewController, StatefulViewController {

    @IBOutlet weak var textView: UITextView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        screenInit()
        
        NotificationCenter.default.addObserver(self, selector: #selector(AboutPointViewController.refreshLanguage), name: NSNotification.Name(LCLLanguageChangeNotification), object: nil)
    }
    
    func screenInit(){
        self.title = "other_lb_about_point".localized()
        setupInitialViewState()
        setupErrorViewState()
        loadData()
    }
    
    func setupErrorViewState() {
        let failureView = ErrorView(frame: view.frame)
        failureView.tapGestureRecognizer.addTarget(self, action: #selector(AboutPointViewController.loadData))
        errorView = failureView
    }
    
    func loadData(){
        
        SVProgressHUD.show()
        
        guard let setting = CurrentCustomerInfo.shareInfo.loadSettingInfo() else {
            SVProgressHUD.dismiss()
            self.transitionViewStates(loading: false, error: NSError(domain: "error_server_fail".localized(), code: 1, userInfo: nil), animated: false, completion: nil)
            return
        }
        
        guard let i = setting.index(where: { $0.key == AppSetting.SETTING_ABOUT_POINT }) else {
            SVProgressHUD.dismiss()
            self.transitionViewStates(loading: false, error: NSError(domain: "error_server_fail".localized(), code: 1, userInfo: nil), animated: false, completion: nil)
            return
        }
        // End loading error page
        self.endLoading(error: nil)
        // Start get value from key
        let settingWithIndex : Setting = setting[i]
        // Set value to text view
        if Localize.currentLanguage() == "en" {
            self.textView.text = settingWithIndex.value
        } else {
            self.textView.text = settingWithIndex.value_cn
        }
        
        SVProgressHUD.dismiss()
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    func refreshLanguage() {
        self.title = "other_lb_about_point".localized()
    }
    
}
