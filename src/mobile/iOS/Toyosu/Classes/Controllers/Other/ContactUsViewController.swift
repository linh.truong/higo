//
//  ContactUsViewController.swift
//  Toyosu
//
//  Created by Linh Trương on 1/25/17.
//  Copyright © 2017 IBS. All rights reserved.
//

import UIKit
import SVProgressHUD
import Localize_Swift
import StatefulViewController
import PopupDialog

class ContactUsViewController: UIViewController, UITextFieldDelegate, UITextViewDelegate, StatefulViewController {

    var dateFormatter = DateFormatter()
    var contact: Contact?
    
    @IBOutlet weak var lbNameStatus: UILabel!
    @IBOutlet weak var txtName: UITextField!
    @IBOutlet weak var lbEmailStatus: UILabel!
    @IBOutlet weak var txtEmail: UITextField!
    @IBOutlet weak var lbPhoneStatus: UILabel!
    @IBOutlet weak var txtPhone: UITextField!
    @IBOutlet weak var lbMessageStatus: UILabel!
    @IBOutlet weak var tvMessage: UITextView!
    @IBOutlet weak var lbPrivacyPolicy: UILabel!
    @IBOutlet weak var tvPrivacyPolicy: UITextView!
    @IBOutlet weak var btnCheckBox: CheckBox!
    @IBOutlet weak var lbAgree: UILabel!
    @IBOutlet weak var btnSubmit: UIButton!
    @IBOutlet weak var scrollView: UIScrollView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        screenInit()
        
        // refresh language ( localized )
        NotificationCenter.default.addObserver(self, selector: #selector(ContactUsViewController.refreshLanguage), name: NSNotification.Name(LCLLanguageChangeNotification), object: nil)
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    func screenInit() {
        btnCheckBox.addTarget(self, action: #selector(self.policyAgreed) , for: UIControlEvents.touchUpInside)
        setupInitialViewState()
        setupErrorViewState()
        loadPrivacyPolicyInfo()
        setScreenLayout()
        setLangLocalized()
        setDelegate()
        dismissKeyboardWhenTapped()
        addDoneButtonOnKeyboard()
        changeKeyboardLayout()
        loadCustomerData()
    }
    
    func setDelegate() {
        self.txtName.delegate = self
        self.txtEmail.delegate = self
        self.txtPhone.delegate = self
        self.tvMessage.delegate = self
        self.tvPrivacyPolicy.delegate = self
    }
    
    func policyAgreed() {
        
        if(btnCheckBox.isChecked == true )
        {
            validButton(isValidated: true)
        }else{
            validButton(isValidated: false)
        }
    }
    
    func validButton(isValidated: DarwinBoolean){
        
        if isValidated.boolValue == true
        {
            btnSubmit.backgroundColor = AppColor.PRIMARY_COLOR
            btnSubmit.isUserInteractionEnabled = true
        }else{
            btnSubmit.backgroundColor = UIColor.lightGray
            btnSubmit.isUserInteractionEnabled = false
        }
        
    }
    
    func setupErrorViewState() {
        let failureView = ErrorView(frame: view.frame)
        failureView.tapGestureRecognizer.addTarget(self, action: #selector(ContactUsViewController.refreshLanguage))
        errorView = failureView
    }
    
    func refreshLanguage() {
        screenInit()
    }
    
    func setScreenLayout() {
        lbNameStatus.textColor = AppColor.DARK_GRAY_COLOR
        lbEmailStatus.textColor = AppColor.DARK_GRAY_COLOR
        lbPhoneStatus.textColor = AppColor.DARK_GRAY_COLOR
        lbMessageStatus.textColor = AppColor.DARK_GRAY_COLOR
        lbPrivacyPolicy.textColor = AppColor.DARK_GRAY_COLOR
        lbAgree.textColor = AppColor.DARK_GRAY_COLOR
        btnSubmit.layer.cornerRadius = 5
        btnSubmit.backgroundColor = UIColor.lightGray
        btnSubmit.isUserInteractionEnabled = false
    }
    
    func setLangLocalized() {
        self.title = "other_lb_contact_us".localized()
        lbNameStatus.text = "customer_lb_name".localized()
        lbPhoneStatus.text = "customer_lb_phone".localized()
        lbEmailStatus.text = "customer_lb_email".localized()
        lbMessageStatus.text = "other_contact_us_content".localized()
        lbPrivacyPolicy.text = "other_lb_privacy_policy".localized()
        lbAgree.text = "customer_contac_us_policy_agree".localized()
        btnSubmit.setTitle("other_contact_us_btn_submit".localized(), for: .normal)
    }
    
    override func viewDidLayoutSubviews() {
        self.tvPrivacyPolicy.setContentOffset(CGPoint.zero, animated: false)
    }
    
    func changeKeyboardLayout(){
        txtName.returnKeyType = UIReturnKeyType.next
        txtEmail.returnKeyType = UIReturnKeyType.next
        txtPhone.returnKeyType = UIReturnKeyType.next
        tvMessage.returnKeyType = UIReturnKeyType.next
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        if textField == txtName {
            txtEmail.becomeFirstResponder()
        }
        
        if textField == txtEmail {
            txtPhone.becomeFirstResponder()
        }
        
        if textField == txtPhone {
            txtPhone.becomeFirstResponder()
        }
        
        return false
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        
        if textField == txtName {
            lbNameStatus.text = "customer_lb_name".localized()
            lbNameStatus.textColor = AppColor.DARK_GRAY_COLOR
        }
        
        if textField == txtEmail {
            lbEmailStatus.text = "customer_lb_email".localized()
            lbEmailStatus.textColor = AppColor.DARK_GRAY_COLOR
        }
        
        if textField == txtPhone {
            lbPhoneStatus.text = "customer_lb_phone".localized()
            lbPhoneStatus.textColor = AppColor.DARK_GRAY_COLOR
        }
    }
    
    func textViewShouldBeginEditing(_ textView: UITextView) -> Bool {
        
        if textView == tvPrivacyPolicy {
            return false
        }
        
        return true
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView == tvMessage {
            lbMessageStatus.text = "other_contact_us_content".localized()
            lbMessageStatus.textColor = AppColor.DARK_GRAY_COLOR
        }
    }
    
    func addDoneButtonOnKeyboard() {
        let doneToolbar: UIToolbar = UIToolbar(frame: CGRect(x: 0, y: 0, width: 320, height: 50))
        doneToolbar.barStyle       = UIBarStyle.default
        let flexSpace              = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        let done: UIBarButtonItem  = UIBarButtonItem(title: "common_done".localized(), style: UIBarButtonItemStyle.done, target: self, action: #selector(ContactUsViewController.doneButtonAction))
        
        var items = [UIBarButtonItem]()
        items.append(flexSpace)
        items.append(done)
        
        doneToolbar.items = items
        doneToolbar.sizeToFit()
        
        txtPhone.inputAccessoryView = doneToolbar
    }
    
    func doneButtonAction() {
        txtPhone.resignFirstResponder()
        tvMessage.resignFirstResponder()
    }
    
    func loadPrivacyPolicyInfo() {
        
        SVProgressHUD.show()
        
        guard let setting = CurrentCustomerInfo.shareInfo.loadSettingInfo() else {
            SVProgressHUD.dismiss()
            self.transitionViewStates(loading: false, error: NSError(domain: "error_server_fail".localized(), code: 1, userInfo: nil), animated: false, completion: nil)
            return
        }
        
        guard let i = setting.index(where: { $0.key == AppSetting.SETTING_PRIVACY_POLICY }) else {
            SVProgressHUD.dismiss()
            self.transitionViewStates(loading: false, error: NSError(domain: "error_server_fail".localized(), code: 1, userInfo: nil), animated: false, completion: nil)
            return
        }
        // End loading error page
        self.endLoading(error: nil)
        // Start get value from key
        let settingWithIndex : Setting = setting[i]
        
        if Localize.currentLanguage() == "en" {
            self.tvPrivacyPolicy.text = settingWithIndex.value
        } else {
            self.tvPrivacyPolicy.text = settingWithIndex.value_cn
        }
        
        SVProgressHUD.dismiss()
    }
    
    func loadCustomerData() {
        if let customer = CurrentCustomerInfo.shareInfo.loadCustomerInfo() {
            txtName.text = customer.name
            txtEmail.text = customer.email
            txtPhone.text = customer.phone
        }
    }
    
    func showConfirmPopup() {
        // Prepare data for popup
        let title = "common_info".localized()
        let message = "other_contact_us_msg_sent".localized()
        
        // Create the dialog
        let popup = PopupDialog(
            title: title,
            message: message,
            buttonAlignment: .horizontal,
            transitionStyle: .zoomIn,
            gestureDismissal: true
        ) {}
        
        // Add buttons to dialog
        popup.addButtons([DefaultButton(title: "common_ok".localized()) {
            _ = self.navigationController?.popViewController(animated: true)
            }])
        
        // Present dialog
        self.present(popup, animated: true, completion: nil)
    }
    
    func insertContactUsAPI(){
        
        SVProgressHUD.show()
        SVProgressHUD.setDefaultMaskType(SVProgressHUDMaskType.black)
        
        let contactData = Contact(
            restaurant_id: Constant.DEFAULT_SHOP_INFO_ID,
            name: txtName.text,
            email: txtEmail.text,
            phone: txtPhone.text,
            content: tvMessage.text,
            status: Constant.DEFAULT_CONTACT_STATUS)
        
        APIService.sharedService.insertContactUs(contactData, completed: { (result, error, message ) in
            guard result else {
                let alertController = UIAlertController(title: "common_info".localized(), message: "common_connection_fail".localized(), preferredStyle: UIAlertControllerStyle.alert)
                let okAction = UIAlertAction(title: "common_ok".localized(), style: UIAlertActionStyle.default) { (result : UIAlertAction) -> Void in
                }
                alertController.addAction(okAction)
                self.present(alertController, animated: true, completion: nil)
                return
            }
            
            SVProgressHUD.dismiss()
            self.showConfirmPopup()
        })
    }
    
    func submitContactUs(){
        
        // validate for name: accept full name with format "LastName FirstName"
        if let name = txtName.text {
            // Returns a new string made by removing from both ends of the String characters contained in a given character set.
            let cs = CharacterSet.init(charactersIn: " ")
            let trim = name.trimmingCharacters(in: cs)
            
            // If trimmed name have contains space characters
            guard trim.characters.contains(" ") else {
                txtName.becomeFirstResponder()
                lbNameStatus.text = "customer_lb_name_required_full_name".localized()
                lbNameStatus.textColor = AppColor.PRIMARY_COLOR
                return
            }
        }
        
        // validate for email
        if let email = txtEmail.text {
            
            guard email.characters.count > 0 else {
                txtEmail.becomeFirstResponder()
                lbEmailStatus.text = "customer_lb_email_required".localized()
                lbEmailStatus.textColor = AppColor.PRIMARY_COLOR
                return
            }
            
            guard email.isEmail(email: email) else {
                txtEmail.becomeFirstResponder()
                lbEmailStatus.text = "customer_lb_email_invalid".localized()
                lbEmailStatus.textColor = AppColor.PRIMARY_COLOR
                return
            }
        }
        
        // validate for phone number: only digits, 8 digits no less no more
        if let phone = txtPhone.text {
            
            guard phone.isPhoneNumber(phoneNumber: phone) else {
                txtPhone.becomeFirstResponder()
                lbPhoneStatus.text = "customer_lb_phone_invalid".localized()
                lbPhoneStatus.textColor = AppColor.PRIMARY_COLOR
                return
            }
        }
        
        // validate for phone number: only digits, 8 digits no less no more
        if let message = tvMessage.text {
            
            guard message.characters.count > 0 else {
                tvMessage.becomeFirstResponder()
                lbMessageStatus.text = "other_contact_us_msg_required".localized()
                lbMessageStatus.textColor = AppColor.PRIMARY_COLOR
                return
            }
        }
        
        insertContactUsAPI()
        
    }
    
    @IBAction func insertContactUs(_ sender: Any) {
        submitContactUs()
    }
}
