//
//  PrivacyPolicyController.swift
//  Toyosu
//
//  Created by nguyen.trung.hau on 10/24/16.
//  Copyright © 2016 IBS. All rights reserved.
//

import Foundation
import UIKit
import SVProgressHUD
import Localize_Swift
import StatefulViewController


class PrivacyPolicyController: BaseViewController, StatefulViewController {
    @IBOutlet weak var textView: UITextView!
    
    override func viewDidLoad() {
        
        textView.scrollRangeToVisible((NSMakeRange(0,0)))
        self.title = "other_lb_privacy_policy".localized()
        
        //create errorView
        let failureView = ErrorView(frame: view.frame)
        failureView.tapGestureRecognizer.addTarget(self, action: #selector(self.loadPolicy))
        errorView = failureView
       
        //load PrivacyPolicy
        loadPolicy()
        
        NotificationCenter.default.addObserver(self, selector: #selector(PrivacyPolicyController.refreshLanguage), name: NSNotification.Name(LCLLanguageChangeNotification), object: nil)
    }
    
    
    func loadPolicy(){
        
        SVProgressHUD.show()
        
        guard let setting = CurrentCustomerInfo.shareInfo.loadSettingInfo() else {
            SVProgressHUD.dismiss()
            self.transitionViewStates(loading: false, error: NSError(domain: "error_server_fail".localized(), code: 1, userInfo: nil), animated: false, completion: nil)
            return
        }
        
        guard let i = setting.index(where: { $0.key == AppSetting.SETTING_PRIVACY_POLICY }) else {
            SVProgressHUD.dismiss()
            self.transitionViewStates(loading: false, error: NSError(domain: "error_server_fail".localized(), code: 1, userInfo: nil), animated: false, completion: nil)
            return
        }
        // End loading error page
        self.endLoading(error: nil)
        // Start get value from key
        let settingWithIndex : Setting = setting[i]
        // Set value to text view
        if Localize.currentLanguage() == "en" {
            self.textView.text = settingWithIndex.value
        } else {
            self.textView.text = settingWithIndex.value_cn
        }
        
        SVProgressHUD.dismiss()
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    func refreshLanguage () {
        self.title = "other_lb_privacy_policy".localized()
    }
}
