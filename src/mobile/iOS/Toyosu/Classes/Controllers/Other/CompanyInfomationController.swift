//
//  CompanyInfomationController.swift
//  Toyosu
//
//  Created by nguyen.trung.hau on 11/3/16.
//  Copyright © 2016 IBS. All rights reserved.
//

import UIKit
import AFNetworking
import Localize_Swift

class ConpanyInformationController: UIViewController {
    
    @IBOutlet weak var comImage: UIImageView!
    
    @IBOutlet weak var lblComName: UILabel!
    
    @IBOutlet weak var lblResHeight: NSLayoutConstraint!
    
    @IBOutlet weak var lblAddress: RestaurantInsetLabel!
    @IBOutlet weak var lblPhoneNumber: RestaurantInsetLabel!
    @IBOutlet weak var txtfieldAbout: UITextView!
    @IBOutlet weak var mapView: UIImageView!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var lblEmailAddress: RestaurantInsetLabel!
    
    @IBOutlet weak var txtAboutHeight: NSLayoutConstraint!
    @IBOutlet weak var addressHeight: NSLayoutConstraint!
    
    @IBOutlet weak var lblAbout: UILabel!
    
    var res: Company?
    
    func loadCommon(inputRes:Company){
        self.title = "other_lb_company_info".localized()
        self.lblAbout?.text = "other_lb_about_us".localized()
        if Localize.currentLanguage() == "en"{
            
            self.lblComName.text = inputRes.company_name
            self.lblAddress.text = inputRes.company_address
            
            if inputRes.company_description  != ""{
                
                self.txtfieldAbout.contentSize = CGSize(width: self.txtfieldAbout.frame.size.width, height: self.txtfieldAbout.contentSize.height);
                self.txtfieldAbout.text = inputRes.company_description
            }
            
            if let height = inputRes.company_address?.heightOf(UIFont.systemFont(ofSize: 14), maxWidth: self.lblAddress.frame.size.width )
            {
                self.addressHeight.constant =  height + 1
                
            } else {
                self.addressHeight.constant =  24
            }
            if let height = inputRes.company_name?.heightOf(UIFont.systemFont(ofSize: 26), maxWidth: self.lblComName.frame.size.width)
            {
                self.lblResHeight.constant =  height + 1
                
            } else {
                self.lblResHeight.constant =  24
            }
        }else{
            
            self.lblComName.text = inputRes.company_name_cn
            self.lblAddress.text = inputRes.company_address_cn
            
            if inputRes.company_description_cn != ""{
                self.txtfieldAbout.contentSize = CGSize(width: self.txtfieldAbout.frame.size.width, height: self.txtfieldAbout.contentSize.height);
                self.txtfieldAbout.text = inputRes.company_description_cn
            }
            
            if let height = inputRes.company_address_cn?.heightOf(UIFont.systemFont(ofSize: 14), maxWidth: self.lblAddress.frame.size.width )
            {
                self.addressHeight.constant =  height + 1
                
            } else {
                self.addressHeight.constant =  24
            }
            if let height = inputRes.company_name_cn?.heightOf(UIFont.systemFont(ofSize: 26), maxWidth: self.lblComName.frame.size.width)
            {
                self.lblResHeight.constant =  height + 1
                
            } else {
                self.lblResHeight.constant =  24
            }
        }
        
    }
    func loadRestaurant()
    {
        
        self.txtfieldAbout.textContainerInset = UIEdgeInsetsMake(0, 8, 0, 8);
        self.txtfieldAbout.showsHorizontalScrollIndicator = false;
        
        if res == nil
        {
            APIService.sharedService.getCompanyInfo( completed: { (result, error, data) in
                
                if data != nil{
                    self.res = data;
                    self.loadCommon(inputRes: data!)
                    if data?.company_image_path == ""
                    {
                        self.comImage.setImageWith(  URL.init(string: "http://placehold.it/350x150")!, placeholderImage: nil)}
                    else{
                        let url:URL = URL.init(string:  (data?.company_image_path)!)!
                        self.comImage.setImageWith(  url, placeholderImage: nil)
                        
                    }
                        
                    if data?.company_map_url == ""
                    {
                        self.mapView.setImageWith(  URL.init(string: "http://placehold.it/350x150")!, placeholderImage: nil)}
                    else{
                        let url:URL = URL.init(string:  (data?.company_map_url)!)!
                        self.mapView.setImageWith(  url, placeholderImage: nil)
                        
                    }
                    self.lblPhoneNumber?.text = data?.company_phone
                    self.lblEmailAddress?.text = data?.company_email
                }
            })
            
        }
        
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        loadRestaurant()
        //loadCommon()
//        NotificationCenter.default.addObserver(self, selector: #selector(RestaurantDetailViewController.refreshLanguage), name: NSNotification.Name(LCLLanguageChangeNotification), object: nil)
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    func refreshLanguage () {
        loadCommon(inputRes: res!)
    }
    override func viewWillDisappear(_ animated: Bool) {
        _ = self.navigationController?.popViewController(animated: false)
    }
}
