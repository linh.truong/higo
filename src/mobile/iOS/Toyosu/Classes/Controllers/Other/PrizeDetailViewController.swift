//
//  PrizeDetailViewController.swift
//  Toyosu
//
//  Created by Linh Trương on 11/11/16.
//  Copyright © 2016 IBS. All rights reserved.
//

import UIKit
import Localize_Swift
import SVProgressHUD
import PopupDialog
import StatefulViewController

class PrizeDetailViewController: RightBarViewController, UITabBarControllerDelegate, StatefulViewController {
    
    var prize: Prize?
    var url : URL? = nil
    let dateFormatter = DateFormatter()
    var customer = CurrentCustomerInfo.shareInfo.loadCustomerInfo()
    
    // Id of cell
    static let segueID = "ShowPrizeDetail"
    
    @IBOutlet weak var tblDetail: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        prizeDetailInit()
        
        // Reload localized after change
        NotificationCenter.default.addObserver(self, selector: #selector(PrizeDetailViewController.refreshLanguage), name: NSNotification.Name(LCLLanguageChangeNotification), object: nil)
    }
    
    func tabBarController(_ tabBarController: UITabBarController, didSelect viewController: UIViewController) {
        _ = self.navigationController?.popToRootViewController(animated: false)
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    func prizeDetailInit(){
        setupInitialViewState()
        setupErrorViewState()
        tblDetail.delegate = self
        tblDetail.dataSource = self
        tblDetail.backgroundColor = AppColor.BACKGROUND_COLOR
        self.tabBarController?.delegate = self
        edgesForExtendedLayout = []
        dateFormatter.dateStyle = .long
        setupInitialViewState()
        setupErrorViewState()
    }
    
    func refreshLanguage() {
        tblDetail.reloadData()
    }
    
    func setupErrorViewState() {
        let failureView = ErrorView(frame: view.frame)
        failureView.tapGestureRecognizer.addTarget(self, action: #selector(PrizeDetailViewController.refreshLanguage))
        errorView = failureView
    }
    
    func linkToECSite(){
        var url: URL? = nil
        
        if let str = prize?.link_address {
            if let u = URL.init(string: str) {
                url = u
                UIApplication.shared.openURL(url!)
            }
        }
    }
    
    func checkUseButton(_ button: UIButton, _ fromDate: Date?){
        let date = Date()
        if let startDate = fromDate {
            if date >= startDate {
                button.setTitle("other_btn_use_prize2".localized(), for: .normal)
                button.isUserInteractionEnabled = true
                button.backgroundColor = AppColor.PRIMARY_COLOR
                if let deliType = prize?.delivery_type {
                    if deliType == Constant.DEFAULT_CUSTOMER_PRIZE_STATUS {
                        button.addTarget(self, action: #selector(self.performToDeliveryForm) , for: .touchUpInside)
                    } else {
                        button.addTarget(self, action: #selector(self.usePrize) , for: .touchUpInside)
                    }
                }
            } else {
                button.isUserInteractionEnabled = false
                button.backgroundColor = UIColor.lightGray
            }
        } else {
            button.isUserInteractionEnabled = false
            button.backgroundColor = UIColor.lightGray
        }
    }
    
    func usePrize(){
        
        SVProgressHUD.show()
        // Load customer information
        if let cus = CurrentCustomerInfo.shareInfo.loadCustomerInfo() {
            // Load authenticate token
            if let authToken = CurrentCustomerInfo.shareInfo.getToken() {
                
                let language = Localize.currentLanguage()
                // Get customer data
                let cusPrizeData = CustomerPrizes(
                    id: nil,
                    customer: cus.id,
                    prize: prize?.id,
                    delivery_type: prize?.delivery_type,
                    name: cus.name,
                    phone: cus.phone,
                    email: cus.email,
                    address: cus.address,
                    use_date: Date(),
                    delivery_date: Date(),
                    receive_date: Date(),
                    status: Constant.DEFAULT_PRIZE_STATUS_RECEIVED_STATUS)
                // Start call API
                APIService.sharedService.usePrize(cus.id!, authToken, (prize?.id)!, cusPrizeData, language) { (result, code, message, data) in
                    
                    guard result else {
                        SVProgressHUD.dismiss()
                        self.transitionViewStates(loading: false, error: NSError(domain: "error_internet_unavailable_title".localized(), code: 1, userInfo: nil), animated: false, completion: nil)
                        return
                    }
                    
                    guard code != ErrorCode.ERROR_CODE_FAIL else {
                        SVProgressHUD.dismiss()
                        self.transitionViewStates(loading: false, error: NSError(domain: "error_internet_unavailable_title".localized(), code: 1, userInfo: nil), animated: false, completion: nil)
                        return
                    }
                    
                    guard code != ErrorCode.ERROR_CODE_NOT_FOUND else {
                        SVProgressHUD.dismiss()
                        self.transitionViewStates(loading: false, error: NSError(domain: "error_unknown".localized(), code: 1, userInfo: nil), animated: false, completion: nil)
                        return
                    }
                    
                    guard code != ErrorCode.ERROR_CODE_USER_LACK_POINT else {
                        SVProgressHUD.dismiss()
                        let popup = CommonFunctions.initPopup(title: "common_info".localized(), message: "customer_current_point_not_enough".localized())
                        popup.addButtons([DefaultButton(title: "common_ok".localized()) {
                            _ = self.navigationController?.popViewController(animated: true)
                            }])
                        self.present(popup, animated: true, completion: nil)
                        return
                    }
                    
                    guard code != ErrorCode.ERROR_CODE_EMPTY_STOCK else {
                        SVProgressHUD.dismiss()
                        let popup = CommonFunctions.initPopup(title: "common_info".localized(), message: "other_prize_msg_not_found".localized())
                        popup.addButtons([DefaultButton(title: "common_ok".localized()) {
                            _ = self.navigationController?.popViewController(animated: true)
                            }])
                        self.present(popup, animated: true, completion: nil)
                        return
                    }
                    
                    self.endLoading()
                    SVProgressHUD.dismiss()
                    self.showTicket()
                }
                
                SVProgressHUD.dismiss()
            }
        } else {
            self.showRequiredLoginPopup()
        }
    }
    
    func performToDeliveryForm(){
        performSegue(withIdentifier: CustomerPrizeViewController.segueID, sender: nil)
    }
    
    func showConfirmPopup() {
        let popup = CommonFunctions.initPopup(title: "common_info".localized(), message: "other_prize_lb_used".localized())
        // Add buttons to dialog
        popup.addButtons([DefaultButton(title: "common_ok".localized()) {
            _ = self.navigationController?.popViewController(animated: true)
            }])
        
        // Present dialog
        self.present(popup, animated: true, completion: nil)
    }
    
    func showRequiredLoginPopup() {
        let popup = CommonFunctions.initPopup(title: "common_info".localized(), message: "other_prize_msg_login_required".localized())
        
        // Add buttons to dialog
        popup.addButtons([DefaultButton(title: "common_ok".localized()) {
            CurrentCustomerInfo.shareInfo.customer = nil
            let homeStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let vc = homeStoryboard.instantiateViewController(withIdentifier: "Login")
            self.navigationController?.pushViewController(vc, animated: true)
            }])
        
        // Add buttons to dialog
        popup.addButtons([DefaultButton(title: "common_cancel".localized()) {
            }])
        
        // Present dialog
        self.present(popup, animated: true, completion: nil)
        
    }
    
    func showTicket() {
        // Prepare data for popup
        let titlePopup = "other_prize_lb_used".localized()
        
        if Localize.currentLanguage() == "en" {
            if let title = prize?.title {
                if let fromDate = prize?.from_date {
                    let fromDateStr = String(format: NSLocalizedString("other_prize_lb_from_date".localized(), comment: ""), "\(dateFormatter.string(from: fromDate))")
                    let message = title + "\n" + fromDateStr
                    // Create the dialog
                    let popup = PopupDialog(
                        title: titlePopup,
                        message: message,
                        buttonAlignment: .horizontal,
                        transitionStyle: .zoomIn,
                        gestureDismissal: true
                    ) {}
                    // Add buttons to dialog
                    popup.addButtons([DefaultButton(title: "common_ok".localized()) {
                        let allVC = self.navigationController?.viewControllers
                        
                        if let listVC = allVC![allVC!.count - 2] as? PointManagementViewController {
                            self.navigationController!.popToViewController(listVC, animated: true)
                        }
                        }])
                    // Present dialog
                    self.present(popup, animated: true, completion: nil)
                }
            }
        } else {
            if let title_cn = prize?.title_cn {
                if let fromDate = prize?.from_date {
                    let fromDateStr = String(format: NSLocalizedString("coupon_lb_expired_date".localized(), comment: ""), "\(dateFormatter.string(from: fromDate))")
                    let message = title_cn + "\n" + fromDateStr
                    
                    // Create the dialog
                    let popup = PopupDialog(
                        title: titlePopup,
                        message: message,
                        buttonAlignment: .horizontal,
                        transitionStyle: .zoomIn,
                        gestureDismissal: true
                    ) {}
                    
                    // Add buttons to dialog
                    popup.addButtons([DefaultButton(title: "common_ok".localized()) {
                        let allVC = self.navigationController?.viewControllers
                        
                        if let listVC = allVC![allVC!.count - 2] as? PointManagementViewController {
                            self.navigationController!.popToViewController(listVC, animated: true)
                        }
                        }])
                    
                    // Present dialog
                    self.present(popup, animated: true, completion: nil)
                }
            }
        }
    }
}

extension PrizeDetailViewController : UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        switch indexPath.row {
        case 0:
            let cell = tableView.dequeueReusableCell(withIdentifier: PrizeDetailTableViewCell.cellID, for: indexPath) as! PrizeDetailTableViewCell
            
            // full screen image
            if let pr = prize {
                cell.calculateImageSize(pr)
            }
            
            if let str = prize?.image_path {
                if let u = URL.init(string: str) {
                    url = u
                    if let fUrl = url {
                        cell.imgPath.setImageWith(fUrl)
                    } else {
                        cell.imgPath.image = UIImage.init(named: "no_img")
                    }
                }
            }
            
            // load request point - title
            if Localize.currentLanguage() == "en" {
                cell.lbPointsAndTitle.text = prize?.title ?? "error_no_data".localized()
            } else {
                cell.lbPointsAndTitle.text = prize?.title_cn ?? "error_no_data".localized()
            }
            
            // load start date
            if let fromDate = prize?.from_date {
                
                cell.lbStartDate.text = String(format: NSLocalizedString("coupon_lb_start_date".localized(), comment: ""), "\(dateFormatter.string(from: fromDate))")
            }
            
            // load button link to EC site
            cell.btnLinkToECSite.setTitle("other_prize_btn_link_ecsite".localized(), for: .normal)
            cell.btnLinkToECSite.addTarget(self, action: #selector(self.linkToECSite), for: UIControlEvents.touchUpInside)
            
            // load T&C
            if let cond = prize?.condition {
                if let condCn = prize?.condition_cn {
                    if Localize.currentLanguage() == "en" {
                        cell.lbTermAndCondition.text = cond
                    } else {
                        cell.lbTermAndCondition.text = condCn
                    }
                }
            }
            checkUseButton(cell.btnUse, prize?.from_date)
            
            return cell
        default:
            
            return UITableViewCell()
        }
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        switch indexPath.row {
        case 0:
            tableView.estimatedRowHeight = 500.0
            tableView.rowHeight = UITableViewAutomaticDimension
            return tableView.rowHeight
        default:
            return 0
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if (segue.identifier == CustomerPrizeViewController.segueID) {
            let cusPrizeVC = segue.destination as! CustomerPrizeViewController
            cusPrizeVC.prize = prize
        }
    }
}


