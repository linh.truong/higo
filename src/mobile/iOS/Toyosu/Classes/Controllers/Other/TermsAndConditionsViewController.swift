//
//  TermsAndConditionsViewController.swift
//  Toyosu
//
//  Created by Linh Trương on 11/2/16.
//  Copyright © 2016 IBS. All rights reserved.
//

import Foundation
import UIKit
import SVProgressHUD
import Localize_Swift
import StatefulViewController

class TermsAndConditionsViewController: BaseViewController, StatefulViewController, UITextViewDelegate {
    
    @IBOutlet weak var txtContent: UITextView!
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = false
        txtContent.delegate = self
        txtContent.scrollRangeToVisible((NSMakeRange(0,0)))
        self.title = "other_lb_terms_and_condition".localized()
    }
    
    override func viewDidLoad() {
        
        //create errorView
        let failureView = ErrorView(frame: view.frame)
        failureView.tapGestureRecognizer.addTarget(self, action: #selector(self.loadTermAndServicePolicy))
        errorView = failureView
        
        setupInitialViewState()
        
        //load PrivacyPolicy
        loadTermAndServicePolicy()
        
        NotificationCenter.default.addObserver(self, selector: #selector(TermsAndConditionsViewController.refreshLanguage), name: NSNotification.Name(LCLLanguageChangeNotification), object: nil)
    }
    
    func loadTermAndServicePolicy(){
        
        SVProgressHUD.show()
        
        guard let setting = CurrentCustomerInfo.shareInfo.loadSettingInfo() else {
            SVProgressHUD.dismiss()
            self.transitionViewStates(loading: false, error: NSError(domain: "error_server_fail".localized(), code: 1, userInfo: nil), animated: false, completion: nil)
            return
        }
        
        guard let i = setting.index(where: { $0.key == AppSetting.SETTING_TERM_AND_CONDITION }) else {
            SVProgressHUD.dismiss()
            self.transitionViewStates(loading: false, error: NSError(domain: "error_server_fail".localized(), code: 1, userInfo: nil), animated: false, completion: nil)
            return
        }
        // End loading error page
        self.endLoading(error: nil)
        // Start get value from key
        let settingWithIndex : Setting = setting[i]
        // Set value to text view
        if Localize.currentLanguage() == "en" {
            txtContent.text = settingWithIndex.value
        } else {
            txtContent.text = settingWithIndex.value_cn
        }
        
        SVProgressHUD.dismiss()
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    func refreshLanguage () {
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        _ = self.navigationController?.popViewController(animated: false)
    }
}
