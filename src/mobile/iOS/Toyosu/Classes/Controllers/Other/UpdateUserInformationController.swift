//
//  ViewController.swift
//  Toyosu
//
//  Created Linh Trương on 10/3/16.
//  Copyright © 2016 IBS. All rights reserved.
//

import UIKit
import Localize_Swift
import StatefulViewController
import SVProgressHUD
import PopupDialog

class UpdateUserInformationController : BaseViewController, UITextFieldDelegate, StatefulViewController {
    
    var registerResult: NSDictionary?
    var areaArr = [Area]()
    var selectedArea : Int? = 0
    var selectedAge: Int? = Constant.CUSTOMER_MIN_AGE
    var customer = CurrentCustomerInfo.shareInfo.loadCustomerInfo()
    var selectedBirthday : Date? = nil
    let currentCalendar = Calendar.current
    let currentDate = Date()
    
    @IBOutlet weak var viewheight: NSLayoutConstraint!
    @IBOutlet weak var txtName: UITextField!
    @IBOutlet weak var lbNameStatus: UILabel!
    @IBOutlet weak var txtEmail: UITextField!
    @IBOutlet weak var lbEmailStatus: UILabel!
    @IBOutlet weak var txtPhone: UITextField!
    @IBOutlet weak var lbPhoneStatus: UILabel!
    @IBOutlet weak var txtAddress: UITextField!
    @IBOutlet weak var lbAddressStatus: UILabel!
    @IBOutlet weak var lbAgeStatus: UILabel!
    @IBOutlet weak var scrollView: RegisterScrollView!
    @IBOutlet weak var segCtrlGender: UISegmentedControl!
    @IBOutlet weak var btnUpdate: UIButton!
    @IBOutlet weak var pickerResidentailArea: RegisterPickerView!
    @IBOutlet weak var lblResidentialArea: UILabel!
    @IBOutlet weak var lblGender: UILabel!
    @IBOutlet weak var birthdayDatePicker: UIDatePicker!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        screenInit()
        
        NotificationCenter.default.addObserver(self, selector: #selector(UpdateUserInformationController.refreshLanguage), name: NSNotification.Name(LCLLanguageChangeNotification), object: nil)
        
        viewheight.constant = btnUpdate.frame.origin.y + btnUpdate.frame.size.height + (self.navigationController?.navigationBar.frame.height)!
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    func refreshLanguage () {
        setLangLocalized()
    }
    
    func screenInit() {
        dismissKeyboardWhenTapped()
        setScreenLayout()
        setLangLocalized()
        setDelegate()
        changeKeyboardLayout()
        addDoneButtonOnKeyboard()
        setupInitialViewState()
        setupErrorViewState()
        loadAreaList()
    }
    
    func setupErrorViewState() {
        let failureView = ErrorView(frame: view.frame)
        failureView.tapGestureRecognizer.addTarget(self, action: #selector(UpdateUserInformationController.screenInit))
        errorView = failureView
    }
    
    func setDelegate() {
        txtName.delegate = self
        txtEmail.delegate = self
        txtPhone.delegate = self
        txtAddress.delegate = self
        pickerResidentailArea.delegate = self
    }
    
    func setLangLocalized(){
        self.title = "other_lb_update_profile".localized()
        lbNameStatus.setRequiredAttribute("customer_lb_name".localized())
        lbEmailStatus.setRequiredAttribute("customer_lb_email".localized())
        lbPhoneStatus.setRequiredAttribute("customer_lb_phone".localized())
        lbAddressStatus.text = "customer_lb_address".localized()
        lbAgeStatus.text = "customer_lb_birthday".localized()
        lblGender.text = "customer_lb_gender".localized()
        segCtrlGender.setTitle("common_sex_male".localized(), forSegmentAt: 0)
        segCtrlGender.setTitle("common_sex_female".localized(), forSegmentAt: 1)
        segCtrlGender.setTitle("common_sex_unknown".localized(), forSegmentAt: 2)
        lblResidentialArea.text = "customer_hint_residential_areas".localized()
        btnUpdate.setTitle("customer_btn_update".localized(), for: .normal)
    }
    
    func setScreenLayout(){
        btnUpdate.layer.cornerRadius = 5
        txtEmail.isUserInteractionEnabled = false
        lbNameStatus.textColor = AppColor.DARK_GRAY_COLOR
        lbEmailStatus.textColor = AppColor.DARK_GRAY_COLOR
        lbPhoneStatus.textColor = AppColor.DARK_GRAY_COLOR
        lbAddressStatus.textColor = AppColor.DARK_GRAY_COLOR
        lblGender.textColor = AppColor.DARK_GRAY_COLOR
        lbAgeStatus.textColor = AppColor.DARK_GRAY_COLOR
        lblResidentialArea.textColor = AppColor.DARK_GRAY_COLOR
        segCtrlGender.tintColor = AppColor.PRIMARY_COLOR
    }
    
    func changeKeyboardLayout(){
        txtName.returnKeyType = UIReturnKeyType.next
        txtPhone.returnKeyType = UIReturnKeyType.next
        txtAddress.returnKeyType = UIReturnKeyType.next
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        if textField == txtName {
            txtPhone.becomeFirstResponder()
        }
        
        if textField == txtPhone {
            txtAddress.becomeFirstResponder()
        }
        
        return false
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        
        if textField == txtName {
            lbNameStatus.textColor = AppColor.DARK_GRAY_COLOR
            lbNameStatus.setRequiredAttribute("customer_lb_name".localized())
        }
        
        if textField == txtPhone {
            lbPhoneStatus.textColor = AppColor.DARK_GRAY_COLOR
            lbPhoneStatus.setRequiredAttribute("customer_lb_phone".localized())
        }
        
        if textField == txtAddress {
            lbAddressStatus.textColor = AppColor.DARK_GRAY_COLOR
            lbAgeStatus.setRequiredAttribute("customer_lb_address".localized())
        }
        
    }
    
    func loadUserData() {
       
        self.txtName.text  = customer?.name
        self.txtEmail.text = customer?.email
        self.txtPhone.text = customer?.phone
        
        if let address = customer?.address
        {
            self.txtAddress.text = address
        }
        
        if let sex = customer?.sex
        {
            self.segCtrlGender.selectedSegmentIndex = sex
        }
        
        if let area_id = customer?.area
        {
            self.pickerResidentailArea.selectRow(self.findAreaInList(areaId: area_id), inComponent: 0, animated: true)
        }
    }
    
    func findAreaInList(areaId: Int) -> Int{
        if areaArr.isEmpty{
            return 0
        }
        
        if let i = areaArr.index(where: { $0.id == areaId}) {
            return i
        }
    
        return 0
    }
    
    func loadAreaList(){
        
        SVProgressHUD.show()
        
        APIService.sharedService.getAreaList { (result, message, code, data) in
            
            guard result else {
                SVProgressHUD.dismiss()
                self.transitionViewStates(loading: false, error: NSError(domain: "error_internet_unavailable_title".localized(), code: 1, userInfo: nil), animated: false, completion: nil)
                return
            }
            
            guard code != ErrorCode.ERROR_CODE_FAIL else {
                SVProgressHUD.dismiss()
                self.transitionViewStates(loading: false, error: NSError(domain: "error_server_fail".localized(), code: 1, userInfo: nil), animated: false, completion: nil)
                return
            }
            
            self.endLoading(error: nil)
            
            // Area list
            for area in data!
            {
                self.areaArr.append(area)
            }
            self.pickerResidentailArea.reloadAllComponents()
            
            // only load userdata after finish load Area list
            self.configureBirthdayPicker()
            self.loadUserData()
            
            SVProgressHUD.dismiss()
        }
    }
    
    func configureBirthdayPicker(){
        birthdayDatePicker.datePickerMode = .date
        birthdayDatePicker.locale = Locale.current
        
        var componentsMin = currentCalendar.dateComponents([.year], from: currentDate)
        var componentsMax = componentsMin
        if let yearMin = componentsMin.year {
            let yearMinimum = yearMin - Constant.CUSTOMER_MAX_AGE
            componentsMin.year = yearMinimum
            let minimumDate : Date = currentCalendar.date(from: componentsMin)!
            birthdayDatePicker.minimumDate = minimumDate
        }
        if let yearMax = componentsMax.year {
            let yearMaximum = yearMax - Constant.CUSTOMER_MIN_AGE
            componentsMax.year = yearMaximum
            let maximumDate : Date = currentCalendar.date(from: componentsMax)!
            birthdayDatePicker.maximumDate = maximumDate
        }
        
        for subview in self.birthdayDatePicker.subviews {
            
            if subview.frame.height <= 5 {
                
                subview.backgroundColor = UIColor.white
                subview.tintColor = UIColor.white
                subview.layer.borderColor = AppColor.PRIMARY_COLOR.cgColor
                subview.layer.borderWidth = 0.5            }
        }
        
        if let pickerView = self.birthdayDatePicker.subviews.first {
            
            for subview in pickerView.subviews {
                
                if subview.frame.height <= 5 {
                    subview.backgroundColor = UIColor.white
                    subview.tintColor = UIColor.white
                    subview.layer.borderColor = AppColor.PRIMARY_COLOR.cgColor
                    subview.layer.borderWidth = 0.5
                }
            }
        }
        
        if let birthday = customer?.birthday {
            self.birthdayDatePicker.date = birthday
        } else {
            var components = currentCalendar.dateComponents([.year], from: currentDate)
            if let year = components.year {
                let nullBirthdayYear : Int? = year - (customer?.age)!
                components.year = nullBirthdayYear
                let nullBirthDate : Date = currentCalendar.date(from: components)!
                self.birthdayDatePicker.date = nullBirthDate
            }
        }
    }
    
    @IBAction func birthdayIndexChanged(_ sender: UIDatePicker) {
        let yearNow = currentDate.getYear()
        self.selectedBirthday = sender.date
        let yearChoose = self.selectedBirthday?.getYear()
        self.selectedAge = yearNow - yearChoose!
    }
    
    
    func addDoneButtonOnKeyboard() {
        let doneToolbar: UIToolbar = UIToolbar(frame: CGRect(x: 0, y: 0, width: 320, height: 50))
        doneToolbar.barStyle       = UIBarStyle.default
        let flexSpace              = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        let done: UIBarButtonItem  = UIBarButtonItem(title: "common_done".localized(), style: UIBarButtonItemStyle.done, target: self, action: #selector(UpdateUserInformationController.doneButtonAction))
        
        var items = [UIBarButtonItem]()
        items.append(flexSpace)
        items.append(done)
        
        doneToolbar.items = items
        doneToolbar.sizeToFit()
        
        txtName.inputAccessoryView = doneToolbar
        txtPhone.inputAccessoryView = doneToolbar
        txtAddress.inputAccessoryView = doneToolbar
    }
    
    func doneButtonAction() {
        txtName.resignFirstResponder()
        txtPhone.resignFirstResponder()
        txtAddress.resignFirstResponder()
    }
    
    @IBAction func update(_ sender: AnyObject) {
        
        // validate for name: accept full name with format "LastName FirstName"
        if let name = txtName.text {
            // Returns a new string made by removing from both ends of the String characters contained in a given character set.
            let cs = CharacterSet.init(charactersIn: " ")
            let trim = name.trimmingCharacters(in: cs)
            
            // If trimmed name have contains space characters
            guard trim.characters.contains(" ") else {
                txtName.becomeFirstResponder()
                lbNameStatus.text = "customer_lb_name_required_full_name".localized()
                lbNameStatus.textColor = AppColor.PRIMARY_COLOR
                return
            }
        }
        
        // validate for phone number: only digits, 8 digits no less no more
        if let phone = txtPhone.text {
            
            guard phone.isPhoneNumber(phoneNumber: phone) else {
                txtPhone.becomeFirstResponder()
                lbPhoneStatus.text = "customer_lb_phone_invalid".localized()
                lbPhoneStatus.textColor = AppColor.PRIMARY_COLOR
                return
            }
        }
        
        updateInformation()
    }
    
    // update user info
    func updateInformation(){
        
        UIApplication.shared.beginIgnoringInteractionEvents()
       
        let customerData = Customers(id: CurrentCustomerInfo.shareInfo.customer?.id,
                                     area: customer?.area,
                                     restaurant: Constant.DEFAULT_SHOP_INFO_ID,
                                     number: nil,
                                     name: txtName.text,
                                     image_path: nil,
                                     age: self.selectedAge ?? Constant.CUSTOMER_MIN_AGE,
                                     birthday: self.selectedBirthday ?? birthdayDatePicker.maximumDate,
                                     sex: segCtrlGender.selectedSegmentIndex,
                                     address: txtAddress.text,
                                     gps_data: "",
                                     email: nil,
                                     login_password: nil,
                                     phone: txtPhone.text ?? nil,
                                     last_transaction_date: nil,
                                     current_point: nil,
                                     is_mail_notify: nil,
                                     is_sms_notify: nil,
                                     recover_password: nil)
        
        if let customer = CurrentCustomerInfo.shareInfo.customer {
            if let authToken = CurrentCustomerInfo.shareInfo.getToken() {
                
                SVProgressHUD.show()
                
                APIService.sharedService.updateCustomer(customerData, authToken, customer.id!, completed: { (result, code, message, rsCustomer) in
                    if result == false {
                        self.transitionViewStates(loading: false, error:  NSError(domain: "common_connection_fail".localized(), code: 1, userInfo: nil), animated: false, completion: nil)
                    } else {
                        self.endLoading(error: nil)
                        
                        if (code == ErrorCode.ERROR_CODE_SUCCESS){
                            SVProgressHUD.dismiss()
                            UIApplication.shared.endIgnoringInteractionEvents()
                            // Save new customer information
                            CurrentCustomerInfo.shareInfo.saveCustomerMyPage(rsCustomer)
                            self.showSuccessPopup()
                        } else {
                            SVProgressHUD.dismiss()
                            UIApplication.shared.endIgnoringInteractionEvents()
                            
                            if (code == 31) {
                                
                                let message = message?.characters.split(separator: " ").map(String.init)[0]
                                if(message == "Phone"){
                                    self.lbPhoneStatus.text = "customer_lb_phone_exist".localized()
                                    self.lbPhoneStatus.textColor = AppColor.PRIMARY_COLOR
                                }
                                
                                if(message == "Email"){
                                    self.lbEmailStatus.text = "customer_lb_email_exist".localized()
                                    self.lbEmailStatus.textColor = AppColor.PRIMARY_COLOR
                                }
                                self.scrollView.setContentOffset(CGPoint.zero, animated: true)
                            } else {
                                SVProgressHUD.dismiss()
                                self.showErrorPopup()
                            }
                        }
                    }
                })
                SVProgressHUD.dismiss()
            }
            
        } else {
            self.logout()
        }
    }
    
    func showSuccessPopup() {
        // Prepare data for popup
        let title = "common_info".localized()
        let message = "customer_msg_update_success".localized()
        
        // Create the dialog
        let popup = PopupDialog(
            title: title,
            message: message,
            buttonAlignment: .horizontal,
            transitionStyle: .zoomIn,
            gestureDismissal: true
        ) {}
        
        // Add buttons to dialog
        popup.addButtons([DefaultButton(title: "common_ok".localized()) {
            _ = self.navigationController?.popViewController(animated: true)
            }])
        
        // Present dialog
        self.present(popup, animated: true, completion: nil)
        
    }
    
    func showErrorPopup() {
        // Prepare data for popup
        let title = "common_info".localized()
        let message = "common_connection_fail".localized()
        
        // Create the dialog
        let popup = PopupDialog(
            title: title,
            message: message,
            buttonAlignment: .horizontal,
            transitionStyle: .zoomIn,
            gestureDismissal: true
        ) {}
        
        // Add buttons to dialog
        popup.addButtons([DefaultButton(title: "common_ok".localized()) {}])
        
        // Present dialog
        self.present(popup, animated: true, completion: nil)
        
    }
    
    func logout(){
        //show error Message
        let alertController = UIAlertController(title: "customer_lb_logout".localized(), message: "customer_msg_logout_confirm".localized(), preferredStyle: UIAlertControllerStyle.alert)
        
        let okAction = UIAlertAction(title: "common_yes".localized(), style: UIAlertActionStyle.default) { (result : UIAlertAction) -> Void in
            
            //call login function
            APIService.sharedService.logOut( completed: { (result, error) in
                if result == true
                {
                    CurrentCustomerInfo.shareInfo.customer = nil
                    CurrentCustomerInfo.shareInfo.persistenceCurrentInfo()
                    let homeStoryboard = UIStoryboard(name: "Main", bundle: nil)
                    let tabBar = homeStoryboard.instantiateViewController(withIdentifier: "tabBar")
                    let appDelegate = UIApplication.shared.delegate as! AppDelegate
                    appDelegate.window!.rootViewController = tabBar
                    
                }else{
                    
                    let alertController = UIAlertController(title: "common_info".localized(), message: "common_connection_fail".localized(), preferredStyle: UIAlertControllerStyle.alert)
                    
                    let okAction = UIAlertAction(title: "common_ok".localized(), style: UIAlertActionStyle.default) { (result : UIAlertAction) -> Void in
                        print("OK")
                    }
                    alertController.addAction(okAction)
                    
                    self.present(alertController, animated: true, completion: nil)
                }
                
            })
        }
        
        let cancelAction = UIAlertAction(title: "common_no".localized(), style: UIAlertActionStyle.default) { (result : UIAlertAction) -> Void in
            alertController.dismiss(animated: true, completion: nil)
            
        }
        
        alertController.addAction(okAction)
        alertController.addAction(cancelAction)
        self.present(alertController, animated: true, completion: nil)
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if textField.placeholder == "customer_lb_name".localized() {
            if (txtName.text?.characters.count)! > 49  && range.length == 0
            {
                return false
            }        }
        if textField.placeholder == "customer_lb_email".localized() {
            if (txtEmail.text?.characters.count)! > 49  && range.length == 0
            {
                return false
            }
        }
        if textField.placeholder == "customer_lb_phone".localized() {
            if (txtPhone.text?.characters.count)! > 7  && range.length == 0
            {
                return false
            }
        }
        if textField.placeholder == "customer_lb_address".localized() {
            if (txtAddress.text?.characters.count)! > 199  && range.length == 0
            {
                return false
            }
        }
        return true
    }
}

extension UpdateUserInformationController : UIPickerViewDataSource, UIPickerViewDelegate {
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return areaArr.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if Localize.currentLanguage() == "en" {
            return areaArr[row].name
        } else {
            return areaArr[row].name_cn
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        self.selectedArea = areaArr[row].id
        self.customer?.area = areaArr[row].id
    }
    
    func pickerView(_ pickerView: UIPickerView, viewForRow row: Int, forComponent component: Int, reusing view: UIView?) -> UIView {
        let pickerLabel = UILabel()
        pickerLabel.textColor = UIColor.black
        pickerLabel.font = UIFont.systemFont(ofSize: 22)
        pickerLabel.textAlignment = NSTextAlignment.center
        //        pickerLabel.backgroundColor = UIColor.white
        
        if Localize.currentLanguage() == "en" {
            pickerLabel.text = areaArr[row].name
        } else {
            pickerLabel.text = areaArr[row].name_cn
        }
        
        return pickerLabel
    }
}

