//
//  OtherTableViewController.swift
//  Toyosu
//
//  Created by nguyen.trung.hau on 10/21/16.
//  Copyright © 2016 IBS. All rights reserved.
//

import Foundation
import UIKit
import Localize_Swift
import Firebase
import PopupDialog

class OthersViewController: UIViewController , UITableViewDataSource, UITableViewDelegate{
    
    // MARK: - Table view data source
    var language: [String] = Localize.availableLanguages(true)
 
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        tableView.dataSource = self
        tableView.delegate = self
        tableView.separatorStyle = UITableViewCellSeparatorStyle.none
        tableView.backgroundColor =  UIColor.init(red: CGFloat(244)/255.0 , green: CGFloat(244)/255.0 , blue: CGFloat(244)/255.0 , alpha: CGFloat(1))
        NotificationCenter.default.addObserver(self, selector: #selector(OthersViewController.refreshLanguage), name: NSNotification.Name(LCLLanguageChangeNotification), object: nil)
    }
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    func refreshLanguage() {
        navigationItem.title = "other_lb_other".localized()
        tableView.reloadData()
    }
    
    func languageDidChange(_ seg: UISegmentedControl) {
        Localize.setCurrentLanguage(language[seg.selectedSegmentIndex])
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        cell.backgroundColor = UIColor.clear
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 3
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        switch section{
        case 0:
            return "other_lb_setting".localized()
            
        case 1:
            return "other_lb_section_information".localized()
            
        default:
            return " "
        }
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if section == 3
        {
            return 15
        }
        else
        {
            return tableView.sectionHeaderHeight
        }
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
            
        case 0:
            return 2
        case 1:
            return 5
        case 2:
            return 1
        default:
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        switch indexPath.section{
        case 0:
            switch indexPath.row {
            case 0:
                let cell = tableView.dequeueReusableCell(withIdentifier: "otherCells", for: indexPath) as UITableViewCell as! OtherTableViewCell
                cell.setLabelName(name: "other_lb_update_profile".localized())
                
                return cell
            // Language
            case 1:
                let cell = tableView.dequeueReusableCell(withIdentifier: "languageCell", for: indexPath) as UITableViewCell as! SettingLanguageTableViewCell
                cell.lbLanguage.text = "other_lb_language".localized()
                cell.setLanguages(language, indexLang: language.index(of: Localize.currentLanguage()))
                cell.segLanguage.addTarget(self, action: #selector(self.languageDidChange(_:)), for: .valueChanged)
                cell.segLanguage.setTitle("other_lang_english".localized(), forSegmentAt: 0)
                cell.segLanguage.setTitle("other_lang_china".localized(), forSegmentAt: 1)
                cell.segLanguage.removeSegment(at: 2, animated: false)
                
                return cell
                
            default:
                return UITableViewCell()
            }
        // Section Information
        case 1:
            switch indexPath.row{
            // Shop info
            case 0:
                let cell = tableView.dequeueReusableCell(withIdentifier: "otherCells", for: indexPath) as! OtherTableViewCell
                cell.setLabelName(name: "shop_info_lb_title".localized())
                return cell
            // About Point
            case 1:
                let cell = tableView.dequeueReusableCell(withIdentifier: "otherCells", for: indexPath) as! OtherTableViewCell
                cell.setLabelName(name: "other_lb_about_point_prize".localized())
                return cell
            // About Prize
//            case 2:
//                let cell = tableView.dequeueReusableCell(withIdentifier: "otherCells", for: indexPath) as! OtherTableViewCell
//                cell.setLabelName(name: "other_lb_about_prize".localized())
//                return cell
            // Terms and Conditions
            case 2:
                let cell = tableView.dequeueReusableCell(withIdentifier: "otherCells", for: indexPath) as! OtherTableViewCell
                cell.setLabelName(name: "other_lb_terms_and_condition".localized())
                return cell
            // Privacy Policy
            case 3:
                let cell = tableView.dequeueReusableCell(withIdentifier: "otherCells", for: indexPath) as! OtherTableViewCell
                cell.setLabelName(name: "other_lb_privacy_policy".localized())
                return cell
            // App Version
            case 4:
                let cell = tableView.dequeueReusableCell(withIdentifier: "app_version", for: indexPath) as! AppVersionTableViewCell
                cell.appVersion.text = Constant.APPLICATION_VERSION
                cell.lblAppversion.text = "other_lb_app_version".localized()
                cell.selectionStyle = .none
                return cell
            default:
                return UITableViewCell()
            }
        // Logout
        case 2:
            // Show Logout
            if CurrentCustomerInfo.shareInfo.customer != nil {
                let cell = tableView.dequeueReusableCell(withIdentifier: "loginCell", for: indexPath) as! LoginTableViewCell
                cell.setLabelName(name: "customer_lb_logout".localized(), image: "logout")
                
                return cell
            // Show Login
            }else{
                let cell = tableView.dequeueReusableCell(withIdentifier: "loginCell", for: indexPath) as! LoginTableViewCell
                cell.setLabelName(name: "customer_lb_login".localized(), image: "login")
                
                return cell
            }
        default:
            return UITableViewCell()
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        switch indexPath.section {
        case 0:
            switch indexPath.row {
            case 0:
                performSegue(withIdentifier: "vc_update_profile", sender: nil)
                break
            default:
                break
            }
        case 1:
            switch indexPath.row{
            case 0:
                let storyboard = UIStoryboard(name: "RestaurantInfo", bundle: nil)
                let controller = storyboard.instantiateViewController(withIdentifier: "ShopInfoListViewController") as! ShopInfoListViewController
                self.show(controller, sender: nil)
            case 1:
                performSegue(withIdentifier: "ShowAboutPointAndPrize", sender: nil)
//            case 2:
//                performSegue(withIdentifier: "ShowAboutPrize", sender: nil)
            case 2:
                performSegue(withIdentifier: "terms_and_condition", sender: nil)
            case 3:
                performSegue(withIdentifier: "vc_privacy_policy", sender: nil)
            case 4:
                break
            default:
                break
            }
            
        case 2:
            if CurrentCustomerInfo.shareInfo.customer != nil{
                showLogoutPopup()
            }else{
                showLogin()
            }
            return
        default:
            break
        }
    }
    
    func showLogoutPopup() {
        // Prepare data for popup
        let title = "customer_lb_logout".localized()
        let message = "customer_msg_logout_confirm".localized()
        
        // Create the dialog
        let popup = PopupDialog(
            title: title,
            message: message,
            buttonAlignment: .horizontal,
            transitionStyle: .zoomIn,
            gestureDismissal: true
        ) {}
        
        // Add buttons to dialog
        popup.addButtons([DefaultButton(title: "common_yes".localized()) {
            APIService.sharedService.logOut( completed: { (result, error) in
                if result == true
                {
                    CurrentCustomerInfo.shareInfo.customer = nil
                    CurrentCustomerInfo.shareInfo.persistenceCurrentInfo()
                    // To Home
//                    let homeStoryboard = UIStoryboard(name: "Main", bundle: nil)
//                    let tabBar = homeStoryboard.instantiateViewController(withIdentifier: "tabBar")
//                    let appDelegate = UIApplication.shared.delegate as! AppDelegate
//                    appDelegate.window!.rootViewController = tabBar
                    // To Login
                    _ = self.tabBarController?.navigationController?.popToRootViewController(animated: true)
                    
                }else{
                    
                    let alertController = UIAlertController(title: "common_info".localized(), message: "common_connection_fail".localized(), preferredStyle: UIAlertControllerStyle.alert)
                    
                    let okAction = UIAlertAction(title: "common_ok".localized(), style: UIAlertActionStyle.default) { (result : UIAlertAction) -> Void in
                    }
                    alertController.addAction(okAction)
                    
                    self.present(alertController, animated: true, completion: nil)
                }
                
            })
        }])
        
        // Add buttons to dialog
        popup.addButtons([DefaultButton(title: "common_no".localized()) {
            }])
        
        // Present dialog
        self.present(popup, animated: true, completion: nil)
        
    }
    
    func showLogin(){
        CurrentCustomerInfo.shareInfo.customer = nil
        let homeStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = homeStoryboard.instantiateViewController(withIdentifier: "topNavi")
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.window!.rootViewController = vc
    }
    
    func logout(){
        //show error Message
        let alertController = UIAlertController(title: "customer_lb_logout".localized(), message: "customer_msg_logout_confirm".localized(), preferredStyle: UIAlertControllerStyle.alert)
        
        let okAction = UIAlertAction(title: "common_yes".localized(), style: UIAlertActionStyle.default) { (result : UIAlertAction) -> Void in
            
            //call login function
            APIService.sharedService.logOut( completed: { (result, error) in
                if result == true
                {
                    CurrentCustomerInfo.shareInfo.customer = nil
                    CurrentCustomerInfo.shareInfo.persistenceCurrentInfo()
                    let homeStoryboard = UIStoryboard(name: "Main", bundle: nil)
                    let tabBar = homeStoryboard.instantiateViewController(withIdentifier: "tabBar")
                    let appDelegate = UIApplication.shared.delegate as! AppDelegate
                    appDelegate.window!.rootViewController = tabBar
                    
                }else{
                    
                    let alertController = UIAlertController(title: "common_info".localized(), message: "common_connection_fail".localized(), preferredStyle: UIAlertControllerStyle.alert)
                    
                    let okAction = UIAlertAction(title: "common_ok".localized(), style: UIAlertActionStyle.default) { (result : UIAlertAction) -> Void in
                        print("OK")
                    }
                    alertController.addAction(okAction)
                    
                    self.present(alertController, animated: true, completion: nil)
                }
                
            })
        }
        
        let cancelAction = UIAlertAction(title: "common_no".localized(), style: UIAlertActionStyle.default) { (result : UIAlertAction) -> Void in
            alertController.dismiss(animated: true, completion: nil)
            
        }
        
        alertController.addAction(okAction)
        alertController.addAction(cancelAction)
        self.present(alertController, animated: true, completion: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.title = "other_lb_other".localized()
        self.navigationController?.isNavigationBarHidden = false
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        tableView.reloadData()
        self.navigationController?.isNavigationBarHidden = false
    }
}
