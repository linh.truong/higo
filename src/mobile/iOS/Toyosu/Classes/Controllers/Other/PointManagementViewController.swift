//
//  PointManagementViewController.swift
//  Toyosu
//
//  Created by Linh Trương on 11/10/16.
//  Copyright © 2016 IBS. All rights reserved.
//

import UIKit
import Localize_Swift
import SVProgressHUD
import StatefulViewController
import AVFoundation
import PopupDialog

private let PAGINATION_LIMIT : Int = 6

class PointManagementViewController: UIViewController, QRCodeReaderViewControllerDelegate, UITabBarControllerDelegate,
StatefulViewController {
    
    // get outlet
    @IBOutlet weak var viewPointBg: UIView!
    @IBOutlet weak var lbCustomerCurrentPoint: UILabel!
    @IBOutlet weak var lbCurrentPoint: UILabel!
    @IBOutlet weak var btnScanQRCode: UIBarButtonItem!
    @IBOutlet weak var tblPrizes: UITableView!
    @IBOutlet weak var viewPointMain: UIView!
    @IBOutlet weak var viewPointMainCons: NSLayoutConstraint!
    
    // define variable
    var url: URL? = nil
    let dateFormatter = DateFormatter()
    let refreshControl = UIRefreshControl()
    var QRResult: String?
    var allPrize : [Prize] = []
    var willViewDetailPrize: Prize?
    var totalPrizes : Int = 0
    var currentPage : Int = 0
    var isLoadingMore = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setPrizeInit()
        
        // Reload localized after change
        NotificationCenter.default.addObserver(self, selector: #selector(PointManagementViewController.refreshLanguage), name: NSNotification.Name(LCLLanguageChangeNotification), object: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        getCustomerPoint()
    }
    
    func setPrizeInit() {
        edgesForExtendedLayout = []
        // Set navigation title localized
        navigationItem.title = "other_lb_point_management".localized()
        lbCustomerCurrentPoint.text = "other_prize_lb_cus_current_point".localized()
        
        viewPointBg.layer.cornerRadius = 10
        viewPointBg.layer.masksToBounds = true
        
        tblPrizes.delegate = self
        tblPrizes.dataSource = self
        
        // Format date time to Long stype display
        dateFormatter.dateStyle = .long
        
        // Refresh home list
        refreshControl.addTarget(self, action: #selector(PointManagementViewController.refresh(_:)), for: UIControlEvents.valueChanged)
        tblPrizes.addSubview(refreshControl)
        
        // Execute function load prize list data
        refreshWithIndi()
        
        // Empty data
        setupErrorViewState()
        setupEmptyViewState()
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    func refreshLanguage () {
        // Set navigation title localized
        navigationItem.title = "other_lb_point_management".localized()
        lbCustomerCurrentPoint.text = "other_prize_lb_cus_current_point".localized()
        // Reload data table view after changed localized
        tblPrizes.reloadData()
        getCustomerPoint()
    }
    
    func setupErrorViewState() {
        let failureView = ErrorView(frame: tblPrizes.frame)
        failureView.tapGestureRecognizer.addTarget(self, action: #selector(PointManagementViewController.refreshWithIndi))
        errorView = failureView
    }
    
    func setupEmptyViewState() {
        let emptyView = EmptyView(frame: tblPrizes.frame)
        errorView = emptyView
    }
    
    func refresh(_ sender: AnyObject) {
        loadPrizeWithPage(0)
    }
    
    func refreshWithIndi() {
        SVProgressHUD.show()
        loadPrizeWithPage(0)
    }
    
    func finishRefreshing() {
        SVProgressHUD.dismiss()
        tblPrizes.reloadData()
        refreshControl.endRefreshing()
        isLoadingMore = false
    }
    
    func loadPrizeWithPage(_ page : Int) {
        currentPage = page
        APIService.sharedService.getPrizeList(PAGINATION_LIMIT, currentPage * PAGINATION_LIMIT, completed: { [weak self] (result, code, message, rsTotal, rsPrize) in
            
            guard rsTotal > 0 else {
                SVProgressHUD.dismiss()
                self?.transitionViewStates(loading: false, error: NSError(domain: "error_no_data".localized(), code: 1, userInfo: nil), animated: false, completion: nil)
                return
            }
            
            guard result else {
                SVProgressHUD.dismiss()
                self?.transitionViewStates(loading: false, error: NSError(domain: "error_internet_unavailable_title".localized(), code: 1, userInfo: nil), animated: false, completion: nil)
                return
            }
            
            guard code != ErrorCode.ERROR_CODE_FAIL else {
                SVProgressHUD.dismiss()
                self?.transitionViewStates(loading: false, error: NSError(domain: "error_server_fail".localized(), code: 1, userInfo: nil), animated: false, completion: nil)
                return
            }
            
            guard code != ErrorCode.ERROR_CODE_NOT_FOUND else {
                SVProgressHUD.dismiss()
                self?.transitionViewStates(loading: false, error: NSError(domain: "error_unknown".localized(), code: 1, userInfo: nil), animated: false, completion: nil)
                return
            }
            // load home data
            self?.endLoading(error: nil)
            self?.finishCallApiWithPrizes(rsPrize, rsTotal)
        })
    }
    
    func loadMorePrize() {
        
        if totalPrizes > allPrize.count {
            self.endLoading(error: nil)
            isLoadingMore = true
            loadPrizeWithPage(currentPage + 1)
        } else {
            self.transitionViewStates(loading: false, error: NSError(domain: "error_internet_unavailable_title".localized(), code: 1, userInfo: nil), animated: false, completion: nil)
        }
        
    }
    
    func finishCallApiWithPrizes(_ prizeList: [Prize]?, _ total: Int) {
        
        guard let prizes = prizeList else {
            self.transitionViewStates(loading: false, error: NSError(domain: "error_server_fail".localized(), code: 1, userInfo: nil), animated: false, completion: nil)
            finishRefreshing()
            return
        }
        
        self.endLoading(error: nil)
        
        if (isLoadingMore) {
            for pr in prizes {
                let currentReg = pr.current_register ?? 0
                let quantity = pr.quantity ?? 100
                if currentReg < quantity {
                    self.allPrize.append(pr)
                }
                
                guard self.allPrize.count > 0 else {
                    SVProgressHUD.dismiss()
                    self.transitionViewStates(loading: false, error: NSError(domain: "error_no_data".localized(), code: 1, userInfo: nil), animated: false, completion: nil)
                    return
                }
                
            }
        } else {
            allPrize = prizes
        }
        
        totalPrizes = total
        finishRefreshing()
    }
    
    // Init reader
    lazy var reader = QRCodeReaderViewController(builder: QRCodeReaderViewControllerBuilder {
        $0.reader = QRCodeReader(metadataObjectTypes: [AVMetadataObjectTypeQRCode])
        $0.showTorchButton = true
    })
    
    // Start QRCode Reader
    func startQRCodeReader(){
        if QRCodeReader.supportsMetadataObjectTypes() {
            reader.completionBlock = { (result: QRCodeReaderResult?) in
                if let result = result {
                    self.QRResult = "\(result.value)"
                }
            }
            present(reader, animated: true, completion: nil)
        }
        else {
            
            let title = "common_info".localized()
            let message = "other_qr_code_msg_not_support".localized()
            
            // Create the dialog
            let popup = PopupDialog(
                title: title,
                message: message,
                buttonAlignment: .horizontal,
                transitionStyle: .zoomIn,
                gestureDismissal: true
            ) {}
            
            // Add buttons to dialog
            popup.addButtons([DefaultButton(title: "common_ok".localized()) {
                }])
            
            // Present dialog
            self.present(popup, animated: true, completion: nil)
        }
    }
    
    @IBAction func refreshPoint(_ sender: UIButton) {
        getCustomerPoint()
        refreshWithIndi()
    }
 
    func getCustomerPoint() {
        
        SVProgressHUD.show()
        
        if let customer = CurrentCustomerInfo.shareInfo.loadCustomerInfo() {
            if let cus_id = customer.id {
                if let authToken = CurrentCustomerInfo.shareInfo.getToken() {
                    
                    APIService.sharedService.getCustomerById(cus_id, authToken) { (result, error, rsCustomer) in
                        guard result else {
                            self.transitionViewStates(loading: false, error: NSError(domain: "error_no_data".localized(), code: 1, userInfo: nil), animated: false, completion: nil)
                            return
                        }
                        
                        self.endLoading(error: nil)
                        
                        if let newPoint = rsCustomer?.current_point {
                            self.lbCurrentPoint.text = newPoint.toString()
                        }
                        
                        SVProgressHUD.dismiss()
                        
                        CurrentCustomerInfo.shareInfo.saveCustomerMyPage(rsCustomer)
                    }
                }
            }
        } else {
            SVProgressHUD.dismiss()
            self.transitionViewStates(loading: false, error: NSError(domain: "error_server_fail".localized(), code: 1, userInfo: nil), animated: false, completion: nil)
        }
    }
    
    // update point after scan QRCode
    func updateCustomerPoint(){
        if let customer = CurrentCustomerInfo.shareInfo.loadCustomerInfo() {
            if let cusId = customer.id {
                if let authToken = CurrentCustomerInfo.shareInfo.getToken() {
                    APIService.sharedService.updateQRPoint(QRResult!, cusId, authToken) { (result, error, msg, rsPoint, rsResPoint) in
                        if result == true
                        {
                            let title = "common_info".localized()
                            
                            if error == ErrorCode.ERROR_CODE_SUCCESS
                            {
                                customer.current_point = rsPoint
                                CurrentCustomerInfo.shareInfo.saveCustomerInfo(customer)
                                
                                self.dismiss(animated: true, completion: nil)
                                
                                if let addPoint = rsResPoint {
                                    let message = String(format: NSLocalizedString("other_prize_got_point".localized(), comment: ""), "\(addPoint)")
                                    
                                    // Create the dialog
                                    let popup = PopupDialog(
                                        title: title,
                                        message: message,
                                        buttonAlignment: .horizontal,
                                        transitionStyle: .zoomIn,
                                        gestureDismissal: true
                                    ) {}
                                    
                                    // Add buttons to dialog
                                    popup.addButtons([DefaultButton(title: "common_ok".localized()) {
                                        }])
                                    
                                    // Present dialog
                                    self.present(popup, animated: true, completion: nil)
                                    
                                    self.getCustomerPoint()
                                } else {
                                    let message = "error_server_fail".localized()
                                    
                                    // Create the dialog
                                    let popup = PopupDialog(
                                        title: title,
                                        message: message,
                                        buttonAlignment: .horizontal,
                                        transitionStyle: .zoomIn,
                                        gestureDismissal: true
                                    ) {}
                                    
                                    // Add buttons to dialog
                                    popup.addButtons([DefaultButton(title: "common_ok".localized()) {
                                        }])
                                    
                                    // Present dialog
                                    self.present(popup, animated: true, completion: nil)
                                }
                            }
                            
                            if error == ErrorCode.ERROR_CODE_FAIL
                            {
                                self.dismiss(animated: true, completion: nil)
                                let message = "other_qr_code_used_expired".localized()
                                
                                // Create the dialog
                                let popup = PopupDialog(
                                    title: title,
                                    message: message,
                                    buttonAlignment: .horizontal,
                                    transitionStyle: .zoomIn,
                                    gestureDismissal: true
                                ) {}
                                
                                // Add buttons to dialog
                                popup.addButtons([DefaultButton(title: "common_ok".localized()) {
                                    }])
                                
                                // Present dialog
                                self.present(popup, animated: true, completion: nil)
                            }
                            
                            if error == ErrorCode.ERROR_CODE_NOT_FOUND
                            {
                                self.dismiss(animated: true, completion: nil)
                                
                                let message = "other_qr_code_not_exist".localized()
                                
                                // Create the dialog
                                let popup = PopupDialog(
                                    title: title,
                                    message: message,
                                    buttonAlignment: .horizontal,
                                    transitionStyle: .zoomIn,
                                    gestureDismissal: true
                                ) {}
                                
                                // Add buttons to dialog
                                popup.addButtons([DefaultButton(title: "common_ok".localized()) {
                                    }])
                                
                                // Present dialog
                                self.present(popup, animated: true, completion: nil)
                                
                            }
                        }
                        
                    }
                }
            }
        } else {
            showRequiredLoginPopupForQRCode()
        }
    }
    
    func showRequiredLoginPopupForQRCode() {
        // Prepare data for popup
        let title = "common_info".localized()
        let message = "other_qr_code_msg_login_required".localized()
        
        // Create the dialog
        let popup = PopupDialog(
            title: title,
            message: message,
            buttonAlignment: .horizontal,
            transitionStyle: .zoomIn,
            gestureDismissal: true
        ) {}
        
        // Add buttons to dialog
        popup.addButtons([DefaultButton(title: "common_ok".localized()) {
                CurrentCustomerInfo.shareInfo.customer = nil
                let homeStoryboard = UIStoryboard(name: "Main", bundle: nil)
                let vc = homeStoryboard.instantiateViewController(withIdentifier: "Login")
                self.navigationController?.pushViewController(vc, animated: true)
            }])
        
        // Add buttons to dialog
        popup.addButtons([DefaultButton(title: "common_cancel".localized()) {
            }])
        
        // Present dialog
        self.present(popup, animated: true, completion: nil)
        
    }
    
    @IBAction func btnQRReader(_ sender: Any) {
        
        if QRCodeReader.supportsMetadataObjectTypes() {
            self.reader.delegate = self
        }
        
        // Only registered user can use this function
        if CurrentCustomerInfo.shareInfo.customer == nil
        {
            showRequiredLoginPopupForQRCode()
            
        }
        else{
            //if app authorized -> start QR
            if AVCaptureDevice.authorizationStatus(forMediaType: AVMediaTypeVideo) ==  AVAuthorizationStatus.authorized
            {
                startQRCodeReader()
            }
            else
            {
                // Permission check
                AVCaptureDevice.requestAccess(forMediaType: AVMediaTypeVideo, completionHandler: { (granted :Bool) -> Void in
                    if granted == true {
                        self.reader.delegate = self
                        self.startQRCodeReader()
                    }
                    else{
                        
                        let alertController = UIAlertController (title: "common_info".localized(), message: "other_qr_code_app_camera_required".localized(), preferredStyle: .alert)
                        
                        let settingsAction = UIAlertAction(title: "Settings", style: .default) { (_) -> Void in
                            guard let settingsUrl = URL(string: UIApplicationOpenSettingsURLString) else {
                                return
                            }
                            
                            if UIApplication.shared.canOpenURL(settingsUrl) {
                                if #available(iOS 10.0, *) {
                                    UIApplication.shared.open(settingsUrl, completionHandler: { (success) in
                                        print("Settings opened: \(success)") // Prints true
                                    })
                                } else {
                                    if let appSettings = NSURL(string: UIApplicationOpenSettingsURLString) {
                                        UIApplication.shared.openURL(appSettings as URL)
                                    }                                }
                            }
                        }
                        alertController.addAction(settingsAction)
                        let cancelAction = UIAlertAction(title: "common_cancel".localized(), style: .default, handler: nil)
                        alertController.addAction(cancelAction)
                        
                        self.present(alertController, animated: true, completion: nil)
                        
                        
                    }
                });
            }
        }
    }
    
    // MARK: - QRCodeReader Delegate Methods
    func reader(_ reader: QRCodeReaderViewController, didScanResult result: QRCodeReaderResult) {
        reader.stopScanning()
        updateCustomerPoint()
    }
    
    func reader(_ reader: QRCodeReaderViewController, didSwitchCamera newCaptureDevice: AVCaptureDeviceInput) {
        if let cameraName = newCaptureDevice.device.localizedName {
            print("Switching capturing to: \(cameraName)")
        }
    }
    
    func readerDidCancel(_ reader: QRCodeReaderViewController) {
        reader.stopScanning()
        dismiss(animated: true, completion: nil)
        
    }
}

extension PointManagementViewController : UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if allPrize.count < totalPrizes {
            return allPrize.count + 1
        } else {
            return allPrize.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let rowAtIndexPath = indexPath.row
        
        if rowAtIndexPath == allPrize.count {
            let loadMoreCell = tableView.dequeueReusableCell(withIdentifier: LoadMoreTableViewCell.cellID, for: indexPath) as! LoadMoreTableViewCell
            
            return loadMoreCell
        } else {
            let prizeCell = tableView.dequeueReusableCell(withIdentifier: PrizeListTableViewCell.cellID, for: indexPath) as! PrizeListTableViewCell
            
            prizeCell.configureCellWith(allPrize[rowAtIndexPath])
            
            return prizeCell
        }
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if (indexPath.row == allPrize.count) {
            loadMorePrize()
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let rowAtIndexPath = indexPath.row
        
        if rowAtIndexPath == allPrize.count {
            return 44.0
        } else {
            tableView.estimatedRowHeight = 120.0
            tableView.rowHeight = UITableViewAutomaticDimension
            return 120.0
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let rowAtIndexPath = indexPath.row
        
        if rowAtIndexPath < allPrize.count {
            willViewDetailPrize = self.allPrize[rowAtIndexPath]
            performSegue(withIdentifier: PrizeDetailViewController.segueID, sender: nil)
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if (segue.identifier == PrizeDetailViewController.segueID) {
            let prizeDetailVC = segue.destination as! PrizeDetailViewController
            prizeDetailVC.prize = willViewDetailPrize
        }
    }
}
