//
//  TNavigationController.swift
//  Toyosu
//
//  Created by Linh Trương on 10/10/16.
//  Copyright © 2016 IBS. All rights reserved.
//

import Foundation
import UIKit

class TNavigationController : UINavigationController {
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.isNavigationBarHidden = true
        self.navigationBar.tintColor = AppColor.PRIMARY_COLOR
        backNavLocalized()
    }
    
    func backNavLocalized() {
        let backItem = UIBarButtonItem()
        backItem.title = "common_back".localized()
        navigationItem.backBarButtonItem = backItem
    }
}
