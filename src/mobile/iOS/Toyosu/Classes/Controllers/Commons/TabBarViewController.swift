//
//  TabBarViewController.swift
//  Toyosu
//
//  Created by Linh Trương on 10/10/16.
//  Copyright © 2016 IBS. All rights reserved.
//

import UIKit
import Localize_Swift

class TabBarViewController: UITabBarController {
    
    override func loadView() {
        super.loadView()
        
        let homeStoryboard = UIStoryboard(name: "Home", bundle: nil)
        let homeNav = homeStoryboard.instantiateViewController(withIdentifier: "NavHome") as! TNavigationController
        
        let couponStoryboard = UIStoryboard(name: "Coupon", bundle: nil)
        let couponVc = couponStoryboard.instantiateViewController(withIdentifier: "NavCoupon") as! TNavigationController
        
        let prizeStoryboard = UIStoryboard(name: "Prize", bundle: nil)
        let prizeVc = prizeStoryboard.instantiateViewController(withIdentifier: "NavPrize") as! TNavigationController
        
        let newsStoryboard = UIStoryboard(name: "News", bundle: nil)
        let newsVc = newsStoryboard.instantiateViewController(withIdentifier: "NavNews") as! TNavigationController
        
//        let restaurantStoryboard = UIStoryboard(name: "RestaurantInfo", bundle: nil)
//        let restaurantVc = restaurantStoryboard.instantiateViewController(withIdentifier: "NavRestaurant") as! TNavigationController
        
        let othersStoryboard = UIStoryboard(name: "Others", bundle: nil)
        let othersVc = othersStoryboard.instantiateViewController(withIdentifier: "NavOthers") as! TNavigationController
        
        self.viewControllers = [homeNav, couponVc, prizeVc, newsVc, othersVc]
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setLocalized()
        
        // Reload localized after change
        NotificationCenter.default.addObserver(self, selector: #selector(TabBarViewController.setLocalized), name: NSNotification.Name(LCLLanguageChangeNotification), object: nil)
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    func setLocalized() {
        self.viewControllers?[0].title = "home_lb_title".localized()
        self.viewControllers?[1].title = "coupon_lb_title".localized()
        self.viewControllers?[2].title = "prize_lb_title".localized()
        self.viewControllers?[3].title = "news_lb_title".localized()
        self.viewControllers?[4].title = "other_lb_other".localized()
    }
    
    func showRelatedItem(_ relatedItems: CustomRelatedItems) {

        if let type = relatedItems.type {
            switch type {
            case .CouponType:
                let coupon = relatedItems.toCoupon()
                let sb = UIStoryboard(name: "Coupon", bundle: nil)
                let couponDetailVC = sb.instantiateViewController(withIdentifier: "CouponDetailViewController") as! CouponDetailViewController
                couponDetailVC.coupon = coupon
                selectedIndex = 1
                (viewControllers?[1] as? UINavigationController)?.pushViewController(couponDetailVC, animated: true)
                
                break
            case .FoodType:
//                let food = relatedItems.toFood()
//                let sb = UIStoryboard(name: "Menu", bundle: nil)
//                let menuDetailVC = sb.instantiateViewController(withIdentifier: "MenuDetailViewController") as! MenuDetailViewController
//                menuDetailVC.food = food
//                selectedIndex = 3
//                (viewControllers?[1] as? UINavigationController)?.pushViewController(menuDetailVC, animated: true)
//                
                break
            case .TopicType:
//                let topic = relatedItems.toTopic()
//                let sb = UIStoryboard(name: "Home", bundle: nil)
//                let homeDetailVC = sb.instantiateViewController(withIdentifier: "HomeDetailViewController") as! HomeDetailViewController
//                homeDetailVC.topic = topic
//                selectedIndex = 0
//                (viewControllers?[0] as? UINavigationController)?.pushViewController(homeDetailVC, animated: true)
                
                break
            }
        }
    
    }

}
