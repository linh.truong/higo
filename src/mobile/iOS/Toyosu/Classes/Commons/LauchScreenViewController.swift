//
//  LauchScreenViewController.swift
//  Roann
//
//  Created by Linh Trương on 4/10/17.
//  Copyright © 2017 IBS. All rights reserved.
//

import UIKit
import SVProgressHUD
import StatefulViewController

class LauchScreenViewController: UIViewController, StatefulViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        setupInitialViewState()
        setupErrorViewState()
        getSettingList()
    }
    
    func setupErrorViewState() {
        let failureView = ErrorView(frame: view.frame)
        failureView.tapGestureRecognizer.addTarget(self, action: #selector(LauchScreenViewController.getSettingList))
        errorView = failureView
    }

    func getSettingList(){
        
        APIService.sharedService.getSettingList(100, 0) { (result, code, mess, rsTotal, rsSetting) in
            guard rsTotal > 0 else {
                SVProgressHUD.dismiss()
                self.transitionViewStates(loading: false, error: NSError(domain: "error_no_data".localized(), code: 1, userInfo: nil), animated: false, completion: nil)
                return
            }
            
            guard result else {
                SVProgressHUD.dismiss()
                self.transitionViewStates(loading: false, error: NSError(domain: "error_internet_unavailable_title".localized(), code: 1, userInfo: nil), animated: false, completion: nil)
                return
            }
            
            guard code != ErrorCode.ERROR_CODE_FAIL else {
                SVProgressHUD.dismiss()
                self.transitionViewStates(loading: false, error: NSError(domain: "error_server_fail".localized(), code: 1, userInfo: nil), animated: false, completion: nil)
                return
            }
            
            guard code != ErrorCode.ERROR_CODE_NOT_FOUND else {
                SVProgressHUD.dismiss()
                self.transitionViewStates(loading: false, error: NSError(domain: "error_unknown".localized(), code: 1, userInfo: nil), animated: false, completion: nil)
                return
            }
            // load home data
            self.endLoading(error: nil)
            CurrentCustomerInfo.shareInfo.saveSettingInfo(rsSetting)
            
            let loginStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let loginVC = loginStoryboard.instantiateViewController(withIdentifier: "NaviLogin") as! UINavigationController
            let appDelegate = UIApplication.shared.delegate as! AppDelegate
            appDelegate.window!.rootViewController = loginVC
            
            if (CurrentCustomerInfo.shareInfo.customer != nil) {
                loginVC.viewControllers[0].performSegue(withIdentifier: "show_home_vc", sender: nil)
            }
        }
    }
}
