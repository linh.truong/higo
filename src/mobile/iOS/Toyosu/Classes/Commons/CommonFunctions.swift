//
//  AFHTTPNetworkingCommon.swift
//  Toyosu
//
//  Created by nguyen.trung.hau on 10/12/16.
//  Copyright © 2016 Linh Trương. All rights reserved.
//

import Foundation
import AFNetworking
import UIKit
import PopupDialog

struct CommonFunctions {
    static func checkSession(reponseObject: Any?, previousViewController: UIViewController){
        let reponseData = reponseObject as!NSDictionary
        let errorData = reponseData["error"] as! NSNumber
        let errorCode = errorData.intValue
        
        if  errorCode == 80 {
            let loginController = LoginController.makeLoginViewController(sessionExpired: true, previousViewController: previousViewController)
            let appDelegate = UIApplication.shared.delegate as! AppDelegate
            appDelegate.window!.rootViewController = loginController
        }
    }
    
    static func initPopup( title: String, message: String) -> PopupDialog {
        let title = title
        let message = message
        
        // Create the dialog
        let popup = PopupDialog(
            title: title,
            message: message,
            buttonAlignment: .horizontal,
            transitionStyle: .zoomIn,
            gestureDismissal: true
        ) {}
        
        return popup
    }
}
