//
//  Constant.swift
//  Toyosu
//
//  Created by Linh Trương on 10/4/16.
//  Copyright © 2016 IBS. All rights reserved.
//

import Foundation
import UIKit

struct Constant {
    
    static let APPLICATION_NAME = "roann style club"
    static let APPLICATION_VERSION = "1.4"
    static let APPLICATION_COPYRIGHT = "C&Marketing"
    static let CURRENCY_DOLLAR = "HK$"
    static let CURRENCY_DOLLAR_ONLY = "$"
    static let CURRENCY_MINUS = "%"
    static let DEFAULT_RESERVATION_STATUS = 1
    static let DEFAULT_CONTACT_STATUS = 1
    static let DEFAULT_CUSTOMER_PRIZE_STATUS = 1
    static let DEFAULT_PRIZE_STATUS_NOT_DELIVERY = 1
    static let DEFAULT_PRIZE_STATUS_RECEIVED_STATUS = 3
    static let DEFAULT_CUSTOMER_COUPON_STATUS = 1
    static let DEVICE_TYPE = 0
    static let RELATED_ITEMS_LIMIT = 30
    static let DEFAULT_SHOP_INFO_ID = 1
    static let REQUEST_TIMEOUT : TimeInterval = 10.0
    static let RESERVE_TIME_INTERVAL = 15
    static let SCREEN_MAX_BRIGHTNESS : CGFloat = 1.0
    static let CUSTOMER_MIN_AGE = 12
    static let CUSTOMER_MAX_AGE = 99
    static let DEFAULT_WEB_URL = "http://discoverjp.info"
    static let NIL_IMAGE : UIImage? = UIImage.from(color: UIColor(hexString: "#fafafa"))
    
}

struct ServerURL {
//    static let DEFAULT = "http://vl00080:6969"
//    static let DEFAULT = "http://10.108.17.13:6969"
//    static let DEFAULT = "http://stg.higo.mb.discoverjp.info"
     static let DEFAULT = "http://ope.higo.mb.discoverjp.info"
}

struct ScreenSize {
    static let MAIN_BOUNDS: CGRect = UIScreen.main.bounds
    static let SCREEN_WIDTH: CGFloat = UIScreen.main.bounds.width
    static let SCREEN_HEIGHT: CGFloat = UIScreen.main.bounds.height
}

struct AppColor {
    
    private struct Alpha {
        static let Opaque = CGFloat(1)
        static let SemiOpaque = CGFloat(0.8)
        static let SemiTransparent = CGFloat(0.5)
        static let Transparent = CGFloat(0.3)
    }
    
//    static let PRIMARY_COLOR = UIColor.init(red: 175/255.0, green: 54/255.0, blue: 41/255.0, alpha: Alpha.Opaque)
//    static let PRIMARY_COLOR = UIColor(hexString: "#AF3629")
    static let PRIMARY_COLOR = UIColor(hexString: "#A63932")
    static let WHITE_COLOR = UIColor.white
    static let BACKGROUND_COLOR = UIColor(hexString: "#EEEEEE")
    static let DARK_GRAY_COLOR = UIColor(hexString: "#585858")
    
    struct TextColors {
        static let Error = UIColor.red
        static let Success = UIColor(red: 0.1303, green: 0.9915, blue: 0.0233, alpha: Alpha.Opaque)
    }
}

struct ErrorCode {
    
    static let ERROR_CODE_SUCCESS = 0
    static let ERROR_CODE_FAIL = 1
    static let ERROR_CODE_NOT_FOUND = 2
    static let ERROR_CODE_LOGIN_USER_PWD_REQUIRED = 11
    static let ERROR_CODE_LOGIN_USER_INACTIVE = 12
    static let ERROR_CODE_LOGIN_USER_NOT_PERMISSION = 13
    static let ERROR_CODE_LOGIN_REQUIRED = 18
    static let ERROR_CODE_LOGIN_FAIL = 19
    static let ERROR_CODE_USER_INSERT_EXIST = 31
    static let ERROR_CODE_USER_LACK_POINT = 37
    static let ERROR_CODE_EMPTY_STOCK = 38
    static let SIMPLY_BOOK_FULL_TABLE = 6969
    
}

struct MemberLevel {
    static let SILVER = 1000
    static let GOLD = 3000
    static let DIAMOND = 7000
}

struct ShopInfo {
    static let ID = 1
    static let CODE = "ROANN0001"
}

struct SimplyBook {
    static let ADDITIONAL_NUMBER_OF_ADULT = "Number of Adult"
    static let ADDITIONAL_NUMBER_OF_CHILD = "Number of Infant"
    static let ADDITIONAL_NUMBER_OF_CHILD2 = "Number of Elementary school student"
    static let ADDITIONAL_COMMENT = "Comment"
    static let EVENT_ID_DEFAULT = "1"
    static let NUMBER_OF_ADULT_DEFAULT = 1
    static let NUMBER_OF_CHILD_DEFAULT = 0
    static let MAX_SEAT = 99
}

struct AppSetting {
    static let SETTING_PRIVACY_POLICY = "privacy_policy"
    static let SETTING_TERM_AND_CONDITION = "terms_and_condition"
    static let SETTING_WEBSITE_URL = "website_url"
    static let SETTING_ABOUT_PRIZE = "about_prize"
    static let SETTING_ABOUT_POINT = "about_point"
    static let SETTING_BOOKING_TANDC = "booking_tandc"
    static let SETTING_PRIZE_START_DATE = "prize_start_date"
    static let SETTING_PRIZE_END_DATE = "prize_end_date"
    static let SETTING_BENEFIT_TC = "benefit_tandc"
}

struct LinkPointAndPize {
    static let English = "http://discoverjp.info/en/shop/points-and-prizes"
    static let Chinese = "http://discoverjp.info/zh/shop/points-and-prizes"
}
