//
//  BaseViewController.swift
//  Roann
//
//  Created by Linh Trương on 3/9/17.
//  Copyright © 2017 IBS. All rights reserved.
//

import UIKit

class BaseViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        self.view.backgroundColor = AppColor.BACKGROUND_COLOR
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
