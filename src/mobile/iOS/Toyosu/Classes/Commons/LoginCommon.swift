//
//  AFHTTPNetworkingCommon.swift
//  Toyosu
//
//  Created by nguyen.trung.hau on 10/12/16.
//  Copyright © 2016 Linh Trương. All rights reserved.
//

import Foundation
import AFNetworking

class LoginCommon {
    static func checkSession(reponseObject: Any?, previousVewController: UIViewController){
        let reponseData = reponseObject as!NSDictionary
        let errorData = reponseData["error"] as! NSNumber
        let errorCode = errorData.intValue
        
        if  errorCode == 80 {
            let loginController = LoginController.makeLoginViewController(sessionExpired: true, previousViewController: previousVewController)
            let appDelegate = UIApplication.shared.delegate as! AppDelegate
            appDelegate.window!.rootViewController = loginController            

        }

    }
    
   
    
}
