//
//  SimplyBookAdditional.swift
//  Roann
//
//  Created by Linh Trương on 3/20/17.
//  Copyright © 2017 IBS. All rights reserved.
//

import Foundation
import ObjectMapper

class SimplyBookAdditional: NSObject, Mappable {
    
    var id: String?
    var title: String?
    var name: String?
    var values: String?
    var unitDefault: String?
    
    override init () {}
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        id <- map["id"]
        title <- map["title"]
        name <- map["name"]
        values <- map["values"]
        unitDefault <- map["default"]
    }
}
