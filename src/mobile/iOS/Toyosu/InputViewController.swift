//
//  InputViewController.swift
//  Demo
//
//  Created by Hang Kim Duyen on 9/13/15.
//  Copyright (c) 2015 Hang Kim Duyen. All rights reserved.
//

import UIKit
class InputViewController: UIViewController, UITextFieldDelegate, UITextViewDelegate, UIGestureRecognizerDelegate {
    
    var tapGesture:UITapGestureRecognizer!
    var scvInputScroll:UIScrollView!
    var myTextField:UITextField!
    var myTextView:UITextView!
    
    override func awakeFromNib() {
        registerKeyboardNotifications()
        
        tapGesture = UITapGestureRecognizer(target: self, action: #selector(InputViewController.resignCurrentTextfield))
        tapGesture.delegate = self
        
        self.view.addGestureRecognizer(tapGesture)
    }
    
    override func viewDidAppear(_ animated: Bool) {
//        self.view.frame = self.parentViewController!.view.frame
        
    }
    
    deinit
    {
        removeKeyboardNotifications()
    }
    
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldReceive touch: UITouch) -> Bool {
        return true
    }

    func resignCurrentTextfield()
    {
        if(myTextField != nil)
        {
            myTextField.resignFirstResponder()
        }
        
        if(myTextView != nil)
        {
            myTextView.resignFirstResponder()
        }
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        myTextField = textField
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        myTextField = nil
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        myTextView = textView
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        myTextView = textView
    }
    
    func registerKeyboardNotifications()
    {
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardFrameChanged(notification:)), name: NSNotification.Name.UIKeyboardWillChangeFrame, object: nil)
    }

    func removeKeyboardNotifications()
    {
        NotificationCenter.default.removeObserver(self, name:NSNotification.Name.UIKeyboardDidChangeFrame, object: nil)
    }
    
    func keyboardFrameChanged(notification:NSNotification)
    {
        let userInfo = notification.userInfo!
        let keyboardSize = (userInfo[UIKeyboardFrameEndUserInfoKey] as! NSValue).cgRectValue
        let keyboardFrameBySelf = self.view.convert(keyboardSize, from: nil)
        let visibleHeight = keyboardFrameBySelf.origin.y - self.scvInputScroll.frame.origin.y
        if(visibleHeight < self.scvInputScroll.frame.size.height)
        {

            self.scvInputScroll.contentInset = UIEdgeInsetsMake(0, 0, self.scvInputScroll.frame.size.height - visibleHeight, 0)
        }else
        {
            self.scvInputScroll.contentInset = UIEdgeInsetsMake(64, 0, 0, 0)

        }
        
        scrollToFirstResponder()
    }
    
    func scrollToFirstResponder()
    {
        if(myTextField != nil)
        {
            let frame = self.scvInputScroll.convert(myTextField.frame, from: myTextField.superview)
            self.scvInputScroll.scrollRectToVisible(frame, animated: true)
        }
        
        if(myTextView != nil)
        {
            let frame = self.scvInputScroll.convert(myTextView.frame, from: myTextView.superview)
            self.scvInputScroll.scrollRectToVisible(frame, animated: true)
        }
       
    }

}
