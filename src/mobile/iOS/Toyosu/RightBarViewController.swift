//
//  RightBarViewController.swift
//  Toyosu
//
//  Created by Linh Truong on 12/29/16.
//  Copyright © 2016 IBS. All rights reserved.
//

import UIKit
import Localize_Swift

class RightBarViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        setBarButtonItem()
        self.view.backgroundColor = AppColor.BACKGROUND_COLOR
        
        NotificationCenter.default.addObserver(self, selector: #selector(RightBarViewController.refresh), name: NSNotification.Name(LCLLanguageChangeNotification), object: nil)
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    func refresh(){
        setBarButtonItem()
    }
    
    func setBarButtonItem() {
        let button : UIButton = UIButton.init(type: .custom)
        button.setBackgroundImage(UIImage(named: "btn_book"), for: .normal)
        button.layer.cornerRadius = 5
        button.addTarget(self, action: #selector(ShopInfoListViewController.showBookingNowScreen), for: .touchUpInside)
        button.frame = CGRect(x: 0, y: 0, width: 35, height: 30)
        let barButton = UIBarButtonItem(customView: button)
        self.navigationItem.rightBarButtonItem = barButton
    }
    
    func showBookingNowScreen() {
        let storyboard = UIStoryboard(name: "Coupon", bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: "SimplyBookViewController") as! SimplyBookViewController
        self.show(controller, sender: nil)
    }

}
