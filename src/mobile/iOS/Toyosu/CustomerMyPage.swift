//
//  CustomerMyPage.swift
//  Roann
//
//  Created by Linh Trương on 3/14/17.
//  Copyright © 2017 IBS. All rights reserved.
//

import Foundation
import Foundation
import ObjectMapper

typealias CustomersDictionary = [String : AnyObject]

class CustomerMyPage: Mappable
{
    // Variable
    var id: Int?
    var area: Int?
    var number: Int?
    var name: String?
    var age: Int?
    var birthday: Date?
    var sex: Int?
    var address: String?
    var gps_data: String?
    var email: String?
    var phone: String?
    var current_point: Int?
    var total_point: Int?
    var is_mail_notify: Int?
    var is_sms_notify: Int?
    var last_transaction_date: Date?
    var mem_status_name: String?
    var mem_status_name_cn: String?
    var mem_status_next_name: String?
    var mem_status_next_name_cn: String?
    var mem_status_next_level_point : Int?
    var mem_status_content: String?
    var mem_status_content_cn: String?
    var mem_status_short_content: String?
    var mem_status_short_content_cn: String?
    var mem_status_short_content_2: String?
    var mem_status_short_content_2_cn: String?
    var mem_status_content_special: String?
    var mem_status_content_special_cn: String?
    var mem_status_goal_point: Int?
    var mem_status_remain_point: Int?
    var mem_status_image_path: String?
    var mem_status_hex_color: String?
    var mem_status_hex_color_benefit_1: String?
    var mem_status_hex_color_benefit_2: String?
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        id    <- map["id"]
        area    <- map["area"]
        birthday <- (map["birthday"], ISO8601DateTransform())
        number <- map["number"]
        name      <- map["name"]
        age    <- map["age"]
        sex  <- map["sex"]
        address  <- map["address"]
        gps_data     <- map["gps_data"]
        email    <-  map["email"]
        phone    <-  map["phone"]
        current_point <- map["current_point"]
        total_point <- map["total_point"]
        is_sms_notify   <-  map["is_sms_notify"]
        is_mail_notify   <-  map["is_mail_notify"]
        last_transaction_date <- (map["last_transaction_date"], ISO8601DateTransform())
        mem_status_name   <-  map["mem_status_name"]
        mem_status_name_cn   <-  map["mem_status_name_cn"]
        mem_status_next_name   <-  map["mem_status_next_name"]
        mem_status_next_name_cn   <-  map["mem_status_next_name_cn"]
        mem_status_next_level_point   <-  map["mem_status_next_level_point"]
        mem_status_content   <-  map["mem_status_content"]
        mem_status_content_cn   <-  map["mem_status_content_cn"]
        mem_status_short_content   <-  map["mem_status_short_content"]
        mem_status_short_content_cn   <-  map["mem_status_short_content_cn"]
        mem_status_short_content_2   <-  map["mem_status_short_content_2"]
        mem_status_short_content_2_cn   <-  map["mem_status_short_content_2_cn"]
        mem_status_content_special   <-  map["mem_status_content_special"]
        mem_status_content_special_cn   <-  map["mem_status_content_special_cn"]
        mem_status_goal_point   <-  map["mem_status_goal_point"]
        mem_status_remain_point   <-  map["mem_status_remain_point"]
        mem_status_image_path   <-  map["mem_status_image_path"]
        mem_status_hex_color   <-  map["mem_status_hex_color"]
        mem_status_hex_color_benefit_1   <-  map["mem_status_hex_color_benefit_1"]
        mem_status_hex_color_benefit_2   <-  map["mem_status_hex_color_benefit_2"]
    }
    
}
