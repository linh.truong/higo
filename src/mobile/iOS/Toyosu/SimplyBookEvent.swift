//
//  SimplyBookEvent.swift
//  Roann
//
//  Created by Linh Trương on 3/20/17.
//  Copyright © 2017 IBS. All rights reserved.
//

import Foundation
import ObjectMapper

class SimplyBookEvent: NSObject, Mappable {
    
    var id: String?
    var name: String?
    var duration: String?
    
    override init () {}
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        id <- map["id"]
        name <- map["name"]
        duration <- map["duration"]
    }
}
