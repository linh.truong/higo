//
//  InstagramModel.swift
//  Roann
//
//  Created by Linh Trương on 4/18/17.
//  Copyright © 2017 IBS. All rights reserved.
//

import Foundation
import ObjectMapper

class InstagramModel: NSObject, Mappable
{
    // Variable
    var link: String?
    var user: InstaUser?
    var caption: InstaCaption?
    var likes: InstaLikes?
    var comments: InstaComments?
    var created_time: String?
    var images: InstaImages?
    var location: InstaLocation?
    
    override init () {}
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        link <- map["link"]
        user <- map["user"]
        caption <- map["caption"]
        likes <- map["likes"]
        comments <- map["comments"]
        created_time <- map["created_time"]
        images <- map["images"]
        location <- map["location"]
    }
}

class InstaUser : NSObject, Mappable {
    
    var profile_picture: String?
    var username: String?
    
    override init () {}
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        profile_picture <- map["profile_picture"]
        username <- map["username"]
    }
}

class InstaCaption : NSObject, Mappable {
    
    var text: String?
    var created_time: String?
    
    override init () {}
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        text <- map["text"]
    }
}

class InstaLikes : NSObject, Mappable {
    
    var count: Int?
    
    override init () {}
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        count <- map["count"]
    }
}

class InstaComments : NSObject, Mappable {
    
    var count: Int?
    
    override init () {}
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        count <- map["count"]
    }
}

class InstaImages : NSObject, Mappable {
    
    var standard_resolution: InstaImageResolution?
    
    override init () {}
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        standard_resolution <- map["standard_resolution"]
    }
}

class InstaImageResolution : NSObject, Mappable {
    
    var url: String?
    var height: Int?
    var width: Int?
    
    override init () {}
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        url <- map["url"]
        height <- map["height"]
        width <- map["width"]
    }
}

class InstaLocation : NSObject, Mappable {
    
    var name: String?
    
    override init () {}
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        name <- map["name"]
    }
}
