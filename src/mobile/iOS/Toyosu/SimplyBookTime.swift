//
//  SimplyBookTime.swift
//  Roann
//
//  Created by Linh Trương on 3/21/17.
//  Copyright © 2017 IBS. All rights reserved.
//

import Foundation
import ObjectMapper

class SimplyBookTime: NSObject, Mappable {
    
    var from: String?
    var to: String?
    
    override init () {}
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        from <- map["from"]
        to <- map["to"]
    }
}
