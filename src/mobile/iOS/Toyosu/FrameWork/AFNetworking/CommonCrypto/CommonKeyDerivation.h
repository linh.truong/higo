<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
<title>CommonKeyDerivation.h</title>
<style type="text/css">
.enscript-comment { font-style: italic; color: rgb(178,34,34); }
.enscript-function-name { font-weight: bold; color: rgb(0,0,255); }
.enscript-variable-name { font-weight: bold; color: rgb(184,134,11); }
.enscript-keyword { font-weight: bold; color: rgb(160,32,240); }
.enscript-reference { font-weight: bold; color: rgb(95,158,160); }
.enscript-string { font-weight: bold; color: rgb(188,143,143); }
.enscript-builtin { font-weight: bold; color: rgb(218,112,214); }
.enscript-type { font-weight: bold; color: rgb(34,139,34); }
.enscript-highlight { text-decoration: underline; color: 0; }
</style>
</head>
<body id="top">
<h1 style="margin:8px;" id="f1">CommonKeyDerivation.h&nbsp;&nbsp;&nbsp;<span style="font-weight: normal; font-size: 0.5em;">[<a href="?txt">plain text</a>]</span></h1>
<hr/>
<div></div>
<pre>
<span class="enscript-comment">/*
 * Copyright (c) 2010 Apple Inc. All Rights Reserved.
 * 
 * @APPLE_LICENSE_HEADER_START@
 * 
 * This file contains Original Code and/or Modifications of Original Code
 * as defined in and that are subject to the Apple Public Source License
 * Version 2.0 (the 'License'). You may not use this file except in
 * compliance with the License. Please obtain a copy of the License at
 * <a href="http://www.opensource.apple.com/apsl/">http://www.opensource.apple.com/apsl/</a> and read it before using this
 * file.
 * 
 * The Original Code and all software distributed under the License are
 * distributed on an 'AS IS' basis, WITHOUT WARRANTY OF ANY KIND, EITHER
 * EXPRESS OR IMPLIED, AND APPLE HEREBY DISCLAIMS ALL SUCH WARRANTIES,
 * INCLUDING WITHOUT LIMITATION, ANY WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE, QUIET ENJOYMENT OR NON-INFRINGEMENT.
 * Please see the License for the specific language governing rights and
 * limitations under the License.
 * 
 * @APPLE_LICENSE_HEADER_END@
 */</span>

#<span class="enscript-reference">ifndef</span> <span class="enscript-variable-name">_CC_PBKDF_H_</span>
#<span class="enscript-reference">define</span> <span class="enscript-variable-name">_CC_PBKDF_H_</span>

#<span class="enscript-reference">include</span> <span class="enscript-string">&lt;sys/types.h&gt;</span>
#<span class="enscript-reference">include</span> <span class="enscript-string">&lt;sys/param.h&gt;</span>

#<span class="enscript-reference">include</span> <span class="enscript-string">&lt;string.h&gt;</span>
#<span class="enscript-reference">include</span> <span class="enscript-string">&lt;limits.h&gt;</span>
#<span class="enscript-reference">include</span> <span class="enscript-string">&lt;stdlib.h&gt;</span>

#<span class="enscript-reference">include</span> <span class="enscript-string">&lt;Availability.h&gt;</span>

#<span class="enscript-reference">include</span> <span class="enscript-string">&lt;CommonCrypto/CommonDigest.h&gt;</span>
#<span class="enscript-reference">include</span> <span class="enscript-string">&lt;CommonCrypto/CommonHMAC.h&gt;</span>


#<span class="enscript-reference">ifdef</span> <span class="enscript-variable-name">__cplusplus</span>
<span class="enscript-type">extern</span> <span class="enscript-string">&quot;C&quot;</span> {
#<span class="enscript-reference">endif</span>
    
<span class="enscript-type">enum</span> {
    kCCPBKDF2 = 2,
};


<span class="enscript-type">typedef</span> uint32_t CCPBKDFAlgorithm;


<span class="enscript-type">enum</span> {
    kCCPRFHmacAlgSHA1 = 1, 
    kCCPRFHmacAlgSHA224 = 2,
    kCCPRFHmacAlgSHA256 = 3,
    kCCPRFHmacAlgSHA384 = 4,
    kCCPRFHmacAlgSHA512 = 5,
};


<span class="enscript-type">typedef</span> uint32_t CCPseudoRandomAlgorithm;

<span class="enscript-comment">/*
 
 @function  CCKeyDerivationPBKDF
 @abstract  Derive a key from a text password/passphrase
 
 @param algorithm       Currently only PBKDF2 is available via kCCPBKDF2
 @param password        The text password used as input to the derivation 
                        function.  The actual octets present in this string 
                        will be used with no additional processing.  It's 
                        extremely important that the same encoding and 
                        normalization be used each time this routine is 
                        called if the same key is  expected to be derived.
 @param passwordLen     The length of the text password in bytes.
 @param salt            The salt byte values used as input to the derivation 
                        function.
 @param saltLen         The length of the salt in bytes.
 @param prf             The Pseudo Random Algorithm to use for the derivation 
                        iterations.
 @param rounds          The number of rounds of the Pseudo Random Algorithm 
                        to use.
 @param derivedKey      The resulting derived key produced by the function.  
                        The space for this must be provided by the caller.
 @param derivedKeyLen   The expected length of the derived key in bytes.
 
 @discussion The following values are used to designate the PRF:
 
 * kCCPRFHmacAlgSHA1
 * kCCPRFHmacAlgSHA224
 * kCCPRFHmacAlgSHA256
 * kCCPRFHmacAlgSHA384
 * kCCPRFHmacAlgSHA512
 
 @result     kCCParamError can result from bad values for the password, salt, 
                and unwrapped key pointers as well as a bad value for the prf function.
 
 */</span>

<span class="enscript-type">int</span> 
<span class="enscript-function-name">CCKeyDerivationPBKDF</span>( CCPBKDFAlgorithm algorithm, <span class="enscript-type">const</span> <span class="enscript-type">char</span> *password, size_t passwordLen,
                      <span class="enscript-type">const</span> uint8_t *salt, size_t saltLen,
                      CCPseudoRandomAlgorithm prf, uint rounds, 
                      uint8_t *derivedKey, size_t derivedKeyLen)
                      __OSX_AVAILABLE_STARTING(__MAC_10_7, __IPHONE_NA);

<span class="enscript-comment">/*
 * All lengths are in bytes - not bits.
 */</span>

<span class="enscript-comment">/*
 
 @function  CCCalibratePBKDF
 @abstract  Determine the number of PRF rounds to use for a specific delay on 
            the current platform.  
 @param algorithm       Currently only PBKDF2 is available via kCCPBKDF2
 @param passwordLen     The length of the text password in bytes.
 @param saltLen         The length of the salt in bytes.
 @param prf             The Pseudo Random Algorithm to use for the derivation 
                        iterations.
 @param derivedKeyLen   The expected length of the derived key in bytes.
 @param msec            The targetted duration we want to achieve for a key 
                        derivation with these parameters.
 
 @result the number of iterations to use for the desired processing time.
 
 */</span>

uint
<span class="enscript-function-name">CCCalibratePBKDF</span>(CCPBKDFAlgorithm algorithm, size_t passwordLen, size_t saltLen,
                 CCPseudoRandomAlgorithm prf, size_t derivedKeyLen, uint32_t msec)
                 __OSX_AVAILABLE_STARTING(__MAC_10_7, __IPHONE_NA);

#<span class="enscript-reference">ifdef</span> <span class="enscript-variable-name">__cplusplus</span>
}
#<span class="enscript-reference">endif</span>

#<span class="enscript-reference">endif</span>  <span class="enscript-comment">/* _CC_PBKDF_H_ */</span>
</pre>
<hr />
</body></html>