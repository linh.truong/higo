<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
<title>CommonHMAC.h</title>
<style type="text/css">
.enscript-comment { font-style: italic; color: rgb(178,34,34); }
.enscript-function-name { font-weight: bold; color: rgb(0,0,255); }
.enscript-variable-name { font-weight: bold; color: rgb(184,134,11); }
.enscript-keyword { font-weight: bold; color: rgb(160,32,240); }
.enscript-reference { font-weight: bold; color: rgb(95,158,160); }
.enscript-string { font-weight: bold; color: rgb(188,143,143); }
.enscript-builtin { font-weight: bold; color: rgb(218,112,214); }
.enscript-type { font-weight: bold; color: rgb(34,139,34); }
.enscript-highlight { text-decoration: underline; color: 0; }
</style>
</head>
<body id="top">
<h1 style="margin:8px;" id="f1">CommonHMAC.h&nbsp;&nbsp;&nbsp;<span style="font-weight: normal; font-size: 0.5em;">[<a href="?txt">plain text</a>]</span></h1>
<hr/>
<div></div>
<pre>
<span class="enscript-comment">/* 
 * Copyright (c) 2004 Apple Computer, Inc. All Rights Reserved.
 * 
 * @APPLE_LICENSE_HEADER_START@
 * 
 * This file contains Original Code and/or Modifications of Original Code
 * as defined in and that are subject to the Apple Public Source License
 * Version 2.0 (the 'License'). You may not use this file except in
 * compliance with the License. Please obtain a copy of the License at
 * <a href="http://www.opensource.apple.com/apsl/">http://www.opensource.apple.com/apsl/</a> and read it before using this
 * file.
 * 
 * The Original Code and all software distributed under the License are
 * distributed on an 'AS IS' basis, WITHOUT WARRANTY OF ANY KIND, EITHER
 * EXPRESS OR IMPLIED, AND APPLE HEREBY DISCLAIMS ALL SUCH WARRANTIES,
 * INCLUDING WITHOUT LIMITATION, ANY WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE, QUIET ENJOYMENT OR NON-INFRINGEMENT.
 * Please see the License for the specific language governing rights and
 * limitations under the License.
 * 
 * @APPLE_LICENSE_HEADER_END@
 */</span>

<span class="enscript-comment">/*!
    @header     CommonHMAC.h
    @abstract   Keyed Message Authentication Code (HMAC) functions.
 */</span>
 
#<span class="enscript-reference">ifndef</span> <span class="enscript-variable-name">_CC_COMMON_HMAC_H_</span>
#<span class="enscript-reference">define</span> <span class="enscript-variable-name">_CC_COMMON_HMAC_H_</span>

#<span class="enscript-reference">include</span> <span class="enscript-string">&lt;CommonCrypto/CommonDigest.h&gt;</span>
#<span class="enscript-reference">include</span> <span class="enscript-string">&lt;sys/types.h&gt;</span>
#<span class="enscript-reference">include</span> <span class="enscript-string">&lt;Availability.h&gt;</span>

#<span class="enscript-reference">ifdef</span> <span class="enscript-variable-name">__cplusplus</span>
<span class="enscript-type">extern</span> <span class="enscript-string">&quot;C&quot;</span> {
#<span class="enscript-reference">endif</span>

<span class="enscript-comment">/*!
    @enum       CCHmacAlgorithm
    @abstract   Algorithms implemented in this module.

    @constant   kCCHmacAlgSHA1      HMAC with SHA1 digest
    @constant   kCCHmacAlgMD5       HMAC with MD5 digest
    @constant   kCCHmacAlgSHA256    HMAC with SHA256 digest
    @constant   kCCHmacAlgSHA384    HMAC with SHA384 digest
    @constant   kCCHmacAlgSHA512    HMAC with SHA512 digest
    @constant   kCCHmacAlgSHA224    HMAC with SHA224 digest
 */</span>
<span class="enscript-type">enum</span> {
    kCCHmacAlgSHA1,
    kCCHmacAlgMD5,
    kCCHmacAlgSHA256,
    kCCHmacAlgSHA384,
    kCCHmacAlgSHA512,
    kCCHmacAlgSHA224
};
<span class="enscript-type">typedef</span> uint32_t CCHmacAlgorithm;

<span class="enscript-comment">/*!
    @typedef    CCHmacContext
    @abstract   HMAC context. 
 */</span>
#<span class="enscript-reference">define</span> <span class="enscript-variable-name">CC_HMAC_CONTEXT_SIZE</span>    96  
<span class="enscript-type">typedef</span> <span class="enscript-type">struct</span> {
    uint32_t            ctx[CC_HMAC_CONTEXT_SIZE];
} CCHmacContext;

<span class="enscript-comment">/*!
    @function   CCHmacInit
    @abstract   Initialize an CCHmacContext with provided raw key bytes.
    
    @param      ctx         An HMAC context.
    @param      algorithm   HMAC algorithm to perform. 
    @param      key         Raw key bytes.
    @param      keyLength   Length of raw key bytes; can be any 
                            length including zero. 
 */</span>
<span class="enscript-type">void</span> <span class="enscript-function-name">CCHmacInit</span>(
    CCHmacContext *ctx, 
    CCHmacAlgorithm algorithm,  
    <span class="enscript-type">const</span> <span class="enscript-type">void</span> *key,
    size_t keyLength)
    __OSX_AVAILABLE_STARTING(__MAC_10_4, __IPHONE_NA);
            
    
<span class="enscript-comment">/*!
    @function   CCHmacUpdate
    @abstract   Process some data.
    
    @param      ctx         An HMAC context.
    @param      data        Data to process.
    @param      dataLength  Length of data to process, in bytes.
    
    @discussion This can be called multiple times.
 */</span>
<span class="enscript-type">void</span> <span class="enscript-function-name">CCHmacUpdate</span>(
    CCHmacContext *ctx, 
    <span class="enscript-type">const</span> <span class="enscript-type">void</span> *data,
    size_t dataLength)
    __OSX_AVAILABLE_STARTING(__MAC_10_4, __IPHONE_NA);

    
<span class="enscript-comment">/*!
    @function   CCHmacFinal
    @abstract   Obtain the final Message Authentication Code.
    
    @param      ctx         An HMAC context.
    @param      macOut      Destination of MAC; allocated by caller. 
    
    @discussion The length of the MAC written to *macOut is the same as 
                the digest length associated with the HMAC algorithm:
    
                kCCHmacSHA1 : CC_SHA1_DIGEST_LENGTH
                
                kCCHmacMD5  : CC_MD5_DIGEST_LENGTH
 */</span>
<span class="enscript-type">void</span> <span class="enscript-function-name">CCHmacFinal</span>(
    CCHmacContext *ctx, 
    <span class="enscript-type">void</span> *macOut)
    __OSX_AVAILABLE_STARTING(__MAC_10_4, __IPHONE_NA);

    
<span class="enscript-comment">/*
 * Stateless, one-shot HMAC function. 
 * Output is written to caller-supplied buffer, as in CCHmacFinal().
 */</span>
<span class="enscript-type">void</span> <span class="enscript-function-name">CCHmac</span>(
    CCHmacAlgorithm algorithm,  <span class="enscript-comment">/* kCCHmacSHA1, kCCHmacMD5 */</span>
    <span class="enscript-type">const</span> <span class="enscript-type">void</span> *key,
    size_t keyLength,           <span class="enscript-comment">/* length of key in bytes */</span>
    <span class="enscript-type">const</span> <span class="enscript-type">void</span> *data,
    size_t dataLength,          <span class="enscript-comment">/* length of data in bytes */</span>
    <span class="enscript-type">void</span> *macOut)               <span class="enscript-comment">/* MAC written here */</span>
    __OSX_AVAILABLE_STARTING(__MAC_10_4, __IPHONE_NA);

#<span class="enscript-reference">ifdef</span> <span class="enscript-variable-name">__cplusplus</span>
}
#<span class="enscript-reference">endif</span>

#<span class="enscript-reference">endif</span>  <span class="enscript-comment">/* _CC_COMMON_HMAC_H_ */</span>
</pre>
<hr />
</body></html>