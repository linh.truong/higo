<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
<title>CommonSymmetricKeywrap.h</title>
<style type="text/css">
.enscript-comment { font-style: italic; color: rgb(178,34,34); }
.enscript-function-name { font-weight: bold; color: rgb(0,0,255); }
.enscript-variable-name { font-weight: bold; color: rgb(184,134,11); }
.enscript-keyword { font-weight: bold; color: rgb(160,32,240); }
.enscript-reference { font-weight: bold; color: rgb(95,158,160); }
.enscript-string { font-weight: bold; color: rgb(188,143,143); }
.enscript-builtin { font-weight: bold; color: rgb(218,112,214); }
.enscript-type { font-weight: bold; color: rgb(34,139,34); }
.enscript-highlight { text-decoration: underline; color: 0; }
</style>
</head>
<body id="top">
<h1 style="margin:8px;" id="f1">CommonSymmetricKeywrap.h&nbsp;&nbsp;&nbsp;<span style="font-weight: normal; font-size: 0.5em;">[<a href="?txt">plain text</a>]</span></h1>
<hr/>
<div></div>
<pre>
<span class="enscript-comment">/*
 * Copyright (c) 2010 Apple Inc. All Rights Reserved.
 * 
 * @APPLE_LICENSE_HEADER_START@
 * 
 * This file contains Original Code and/or Modifications of Original Code
 * as defined in and that are subject to the Apple Public Source License
 * Version 2.0 (the 'License'). You may not use this file except in
 * compliance with the License. Please obtain a copy of the License at
 * <a href="http://www.opensource.apple.com/apsl/">http://www.opensource.apple.com/apsl/</a> and read it before using this
 * file.
 * 
 * The Original Code and all software distributed under the License are
 * distributed on an 'AS IS' basis, WITHOUT WARRANTY OF ANY KIND, EITHER
 * EXPRESS OR IMPLIED, AND APPLE HEREBY DISCLAIMS ALL SUCH WARRANTIES,
 * INCLUDING WITHOUT LIMITATION, ANY WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE, QUIET ENJOYMENT OR NON-INFRINGEMENT.
 * Please see the License for the specific language governing rights and
 * limitations under the License.
 * 
 * @APPLE_LICENSE_HEADER_END@
 */</span>

#<span class="enscript-reference">ifndef</span> <span class="enscript-variable-name">_CC_SYMKEYWRAP_H_</span>
#<span class="enscript-reference">define</span> <span class="enscript-variable-name">_CC_SYMKEYWRAP_H_</span>

#<span class="enscript-reference">include</span> <span class="enscript-string">&lt;sys/types.h&gt;</span>
#<span class="enscript-reference">include</span> <span class="enscript-string">&lt;sys/param.h&gt;</span>
#<span class="enscript-reference">include</span> <span class="enscript-string">&lt;stdint.h&gt;</span>

#<span class="enscript-reference">include</span> <span class="enscript-string">&lt;string.h&gt;</span>
#<span class="enscript-reference">include</span> <span class="enscript-string">&lt;limits.h&gt;</span>
#<span class="enscript-reference">include</span> <span class="enscript-string">&lt;stdlib.h&gt;</span>
#<span class="enscript-reference">include</span> <span class="enscript-string">&lt;Availability.h&gt;</span>


#<span class="enscript-reference">ifdef</span> <span class="enscript-variable-name">__cplusplus</span>
<span class="enscript-type">extern</span> <span class="enscript-string">&quot;C&quot;</span> {
#<span class="enscript-reference">endif</span>
    
<span class="enscript-type">enum</span> {
    kCCWRAPAES = 1,
};

<span class="enscript-type">extern</span> <span class="enscript-type">const</span> uint8_t *CCrfc3394_iv  <span class="enscript-function-name">__OSX_AVAILABLE_STARTING</span>(__MAC_10_7, __IPHONE_NA);
<span class="enscript-type">extern</span> <span class="enscript-type">const</span> size_t CCrfc3394_ivLen  <span class="enscript-function-name">__OSX_AVAILABLE_STARTING</span>(__MAC_10_7, __IPHONE_NA);

<span class="enscript-type">typedef</span> uint32_t CCWrappingAlgorithm;

<span class="enscript-comment">/*!
 @function  CCSymmetricKeyWrap
 @abstract  Wrap a symmetric key with a Key Encryption Key (KEK).  
 
 @param algorithm       Currently only AES Keywrapping (rfc3394) is available 
                        via kCCWRAPAES
 @param iv              The initialization value to be used.  CCrfc3394_iv is 
                        available as a constant for the standard IV to use.
 @param ivLen           The length of the initialization value to be used.  
                        CCrfc3394_ivLen is available as a constant for the 
                        standard IV to use.
 @param kek             The Key Encryption Key to be used to wrap the raw key.
 @param kekLen          The length of the KEK in bytes.
 @param rawKey          The raw key bytes to be wrapped.
 @param rawKeyLen       The length of the key in bytes.
 @param wrappedKey      The resulting wrapped key produced by the function.  
                        The space for this must be provided by the caller.
 @param wrappedKeyLen   The length of the wrapped key in bytes.
 
 @discussion The algorithm chosen is determined by the algorithm parameter 
                and the size of the key being wrapped (ie aes128 for 128 bit 
                keys).

 @result    kCCBufferTooSmall indicates insufficent space in the wrappedKey 
            buffer. 
            kCCParamError can result from bad values for the kek, rawKey, and 
            wrappedKey key pointers.
 */</span>

<span class="enscript-type">int</span>  
<span class="enscript-function-name">CCSymmetricKeyWrap</span>( CCWrappingAlgorithm algorithm, 
                   <span class="enscript-type">const</span> uint8_t *iv, <span class="enscript-type">const</span> size_t ivLen,
                   <span class="enscript-type">const</span> uint8_t *kek, size_t kekLen,
                   <span class="enscript-type">const</span> uint8_t *rawKey, size_t rawKeyLen,
                   uint8_t  *wrappedKey, size_t *wrappedKeyLen)
                   __OSX_AVAILABLE_STARTING(__MAC_10_7, __IPHONE_NA);

<span class="enscript-comment">/*!
 @function  CCSymmetricKeyUnwrap
 @abstract  Unwrap a symmetric key with a Key Encryption Key (KEK).  
 
 @param algorithm       Currently only AES Keywrapping (rfc3394) is available 
                        via kCCWRAPAES
 @param iv              The initialization value to be used.  CCrfc3394_iv is 
                        available as a constant for the standard IV to use.
 @param ivLen           The length of the initialization value to be used.  
                        CCrfc3394_ivLen is available as a constant for the 
                        standard IV to use.
 @param kekLen          The length of the KEK in bytes.
 @param wrappedKey      The wrapped key bytes.
 @param wrappedKeyLen   The length of the wrapped key in bytes.
 @param rawKey          The resulting raw key bytes. The space for this must 
                        be provided by the caller.
 @param rawKeyLen       The length of the raw key in bytes.
 
 @discussion The algorithm chosen is determined by the algorithm parameter 
                and the size of the key being wrapped (ie aes128 for 128 bit 
                keys).
 
 @result    kCCBufferTooSmall indicates insufficent space in the rawKey buffer. 
            kCCParamError can result from bad values for the kek, rawKey, and 
            wrappedKey key pointers.
 */</span>


<span class="enscript-type">int</span>  
<span class="enscript-function-name">CCSymmetricKeyUnwrap</span>( CCWrappingAlgorithm algorithm,
                     <span class="enscript-type">const</span> uint8_t *iv, <span class="enscript-type">const</span> size_t ivLen,
                     <span class="enscript-type">const</span> uint8_t *kek, size_t kekLen,
                     <span class="enscript-type">const</span> uint8_t  *wrappedKey, size_t wrappedKeyLen,
                     uint8_t  *rawKey, size_t *rawKeyLen)
                     __OSX_AVAILABLE_STARTING(__MAC_10_7, __IPHONE_NA);

<span class="enscript-comment">/*!
 @function  CCSymmetricWrappedSize
 @abstract  Determine the buffer size required to hold a key wrapped with 
            CCAESKeyWrap().  
 
 @param     algorithm       Currently only AES Keywrapping (rfc3394) is 
                            available via kCCWRAPAES
 @param     rawKeyLen       The length of the key in bytes.
 @result    The length of the resulting wrapped key.
 */</span>

size_t
<span class="enscript-function-name">CCSymmetricWrappedSize</span>( CCWrappingAlgorithm algorithm, size_t rawKeyLen)
<span class="enscript-function-name">__OSX_AVAILABLE_STARTING</span>(__MAC_10_7, __IPHONE_NA);

<span class="enscript-comment">/*!
 @function  CCSymmetricUnwrappedSize
 @abstract  Determine the buffer size required to hold a key unwrapped with 
            CCAESKeyUnwrap().  
 
 @param     algorithm       Currently only AES Keywrapping (rfc3394) is 
                            available via kCCWRAPAES
 @param     wrappedKeyLen   The length of the wrapped key in bytes.
 @result    The length of the resulting raw key.
 */</span>

size_t
<span class="enscript-function-name">CCSymmetricUnwrappedSize</span>( CCWrappingAlgorithm algorithm, size_t wrappedKeyLen)
<span class="enscript-function-name">__OSX_AVAILABLE_STARTING</span>(__MAC_10_7, __IPHONE_NA);

#<span class="enscript-reference">ifdef</span> <span class="enscript-variable-name">__cplusplus</span>
}
#<span class="enscript-reference">endif</span>

#<span class="enscript-reference">endif</span> <span class="enscript-comment">/* _CC_SYMKEYWRAP_H_ */</span>
</pre>
<hr />
</body></html>