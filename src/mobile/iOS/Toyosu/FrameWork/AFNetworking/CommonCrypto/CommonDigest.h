<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
<title>CommonDigest.h</title>
<style type="text/css">
.enscript-comment { font-style: italic; color: rgb(178,34,34); }
.enscript-function-name { font-weight: bold; color: rgb(0,0,255); }
.enscript-variable-name { font-weight: bold; color: rgb(184,134,11); }
.enscript-keyword { font-weight: bold; color: rgb(160,32,240); }
.enscript-reference { font-weight: bold; color: rgb(95,158,160); }
.enscript-string { font-weight: bold; color: rgb(188,143,143); }
.enscript-builtin { font-weight: bold; color: rgb(218,112,214); }
.enscript-type { font-weight: bold; color: rgb(34,139,34); }
.enscript-highlight { text-decoration: underline; color: 0; }
</style>
</head>
<body id="top">
<h1 style="margin:8px;" id="f1">CommonDigest.h&nbsp;&nbsp;&nbsp;<span style="font-weight: normal; font-size: 0.5em;">[<a href="?txt">plain text</a>]</span></h1>
<hr/>
<div></div>
<pre>
<span class="enscript-comment">/* 
 * Copyright (c) 2004 Apple Computer, Inc. All Rights Reserved.
 * 
 * @APPLE_LICENSE_HEADER_START@
 * 
 * This file contains Original Code and/or Modifications of Original Code
 * as defined in and that are subject to the Apple Public Source License
 * Version 2.0 (the 'License'). You may not use this file except in
 * compliance with the License. Please obtain a copy of the License at
 * <a href="http://www.opensource.apple.com/apsl/">http://www.opensource.apple.com/apsl/</a> and read it before using this
 * file.
 * 
 * The Original Code and all software distributed under the License are
 * distributed on an 'AS IS' basis, WITHOUT WARRANTY OF ANY KIND, EITHER
 * EXPRESS OR IMPLIED, AND APPLE HEREBY DISCLAIMS ALL SUCH WARRANTIES,
 * INCLUDING WITHOUT LIMITATION, ANY WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE, QUIET ENJOYMENT OR NON-INFRINGEMENT.
 * Please see the License for the specific language governing rights and
 * limitations under the License.
 * 
 * @APPLE_LICENSE_HEADER_END@
 */</span>

<span class="enscript-comment">/*
 * CommonDigest.h - common digest routines: MD2, MD4, MD5, SHA1.
 */</span>
 
#<span class="enscript-reference">ifndef</span> <span class="enscript-variable-name">_CC_COMMON_DIGEST_H_</span>
#<span class="enscript-reference">define</span> <span class="enscript-variable-name">_CC_COMMON_DIGEST_H_</span>

#<span class="enscript-reference">include</span> <span class="enscript-string">&lt;stdint.h&gt;</span>
#<span class="enscript-reference">include</span> <span class="enscript-string">&lt;Availability.h&gt;</span>

#<span class="enscript-reference">ifdef</span> <span class="enscript-variable-name">__cplusplus</span>
<span class="enscript-type">extern</span> <span class="enscript-string">&quot;C&quot;</span> {
#<span class="enscript-reference">endif</span>

<span class="enscript-comment">/*
 * For compatibility with legacy implementations, the *Init(), *Update(),
 * and *Final() functions declared here *always* return a value of 1 (one). 
 * This corresponds to &quot;success&quot; in the similar openssl implementations. 
 * There are no errors of any kind which can be, or are, reported here, 
 * so you can safely ignore the return values of all of these functions 
 * if you are implementing new code.
 *
 * The one-shot functions (CC_MD2(), CC_SHA1(), etc.) perform digest
 * calculation and place the result in the caller-supplied buffer
 * indicated by the md parameter. They return the md parameter.
 * Unlike the opensssl counterparts, these one-shot functions require
 * a non-NULL md pointer. Passing in NULL for the md parameter 
 * results in a NULL return and no digest calculation. 
 */</span>
 
<span class="enscript-type">typedef</span> uint32_t CC_LONG;       <span class="enscript-comment">/* 32 bit unsigned integer */</span>
<span class="enscript-type">typedef</span> uint64_t CC_LONG64;     <span class="enscript-comment">/* 64 bit unsigned integer */</span>

<span class="enscript-comment">/*** MD2 ***/</span>

#<span class="enscript-reference">define</span> <span class="enscript-variable-name">CC_MD2_DIGEST_LENGTH</span>    16          <span class="enscript-comment">/* digest length in bytes */</span>
#<span class="enscript-reference">define</span> <span class="enscript-variable-name">CC_MD2_BLOCK_BYTES</span>      64          <span class="enscript-comment">/* block size in bytes */</span>
#<span class="enscript-reference">define</span> <span class="enscript-variable-name">CC_MD2_BLOCK_LONG</span>       (CC_MD2_BLOCK_BYTES / sizeof(CC_LONG))

<span class="enscript-type">typedef</span> <span class="enscript-type">struct</span> CC_MD2state_st
{
    <span class="enscript-type">int</span> num;
    <span class="enscript-type">unsigned</span> <span class="enscript-type">char</span> data[CC_MD2_DIGEST_LENGTH];
    CC_LONG cksm[CC_MD2_BLOCK_LONG];
    CC_LONG state[CC_MD2_BLOCK_LONG];
} CC_MD2_CTX;

<span class="enscript-type">extern</span> <span class="enscript-type">int</span> <span class="enscript-function-name">CC_MD2_Init</span>(CC_MD2_CTX *c)
<span class="enscript-function-name">__OSX_AVAILABLE_STARTING</span>(__MAC_10_4, __IPHONE_NA);

<span class="enscript-type">extern</span> <span class="enscript-type">int</span> <span class="enscript-function-name">CC_MD2_Update</span>(CC_MD2_CTX *c, <span class="enscript-type">const</span> <span class="enscript-type">void</span> *data, CC_LONG len)
<span class="enscript-function-name">__OSX_AVAILABLE_STARTING</span>(__MAC_10_4, __IPHONE_NA);

<span class="enscript-type">extern</span> <span class="enscript-type">int</span> <span class="enscript-function-name">CC_MD2_Final</span>(<span class="enscript-type">unsigned</span> <span class="enscript-type">char</span> *md, CC_MD2_CTX *c)
<span class="enscript-function-name">__OSX_AVAILABLE_STARTING</span>(__MAC_10_4, __IPHONE_NA);

<span class="enscript-type">extern</span> <span class="enscript-type">unsigned</span> <span class="enscript-type">char</span> *<span class="enscript-function-name">CC_MD2</span>(<span class="enscript-type">const</span> <span class="enscript-type">void</span> *data, CC_LONG len, <span class="enscript-type">unsigned</span> <span class="enscript-type">char</span> *md)
<span class="enscript-function-name">__OSX_AVAILABLE_STARTING</span>(__MAC_10_4, __IPHONE_NA);

<span class="enscript-comment">/*** MD4 ***/</span>

#<span class="enscript-reference">define</span> <span class="enscript-variable-name">CC_MD4_DIGEST_LENGTH</span>    16          <span class="enscript-comment">/* digest length in bytes */</span>
#<span class="enscript-reference">define</span> <span class="enscript-variable-name">CC_MD4_BLOCK_BYTES</span>      64          <span class="enscript-comment">/* block size in bytes */</span>
#<span class="enscript-reference">define</span> <span class="enscript-variable-name">CC_MD4_BLOCK_LONG</span>       (CC_MD4_BLOCK_BYTES / sizeof(CC_LONG))

<span class="enscript-type">typedef</span> <span class="enscript-type">struct</span> CC_MD4state_st
{
    CC_LONG A,B,C,D;
    CC_LONG Nl,Nh;
    CC_LONG data[CC_MD4_BLOCK_LONG];
    <span class="enscript-type">int</span> num;
} CC_MD4_CTX;

<span class="enscript-type">extern</span> <span class="enscript-type">int</span> <span class="enscript-function-name">CC_MD4_Init</span>(CC_MD4_CTX *c)
<span class="enscript-function-name">__OSX_AVAILABLE_STARTING</span>(__MAC_10_4, __IPHONE_NA);

<span class="enscript-type">extern</span> <span class="enscript-type">int</span> <span class="enscript-function-name">CC_MD4_Update</span>(CC_MD4_CTX *c, <span class="enscript-type">const</span> <span class="enscript-type">void</span> *data, CC_LONG len)
<span class="enscript-function-name">__OSX_AVAILABLE_STARTING</span>(__MAC_10_4, __IPHONE_NA);

<span class="enscript-type">extern</span> <span class="enscript-type">int</span> <span class="enscript-function-name">CC_MD4_Final</span>(<span class="enscript-type">unsigned</span> <span class="enscript-type">char</span> *md, CC_MD4_CTX *c)
<span class="enscript-function-name">__OSX_AVAILABLE_STARTING</span>(__MAC_10_4, __IPHONE_NA);

<span class="enscript-type">extern</span> <span class="enscript-type">unsigned</span> <span class="enscript-type">char</span> *<span class="enscript-function-name">CC_MD4</span>(<span class="enscript-type">const</span> <span class="enscript-type">void</span> *data, CC_LONG len, <span class="enscript-type">unsigned</span> <span class="enscript-type">char</span> *md)
<span class="enscript-function-name">__OSX_AVAILABLE_STARTING</span>(__MAC_10_4, __IPHONE_NA);


<span class="enscript-comment">/*** MD5 ***/</span>

#<span class="enscript-reference">define</span> <span class="enscript-variable-name">CC_MD5_DIGEST_LENGTH</span>    16          <span class="enscript-comment">/* digest length in bytes */</span>
#<span class="enscript-reference">define</span> <span class="enscript-variable-name">CC_MD5_BLOCK_BYTES</span>      64          <span class="enscript-comment">/* block size in bytes */</span>
#<span class="enscript-reference">define</span> <span class="enscript-variable-name">CC_MD5_BLOCK_LONG</span>       (CC_MD5_BLOCK_BYTES / sizeof(CC_LONG))

<span class="enscript-type">typedef</span> <span class="enscript-type">struct</span> CC_MD5state_st
{
    CC_LONG A,B,C,D;
    CC_LONG Nl,Nh;
    CC_LONG data[CC_MD5_BLOCK_LONG];
    <span class="enscript-type">int</span> num;
} CC_MD5_CTX;

<span class="enscript-type">extern</span> <span class="enscript-type">int</span> <span class="enscript-function-name">CC_MD5_Init</span>(CC_MD5_CTX *c)
<span class="enscript-function-name">__OSX_AVAILABLE_STARTING</span>(__MAC_10_4, __IPHONE_NA);

<span class="enscript-type">extern</span> <span class="enscript-type">int</span> <span class="enscript-function-name">CC_MD5_Update</span>(CC_MD5_CTX *c, <span class="enscript-type">const</span> <span class="enscript-type">void</span> *data, CC_LONG len)
<span class="enscript-function-name">__OSX_AVAILABLE_STARTING</span>(__MAC_10_4, __IPHONE_NA);

<span class="enscript-type">extern</span> <span class="enscript-type">int</span> <span class="enscript-function-name">CC_MD5_Final</span>(<span class="enscript-type">unsigned</span> <span class="enscript-type">char</span> *md, CC_MD5_CTX *c)
<span class="enscript-function-name">__OSX_AVAILABLE_STARTING</span>(__MAC_10_4, __IPHONE_NA);

<span class="enscript-type">extern</span> <span class="enscript-type">unsigned</span> <span class="enscript-type">char</span> *<span class="enscript-function-name">CC_MD5</span>(<span class="enscript-type">const</span> <span class="enscript-type">void</span> *data, CC_LONG len, <span class="enscript-type">unsigned</span> <span class="enscript-type">char</span> *md)
<span class="enscript-function-name">__OSX_AVAILABLE_STARTING</span>(__MAC_10_4, __IPHONE_NA);


<span class="enscript-comment">/*** SHA1 ***/</span>

#<span class="enscript-reference">define</span> <span class="enscript-variable-name">CC_SHA1_DIGEST_LENGTH</span>   20          <span class="enscript-comment">/* digest length in bytes */</span>
#<span class="enscript-reference">define</span> <span class="enscript-variable-name">CC_SHA1_BLOCK_BYTES</span>     64          <span class="enscript-comment">/* block size in bytes */</span>
#<span class="enscript-reference">define</span> <span class="enscript-variable-name">CC_SHA1_BLOCK_LONG</span>      (CC_SHA1_BLOCK_BYTES / sizeof(CC_LONG))

<span class="enscript-type">typedef</span> <span class="enscript-type">struct</span> CC_SHA1state_st
{
    CC_LONG h0,h1,h2,h3,h4;
    CC_LONG Nl,Nh;
    CC_LONG data[CC_SHA1_BLOCK_LONG];
    <span class="enscript-type">int</span> num;
} CC_SHA1_CTX;

<span class="enscript-type">extern</span> <span class="enscript-type">int</span> <span class="enscript-function-name">CC_SHA1_Init</span>(CC_SHA1_CTX *c)
<span class="enscript-function-name">__OSX_AVAILABLE_STARTING</span>(__MAC_10_4, __IPHONE_NA);

<span class="enscript-type">extern</span> <span class="enscript-type">int</span> <span class="enscript-function-name">CC_SHA1_Update</span>(CC_SHA1_CTX *c, <span class="enscript-type">const</span> <span class="enscript-type">void</span> *data, CC_LONG len)
<span class="enscript-function-name">__OSX_AVAILABLE_STARTING</span>(__MAC_10_4, __IPHONE_NA);

<span class="enscript-type">extern</span> <span class="enscript-type">int</span> <span class="enscript-function-name">CC_SHA1_Final</span>(<span class="enscript-type">unsigned</span> <span class="enscript-type">char</span> *md, CC_SHA1_CTX *c)
<span class="enscript-function-name">__OSX_AVAILABLE_STARTING</span>(__MAC_10_4, __IPHONE_NA);

<span class="enscript-type">extern</span> <span class="enscript-type">unsigned</span> <span class="enscript-type">char</span> *<span class="enscript-function-name">CC_SHA1</span>(<span class="enscript-type">const</span> <span class="enscript-type">void</span> *data, CC_LONG len, <span class="enscript-type">unsigned</span> <span class="enscript-type">char</span> *md)
<span class="enscript-function-name">__OSX_AVAILABLE_STARTING</span>(__MAC_10_4, __IPHONE_NA);


<span class="enscript-comment">/*** SHA224 ***/</span>
#<span class="enscript-reference">define</span> <span class="enscript-variable-name">CC_SHA224_DIGEST_LENGTH</span>     28          <span class="enscript-comment">/* digest length in bytes */</span>
#<span class="enscript-reference">define</span> <span class="enscript-variable-name">CC_SHA224_BLOCK_BYTES</span>       64          <span class="enscript-comment">/* block size in bytes */</span>

<span class="enscript-comment">/* same context struct is used for SHA224 and SHA256 */</span>
<span class="enscript-type">typedef</span> <span class="enscript-type">struct</span> CC_SHA256state_st
{   CC_LONG count[2];
    CC_LONG hash[8];
    CC_LONG wbuf[16];
} CC_SHA256_CTX;

<span class="enscript-type">extern</span> <span class="enscript-type">int</span> <span class="enscript-function-name">CC_SHA224_Init</span>(CC_SHA256_CTX *c)
<span class="enscript-function-name">__OSX_AVAILABLE_STARTING</span>(__MAC_10_4, __IPHONE_NA);

<span class="enscript-type">extern</span> <span class="enscript-type">int</span> <span class="enscript-function-name">CC_SHA224_Update</span>(CC_SHA256_CTX *c, <span class="enscript-type">const</span> <span class="enscript-type">void</span> *data, CC_LONG len)
<span class="enscript-function-name">__OSX_AVAILABLE_STARTING</span>(__MAC_10_4, __IPHONE_NA);

<span class="enscript-type">extern</span> <span class="enscript-type">int</span> <span class="enscript-function-name">CC_SHA224_Final</span>(<span class="enscript-type">unsigned</span> <span class="enscript-type">char</span> *md, CC_SHA256_CTX *c)
<span class="enscript-function-name">__OSX_AVAILABLE_STARTING</span>(__MAC_10_4, __IPHONE_NA);

<span class="enscript-type">extern</span> <span class="enscript-type">unsigned</span> <span class="enscript-type">char</span> *<span class="enscript-function-name">CC_SHA224</span>(<span class="enscript-type">const</span> <span class="enscript-type">void</span> *data, CC_LONG len, <span class="enscript-type">unsigned</span> <span class="enscript-type">char</span> *md)
<span class="enscript-function-name">__OSX_AVAILABLE_STARTING</span>(__MAC_10_4, __IPHONE_NA);


<span class="enscript-comment">/*** SHA256 ***/</span>

#<span class="enscript-reference">define</span> <span class="enscript-variable-name">CC_SHA256_DIGEST_LENGTH</span>     32          <span class="enscript-comment">/* digest length in bytes */</span>
#<span class="enscript-reference">define</span> <span class="enscript-variable-name">CC_SHA256_BLOCK_BYTES</span>       64          <span class="enscript-comment">/* block size in bytes */</span>

<span class="enscript-type">extern</span> <span class="enscript-type">int</span> <span class="enscript-function-name">CC_SHA256_Init</span>(CC_SHA256_CTX *c)
<span class="enscript-function-name">__OSX_AVAILABLE_STARTING</span>(__MAC_10_4, __IPHONE_NA);

<span class="enscript-type">extern</span> <span class="enscript-type">int</span> <span class="enscript-function-name">CC_SHA256_Update</span>(CC_SHA256_CTX *c, <span class="enscript-type">const</span> <span class="enscript-type">void</span> *data, CC_LONG len)
<span class="enscript-function-name">__OSX_AVAILABLE_STARTING</span>(__MAC_10_4, __IPHONE_NA);

<span class="enscript-type">extern</span> <span class="enscript-type">int</span> <span class="enscript-function-name">CC_SHA256_Final</span>(<span class="enscript-type">unsigned</span> <span class="enscript-type">char</span> *md, CC_SHA256_CTX *c)
<span class="enscript-function-name">__OSX_AVAILABLE_STARTING</span>(__MAC_10_4, __IPHONE_NA);

<span class="enscript-type">extern</span> <span class="enscript-type">unsigned</span> <span class="enscript-type">char</span> *<span class="enscript-function-name">CC_SHA256</span>(<span class="enscript-type">const</span> <span class="enscript-type">void</span> *data, CC_LONG len, <span class="enscript-type">unsigned</span> <span class="enscript-type">char</span> *md)
<span class="enscript-function-name">__OSX_AVAILABLE_STARTING</span>(__MAC_10_4, __IPHONE_NA);


<span class="enscript-comment">/*** SHA384 ***/</span>

#<span class="enscript-reference">define</span> <span class="enscript-variable-name">CC_SHA384_DIGEST_LENGTH</span>     48          <span class="enscript-comment">/* digest length in bytes */</span>
#<span class="enscript-reference">define</span> <span class="enscript-variable-name">CC_SHA384_BLOCK_BYTES</span>      128          <span class="enscript-comment">/* block size in bytes */</span>

<span class="enscript-comment">/* same context struct is used for SHA384 and SHA512 */</span>
<span class="enscript-type">typedef</span> <span class="enscript-type">struct</span> CC_SHA512state_st
{   CC_LONG64 count[2];
    CC_LONG64 hash[8];
    CC_LONG64 wbuf[16];
} CC_SHA512_CTX;

<span class="enscript-type">extern</span> <span class="enscript-type">int</span> <span class="enscript-function-name">CC_SHA384_Init</span>(CC_SHA512_CTX *c)
<span class="enscript-function-name">__OSX_AVAILABLE_STARTING</span>(__MAC_10_4, __IPHONE_NA);

<span class="enscript-type">extern</span> <span class="enscript-type">int</span> <span class="enscript-function-name">CC_SHA384_Update</span>(CC_SHA512_CTX *c, <span class="enscript-type">const</span> <span class="enscript-type">void</span> *data, CC_LONG len)
<span class="enscript-function-name">__OSX_AVAILABLE_STARTING</span>(__MAC_10_4, __IPHONE_NA);

<span class="enscript-type">extern</span> <span class="enscript-type">int</span> <span class="enscript-function-name">CC_SHA384_Final</span>(<span class="enscript-type">unsigned</span> <span class="enscript-type">char</span> *md, CC_SHA512_CTX *c)
<span class="enscript-function-name">__OSX_AVAILABLE_STARTING</span>(__MAC_10_4, __IPHONE_NA);

<span class="enscript-type">extern</span> <span class="enscript-type">unsigned</span> <span class="enscript-type">char</span> *<span class="enscript-function-name">CC_SHA384</span>(<span class="enscript-type">const</span> <span class="enscript-type">void</span> *data, CC_LONG len, <span class="enscript-type">unsigned</span> <span class="enscript-type">char</span> *md)
<span class="enscript-function-name">__OSX_AVAILABLE_STARTING</span>(__MAC_10_4, __IPHONE_NA);


<span class="enscript-comment">/*** SHA512 ***/</span>

#<span class="enscript-reference">define</span> <span class="enscript-variable-name">CC_SHA512_DIGEST_LENGTH</span>     64          <span class="enscript-comment">/* digest length in bytes */</span>
#<span class="enscript-reference">define</span> <span class="enscript-variable-name">CC_SHA512_BLOCK_BYTES</span>      128          <span class="enscript-comment">/* block size in bytes */</span>

<span class="enscript-type">extern</span> <span class="enscript-type">int</span> <span class="enscript-function-name">CC_SHA512_Init</span>(CC_SHA512_CTX *c)
<span class="enscript-function-name">__OSX_AVAILABLE_STARTING</span>(__MAC_10_4, __IPHONE_NA);

<span class="enscript-type">extern</span> <span class="enscript-type">int</span> <span class="enscript-function-name">CC_SHA512_Update</span>(CC_SHA512_CTX *c, <span class="enscript-type">const</span> <span class="enscript-type">void</span> *data, CC_LONG len)
<span class="enscript-function-name">__OSX_AVAILABLE_STARTING</span>(__MAC_10_4, __IPHONE_NA);

<span class="enscript-type">extern</span> <span class="enscript-type">int</span> <span class="enscript-function-name">CC_SHA512_Final</span>(<span class="enscript-type">unsigned</span> <span class="enscript-type">char</span> *md, CC_SHA512_CTX *c)
<span class="enscript-function-name">__OSX_AVAILABLE_STARTING</span>(__MAC_10_4, __IPHONE_NA);

<span class="enscript-type">extern</span> <span class="enscript-type">unsigned</span> <span class="enscript-type">char</span> *<span class="enscript-function-name">CC_SHA512</span>(<span class="enscript-type">const</span> <span class="enscript-type">void</span> *data, CC_LONG len, <span class="enscript-type">unsigned</span> <span class="enscript-type">char</span> *md)
<span class="enscript-function-name">__OSX_AVAILABLE_STARTING</span>(__MAC_10_4, __IPHONE_NA);

<span class="enscript-comment">/*
 * To use the above digest functions with existing code which uses
 * the corresponding openssl functions, #define the symbol 
 * COMMON_DIGEST_FOR_OPENSSL in your client code (BEFORE including
 * this file), and simply link against libSystem (or System.framework)
 * instead of libcrypto.
 *
 * You can *NOT* mix and match functions operating on a given data
 * type from the two implementations; i.e., if you do a CC_MD5_Init()
 * on a CC_MD5_CTX object, do not assume that you can do an openssl-style
 * MD5_Update() on that same context.
 */</span>
 
#<span class="enscript-reference">ifdef</span>  <span class="enscript-variable-name">COMMON_DIGEST_FOR_OPENSSL</span>

#<span class="enscript-reference">define</span> <span class="enscript-variable-name">MD2_DIGEST_LENGTH</span>           CC_MD2_DIGEST_LENGTH
#<span class="enscript-reference">define</span> <span class="enscript-variable-name">MD2_CTX</span>                     CC_MD2_CTX
#<span class="enscript-reference">define</span> <span class="enscript-variable-name">MD2_Init</span>                    CC_MD2_Init
#<span class="enscript-reference">define</span> <span class="enscript-variable-name">MD2_Update</span>                  CC_MD2_Update
#<span class="enscript-reference">define</span> <span class="enscript-variable-name">MD2_Final</span>                   CC_MD2_Final

#<span class="enscript-reference">define</span> <span class="enscript-variable-name">MD4_DIGEST_LENGTH</span>           CC_MD4_DIGEST_LENGTH
#<span class="enscript-reference">define</span> <span class="enscript-variable-name">MD4_CTX</span>                     CC_MD4_CTX
#<span class="enscript-reference">define</span> <span class="enscript-variable-name">MD4_Init</span>                    CC_MD4_Init
#<span class="enscript-reference">define</span> <span class="enscript-variable-name">MD4_Update</span>                  CC_MD4_Update
#<span class="enscript-reference">define</span> <span class="enscript-variable-name">MD4_Final</span>                   CC_MD4_Final

#<span class="enscript-reference">define</span> <span class="enscript-variable-name">MD5_DIGEST_LENGTH</span>           CC_MD5_DIGEST_LENGTH
#<span class="enscript-reference">define</span> <span class="enscript-variable-name">MD5_CTX</span>                     CC_MD5_CTX
#<span class="enscript-reference">define</span> <span class="enscript-variable-name">MD5_Init</span>                    CC_MD5_Init
#<span class="enscript-reference">define</span> <span class="enscript-variable-name">MD5_Update</span>                  CC_MD5_Update
#<span class="enscript-reference">define</span> <span class="enscript-variable-name">MD5_Final</span>                   CC_MD5_Final

#<span class="enscript-reference">define</span> <span class="enscript-variable-name">SHA_DIGEST_LENGTH</span>           CC_SHA1_DIGEST_LENGTH
#<span class="enscript-reference">define</span> <span class="enscript-variable-name">SHA_CTX</span>                     CC_SHA1_CTX
#<span class="enscript-reference">define</span> <span class="enscript-variable-name">SHA1_Init</span>                   CC_SHA1_Init
#<span class="enscript-reference">define</span> <span class="enscript-variable-name">SHA1_Update</span>                 CC_SHA1_Update
#<span class="enscript-reference">define</span> <span class="enscript-variable-name">SHA1_Final</span>                  CC_SHA1_Final

#<span class="enscript-reference">define</span> <span class="enscript-variable-name">SHA224_DIGEST_LENGTH</span>        CC_SHA224_DIGEST_LENGTH
#<span class="enscript-reference">define</span> <span class="enscript-variable-name">SHA256_CTX</span>                  CC_SHA256_CTX
#<span class="enscript-reference">define</span> <span class="enscript-variable-name">SHA224_Init</span>                 CC_SHA224_Init
#<span class="enscript-reference">define</span> <span class="enscript-variable-name">SHA224_Update</span>               CC_SHA224_Update
#<span class="enscript-reference">define</span> <span class="enscript-variable-name">SHA224_Final</span>                CC_SHA224_Final

#<span class="enscript-reference">define</span> <span class="enscript-variable-name">SHA256_DIGEST_LENGTH</span>        CC_SHA256_DIGEST_LENGTH
#<span class="enscript-reference">define</span> <span class="enscript-variable-name">SHA256_Init</span>                 CC_SHA256_Init
#<span class="enscript-reference">define</span> <span class="enscript-variable-name">SHA256_Update</span>               CC_SHA256_Update
#<span class="enscript-reference">define</span> <span class="enscript-variable-name">SHA256_Final</span>                CC_SHA256_Final

#<span class="enscript-reference">define</span> <span class="enscript-variable-name">SHA384_DIGEST_LENGTH</span>        CC_SHA384_DIGEST_LENGTH
#<span class="enscript-reference">define</span> <span class="enscript-variable-name">SHA512_CTX</span>                  CC_SHA512_CTX
#<span class="enscript-reference">define</span> <span class="enscript-variable-name">SHA384_Init</span>                 CC_SHA384_Init
#<span class="enscript-reference">define</span> <span class="enscript-variable-name">SHA384_Update</span>               CC_SHA384_Update
#<span class="enscript-reference">define</span> <span class="enscript-variable-name">SHA384_Final</span>                CC_SHA384_Final

#<span class="enscript-reference">define</span> <span class="enscript-variable-name">SHA512_DIGEST_LENGTH</span>        CC_SHA512_DIGEST_LENGTH
#<span class="enscript-reference">define</span> <span class="enscript-variable-name">SHA512_Init</span>                 CC_SHA512_Init
#<span class="enscript-reference">define</span> <span class="enscript-variable-name">SHA512_Update</span>               CC_SHA512_Update
#<span class="enscript-reference">define</span> <span class="enscript-variable-name">SHA512_Final</span>                CC_SHA512_Final


#<span class="enscript-reference">endif</span>  <span class="enscript-comment">/* COMMON_DIGEST_FOR_OPENSSL */</span>

<span class="enscript-comment">/*
 * In a manner similar to that described above for openssl 
 * compatibility, these macros can be used to provide compatiblity 
 * with legacy implementations of MD5 using the interface defined 
 * in RFC 1321.
 */</span>
 
#<span class="enscript-reference">ifdef</span>  <span class="enscript-variable-name">COMMON_DIGEST_FOR_RFC_1321</span>

#<span class="enscript-reference">define</span> <span class="enscript-variable-name">MD5_CTX</span>                     CC_MD5_CTX
#<span class="enscript-reference">define</span> <span class="enscript-variable-name">MD5Init</span>                     CC_MD5_Init
#<span class="enscript-reference">define</span> <span class="enscript-variable-name">MD5Update</span>                   CC_MD5_Update
<span class="enscript-type">void</span> <span class="enscript-function-name">MD5Final</span> (<span class="enscript-type">unsigned</span> <span class="enscript-type">char</span> [16], MD5_CTX *)
<span class="enscript-function-name">__OSX_AVAILABLE_STARTING</span>(__MAC_10_4, __IPHONE_NA);

#<span class="enscript-reference">endif</span>  <span class="enscript-comment">/* COMMON_DIGEST_FOR_RFC_1321 */</span>

#<span class="enscript-reference">ifdef</span> <span class="enscript-variable-name">__cplusplus</span>
}
#<span class="enscript-reference">endif</span>

#<span class="enscript-reference">endif</span>  <span class="enscript-comment">/* _CC_COMMON_DIGEST_H_ */</span>
</pre>
<hr />
</body></html>