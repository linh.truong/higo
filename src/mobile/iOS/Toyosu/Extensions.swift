//
//  Extensions.swift
//  Roann
//
//  Created by Linh Trương on 3/6/17.
//  Copyright © 2017 IBS. All rights reserved.
//

import Foundation
import UIKit

extension UIColor {
    convenience init(hexString: String) {
        let hex = hexString.trimmingCharacters(in: CharacterSet.alphanumerics.inverted)
        var int = UInt32()
        Scanner(string: hex).scanHexInt32(&int)
        let a, r, g, b: UInt32
        switch hex.characters.count {
        case 3: // RGB (12-bit)
            (a, r, g, b) = (255, (int >> 8) * 17, (int >> 4 & 0xF) * 17, (int & 0xF) * 17)
        case 6: // RGB (24-bit)
            (a, r, g, b) = (255, int >> 16, int >> 8 & 0xFF, int & 0xFF)
        case 8: // ARGB (32-bit)
            (a, r, g, b) = (int >> 24, int >> 16 & 0xFF, int >> 8 & 0xFF, int & 0xFF)
        default:
            (a, r, g, b) = (255, 0, 0, 0)
        }
        self.init(red: CGFloat(r) / 255, green: CGFloat(g) / 255, blue: CGFloat(b) / 255, alpha: CGFloat(a) / 255)
    }
}

extension String {
    
    func isEmail(email: String) -> Bool {
        let EMAIL_REGEX = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
        let emailPredicate = NSPredicate(format: "SELF MATCHES %@", EMAIL_REGEX)
        let result = emailPredicate.evaluate(with: email)
        
        return result
    }
    
    func isPhoneNumber(phoneNumber: String) -> Bool {
        
        let PHONE_REGEX = "^[0-9]{8,12}$"
        let phonePredicate = NSPredicate(format: "SELF MATCHES %@", PHONE_REGEX)
        let result = phonePredicate.evaluate(with: phoneNumber)
        
        return result
    }
    
    func isDigit(digit: String) -> Bool {
        let NUMBER_REGEX = "^[0-9]{1,11}$"
        let numberPredicate = NSPredicate(format: "SELF MATCHES %@", NUMBER_REGEX)
        let result = numberPredicate.evaluate(with: digit)
        
        return result
    }
    
    var numbers: String { return components(separatedBy: Numbers.characterSet.inverted).joined() }
    var integer: Int { return Int(numbers) ?? 0 }
    var double: Double { return Double(numbers) ?? 0 }
}

extension String {
    func heightOf(_ font: UIFont, maxWidth: CGFloat) -> CGFloat {
        let rect = (self as NSString).boundingRect(with: CGSize(width: maxWidth, height: CGFloat.greatestFiniteMagnitude), options: [.usesLineFragmentOrigin, .usesFontLeading], attributes: [NSFontAttributeName:font], context: nil)
        return rect.height
    }
    var html2AttributedString: NSAttributedString? {
        do {
            return try NSAttributedString(data: Data(utf8), options: [NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType, NSCharacterEncodingDocumentAttribute: String.Encoding.utf8.rawValue], documentAttributes: nil)
        } catch {
            print(error)
            return nil
        }
    }
}

extension Int
{
    func toString() -> String
    {
        let myString = String(self)
        
        return myString
    }
}

extension Float
{
    func toString() -> String
    {
        let myString = String(self)
        
        return myString
    }
}

extension Date {
    func getDayOfWeek() -> Int {
        return Calendar.current.dateComponents([.weekday], from: self).weekday!
    }
    func getYear() -> Int {
        return Calendar.current.dateComponents([.year], from: self).year!
    }
    func getMonth() -> Int {
        return Calendar.current.dateComponents([.month], from: self).month!
    }
    func getDay() -> Int {
        return Calendar.current.dateComponents([.day], from: self).day!
    }
}

struct Numbers { static let characterSet = CharacterSet(charactersIn: "0123456789") }

extension NumberFormatter {
    convenience init(numberStyle: NumberFormatter.Style) {
        self.init()
        self.numberStyle = numberStyle
    }
}

extension UIViewController {
    func dismissKeyboardWhenTapped() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(UIViewController.dismissKeyboard))
        view.addGestureRecognizer(tap)
    }
    
    func dismissKeyboard() {
        view.endEditing(true)
    }
}

extension UIImage {
    static func from(color: UIColor) -> UIImage {
        let rect = CGRect(x: 0, y: 0, width: 1, height: 1)
        UIGraphicsBeginImageContext(rect.size)
        let context = UIGraphicsGetCurrentContext()
        context!.setFillColor(color.cgColor)
        context!.fill(rect)
        let img = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return img!
    }
}
