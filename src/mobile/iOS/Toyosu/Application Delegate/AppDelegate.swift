//
//  AppDelegate.swift
//  Toyosu
//
//  Created by Linh Trương on 10/3/16.
//  Copyright © 2016 IBS. All rights reserved.
//

import UIKit
import AFNetworking
import SVProgressHUD
import Localize_Swift
import Firebase
import UserNotifications
import PopupDialog
import IQKeyboardManagerSwift

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, UNUserNotificationCenterDelegate {
    
    var window: UIWindow?
    
    func application(_ application: UIApplication, willFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey : Any]? = nil) -> Bool {
        self.loadDefaultLanguage()
        self.loadSavedLoginInfo()
        self.setLayoutForPopup()
        SVProgressHUD.setDefaultMaskType(SVProgressHUDMaskType.black)
        return true
    }
    
    func setLayoutForPopup() {
        let dialogAppearance = PopupDialogDefaultView.appearance()
        dialogAppearance.messageTextAlignment = .left
        dialogAppearance.titleFont = UIFont.boldSystemFont(ofSize: 17)
        dialogAppearance.titleColor = AppColor.PRIMARY_COLOR
        dialogAppearance.messageColor = UIColor.black
        let buttonDialogAppearance = PopupDialogButton.appearance()
        buttonDialogAppearance.titleColor = UIColor.white
        buttonDialogAppearance.buttonColor = AppColor.PRIMARY_COLOR
    }
    
    func loadDefaultLanguage() {
        if !LocalService.loadFirstLoad() {
            let systemLang = NSLocale.preferredLanguages[0]
            let langEN = Localize.availableLanguages()[1]
            let langCN = Localize.availableLanguages()[2]
            if (systemLang == "zh-hk" || systemLang == "zh-HK" || systemLang == "zh" || systemLang == "zh-Hant-HK") {
                Localize.setCurrentLanguage(langCN)
            } else {
                Localize.setCurrentLanguage(langEN)
            }
            LocalService.saveFirstLoad(true)
        }
    }
    
    func loadSavedLoginInfo () {
        if let customerData = LocalService.loadSavedUserInfo() {
            CurrentCustomerInfo.shareInfo.customer = customerData
//            CurrentCustomerInfo.shareInfo.settingArr = LocalService.loadSavedSettingInfo()
        }
    }
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions:
        [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        
        // Set navigation bar tint / background colour
        UINavigationBar.appearance().barTintColor = AppColor.BACKGROUND_COLOR
        // Set Navigation bar Title colour
        UINavigationBar.appearance().titleTextAttributes = [NSForegroundColorAttributeName:UIColor.black]
        // Set navigation bar Back button tint colour
        UINavigationBar.appearance().tintColor = UIColor.black
        // Set navigation bar Item Button tint colour
        UIBarButtonItem.appearance().tintColor = UIColor.black
        //Set navigation bar Back button tint colour
        UINavigationBar.appearance().tintColor = UIColor.black
        //Set tab bar tint colour
        UITabBar.appearance().tintColor = AppColor.PRIMARY_COLOR
        //Set tab bar bar tint colour
        UITabBar.appearance().barTintColor = UIColor.white
        
        IQKeyboardManager.sharedManager().enable = true
        
        return true
    }
    
    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }
    
    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }
    
    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
        
    }
    
    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }
    
    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }
    
    
}
