//
//  Membership.swift
//  Toyosu
//
//  Created by Linh Trương on 1/23/17.
//  Copyright © 2017 IBS. All rights reserved.
//

import Foundation
import ObjectMapper

typealias MembershipDictionary = [String : AnyObject]

class Membership: NSObject, Mappable
{
    // Variable
    var id: Int?
    var device_id: Int?
    var number: Int?
    var bar_code: String?
    var qr_code: String?
    var type: String?
    
    override init () {}
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        id <- map["id"]
        device_id <- map["device_id"]
        number <- map["number"]
        bar_code <- map["barcode"]
        qr_code <- map["qr_code"]
        type <- map["type"]
    }
}
