//
//  AboutPointAndPrizeViewController.swift
//  Roann
//
//  Created by Minh on 5/4/17.
//  Copyright © 2017 IBS. All rights reserved.
//

import UIKit
import Localize_Swift
import SVProgressHUD

class AboutPointAndPrizeViewController: BaseViewController, UIWebViewDelegate {

    @IBOutlet weak var webView: UIWebView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        webView.stringByEvaluatingJavaScript(from: "localStorage.clear();")
        webView.delegate = self
        
        if Localize.currentLanguage() == "en" {
            if let url = URL(string: LinkPointAndPize.English) {
                let request = URLRequest(url: url)
                webView.loadRequest(request)
            }
        } else {
            if let url = URL(string: LinkPointAndPize.Chinese) {
                let request = URLRequest(url: url)
                webView.loadRequest(request)
            }
        }
        webView.reload()
    }
    
    //MARK:- UIWebViewDelegate Method
    
    func webViewDidStartLoad(_ webView: UIWebView) {
        SVProgressHUD.show()
    }
    func webViewDidFinishLoad(_ webView: UIWebView) {
        SVProgressHUD.dismiss()
    }

}
