//
//  MemberPointView.swift
//  MemberPointView
//
//  Created by Phan Quang Ha on 3/9/17.
//  Copyright © 2017 IBSV. All rights reserved.
//

import UIKit
import Foundation

class LCTextLayer : CATextLayer {
    override init() {
        super.init()
        
        isWrapped = true
        alignmentMode = "center"
        contentsScale = UIScreen.main.scale
    }
    
    override init(layer: Any) {
        super.init(layer: layer)
        self.fontSize = (layer as! LCTextLayer).fontSize
    }
    
    required init(coder aDecoder: NSCoder) {
        super.init(layer: aDecoder)
    }
    
    override func draw(in ctx: CGContext) {
        let height = self.bounds.size.height
        let fontSize = self.fontSize
        
        self.model().fontSize = fontSize > height ? height - 3 : fontSize
        
        let yDiff = (height - fontSize)/2 - fontSize/10
        
        ctx.saveGState()
        ctx.translateBy(x: 0.0, y: yDiff)
        super.draw(in: ctx)
        ctx.restoreGState()
    }
}

public enum MemberPointViewCustomize {
    case circleBackgroundColor(UIColor)
    case indicatorColor(UIColor)
    case indicatorWidthRatio(CGFloat)
    case levelIcon(UIImage?)
    case levelName(String)
    case levelSuffix(String)
    case firstDescription(String)
    case secondDesciption(String)
    case nextLevelName(String)
    case currentPoint(Int)
    case nextLevelPoint(Int)
}

fileprivate class MemberPointViewOption: NSCopying {
    var circleBackgroundColor: UIColor = UIColor.white
    var indicatorColor: UIColor = UIColor.red
    var indicatorWidthRatio: CGFloat = 0.22
    var levelIcon: UIImage? = nil
    var levelName: String? = nil
    var levelSuffix: String? = nil
    var nextLevelName: String? = nil
    var currentPoint: Int = 0
    var nextLevelPoint: Int = -1
    var firstDescription: String? = nil
    var secondDescription: String? = nil
    
    func copy(with zone: NSZone? = nil) -> Any {
        
        let newOption = MemberPointViewOption()
        newOption.circleBackgroundColor = circleBackgroundColor.copy() as! UIColor
        newOption.indicatorColor = indicatorColor.copy() as! UIColor
        newOption.indicatorWidthRatio = indicatorWidthRatio
        newOption.levelIcon = levelIcon
        newOption.levelName = levelName?.copy() as? String
        newOption.levelSuffix = levelSuffix?.copy() as? String
        newOption.currentPoint = currentPoint
        newOption.nextLevelPoint = nextLevelPoint
        newOption.firstDescription = firstDescription?.copy() as? String
        newOption.secondDescription = secondDescription?.copy() as? String
        
        return newOption
    }
}

public class MemberPointView : UIView {
    
    fileprivate lazy var option = MemberPointViewOption()
    fileprivate var lastOption: MemberPointViewOption?
    
    fileprivate var indicatorLayer = CAShapeLayer()
    fileprivate var levelIconLayer = CALayer()
    
    fileprivate var pointLayer = LCTextLayer()
    fileprivate var pLayer = LCTextLayer()
    
    fileprivate var levelNameLayer = LCTextLayer()
    fileprivate var remainPointToNextLevelLayer = LCTextLayer()
    fileprivate var toNextLevelLayer = LCTextLayer()
    
    public override func draw(_ rect: CGRect) {
        super.draw(rect)
        drawBackgroundCircle(rect)
    }
    
    
    fileprivate func insertLayers () {
        
        levelIconLayer.contentsScale = UIScreen.main.scale
        layer.addSublayer(levelIconLayer)
        
        layer.addSublayer(pointLayer)
        pointLayer.fontSize = 70
        
        layer.addSublayer(levelNameLayer)
        
        layer.addSublayer(remainPointToNextLevelLayer)
        
        layer.addSublayer(toNextLevelLayer)
        
        layer.addSublayer(indicatorLayer)
        
        layer.addSublayer(pLayer)
        
        levelIconLayer.isHidden = true
        pointLayer.isHidden = true
        levelNameLayer.isHidden = true
        pLayer.isHidden = true
        remainPointToNextLevelLayer.isHidden = true
        toNextLevelLayer.isHidden = true
    }
    
    public override func layoutSubviews() {
        super.layoutSubviews()
        setNeedsDisplay()
        
        reloadAllLayers()
    }
    
    
    public override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    public override init(frame: CGRect) {
        super.init(frame: frame)
        insertLayers()
    }
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        insertLayers()
    }
    
    public convenience init(frame: CGRect, config: [MemberPointViewCustomize]?) {
        self.init(frame: frame)
        
        insertLayers()
        
        if let config = config {
            changeOption(options: config)
        }
        reloadUI(false)
    }
    
    fileprivate func changeOption(options: [MemberPointViewCustomize]) {
        
        for custom in options {
            switch custom {
            case .circleBackgroundColor(let color):
                option.circleBackgroundColor = color
                break
            case .indicatorColor(let color):
                option.indicatorColor = color
                break
            case .indicatorWidthRatio(let ratio):
                option.indicatorWidthRatio = ratio
                break
            case .levelIcon(let icon):
                option.levelIcon = icon
                break
            case .levelName(let name):
                option.levelName = name
                break
            case .levelSuffix(let name):
                option.levelSuffix = name
                break
            case .nextLevelName(let levelName):
                option.nextLevelName = levelName
                break
            case .currentPoint(let point):
                option.currentPoint = point
                break
            case .nextLevelPoint(let point):
                option.nextLevelPoint = point
                break
            case .firstDescription(let des):
                option.firstDescription = des
                break
            case .secondDesciption(let des):
                option.secondDescription = des
                break
            }
        }
    }
    
    
    public func configPointView(customize: [MemberPointViewCustomize], animate: Bool? = true) {
        if option.nextLevelPoint > 0 {
            lastOption = option.copy() as? MemberPointViewOption
        }
        
        levelIconLayer.isHidden = false
        pointLayer.isHidden = false
        levelNameLayer.isHidden = false
        pLayer.isHidden = false
        remainPointToNextLevelLayer.isHidden = false
        toNextLevelLayer.isHidden = false
        
        changeOption(options: customize)
        reloadUI(animate)
    }
    
    public func reloadUI(_ animating: Bool? = false) {
        
        if animating == true {
            guard let last = lastOption else {
                //first time
                setNeedsDisplay()
                reloadAllLayers()
                
                animateToCurrentPoint()
                return
            }
            
            if option.currentPoint > last.nextLevelPoint {
                finishCurrentLevel()
            }else{
                reloadAllLayers()
                
                animateToCurrentPoint()
            }
            
        }else{
            setNeedsDisplay()
            reloadAllLayers()
        }
    }
}

extension MemberPointView {
    
    func drawBackgroundCircle(_ rect: CGRect) {
        let newRect = rect.insetBy(dx: option.indicatorWidthRatio * rect.width / 4, dy: option.indicatorWidthRatio * rect.width / 4)
        let circularPath = UIBezierPath(roundedRect: newRect, cornerRadius: newRect.width / 2)
        circularPath.lineWidth = option.indicatorWidthRatio * rect.width / 2
        option.circleBackgroundColor.setStroke()
        circularPath.stroke()
    }
    
    func indicatorPath (rect: CGRect, ratio: CGFloat, currentPoint: Int, maximumPoint: Int) -> UIBezierPath {
        let newRect = rect.insetBy(dx: ratio * rect.width / 4, dy: ratio * rect.width / 4)
        
        let value = Double(currentPoint) / Double(maximumPoint)
        
        let indicatorPath = UIBezierPath(arcCenter: CGPoint(x: rect.width / 2, y: rect.height / 2), radius: newRect.width / 2, startAngle: -CGFloat(M_PI_2), endAngle: CGFloat(-M_PI_2 + value * 2 * M_PI), clockwise: true)
        
        return indicatorPath
    }
}

extension MemberPointView {
    
    func reloadIndicatorLayer () {
        indicatorLayer.frame = bounds
        indicatorLayer.strokeColor = option.indicatorColor.cgColor
        indicatorLayer.fillColor = UIColor.clear.cgColor
        indicatorLayer.lineWidth = option.indicatorWidthRatio * bounds.width / 2
        indicatorLayer.path = indicatorPath(rect: bounds, ratio: option.indicatorWidthRatio, currentPoint: option.currentPoint, maximumPoint: option.nextLevelPoint).cgPath
    }
    
    func reloadPointLayer (){
        let width = bounds.width - option.indicatorWidthRatio * bounds.width
        let height = width / 5
        
        pointLayer.font = CTFontCreateWithName(UIFont.boldSystemFont(ofSize:1).fontName as CFString?, 40,  nil)
        pointLayer.fontSize = 40
        pointLayer.foregroundColor = UIColor.black.cgColor
        pointLayer.string = option.currentPoint > 0 ? "\(option.currentPoint)" : "0"
        pointLayer.frame = CGRect.init(x: (bounds.width - width)/2, y: (bounds.height - height) / 2, width: width, height: height)
        pointLayer.setNeedsDisplay()
        
    }
    
    func reloadLevelNameLayer() {
        let width = bounds.width - option.indicatorWidthRatio * 2 * bounds.width
        let height = width / 6
        
        levelNameLayer.font = CTFontCreateWithName(UIFont.systemFont(ofSize:1).fontName as CFString?, 40,  nil)
        levelNameLayer.foregroundColor = UIColor.black.cgColor
        levelNameLayer.string = option.levelName
        levelNameLayer.frame = CGRect.init(x: (bounds.width - width)/2, y: pointLayer.frame.minY - height * 1, width: width, height: height)
        levelNameLayer.setNeedsDisplay()
    }
    
    func reloadRemainingLayer() {
        let width = bounds.width - option.indicatorWidthRatio * 2 * bounds.width
        let height = width / 9
        
        remainPointToNextLevelLayer.font = CTFontCreateWithName(UIFont.systemFont(ofSize:1).fontName as CFString?, 40,  nil)
        remainPointToNextLevelLayer.foregroundColor = UIColor.black.cgColor
        remainPointToNextLevelLayer.string = option.firstDescription //?? "remain \(option.nextLevelPoint - option.currentPoint)p"
        remainPointToNextLevelLayer.frame = CGRect.init(x: (bounds.width - width)/2, y: pointLayer.frame.maxY + height / 2, width: width, height: height)
        remainPointToNextLevelLayer.setNeedsDisplay()
    }
    
    func reloadToNextLayer() {
        let width = bounds.width - option.indicatorWidthRatio * 2 * bounds.width
        let height = width / 11
        
        toNextLevelLayer.font = CTFontCreateWithName(UIFont.systemFont(ofSize:1).fontName as CFString?, 30,  nil)
        toNextLevelLayer.foregroundColor = UIColor.black.cgColor
        toNextLevelLayer.string = option.secondDescription //?? "until \(option.nextLevelName ?? "") level"
        toNextLevelLayer.frame = CGRect.init(x: (bounds.width - width)/2, y: remainPointToNextLevelLayer.frame.maxY + height / 2, width: width, height: height)
        toNextLevelLayer.setNeedsDisplay()
    }
    
    func reloadIconLayer () {
        let width = bounds.width - option.indicatorWidthRatio * 2.8 * bounds.width
        let height = width / 3
        
        
        levelIconLayer.frame = CGRect.init(x: (bounds.width - width)/2, y: levelNameLayer.frame.minY - height, width: width, height: height)
        levelIconLayer.contents = option.levelIcon?.cgImage
        levelIconLayer.contentsGravity = kCAGravityResizeAspect
        
    }
    
    func reloadPLayer() {
        let width = pointLayer.frame.width / 12
        let height = width
        
        pLayer.font = CTFontCreateWithName(UIFont.boldSystemFont(ofSize:1).fontName as CFString?, 40,  nil)
        pLayer.foregroundColor = UIColor.black.cgColor
        pLayer.string = option.levelSuffix
        
        let pointSize = ((pointLayer.string ?? "") as! NSString).boundingRect(with: pointLayer.bounds.size, options: [.usesLineFragmentOrigin, .usesFontLeading], attributes: [NSFontAttributeName : UIFont.systemFont(ofSize: pointLayer.fontSize)], context: nil)
        
        pLayer.frame = CGRect.init(x: (bounds.width - width) / 2 + pointSize.width / 2 + 15, y: (bounds.height / 2) + (pointSize.height / 2 - height) / 2 - 5, width: width, height: height)
        pLayer.setNeedsDisplay()
    }
    
    func reloadAllLayers () {
        reloadIndicatorLayer()
        reloadPointLayer()
        reloadPLayer()
        reloadLevelNameLayer()
        reloadRemainingLayer()
        reloadToNextLayer()
        reloadIconLayer()
    }
    
    func animateIndicatorView(from: CGFloat, to: CGFloat, finishing: Bool? = false){
        let animation = CABasicAnimation(keyPath: "strokeEnd")
        animation.duration = 0.5
        animation.isRemovedOnCompletion = true
        animation.fromValue = from / to
        animation.toValue = 1
        animation.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseIn)
        
        if finishing == true {
            animation.delegate = self
            indicatorLayer.add(animation, forKey: "finishLevel")
        }else{
            indicatorLayer.add(animation, forKey: "drawIndicator")
        }
        
    }
    
    func finishCurrentLevel () {
        fillIndicatorView()
    }
    
    func fillIndicatorView() {
        indicatorLayer.path = indicatorPath(rect: bounds, ratio: option.indicatorWidthRatio, currentPoint: lastOption!.nextLevelPoint, maximumPoint: lastOption!.nextLevelPoint).cgPath
        animateIndicatorView(from: CGFloat(lastOption!.currentPoint) / CGFloat(lastOption!.nextLevelPoint), to: 1, finishing: true)
    }
    
    func animateToCurrentPoint () {
        if let last = lastOption {
            animateIndicatorView(from: CGFloat(last.currentPoint) / CGFloat(last.nextLevelPoint), to: CGFloat(option.currentPoint) / CGFloat(option.nextLevelPoint))
        }else{
            animateIndicatorView(from: 0, to: 1)
        }
    }
}

extension MemberPointView : CAAnimationDelegate {
    public func animationDidStop(_ anim: CAAnimation, finished flag: Bool) {
        setNeedsDisplay()
        reloadAllLayers()
        
        lastOption = nil
        animateToCurrentPoint()
    }
}
